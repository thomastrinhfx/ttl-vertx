package com.itrade.hks.request;

import java.util.Hashtable;
import java.util.Vector;

import com.itrade.tp.TPBaseRequest;
import com.itrade.tp.TPManager;
import com.systekit.common.msg.IMsgXMLNode;

/**
 * The HKSPlaceOrderRequest class defined methods that operation xml request with place of order.
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class HKSPlaceOrderRequest extends TPBaseRequest
{
	/**
	 * Constructor for HKSPlaceOrderRequest class.
	 * @param pTPManager Object of TPManager.
	 * @param pTemplateNode Object of IMsgXMLNode.
	 */
	public HKSPlaceOrderRequest(TPManager pTPManager, IMsgXMLNode pTemplateNode)
	{
		super(pTPManager, pTemplateNode);
	}

	/**
	 * This method send data of modify order xml request.
	 * @param pParameterMap The Map about data.
	 */
	public IMsgXMLNode send(Hashtable pParameterMap)
	{
		//Begin Task #RC00181 - Rice Cheng 20090109
//		IMsgXMLNode lvRootNode = getRequestNode();
//		IMsgXMLNode lvLoop0Node = lvRootNode.getChildNode("LOOP_ORDER").getChildNode("LOOP_ORDER_ELEMENT");
//		Vector		lvMissingField = mergeParameter(pParameterMap, lvLoop0Node);
//		if (lvMissingField.size() > 0)
//		{
//			return makeErrorXML(new Exception("Missing mandatory fields: " + lvMissingField.toString()));
//		}
//		else
//		{
//			return send(lvRootNode);
//		}
		return send(pParameterMap, "");
		//End Task #RC00181 - Rice Cheng 20090109
	}
	
	//Begin Task #RC00181 - Rice Cheng 20090109
	/**
	 * This method send IMsgXMLNode of xml request.
	 * @param pParameterMap The Map about data.
	 * @param pLanguage The currently language.
	 */
	public IMsgXMLNode send(Hashtable pParameterMap, String pLanguage)
	{
		IMsgXMLNode lvRootNode = getRequestNode();
		IMsgXMLNode lvLoop0Node = lvRootNode.getChildNode("LOOP_ORDER").getChildNode("LOOP_ORDER_ELEMENT");
		Vector		lvMissingField = mergeParameter(pParameterMap, lvLoop0Node);
		if (lvMissingField.size() > 0)
		{
			return makeErrorXML(new Exception("Missing mandatory fields: " + lvMissingField.toString()));
		}
		else
		{
			return send(lvRootNode);
		}
	}
	//End Task #RC00181 - Rice Cheng 20090109
}
