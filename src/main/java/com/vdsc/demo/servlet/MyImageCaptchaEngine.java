package com.vdsc.demo.servlet;

import java.awt.Color;
import java.awt.Font;
import com.octo.captcha.component.image.backgroundgenerator.BackgroundGenerator;
import com.octo.captcha.component.image.backgroundgenerator.UniColorBackgroundGenerator;
import com.octo.captcha.component.image.color.RandomRangeColorGenerator;
import com.octo.captcha.component.image.fontgenerator.FontGenerator;
import com.octo.captcha.component.image.fontgenerator.RandomFontGenerator;
import com.octo.captcha.component.image.textpaster.RandomTextPaster;
import com.octo.captcha.component.image.textpaster.TextPaster;
import com.octo.captcha.component.image.wordtoimage.ComposedWordToImage;
import com.octo.captcha.component.image.wordtoimage.WordToImage;
import com.octo.captcha.component.word.wordgenerator.RandomWordGenerator;
import com.octo.captcha.component.word.wordgenerator.WordGenerator;
import com.octo.captcha.engine.image.ListImageCaptchaEngine;
import com.octo.captcha.image.gimpy.GimpyFactory;

public class MyImageCaptchaEngine extends ListImageCaptchaEngine {
	
	private static int WIDTH = 106;
	private static int HEIGHT = 40;
	
	public MyImageCaptchaEngine(){}
	
	protected void buildInitialFactories() {
        WordGenerator wgen = new RandomWordGenerator("0123456789"); /// list chars
        RandomRangeColorGenerator cgen = new RandomRangeColorGenerator(
             new int[] {0, 100},
             new int[] {0, 100},
             new int[] {0, 100});
        TextPaster textPaster = new RandomTextPaster(new Integer(4), new Integer(4), cgen, true);
        
        Color bg = new Color(240, 240, 240);

        BackgroundGenerator backgroundGenerator = new UniColorBackgroundGenerator(new Integer(WIDTH), new Integer(HEIGHT), bg );
        Font[] fontsList = new Font[] {
            new Font("Courier", 0, 16),
            new Font("Times New Roman", 0, 16),
        };
        
        FontGenerator fontGenerator = new RandomFontGenerator(new Integer(20), new Integer(25), fontsList);

        WordToImage wordToImage = new ComposedWordToImage(fontGenerator, backgroundGenerator, textPaster);
        this.addFactory(new GimpyFactory(wgen, wordToImage));
 }

}
