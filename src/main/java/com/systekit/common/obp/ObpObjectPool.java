package com.systekit.common.obp;

/**
 * An implementation of an object pool. It is used to create generic objects by
 * using an object factory, with which extends all the necessary interface to
 * be used by this class. It defines the initial and maximum pool size so that
 * object can be created as needed within the limit. If the number of current
 * objects reaches the maximnm, the client requesting the object has to wait
 * until the other releases/checks-in its used object.
 *
 * Creation date: (3/2/2001 3:51:13 PM)
 * @author: Pear
 */

import java.util.*;

public class ObpObjectPool
{
	protected ObpObjectFactory ivObjectFactory;

	private LinkedList ivPool = new LinkedList();
	private LinkedList ivPoolAll = new LinkedList();
	private int ivMaxCapacity;

	public ObpObjectPool(ObpObjectFactory pObjectFactory, int pMaxCapacity)
	{
		super();
		ivMaxCapacity = pMaxCapacity;
		ivObjectFactory = pObjectFactory;
	}

	/**
	 * Creates a new object pool
	 *
	 * @param pObjectFactory a factory to create generic object
	 * @param pInitialCapacity the initail number object created
	 * @param pMaxCapacity the system object limit
	 */
	public ObpObjectPool(ObpObjectFactory pObjectFactory, int pInitialCapacity, int pMaxCapacity)
	{
		super();
		ivMaxCapacity = pMaxCapacity;
		ivObjectFactory = pObjectFactory;

		createInitialObject(pInitialCapacity);
	}

	public void createInitialObject(int pInitialCapacity)
	{
		if (pInitialCapacity > ivMaxCapacity)
		{
			throw new java.lang.IllegalArgumentException("Initial capacity exceeds maximnu capacity");
		}

		for (int i=0; i<pInitialCapacity; i++)
		{
			createNewObject(false); // create new object without checking out
		}
	}

	/**
	 * check in the object after use
	 *
	 * @param pObject an object to be checked in
	 */
	public void checkIn(Object pObject)
	{
		synchronized (ivPool)
		{
			ivPool.addFirst(pObject);
			ivPool.notifyAll();
		}
	}

	/**
	 * check out an object.
	 *
	 * @param pBlockForWaiting true if the requesting clients has to wait when
	 * the pool runs out of all available objects
	 * @exception java.lang.Exception cannot create a new object
	 */
	public Object checkOut(boolean pBlockForWaiting) throws Exception
	{
		synchronized (ivPool)
		{
			while (true)
			{
				if (ivPool.size() > 0)
				{
					return ivPool.removeFirst();
				}
				else
				{
					// pool is now full, wait for other to release
					// or create new object if max is not reached
					if (ivPoolAll.size() < ivMaxCapacity)
					{
						Object lvObject = createNewObject(true);
						return lvObject;
					}
					else
					{
						if (pBlockForWaiting)
							ivPool.wait();
						else
							return null;
					}
				}
			}
		}
	}

	private Object createNewObject(boolean pCheckOutImmediately)
	{
		Object lvObject = ivObjectFactory.createObject();
		ivPoolAll.add(lvObject);

		if (pCheckOutImmediately == false)
			ivPool.add(lvObject);

		return lvObject;
	}

	/**
	 * Destroys all inactive object in the pool
	 */
	public void destroyAllInactiveObject()
	{
		Object lvObject = null;

		synchronized (ivPool)
		{
			while (ivPool.size() > 0)
			{
				lvObject = ivPool.removeFirst();
				destroyObject(lvObject);
			}
		}
	}

	/**
	 * Destroy a CHECKED OUT object
	 * @param pObject
	 */
	public void destroyObject(Object pObject)
	{
		synchronized (ivPool)
		{
			ivObjectFactory.destroyObject(pObject);
			ivPoolAll.remove(pObject);
		}
	}

	/**
	 * Gets the iterator for the pool
	 */
	public Iterator getIterator()
	{
		return ivPoolAll.iterator();
	}
}
