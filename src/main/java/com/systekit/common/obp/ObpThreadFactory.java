package com.systekit.common.obp;

/**
 * This class is used to create ObpWorkerThread for the ObpThreadPool
 * Creation date: (20/6/2001)
 * @author: Pear
 */

public class ObpThreadFactory implements ObpObjectFactory
{
	private ThreadGroup ivThreadGroup = null;
	private ObpThreadPool ivThreadPool = null;
	private int ivObjectCount = 0;

	public ObpThreadFactory(ThreadGroup pThreadGroup)
	{
		ivThreadGroup = pThreadGroup;
	}

	public Object createObject()
	{
		ivObjectCount++;
		ObpWorkerThread lvWorkerThread = new ObpWorkerThread(ivThreadGroup, "Worker Thread " + ivObjectCount);
		lvWorkerThread.setThreadPool(ivThreadPool);
		lvWorkerThread.startThread();
		return lvWorkerThread;
	}

	public void destroyObject(Object pObject)
	{
		ObpWorkerThread lvWorkerThread = (ObpWorkerThread) pObject;
		lvWorkerThread.stopThread();
	}

	public void setThreadPool(ObpThreadPool pThreadPool)
	{
		ivThreadPool = pThreadPool;
	}
}
