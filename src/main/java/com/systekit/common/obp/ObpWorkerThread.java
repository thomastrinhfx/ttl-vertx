package com.systekit.common.obp;

/**
 * This is a real thread create by ObpThreadFactory, it will run when a new job is assigned and wait
 * when a job finished.
 * Creation date: (20/6/2001)
 * @author: Pear
 */

public class ObpWorkerThread extends Thread
{
	private IObpThreadPoolJob ivJob = null;
	private Object ivNewJobEvent = new Object();
	private Object ivThreadStartedEvent = new Object();

	private boolean ivStop = false;
	private ObpThreadPool ivThreadPool = null;

   public ObpWorkerThread(ThreadGroup pThreadGroup, String pThreadName)
   {
		super(pThreadGroup, pThreadName);
   }

	public void doJob(IObpThreadPoolJob pThreadPoolJob)
	{
		synchronized (ivNewJobEvent)
		{
			ivJob = pThreadPoolJob;
			ivNewJobEvent.notifyAll();
		}
	}

	/**
	 * start the thread and make sure the run loop is entered when this call returns
	 */
	public void startThread()
	{
		synchronized (ivThreadStartedEvent)
		{
			start();
			try
			{
				ivThreadStartedEvent.wait();
			}
			catch (Exception e)
			{
				// this exception is never thrown
			}
		}
	}

	public void run()
	{
		synchronized(ivNewJobEvent)
		{
			// make ivThreadStartedEvent inside ivNewJobEvent to avoid the first doWork hold the lock ivNewJobEvent
			// otherwise the first ivNewJobEvent.wait() won't be notified
			synchronized(ivThreadStartedEvent)
			{
				ivThreadStartedEvent.notifyAll();
			}

			while (!ivStop)
			{
				try
				{
					ivNewJobEvent.wait();
					ivJob.doThreadPoolJob(); // it should never throw any exceptions
				}
				catch(IllegalMonitorStateException e1)
				{
					// ivNewJobEvent is in synchronized block, so this exception will never be thrown
					// actually this exception for the wait method is only thrown for jdk1.1.7
				}
				catch(InterruptedException e2)
				{
					// other thread has interrupted me, i should honor this interruption, so exit
					break;
				}

				if (ivStop)
				{
					break;
				}
				ivThreadPool.checkIn(this);
			}
		}
	}

	/**
	 * Use ObpTHreadPool.destroyObject to properly stop and free this thread
	 */
	void stopThread()
	{
		ivStop = true;
		interrupt();
	}

	public void setThreadPool(ObpThreadPool pThreadPool)
	{
		ivThreadPool = pThreadPool;
	}
}