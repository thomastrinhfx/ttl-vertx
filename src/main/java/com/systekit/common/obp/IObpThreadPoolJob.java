package com.systekit.common.obp;

/**
 * An interface for a thread in the thread pool to run a particular job.
 * Creation date: (20/6/2001)
 * @author: Pear
 */

public interface IObpThreadPoolJob
{
	// this method should never thrown any exception, otherwise the thread in the pool will exit
	// and the object count in the pool will be incorrect
	public void doThreadPoolJob();
}