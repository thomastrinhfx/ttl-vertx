package com.systekit.common.obp;

/**
 * Interface for creating a generic object
 * Creation date: (3/2/2001 3:48:38 PM)
 *
 * @author: Pear
 * @see ObpObjectPool
 */

public interface ObpObjectFactory
{
	/**
	 * Creates a new object for this factory
	 */
	public Object createObject();

	/**
	 * Destroys a given object
	 *
	 * @param pObject an object to be destroyed
	 */
	public void destroyObject(Object pObject);
}
