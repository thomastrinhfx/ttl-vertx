//package com.itrade.web.helper;
//
//import java.math.BigDecimal;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.LinkedHashMap;
//import java.util.List;
//import java.util.Map;
//
//import com.itrade.configuration.FrontEndConfiguration;
//import com.itrade.configuration.diversity.BaseAppDiversityConfiguration;
//import com.itrade.configuration.pattern.PatternContext.EPatternType;
//import com.itrade.configuration.pattern.PatternEntry;
//import com.itrade.ct.support.ITradeCompanyConfig;
//import HKSOrderEnqDetails;
//import HKSInstrument;
//import com.itrade.message.ITradeCommonMessage;
//import com.itrade.tag.ActionTagName;
//import com.itrade.tag.GroupTagName;
//import com.itrade.tag.JSONRawTagName;
//import com.itrade.tag.JSONTagName;
//import com.itrade.tag.TXNTagName;
//import com.itrade.text.format.datetime.DateFormatter;
//import com.itrade.text.format.number.NumberFormatter;
//import com.itrade.util.StringUtils;
//import com.itrade.web.engine.ITradeServlet;
//import com.itrade.web.engine.util.JSONParse;
//import com.itrade5.util.TextUtil;
//import com.systekit.winvest.hks.util.Utils;
//
//*//**
// * 
// * @author xieyiwen
// * @category:(etc.)
// * @since 2014-1-27
// *//*
//
////UPDATE TASK : TTL-HK-WLCW-01688 - 21 DEC 2015 Walter Lau [ALL] Modify and Cancel Order Request change to Modify Big Order Request (ITRADEFIVE-728)
////UPDATE TASK : TTL-HK-WLCW-01689 - 21 DEC 2015 Walter Lau [ALL] MAMK Place Sell Order Lot Size Checking Issue (ITRADEFIVE-718)
//
//public class OrderEnquiryHelper {
////BEGIN TASK #:TTL-GZ-clover_he-00104.2 20140430[ITrade5] Generate main page row data list for order enquiry.[ITRADEFIVE-152]
//	public static List<Map<String, String>> GenerateMainPageRowDataList(HKSOrderEnqDetails[] pHKSOrderEnqDetails ,List<String> pConfigOrderEnquirycolumns, String pLangText,String pDwid,String pHighlightFlag)//add higlight flag
//	{
//		get some need to use configuration 
//		ITradeCompanyConfig lvCompanyConfig = ITradeServlet.svCompanyConfig;
//		String lvDefaultChannelID = lvCompanyConfig.getChannelID("default");
//		String lvDefaultMarketID = lvCompanyConfig.defaultMarket().marketID();
//		boolean lvRejectReasonAndRemark = Boolean.parseBoolean(lvCompanyConfig.getRejectAndRemark());
//		//boolean lvShowOrderDetail = Boolean.parseBoolean(lvCompanyConfig.getGroupTradeDetails());
//		boolean lvShowOrderDetail = "Y".equalsIgnoreCase(lvCompanyConfig.getGroupTradeDetails());
//		String lvHypenString = lvCompanyConfig.getHypenString();			
//		List<Map<String, String>> lvRowDataList = new ArrayList<Map<String,String>>();
//		modifyOrderSequenceOCO(pHKSOrderEnqDetails);
//		int j = 0;
//		String lvbuysell = "";
//		for(int i = pHKSOrderEnqDetails.length-1; i>=0; i-- )
//		{
//		    j++ ;
//			HKSOrderEnqDetails lvOrderEnqDetails = pHKSOrderEnqDetails[i];
//			String lvOrderStatus = lvOrderEnqDetails.getStatus();
//			 *//**
//			 * @author jay.wince
//			 * @description:
//			 *    CMO:Sending.
//			 *    MXX:Sending.
//			 *    MRJ:reject when modifying
//			 *    ALT:Allocated
//			 * @since  2014-03-12
//			 *//*
//			if (lvOrderStatus.equals("CMO"))
//				continue;
//			else if (lvOrderStatus.equals("MXX"))
//				continue;
//			else if (lvOrderStatus.equals("MXL"))
//				continue;
//			else if (lvOrderStatus.equals("MRJ"))
//				continue;
//			else if (lvOrderStatus.equals("MLX"))
//				continue;
//			else if (lvOrderStatus.equals("ALT"))
//                continue;
//			
//			HashMap<String, String> hTable = lvOrderEnqDetails.getMapData();
//			hTable.put(JSONRawTagName.HIGHLIGHT_FLAG, pHighlightFlag);
////BEGIN TASK #:TTL-GZ-clover_he-00104.1 20140424 [ITrade5] get order details JavaScript.[ITRADEFIVE-152]    	
//			HashMap<String, String> lvOrderEnquiryMapData = (HashMap<String, String>) hTable.clone();
////END TASK #:TTL-GZ-clover_he-00104.1 20140424 [ITrade5] get order details JavaScript.[ITRADEFIVE-152]
//			PatternEntry entry = BaseAppDiversityConfiguration.getInstance().getPatternEntry(EPatternType.STOCKQUANTITY);
//    		NumberFormatter priceFormatter = new NumberFormatter(entry.getPattern());
//			String lvMarketID = StringUtils.isNullStr(lvOrderEnqDetails.getMarketID()) ? lvDefaultMarketID : lvOrderEnqDetails.getMarketID().trim();
//			String lvInstrumentID = lvOrderEnqDetails.getStockID().trim();
//			String lvOrderTypeValue = lvOrderEnqDetails.getOrderType().trim();
//		    HKSInstrument lvInstrument = InstrumentManager.instrument(lvMarketID, lvInstrumentID);
//		    if(lvInstrument!=null)
//		    {
////BEGIN TASK #:TTL-GZ-clover_he-00104 2014-04-02 [ITrade5] Get order action script for modify/cancel order.[ITRADEFIVE-152]
//		    	 Modify/Cancel order action 			
//				lvOrderEnquiryMapData.put(ActionTagName.LOT_SIZE, lvInstrument.getLotSize()+"" );	
//				lvOrderEnquiryMapData.put(ActionTagName.QTY_DECIMAL_PLACES, lvInstrument.getQtyDecimalPlaces());
//				if("N".equals(lvOrderTypeValue)){
//					String lvCurrentOrderGroupID = pHKSOrderEnqDetails[i].getOrderGroupID().trim();
//					if("C".equals(lvOrderEnqDetails.getIsCDMaster()) && (i+1 <= pHKSOrderEnqDetails.length-1)){
//						String lvPreviousOrderGroupID = pHKSOrderEnqDetails[i+1].getOrderGroupID().trim();
//						if(lvCurrentOrderGroupID.equals(lvPreviousOrderGroupID)){
//							lvOrderEnquiryMapData.put(ActionTagName.OCO_PRIMARY_ORDER_STATUS, pHKSOrderEnqDetails[i+1].getStatus());
//						}
//					}
//				}
//				*//**
//				 * The "ITradeSessionManager.currentUser().getLocale()"  to priceFormatter.print(Number number)
//				 * can get the Data
//				 * @author canyong.lin
//				 * @since2014-06-20
//				 *//*
////BEGIN TASK #:TTL-GZ-PENGJM-00266.5 20141124[ITradeR5]Order type is L,N,T don't show modify button 
//				//junming on 20141217:Normal tab have modify button
////BEGIN TASK #:TTL-GZ-PENGJM-00334 20150104[ITradeR5]Dwid improvement-Applied to the module(ITRADEFIVE-463)		
//				hTable.put(JSONTagName.PARAMETERS_DATA_JSON, createOrderActionDataJson(lvOrderEnquiryMapData));
//				//if(lvCompanyConfig.orderTypeForAllowedToModify().contains(lvOrderTypeValue) && !StringUtils.isNullStr(lvOrderEnqDetails.getGoodTillDate())){//junming on 20141208:Using configuration
////BEGIN TASK # :TTL-GZ-kelly.kuang-00021 20160111[ITradeR5]Check disable market modify button.
//				if(lvCompanyConfig.orderTypeForAllowedToModify().contains(lvOrderTypeValue) || lvCompanyConfig.disableMarketForModify().contains(lvMarketID)){
////END TASK # :TTL-GZ-kelly.kuang-00021 20160111[ITradeR5]Check disable market modify button.
//					if(isCreateButton(lvOrderEnquiryMapData)){
//						hTable.put(JSONRawTagName.MODIFY_IS_DISPLAY, "none");
//						hTable.put(JSONRawTagName.CANCEL_IS_DISPLAY, "none");
//					}else{
//						hTable.put(JSONRawTagName.MODIFY_IS_DISPLAY, "none");
//                        hTable.put(JSONRawTagName.CANCEL_IS_DISPLAY, "block");
//					}
//				}else{
//					if(isCreateButton(lvOrderEnquiryMapData)){
//						hTable.put(JSONRawTagName.MODIFY_IS_DISPLAY, "none");
//                        hTable.put(JSONRawTagName.CANCEL_IS_DISPLAY, "none");
//					}else{
//					    hTable.put(JSONRawTagName.MODIFY_IS_DISPLAY, "block");
//                        hTable.put(JSONRawTagName.CANCEL_IS_DISPLAY, "block");
//					}
//				}
////END TASK #:TTL-GZ-PENGJM-00334 20150104[ITradeR5]Dwid improvement-Applied to the module(ITRADEFIVE-463)				
////END TASK #:TTL-GZ-PENGJM-00266.5 20141124[ITradeR5]Order type is L,N,T don't show modify button
////END TASK #:TTL-GZ-clover_he-00104 2014-04-02 [ITrade5] Get order action script for modify/cancel order.[ITRADEFIVE-152]
//			    hTable.put(JSONTagName.STOCK_ID,lvOrderEnqDetails.getStockID());
//				String lvInstrumentName = InstrumentManager.getInstrumentName(lvMarketID, lvOrderEnqDetails.getStockID(), pLangText);
//				hTable.put(JSONTagName.STOCK_NAME, StringUtils.isNullStr(lvInstrumentName)?"":lvInstrumentName);
//				
////				String lvFormatQty = TextFormatter.formatNumber(lvOrderEnqDetails.getQty(), "", lvStockQuantityFormat);
////				String lvFormatQty = lvCompanyConfig.formatStockQuantity(lvOrderEnqDetails.getQty(), "",ITradeSessionManager.currentUser().getLocale());
//				String lvFormatQty = priceFormatter.print(Double.parseDouble(lvOrderEnqDetails.getQty()));
//				hTable.put(JSONTagName.QTY, lvFormatQty);
//				
////BEGIN TASK #:TTL-GZ-STEVENZG.LI-00028 2016-01-12[ITradeR5-CISI]Show default Market/Currency Lable when have not found in the i18n config file.(ITRADEFIVE-751)
//				String lvCurrencyId = lvOrderEnqDetails.getCurrencyID().trim();
//				String lvCurrencyIDText = ITradeCommonMessage.NlocalizedText("CCY."+ lvCurrencyId,pLangText);//junming on 20140930:Lang text fixed
//				if (lvCurrencyIDText.equals("CCY." + lvCurrencyId)) {
//				    lvCurrencyIDText = lvCurrencyId;
//				}
//				hTable.put(JSONTagName.CURRENCY_ID, lvCurrencyIDText);
////END TASK #:TTL-GZ-STEVENZG.LI-00028 2016-01-12[ITradeR5-CISI]Show default Market/Currency Lable when have not found in the i18n config file.(ITRADEFIVE-751)
//				String lvChannelID = StringUtils.isNullStr(lvOrderEnqDetails.getChannelID())?lvDefaultChannelID:lvOrderEnqDetails.getChannelID();
//				String lvDisplayChannelID = ITradeCommonMessage.NlocalizedText("channel."+lvChannelID,pLangText);
//				if (lvDisplayChannelID.equals("channel." + lvChannelID)) {
//				    lvDisplayChannelID = lvChannelID;
//				}
//				hTable.put(JSONTagName.CHANNEL, lvDisplayChannelID);
// BEGIN TASK #:TTL-GZ-demonwx.gu-00001 20150716[ITradeR5]add remark(ITRADEFIVE-560) 
//				String lvRemark = lvOrderEnqDetails.getRemark().trim();
//				if(lvRejectReasonAndRemark)
//				{
//					if(!StringUtils.isNullStr(lvRemark)){
//						lvRemark = lvRemark + ", " + lvOrderEnqDetails.getRejectReason().trim();
//					}else{
//					    lvRemark = lvOrderEnqDetails.getRejectReason().trim();
//					}
//				}
//				hTable.put(JSONTagName.REMARK, lvRemark );	
//				
// END TASK #:TTL-GZ-demonwx.gu-00001 20150716[ITrade5]add remark(ITRADEFIVE-560) 
//				entry = BaseAppDiversityConfiguration.getInstance().getPatternEntry(EPatternType.STOCKPRICE);
//	    	    priceFormatter = new NumberFormatter(entry.getPattern());
////BEGIN TASK #:TTL-GZ-PENGJM-00266.5 20141124[ITradeR5]Order type is A|M don't format value
//	    	    String lvPendPriceStr = "";
//				if(!StringUtils.isNullStr(lvOrderTypeValue) && ("A".equals(lvOrderTypeValue) || "M".equals(lvOrderTypeValue)))
//		        {
//					hTable.put(JSONTagName.PRICE,  lvCompanyConfig.formatPrice("0", "", ITradeSessionManager.currentUser().getLocale()));
//		        }else{        	
//		            hTable.put(JSONTagName.PRICE,  lvCompanyConfig.formatPrice(lvOrderEnqDetails.getPrice(), "", ITradeSessionManager.currentUser().getLocale()));
//		        }
//					hTable.put(JSONTagName.PRICE,  lvOrderEnqDetails.getPendingPrice());
//					lvPendPriceStr = lvOrderEnqDetails.getPendingPrice();
//		        }else{        	
//		            lvPendPriceStr = priceFormatter.print(Double.parseDouble(lvOrderEnqDetails.getPendingPrice()));
//                    if((BigDecimal.ZERO.compareTo(new BigDecimal(StringUtils.replace(lvPendPriceStr, ",", ""))) == 0 || lvOrderStatus.equals("PSB"))) {
//                        lvPendPriceStr = "-";
//                    }
////END TASK #:TTL-GZ-PENGJM-00266.5 20141124[ITradeR5]Order type is A|M don't format value
//		            hTable.put(JSONTagName.PRICE,  priceFormatter.print(Double.parseDouble(lvOrderEnqDetails.getPrice())));
//		        }
//		        
////BEGIN TASK #:TTL-GZ-PENGJM-00091 2013-07-26[BOCM]show GTOOrder in day trade
//		        String lvGTDOrder = StringUtils.isNullStr(lvOrderEnqDetails.getGoodTillDate()) ?  lvHypenString : lvOrderEnqDetails.getGoodTillDate();
//		        hTable.put(JSONTagName.GTD_ORDER, lvGTDOrder );
////END TASK #:TTL-GZ-PENGJM-00091 2013-07-26[BOCM]show GTOOrder in day trade
//		        if(!svEnableMultiMarket)
//				hTable.remove(TagName.MARKETID);
//		        
////		        hTable.put(JSONTagName.AVG_PRICE, lvCompanyConfig.formatAvgPrice(lvOrderEnqDetails.getAvgPrice(), "", ITradeSessionManager.currentUser().getLocale()));
////		        hTable.put(TagName.CANCELQTY,  TextFormatter.formatNumber(lvOrderEnqDetails.getCancelQty(), "", lvStockQuantityFormat));
////				hTable.put(JSONTagName.FILLED, TextFormatter.formatNumber(lvOrderEnqDetails.getFilledQty(), "", lvStockQuantityFormat) );
////     			hTable.put(TagName.CANCELQTY,  lvCompanyConfig.formatStockQuantity(lvOrderEnqDetails.getCancelQty(), "", ITradeSessionManager.currentUser().getLocale()));
////				hTable.put(JSONTagName.FILLED, lvCompanyConfig.formatStockQuantity(lvOrderEnqDetails.getFilledQty(), "", ITradeSessionManager.currentUser().getLocale()) );
//	    	    
//	    	    
//				hTable.put(JSONTagName.OS_QTY,  lvCompanyConfig.formatStockQuantity(lvOrderEnqDetails.getOSQty(), "", ITradeSessionManager.currentUser().getLocale()) );
//				hTable.put(TagName.PENDQTY,  lvCompanyConfig.formatStockQuantity(lvOrderEnqDetails.getPendingQty(), "", ITradeSessionManager.currentUser().getLocale()) );
//		        hTable.put(JSONTagName.AVG_PRICE, ITradeServlet.svCompanyConfig.formatAvgPrice(String.valueOf(lvOrderEnqDetails.getAvgPrice()), "",ITradeServlet.svCompanyConfig.toLocal(pLangText)));
//		        entry = BaseAppDiversityConfiguration.getInstance().getPatternEntry(EPatternType.STOCKQUANTITY);
//	    	    priceFormatter = new NumberFormatter(entry.getPattern());
//		        hTable.put(JSONTagName.CANCEL_QTY,  priceFormatter.print(Double.parseDouble(lvOrderEnqDetails.getCancelQty()))); 20140720 demonwx.gu  add it
//				hTable.put(JSONTagName.FILLED, priceFormatter.print(Double.parseDouble(lvOrderEnqDetails.getFilledQty())));
//			   
////			    hTable.put(JSONTagName.REMARK,  lvOrderEnqDetails.getRemark());  /*20150730 demonwx.gu  notes it*/
//				hTable.put(JSONTagName.OS_QTY,  priceFormatter.print(Double.parseDouble(lvOrderEnqDetails.getOSQty())));
////BEGIN TTL-GZ-PENGJM-00308 20141112[ITradeR5]Order Enquiry controls for dashboard preview.(ITRADEFIVE-372)				
//				String lvPendQtyStr = priceFormatter.print(Double.parseDouble(lvOrderEnqDetails.getPendingQty()));
//				if((new Double(lvOrderEnqDetails.getPendingQty()).longValue() == 0 || lvOrderStatus.equals("PSB"))) {
//	                lvPendQtyStr = "-";
//				}
//				hTable.put(JSONTagName.PEND_QTY,  lvPendQtyStr);
//				entry = BaseAppDiversityConfiguration.getInstance().getPatternEntry(EPatternType.STOCKPRICE);
//	    	    priceFormatter = new NumberFormatter(entry.getPattern());
//				try{
//					hTable.put(JSONTagName.PEND_PRICE, lvPendPriceStr);
//				}catch(NumberFormatException nfe){
//					hTable.put(JSONTagName.PEND_PRICE, lvOrderEnqDetails.getPendingPrice());
//				}
////END TTL-GZ-PENGJM-00308 20141112[ITradeR5]Order Enquiry controls for dashboard preview.(ITRADEFIVE-372)
//				
//				BigDecimal svZero = new BigDecimal("0");
//				if (lvOrderStatus.equals("BIX")){
//					if (svZero.compareTo(new BigDecimal(lvOrderEnqDetails.getFilledQty())) == 0){
//						lvOrderStatus = "BIX1"; // Queue
//					}else{
//						lvOrderStatus = "BIX2"; // Partially Filled
//					}
//				}
//				String lvDisplayStatus = ITradeCommonMessage.NlocalizedText("orderStatus."+lvOrderStatus,pLangText);
//				if (lvDisplayStatus.equals("orderStatus." + lvOrderStatus)) {
//				    lvDisplayStatus = lvOrderStatus;
//				}
//				hTable.put(JSONTagName.STATUS, lvDisplayStatus);//junming on 20140930:Lang text fixed
//				hTable.put(JSONTagName.BS, ITradeCommonMessage.NlocalizedText("BuyOrSell."+lvOrderEnqDetails.getBS(),pLangText));
//				
//				String lvOrderTypeText = ITradeCommonMessage.NlocalizedOrderTypeText(lvOrderEnqDetails.getOrderType(),pLangText);
//		        hTable.put(JSONTagName.ORDER_TYPE, lvOrderTypeText );
//		        
//		        String lvStopPrice = lvOrderEnqDetails.getStopPrice();
//		        if(!StringUtils.isNullStr(lvStopPrice)){
//		        	BigDecimal lvStopPriceBig = new BigDecimal(lvStopPrice);
//		        	if(svZero.compareTo(lvStopPriceBig) == 0){
//		        		hTable.put(JSONTagName.TRI_PRICE, lvHypenString);
//		        	}else{
//		        		//hTable.put(JSONTagName.TRI_PRICE, lvCompanyConfig.formatPrice(lvStopPrice, "", ITradeSessionManager.currentUser().getLocale()));
//		        		hTable.put(JSONTagName.TRI_PRICE, priceFormatter.print(Double.parseDouble(lvStopPrice)));
//		        	
//		        	}
//		        }else{
//		        	hTable.put(JSONTagName.TRI_PRICE, lvHypenString);
//		        }
//		        
//		        hTable.put(JSONTagName.INPUT_TIME, lvOrderEnqDetails.getInputTime());
//		        hTable.put(JSONTagName.MODIFIED_TIME, lvOrderEnqDetails.getModifiedTime());20140720 demonwx.gu  add it
//		        
////BEGIN TASK #:TTL-GZ-clover_he-00104.1 20140424 [ITrade5] get order details JavaScript.[ITRADEFIVE-152]		
//				if(lvShowOrderDetail)
//				{
//					lvOrderEnquiryMapData.put("RejectReasonAndRemark", String.valueOf(lvRejectReasonAndRemark));
//					if("N".equals(lvOrderTypeValue)){
//						String lvCurrentOrderGroupID = pHKSOrderEnqDetails[i].getOrderGroupID().trim();
//						if("P".equals(lvOrderEnqDetails.getIsCDMaster()) && i-1>=0){
//							String lvNextOrderGroupID = pHKSOrderEnqDetails[i-1].getOrderGroupID().trim();
//							if(lvCurrentOrderGroupID.equals(lvNextOrderGroupID)){
//								lvOrderEnquiryMapData.put(ActionTagName.OCO_SECONDARY_ORDER_STATUS, pHKSOrderEnqDetails[i-1].getStatus());
//								lvOrderEnquiryMapData.put(ActionTagName.OCO_SECONDARY_AVGERAGE_PRICE, pHKSOrderEnqDetails[i-1].getAvgPrice());
//								lvOrderEnquiryMapData.put(ActionTagName.LOWEST_LIMIT_PRICE, pHKSOrderEnqDetails[i-1].getPrice());
//								lvOrderEnquiryMapData.put(ActionTagName.STOP_PRICE, pHKSOrderEnqDetails[i-1].getStopPrice());
//							}
//							hTable.put(JSONTagName.ORDER_DETAIL_PARAMS_JSON, getOrderDetailAction(lvOrderEnquiryMapData).get(JSONTagName.ORDER_DETAIL_PARAMS_JSON));//junming on 20150427 : Order details json params
//						}
//					}else{
//						hTable.put(JSONTagName.ORDER_DETAIL_PARAMS_JSON, getOrderDetailAction(lvOrderEnquiryMapData).get(JSONTagName.ORDER_DETAIL_PARAMS_JSON));
//					}
//				}
//				hTable.put(JSONTagName.ORDER_GROUP_ID, lvOrderEnqDetails.getOrderGroupID());
////END TASK #:TTL-GZ-clover_he-00104.1 20140424 [ITrade5] get order details JavaScript.[ITRADEFIVE-152]
//
////BEGIN TASK #:TTL-GZ-clover_he-00105 2014-04-02 [ITrade5] add template for OCO order's second order. [ITRADEFIVE-91]
//				if("N".equals(lvOrderTypeValue)){			
//					if("C".equals(lvOrderEnqDetails.getIsCDMaster())){
//						hTable.put(JSONTagName.MIN_SELL_PRICE, ITradeCommonMessage.NlocalizedText("orderEnquiry.label.MINSellPrice",pLangText));//junming on 20140930:Lang text fixed
//						String lvEmptyString = lvCompanyConfig.getEmptyString();
//						hTable.put(JSONTagName.CURRENCY_ID, lvEmptyString);
//						hTable.put(JSONTagName.BS, lvEmptyString);
////						hTable.put(JSONTagName.QTY, lvEmptyString);
//						hTable.put(JSONTagName.ORDER_TYPE, lvEmptyString);
//						hTable.put(JSONTagName.ORDER_GROUP_ID, lvOrderEnqDetails.getOrderGroupID());
//					}
//				}else{
//					hTable.put(JSONTagName.MIN_SELL_PRICE,lvHypenString);
//				}
////END TASK #:TTL-GZ-clover_he-00105 2014-04-02 [ITrade5] add template for OCO order's second order. [ITRADEFIVE-91]
//			  
//				
//				if(lvOrderEnqDetails.getBS().equals("B")){
//					lvbuysell = "buy";
//				}
//				else{
//					lvbuysell = "sell";
//				}
//			    hTable.put(JSONRawTagName.BS,lvbuysell);
//				hTable.put(JSONRawTagName.STATUS,lvOrderStatus);
//				hTable.put(JSONRawTagName.ORDER_TYPE, lvOrderTypeValue);
//				hTable.put(JSONRawTagName.IS_CD_MASTER, lvOrderEnqDetails.getIsCDMaster());
////BEGIN TTL-GZ-PENGJM-00308 20141112[ITradeR5]Order Enquiry controls for dashboard preview.(ITRADEFIVE-372)				
////BEGIN TASK #:TTL-GZ-STEVENZG.LI-00028 2016-01-12[ITradeR5-CISI]Show default Market/Currency Lable when have not found in the i18n config file.(ITRADEFIVE-751)
//				String lvMarketIdText = ITradeCommonMessage.NlocalizedText("market."+lvMarketID,pLangText);
//				if (lvMarketIdText.equals("market." + lvMarketID)) {
//				    lvMarketIdText = lvMarketID;
//				}
//				hTable.put(JSONRawTagName.MARKET_ID, lvMarketIdText);//junming on 20150212:lang text fixed
////END TASK #:TTL-GZ-STEVENZG.LI-00028 2016-01-12[ITradeR5-CISI]Show default Market/Currency Lable when have not found in the i18n config file.(ITRADEFIVE-751)
//				hTable.put(JSONRawTagName.NET_AMOUNT,lvOrderEnqDetails.getNetAmt());
//				
//				Map<String, String> lvRowDataModel = new LinkedHashMap<String, String>();
//				for(int flag=0;flag<pConfigOrderEnquirycolumns.size();flag++){
//					lvRowDataModel.put(pConfigOrderEnquirycolumns.get(flag),hTable.get(pConfigOrderEnquirycolumns.get(flag)));
//				}
//				
//				if ("true".equals(lvCompanyConfig.getTeblicGroupSupported("order-book/HKSOEEPINT002","group-supported"))) {
//					lvRowDataModel.put(GroupTagName.MARKET_ID, lvMarketID);
//					lvRowDataModel.put(GroupTagName.CURRENCY_ID, lvOrderEnqDetails.getCurrencyID());
//					lvRowDataModel.put(JSONRawTagName.BS,lvOrderEnqDetails.getBS());
//					lvRowDataModel.put(JSONRawTagName.ORDER_TYPE,lvOrderEnqDetails.getOrderType());
//					lvRowDataModel.put(JSONRawTagName.STATUS,lvOrderEnqDetails.getStatus());
//					lvRowDataModel.put(JSONRawTagName.IS_CD_MASTER,lvOrderEnqDetails.getIsCDMaster());
//				}
//				lvRowDataList.add(lvRowDataModel);
//		    }
//
//		    if(j==lvCompanyConfig.getTradingMainDayTradeRecord() && "tradingc-HKSOEEPINT002".equals(pDwid)){
//                break;
//            }
////END TTL-GZ-PENGJM-00308 20141112[ITradeR5]Order Enquiry controls for dashboard preview.(ITRADEFIVE-372)
//		}
//		return lvRowDataList;
//	}
////END TASK #:TTL-GZ-clover_he-00104.2 20140430[ITrade5] Generate main page row data list for order enquiry.[ITRADEFIVE-152]
//
////BEGIN TASK #:TTL-GZ-clover_he-00104.2 20140430 [ITrade5] create cancel/modify order action parameters data.[ITRADEFIVE-152]
//	private static String createOrderActionDataJson(HashMap<String, String> pOrderEnquiryMapData)
//	{	
//		ITradeCompanyConfig lvCompanyConfig = ITradeServlet.svCompanyConfig;
//		SimpleDateFormat	dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		PatternEntry entry = BaseAppDiversityConfiguration.getInstance().getPatternEntry(EPatternType.TIME);
//		DateFormatter formatDate = new DateFormatter(entry.getPattern());
//		String lvOrderType = pOrderEnquiryMapData.get(TXNTagName.ORDER_TYPE);
//		String lvIsCDMaster = pOrderEnquiryMapData.get(TXNTagName.IS_CD_MASTER);
//		Date lvInputTime = null;
//		try {
//			lvInputTime = dateFormatter.parse((String) pOrderEnquiryMapData.get(TXNTagName.INPUT_TIME));
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		 Modify/Cancel order parameters data 
//		Map<String, String> lvActionTemplateData = new HashMap<String, String>();
//		lvActionTemplateData.put(JSONTagName.ORDER_ID, pOrderEnquiryMapData.get(TXNTagName.ORDER_ID));
//		lvActionTemplateData.put(JSONTagName.BUY_SELL, pOrderEnquiryMapData.get(TXNTagName.BS));
//		lvActionTemplateData.put(JSONTagName.INSTRUMENT_ID, pOrderEnquiryMapData.get(TXNTagName.INSTRUMENT_ID));
//		lvActionTemplateData.put(JSONTagName.MARKET_ID, pOrderEnquiryMapData.get(TXNTagName.MARKET_ID));
//		lvActionTemplateData.put(JSONTagName.QUANTITY, pOrderEnquiryMapData.get(TXNTagName.QTY));
//		lvActionTemplateData.put(JSONTagName.CANCEL_QUANTITY, pOrderEnquiryMapData.get(TXNTagName.CANCEL_QTY));
////		lvActionTemplateData.put(JSONTagName.INPUT_TIME, TextFormatter.getFormattedTime( (String) pOrderEnquiryMapData.get(TXNTagName.INPUT_TIME), "HH:mm:ss"));
//		lvActionTemplateData.put(JSONTagName.INPUT_TIME, formatDate.print(lvInputTime));
//		lvActionTemplateData.put(JSONTagName.STOP_TYPE, pOrderEnquiryMapData.get(TXNTagName.STOP_ORDER_TYPE));		
//		lvActionTemplateData.put(JSONTagName.ORDER_TYPE, lvOrderType);
//		lvActionTemplateData.put(JSONTagName.ALL_OR_NOTHING, pOrderEnquiryMapData.get(TXNTagName.ALL_OR_NOTHING));
//		lvActionTemplateData.put(JSONTagName.STOP_ORDER_EXP_DATE, StringUtils.isNullStr(pOrderEnquiryMapData.get(TXNTagName.STOP_ORDER_EXPIRY_DATE))?"":pOrderEnquiryMapData.get(TXNTagName.STOP_ORDER_EXPIRY_DATE));
//		lvActionTemplateData.put(JSONTagName.VALIDITY_DATE, StringUtils.isNullStr(pOrderEnquiryMapData.get(TXNTagName.VALIDITY_DATE)) ? "" : pOrderEnquiryMapData.get(TXNTagName.VALIDITY_DATE));
//		lvActionTemplateData.put(JSONTagName.ACTIVATION_DATE, StringUtils.isNullStr(pOrderEnquiryMapData.get(TXNTagName.ACTIVATION_DATE))?"":pOrderEnquiryMapData.get(TXNTagName.ACTIVATION_DATE));
//		lvActionTemplateData.put(JSONTagName.GOOD_TILL_DATE, pOrderEnquiryMapData.get(TXNTagName.GOOD_TILL_DATE));
//		lvActionTemplateData.put(JSONTagName.REMARK, pOrderEnquiryMapData.get(TXNTagName.REMARK));
//		lvActionTemplateData.put(JSONTagName.CONTACT_PHONE, pOrderEnquiryMapData.get(TXNTagName.CONTACT_PHONE));		
//		lvActionTemplateData.put(JSONTagName.GROSS_AMOUNT, pOrderEnquiryMapData.get(TXNTagName.GROSS_AMT));
//		lvActionTemplateData.put(JSONTagName.NET_AMOUNT, pOrderEnquiryMapData.get(TXNTagName.NET_AMT));
//		lvActionTemplateData.put(JSONTagName.SCRIP, pOrderEnquiryMapData.get(TXNTagName.SCRIP));
//		lvActionTemplateData.put(JSONTagName.ORDER_GROUP_ID, pOrderEnquiryMapData.get(TXNTagName.ORDER_GROUP_ID));		
//		lvActionTemplateData.put(JSONTagName.AVERAGE_PRICE, pOrderEnquiryMapData.get(TXNTagName.AVG_PRICE));
//		lvActionTemplateData.put(JSONTagName.OS_QUANTITY, pOrderEnquiryMapData.get(TXNTagName.OS_QTY));
//		lvActionTemplateData.put(JSONTagName.FILLED_QUANTITY, pOrderEnquiryMapData.get(TXNTagName.FILLED_QTY));
//		lvActionTemplateData.put(JSONTagName.CHANNEL_ID, pOrderEnquiryMapData.get(TXNTagName.CHANNEL_ID));
//		lvActionTemplateData.put(JSONTagName.LOT_SIZE, pOrderEnquiryMapData.get(ActionTagName.LOT_SIZE));
//		lvActionTemplateData.put(JSONTagName.IS_CD_MASTER, lvIsCDMaster);
//		lvActionTemplateData.put(JSONTagName.QTY_DECIMAL_PLACES, pOrderEnquiryMapData.get(ActionTagName.QTY_DECIMAL_PLACES));
//		
//		String lvPriceValue = pOrderEnquiryMapData.get(TXNTagName.PRICE);
//		if("A".equals(lvOrderType) || "M".equals(lvOrderType))
//        {
//            lvPriceValue = "0";
//        }
//		lvActionTemplateData.put(JSONTagName.PRICE, lvPriceValue);
//		
//		if("N".equals(lvOrderType) || "T".equals(lvOrderType) || lvOrderType.equals("P")){
//			lvActionTemplateData.put(JSONTagName.STOP_PRICE, pOrderEnquiryMapData.get(TXNTagName.STOP_PRICE));
//		}
//		if("N".equals(lvOrderType) && "C".equals(lvIsCDMaster)){
//			lvActionTemplateData.put(JSONTagName.LOWEST_LIMIT_PRICE, pOrderEnquiryMapData.get(TXNTagName.PRICE));	
//		}
//		return JSONParse.writeJSON(lvActionTemplateData);
//	}
//	
//	private static String checkOrderStatus(HashMap<String, String> pOrderEnquiryMapData)
//	{
//		String lvOrderStatus = pOrderEnquiryMapData.get(TXNTagName.STATUS_INTERNAL);
//// Added by Bowen Chau on 18 Apr 2006
//// Handle WC and WM
//		BigDecimal lvQty = new BigDecimal(pOrderEnquiryMapData.get(TXNTagName.QTY).replaceAll(",", ""));
//		BigDecimal lvFilledQty = new BigDecimal(pOrderEnquiryMapData.get(TXNTagName.FILLED_QTY).replaceAll(",", ""));
//		BigDecimal lvCancelledQty = new BigDecimal(pOrderEnquiryMapData.get(TXNTagName.CANCEL_QTY).replaceAll(",", ""));
//		BigDecimal lvPendQty = new BigDecimal(pOrderEnquiryMapData.get(TXNTagName.PEND_QTY).replaceAll(",", ""));
//		BigDecimal lvOSQty = new BigDecimal(pOrderEnquiryMapData.get(TXNTagName.OS_QTY).replaceAll(",", ""));
//		*//**
//		 * @author jay.wince
//		 * @description:
//		 *    BPM:Waiting cancel.
//		 *    MPS:Waiting modify.
//		 *    SPO:Ready To Send
//		 *    SPP:Partially Filled
//		 * @since  2014-03-12
//		 *//*
//		if(lvOrderStatus.equals("SPO") && lvOSQty.compareTo(new BigDecimal(0)) != 0 && lvFilledQty.compareTo(new BigDecimal(0)) != 0){
//			lvOrderStatus = "SPP";
//		}
//		if (lvOrderStatus.equals("BPM")) {
//			if ((lvPendQty.abs()).compareTo(lvQty.subtract(lvFilledQty).subtract(lvCancelledQty)) == 0) {
//				lvOrderStatus = "BPM";
//			} else {
//				lvOrderStatus = "MPS";
//			}
//		}
//		return lvOrderStatus;
//		
//	}
////END TASK #:TTL-GZ-clover_he-00104.2 20140430 [ITrade5] create cancel/modify order action parameters data..[ITRADEFIVE-152]
//	
////BEGIN TASK #:TTL-GZ-PENGJM-00334 20150104[ITradeR5]Dwid improvement-Applied to the module(ITRADEFIVE-463)
//	private static boolean isCreateButton(HashMap<String, String> pOrderEnquiryMapData)
//	{
//	    String lvOrderType = pOrderEnquiryMapData.get(TXNTagName.ORDER_TYPE);		
//	    String lvOrderStatus = checkOrderStatus(pOrderEnquiryMapData);
//	    boolean lvCancelOrderAble = ITradeServlet.svCompanyConfig.orderStatusForAllowedToCancel().contains(lvOrderStatus);
//	    if("N".equals(lvOrderType)){
//	        String lvIsCDMaster = pOrderEnquiryMapData.get(TXNTagName.IS_CD_MASTER);
//	        if("P".equals(lvIsCDMaster) &&  lvCancelOrderAble ){
//	            return false;
//	        }
//	        if("C".equals(lvIsCDMaster)){
//	            String lvPrimaryOrderStatus = pOrderEnquiryMapData.get(ActionTagName.OCO_PRIMARY_ORDER_STATUS);
//	            if (lvPrimaryOrderStatus==null) {
//	                lvPrimaryOrderStatus = "";
//	            }
//	            if(lvCancelOrderAble &&  !ITradeServlet.svCompanyConfig.orderStatusForAllowedToCancel().contains(lvPrimaryOrderStatus)){
//	                return false;
//	            }
//	        }
//	    }else{
//	        if(lvCancelOrderAble){
//	            return false;
//	        }
//	    }		
//	    return true;
//	}
////END TASK #:TTL-GZ-PENGJM-00334 20150104[ITradeR5]Dwid improvement-Applied to the module(ITRADEFIVE-463)
////BEGIN TASK #:TTL-GZ-clover_he-00104.2 20140430 [ITrade5] get modify order JavaScript.[ITRADEFIVE-152]
//	private static String createModifyAction(HashMap<String, String> pOrderEnquiryMapData)
//	{
//		String lvOrderType = pOrderEnquiryMapData.get(TXNTagName.ORDER_TYPE);
//		Map<String, String> lvRowOrderModifiedActionTextMap = new HashMap<String, String>();
//		
//		if(!"N".equals(lvOrderType)){			
//			String lvOrderStatus = checkOrderStatus(pOrderEnquiryMapData);
//			if (ITradeServlet.svCompanyConfig.orderStatusForAllowedToModify().contains(lvOrderStatus)){
//				Map<String, String> lvModifiedScriptTextMap = new HashMap<String, String>();
//				lvModifiedScriptTextMap.put(JSONTagName.PARAMETERS_DATA_JSON, createOrderActionDataJson(pOrderEnquiryMapData));
//				lvModifiedScriptTextMap.put(JSONTagName.DWID, pOrderEnquiryMapData.get("Dwid"));	
//				String lvModifyOrderScript = TextUtil.composeMessage(FrontEndConfiguration.modifyOrderScript(),lvModifiedScriptTextMap);
//				lvRowOrderModifiedActionTextMap.put(JSONTagName.MODIFY_ORDER_ACTION, lvModifyOrderScript);
//			}
//		}		
//		return TextUtil.composeMessage(FrontEndConfiguration.modifyOrderDivScript(),lvRowOrderModifiedActionTextMap);
//		
//	}
////END TASK #:TTL-GZ-clover_he-00104.2 20140430 [ITrade5] get modify order JavaScript.[ITRADEFIVE-152]
////BEGIN TASK #:TTL-GZ-clover_he-00104.2 20140430 [ITrade5] get cancel order JavaScript.[ITRADEFIVE-152]
//	private static String createCancelAction(HashMap<String, String> pOrderEnquiryMapData)
//	{
//		String lvOrderType = pOrderEnquiryMapData.get(TXNTagName.ORDER_TYPE);		
//		Map<String, String> lvRowOrderCancelActionTextMap = new HashMap<String, String>();
//		String lvOrderStatus = checkOrderStatus(pOrderEnquiryMapData);
//		
//		boolean lvCancelOrderAble = ITradeServlet.svCompanyConfig.orderStatusForAllowedToCancel().contains(lvOrderStatus);
//		
//		Map<String, String> lvCancelScriptTextMap = new HashMap<String, String>();
//		lvCancelScriptTextMap.put(JSONTagName.PARAMETERS_DATA_JSON, createOrderActionDataJson(pOrderEnquiryMapData));
//		lvCancelScriptTextMap.put(JSONTagName.DWID, pOrderEnquiryMapData.get("Dwid"));		
//		String lvCancelOrderScript = TextUtil.composeMessage(FrontEndConfiguration.cancelOrderScript(),lvCancelScriptTextMap);
//		if("N".equals(lvOrderType)){
//			String lvIsCDMaster = pOrderEnquiryMapData.get(TXNTagName.IS_CD_MASTER);
//			if("P".equals(lvIsCDMaster) &&  lvCancelOrderAble ){
//				lvRowOrderCancelActionTextMap.put(JSONTagName.CANCEL_ORDER_ACTION, lvCancelOrderScript);
//			}
//			if("C".equals(lvIsCDMaster)){
//				String lvPrimaryOrderStatus = pOrderEnquiryMapData.get(ActionTagName.OCO_PRIMARY_ORDER_STATUS);
//				*//**
//				 * Avoid null pointer exception
//				 * @author canyong.lin
//				 * @since 2014-06-20
//				 *//*
//				if (lvPrimaryOrderStatus==null) {
//					lvPrimaryOrderStatus = "";
//				}
//				if(lvCancelOrderAble &&  !ITradeServlet.svCompanyConfig.orderStatusForAllowedToCancel().contains(lvPrimaryOrderStatus)){
//					lvRowOrderCancelActionTextMap.put(JSONTagName.CANCEL_ORDER_ACTION, lvCancelOrderScript);
//				}
//			}
//		}else{
//			if(lvCancelOrderAble){
//				lvRowOrderCancelActionTextMap.put(JSONTagName.CANCEL_ORDER_ACTION, lvCancelOrderScript);
//			}
//		}		
//		return TextUtil.composeMessage(FrontEndConfiguration.cancelOrderDivScript(),lvRowOrderCancelActionTextMap);
//	}
////END TASK #:TTL-GZ-clover_he-00104.2 20140430 [ITrade5] get cancel order JavaScript.[ITRADEFIVE-152]
//
////BEGIN TASK #:TTL-GZ-clover_he-00104.1 20140424 [ITrade5] get order details JavaScript.[ITRADEFIVE-152]
//	private static Map<String, String> getOrderDetailAction(HashMap<String, String> pOrderDetailMapData)
//	{//junming - Order details json params
//		ITradeCompanyConfig lvCompanyConfig = ITradeServlet.svCompanyConfig;
//		SimpleDateFormat	dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		PatternEntry entry = BaseAppDiversityConfiguration.getInstance().getPatternEntry(EPatternType.TIME);
//		DateFormatter formatDate = new DateFormatter(entry.getPattern());
//		Date lvModifiedTime = null;
//		try {
//			lvModifiedTime = dateFormatter.parse((String) pOrderDetailMapData.get(TXNTagName.MODIFIED_TIME));
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		String lvOrderGroupID = pOrderDetailMapData.get(TXNTagName.ORDER_GROUP_ID);
//		String lvOrderType = pOrderDetailMapData.get(TXNTagName.ORDER_TYPE);
//		Map<String, String> lvRowOrderDetailActionPramTextMap = new HashMap<String, String>();
//		lvRowOrderDetailActionPramTextMap.put(JSONTagName.ORDER_GROUP_ID, lvOrderGroupID);
//		
//		Map<String, String> lvActionTemplateData = new HashMap<String, String>();
//		lvActionTemplateData.put(JSONTagName.IS_HISTORY, "N");
//		lvActionTemplateData.put(JSONTagName.ORDER_GROUP_ID, lvOrderGroupID);
//		lvActionTemplateData.put(JSONTagName.QUANTITY, pOrderDetailMapData.get(TXNTagName.QTY));
//		lvActionTemplateData.put(JSONTagName.PRICE, pOrderDetailMapData.get(TXNTagName.PRICE));		
//		lvActionTemplateData.put(JSONTagName.ORDER_TYPE, lvOrderType);
//		lvActionTemplateData.put(JSONTagName.STOP_PRICE, pOrderDetailMapData.get(TXNTagName.STOP_PRICE));
//		lvActionTemplateData.put(JSONTagName.CURRENCY_ID, pOrderDetailMapData.get(TXNTagName.CURRENCY_ID));
//		lvActionTemplateData.put(JSONTagName.ORDER_ID, pOrderDetailMapData.get(TXNTagName.ORDER_ID));
//		lvActionTemplateData.put(JSONTagName.QUANTITY, pOrderDetailMapData.get(TXNTagName.QTY));
////		lvActionTemplateData.put(JSONTagName.FILLED_QUANTITY, pOrderDetailMapData.get(TXNTagName.FILLED_QTY));
////		lvActionTemplateData.put(JSONTagName.OS_QTY, pOrderDetailMapData.get(TXNTagName.OS_QTY));
//		lvActionTemplateData.put(JSONTagName.CANCEL_QUANTITY, pOrderDetailMapData.get(TXNTagName.CANCEL_QTY));
//		lvActionTemplateData.put(JSONTagName.GOOD_TILL_DATE, pOrderDetailMapData.get(TXNTagName.GOOD_TILL_DATE));
//		lvActionTemplateData.put(JSONTagName.BS, pOrderDetailMapData.get(TXNTagName.BS));
//		boolean lvDisplayRejectReasoninRemark = Boolean.getBoolean(pOrderDetailMapData.get("RejectReasonAndRemark"));
//		String lvRemark = pOrderDetailMapData.get(TXNTagName.REMARK);				
//		if(lvDisplayRejectReasoninRemark)
//        {
//			if(!Utils.isNullStr(lvRemark)){
//				lvRemark = lvRemark + ", " + pOrderDetailMapData.get(TXNTagName.REJECT_REASON).trim();
//			}else{
//				lvRemark = pOrderDetailMapData.get(TXNTagName.REJECT_REASON).trim();
//			}
//        }else{
//        	lvRemark = pOrderDetailMapData.get(TXNTagName.STATUS_INTERNAL);
//        }
//        if (Utils.isNullStr(lvRemark)) lvRemark = "";
//		lvActionTemplateData.put(JSONTagName.REMARK, lvRemark);
//		//pHKSOrderEnqDetails.getModifiedTime()+" "+pHKSOrderEnqDetails.getActivationDate();
//		String lvModifiedTime = TextFormatter.getFormattedTime(pOrderDetailMapData.get(TXNTagName.MODIFIED_TIME), "HH:mm:ss")+pOrderDetailMapData.get(TXNTagName.ACTIVATION_DATE);
//		lvActionTemplateData.put(JSONTagName.LAST_MODIFIED_DATE, lvModifiedTime);	
//		lvActionTemplateData.put(JSONTagName.MODIFIED_TIME, TextFormatter.getFormattedTime(pOrderDetailMapData.get(TXNTagName.MODIFIED_TIME), "HH:mm:ss"));	
////		lvActionTemplateData.put(JSONTagName.MODIFIED_TIME, lvCompanyConfig.formatTime(lvModifiedTime, ITradeSessionManager.currentUser().getLocale()));
//		lvActionTemplateData.put(JSONTagName.MODIFIED_TIME, formatDate.print(lvModifiedTime));
//		lvActionTemplateData.put(JSONTagName.ACTIVATION_DATE, pOrderDetailMapData.get(TXNTagName.ACTIVATION_DATE));	
//		lvActionTemplateData.put(JSONTagName.CHANNEL_ID, pOrderDetailMapData.get(TXNTagName.CHANNEL_ID));
//		
//		if("N".equals(lvOrderType)){
//			String lvIsCDMaster = pOrderDetailMapData.get(TXNTagName.IS_CD_MASTER);
//			if("P".equals(lvIsCDMaster)){				
//				String lvPrimaryOrderStatus = pOrderDetailMapData.get(TXNTagName.STATUS_INTERNAL);
//				String lvSecondOrderStatus = pOrderDetailMapData.get(ActionTagName.OCO_SECONDARY_ORDER_STATUS);
//				if("CAN".equals(lvPrimaryOrderStatus) || "REJ".equals(lvPrimaryOrderStatus) || "EXP".equals(lvPrimaryOrderStatus)){
//					if(!"CAN".equals(lvSecondOrderStatus) && !"REJ".equals(lvSecondOrderStatus) && !"EXP".equals(lvSecondOrderStatus)){
//						lvActionTemplateData.put(JSONTagName.OCO_PRICE, pOrderDetailMapData.get(ActionTagName.LOWEST_LIMIT_PRICE));
//						lvActionTemplateData.put(JSONTagName.AVERAGE_PRICE, pOrderDetailMapData.get(ActionTagName.OCO_SECONDARY_AVGERAGE_PRICE));
//					}else{
//						lvActionTemplateData.put(JSONTagName.OCO_PRICE, pOrderDetailMapData.get(TXNTagName.PRICE));
//						lvActionTemplateData.put(JSONTagName.AVERAGE_PRICE, pOrderDetailMapData.get(TXNTagName.AVG_PRICE));
//					}
//				}else{
//					lvActionTemplateData.put(JSONTagName.OCO_PRICE, pOrderDetailMapData.get(TXNTagName.PRICE));
//					lvActionTemplateData.put(JSONTagName.AVERAGE_PRICE, pOrderDetailMapData.get(TXNTagName.AVG_PRICE));
//				}
//				
//				lvActionTemplateData.put(JSONTagName.PRICE, pOrderDetailMapData.get(TXNTagName.PRICE));
//				lvActionTemplateData.put(JSONTagName.LOWEST_LIMIT_PRICE, pOrderDetailMapData.get(ActionTagName.LOWEST_LIMIT_PRICE));
//				lvActionTemplateData.put(JSONTagName.STOP_PRICE, pOrderDetailMapData.get(ActionTagName.STOP_PRICE));
//			}
//		}else{
//			lvActionTemplateData.put(JSONTagName.AVERAGE_PRICE, pOrderDetailMapData.get(TXNTagName.AVG_PRICE));
//		}
//		
//		lvRowOrderDetailActionPramTextMap.put(JSONTagName.ORDER_DETAIL_PARAMS_JSON, JSONParse.writeJSON(lvActionTemplateData));
////		lvRowOrderDetailActionPramTextMap.put(JSONTagName.DWID, pOrderDetailMapData.get("Dwid"));
////		lvRowOrderDetailActionPramTextMap.put(JSONTagName.DWIDTH, pOrderDetailMapData.get("Dwidth"));
//		return lvRowOrderDetailActionPramTextMap;
//	}
////END TASK #:TTL-GZ-clover_he-00104.1 20140424 [ITrade5] get order details JavaScript.[ITRADEFIVE-152]
//	
////BEGIN TASK #:TTL-GZ-clover_he-00104 2014-04-02 [ITrade5] Make sure the primary order in front of the second order when OrderType="N".[ITRADEFIVE-152]
//	private static void modifyOrderSequenceOCO(HKSOrderEnqDetails[] pOrderEnqDetails)
//	{
//		int lvLength = pOrderEnqDetails.length;		
//		String lvOrderType = "";
//		String lvIsCdMaster = "";
//	//	HKSOrderEnqDetails lvPreviousOrderEnqDetails = null;
//		HKSOrderEnqDetails lvCurrentOrderEnqDetails = null;
//		HKSOrderEnqDetails lvNextOrderEnqDetails = null;
//		for (int i = lvLength - 1; i >= 0; i--){
//			lvCurrentOrderEnqDetails = pOrderEnqDetails[i];
//			lvOrderType = lvCurrentOrderEnqDetails.getOrderType();
//			if("N".equals(lvOrderType)){
//				lvIsCdMaster = lvCurrentOrderEnqDetails.getIsCDMaster();
//				if("C".equals(lvIsCdMaster) && i>0){
//					lvNextOrderEnqDetails = pOrderEnqDetails[i-1];
//					if(i<lvLength-1){
//						lvPreviousOrderEnqDetails = pOrderEnqDetails[i+1];
//					}
//					if(!StringUtils.isNullStr(lvPreviousOrderEnqDetails) && lvCurrentOrderEnqDetails.getOrderGroupID().trim().equals(lvPreviousOrderEnqDetails.getOrderGroupID().trim())){
//						
//					}
//					if(!StringUtils.isNullStr(lvNextOrderEnqDetails) && lvCurrentOrderEnqDetails.getOrderGroupID().trim().equals(lvNextOrderEnqDetails.getOrderGroupID().trim())){
//						pOrderEnqDetails[i] = lvNextOrderEnqDetails;
//						pOrderEnqDetails[i-1] = lvCurrentOrderEnqDetails;
//						i-=1;
//					}
//				}
//			}
//		}
//	}
////END TASK #:TTL-GZ-clover_he-00104 2014-04-02 [ITrade5] Make sure the primary order in front of the second order when OrderType="N".[ITRADEFIVE-152]
//	
//
//}
//*/