/*package com.itrade.web.helper;

import static HKSSesstionKey.CURRENT_COMPANY;
import static HKSSesstionKey.ITRADE_USER_SESSION_KEY;
import static HKSSesstionKey.LANGUAGE;
import static HKSSesstionKey.LAYOUT_CURRENT;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import DashboardLayoutModel;
import com.itrade.base.data.DataPadding;
import com.itrade.base.jesapi.ITradeAPI;
import IUser;
import ITradeUser;
import com.itrade.configuration.diversity.BaseAppDiversityConfiguration;
import com.itrade.ct.support.ITradeLang;
import Log;
//import com.itrade.web.engine.ITradeServlet;
import com.systekit.winvest.hks.util.Utils;
*//**
 * 
 * @author jay.wince
 * @category:(etc.)
 * @since 2013-11-08
 *//*
public class ITradeSessionManager {

	enum Type{
		String,Object,Boolean,Integer,Long,Bigdecimal		
	}
	
	public static final String getCurrentTheme(){
		return "";// getITradeUser().getCompanyWrapper().getCompanyTheme().getCurrentTheme();
	}

	 *//**
     * 
     * Case 1:request-response chain plus Authenticated.
     * @see ITradeMultipleCompany#companyThemeOffer
     * @param request
     * @return Company code.
     * @author jay.wince
     *//*
	@Deprecated
	public static final String getCurrentCompany(){
		final HttpServletRequest request = ITradeAPI.httpUtilities().getCurrentRequest();
		return getCurrentCompany(request);
	}
    *//**
     * Use {@link ITradeSessionManager#getCurrentCompany()} instead.
     * <br>
     * Case 1:request-response chain plus Authenticated.
     * @see ITradeMultipleCompany#companyThemeOffer
     * @param request
     * @return Company code.
     * @author jay.wince
     * @Deprecated
     *//*
	public static final String getCurrentCompany(final HttpServletRequest request){
		//return getSessionAttributeValue(request, CURRENT_COMPANY,Type.String).toString();
//BEGIN TASK #:TTL-GZ-Jay-00132 2014-02-08[ITrade5]Setter/Getter support for company code in ITradeUser.
//BETIN TASK #:TTL-GZ-jay.wince-00241 20151116[ITradeR5]One web application ,one company business logic.(ITRADEFIVE-686)
		String companyCode;
		if (currentUser() == null) {
			companyCode = BaseAppDiversityConfiguration.getInstance().final_CompanySettings().get("default-company");
		}else {
			companyCode = getITradeUser().getCompanyCode();
		}
		return companyCode;

	    return "";// ITradeAPI.iTradeConfiguration().getDefaultCompany();
//END TASK #:TTL-GZ-jay.wince-00241 20151116[ITradeR5]One web application ,one company business logic.(ITRADEFIVE-686)	    
//END   TASK #:TTL-GZ-Jay-00132 2014-02-08[ITrade5]Setter/Getter support for company code in ITradeUser.
	}
	@Deprecated
	public static final DashboardLayoutModel getCurrentLayout(){
		  return getSessionAttributeValue(ITradeAPI.httpUtilities().getCurrentRequest(), LAYOUT_CURRENT, Type.Object);
	}
	
	*//**
	 * Please use {@link ITradeSessionManager#getCurrentLayout()} instead in case under request-response chain.
	 * @see #currentUser()
	 * @param request
	 * @return
	 * @Deprecated
	 *//*	
	public static final DashboardLayoutModel getCurrentLayout(final HttpServletRequest request){
		  return getSessionAttributeValue(request, LAYOUT_CURRENT, Type.Object);
	}
	public static final ITradeUser getITradeUser(){
		if(currentUser() instanceof ITradeUser){
			return (ITradeUser)currentUser();
		};
		return null;
	}
	public static final IUser currentUser(){
		  return getSessionAttributeValue(ITRADE_USER_SESSION_KEY);
	}
	*//**
	 * Please use {@link ITradeSessionManager#currentUser()} instead in case under request-response chain.
	 * @see #currentUser()
	 * @param request
	 * @return
	 * @Deprecated
	 *//*	
	public static final IUser currentUser(final HttpServletRequest request){
		  return getSessionAttributeValue(request, ITRADE_USER_SESSION_KEY, null);
	}
	
	//BEGIN TASK #:TTL-GZ-clover_he-00069 2013-12-11 [ITrade5] Use ITradeSessionManager get lang code ,not use LangUtil
	*//**
	 * @category
	 * Please use {@link ITradeSessionManager#getCurrentLang()} instead in case under request-response chain.
	 * @author jay.wince
	 * @see  com.itrade.ct.support.ITradeMultipleCompany#getLangCodeFallback()
	 * @Deprecated
	 *//*	
	public static final Integer getCurrentLang(final HttpServletRequest request){
//BEGIN TASK #:TTL-GZ-Jay-00132 2014-02-07[ITrade5]Language issue:Embed the language issue into IUser implementation .		
		return 1;//ITradeServlet.svCompanyConfig.fromLocalToLangCode(currentUser().getLocale()); 
//END   TASK #:TTL-GZ-Jay-00132 2014-02-07[ITrade5]Language issue:Embed the language issue into IUser implementation .		
	}
	
	public static final Integer getCurrentLang(){
		return getCurrentLang(ITradeAPI.httpUtilities().getCurrentRequest());
	}
	
//BEGIN TASK #:TTL-GZ-Jay-00132 2014-02-07[ITrade5]Language issue:Embed the language issue into IUser implementation .	
	*//**
	 * @category
	 * Please use {@link ITradeSessionManager#getCurrentLang()} instead in case under request-response chain.
	 * @author jay.wince
	 * @see  com.itrade.ct.support.ITradeMultipleCompany#getLangCodeFallback()
	 * @Deprecated
	 *//*
	public static final String getCurrentLangText(final HttpServletRequest request){
		return ITradeLang.fromLocalToLangText(currentUser(request).getLocale());
	}
//END   TASK #:TTL-GZ-Jay-00132 2014-02-07[ITrade5]Language issue:Embed the language issue into IUser implementation .
	
	public static final String getCurrentLangText(){
		return ITradeLang.fromLocalToLangText(currentUser().getLocale());
	}
	
	
	//END TASK #:TTL-GZ-clover_he-00069 2013-12-11 [ITrade5] Use ITradeSessionManager get lang code ,not use LangUtil
	
	//BEGIN TASK #:TTL-GZ-clover_he-00057 2013-11-22 [ITrade5] User session mechanisms achieve entering only one order for CA module
	public static final DataPadding getITradeContractSessionValue(final HttpServletRequest request, final String key ){
		return getSessionAttributeValue(request, key ,Type.Object);
	}
	
	*//**
	 * @category
	 * Please use {@link ITradeSessionManager#removeValue(String)} instead in case under request-response chain.
	 * @author 
	 * @Deprecated
	 *//*
	public static final void removeValue(final HttpSession session,final String key){
		//Jay:2013-12-25:Secure Enhancement.
		synchronized (session.getId().intern()) {
			if(session != null && !Utils.isNullStr(key)){
				session.removeAttribute(key);
			}else{
				Log.println("Session has been invalid.Remove Key:"+key+" to session unsuccessfully", Log.ERROR_LOG);
			}
		}
	}
	//END TASK #:TTL-GZ-clover_he-00057 2013-11-22 [ITrade5] User session mechanisms achieve entering only one order for CA module

//BEGIN TASK #:TTL-GZ-clover_he-00090 2014-02-11 [ITrade5] reloaded removeValue and storeValue methods for session.[ITRADEFIVE-85]
	public static final void removeValue(final String key){
		HttpSession lvSession = ITradeAPI.httpUtilities().getCurrentRequest().getSession(false);
		removeValue(lvSession, key);
	}
	
	public static final void storeValue(final String key,final Object value){		
		storeValue(ITradeAPI.httpUtilities().getCurrentRequest(), key, value);
	}
//END TASK #:TTL-GZ-clover_he-00090 2014-02-11 [ITrade5] reloaded removeValue and storeValue methods for session.[ITRADEFIVE-85]
	
//BEGIN TASK #:TTL-GZ-Jay-00117 [ITrade5]a handsome way to get session from request.
	*//**
	 * @category
	 * Please use {@link ITradeSessionManager#storeValue(String, Object)} instead in case under request-response chain.
	 * @author 
	 * @Deprecated
	 *//*
	public static final void storeValue(final HttpServletRequest request,final String key,final Object value){
		HttpSession session = request.getSession(false);
		if (session!=null) {
			if(value.equals(getSessionAttributeValue(request, key, null))){//Check whether the new value is the same as old value ?
			     return;	
			}
			try {				
				//1.Call back when language changed.
				if (LANGUAGE.equalsIgnoreCase(key)) {//notify...									
					//currentUser(request).setLocale(ITradeServlet.svCompanyConfig.toLocal(value.toString()));
					return;//It's not required to set the language to session.
				}else if(CURRENT_COMPANY.equals(key)){
					//getITradeUser().setCompanyCode(value.toString());
					return;//It's not required to set the company code to session;
				}
				session.setAttribute(key, value);
			} catch (Exception e) {
				Log.println("Session has been invalid.Set Key:"+key+" to session unsuccessfully", Log.ERROR_LOG);			
			}
		}		
	}
//END TASK #:TTL-GZ-Jay-00117 [ITrade5]a handsome way to get session from request.
	public static final <T> T getSessionAttributeValue(final String key){
		return getSessionAttributeValue(ITradeAPI.httpUtilities().getCurrentRequest(), key, null);
	}
	protected static final <T> T getSessionAttributeValue(HttpServletRequest request,final String key,Type t){
		HttpSession session = request.getSession(false);
		return getSessionAttributeValue(session, key, t);
	}
	protected static final <T> T getSessionAttributeValue(HttpSession session,final String key,Type t){
		Object object = null;
		if (session!=null) {
			try {
				object = session.getAttribute(key);		
			} catch (Exception e) {
				Log.println("Session has been invalid.Get Key:"+key+" from session unsuccessfully", Log.ERROR_LOG);				
			}
		}
		if (t==null)t = Type.Object;
		if(object!=null){
			switch (t) {
			case String:
				object = object.toString();				
				break;
			case Boolean:
				object = Boolean.TRUE.equals(object.toString());
				break;
			case Integer:
				object = Integer.parseInt(object.toString());					
				break;
			case Long:
				object = Long.parseLong(object.toString());
				break;
			default:					
				break;
			}
		}else{
			switch (t) {
			case String:
				object = "";
				break;
			case Boolean:
				object = false;
				break;
			case Integer:
				object = -1;
				break;
			case Long:				
				object = 0L;
				break;
			default:
				object = (T)object;
				break;
			}
		}
		return (T)object;
	}
}*/