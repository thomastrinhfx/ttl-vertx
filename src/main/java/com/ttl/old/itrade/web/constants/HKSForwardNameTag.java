package com.ttl.old.itrade.web.constants;
/**
 * @description used for forward name configured in itrade-config-x.xml
 * @author jay.wince
 * @since 2013-08-23
 */
public interface HKSForwardNameTag {
	/*
	 * Common. 
	 */
	public final static String MAIN_PAGE = "main";
	/*
	 * DashboardWidgetAction
	 */
	public final static String WIDGET_ACTION_2GSAW = "2gsaw";
	
	//BEGIN TASK#: TTL-GZ-Jay-00052 2013-08-27[Itrade5]add widget & edit layout for dashboard.
	/**
	 * DashboardLayoutAction
	 */
	public static final String LAYOUT_ACTION_2_EDIT_LAYOUT = "2elpg";
	//END   TASK#: TTL-GZ-Jay-00052 2013-08-27[Itrade5]add widget & edit layout for dashboard.

	//BEGIN TASK #:TTL-GZ-Jay-00083 2013-10-15[ITtrade5]Theme swiching based on packtag.
	public static final String THEME_ACTION_2_THEME_BROKER = "2themebroker";
	//END TASK #:TTL-GZ-Jay-00083 2013-10-15[ITtrade5]Theme swiching based on packtag.
	
	//aastock favorite
	public static final String AASTOCK_FAVORITES="AAStockFavorite";
	
	// Technical Analysis
	public static final String TA_SHOW = "taShow";
//	public static final String PORTFOLIO_SUMMARY = "PortfolioSummary";
	
//junming.peng on 20140819 : To setting page key	
	public static final String SETTINGS_PAGE = "settings";
//BEGIN TASK #:CANYONG.LIN-00029.2 20150226[ITradeR5]Deciding whether to login.(ITRADEFIVE-489)
	public static final String LOGIN_CHOOSE = "loginChoose";
//END   TASK #:CANYONG.LIN-00029.2 20150226[ITradeR5]Deciding whether to login.(ITRADEFIVE-489)
//BEGIN TASK #:TTL-GZ-CANYONG.LIN-00035 20150319[ITradeR5]ForwardNname improvement.(ITRADEFIVE-518)
	public static final String REPLY_PAGE = "reply";
	public static final String CONFIRM_PAGE = "confirm";
	//table data eg:news
	public static final String LIST_PAGE = "list";
	public static final String DETAIL_PAGE = "view";
	public static final String RESULT_PAGE = "result";
	public static final String MODIFY_PAGE = "modify";
//END   TASK #:TTL-GZ-CANYONG.LIN-00035 20150319[ITradeR5]ForwardNname improvement.(ITRADEFIVE-518)
//BEGIN TASK #:TTL-GZ-BO.LI-00105 20150325[ITradeR5]ForwardNname improvement.(ITRADEFIVE-525)
	public static final String LIST_PAGE_2 = "list2"; 
	public static final String LIST_PAGE_3 = "list3";
	public static final String CHART_PAGE = "chart";
	public static final String AACHART_PAGE = "aachart";
	public static final String CHART_PAGE_2 = "chart2";
	public static final String PLAN_PAGE = "plan";
	public static final String APPLY_PAGE = "apply";
	public static final String TERMS_PAGE = "terms";
	public static final String PREVIEW_PAGE = "preview";
	public static final String SUCCESS_PAGE = "success";
	public static final String REJECT_PAGE = "reject";
	public static final String RECORD_PAGE = "record";
//END TASK #:TTL-GZ-BO.LI-00105 20150325[ITradeR5]ForwardNname improvement.(ITRADEFIVE-525)
//BEGIN TASK #:TTL-GZ-PENGJM-00354 20150327[ITradeR5]Rename the forward name(ITRADEFIVE-526)
	public static final String CANCEL_PAGE = "cancel";
//END TASK #:TTL-GZ-PENGJM-00354 20150327[ITradeR5]Rename the forward name(ITRADEFIVE-526)
//BEGIN TASK #:TTL-GZ-jummyjw.liu-00005 20150817[ITradeR5]detect(ITRADEFIVE-453)
	public static final String DETECT = "detect";
//END TASK #:TTL-GZ-jummyjw.liu-00005 20150817[ITradeR5]detect(ITRADEFIVE-453)
	public static final String SEARCH_HISTORY_PAGE = "elsh";
	public static final String OPERATIONS_PAGE = "elop";
	public static final String FEE_DETAILS_PAGE = "feedetails";
}
