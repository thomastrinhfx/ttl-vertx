package com.ttl.old.itrade.web.constants;
/**
 * @since  2013-08-30
 * @author jay.wince
 * @description : request parameter key used frequently.
 */
public final class HKSRequestParameterKey {
	
	public static final String ACTION 	 = "action";
	public static final String COLUMN 	 = "column";
	public static final String LANG 	 = "lang";
	public static final String LANG_TEXT = "langText";
	public static final String LAYOUT 	 = "layout";
	
	public final static String DASHBOARD_WIDTH  = "Dwidth";
	public final static String FORWARD_KEY 		= "fto";
	//BEGIN TASK #:TTL-GZ-PENGJM-00113 2013-09-06[Itrade5]IE 8 issue and change password bug fixed
	public final static String NEEDCHANGEPASSWORD 	= "needChangePassword";
	public final static String FIRSTCP 				= "firstCp";
	public final static String CPSUCCESS 			= "cpsuccess";
	//END TASK #:TTL-GZ-PENGJM-00113 2013-09-06[Itrade5]IE 8 issue and change password bug fixed
	//BEGIN TASK #:TTL-GZ-PENGJM-00119 2013-09-22[Itrade5]first login load header page and source standard update 
	public final static String FIRST_LOGIN_LOAD_CHANGEPASSWORD_HEADER = "flcp";
	public final static String FIRST_LOGIN_LOAD_DICLAIMER_HEADER 	  = "flgd";
	//END TASK #:TTL-GZ-PENGJM-00119 2013-09-22[Itrade5]first login load header page and source standard update
	//BEGIN TASK #:TTL-GZ-PENGJM-00120 2013-09-24[Itrade5]Merge disclaimer action into HKSDisclaimerAction(ITRADEFIVE-20)
	public final static String DICLAIMERPARAMETER = "dcmmtd";
	//END TASK #:TTL-GZ-PENGJM-00120 2013-09-24[Itrade5]Merge disclaimer action into HKSDisclaimerAction(ITRADEFIVE-20)
	
// BEGIN TASK #:TTL-GZ-Jay-00095 2013-11-11[ITrade5]CORE implementation for COMMON VERSION based on COMPANY level.
	public final static String Company_Code 	= "cpycode";
// BEGIN TASK #:TTL-GZ-Jay-00095 2013-11-11[ITrade5]CORE implementation for COMMON VERSION based on COMPANY level.
	public final static String Company_Theme 	= "cpytheme";
	
	//BEGIN TASK #:TTL-GZ-clover_he-00057 2013-11-22 [ITrade5] User session mechanisms achieve entering only one order for CA module
	public final static String ESCAPE_POST_CHECK 	= "EscapePostCheck";
	//END TASK #:TTL-GZ-clover_he-00057 2013-11-22 [ITrade5] User session mechanisms achieve entering only one order for CA module
	
// BEGIN TASK #:TTL-GZ-Jay-00126.2 2013-12-26[ITrade5]Provide a convinient interface to check whether it's required to restore settings.
	public final static String SERIALIZE_DATA 		= "sd";
// END   TASK #:TTL-GZ-Jay-00126.2 2013-12-26[ITrade5]Provide a convinient interface to check whether it's required to restore settings.
	
	public final static String ATTR_CLIENT_ID 		= "clientID";
	
//BEGIN TASK #:TTL-GZ-clover_he-00086.2 2014-02-24 [ITrade5] Action Layer：modify parameterKey for transfer data from PortfolioSummary.jsp to genenterorder.jsp when click [Buy]/[Sell]/[Due Order]. [ITRADEFIVE-109]
	public final static String INSTRUMENT_ID 	= "P_InstrumentID";
	public final static String QUANTITY 		= "P_quantity";
	public final static String BUY_SELL 		= "P_BuySell";
	public final static String MARKET_ID 		= "P_MarketID";
//END TASK #:TTL-GZ-clover_he-00086.2 2014-02-24 [ITrade5] Action Layer：modify parameterKey for transfer data from PortfolioSummary.jsp to genenterorder.jsp when click [Buy]/[Sell]/[Due Order]. [ITRADEFIVE-109]
	
//BEGIN TASK #:TTL-GZ-clover_he-00094 2014-02-26 [ITrade5] Add parameter key for transfer data from order enquiry to modify order.[ITRADEFIVE-129]
	public static final String CHANNEL_ID 			= "P_ChannelID";
	public final static String ORDER_GROUP_ID		= "P_OrderGroupID";
	public final static String ORDER_ID				= "P_OrderID";
	public final static String ORDER_TYPE		    = "P_OrderType";
	public final static String OS_QUANTITY 			= "P_OS_Quantity";
	public final static String PRICE 				= "P_price";
	public final static String CANCEL_QUANTITY 		= "P_CancelQuantity";
	public final static String GROSS_AMOUNT 		= "P_GrossAmount";
	public final static String AVERAGE_PRICE  		= "P_AveragePrice";
	public final static String LOT_SIZE				= "P_LotSize";
	public final static String FILLED_QUANTITY 		= "P_FilledQuantity";
	public final static String STOP_TYPE 			= "P_StopType";
	public final static String STOP_PRICE 			= "P_StopPrice";
	public final static String NET_AMOUNT 			= "P_NetAmount";
	public final static String SCRIP 				= "P_SCRIP";
	public final static String ALL_OR_NOTHING 		= "P_AllOrNothing";
	public final static String ACTIVATION_DATE 		= "P_ActivationDate";
	public final static String QTY_DECIMAL_PLACES   = "P_QtyDecimalPlaces";
//END TASK #:TTL-GZ-clover_he-00094 2014-02-26 [ITrade5] Add parameter key for transfer data from order enquiry to modify order.[ITRADEFIVE-129]
	
	public final static String ERROR_CODE = "P_ERROR_CODE";
	public final static String ERROR_DESC = "P_ERROR_DESC";
	public final static String P_REQUEST_TYPE = "P_PRTK";

//BEGIN TASK #:TTL-GZ-clover_he-00098 2014-03-05 [ITrade5] Add parameter key for transfer data from order enquiry to cancel order.[ITRADEFIVE-133]
	public final static String GOOD_TILL_DATE = "P_GoodTillDate";
	public final static String LOWEST_LIMIT_PRICE = "P_LowestLimitPrice";
	public final static String IS_CD_MASTER = "P_IsCdMaster";	
//END TASK #:TTL-GZ-clover_he-00098 2014-03-05 [ITrade5] Add parameter key for transfer data from order enquiry to cancel order.[ITRADEFIVE-133]
    /*Table REF*/
	public final static String CurrentPage = "CurrentPg";
	
//BEGIN TASK #:TTL-GZ-clover_he-00104.1 20140424 [ITrade5] Add parameter key for transfer data from order enquiry to order detail.[ITRADEFIVE-152]
	public final static String CURRENCY_ID 		= "P_CurrencyID";
	public final static String MODIFIED_TIME 	= "P_ModifiedTime";
	public final static String REMARK 			= "P_Remark";
	public final static String IS_HISTORY		= "P_IsHistory";
	public final static String OCO_PRICE 		= "P_OCOPrice";
//END TASK #:TTL-GZ-clover_he-00104.1 20140424 [ITrade5] Add parameter key for transfer data from order enquiry to order detail.[ITRADEFIVE-152]

//BEGIN TASK #:TTL-GZ-Jay-00173.4 20140603[ITradeR5]Version 1.1:implementation for dashboard/widget addition.[ITRADEFIVE-159]	
	public final static String DERIVED_WIDGET_ID = "dwid";
//END TASK #:TTL-GZ-Jay-00173.4 20140603[ITradeR5]Version 1.1:implementation for dashboard/widget addition.[ITRADEFIVE-159]
	public final static String PARA_DASHBOARD_ID = "did";
	public final static String PARA_FORM_PARAMS_ALIKE = "params";
//BEGIN TASK #:TTL-GZ-PENGJM-00250 20140721[ITradeR5]Quote carousel restore key(ITRADEFIVE-206)	
	public final static String CAROUSEL_INDEX = "index";
//END TASK #:TTL-GZ-PENGJM-00250 20140721[ITradeR5]Quote carousel restore key(ITRADEFIVE-206)	
//BEGIN TASK #: TTL-GZ-BO.LI-00040 20140912[ITradeR5]Use button group(Segment) for News&Top20 and similar design(ITRADEFIVE-258)
	public final static String PAPA_FORM_BUTTON = "button";
//END TASK #: TTL-GZ-BO.LI-00040 20140912[ITradeR5]Use button group(Segment) for News&Top20 and similar design(ITRADEFIVE-258)
//BEGIN TASK #:TTL-GZ-CANYONG.LIN-00014 20150121[ITradeR5]Associated with the token.(ITRADEFIVE-436)
	public final static String PAPA_FUNCTION_ID = "func";
	public final static String PAPA_FORM_TOKEN_VALUE = "ftkn";
	public final static String PAPA_CSRF_TOKEN_VALUE = "ctkn";
//EDN   TASK #:TTL-GZ-CANYONG.LIN-00014 20150121[ITradeR5]Associated with the token.(ITRADEFIVE-436)
//BEGIN TASK #:TTL-GZ-BO.LI-00069.5 20141211[ITradeR5]Add FundEnquiry action column(ITRADEFIVE-358)
	public final static String INSTRUCTIONID = "P_InstructionID";
	public final static String CURRENCYID = "P_CurrencyID";
	public final static String ACCOUNTNO = "P_AccountNO";
	public final static String SETTLEAMOUT = "P_SettleAmout";
	public final static String SETTLEMETHOD = "P_SettleMethod";
	public final static String CASHREF = "P_CashRef";
	public final static String CHEQUEBANK = "P_ChequeBank";
	public final static String CHEQUENUMBER = "P_ChequeNumber";
	public final static String TRANREF = "P_TranRef";
	public final static String REDIRECTACTION = "P_Redirectaction";
	public final static String TRANSERTYPE = "P_TranserType";
	public final static String FROM = "P_From";
    public final static String DATE = "P_Date";
    public final static String REFERENCE = "P_Reference";
//BEGIN TASK#:TTL-GZ-BO.LI-00069.5 20141211[ITradeR5]Add FundEnquiry action column(ITRADEFIVE-358)
//BEGIN TASK#:TTL-GZ-CANYONG.LIN-00028 20150209[ITradeR5]Redirect(ITRADEFIVE-486)
	public final static String REDIRECT = "redirect";
//END   TASK#:TTL-GZ-CANYONG.LIN-00028 20150209[ITradeR5]Redirect(ITRADEFIVE-486)
//BEGIN TASK#:TTL-GZ-CANYONG.LIN-00029.2 20150226[ITradeR5]Continue login.(ITRADEFIVE-489)
	public final static String CONTINUE_LOGIN = "continueLogin";
	public final static String LOGIN_CHOOSE = "loginChoose";
//END   TASK#:TTL-GZ-CANYONG.LIN-00029.2 20150226[ITradeR5]Continue login.(ITRADEFIVE-489)
//BEGIN TASK#:TTL-GZ-CANYONG.LIN-00033 20150316[ITradeR5]Checking the main page unload.(ITRADEFIVE-514)
	public final static String PAPA_UNLOAD = "unload";
//END   TASK#:TTL-GZ-CANYONG.LIN-00033 20150316[ITrader5]Checking the main page unload.(ITRADEFIVE-514)
//BEGIN TASK#:TTL-GZ-CANYONG.LIN-00033 20150316[ITradeR5]Whether already loaded the loginChoose.jsp.(ITRADEFIVE-514)
	public final static String PAPA_LOGIN_CHOOSE_PAGE_CLOSE = "loginChooseClose";
//END   TASK#:TTL-GZ-CANYONG.LIN-00033 20150316[ITradeR5]Whether already loaded the loginChoose.jsp.(ITRADEFIVE-514)
//BEGIN TASK#:TTL-GZ-CANYONG.LIN-00033 20150319[ITradeR5]session id.(ITRADEFIVE-514)
	public final static String PAPA_SESSION_ID = "psid";
//END   TASK#:TTL-GZ-CANYONG.LIN-00033 20150319[ITradeR5]session id.(ITRADEFIVE-514)
//BEGIN TASK#:TTL-GZ-CANYONG.LIN-00038 20150325[ITradeR5]Block id.(ITRADEFIVE-511)
	public final static String PAPA_BLOCK_ID = "blockID";
//END   TASK#:TTL-GZ-CANYONG.LIN-00038 20150325[ITradeR5]Block id.(ITRADEFIVE-511)
//BEGIN TASK#:TTL-GZ-CANYONG.LIN-00038 20150325[ITradeR5]Resource load checking.(ITRADEFIVE-511)
	public final static String PAPA_LOAD = "load";
//END   TASK#:TTL-GZ-CANYONG.LIN-00038 20150325[ITradeR5]Resource load checking.(ITRADEFIVE-511)
//BEGIN TASK#:TTL-GZ-CANYONG.LIN-00038 20150327[ITradeR5]Block theme switch.(ITRADEFIVE-511)
	public final static String PAPA_BLOCK_THEME_SWITCH = "themeSwitch";
//END   TASK#:TTL-GZ-CANYONG.LIN-00038 20150327[ITradeR5]Block theme switch.(ITRADEFIVE-511)
	public final static String PAPA_BLOCK_LOADED = "loaded";
}
