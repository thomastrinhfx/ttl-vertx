/*package com.itrade.web.helper;

import java.util.HashMap;

import HKSInstrument;
import TPManager;
import com.itrade.util.StringUtils;
import com.systekit.winvest.hks.util.Utils;
*//**
 * 
 * @author clover_he
 * @description 
 * @since 2014-01-21
 *//*
public class InstrumentManager {
	private static final String V_DEF_STOCK_NAME = " - ";
	
//BEGIN TASK #:TTL-GZ-Jay-00131.2 2014-01-26[ITrade5]Market settings&Instrument utility.[ITradeFIVE-78]	
	public static String getInstrumentName(String pMarketID,final String pStockCode,String pLangText){		  
    	  String lvInstrumentName = V_DEF_STOCK_NAME;
//BEGIN TASK #:TTL-GZ-clover_he-00090.1 2014-02-13 [ITrade5] get stock name according default market if marketID is null or "".[ITRADEFIVE-85]
  		if(StringUtils.isNullStr(pMarketID)){
  			pMarketID = ITradeMultipleCompany.defaultMarket().marketID();
  		}
//END TASK #:TTL-GZ-clover_he-00090.1 2014-02-13 [ITrade5] get stock name according default market if marketID is null or "".[ITRADEFIVE-85]
		  HKSInstrument lvHKSInstrument = instrument(pMarketID,pStockCode);
//BEGIN TASK #:TTL-GZ-XYW-00087 2014-02-13[ITrade5]The current market Not get Instrument[ITradeFIVE-85]	
		  if(lvHKSInstrument==null){
			  return null;
		  }
//END TASK #:TTL-GZ-XYW-00087 2014-02-13[ITrade5]The current market Not get Instrument[ITradeFIVE-85]
          if ("en_US".equals(pLangText)) {
        	  lvInstrumentName = lvHKSInstrument.getShortName();
        	  //If no English short name,it will fall back to Chinese short name.
        	  if("&#45".equals(lvInstrumentName) || "".equals(lvInstrumentName)){
					lvInstrumentName = lvHKSInstrument.getChineseShortName();
			  }
		  }else if ("zh_TW".equals(pLangText)) {
			  lvInstrumentName = lvHKSInstrument.getChineseShortName();
			  //If no Chinese short name,it will fall back to English short name.
			  if("&#45".equals(lvInstrumentName) || "".equals(lvInstrumentName)){
					lvInstrumentName = lvHKSInstrument.getShortName();
			  }
		  }else if ("zh_CN".equals(pLangText)) {
			  lvInstrumentName = lvHKSInstrument.getChineseShortName();
			  //If no Chinese short name,it will fall back to English short name.
			  if("&#45".equals(lvInstrumentName) || "".equals(lvInstrumentName)){
					lvInstrumentName = lvHKSInstrument.getShortName();
			  }
		  }
		  return lvInstrumentName;
	}

//BEGIN TASK #:TTL-GZ-clover_he-00086.5 20140425 [ITrade5] get all intrument name for three lang.[ITRADEFIVE-109]	
	public static HashMap<String,String> getInstrumentNameForThreeLang(String pMarketID,final String pStockCode)
	{
		HashMap<String,String> lvInstrumentNamesMap = new HashMap<String,String>();
		HKSInstrument lvHKSInstrument = instrument(pMarketID,pStockCode);
		if(lvHKSInstrument==null){
			return null;
		}
		String lvUsInstrumentName = lvHKSInstrument.getShortName();
		String lvTwInstrumentName = lvHKSInstrument.getChineseShortName();
		String lvCnInstrumentName =  lvHKSInstrument.getChineseShortName();
		
		en_US
		if ("&#45".equals(lvUsInstrumentName) || StringUtils.isNullStr(lvUsInstrumentName)){
			lvUsInstrumentName = lvTwInstrumentName;
		}
		zh_TW
		if ("&#45".equals(lvTwInstrumentName) || StringUtils.isNullStr(lvTwInstrumentName)){
			if(!"&#45".equals(lvCnInstrumentName) && !StringUtils.isNullStr(lvCnInstrumentName)){
				lvTwInstrumentName = lvCnInstrumentName;
			}else{
				lvTwInstrumentName = lvUsInstrumentName;
			}
		}
		zh_CN
		if ("&#45".equals(lvCnInstrumentName) || StringUtils.isNullStr(lvCnInstrumentName)){
			if(!"&#45".equals(lvTwInstrumentName) && !StringUtils.isNullStr(lvTwInstrumentName)){
				lvCnInstrumentName = lvTwInstrumentName;
			}else{
				lvCnInstrumentName = lvUsInstrumentName;
			}
		}
		lvInstrumentNamesMap.put("UName", lvUsInstrumentName);
		lvInstrumentNamesMap.put("TName", lvTwInstrumentName);
		lvInstrumentNamesMap.put("CName", lvCnInstrumentName);
		return lvInstrumentNamesMap;
	}
//END TASK #:TTL-GZ-clover_he-00086.5 20140425 [ITrade5] get all intrument name for three lang.[ITRADEFIVE-109]
	
	public static HKSInstrument instrument(final String pMarketID, String pStockCode){
//BEGIN TASK #:TTL-GZ-clover_he-00090.1 2014-02-13 [ITrade5] get instrument according default market if marketID is null or "".[ITRADEFIVE-85]
		String lvMarketID;
		if(StringUtils.isNullStr(pMarketID)){
			lvMarketID = ITradeMultipleCompany.defaultMarket().marketID();
		}else{
			lvMarketID = pMarketID;
		}
		EMarket market = ITradeMarket.offer(lvMarketID);
//END TASK #:TTL-GZ-clover_he-00090.1 2014-02-13 [ITrade5] get instrument according default market if marketID is null or "".[ITRADEFIVE-85]
		HKSInstrument lvHKSInstrument = (HKSInstrument)TPManager.mvInstrumentVector.get(pMarketID.concat("|").concat(pStockCode.toUpperCase()));
		return lvHKSInstrument;
	}

	
//END TASK #:TTL-GZ-Jay-00131.2 2014-01-26[ITrade5]Market settings&Instrument utility.[ITradeFIVE-78]	
	
	public static HKSInstrument getInstrument(String pMarketID, String pStockCode) {
		HKSInstrument lvHKSInstrument = new HKSInstrument();
		if (!Utils.isNullStr(pMarketID)) {
			// Log.println("[ DefaultAction MarketID : " + pMarketID + "]",
			// Log.DEBUG_LOG);
			if (svUsingStockCodeFormatMarket != null
					&& svUsingStockCodeFormatMarket
							.contains((String) pMarketID)) {
				lvHKSInstrument = (HKSInstrument) TPManager.mvInstrumentVector
						.get(pMarketID.concat("|").concat(
								Utils.getFormattedInstrumentID(pStockCode)));
			} else {
				lvHKSInstrument = (HKSInstrument) TPManager.mvInstrumentVector
						.get(pMarketID.concat("|").concat(
								pStockCode.toUpperCase()));
			//}
		}
		return lvHKSInstrument;
	}
	
	public static String getInstrumentName(HKSInstrument pHKSInstrument)
	{
//BEGIN TASK #:TTL-GZ-Wic.feng-00001.1 20141218[ITradeR5] get InstrumentName
		return getInstrumentName(pHKSInstrument, ITradeSessionManager.getCurrentLang());
//END 	TASK# TLL-GZ-Wic.feng-00001.1 20141218[ITradeR5] get InstrumentName
	}
	
	public static String getInstrumentName(HKSInstrument pHKSInstrument , int pLangCode)
	{
//BEGIN TASK #:TTL-GZ-PENGJM-00181 2013-12-09[ITrade5]when there is no chinese instrument name will show english name
		String lvInstrumentName = V_DEF_STOCK_NAME;
		if (pHKSInstrument != null){
			if (pLangCode == 1 || pLangCode == 2){
				lvInstrumentName = pHKSInstrument.getChineseShortName();
				if("&#45".equals(lvInstrumentName) || "".equals(lvInstrumentName)){
					lvInstrumentName = pHKSInstrument.getShortName();
				}
			}else{
				lvInstrumentName = pHKSInstrument.getShortName();
				if("&#45".equals(lvInstrumentName) || "".equals(lvInstrumentName)){
					lvInstrumentName = pHKSInstrument.getChineseShortName();
				}
			}
		}
//END TASK #:TTL-GZ-PENGJM-00181 2013-12-09[ITrade5]when there is no chinese instrument name will show english name
		return lvInstrumentName;
	}

}
*/