package com.ttl.old.itrade.tp;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import java.util.Vector;

import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLParser;
import com.systekit.common.msg.MsgManager;
import com.systekit.winvest.hks.util.Utils;

/**
 * The class defined method that Packaging the request content and send request.
 * @author
 *
 */
abstract public class TPBaseRequest
{
	protected TPManager				ivTPManager;
	protected IMsgXMLNode			ivTemplateNode;
	protected Vector				ivMandatoryField = new Vector();

	static protected Object			svLockObject = new Object();
	static protected long			svLastLinkID = 0L;
	static protected DecimalFormat  svLinkIDFormatter = new DecimalFormat("0000000000");

	//Begin Task #RC00181 - Rice Cheng 20081229
	public static String svLanguage_EN = "en_US";
	public static String svLanguage_TW = "zh_TW";
	public static String svLanguage_CN = "zh_CN";
	public Set mvLanguageSet = new HashSet(); 
	//End Task #RC00181 - Rice Cheng 20081229
	
	/**
	 * Default constructor for TPBaseRequest class.
	 */
	protected TPBaseRequest(TPManager pTPManager, IMsgXMLNode pTemplateNode)
	{
		ivTPManager = pTPManager;
		ivTemplateNode = pTemplateNode;
		
		//Begin Task #RC00181 - Rice Cheng 20090108
		mvLanguageSet.add(svLanguage_EN);
		mvLanguageSet.add(svLanguage_TW);
		mvLanguageSet.add(svLanguage_CN);
		//End Task #RC00181 - Rice Cheng 20090108
	}

	/**
	 * This method get the next link ID.
	 * @return the string for link id.
	 */
	protected static String getNextLinkID()
	{
		long	lvNextID;
		synchronized (svLockObject)
		{
			svLastLinkID++;
			lvNextID = svLastLinkID;
			return svLinkIDFormatter.format(lvNextID);
		}
	}

	/**
	 * This method set the xml header as required by TP
	 * @param pNode
	 */
	protected void setHeader(IMsgXMLNode pNode)
	{
		SimpleDateFormat	lvDateFormatter = new SimpleDateFormat("yyyyMMddhhmmss");

		pNode.setAttribute("resvr", getNextLinkID());
		pNode.setAttribute("issueTime", lvDateFormatter.format(new Date()));
	}

	/**
	 * This method get the request XML node by apply the parameter from the map
	 * @param pParameterMap the map with contains specific data.
	 * @return the request XML node.
	 */
	protected IMsgXMLNode getRequestNode()
	{
		return (IMsgXMLNode) ivTemplateNode.clone();
	}

	/**
	 * This method merge the pParameterMap with the XML node
	 * @param pParameterMap the map with contains specific data.
	 * @param pNode the return node for IMsgXMLNode.
	 * @return the vector containing the missing mandatory field, ie, if vector size zero, the merge was successful
	 */
	protected Vector mergeParameter(Hashtable pParameterMap, IMsgXMLNode lvRequestNode)
	{
		Vector  lvMissingField = new Vector();

		String test = lvRequestNode.toString();

		int		lvTotalChild = lvRequestNode.getChildTotal();
		for (int i = 0; i < lvTotalChild; i++)
		{
			String  lvTagName = lvRequestNode.getChildNode(i).getName();
			if (lvTagName.equalsIgnoreCase("#PCDATA"))
			{
				continue;
			}
			String  lvNewValue = null;
			if (pParameterMap != null)
			{
				lvNewValue = (String) pParameterMap.get(lvTagName);
			}

			if (lvNewValue != null)
			{
				lvRequestNode.getChildNode(i).setValue(lvNewValue);
			}
			else
			{

				// parameter doesn't contain the field, check for mandatory
				if (ivMandatoryField.contains(lvTagName))
				{
					lvMissingField.addElement(lvTagName);
				}
			}
		}
		return lvMissingField;
	}

	/**
	 * This method invoke other method to send the data.
	 * @param pParameterMap the map with contains specific data.
	 * @return the result from TP.
	 */
	//Begin Task #RC00181 - Rice Cheng 20081229
	public IMsgXMLNode send(Hashtable pParameterMap)
	{
		return send(pParameterMap, "");
	}
	//End Task #RC00181 - Rice Cheng 20081229
	/**
	 * This method make a request XML from template and fill the request field from the parameter map
	 * then send the resulting XML to TP
	 * If the request contains loop and it can't be mapped directly from the hashtable
	 * the derived class should overrid this method to do some more complex mapping
	 * @param pParameterMap  the map with contains specific data.
	 * @param pLanguage the using language.
	 * @return the result from TP.
	 */
	public IMsgXMLNode send(Hashtable pParameterMap, String pLanguage)
	{
		IMsgXMLNode lvRequestNode = getRequestNode();
		Vector		lvMissingField = mergeParameter(pParameterMap, lvRequestNode);
		if (lvMissingField.size() > 0)
		{
			return makeErrorXML(new Exception("Missing mandatory fields: " + lvMissingField.toString()));
		}
		else
		{
			//Begin Task #RC00181 - Rice Cheng 20081229
			if(!Utils.isNullStr(pLanguage)){
				String lvLanguage = svLanguage_EN;
				if(mvLanguageSet.contains(pLanguage))
				{
					lvLanguage = pLanguage;
				}
				String [] lvLanguageSet = lvLanguage.split("_");
				lvRequestNode.setAttribute("language", lvLanguageSet[0]);
				lvRequestNode.setAttribute("country", lvLanguageSet[1]);
				//End Task #RC00181 - Rice Cheng 20081224
				}
			return send(lvRequestNode);
		}
	}

	/**
	 * This method send the node to TP. It can only be called by the derived class
	 * @param pNode the object of IMsgXMLNode.
	 * @return the result from TP.
	 */
	protected IMsgXMLNode send(IMsgXMLNode pNode)
	{
		try
		{
			setHeader(pNode);
			String  lvLinkID = pNode.getAttribute("resvr");

			String  lvXML = pNode.toString();
			Log.println("TP XML Request : " + lvXML, Log.TRANSACTION_LOG);
			IMsgXMLNode lvRetNode = ivTPManager.send(lvLinkID, lvXML);
			if (lvRetNode == null)
			{
				throw new Exception("TP not responding");
			}
			else
			{
				Log.println("TP XML Response : " + lvRetNode.toString(), Log.TRANSACTION_LOG);
				return lvRetNode;
			}
		}
		catch (Exception e)
		{
			return makeErrorXML(e);
		}
	}

	/**
	 * This method returns Error from system level.
	 * @param e a exception.
	 * @return A instance for IMsgXMLNode.
	 */
	protected IMsgXMLNode makeErrorXML(Exception e)
	{
		try
		{
			IMsgXMLParser   lvParser = MsgManager.createParser();
			IMsgXMLNode		lvErrorode = lvParser.createXMLNode("ERROR");
			lvErrorode.addChildNode("C_ERROR_CODE").setValue("TPBASEREQERROR");
			lvErrorode.addChildNode("DESC").setValue(e.getMessage());
			return lvErrorode;
		}
		catch (Exception e1)
		{
			return null;
		}
	}

	/**
	 * Error returns from TP
	 * @param pErrorNode
	 * @return
	 */

	/*
	 * private IMsgXMLNode makeErrorXML(IMsgXMLNode pErrorNode)
	 * {
	 * try
	 * {
	 * IMsgXMLParser lvParser = MsgManager.createParser();
	 * IMsgXMLNode lvErrorode = lvParser.createXMLNode("ERROR");
	 *
	 * // todo change to our definatio of error node
	 * return pErrorNode;
	 * }
	 * catch (Exception e)
	 * {
	 * return null;
	 * }
	 * }
	 */
	//BEGIN TASK: TTL-GZ-JJ-00001 JiangJi 20091206 [iTradeR5 VN] FO:Pop up stock info	
	/**
	 * This method Building a QRYPOPUP XML Header.
	 * @param pIssueTime the issue time.
	 * @param pIssueloc the issue log.
	 * @param pIssueMetd the issue method.
	 * @param pOprId the operation ID.
	 * @param pPwd the password.
	 * @param pResvr the re server.
	 * @param pLanguage the language.
	 * @param pCountry the country.
	 * @param pEntityId the entity ID.
	 * @return lvIMsgXMLNode A instance for IMsgXMLNode.
	 */
	public IMsgXMLNode buildQRYPOPUPXMLHeader(String pIssueTime, String pIssueloc, String pIssueMetd, String pOprId, String pPwd, String pResvr, String pLanguage, String pCountry, String pEntityId){
		IMsgXMLNode lvIMsgXMLNode = null;
		IMsgXMLParser lvParser = MsgManager.createParser();
		lvIMsgXMLNode = lvParser.createXMLNode("QRYPOPUP");
		
		lvIMsgXMLNode.setAttribute("msgId", "QRYPOPUP");
		lvIMsgXMLNode.setAttribute("issueTime", pIssueTime);
		lvIMsgXMLNode.setAttribute("issueLoc", pIssueloc);
		lvIMsgXMLNode.setAttribute("issueMetd", pIssueMetd);
		lvIMsgXMLNode.setAttribute("oprId", pOprId);
		lvIMsgXMLNode.setAttribute("pwd", pPwd);
		lvIMsgXMLNode.setAttribute("resvr", pResvr);
		lvIMsgXMLNode.setAttribute("language", pLanguage);
		lvIMsgXMLNode.setAttribute("country", pCountry);
		lvIMsgXMLNode.setAttribute("entityId", pEntityId);
		
		return lvIMsgXMLNode;
	}
	/**
	 * This method to Building a QRYPOPUP XML Header.
	 * @return lvIMsgXMLNode The instance for lvIMsgXMLNode. 
	 */
	public IMsgXMLNode buildQRYPOPUPXMLHeader(){
		IMsgXMLNode lvIMsgXMLNode = null;
		IMsgXMLParser lvParser = MsgManager.createParser();
		lvIMsgXMLNode = lvParser.createXMLNode("QRYPOPUP");
		
		lvIMsgXMLNode.setAttribute("msgId", "QRYPOPUP");
		lvIMsgXMLNode.setAttribute("issueTime", "");
		lvIMsgXMLNode.setAttribute("issueLoc", "");
		lvIMsgXMLNode.setAttribute("issueMetd", "");
		//BEGIN TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
		lvIMsgXMLNode.setAttribute("oprId", IMain.getProperty("AgentID"));
		lvIMsgXMLNode.setAttribute("pwd", IMain.getProperty("AgentPassword"));
		lvIMsgXMLNode.setAttribute("resvr", "0000000000000000000000002");
		lvIMsgXMLNode.setAttribute("language", "");
		lvIMsgXMLNode.setAttribute("entityId", IMain.getProperty("EntityID"));
		//END TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
		
		return lvIMsgXMLNode;
	}
	/**
	 * This method to send the node to TP.
	 * @param pNode the instance for IMsgXMLNode.
	 * @return the result from TP.
	 */
	public IMsgXMLNode sendRequestXML(IMsgXMLNode pNode)
	{
		try {
			setHeader(pNode);
			String  lvLinkID = pNode.getAttribute("resvr");
			String  lvXML = pNode.toString();
			Log.println("TP XML Request : " + lvXML, Log.TRANSACTION_LOG);
			IMsgXMLNode lvRetNode = ivTPManager.send(lvLinkID, lvXML);
			if (lvRetNode == null) {
				Log.println("TP XML Response : TP not responding", Log.ERROR_LOG);
				return makeErrorMessageXML(new Exception("Unexpected error has occurred, operation cancelled"));
			}
			else {
				Log.println("TP XML Response : " + lvRetNode.toString(), Log.TRANSACTION_LOG);
				return lvRetNode;
			}
		} catch (Exception e) {
			return makeErrorMessageXML(e);
		}
	}
	/**
	 * This method to Building a Error Message XML
	 * @param e A exception.
	 * @return lvErrorode A error instance for IMsgXMLNode.
	 */
	protected IMsgXMLNode makeErrorMessageXML(Exception e)
	{
		try
		{
			IMsgXMLParser   lvParser = MsgManager.createParser();
			IMsgXMLNode		lvErrorode = lvParser.createXMLNode("ERROR");
			lvErrorode.addChildNode("C_ERROR_CODE").setValue("UNEXPECTEDERROR");
			lvErrorode.addChildNode("DESC").setValue(e.getMessage());
			return lvErrorode;
		}
		catch (Exception e1)
		{
			return null;
		}
	}
	//END TASK: TTL-GZ-JJ-00001 JiangJi 20091206 [iTradeR5 VN] FO:Pop up stock info
}
