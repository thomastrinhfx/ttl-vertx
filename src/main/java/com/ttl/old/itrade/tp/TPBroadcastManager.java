/*package com.itrade.tp;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;


import ITradeServlet;
import OrderEnquiryDataCachingManager;
import HKSTag;
import HKSMarketIndexBean;
import com.itrade.hks.txn.HKSDownloadInstrumentTxn;
import com.itrade.hks.txn.HKSDownloadTradeDateTxn;
import FieldSplitter;
import StockPriceInfoParser;
import StringSplitter;
import TextFormatter;
import IBroadcastManager;
import IMain;
import Log;
import SessionTracker;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.common.obp.ObpThreadPool;
import com.systekit.common.obp.ObpWorkerThread;
import com.systekit.winvest.hks.config.mapping.TagName;
import com.systekit.winvest.hks.config.mapping.TopicName;
import com.systekit.winvest.hks.util.Utils;

*//**
 * Title: TP Broadcast Manager Description: The TPBroadcastManager class defined
 * methods to broadcast the dynamic update from TP
 * 
 * @author not attributable
 *//*
public class TPBroadcastManager implements IBroadcastManager {
	private ObpThreadPool ivNotificationThreadPool = null;
	private static List<String> ivPendingInsturmentID = new ArrayList<String>();

	private boolean useFoBroadcastMarketData = false;
	*//**
	 * Constructor for TPBroadcastManager class
	 *//*
	public TPBroadcastManager() {
		ivNotificationThreadPool = new ObpThreadPool("Notification Pool", 10,
				1000);
		try {
			useFoBroadcastMarketData = new Boolean(IMain.getProperty("use.fo.broadcast.market.data"));
		} catch (Exception e) {
			useFoBroadcastMarketData = false;
		}
	}

	*//**
	 * This method gets the updated IMsgXMLNode objects and sends them to gateways by the method sendMesasgeToGateway
	 * @param IMsgXMLNode pNode : An IMsgXMLNode object gets from TP.
	 *//*
	public void onBroadcastMessageReceived(IMsgXMLNode pNode) {
		// Start By YuLong On 20090710
		if (pNode.getName().equalsIgnoreCase("TOPIC_HKSFO_EMESSAGE")) {
			IMsgXMLNode lvFieldsNode = pNode.getChildNode("LoopRows")
					.getChildNode("FNs");
			IMsgXMLNodeList lvNodeListValue = pNode.getChildNode("LoopRows")
					.getNodeList("Vs");
			for (int i = 0; i < lvNodeListValue.size(); i++) {
				IMsgXMLNode lvValuesNode = lvNodeListValue.getNode(i);
				Hashtable lvDataHash = FieldSplitter.tokenizeOrderEnquiryNodes(
						lvFieldsNode, lvValuesNode);

				String lvClientID = lvDataHash.get("CLIENTID") == null ? ""
						: (String) lvDataHash.get("CLIENTID");
				String lvMessageID = lvDataHash.get("MESSAGEID") == null ? ""
						: (String) lvDataHash.get("MESSAGEID");
				String lvMessage = lvDataHash.get("MESSAGE") == null ? ""
						: (String) lvDataHash.get("MESSAGE");
				String lvPlaceDate = lvDataHash.get("PLACEDATE") == null ? ""
						: (String) lvDataHash.get("PLACEDATE");
				Set lvAppletGatewaySet = AppletGateway
						.getAppletGatewaySetByClientID(lvClientID);
				String lvMessageListString = "TOPIC_HKSFO_EMESSAGE|"
						+ lvClientID + "|" + lvMessageID + "|" + lvMessage
						+ "|" + lvPlaceDate;
				sendMesasgeToGateway(lvAppletGatewaySet, lvMessageListString);
			}
		}
		// End By YuLong On 20090710
		else if (pNode.getName().equalsIgnoreCase(
				TopicName.TOPIC_HKSFO_PRICE_ALERT)) {
			IMsgXMLNode lvFieldsNode = pNode.getChildNode("FNs");
			IMsgXMLNodeList lvNodeListValue = pNode.getChildNode("LoopRows")
					.getNodeList("Vs");
			for (int i = 0; i < lvNodeListValue.size(); i++) {
				IMsgXMLNode lvValuesNode = lvNodeListValue.getNode(i);
				Hashtable lvDataHash = FieldSplitter.tokenizeNodes(
						lvFieldsNode, lvValuesNode);
				String lvClientID = (String) lvDataHash.get(TagName.CLIENTID);
				String lvInstrumentID = (String) lvDataHash
						.get(TagName.INSTRUMENTID);
				String lvSign = (String) lvDataHash.get("SIGN");
				String lvAlertPrice = (String) lvDataHash.get("ALERTPRICE");
				double lvRandomNumber1 = java.lang.Math.random();
				double lvRandomNumber2 = java.lang.Math.random();
				double lvRandomNumber = lvRandomNumber1 * lvRandomNumber2;
				String lvRandomString = String.valueOf(lvRandomNumber);
				Set lvAppletGatewaySet = AppletGateway
						.getAppletGatewaySetByClientID(lvClientID);
				String lvMessageListString = "TOPIC_HKSFO_PRICE_ALERT|"
						+ lvClientID + "|" + lvInstrumentID + "|" + lvSign
						+ "|" + lvAlertPrice + "|"
						+ new Timestamp(System.currentTimeMillis())
						+ lvRandomString;
				sendMesasgeToGateway(lvAppletGatewaySet, lvMessageListString);
				// sendMesasgeToGateway(lvAppletGatewaySet,
				// TopicName.TOPIC_HKSFO_PRICE_ALERT + "|" + lvClientID + "|" +
				// lvInstrumentID + "|" + lvSign + "|" + lvAlertPrice + "\n" +
				// "|" + new Timestamp(System.currentTimeMillis()) +
				// lvRandomString);
			}
		} else if (pNode.getName().equalsIgnoreCase(
				TopicName.TOPIC_HKSFO_SECURITIES_ENQUIRY_OBCD)) {
			IMsgXMLNode lvFieldsNode = pNode.getChildNode("FNs");
			IMsgXMLNodeList lvNodeListValue = pNode.getChildNode("LoopRows")
					.getNodeList("Vs");
			for (int i = 0; i < lvNodeListValue.size(); i++) {
				IMsgXMLNode lvValuesNode = lvNodeListValue.getNode(i);
				Hashtable lvDataHash = FieldSplitter.tokenizeNodes(
						lvFieldsNode, lvValuesNode);
				// Begin Task #:- TTL-GZ-ZZW-00005 Wind Zhao 200909028
				String lvMarketID = lvDataHash.get("MARKETID") == null ? ""
						: (String) lvDataHash.get("MARKETID");
				String lvInstrumentID = lvDataHash.get("INSTRUMENTID") == null ? ""
						: (String) lvDataHash.get("INSTRUMENTID");
				String lvNominalPrice = lvDataHash.get("NOMINAL") == null ? ""
						: (String) lvDataHash.get("NOMINAL");
				String lvReferencePrice = lvDataHash.get("REFERENCE_PRICE") == null ? ""
						: (String) lvDataHash.get("REFERENCE_PRICE");
				String lvHighPrice = lvDataHash.get("HIGHPRICE") == null ? ""
						: (String) lvDataHash.get("HIGHPRICE");
				String lvLowPrice = lvDataHash.get("LOWPRICE") == null ? ""
						: (String) lvDataHash.get("LOWPRICE");
				String lvOpeningPrice = lvDataHash.get("OPEN_PRICE") == null ? ""
						: (String) lvDataHash.get("OPEN_PRICE");
				String lvClosePrice = lvDataHash.get("CLOSE_PRICE") == null ? ""
						: (String) lvDataHash.get("CLOSE_PRICE");
				String lvCeilingPrice = lvDataHash.get("CEILING_PRICE") == null ? ""
						: (String) lvDataHash.get("CEILING_PRICE");
				String lvFloorPrice = lvDataHash.get("FLOOR_PRICE") == null ? ""
						: (String) lvDataHash.get("FLOOR_PRICE");
				String lvVolume = lvDataHash.get("TRADING_VOLUME") == null ? ""
						: (String) lvDataHash.get("TRADING_VOLUME");
				String lvBidQueue = lvDataHash.get("BIDQUEUE") == null ? ""
						: (String) lvDataHash.get("BIDQUEUE");
				String lvAskQueue = lvDataHash.get("ASKQUEUE") == null ? ""
						: (String) lvDataHash.get("ASKQUEUE");
				// VanTran add more field
				String lvmatchPrice = lvDataHash.get("MATCH_PRICE") == null ? ""
						: (String) lvDataHash.get("MATCH_PRICE");
				String lvMatchQty = lvDataHash.get("MATCH_QTY") == null ? ""
						: (String) lvDataHash.get("MATCH_QTY");
				
				String lvTotalTradingQty = lvDataHash.get("TOTAL_TRADING_QTTY") == null ? ""
						: (String) lvDataHash.get("TOTAL_TRADING_QTTY");
				String lvTotalTradingValue = lvDataHash.get("TOTAL_TRADING_VALUE") == null ? ""
						: (String) lvDataHash.get("TOTAL_TRADING_VALUE");
				String lvBuyForeignQty = lvDataHash.get("BUY_FOREIGN_QTTY") == null ? ""
						: (String) lvDataHash.get("BUY_FOREIGN_QTTY");
				String lvSellForeignQty = lvDataHash.get("SELL_FOREIGN_QTTY") == null ? ""
						: (String) lvDataHash.get("SELL_FOREIGN_QTTY");
				String lvCurrentRoom = lvDataHash.get("CURRENT_ROOM") == null ? ""
						: (String) lvDataHash.get("CURRENT_ROOM");
				
				boolean isUsingTTL = "true".equalsIgnoreCase(IMain.getProperty("isUsingTTLService"));
				boolean isTotalTradeQtyChanged =true;
				// Check which channel of broadcast message will be used
				// If is TTL channel then need to recalculate the total qty
				if(isUsingTTL){
				// If  HOSE stock then processing  total trading qty
				// and total trading value
				if(lvMarketID.trim().equalsIgnoreCase("HO")){
					try{
					Log.println("Start log processing HOSE total trading qty & value for stock :"+lvInstrumentID, Log.DEBUG_LOG);
					// processing to get the  most recently stock info
					SortedMap<Long,String> lvStockHisMap= IMain.stockHistMap.get(lvInstrumentID);
					if(lvStockHisMap!=null){
					// the most recently item will be in the last item in Sorted Map
					String lvLastItemData = lvStockHisMap.get(lvStockHisMap.lastKey());
					String[] lvStockInfoArray = StringSplitter.split(lvLastItemData, "\\|");
					
					String lvLastMarketID= lvStockInfoArray[0];
					
					// calculate the newTradingValue
					BigDecimal newTradingValue; 
					
					if (lvMatchQty.trim().isEmpty()
							|| lvMatchQty.trim().equals("-")
							|| lvmatchPrice.trim().isEmpty()
							|| lvmatchPrice.trim().equals("-")) {
						newTradingValue = new BigDecimal(0);
					} else {
						// calculate the Trading value
						// need to convert lot size and convert match price
						newTradingValue = BigDecimal.valueOf(
								Double.parseDouble(lvmatchPrice)*1000).multiply(
								BigDecimal.valueOf(Double
										.parseDouble(StockPriceInfoParser.convertVolWithLotSize(lvLastMarketID,lvMatchQty,false))));
					}
					// Total Matched Qty
					BigDecimal lvLastTotalMatchQty = new BigDecimal(0);
					if (!lvStockInfoArray[15].trim().equals("-") && !lvStockInfoArray[15].trim().equals("")){            	
							lvLastTotalMatchQty = new BigDecimal(Double.parseDouble(lvStockInfoArray[15]));
					}
					
					// lvTotalTradingValue
					BigDecimal lvLastTotalTradingValue = new BigDecimal(0);
					if (!lvStockInfoArray[16].trim().equals("-") && !lvStockInfoArray[16].trim().equals("")){            	
						lvLastTotalTradingValue = new BigDecimal(Double.parseDouble(lvStockInfoArray[16]));
					}

					//Log.println("lvMatchQty:"+lvMatchQty, Log.DEBUG_LOG);
					//Log.println("lvLastTotalMatchQty:"+lvLastTotalMatchQty, Log.DEBUG_LOG);
					if(lvMatchQty.isEmpty() || lvMatchQty.trim().equals("-")){
						lvTotalTradingQty = lvLastTotalMatchQty.toString();
					}else{
						lvTotalTradingQty = BigDecimal.valueOf(Double.parseDouble(lvMatchQty)).add(lvLastTotalMatchQty).toString();
					}
					//Log.println("lvTotalTradingQty:"+lvTotalTradingQty, Log.DEBUG_LOG);

					//Log.println("newTradingValue:"+newTradingValue, Log.DEBUG_LOG);
					//Log.println("lvLastTotalTradingValue:"+lvLastTotalTradingValue, Log.DEBUG_LOG);
					lvTotalTradingValue = newTradingValue.add(lvLastTotalTradingValue).toString();
					//Log.println("lvTotalTradingValue:"+lvTotalTradingValue, Log.DEBUG_LOG);
					//Log.println("End log processing HOSE total trading qty & value", Log.DEBUG_LOG);

					
					}
					}catch (Exception e) {
						Log.println(e, Log.ERROR_LOG);
					}
				}else {
					// For HA && OTC check if total trading value in not difference then do not add to historical map
					SortedMap<Long,String> lvStockHisMap= IMain.stockHistMap.get(lvInstrumentID);
					if(lvStockHisMap!=null){
						// the most recently item will be in the last item in Sorted Map
						String lvLastItemData = lvStockHisMap.get(lvStockHisMap.lastKey());
						String[] lvStockInfoArray = StringSplitter.split(lvLastItemData, "\\|");
						
						// Total Matched Qty
						BigDecimal lvLastTotalMatchQty = new BigDecimal(0);
						if (!lvStockInfoArray[15].trim().equals("-") && !lvStockInfoArray[15].trim().equals("")){            	
								lvLastTotalMatchQty = new BigDecimal(Double.parseDouble(lvStockInfoArray[15]));
						}
						
						if(lvTotalTradingQty.isEmpty() || lvLastTotalMatchQty.compareTo(new BigDecimal(lvTotalTradingQty))==0){
							isTotalTradeQtyChanged =false;
						}
					}
				}
				}
				SimpleDateFormat df = new SimpleDateFormat("hh:mm:ss"); 
				Date lstDate = new Date();
								
				// VanTran end add
				String lvMessageListString = lvMarketID + "|" + lvInstrumentID
						+ "|" + lvNominalPrice + "|" + lvReferencePrice + "|"
						+ lvHighPrice + "|" + lvLowPrice + "|" + lvOpeningPrice
						+ "|" + lvClosePrice + "|" + lvCeilingPrice + "|"
						+ lvFloorPrice + "|" + lvVolume + "|" + lvBidQueue
						+ "|" + lvAskQueue + "|" + lvmatchPrice + "|" + lvMatchQty
						+ "|" + lvTotalTradingQty + "|" + lvTotalTradingValue 
						+ "|" + lvBuyForeignQty  +"|" + lvCurrentRoom + "|"+lvSellForeignQty 
						+ "|" + df.format(lstDate);
				// End Task #:- TTL-GZ-ZZW-00005 Wind Zhao 20090928
				
				//VanTran When receive OBCD message then update clear historical data to false
				// in order to let stock watch to get the info using current day info (ceiling,floor,bid,ask,...)
				IMain.mvIsRunClearPreHistoricalData =false;
				
				if (isUsingTTL) {
					if (lvMatchQty.trim().isEmpty()
							|| lvMatchQty.trim().equals("-")
							|| Utils.parseBigDecimal(lvMatchQty.trim(), false)
									.doubleValue() == 0
							|| lvmatchPrice.trim().isEmpty()
							|| Utils
									.parseBigDecimal(lvmatchPrice.trim(), false)
									.doubleValue() == 0
							|| lvmatchPrice.trim().equals("-")
							|| !isTotalTradeQtyChanged) {
						sendMarketDataToPool(lvMessageListString,
								lvInstrumentID, false);
					} else {
						sendMarketDataToPool(lvMessageListString,
								lvInstrumentID, true);
					}
				} else {
					if (lvMarketID.trim().equalsIgnoreCase(
							HKSTag.HNX_EXCHANGE_CODE)) {
						//Calendar currentDate = Calendar.getInstance();
						// Stop get HNX OBCD message after 11h 30
						if (IMain.mvMarketStatus != null
								&& IMain.mvMarketStatus
										.get(HKSTag.HNX_EXCHANGE_CODE) != null
								&& !IMain.mvMarketStatus
										.get(HKSTag.HNX_EXCHANGE_CODE).equalsIgnoreCase("13")) {
							sendMarketDataToPool(lvMessageListString,
									lvInstrumentID, false);
						}else{
							Log.print("HNX Market status : "+IMain.mvMarketStatus ==null ? "market status map is null":
									IMain.mvMarketStatus.get(HKSTag.HNX_EXCHANGE_CODE), Log.DEBUG_LOG);
						}
						
					} else {
						sendMarketDataToPool(lvMessageListString,
								lvInstrumentID, false);
					}
				}

			}
		} else if (pNode.getName().equalsIgnoreCase(
				TopicName.TOPIC_HKSFO_ORDER_ENQUIRY)) {
			IMsgXMLNode lvFieldsNode = pNode.getChildNode("LoopRows")
					.getChildNode("FNs");
			IMsgXMLNodeList lvNodeListValue = pNode.getChildNode("LoopRows")
					.getNodeList("Vs");
			
			for(int i = 0; i < lvNodeListValue.size(); i++) {
				IMsgXMLNode lvValuesNode = lvNodeListValue.getNode(i);
				// Edited by Bowen Chau on 29 Mar 2006
				// Handle old format message for order enquiry
				Hashtable lvDataHash = FieldSplitter.tokenizeOrderEnquiryNodes(
						lvFieldsNode, lvValuesNode);
				// Task Start : Add by YuLong on 20090601
				String lvOrderGroupID = lvDataHash.get("ORDERGROUPID") == null ? ""
						: (String) lvDataHash.get("ORDERGROUPID");
				String lvOrderID = lvDataHash.get("ORDERID") == null ? ""
						: (String) lvDataHash.get("ORDERID");
				String lvClientID = lvDataHash.get("CLIENTID") == null ? ""
						: (String) lvDataHash.get("CLIENTID");
				String lvAction = lvDataHash.get("BS") == null ? ""
						: (String) lvDataHash.get("BS");
				String lvMarketID = lvDataHash.get("MARKETID") == null ? ""
						: (String) lvDataHash.get("MARKETID");
				String lvStatus = lvDataHash.get("STATUS_INTERNAL") == null ? ""
						: (String) lvDataHash.get("STATUS_INTERNAL");
				String lvPrice = lvDataHash.get("PRICE") == null ? ""
						: (String) lvDataHash.get("PRICE");
				String lvStopPrice = lvDataHash.get("STOPPRICE") == null ? ""
						: (String) lvDataHash.get("STOPPRICE");
				String lvShortName = lvDataHash.get("SHORTNAME") == null ? ""
						: (String) lvDataHash.get("SHORTNAME");
				String lvChannelID = lvDataHash.get("CHANNELID") == null ? ""
						: (String) lvDataHash.get("CHANNELID");
				String lvFilledQty = lvDataHash.get("FILLEDQTY") == null ? ""
						: (String) lvDataHash.get("FILLEDQTY");
				String lvQty = lvDataHash.get("QTY") == null ? ""
						: (String) lvDataHash.get("QTY");
				String lvRejectReason = lvDataHash.get("REJECTREASON") == null ? ""
						: (String) lvDataHash.get("REJECTREASON");
				String lvInputTime = lvDataHash.get("INPUTTIME") == null ? ""
						: TextFormatter.getFormattedTime((String) lvDataHash
								.get("INPUTTIME"), "HH:mm:ss");
				String lvPendPrice = lvDataHash.get("PENDPRICE") == null ? ""
						: (String) lvDataHash.get("PENDPRICE");
				String lvActivationDate = lvDataHash.get("ACTIVATIONDATE") == null ? ""
						: (String) lvDataHash.get("ACTIVATIONDATE");
				String lvGoodTillDate = lvDataHash.get("GOODTILLDATE") == null ? ""
						: (String) lvDataHash.get("GOODTILLDATE");
				String lvInstrumentID = lvDataHash.get("INSTRUMENTID") == null ? ""
						: (String) lvDataHash.get("INSTRUMENTID");
				String lvPendQty = lvDataHash.get("PENDQTY") == null ? ""
						: (String) lvDataHash.get("PENDQTY");
				String lvCurrencyID = lvDataHash.get("CURRENCYID") == null ? ""
						: (String) lvDataHash.get("CURRENCYID");
				String lvAvgPrice = lvDataHash.get("AVGPRICE") == null ? ""
						: (String) lvDataHash.get("AVGPRICE");
				String lvOSQty = lvDataHash.get("OSQTY") == null ? ""
						: (String) lvDataHash.get("OSQTY");
				String lvOrderType = lvDataHash.get("ORDERTYPE") == null ? ""
						: (String) lvDataHash.get("ORDERTYPE");
				String lvRemark = lvDataHash.get("REMARK") == null ? ""
						: (String) lvDataHash.get("REMARK");
				String lvCancelQty = lvDataHash.get("CANCELQTY") == null ? ""
						: (String) lvDataHash.get("CANCELQTY");
				String lvStopOrderType = lvDataHash.get("STOPORDERTYPE") == null ? ""
						: (String) lvDataHash.get("STOPORDERTYPE");
				String lvGrossAmt = lvDataHash.get("GROSSAMT") == null ? ""
						: (String) lvDataHash.get("GROSSAMT");
				String lvNetAmt = lvDataHash.get("NETAMT") == null ? ""
						: (String) lvDataHash.get("NETAMT");
				String lvContactPhone = lvDataHash.get("CONTACTPHONE") == null ? ""
						: (String) lvDataHash.get("CONTACTPHONE");
				String lvSCRIP = lvDataHash.get("SCRIP") == null ? ""
						: (String) lvDataHash.get("SCRIP");
				// Task End : Add by YuLong on 20090601

				// String lvClientID = (String)
				// lvDataHash.get(TagName.CLIENTID);
				Set lvAppletGatewaySet = AppletGateway
						.getAppletGatewaySetByClientID(lvClientID);
				// Task Start : Add by YuLong on 20090601
				// sendMesasgeToGateway(lvAppletGatewaySet,
				// TopicName.TOPIC_HKSFO_ORDER_ENQUIRY + "|" + lvClientID +
				// "\n");
				String lvTopOrderBeanString = TopicName.TOPIC_HKSFO_ORDER_ENQUIRY
						+ "|" + lvOrderGroupID + "|" + lvOrderID + "|" + lvClientID
						+ "|" + lvAction + "|" + lvMarketID + "|" + lvStatus 
						+ "|" + lvPrice + "|" + lvStopPrice + "|" + lvShortName + "|" + lvChannelID
						+ "|" + lvFilledQty + "|" + lvQty + "|" + lvRejectReason
						+ "|" + lvInputTime + "|" + lvPendPrice + "|" + lvActivationDate
						+ "|" + lvGoodTillDate + "|" + lvInstrumentID + "|" + lvPendQty
						+ "|" + lvCurrencyID + "|" + lvAvgPrice + "|" + lvOSQty + "|" + lvOrderType
						+ "|" + lvRemark + "|" + lvCancelQty + "|" + lvStopOrderType + "|"
						+ lvGrossAmt + "|" + lvNetAmt + "|" + lvContactPhone + "|" + lvSCRIP;
				sendMesasgeToGateway(lvAppletGatewaySet, lvTopOrderBeanString);
				OrderEnquiryDataCachingManager.insertOrderEnquiryString(lvClientID, lvTopOrderBeanString);
				// Task End : Add by YuLong on 20090601
			}
		} else if (pNode.getName().equalsIgnoreCase(
				TopicName.TOPIC_HKSFO_ITRADE_CLIENT_LOGIN)) {
			IMsgXMLNode lvFieldsNode = pNode.getChildNode("FNs");
			IMsgXMLNodeList lvNodeListValue = pNode.getChildNode("LoopRows")
					.getNodeList("Vs");
			for (int i = 0; i < lvNodeListValue.size(); i++) {
				IMsgXMLNode lvValuesNode = lvNodeListValue.getNode(i);
				Hashtable lvDataHash = FieldSplitter.tokenizeNodes(
						lvFieldsNode, lvValuesNode);
				String lvClientID = (String) lvDataHash.get(TagName.CLIENTID);
				String lvITradeServerIP = (String) lvDataHash
						.get("ITRADESERVERIP");
				if (!lvITradeServerIP.equalsIgnoreCase("")) {
					if (!ITradeServlet.getItradeServerIP().equals(
							lvITradeServerIP)) {
						lvITradeServerIP = Utils.isNullStr(lvITradeServerIP) ? ""
								: lvITradeServerIP;
						String lvSvTradeServerIP = Utils
								.isNullStr(ITradeServlet.getItradeServerIP()) ? ""
								: ITradeServlet.getItradeServerIP();
						Log.println(
								"TPBroadcastManager : Remove Session ! Login IP : "
										+ lvITradeServerIP + " Server IP :"
										+ lvSvTradeServerIP, Log.DEBUG_LOG);
						SessionTracker lvSessionTracker = ITradeServlet
								.getIvStartUpMain().getLocalSessionTracker();
						lvSessionTracker.removeSession(lvClientID);
					}
				}
			}
		}
		// Added by Bowen Chau on 30 Mar 2006
		// Handle TOPIC_HKSBO_MARKET_TRADEDATE_ADVANCED dynamic notification
		// case
		else if (pNode.getName().equalsIgnoreCase(
				TopicName.TOPIC_HKSBO_MARKET_TRADEDATE_ADVANCED)) {
            Log.println("Start receive TOPIC_HKSBO_MARKET_TRADEDATE_ADVANCED", Log.ACCESS_LOG);
			if (!ITradeServlet.getDownloadTradeDateStarted()
					&& !ITradeServlet.getDownloadInstrumentStarted()) {
				ITradeServlet.setDownloadInstrumentStarted(true);
				ITradeServlet.setDownloadTradeDateStarted(true);
				
				// Begin - Task #: TTL-HK-WLCW-00963. - Walter Lau 20100122 Re-Try Download Data
                Thread lvThread = new Thread(new Runnable() {

                    public boolean lvDownloadTradeDateFinished = false;
                    public boolean lvDownloadInstrumentFinished = false;
                    public boolean lvReDownloadStarted = false;

                    public void run() {

                        try {
                            // Download trade date information
                            downloadDataFromTP();

                            if (!lvDownloadTradeDateFinished || !lvDownloadInstrumentFinished) {
                                if (!lvReDownloadStarted && ITradeServlet.getReDownloadDataEnable()) {
                                    reDownloadDataFromTP();
                                }
                            }


                        } catch (Exception ex) {
                            Log.println(ex, Log.ERROR_LOG);
                        }


                    }

                    public void downloadDataFromTP() {

                        Log.println("[ TPBroadcastManager.download Data From TP Node Name : TOPIC_HKSBO_MARKET_TRADEDATE_ADVANCED ]", Log.ACCESS_LOG);


                        if (!lvDownloadTradeDateFinished) {
                            // Download trade date information
                            HKSDownloadTradeDateTxn lvHKSDownloadTradeDateTxn = new HKSDownloadTradeDateTxn(Integer.parseInt(ITradeServlet.getMvITradeProperties().getProperty("NumberOfAvailableGoodTillDate")));
                            try {
                                lvHKSDownloadTradeDateTxn.process();
                            } catch (Exception ex) {
                                lvReDownloadStarted = false;
                                Log.println(ex, Log.ERROR_LOG);
                            }

                            int lvReturnCode = lvHKSDownloadTradeDateTxn.getReturnCode();
                            if (lvReturnCode != TPErrorHandling.TP_NORMAL) {
                                if (lvReturnCode == TPErrorHandling.TP_APP_ERR) {
                                    Log.println("[ TPBroadcastManager.queryNextTradeDateProcess(): Download TradeDate Action failed with application error:" + lvHKSDownloadTradeDateTxn.getErrorMessage() + " ]", Log.ERROR_LOG);
                                } else if (lvReturnCode == TPErrorHandling.TP_SYS_ERR) {
                                    Log.println("[ TPBroadcastManager.queryNextTradeDateProcess(): Download TradeDate Action failed with system error:" + lvHKSDownloadTradeDateTxn.getErrorMessage() + " ]", Log.ERROR_LOG);
                                }

                                lvReDownloadStarted = false;
                                //mvDownloadTimer.cancel();
                            } else {
                                lvDownloadTradeDateFinished = true;
                            }
                        }

                        // Download instrument information
                        if (!lvDownloadInstrumentFinished) {
//                                        HKSDownloadInstrumentTxn lvHKSDownloadInstrumentTxn = new HKSDownloadInstrumentTxn();

                            //Begin Task: WL00619 Walter Lau 2007 July 27
                            HKSDownloadInstrumentTxn lvHKSDownloadInstrumentTxn = new HKSDownloadInstrumentTxn(ITradeServlet.getMvITradeProperties().getProperty("UsingStockCodeFormatMarket"));
                            //End Task: WL00619 Walter Lau 2007 July 27
                            try {
                                lvHKSDownloadInstrumentTxn.process();
                            } catch (Exception ex) {
                                lvReDownloadStarted = false;
                                Log.println(ex, Log.ERROR_LOG);
                            }

                            int lvReturnCode = lvHKSDownloadInstrumentTxn.getReturnCode();
                            if (lvReturnCode != TPErrorHandling.TP_NORMAL) {
                                if (lvReturnCode == TPErrorHandling.TP_APP_ERR) {
                                    Log.println("[ TPBroadcastManager.initInstrumentProcess(): Download Instrument Action failed with application error:" + lvHKSDownloadInstrumentTxn.getErrorMessage() + " ]", Log.ERROR_LOG);
                                } else if (lvReturnCode == TPErrorHandling.TP_SYS_ERR) {
                                    Log.println("[ TPBroadcastManager.initInstrumentProcess(): Download Instrument Action failed with system error:" + lvHKSDownloadInstrumentTxn.getErrorMessage() + " ]", Log.ERROR_LOG);
                                }
                                //mvDownloadTimer.cancel();
                                lvReDownloadStarted = false;
                            } else {
                                lvDownloadInstrumentFinished = true;
                            }
                        }
                    }
                    
                    public void reDownloadDataFromTP() {
                        try {

                            Log.println("[ TPBroadcastManager.operatorLogin() re-get Data form TP sleep start: ]", Log.ACCESS_LOG);
                            this.wait(ITradeServlet.getReDownloadDataTime());
                            Log.println("[ TPBroadcastManager.operatorLogin() re-get Data form TP sleep stop and download again: ]", Log.ACCESS_LOG);
                            downloadDataFromTP();
                        } catch (Exception ex) {
                            Log.println(ex, Log.ERROR_LOG);
                        }
                    }
                    //END - Task #: TTL-HK-WLCW-00963. - Walter Lau 20100122 Re-Try Download Data
				});
				lvThread.start();
			}

		}
		// Added by Bowen Chau on 29 Mar 2006
		// Handle MCSYS dynamic notification case
		else if (pNode.getName().equalsIgnoreCase("MCSYS")
				|| pNode.getName().equalsIgnoreCase("CORDL001D49")) {
            Log.println("Start receive MCSYS or CORDL001D49", Log.ACCESS_LOG);
			if (!ITradeServlet.getDownloadInstrumentStarted()
					&& !ITradeServlet.getDownloadTradeDateStarted()) {
				ITradeServlet.setDownloadInstrumentStarted(true);
				ITradeServlet.setDownloadTradeDateStarted(true);

				//BEGIN - Task #: TTL-HK-WLCW-00963. - Walter Lau 20100122 Re-Try Download Data
				Thread lvThread = new Thread(new Runnable() {
	                public boolean lvDownloadTradeDateFinished = false;
	                public boolean lvDownloadInstrumentFinished = false;
	                public boolean lvReDownloadStarted = false;
	
	                public void run() {
	                    try {
	                        downloadDataFromTP();
	
	                        if (!lvDownloadTradeDateFinished || !lvDownloadInstrumentFinished) {
	                            if (!lvReDownloadStarted && ITradeServlet.getReDownloadDataEnable()) {
	                                reDownloadDataFromTP();
	                            }
	                        }
	
	                    } catch (Exception ex) {
	                        Log.println(ex, Log.ERROR_LOG);
	                    }
	                }
	
	                public void downloadDataFromTP() {
	
	                    Log.println("[ TPBroadcastManager.download Data From TP Node Name : MCSYS ]", Log.ACCESS_LOG);
	
	
	                    if (!lvDownloadTradeDateFinished) {
	                        // Download trade date information
	                        HKSDownloadTradeDateTxn lvHKSDownloadTradeDateTxn = new HKSDownloadTradeDateTxn(Integer.parseInt(ITradeServlet.getMvITradeProperties().getProperty("NumberOfAvailableGoodTillDate")));
	                        try {
	                            lvHKSDownloadTradeDateTxn.processMarketClose();
	                        } catch (Exception ex) {
	                            lvReDownloadStarted = false;
	                            Log.println(ex, Log.ERROR_LOG);
	                        }
	
	                        int lvReturnCode = lvHKSDownloadTradeDateTxn.getReturnCode();
	                        if (lvReturnCode != TPErrorHandling.TP_NORMAL) {
	                            if (lvReturnCode == TPErrorHandling.TP_APP_ERR) {
	                                Log.println("[ TPBroadcastManager.queryNextTradeDateProcess(): Download TradeDate Action failed with application error:" + lvHKSDownloadTradeDateTxn.getErrorMessage() + " ]", Log.ERROR_LOG);
	                            } else if (lvReturnCode == TPErrorHandling.TP_SYS_ERR) {
	                                Log.println("[ TPBroadcastManager.queryNextTradeDateProcess(): Download TradeDate Action failed with system error:" + lvHKSDownloadTradeDateTxn.getErrorMessage() + " ]", Log.ERROR_LOG);
	                            }
	
	                            lvReDownloadStarted = false;
	                        } else {
	                            lvDownloadTradeDateFinished = true;
	                        }
	                    }
	
	                    if (!lvDownloadInstrumentFinished) {
	                        // Download instrument information
	                        //Begin Task: WL00619 Walter Lau 2007 July 27
	                        HKSDownloadInstrumentTxn lvHKSDownloadInstrumentTxn = new HKSDownloadInstrumentTxn(ITradeServlet.getMvITradeProperties().getProperty("UsingStockCodeFormatMarket"));
	                        //End Task: WL00619 Walter Lau 2007 July 27
	                        try {
	
	                            // Download instrument information
	
	                            lvHKSDownloadInstrumentTxn.process();
	                        } catch (Exception ex) {
	                            lvReDownloadStarted = false;
	                            Log.println(ex, Log.ERROR_LOG);
	                        }
	
	                        int lvReturnCode = lvHKSDownloadInstrumentTxn.getReturnCode();
	                        if (lvReturnCode != TPErrorHandling.TP_NORMAL) {
	                            if (lvReturnCode == TPErrorHandling.TP_APP_ERR) {
	                                Log.println("[ TPBroadcastManager.initInstrumentProcess(): Download Instrument Action failed with application error:" + lvHKSDownloadInstrumentTxn.getErrorMessage() + " ]", Log.ERROR_LOG);
	                            } else if (lvReturnCode == TPErrorHandling.TP_SYS_ERR) {
	                                Log.println("[ TPBroadcastManager.initInstrumentProcess(): Download Instrument Action failed with system error:" + lvHKSDownloadInstrumentTxn.getErrorMessage() + " ]", Log.ERROR_LOG);
	                            }
	                            //mvDownloadTimer.cancel();
	                            lvReDownloadStarted = false;
	                        } else {
	                            lvDownloadInstrumentFinished = true;
	                        }
	                    }
	                }
	
	                public void reDownloadDataFromTP() {
	                    try {
	                        lvReDownloadStarted = true;
	                        Log.println("[ TPBroadcastManager.operatorLogin() re-get Data form TP sleep start: ]", Log.ACCESS_LOG);
	                        this.wait(ITradeServlet.getReDownloadDataTime());
	                        Log.println("[ TPBroadcastManager.operatorLogin() re-get Data form TP sleep stop and download again: ]", Log.ACCESS_LOG);
	                        downloadDataFromTP();
	                    } catch (Exception ex) {
	                        Log.println(ex, Log.ERROR_LOG);
	                    }
	                }
	                //END - Task #: TTL-HK-WLCW-00963. - Walter Lau 20100122 Re-Try Download Data
				});
				lvThread.start();
			}
            Log.println("End receive MCSYS or CORDL001D49", Log.ACCESS_LOG);
		}
		// Modified by Bowen Chau on 6 Apr 2006
		// Handle MCINSTRUMNET and CORDL001D19 dynamic notification to update
		// instrument information
		else if (pNode.getName().equalsIgnoreCase("MCINSTRUMENT")
				|| pNode.getName().equalsIgnoreCase("CORDL001D19")) {
            Log.println("Start receive MCINSTRUMENT or CORDL001D19", Log.ACCESS_LOG);
			if (!ITradeServlet.getDownloadInstrumentStarted()) {
				ITradeServlet.setDownloadInstrumentStarted(true);
				// Begin Task: WL00619 Walter Lau 2007 July 27
				// BEGIN TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5]
				// Allow plugin.ini full Customization
				HKSDownloadInstrumentTxn lvHKSDownloadInstrumentTxn = new HKSDownloadInstrumentTxn(
				IMain.getProperty("UsingStockCodeFormatMarket"));
				// End Task: WL00619 Walter Lau 2007 July 27
				// END TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5]
				// Allow plugin.ini full Customization
				lvHKSDownloadInstrumentTxn.updateInstrument(pNode);
			}
            Log.println("End receive MCINSTRUMENT or CORDL001D19", Log.ACCESS_LOG);
		}
		// Modified by Bowen Chau on 06 Apr 2006
		// Handle TOPIC_HKSFO_MARKET_STATUS dynamic notification to update good
		// till date after market close
		else if (pNode.getName().equalsIgnoreCase(
				TopicName.TOPIC_HKSFO_MARKET_STATUS)) {
			Log.println("Start receive TOPIC_HKSFO_MARKET_STATUS", Log.ACCESS_LOG);
			//Begin task 2010-11-17 VanTran Update Run Clear Historical Data Flag
			IMain.mvIsRunClearPreHistoricalData=false;
			//End task 2010-11-17 VanTran Update Run Clear Historical Data Flag
			
			boolean lvMarketCloseFlag = false;
			IMsgXMLNode lvFieldsNode = pNode.getChildNode("FNs");
			IMsgXMLNodeList lvNodeListValue = pNode.getChildNode("LoopRows")
					.getNodeList("Vs");
			for (int i = 0; i < lvNodeListValue.size(); i++) {
				IMsgXMLNode lvValuesNode = lvNodeListValue.getNode(i);
				Hashtable lvDataHash = FieldSplitter.tokenizeNodes(
						lvFieldsNode, lvValuesNode);
				String lvMarketCode = (String) lvDataHash.get("MARKETCODE");
				String lvMarketID = (String) lvDataHash.get("MARKETID");
				String lvMarketStatus = (String) lvDataHash
						.get("TRADING_STATUS");

				// Add market status to hash map
				if(IMain.mvMarketStatus!=null && !lvMarketID.trim().isEmpty() && !lvMarketStatus.trim().isEmpty()) {
					IMain.mvMarketStatus.put(lvMarketID, lvMarketStatus);
				}
				
				// Identify the market status of MAIN board
				if (("MAIN").equals(lvMarketCode)
						&& ("DC").equals(lvMarketStatus)) {
					i = lvNodeListValue.size() + 1; // Leave the FOR loop
					lvMarketCloseFlag = true;
				}
			}
			if (lvMarketCloseFlag) {
				if (!ITradeServlet.getDownloadTradeDateStarted()) {
					ITradeServlet.setDownloadTradeDateStarted(true);
					Thread lvThread = new Thread(new Runnable() {
						//BEGIN - Task #: TTL-HK-WLCW-00963. - Walter Lau 20100122 Re-Try Download Data
                        public boolean lvDownloadTradeDateFinished = false;
                        public boolean lvReDownloadStarted = false;

                        public void run() {
                            try {
                                downloadDataFromTP();

                                if (!lvDownloadTradeDateFinished) {
                                    if (!lvReDownloadStarted && ITradeServlet.getReDownloadDataEnable()) {
                                        reDownloadDataFromTP();
                                    }
                                }

                            } catch (Exception ex) {
                                Log.println(ex, Log.ERROR_LOG);
                            }
                        }

                        public void downloadDataFromTP() {
                            Log.println("[ TPBroadcastManager.download Data From TP Node Name : TOPIC_HKSFO_MARKET_STATUS ]", Log.ACCESS_LOG);
                            if (!lvDownloadTradeDateFinished) {
                                // Download trade date information
                                HKSDownloadTradeDateTxn lvHKSDownloadTradeDateTxn = new HKSDownloadTradeDateTxn(Integer.parseInt(ITradeServlet.getMvITradeProperties().getProperty("NumberOfAvailableGoodTillDate")));
                                try {
                                    lvHKSDownloadTradeDateTxn.processMarketClose();
                                } catch (Exception ex) {
                                    lvReDownloadStarted = false;
                                    Log.println(ex, Log.ERROR_LOG);
                                }
                                int lvReturnCode = lvHKSDownloadTradeDateTxn.getReturnCode();
                                if (lvReturnCode != TPErrorHandling.TP_NORMAL) {
                                    if (lvReturnCode == TPErrorHandling.TP_APP_ERR) {
                                        Log.println("[ TPBroadcastManager.queryNextTradeDateProcess(): Download TradeDate Action failed with application error:" + lvHKSDownloadTradeDateTxn.getErrorMessage() + " ]", Log.ERROR_LOG);
                                    } else if (lvReturnCode == TPErrorHandling.TP_SYS_ERR) {
                                        Log.println("[ TPBroadcastManager.queryNextTradeDateProcess(): Download TradeDate Action failed with system error:" + lvHKSDownloadTradeDateTxn.getErrorMessage() + " ]", Log.ERROR_LOG);
                                    }

                                    lvReDownloadStarted = false;
                                } else {
                                    lvDownloadTradeDateFinished = true;
                                }
                            }
                        }

                        public void reDownloadDataFromTP() {
                            try {
                                lvReDownloadStarted = true;
                                Log.println("[ TPBroadcastManager.operatorLogin() re-get Data form TP sleep start: ]", Log.ACCESS_LOG);
                                this.wait(ITradeServlet.getReDownloadDataTime());
                                Log.println("[ TPBroadcastManager.operatorLogin() re-get Data form TP sleep stop and download again: ]", Log.ACCESS_LOG);
                                downloadDataFromTP();
                            } catch (Exception ex) {
                                Log.println(ex, Log.ERROR_LOG);
                            }
                        }
						//END - Task #: TTL-HK-WLCW-00963. - Walter Lau 20100122 Re-Try Download Data
					});
					lvThread.start();
				}
			}
			Log.println("End receive TOPIC_HKSFO_MARKET_STATUS", Log.ACCESS_LOG);
		} else if (pNode.getName().equalsIgnoreCase("TOPIC_ORDER_EXPIRATION")
				|| pNode.getName().equalsIgnoreCase("TOPIC_DAY_END")) {
			Log.println("Start receive TOPIC_ORDER_EXPIRATION or TOPIC_DAY_END", Log.ACCESS_LOG);
			if (!ITradeServlet.getDownloadTradeDateStarted()) {
				ITradeServlet.setDownloadTradeDateStarted(true);
				Thread lvThread = new Thread(new Runnable() {
					//BEGIN - Task #: TTL-HK-WLCW-00963. - Walter Lau 20100122 Re-Try Download Data
                    public boolean lvDownloadTradeDateFinished = false;
                    public boolean lvReDownloadStarted = false;

                    public void run() {
                        try {
                            downloadDataFromTP();

                            if (!lvDownloadTradeDateFinished) {
                                if (!lvReDownloadStarted && ITradeServlet.getReDownloadDataEnable()) {
                                    reDownloadDataFromTP();
                                }
                            }

                        } catch (Exception ex) {
                            Log.println(ex, Log.ERROR_LOG);
                        }
                    }

                    public void downloadDataFromTP() {
                        Log.println("[ TPBroadcastManager.download Data From TP Node Name : TOPIC_ORDER_EXPIRATION ]", Log.ACCESS_LOG);
                        if (!lvDownloadTradeDateFinished) {
                            // Download trade date information
                            HKSDownloadTradeDateTxn lvHKSDownloadTradeDateTxn = new HKSDownloadTradeDateTxn(Integer.parseInt(ITradeServlet.getMvITradeProperties().getProperty("NumberOfAvailableGoodTillDate")));
                            try {
                                lvHKSDownloadTradeDateTxn.processMarketClose();
                            } catch (Exception ex) {
                                lvReDownloadStarted = false;
                                Log.println(ex, Log.ERROR_LOG);
                            }

                            int lvReturnCode = lvHKSDownloadTradeDateTxn.getReturnCode();
                            if (lvReturnCode != TPErrorHandling.TP_NORMAL) {
                                if (lvReturnCode == TPErrorHandling.TP_APP_ERR) {
                                    Log.println("[ TPBroadcastManager.queryNextTradeDateProcess(): Download TradeDate Action failed with application error:" + lvHKSDownloadTradeDateTxn.getErrorMessage() + " ]", Log.ERROR_LOG);
                                } else if (lvReturnCode == TPErrorHandling.TP_SYS_ERR) {
                                    Log.println("[ TPBroadcastManager.queryNextTradeDateProcess(): Download TradeDate Action failed with system error:" + lvHKSDownloadTradeDateTxn.getErrorMessage() + " ]", Log.ERROR_LOG);
                                }

                                lvReDownloadStarted = false;
                            } else {
                                lvDownloadTradeDateFinished = true;
                            }
                        }
                    }

                    public void reDownloadDataFromTP() {
                        try {
                            lvReDownloadStarted = true;
                            Log.println("[ TPBroadcastManager.operatorLogin() re-get Data form TP sleep start: ]", Log.ACCESS_LOG);
                            this.wait(ITradeServlet.getReDownloadDataTime());
                            Log.println("[ TPBroadcastManager.operatorLogin() re-get Data form TP sleep stop and download again: ]", Log.ACCESS_LOG);
                            downloadDataFromTP();
                        } catch (Exception ex) {
                            Log.println(ex, Log.ERROR_LOG);
                        }
                    }
                    //END - Task #: TTL-HK-WLCW-00963. - Walter Lau 20100122 Re-Try Download Data
				});
				lvThread.start();
			}
			Log.println("End receive TOPIC_ORDER_EXPIRATION or TOPIC_DAY_END", Log.ACCESS_LOG);
		}
		// VanTran add for market index update
		else if (pNode.getName().equalsIgnoreCase("TOPIC_HKSFO_MARKET_INDEX"))
		{
			if(useFoBroadcastMarketData){
				Log.println("Start receive from TOPIC_HKSFO_MARKET_INDEX", Log.ACCESS_LOG);
				//Begin task 2010-11-17 VanTran Update Run Clear Historical Data Flag
				IMain.mvIsRunClearPreHistoricalData=false;
				//End task 2010-11-17 VanTran Update Run Clear Historical Data Flag
				
				IMsgXMLNode lvFieldsNode = pNode.getChildNode("FNs");
				IMsgXMLNodeList lvNodeListValue = pNode.getChildNode("LoopRows").getNodeList("Vs");
				HKSMarketIndexBean matketData;
				for (int i = 0; i < lvNodeListValue.size(); i++) {
					IMsgXMLNode lvValuesNode = lvNodeListValue.getNode(i);
					Hashtable lvDataHash = FieldSplitter.tokenizeNodes(lvFieldsNode, lvValuesNode);
					
					// Task Start : Add by YuLong on 20090601
					matketData= new HKSMarketIndexBean();
					String lvMarketID = lvDataHash.get("MARKETID") == null ? "": (String) lvDataHash.get("MARKETID");
					matketData.setMvMaketID(lvMarketID);
					
					String lvMarketIndex = lvDataHash.get(TagName.MARKETINDEX) == null ? "": (String) lvDataHash.get(TagName.MARKETINDEX);
					String lvDifference = lvDataHash.get("INDEXCHANGE") == null ? "": (String) lvDataHash.get("INDEXCHANGE");
					String lvPercentage = lvDataHash.get("INDEXPERCENT") == null ? "": (String) lvDataHash.get("INDEXPERCENT");
					
					String lvMarketTotalValue = lvDataHash.get("MARKETTOTALVALUE") == null ? "": (String) lvDataHash.get("MARKETTOTALVALUE");
					
					String lvMarketTotalQty;
					// BEGIN - Task #: TTL-VN VanTran For HOSE need to get total market qty with MARKETTOTALSTOCK
					if(lvMarketID.equalsIgnoreCase("HO")){
						lvMarketTotalQty= lvDataHash.get("MARKETTOTALSTOCK") == null ? "": (String) lvDataHash.get("MARKETTOTALSTOCK");
					}else {
						lvMarketTotalQty= lvDataHash.get("MARKETTOTALQTY") == null ? "": (String) lvDataHash.get("MARKETTOTALQTY");
					}
					// END - Task #: TTL-VN VanTran For HOSE need to get total market qty with MARKETTOTALSTOCK
					
					String lvMarketAdvance = lvDataHash.get("MARKETADVANCES") == null ? "": (String) lvDataHash.get("MARKETADVANCES");
					String lvMarketDecline = lvDataHash.get("MARKETDECLINES") == null ? "": (String) lvDataHash.get("MARKETDECLINES");
					String lvMarketNoChange = lvDataHash.get("MARKETNOCHANGE") == null ? "": (String) lvDataHash.get("MARKETNOCHANGE");
					
					matketData.setMvMarketIndex(lvMarketIndex);
					matketData.setMvDifference(lvDifference);
					matketData.setMvPercentage(lvPercentage);
					
					matketData.setMvMarketTotalvalue(lvMarketTotalValue);
					matketData.setMvMarketTotalQty(lvMarketTotalQty);
					
					matketData.setMvMarketAdvances(lvMarketAdvance);
					matketData.setMvMarketDeclines(lvMarketDecline);
					matketData.setMvMarketNoChange(lvMarketNoChange);
					
					if(IMain.mvMarketIndex!=null) {
						IMain.mvMarketIndex.put(lvMarketID, matketData);
					}
				}
			}
			
		}
		// VanTran add end
		
		//BEGIN - TASK : Van Tran 20100802 - get FO server time
		else if (pNode.getName().equalsIgnoreCase("XHBT")){
			if(pNode.getAttribute("SERVER_TIME") !=null ){
				IMain.SERVER_TIME = Long.valueOf(pNode.getAttribute("SERVER_TIME"));
			}
			
		}
		//END - TASK : Van Tran 20100802 - add variable to store the FO server time
	}

	*//**
	 * This method sends updated message that comes from TP to every gateway. 
	 * @param pGatewaySet : A set that contains the Gateway object that belongs the client.
	 * @param lvMessageData : A String object that contains the updated message that comes from TP.
	 *//*
	private void sendMesasgeToGateway(Set pGatewaySet, String lvMessageData) {
		if (pGatewaySet == null)
			return;
		try {
			Iterator lvIter = pGatewaySet.iterator();
			while (lvIter.hasNext()) {
				AppletGateway lvAppletGateWay = (AppletGateway) lvIter.next();
				if (lvAppletGateWay != null) {
					lvAppletGateWay.addPendingSendMessage(lvMessageData, false);
					ObpWorkerThread lvWorkerThread = (ObpWorkerThread) ivNotificationThreadPool
							.checkOut(true);
					lvWorkerThread.doJob(lvAppletGateWay);
				}
			}
		} catch (Exception e) {
			Log.println(e, Log.ERROR_LOG);
		}
	}

	*//**
	 * This method sends the market updated data that comes from TP to every gateway.
	 * @param lvMessageData : A String object that contains the market updated data that comes from TP.
	 * @param pInstrumentID : Stock id in the market.
	 *//*
	private void sendMarketDataToPool(String lvMessageData, String pInstrumentID,boolean isNeedWrite2File) {
		try {
//			Log.println("FO.msg.broadcast:" + useFoBroadcastMarketData + ":" + lvMessageData, Log.ACCESS_LOG);
			if(useFoBroadcastMarketData){
				AppletGateway lvAppletGateWay = new AppletGateway();
				// for the first time checking
				if (ivPendingInsturmentID.size() == 0) {
					ivPendingInsturmentID.add(pInstrumentID);
					lvAppletGateWay.addPendingMarketData(lvMessageData);
				} else {
					// loop for finding and replacing the instruments which already
					// exist in the pool
					boolean flag = false;
					for (int i = 0; i < ivPendingInsturmentID.size(); i++) {
						if (ivPendingInsturmentID.get(i).equals(pInstrumentID)) {
							lvAppletGateWay.addPendingMarketData(lvMessageData);
							flag = true;
							break;
						}
					}
					
					if (flag == false) {
						// add new instrument to list if this stock does not exist in
						// the pool
						ivPendingInsturmentID.add(pInstrumentID);
						lvAppletGateWay.addPendingMarketData(lvMessageData);	
					}
					
				}
				if(isNeedWrite2File){
					//BEGIN - TASK : Nghia Nguyen 20100722 - historical trade data
					Long ts = System.currentTimeMillis();
					IMain.addTradingHistData(pInstrumentID, ts, lvMessageData);
					//END - TASK : Nghia Nguyen 20100722 - historical trade data
				}
			}
		} catch (Exception e) {
			Log.println(e, Log.ERROR_LOG);
		}
	}
	
}
*/