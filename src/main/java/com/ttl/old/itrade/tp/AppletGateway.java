package com.ttl.old.itrade.tp;

import java.io.BufferedOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpSession;

import com.ttl.old.itrade.hks.util.StringSplitter;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.obp.IObpThreadPoolJob;
import com.systekit.winvest.hks.config.mapping.TopicName;

/**
 * The AppletGateway class defined methods that process some message.
 * @author
 *
 */
public class AppletGateway extends Thread implements IObpThreadPoolJob, Comparable
{
	static private int svAppletID = 0;
	static private Map svAppletGateWaySet_MapByClientID = Collections.synchronizedMap(new HashMap());
	// added mingl 20050125
	static private int svAppletGatewaySocketLingerTime = 30;
	static private int svAppletGatewaySocketTimeOut = 30;
	static private boolean svAppletGatewaySocketKeepAlive = false;
	static private boolean svAppletGatewaySocketLinger = false;
	private long ivSendingStartTime = 0; // 0 - not in sending status
	private Thread ivMonitoringThread = null;
	private boolean ivMonitoringThreadReadyToDead = false;
	private int ivAppletID;
	private Socket ivAppletSocket;
	private BufferedOutputStream ivAppletOutputStream = null;
	private BufferedLineReader ivAppletReader = null;
	private Vector ivPendingMessage = new Vector();
	//private static List<String> ivPendingInsturmentID = new ArrayList<String>();
	//public static List<String> ivPendingMarketData = new ArrayList<String>();
	public static Map<String, String> ivPendingMarketData = new ConcurrentHashMap<String, String>(){

		@Override
		public String put(String key, String value) {
			lastPendingUpdated = System.currentTimeMillis();
			return super.put(key, value);
		}
		
	};
	
	private static long lastPendingUpdated = 0;
	
	public static long getLastPendingUpdated() {
		return lastPendingUpdated;
	}

	private String ivSocketString = "";
	private boolean ivStartByApplet = true;
	private boolean ivClientRegistered = false;
	private boolean ivPendingOrderEnquiry = false;
	private boolean ivThreadIsSleeping = false;
	private List<String> ivPendingAJAXMessage = new ArrayList<String>();
	//private String ivReplyMessage = "";
	Object ivObject = new Object();

	/**
	 * Returns the message list of pending about ajax. 
	 * @return The message list of pending about ajax.
	 */
	//Task Start : Add by YuLong on 20090601
	public List getMvPendingAJAXMessage(){
		return this.ivPendingAJAXMessage;
	}
	/**
	 * Sets the message list of pending about ajax. 
	 * @param pPendingAJAXMessage The message list of pending about ajax.
	 */
	public void setMvPendingAJAXMessage(List pPendingAJAXMessage){
		this.ivPendingAJAXMessage = pPendingAJAXMessage;
	}
	//Task End : Add by YuLong on 20090601
	
	/**
	 * Default constructor for AppletGateway class.
	 */
	protected AppletGateway()
	{
		ivAppletID = getNextAppletID();
		setName("AppletGateway " + ivAppletID);
	}

	/**
	 * This constructor is made specially for the invocation by AJAX servlet
	 * handler. The object created by this method is not a thread. Do not call
	 * start by object create by this constructor! Use AppletGateway(Socket) if
	 * it is a traditional AppletGateway.
	 *
	 * @param pClientID
	 *            The Session's Client ID
	 */
	public AppletGateway(String pClientID)
	{
		this();
		ivStartByApplet = false;
		//Thread df = createDummyUpdateMarketIndexThread();
		//df.start();
	}

	/**
	 * This constructor use socket create a object.
	 * @param pSocket The client socket.
	 */
	public AppletGateway(Socket pSocket)
	{
		this();
		ivStartByApplet = true;
		ivAppletSocket = pSocket;
		ivSocketString = ivAppletSocket.toString();
		// added mingl 20050125
		final AppletGateway lvAG = this;
		ivMonitoringThread = new Thread(new Runnable()
		{
			public void run()
			{
				while (true)
				{
					try
					{
						if (lvAG.ivMonitoringThreadReadyToDead)
							break;
						if (lvAG.ivSendingStartTime != 0)
						{
							long lvNow = Calendar.getInstance().getTime().getTime();
							long lvDiffinMS = lvNow - lvAG.ivSendingStartTime;
							if ((lvDiffinMS) >= (AppletGateway.getAppletGatewaySocketTimeOut() * 1000))
							{
								// timeout and close the socket
								Log.println("sendToClient() timeout with " + lvDiffinMS + "ms - AppletGateway: " + ivAppletID + " IP: " + ivAppletSocket.getInetAddress().getHostAddress(), Log.DEBUG_LOG);
								lvAG.removeAppletGateway(lvAG);
								lvAG.disconnect();
								Log.println("AppletGateway: " + ivAppletID + " socket was closed.", Log.DEBUG_LOG);
								break;
							}
						}
						
						Thread.sleep(1000);
					}
					catch (Exception e)
					{
						Log.println(e, Log.ERROR_LOG);
					}
				}
			}
		});
		// lvMonitoringThread.start();
	}

	/**
	 * This method use client id to get  a set of AppletGateway.
	 * @param pClientID The client id.
	 * @return AppletGateway's set.
	 */
	public static Set getAppletGatewaySetByClientID(String pClientID)
	{
		return (Set) svAppletGateWaySet_MapByClientID.get(pClientID);
	}

	/**
	 * This method to remove a AppletGateway object.
	 * @param pObj The AppletGateway object.
	 */
	public static void removeAppletGateway(AppletGateway pObj)
	{
		synchronized (svAppletGateWaySet_MapByClientID)
		{
			for (Iterator lvIter = svAppletGateWaySet_MapByClientID.values().iterator(); lvIter.hasNext();)
			{
				Set lvAppletGatewaySet = (Set) lvIter.next();
				lvAppletGatewaySet.remove(pObj);
			}
		}
	}

	/**
	 * This method get next applet id.
	 * @return The next applet id.
	 */
	private synchronized int getNextAppletID()
	{
		try
		{
			return ++svAppletID;
		}
		catch (Exception ex)
		{
			return 0;
		}
	}

	/**
	 * This method get applet client id.
	 * @return The applet client id.
	 */
	public int getAppletClientID()
	{
		return ivAppletID;
	}

	/**
	 * This method to add send message of pending.
	 * @param pMessage the send message.
	 * @param pSendFirst the is first send or not.
	 */
	public void addPendingSendMessage(String pMessage, boolean pSendFirst)
	{
		synchronized (ivPendingMessage)
		{
			if (pSendFirst)
			{
				ivPendingMessage.insertElementAt(pMessage, 0);
			}
			else
			{
				ivPendingMessage.addElement(pMessage);
			}
		}
	}
	
	/** 
	 * Pool for market data to add new instruments
	 * @param pMessage the data of market.
	 */
	public void addPendingMarketData(String pMessage)
	{	
		String[] tmp = StringSplitter.split(pMessage, "\\|");
		String key = (tmp[0]+"|"+tmp[1]).toUpperCase();
		//String value = pMessage.substring(key.length()-1);
		synchronized (ivPendingMarketData)
		{
			ivPendingMarketData.put(key, pMessage);
		}
	}
	/** 
	 * Pool for market data to replace the exist instruments
	 * @param the index of list.
	 * @param the data of market.
	 */
//	public void addPendingMarketData(int index, String pMessage)
//	{		
//		
//		synchronized (ivPendingMarketData)
//		{
//			ivPendingMarketData.remove(index);
//			ivPendingMarketData.add(index, pMessage);
//		}
//	}
	
	/**
	 * Thread Pool's thread entry point
	 */
	public void doThreadPoolJob()
	{
		String lvMessage = null;
		synchronized (ivPendingMessage)
		{
			while (ivPendingMessage.size() > 0)
			{
				lvMessage = (String) ivPendingMessage.elementAt(0);
				ivPendingMessage.removeElementAt(0);
			}
		}
		if (lvMessage != null)
			sendToClient(lvMessage);
	}
	
	/**
	 * This method wait specified time to do some operation.
	 * @param pAJAXWaitingTime the wait time.
	 * @param pSession the session.
	 * @return the list of reply data.
	 */
	public List waitForResponse(int pAJAXWaitingTime, HttpSession pSession)
	{
		Log.println("AppletGateway.waitForResponse: Start", Log.DEBUG_LOG);
		setClientRegistered(false);
		long lvRefreshRate = pSession.getAttribute("AJAXOrderEnquiryRefreshTime") == null ? 3000 : Long.parseLong(pSession.getAttribute("AJAXOrderEnquiryRefreshTime").toString().trim());
		String lvReplyMessage = "";
		List lvReplyData = null;
		if (ivPendingAJAXMessage.size() == 0)
		{
         synchronized(this)
         {
            if (getPendingOrderEnquiry())
            {
               setPendingOrderEnquiry(false);
               setThreadIsSleeping(true);
/*               try
               {
                  this.sleep(lvRefreshRate);
               }
               catch (InterruptedException lvInterruptedException)
               {
                  Log.println("AppletGateway.waitForResponse: " + lvInterruptedException, Log.ERROR_LOG);
               }*/

			   setPendingOrderEnquiry(false);
			   lvReplyMessage = "ORDER_ENQUIRY";
			   //sendToClient("ORDER_ENQUIRY");
			   setThreadIsSleeping(false);
            }
            else
            {
/*				try
				{
					this.wait(pAJAXWaitingTime);
				}
				catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
               // For dedug
               //System.out.println("Reply message: " + lvReplyMessage);
               if(ivPendingAJAXMessage.size()>0)
			   {
				   lvReplyMessage = (String) ivPendingAJAXMessage.remove(0);
				   setPendingOrderEnquiry(false);
			   }
               else
			   {
				   lvReplyMessage = "TOPIC_HKSFO_ORDER_ENQUIRY";
			   }
            }
         }
		}
		else
		{
			lvReplyData = ivPendingAJAXMessage;
			setPendingOrderEnquiry(false);
		}

		setClientRegistered(true);
        Log.println("AppletGateway.waitForResponse: END , " + lvReplyMessage, Log.DEBUG_LOG);

		return lvReplyData;
	}

	/**
	 * This method send message to client.
	 * @param pMessage the specified message.
	 */
	public void sendToClient(String pMessage)
	{
		String lvReplyMessage = "";
		try
		{
			if (ivStartByApplet)
			{
				synchronized (ivAppletOutputStream)
				{
					// added by mingl 20050124
					long lvStartTime = Calendar.getInstance().getTime().getTime();
					// mark the start time
					ivSendingStartTime = lvStartTime;
					Log.println("to IP: " + ivAppletSocket.getInetAddress().getHostAddress() + " start", Log.ALERT_LOG);
					ivAppletOutputStream.write(pMessage.getBytes("UTF8"));
					ivAppletOutputStream.flush();
					Log.println("to IP: " + ivAppletSocket.getInetAddress().getHostAddress() + " end", Log.ALERT_LOG);
					// reset the start time
					ivSendingStartTime = 0;
					long lvEndTime = Calendar.getInstance().getTime().getTime();
					long lvDiffinMS = lvEndTime - lvStartTime;
					// added by mingl 20050124
					if (lvDiffinMS > 2000)
						Log.println("[[(( processed with " + lvDiffinMS + "ms - AppletGateway: " + ivAppletID + " ip: " + ivAppletSocket.getInetAddress().getHostAddress(), Log.ALERT_LOG);
				}
			}
			else
			{
				synchronized (this)
				{
					lvReplyMessage = pMessage == null ? "" : pMessage;
					String lvReplyMessageArr[] = StringSplitter.split(lvReplyMessage, "|");
					String lvReplyMessageFunctionName = lvReplyMessageArr[0];

					if (lvReplyMessageFunctionName.equalsIgnoreCase(TopicName.TOPIC_HKSFO_ORDER_ENQUIRY))
					{
						if (!getClientRegistered())
						{
							setPendingOrderEnquiry(true);
						}
						else
						{
							if (getThreadIsSleeping())
							{
								setPendingOrderEnquiry(true);
							}
							else
							{
								ivPendingAJAXMessage.add(lvReplyMessage);
								setPendingOrderEnquiry(true);
								this.notify();
							}
						}
					}
					else
					{
						if (!getClientRegistered())
						{
							ivPendingAJAXMessage.add(lvReplyMessage);
						}
						else
						{
							ivPendingAJAXMessage.add(lvReplyMessage);
							this.notify();
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Log.println("Exception to IP: " + ivAppletSocket.getInetAddress().getHostAddress() + " end", Log.ALERT_LOG);
		}
	}

	/**
	 * This method to be invoke when the thread start.
	 */
	public void run()
	{
		String lvAppletRequest = null;
		if (ivMonitoringThread != null)
		{
			try
			{
				ivMonitoringThread.start();
			}
			catch (Exception e)
			{
				Log.println(e, Log.ERROR_LOG);
			}
		}
		try
		{
			ivAppletReader = new BufferedLineReader(new InputStreamReader(ivAppletSocket.getInputStream()), 512);
			ivAppletOutputStream = new BufferedOutputStream(ivAppletSocket.getOutputStream());
			while ((lvAppletRequest = ivAppletReader.readLine()) != null)
			{
				if (lvAppletRequest.equalsIgnoreCase("QUIT"))
				{
					break;
				}
				Vector lvRequest = split(lvAppletRequest, "|");
				if (!processMessage(lvRequest))
				{
					break;
				}
			}
		}
		catch (Exception e)
		{
			if (lvAppletRequest != null)
			{
				Log.println(lvAppletRequest, Log.ERROR_LOG);
			}
			Log.println(e, Log.ERROR_LOG);
		}
		finally
		{
			// added by mingl
			this.ivMonitoringThreadReadyToDead = true;
			removeAppletGateway(this);
			disconnect();
		}
		Log.println(this.toString() + " Exists", Log.ACCESS_LOG);
	}

	/**
	 * This method to register the specified client with id.
	 * @param pClientID the client id.
	 */
	public void registerClient(String pClientID)
	{
//		ivReplyMessage = null;
		Vector lvRequest = new Vector();
		lvRequest.add("REGISTERCLIENTID");
		lvRequest.add(pClientID);
		try
		{
			processMessage(lvRequest);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Log.println(e, Log.ERROR_LOG);
		}
	}

	/**
	 * This method to process the message.
	 * @param lvRequest the request message.
	 * @return the process is success or not.
	 * @throws Exception 
	 */
	private boolean processMessage(Vector lvRequest) throws Exception
	{
		String lvRequestName = (String) lvRequest.elementAt(0);
		String lvClientID = (String) lvRequest.elementAt(1);
		if (lvRequestName.equalsIgnoreCase("REGISTERCLIENTID")) // register
																// series
		{
			removeAppletGateway(this); // clear all reference of this gateway
										// because we only allow each applet
										// registers one series.
			Set lvAppletGatewaySet = (Set) svAppletGateWaySet_MapByClientID.get(lvClientID);
			if (lvAppletGatewaySet == null)
			{
				lvAppletGatewaySet = new HashSet();
				svAppletGateWaySet_MapByClientID.put(lvClientID, lvAppletGatewaySet);
			}
			lvAppletGatewaySet.add(this);
			return true;
		}
		else
		{
			throw new Exception("Unknown request was received from applet client: " + lvRequestName);
		}
	}

	/**
	 * This method to analyze the string and make it into a vector.
	 * @param pSource the string to be analyze.
	 * @param pDelimiter the delimiter.
	 * @return a string vector.
	 */
	private Vector split(String pSource, String pDelimiter)
	{
		Vector lvResult = new Vector();
		StringTokenizer lvST = new StringTokenizer(pSource, pDelimiter);
		while (lvST.hasMoreTokens())
		{
			lvResult.addElement(lvST.nextToken());
		}
		return lvResult;
	}

	/**
	 * This method to close connect.
	 */
	public void disconnect()
	{
		if (ivAppletReader != null)
		{
			try
			{
				ivAppletReader.close();
			}
			catch (Exception lvException)
			{
			}
			ivAppletReader = null;
		}
		if (ivAppletOutputStream != null)
		{
			try
			{
				ivAppletOutputStream.close();
			}
			catch (Exception lvException)
			{
			}
			ivAppletOutputStream = null;
		}
		if (ivAppletSocket != null)
		{
			try
			{
				ivAppletSocket.close();
			}
			catch (Exception lvException)
			{
			}
			ivAppletSocket = null;
		}
	}

	/**
	 * This method to compare the object.
	 * @param the object to be compared.
	 */
	public int compareTo(Object o)
	{
		AppletGateway obj = (AppletGateway) o;
		return obj.ivAppletID - ivAppletID;
	}

	/**
	 * This method compare this string with the specified object.
	 * @param the object to be compared.
	 */
	public boolean equals(Object obj)
	{
		AppletGateway gateway = (AppletGateway) obj;
		return (getAppletClientID() == gateway.getAppletClientID());
	}

	/**
	 * Returns a string representation of the object.
	 */
	public String toString()
	{
		return "AG " + getAppletClientID() + " - " + ivSocketString;
	}

	/**
	 * Sets the AppletGateway socket linger time.
	 * @param pAppletGatewaySocketLingerTime the AppletGateway socket linger time. 
	 */
	public static void setAppletGatewaySocketLingerTime(int pAppletGatewaySocketLingerTime)
	{
		svAppletGatewaySocketLingerTime = pAppletGatewaySocketLingerTime;
	}

	/**
	 * Sets the AppletGateway socket TimeOut time.
	 * @param pAppletGatewaySocketTimeOut the AppletGateway socket TimeOut time.
	 */
	public static void setAppletGatewaySocketTimeOut(int pAppletGatewaySocketTimeOut)
	{
		svAppletGatewaySocketTimeOut = pAppletGatewaySocketTimeOut;
	}

	/**
	 * Sets the AppletGateway socket keep alive.
	 * @param pAppletGatewaySocketKeepAlive the AppletGateway socket keep alive.
	 */
	public static void setAppletGatewaySocketKeepAlive(boolean pAppletGatewaySocketKeepAlive)
	{
		svAppletGatewaySocketKeepAlive = pAppletGatewaySocketKeepAlive;
	}

	/**
	 * Sets the AppletGateway socket linger.
	 * @param pAppletGatewaySocketLinger the AppletGateway socket linger.
	 */
	public static void setAppletGatewaySocketLinger(boolean pAppletGatewaySocketLinger)
	{
		svAppletGatewaySocketLinger = pAppletGatewaySocketLinger;
	}

	/**
	 * Returns the AppletGateway socket linger time. 
	 * @return The AppletGateway socket linger time.
	 */
	public static int getAppletGatewaySocketLingerTime()
	{
		return svAppletGatewaySocketLingerTime;
	}

	/**
	 * Returns the AppletGateway socket TimeOut time.
	 * @return The AppletGateway socket TimeOut time.
	 */
	public static int getAppletGatewaySocketTimeOut()
	{
		return svAppletGatewaySocketTimeOut;
	}

	/**
	 * Returns the AppletGateway socket keep alive. 
	 * @return The AppletGateway socket keep alive.
	 */
	public static boolean getAppletGatewaySocketKeepAlive()
	{
		return svAppletGatewaySocketKeepAlive;
	}

	/**
	 * Returns the AppletGateway socket linger.
	 * @return The AppletGateway socket linger.
	 */
	public static boolean getAppletGatewaySocketLinger()
	{
		return svAppletGatewaySocketLinger;
	}

	/**
	 * Sets whether the client is registered.
	 * @param pClientRegistered the client is register or not.
	 */
	public void setClientRegistered(boolean pClientRegistered) {
		ivClientRegistered = pClientRegistered;
	}

	/**
	 * Retruns the client is register or not. 
	 * @return The client is register or not.
	 */
	public boolean getClientRegistered() {
		return ivClientRegistered;
	}

	/**
	 * Sets whether the pending order is enquiry.
	 * @param pPendingOrderEnquiry the pending order is enquiry or not.
	 */
	private void setPendingOrderEnquiry(boolean pPendingOrderEnquiry)
   {
   		ivPendingOrderEnquiry = pPendingOrderEnquiry;
	}

	/**
	 * Returns the pending order is enquiry or not.
	 * @return The pending order is enquiry or not.
	 */
	private boolean getPendingOrderEnquiry()
   {
			return ivPendingOrderEnquiry;
	}

	/**
	 * Sets whether the thread is sleep.
	 * @param pThreadIsSleeping the thread is sleep or not. 
	 */
	public synchronized void setThreadIsSleeping(boolean pThreadIsSleeping) {
		ivThreadIsSleeping = pThreadIsSleeping;
	}

	/**
	 * Returns the thread is sleep or not. 
	 * @return The thread is sleep or not. 
	 */
	public boolean getThreadIsSleeping() {
		return ivThreadIsSleeping;
	}
		
}
