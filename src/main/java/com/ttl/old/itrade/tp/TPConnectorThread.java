package com.ttl.old.itrade.tp;

import com.ttl.old.itrade.util.Log;
/**
 * This class process the TP Connector thread
 * @author not attribute
 *
 */
public class TPConnectorThread extends Thread
{
	private TPManager ivTPManager = null;
	private TPConnector ivTPConnector = null;
	/**
	 * Default constructor for TPConnectorThread class
	 * @param pTPManager the TPManager class
	 * @param pTPConnector the TPConnector class
	 */
	public TPConnectorThread(TPManager pTPManager, TPConnector pTPConnector)
	{
		ivTPManager = pTPManager;
		ivTPConnector = pTPConnector;
	}
	/**
	 * This method for run TP connect thread
	 */
	public void run()
	{
		String lvReceivedMessage = "";

		while (true)
		{
			try
			{
				if (!ivTPConnector.isConnected())
				{
					if (!ivTPConnector.startConnect())
					{
						Thread.sleep(5000);
						continue;
					}
				}

				// added by mingl
				ivTPManager.onTPConnect();

				while (true)
				{
					lvReceivedMessage = ivTPConnector.receive(false);
					synchronized (ivTPManager.ivDataReceived)
					{
						ivTPManager.ivDataReceived.add(lvReceivedMessage);
						ivTPManager.ivDataReceived.notifyAll();
					}
				}
			}
			catch (Exception e1)
			{
				Log.println("TPConnector Thread throws " + e1.toString(), Log.ERROR_LOG);
				Log.println(e1, Log.ERROR_LOG);
				ivTPManager.svSeriesDownloaded = new Boolean(false);
			}
		}
	}
}
