package com.ttl.old.itrade.tp;

import java.io.FilterReader;
import java.io.IOException;
import java.io.Reader;

/**
 * New implementatio of readLine.
 * If the incoming data exceeds the limit, the underlying stream will close and null string is returned.
 */
public class BufferedLineReader extends FilterReader
{
	private int		ivLimit;
	private char[]  lineTerminator = System.getProperty("line.separator").toCharArray();

	/**
	 * Default constructor for BufferedLineReader class.
	 */
	public BufferedLineReader(Reader in, int pMaxCharPerLine)
	{
		super(in);
		ivLimit = pMaxCharPerLine + lineTerminator.length;
	}

	/**
	 * This method read a line of text.
	 * @return A string containing the lines.
	 * @throws IOException If an I / O error.
	 */
	public String readLine() throws IOException
	{
		StringBuffer	buf = new StringBuffer();
		char[]			save = new char[lineTerminator.length];
		int				pos = 0;
		int				ch = read();
		boolean			done = false;

		if (ch < 0)
		{
			return (null);

		}
		int lvCurrentRead = 0;

		do
		{
			if (ch == lineTerminator[pos])
			{
				save[pos] = (char) ch;
				pos++;
			}
			else
			{

				// if a char in the line terminator is returned
				// but one was skipped, then skip it by moving pos
				// up by two
				if (pos + 1 < lineTerminator.length && ch == lineTerminator[pos + 1])
				{
					pos += 2;
				}
				else
				{
					if (pos > 0)
					{
						buf.append(save, 0, pos);
						pos = 0;
					}

					buf.append((char) ch);
				}
			}

			done = pos >= lineTerminator.length;

			if (!done)
			{
				ch = read();
			}

			lvCurrentRead++;
			if (lvCurrentRead > ivLimit)
			{
				close();
				return null;
			}

			if (ch < 0)
			{
				done = true;
			}
		}
		while (!done);

		return (new String(buf));
	}
}
