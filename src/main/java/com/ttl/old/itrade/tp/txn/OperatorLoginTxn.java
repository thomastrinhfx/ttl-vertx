package com.ttl.old.itrade.tp.txn;

import java.util.Hashtable;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.systekit.common.msg.IMsgXMLNode;

/**
 * Title:        HSI Operator Login Transaction
 * Description:  This class stores and processes the HSI Operator Login transaction
 * Copyright:    Copyright (c) 2002
 * Company:      SYSTEK IT Ltd.
 * @author
 * @version 1.0
 * @since 2002.11.28
 */

public class OperatorLoginTxn {
	private String clientId;
	private int returnCode;
	private String errorCode;
	private String errorMessage;

	TPErrorHandling tpError;
	TPManager ivTPManager = null;

	private String mvRequestName;

	/**
	 * Default constructor for HSILogoutTxn class
	 */
	public OperatorLoginTxn(TPManager pTPManager, String pRequestName) {
		tpError = new TPErrorHandling();
		ivTPManager = pTPManager;
		mvRequestName = pRequestName;
	}

	/**
	 * Calling TP using a parameter Hashtable and a Request Name,
	 * and get the return code from TP
	 * @see TPErrorHandling
	 * @exception Exception e
	 */
	public void process() {
		try {
			Hashtable lvTxnMap = new Hashtable();
			IMsgXMLNode lvRetNode;
			TPBaseRequest lvHSIOperatorLoginRequest = ivTPManager
					.getRequest(mvRequestName);

			lvRetNode = lvHSIOperatorLoginRequest.send(lvTxnMap);

			setReturnCode(tpError.checkError(lvRetNode));
			if (returnCode != tpError.TP_NORMAL) {
				setErrorMessage(tpError.getErrDesc());
				if (returnCode == tpError.TP_APP_ERR) {
					setErrorCode(tpError.getErrCode());
				}
			} else {
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get method for Return Code
	 * 
	 * @return Return Code
	 */
	public int getReturnCode() {
		return returnCode;
	}

	/**
	 * Set method for Return Code
	 * 
	 * @param Return Code
	 */
	public void setReturnCode(int returnCode) {
		this.returnCode = returnCode;
	}

	/**
	 * Get method for Error Code
	 * 
	 * @return Error Code
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * Set method for Error Code
	 * 
	 * @param Error Code
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * Get method for Error Message
	 * 
	 * @return Error Message
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Set method for Error Message
	 * 
	 * @param Error Message
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
