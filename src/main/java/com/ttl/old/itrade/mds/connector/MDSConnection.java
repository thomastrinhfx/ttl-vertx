package com.ttl.old.itrade.mds.connector;

import com.ttl.old.itrade.mds.action.BaseAction;
import com.ttl.old.itrade.mds.model.ATTR_RequestModel;
import com.ttl.old.itrade.util.Log;
import com.ttl.wtrade.utils.WTradeLogger;
import com.txtech.mds.api.MdsClientAPI;
import com.txtech.mds.api.MdsMessageType;
import com.txtech.mds.api.listener.IMdsConnectionListener;
import com.txtech.mds.msg.MsgExchangeID;
import com.txtech.mds.msg.clientInterface.IMsgBaseMessage;
import com.txtech.mds.msg.clientInterface.api.MdsMarketDataListenerInterface;

import java.io.IOException;
import java.util.EnumSet;
import java.util.Iterator;

public class MDSConnection implements IMdsConnectionListener {

    // ---------------------------------------Enumerate the server status.
    public enum EServerStatus {
        OPEN, CONNECTING, CONNECTED, DROPPED, TERMINATED
    }

    // ---------------------------------------Enumerate the service type.
    enum RequestType {
        SNAPSHOT, STREAMING
    }

    // --------------------Enumerate the requested message type
    public enum RequestMsgType {
        PriceData, AggregateOrddrBook, TradeData, BrokerQueue, InstrumentStatus, MarketStatus, IndexData, InstrumentFundamental, Security_Definition;

        public EnumSet<MdsMessageType> mappingToMDSMsgTypeSet() {
            EnumSet<MdsMessageType> lvEnumSet = null;
            switch (this) {
                case PriceData:
                    lvEnumSet = EnumSet.of(MdsMessageType.NOMINAL_PRICE, MdsMessageType.CLOSING_PRICE,
                            MdsMessageType.STATISTICS);
                    break;
                case AggregateOrddrBook:
                    lvEnumSet = EnumSet.of(MdsMessageType.AGGREGATE_ORDER_BOOK);
                    break;
                case TradeData:
                    lvEnumSet = EnumSet.of(MdsMessageType.TRADE, MdsMessageType.TRADE_TICKER);
                    break;
                case BrokerQueue:
                    lvEnumSet = EnumSet.of(MdsMessageType.BROKER_QUEUE);
                    break;
                case InstrumentStatus:
                    lvEnumSet = EnumSet.of(MdsMessageType.SECURITY_STATUS);
                    break;
                case MarketStatus:
                    lvEnumSet = EnumSet.of(MdsMessageType.TRADING_SESSION_STATUS);
                    break;
                case IndexData:
                    lvEnumSet = EnumSet.of(MdsMessageType.INDEX_DATA);
                    break;
                case InstrumentFundamental:
                    lvEnumSet = EnumSet.of(MdsMessageType.SECURITY_FUNDAMENTAL);
                    break;
                case Security_Definition:
                    lvEnumSet = EnumSet.of(MdsMessageType.SECURITY_DEFINITION/*, MdsMessageType.BEST_BID_ASK*/);
            }
            return lvEnumSet;
        }
    }

    private static MDSConnection svService;
    private MdsMarketDataListenerInterface svListenner;
    private EServerStatus mvServerStatus;
    private IMsgBaseMessage mvMessage;
    private MdsClientAPI mvMDSClientAPI;

    public IMsgBaseMessage getMvMessage() {
        return mvMessage;
    }

    public void setMvMessage(IMsgBaseMessage mvMessage) {
        this.mvMessage = mvMessage;
    }

    public MdsMarketDataListenerInterface getSvListenner() {
        if (svListenner == null) {
            return new BaseAction();
        }
        return svListenner;
    }

    public void setSvListenner(MdsMarketDataListenerInterface svListenner) {
        this.svListenner = svListenner;
    }

    private static final MsgExchangeID C_DEF_EXCHANGE_ID = MsgExchangeID.HK;

    // Get instance function
    public static MDSConnection getInstance() {
        if (svService == null) {
            synchronized (MDSConnection.class) {
                if (svService == null) {
                    svService = new MDSConnection();
                    svService.init();
                }
            }
        }
        return svService;
    }

    // Init Function to set Server IP and Port for MDS
    private void init() {
        //String lvWhere = IMain.getProperty("MdsServerIP");
        //int lvPort = Integer.parseInt(IMain.getProperty("MdsServerPort"));
        //String lvInterval = IMain.getProperty("reconnect-interval");
        ////Log.print("--- MDS Server Init at Server IP: " + lvWhere + " == Port: " + lvPort + " == reconnect-interval: "
        //		+ lvInterval + ".", //Log.ACCESS_//Log);
        //mvMDSClientAPI = new MdsClientAPI(lvWhere, lvPort);
        try {
            mvMDSClientAPI = new MdsClientAPI("192.168.150.193", 9998);
            mvMDSClientAPI.setConnectionListener(new MDSConnection());
            mvMDSClientAPI.setReconnectInterval(Integer.parseInt("18000"));
            MDSConnection.getInstance().mvServerStatus = EServerStatus.CONNECTING;
        } catch (Exception e) {
            WTradeLogger.print("MDSConnection", "Exception in init, EServer will be dropped");
            MDSConnection.getInstance().mvServerStatus = EServerStatus.DROPPED;
        }
        Log.print("===================================\nComplete Init()", Log.ACCESS_LOG);
    }

    protected void buildListeners() {
        this.mvMDSClientAPI.addMarketDataListener(getSvListenner());
        this.mvServerStatus = EServerStatus.CONNECTED;
    }

    // Start MDS
    public void start(MdsMarketDataListenerInterface pListener) {
        this.setSvListenner(pListener);
        this.mvMDSClientAPI.start(false);
        this.buildListeners();
        Log.println("Start Complete, MDS Listener is: " + pListener.toString(), Log.ACCESS_LOG);
        WTradeLogger.print("MDSConnection","Start Complete, MDS Listener is: " + pListener.toString());
    }

    // Shutdown MDS
    public void shutdown() {
        try {
            this.mvMDSClientAPI.shutdown();
        } catch (IOException e) {
            Log.println("Exception ==> Shutdown MDS: " + e.getMessage(), Log.ACCESS_LOG);
        }
    }

    // --------------------------------------------------- Subscribe mvMDSClientAPI

    /**
     * @param pType
     * @param exchangeID
     * @param key        A must ,not optional.
     *                   <ol>
     *                   <li>Stock code such as 1(VN30F1803 for HKEX),8(VN30F1803 for
     *                   HKEX),339(VN30F1803 for HKEX),600005(for MAMK) For all stock codes
     *                   included ,use "ALL_KEY" instead.</li>
     *                   <li>(Pending...)</li>
     *                   </ol>
     */
    public void subscribe(RequestMsgType pType, final MsgExchangeID exchangeID, final String key) {

        MsgConvertUtil.handle(pType, new MDSMsgTypeProcessor() {

            public void process(MdsMessageType pType) {
                if (mvMDSClientAPI.isSubscribed(exchangeID, pType, key)) {
                    mvMDSClientAPI.unsubscribe(pType, exchangeID, key);
                    //return;
                    WTradeLogger.print("MDSConnection",
                            String.format("Found duplicate ==> Re-subscribe: %s |id: %s |key: %s |",
                                    pType.toString(),
                                    exchangeID.toString(),
                                    key));
//                    System.out.println("Re subsribe: " + pType.toString() + " id: " + exchangeID.toString() + " key: " + key);
                    mvMDSClientAPI.subscribe(pType, exchangeID, key);
                    return;
                }
                WTradeLogger.print("MDSConnection",
                        String.format("Subscribe: %s |id: %s |key: %s |",
                                pType.toString(),
                                exchangeID.toString(),
                                key));

//                System.out.println(" subsribe: " + pType.toString() + " id: " + exchangeID.toString() + " key: " + key);
                mvMDSClientAPI.subscribe(pType, exchangeID, key);
            }
        });
    }

    public void subscribe(MdsMessageType pType, final MsgExchangeID exchangeID, final String key) {

        System.out.println(pType.toString());
        System.out.println(key.toString() + "exchange id " + exchangeID);
        mvMDSClientAPI.subscribe(pType, exchangeID, key);

        // MsgConvertUtil.handle(pType, new MDSMsgTypeProcessor()
        // {
        //
        // public void process(MdsMessageType pType)
        // {
        // if(mvMDSClientAPI.isSubscribed(exchangeID, pType, key)){
        // return;
        // }
        //
        // System.out.println("subsribe: "+ pType.toString() + " id "
        // +exchangeID.toString() + " key: "+ key);
        // mvMDSClientAPI.subscribe(pType, exchangeID, key);
        // }
        // });
    }

    /**
     * @param pType
     * @param key   A must ,not optional.
     */
    public void subscribe(RequestMsgType pType, final String key) {
        this.subscribe(pType, C_DEF_EXCHANGE_ID, key);
    }

    public void subscribe(ATTR_RequestModel pModel) {
        // Null or not ?
        pModel.getRequestType().toString();
        pModel.getKey().toString();

        final MsgExchangeID lvExchangeID = pModel.getExchangeID();
        final String lvInputKey = pModel.getKey();
        MsgConvertUtil.handle(pModel.getRequestMsgType(), new MDSMsgTypeProcessor() {
            public void process(MdsMessageType pType) {
                if (mvMDSClientAPI.isSubscribed(lvExchangeID, pType, lvInputKey)) {
                    return;
                }
                mvMDSClientAPI.subscribe(pType, lvExchangeID, lvInputKey);
            }
        });
    }

    // -----------------------------------------------------Unsubscribe

    /**
     * @param pType
     * @param exchangeID
     * @param key        A must ,not optional.
     */
    public void unsubscribe(RequestMsgType pType, final MsgExchangeID exchangeID, final String key) {
        // Null or not ?
        pType.toString();
        key.toString();

        MsgConvertUtil.handle(pType, new MDSMsgTypeProcessor() {

            public void process(MdsMessageType pType) {
                mvMDSClientAPI.unsubscribe(pType, exchangeID, key);
            }
        });
    }

    /**
     * @param pType
     * @param key   A must ,not optional.
     */
    public void unsubscribe(RequestMsgType pType, final String key) {
        this.unsubscribe(pType, C_DEF_EXCHANGE_ID, key);
    }

    public void unsubscribe(ATTR_RequestModel pModel) {
        // Null or not ?mvServerStatus
        pModel.getRequestMsgType().toString();
        pModel.getKey().toString();

        final MsgExchangeID lvExchangeID = pModel.getExchangeID();
        final String lvInputKey = pModel.getKey();
        MsgConvertUtil.handle(pModel.getRequestMsgType(), new MDSMsgTypeProcessor() {

            public void process(MdsMessageType pType) {
                mvMDSClientAPI.unsubscribe(pType, lvExchangeID, lvInputKey);
            }
        });
    }

    /**
     * @author jay.wince
     * @since 20141022
     */
    static class MsgConvertUtil {
        private static void handle(RequestMsgType pType, MDSMsgTypeProcessor pProcessor) {
            Iterator<MdsMessageType> lvIterator = pType.mappingToMDSMsgTypeSet().iterator();
            // 1.If the server is not ready,just do nothing.
            Object a = MDSConnection.getInstance().mvServerStatus;
            Object y = EServerStatus.CONNECTED;
            if (MDSConnection.getInstance().mvServerStatus != EServerStatus.CONNECTED) {
                return;
            }
            while (lvIterator.hasNext()) {
                MdsMessageType mdsMessageType = (MdsMessageType) lvIterator.next();
                pProcessor.process(mdsMessageType);
            }
        }
    }

    /**
     * @author jay.wince
     * @since 20141022MDSConnection
     */
    interface MDSMsgTypeProcessor {
        public void process(MdsMessageType pType);
    }

    /**************************************************************************************************************
     * The status report for the MDI server.
     **************************************************************************************************************
     */

    public void onConnectFailed() {
        this.mvServerStatus = EServerStatus.OPEN;
        //Log.println("Fail when trying to build the connection btw ITrade and MDI", //Log.ACCESS_//Log);
    }

    public void onConnected() {
        this.mvServerStatus = EServerStatus.CONNECTED;
        //Log.println("Succeed when trying to build the connection btw ITrade and MDI", //Log.ACCESS_//Log);
    }

    public void onConnecting() {
        this.mvServerStatus = EServerStatus.CONNECTING;
        //Log.println("Connecting when trying to build the connection btw ITrade and MDI", //Log.ACCESS_//Log);
    }

    public void onConnectionDropped(Throwable paramThrowable) {
        this.mvServerStatus = EServerStatus.DROPPED;
        //Log.println("The connection btw ITrade and MDI is dropped!The error is : " + paramThrowable, //Log.ACCESS_//Log);
    }

    public void onTerminated() {
        this.mvServerStatus = EServerStatus.TERMINATED;
        //Log.println("The sever is terminated!", //Log.ACCESS_//Log);
    }

    public EServerStatus ServerStatus() {
        return this.mvServerStatus;
    }

}
