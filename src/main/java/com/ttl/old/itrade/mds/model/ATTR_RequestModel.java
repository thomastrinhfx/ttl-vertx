package com.ttl.old.itrade.mds.model;

import com.ttl.old.itrade.mds.connector.MDSConnection;
import com.txtech.mds.msg.MsgExchangeID;

/**
 * @author jay.wince
 * @since  20141022
 */
public class ATTR_RequestModel
{
 
public enum ERequestType{
        SNAPSHOT,
        STREAMING
    }
    
// -------------Constants    
    private static final ERequestType   C_DEF_REQ_TYPE            = ERequestType.SNAPSHOT;
    private static final MsgExchangeID C_DEF_EXCHANGE_ID          = MsgExchangeID.HK;    
    
// ------------------------------- Member variables    
    private ERequestType                                             mvRequestType;    
    private MDSConnection.RequestMsgType mvType;
    private MsgExchangeID                                             mvExchangeID;
    private String                                                           mvKey;

    
    
// -------------------------------- Constrctors
    public ATTR_RequestModel(){}
    
    public ATTR_RequestModel(MDSConnection.RequestMsgType pType){this.mvType=pType;}
// -------------------------------- Setters and Getters    
    public MDSConnection.RequestMsgType getRequestMsgType()
    {
        return mvType;
    }
    public void setRequestMsgType(MDSConnection.RequestMsgType pType)
    {
        this.mvType = pType;
    }
    public MsgExchangeID getExchangeID()
    {
        if(this.mvExchangeID==null) return C_DEF_EXCHANGE_ID;
        return mvExchangeID;
    }
    public void setExchangeID(MsgExchangeID pExchangeID)
    {
        this.mvExchangeID = pExchangeID;
    }
    public String getKey()
    {
        return mvKey;
    }
    public void setKey(String pKey)
    {
        this.mvKey = pKey;
    }
    
    public ERequestType getRequestType(){
        if(this.mvRequestType==null) return C_DEF_REQ_TYPE;
        return this.mvRequestType;
    }
    
    public void setRequestType(ERequestType pType){
       this.mvRequestType = pType;   
    }
}
