package com.ttl.old.itrade.mds.action;

import com.txtech.mds.msg.clientInterface.IMsgBaseAggregateOrderBook;
import com.txtech.mds.msg.clientInterface.IMsgBaseBestBidAsk;
import com.txtech.mds.msg.clientInterface.IMsgBaseBrokerQueue;
import com.txtech.mds.msg.clientInterface.IMsgBaseClosingPrice;
import com.txtech.mds.msg.clientInterface.IMsgBaseCurrencyRate;
import com.txtech.mds.msg.clientInterface.IMsgBaseEquilibriumPrice;
import com.txtech.mds.msg.clientInterface.IMsgBaseFullOrderBook;
import com.txtech.mds.msg.clientInterface.IMsgBaseIndexData;
import com.txtech.mds.msg.clientInterface.IMsgBaseIndexDefinition;
import com.txtech.mds.msg.clientInterface.IMsgBaseLiquidityProvider;
import com.txtech.mds.msg.clientInterface.IMsgBaseMarketDefinition;
import com.txtech.mds.msg.clientInterface.IMsgBaseMarketTurnover;
import com.txtech.mds.msg.clientInterface.IMsgBaseNews;
import com.txtech.mds.msg.clientInterface.IMsgBaseNominalPrice;
import com.txtech.mds.msg.clientInterface.IMsgBaseOddLotOrderBook;
import com.txtech.mds.msg.clientInterface.IMsgBaseOpenInterest;
import com.txtech.mds.msg.clientInterface.IMsgBaseOrderImbalance;
import com.txtech.mds.msg.clientInterface.IMsgBaseQuoteRequestInformation;
import com.txtech.mds.msg.clientInterface.IMsgBaseReferencePrice;
import com.txtech.mds.msg.clientInterface.IMsgBaseSecurityDefinition;
import com.txtech.mds.msg.clientInterface.IMsgBaseSecurityDefinitionLite;
import com.txtech.mds.msg.clientInterface.IMsgBaseSecurityFinancialReport;
import com.txtech.mds.msg.clientInterface.IMsgBaseSecurityFundamental;
import com.txtech.mds.msg.clientInterface.IMsgBaseSecurityStatus;
import com.txtech.mds.msg.clientInterface.IMsgBaseStatistics;
import com.txtech.mds.msg.clientInterface.IMsgBaseTrade;
import com.txtech.mds.msg.clientInterface.IMsgBaseTradeCancel;
import com.txtech.mds.msg.clientInterface.IMsgBaseTradingSessionStatus;
import com.txtech.mds.msg.clientInterface.IMsgBaseVcmTrigger;
import com.txtech.mds.msg.clientInterface.IMsgBaseYield;
import com.txtech.mds.msg.clientInterface.IMsgMdsConnectionStatus;
import com.txtech.mds.msg.clientInterface.IMsgMdsResponse;
import com.txtech.mds.msg.clientInterface.api.MdsMarketDataListenerInterface;

public class BaseAction implements MdsMarketDataListenerInterface {
	int i=0;
	//------------ Received Method listening data
	@Override
	public void onReceived(IMsgMdsConnectionStatus arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgMdsResponse arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseAggregateOrderBook arg0) {
		StockInfoMdstxn txn = StockInfoMdstxn.getInstance();
		txn.setMvArrgregateOrderBook(arg0);
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseBestBidAsk arg0) {
		StockInfoMdstxn txn = StockInfoMdstxn.getInstance();
		txn.setMvBestBidAsk(arg0);
	}

	@Override
	public void onReceived(IMsgBaseBrokerQueue arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseCurrencyRate arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseClosingPrice arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseEquilibriumPrice arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseFullOrderBook arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseIndexData arg0) {
		StockInfoMdstxn.getInstance().setMvIMsgBaseIndexData(arg0);
	}

	@Override
	public void onReceived(IMsgBaseIndexDefinition arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseLiquidityProvider arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseMarketDefinition arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseMarketTurnover arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseNews arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseNominalPrice arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseOddLotOrderBook arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseOpenInterest arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseOrderImbalance arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseQuoteRequestInformation arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseReferencePrice arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseSecurityDefinition arg0) {
		StockInfoMdstxn.getInstance().setMvSecurityDifinition(arg0);
	}

	@Override
	public void onReceived(IMsgBaseSecurityDefinitionLite arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseSecurityFinancialReport arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseSecurityFundamental arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseSecurityStatus arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseStatistics arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseTrade arg0) {
		// TODO Auto-generated method stubx
		
	}

	@Override
	public void onReceived(IMsgBaseTradeCancel arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseTradingSessionStatus arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseVcmTrigger arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceived(IMsgBaseYield arg0) {
		// TODO Auto-generated method stub
		
	}
}
