package com.ttl.old.itrade.mds.bean;

import java.math.BigDecimal;

public class mdsIndexDataBean {
	private String mvIDIndex;
	private String mvIndexCode;
	private BigDecimal mvValue;
	private String mvCallTime;
	private BigDecimal mvChange;
	private BigDecimal mvRatioChange;
	private BigDecimal mvTotalQtty;
	private BigDecimal mvTotalValue;
	private String mvTraddingDate;
	private String mvCurrentStatus;
	private BigDecimal mvTotalStock;
	private BigDecimal mvPriorIndexVal;
	private BigDecimal mvHighestIndex;
	private BigDecimal mvLowestIndex;
	private BigDecimal mvCloseIndex;
	private String mvTypeIndex;
	private String mvIndexName;
	public String getMvIDIndex() {
		return mvIDIndex;
	}
	public void setMvIDIndex(String mvIDIndex) {
		this.mvIDIndex = mvIDIndex;
	}
	public String getMvIndexCode() {
		return mvIndexCode;
	}
	public void setMvIndexCode(String mvIndexCode) {
		this.mvIndexCode = mvIndexCode;
	}
	public BigDecimal getMvValue() {
		return mvValue;
	}
	public void setMvValue(BigDecimal mvValue) {
		this.mvValue = mvValue;
	}
	public String getMvCallTime() {
		return mvCallTime;
	}
	public void setMvCallTime(String mvCallTime) {
		this.mvCallTime = mvCallTime;
	}
	public BigDecimal getMvChange() {
		return mvChange;
	}
	public void setMvChange(BigDecimal mvChange) {
		this.mvChange = mvChange;
	}
	public BigDecimal getMvRatioChange() {
		return mvRatioChange;
	}
	public void setMvRatioChange(BigDecimal mvRatioChange) {
		this.mvRatioChange = mvRatioChange;
	}
	public BigDecimal getMvTotalQtty() {
		return mvTotalQtty;
	}
	public void setMvTotalQtty(BigDecimal mvTotalQtty) {
		this.mvTotalQtty = mvTotalQtty;
	}
	public BigDecimal getMvTotalValue() {
		return mvTotalValue;
	}
	public void setMvTotalValue(BigDecimal mvTotalValue) {
		this.mvTotalValue = mvTotalValue;
	}
	public String getMvTraddingDate() {
		return mvTraddingDate;
	}
	public void setMvTraddingDate(String mvTraddingDate) {
		this.mvTraddingDate = mvTraddingDate;
	}
	public String getMvCurrentStatus() {
		return mvCurrentStatus;
	}
	public void setMvCurrentStatus(String mvCurrentStatus) {
		this.mvCurrentStatus = mvCurrentStatus;
	}
	public BigDecimal getMvTotalStock() {
		return mvTotalStock;
	}
	public void setMvTotalStock(BigDecimal mvTotalStock) {
		this.mvTotalStock = mvTotalStock;
	}
	public BigDecimal getMvPriorIndexVal() {
		return mvPriorIndexVal;
	}
	public void setMvPriorIndexVal(BigDecimal mvPriorIndexVal) {
		this.mvPriorIndexVal = mvPriorIndexVal;
	}
	public BigDecimal getMvHighestIndex() {
		return mvHighestIndex;
	}
	public void setMvHighestIndex(BigDecimal mvHighestIndex) {
		this.mvHighestIndex = mvHighestIndex;
	}
	public BigDecimal getMvLowestIndex() {
		return mvLowestIndex;
	}
	public void setMvLowestIndex(BigDecimal mvLowestIndex) {
		this.mvLowestIndex = mvLowestIndex;
	}
	public BigDecimal getMvCloseIndex() {
		return mvCloseIndex;
	}
	public void setMvCloseIndex(BigDecimal mvCloseIndex) {
		this.mvCloseIndex = mvCloseIndex;
	}
	public String getMvTypeIndex() {
		return mvTypeIndex;
	}
	public void setMvTypeIndex(String mvTypeIndex) {
		this.mvTypeIndex = mvTypeIndex;
	}
	public String getMvIndexName() {
		return mvIndexName;
	}
	public void setMvIndexName(String mvIndexName) {
		this.mvIndexName = mvIndexName;
	}
	
}
