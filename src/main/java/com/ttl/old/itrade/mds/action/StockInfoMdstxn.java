package com.ttl.old.itrade.mds.action;

import com.ttl.old.itrade.ITradeAtmosphereAPI;
import com.ttl.old.itrade.mds.bean.mdsIndexDataBean;
import com.ttl.old.itrade.mds.bean.mdsStockInfoBean;
import com.ttl.wtrade.utils.WTradeLogger;
import com.txtech.mds.msg.clientInterface.IMsgBaseAggregateOrderBook;
import com.txtech.mds.msg.clientInterface.IMsgBaseAggregateOrderBook.Entry;
import com.txtech.mds.msg.clientInterface.IMsgBaseBestBidAsk;
import com.txtech.mds.msg.clientInterface.IMsgBaseIndexData;
import com.txtech.mds.msg.clientInterface.IMsgBaseSecurityDefinition;
import io.vertx.core.Vertx;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;

import java.util.List;

public class StockInfoMdstxn {

    private mdsStockInfoBean mvStockInfo;
    private mdsIndexDataBean mvIndexDataBean;
    private IMsgBaseSecurityDefinition mvSecurityDifinition;
    private IMsgBaseAggregateOrderBook mvAggregateOrderBook;
    private IMsgBaseIndexData mvIMsgBaseIndexData;
    private IMsgBaseBestBidAsk mvBestBidAsk;

    private SharedData mvSharedDataStockInfo;
    private MdsGetKeyProtocol mvMdsGetByKey;

    private volatile static LocalMap<String, String> mvStockSessionMap = null; // mapping Stock code and SessionId List
    private String mvStockCode;
    private volatile static StockInfoMdstxn instance = null;
    private static Vertx vertx = null;

    public static void setVertx(Vertx pVertx) {
        vertx = pVertx;
    }

    private StockInfoMdstxn() {
        this.mvSharedDataStockInfo = null;
    }

    public static StockInfoMdstxn getInstance() {
        if (instance == null) {
            synchronized (StockInfoMdstxn.class) {
                if (instance == null) {
                    instance = new StockInfoMdstxn();
                }
            }
        }
        return instance;
    }

    public StockInfoMdstxn(IMsgBaseSecurityDefinition pvSecurityDifinition) {
        // this.mvSecurityDifinition = pvSecurityDifinition;
        // this.mvMdsGetByKey = new MdsGetKeyProtocol(mvSharedDataStockInfo.toString());
        // mvStockInfo = new mdsStockInfoBean();
        // this.mvStockCode = this.mvSecurityDifinition.getKey()[0];
        // this.mvSharedDataStockInfo = Vertx.vertx().sharedData();
        // mvStockSessionMap = this.mvSharedDataStockInfo.getLocalMap("StockInfoMap");
        // Log.println("Start push stockinfo to Client for instrument:"+
        // this.mvStockCode +"===Data: "+ this.mvSecurityDifinition , Log.ACCESS_LOG);
    }

    public void DecodeMessageSecurityDifinition() {
        // Log.println("Start push stockinfo to Client for instrument:"+
        // this.mvStockCode +"===Data: "+ this.mvSecurityDifinition , Log.ACCESS_LOG);

        mvStockInfo = new mdsStockInfoBean();
        this.mvStockCode = this.mvSecurityDifinition.getKey()[0];
        // this.mvSharedDataStockInfo = Vertx.vertx().sharedData();
        // setMvStockSessionMap(this.mvSharedDataStockInfo.getLocalMap(this.mvStockCode));
        // LocalMap<String, String> mvStoc = mvSharedDataStockInfo.getLocalMap("AAA");

        this.mvMdsGetByKey = new MdsGetKeyProtocol(this.mvSecurityDifinition.toString());

        this.mvStockInfo.setMvStockCode(mvStockCode);
        this.mvStockInfo.setMvMarket(this.mvSecurityDifinition.getMarketCode());
        this.mvStockInfo.setMvCeiling(this.mvMdsGetByKey.getKey(332));
        this.mvStockInfo.setMvFloor(this.mvMdsGetByKey.getKey(333));
        this.mvStockInfo.setMvReferences(this.mvMdsGetByKey.getKey(260));
        this.mvStockInfo.setMvOpen(this.mvMdsGetByKey.getKey(137));
        this.mvStockInfo.setMvHigh(this.mvMdsGetByKey.getKey(266));
        this.mvStockInfo.setMvLow(this.mvMdsGetByKey.getKey(2661));
        // this.mvStockInfo.setMvNomial(this.mvMdsGetByKey.getKey(--));
        this.mvStockInfo.setMvMatchPrice(this.mvMdsGetByKey.getKey(31));
        this.mvStockInfo.setMvMatchVol(this.mvMdsGetByKey.getKey(32));
        if ((!this.mvStockInfo.getMvReferences().equals("-") && !this.mvStockInfo.getMvMatchPrice().equals("-"))) {
            if (Integer.parseInt(this.mvStockInfo.getMvReferences()) != 0
                    && Integer.parseInt(this.mvStockInfo.getMvMatchPrice()) != 0) {
                this.mvStockInfo.setMvMatchUpDown(String.valueOf(Integer.parseInt(this.mvStockInfo.getMvMatchPrice())
                        - Integer.parseInt(this.mvStockInfo.getMvReferences())));

            }
        }
        this.mvStockInfo.setMvMatchVolTotal(this.mvMdsGetByKey.getKey(391));
        this.mvStockInfo.setMvForeignForBuy(this.mvMdsGetByKey.getKey(397));
        this.mvStockInfo.setMvForeignForSell(this.mvMdsGetByKey.getKey(398));
    }

    public void DecodeMessageAggregateOrderBook() {
        this.mvStockInfo = new mdsStockInfoBean();
        this.mvStockCode = this.mvAggregateOrderBook.getKey()[0];
        this.mvStockInfo.setMvStockCode(this.mvStockCode);
        this.mvStockInfo.setMvStockCode(mvStockCode);
        this.mvStockInfo.setMvMarket(this.mvAggregateOrderBook.getExchangeID().toString());
        // this.mvSharedDataStockInfo = Vertx.vertx().sharedData();
        // setMvStockSessionMap(this.mvSharedDataStockInfo.getLocalMap(this.mvStockCode));
        List<? extends Entry> lstAsk = this.mvAggregateOrderBook.getAskQueue();
        List<? extends Entry> lstBid = this.mvAggregateOrderBook.getBidQueue();
        this.mvMdsGetByKey = new MdsGetKeyProtocol(this.mvAggregateOrderBook.toString());
        int i = Integer.parseInt(mvMdsGetByKey.getKey(555).trim());
        if (i > 0) {
            try {
                this.mvStockInfo.setMvBidPrice1(lstBid.get(0).getPrice().getValue());
                this.mvStockInfo.setMvBidVol1(lstBid.get(0).getAggregateQuantity().longValueExact());
            } catch (Exception e) {
            }
            try {
                this.mvStockInfo.setMvOfferPrice1(lstAsk.get(0).getPrice().getValue());
                this.mvStockInfo.setMvOfferVol1(lstAsk.get(0).getAggregateQuantity().longValueExact());
            } catch (Exception e) {
            }
            if (i > 1) {
                try {
                    this.mvStockInfo.setMvBidPrice2(lstBid.get(1).getPrice().getValue());
                    this.mvStockInfo.setMvBidVol2(lstBid.get(1).getAggregateQuantity().longValueExact());
                } catch (Exception e) {
                }
                try {
                    this.mvStockInfo.setMvOfferPrice2(lstAsk.get(1).getPrice().getValue());
                    this.mvStockInfo.setMvOfferVol2(lstAsk.get(1).getAggregateQuantity().longValueExact());
                } catch (Exception e) {
                }
                if (i > 2) {
                    try {
                        this.mvStockInfo.setMvBidPrice3(lstBid.get(2).getPrice().getValue());
                        this.mvStockInfo.setMvBidVol3(lstBid.get(2).getAggregateQuantity().longValueExact());
                    } catch (Exception e) {
                    }
                    try {
                        this.mvStockInfo.setMvOfferPrice3(lstAsk.get(2).getPrice().getValue());
                        this.mvStockInfo.setMvOfferVol3(lstAsk.get(2).getAggregateQuantity().longValueExact());
                    } catch (Exception e) {
                    }
                    return;
                }
                return;
            }
            return;
        }

    }

    public void DecodeMessageBaseIndexData() {
        this.mvIndexDataBean.setMvIDIndex(this.mvIMsgBaseIndexData.getKey()[0]);
        this.mvIndexDataBean.setMvIndexCode(this.mvIMsgBaseIndexData.getIndexCode());
        //this.mvIndexDataBean.setMvValue(this.mvIMsgBaseIndexData.get);
        this.mvIndexDataBean.setMvCallTime(this.mvIMsgBaseIndexData.getIndexTime().toString());
        //this.mvIndexDataBean.setMvChange(this.mvIMsgBaseIndexData.get);
    }

    // ==================Get/Set Mothod============
    // public HKSStockInfoBean getMvStockInfo() {
    // return mvStockInfo;
    // }
    // public void setMvStockInfo(HKSStockInfoBean mvStockInfo) {
    // this.mvStockInfo = mvStockInfo;
    // }
    // public String getMvStockCode() {
    // return mvStockCode;
    // }
    // public void setMvStockCode(String mvStockCode) {
    // this.mvStockCode = mvStockCode;
    // }

    public IMsgBaseSecurityDefinition getMvSecurityDifinition() {
        return mvSecurityDifinition;
    }

    public void setMvSecurityDifinition(IMsgBaseSecurityDefinition mvSecurityDifinition) {
        this.mvSecurityDifinition = mvSecurityDifinition;
        sendStockInfo();
    }

    public SharedData getMvSharedDataStockInfo() {
        return mvSharedDataStockInfo;
    }

    public void setMvSharedDataStockInfo(SharedData mvSharedDataStockInfo) {
        this.mvSharedDataStockInfo = mvSharedDataStockInfo;
    }

	/*
     * public LocalMap<String, List<ITradeUser>> getMvStockSessionMap() { return
	 * mvStockSessionMap;result
	 * 
	 * }
	 * 
	 * 
	 * public void setMvStockSessionMap(LocalMap<String, List<ITradeUser>>
	 * mvStockSessionMap) { this.mvStockSessionMap = mvStockSessionMap; }
	 */

	/*
	 * public boolean putNewStockSession(String pvStockCode, ITradeUser user) {
	 * 
	 * List<ITradeUser> list = null; try {
	 * if(this.mvStockSessionMap.get(pvStockCode)!=null) { list =
	 * mvStockSessionMap.get(pvStockCode); list.add(user);
	 * mvStockSessionMap.replace(pvStockCode, list);
	 * mvSharedDataStockInfo.getLocalMap("StockInfoMap").replace(pvStockCode, list);
	 * return true; }else { list.add(user); mvStockSessionMap.put(pvStockCode,list);
	 * mvSharedDataStockInfo.getLocalMap("StockInfoMap").replace(pvStockCode, list);
	 * return true; } }catch(Exception e) { return false; } }
	 * 
	 * private List getListItradeUser(String key) { try { return
	 * this.mvStockSessionMap.get(key); }catch(Exception e ) { return null; } }
	 */

    public void sendStockInfo() {
        DecodeMessageSecurityDifinition();
        ITradeAtmosphereAPI api = ITradeAtmosphereAPI.getInstance();
        String strUser = this.getMvStockSessionMap().get(this.mvStockCode);

        if (strUser == null) {
            return;
        }
        char csplit = '\u0001';
        String[] listUser = strUser.split(String.valueOf(csplit));
        for (String iTradeUser : listUser) {
            api.pushTopic(iTradeUser, "StockInfo", this.mvStockInfo);
        }
        // for (String iTradeUser : listUser ) {
        // api.pushTopic(iTradeUser, "", this.mvStockInfo);
        // }
    }

    @SuppressWarnings({"static-access", "static-access"})
    public void sendBidAsk() {
        // DecodeMessageBestBidAsk();
        ITradeAtmosphereAPI api = ITradeAtmosphereAPI.getInstance();

        String strUser = this.getMvStockSessionMap().get(this.mvStockCode);
        if (strUser == null) {
            return;
        }
        char csplit = '\u0001';
        String[] listUser = strUser.split(String.valueOf(csplit));

        for (String iTradeUser : listUser) {
            api.pushTopic(iTradeUser, "", this.mvStockInfo);
        }
    }

    public void sendBaseIndexData() {
        DecodeMessageBaseIndexData();
        ITradeAtmosphereAPI api = ITradeAtmosphereAPI.getInstance();

        String strUser = this.getMvStockSessionMap().get("_ALL");
        if (strUser == null) {
            return;
        }
        char csplit = '\u0001';
        String[] listUser = strUser.split(String.valueOf(csplit));

        for (String iTradeUser : listUser) {
            api.pushTopic(iTradeUser, "HNXIndexData", this.mvIndexDataBean);
        }
    }

    public void sendAggregateOrddrBook() {
        DecodeMessageAggregateOrderBook();
        ITradeAtmosphereAPI api = ITradeAtmosphereAPI.getInstance();

        String strUser = this.getMvStockSessionMap().get(this.mvStockCode);
        if (strUser == null) {
            return;
        }
        char csplit = '\u0001';
        String[] listUser = strUser.split(String.valueOf(csplit));

        for (String iTradeUser : listUser) {
            api.pushTopic(iTradeUser, "AggregateOrddrBook", this.mvStockInfo);
        }
    }

    public IMsgBaseBestBidAsk getMvBestBidAsk() {
        return mvBestBidAsk;
    }

    public void setMvBestBidAsk(IMsgBaseBestBidAsk mvBestBidAsk) {
        this.mvBestBidAsk = mvBestBidAsk;
        sendBidAsk();
    }

    public void setMvArrgregateOrderBook(IMsgBaseAggregateOrderBook mvAggregateOrderBook) {
        WTradeLogger.print(this.getClass().getSimpleName(), "Received aggregate order book from MDS\n\t " + mvAggregateOrderBook);
        this.mvAggregateOrderBook = mvAggregateOrderBook;
        sendAggregateOrddrBook();

    }

    public static LocalMap<String, String> getMvStockSessionMap() {
        if (mvStockSessionMap == null) {
            mvStockSessionMap = vertx.sharedData().getLocalMap("_");
        }
        return mvStockSessionMap;
    }

    public static void setMvStockSessionMap(LocalMap<String, String> mvStockSessionMap) {
        StockInfoMdstxn.mvStockSessionMap = mvStockSessionMap;
    }

    public static void addMvStockSessionMap(String key, String value) {
        LocalMap<String, String> listdata = mvStockSessionMap;
        if (listdata != null) {
            System.out.println(listdata.get(key));

            String listusr = listdata.get(key);
            if (listusr == null) {
                listusr = value;
                listdata.put(key, listusr);

            } else {
                if (listusr.indexOf(value) < 0) {
                    char csplit = '\u0001';
                    listusr += csplit + value;
                    listusr.trim();
                    listdata.put(key, listusr);
                    System.out.println("List data not null value not null add new:" + listusr);
                }
            }
        } else {
            StockInfoMdstxn.getInstance().getMvStockSessionMap().put(key, value);
        }
    }

    public mdsIndexDataBean getMvIndexDataBean() {
        return mvIndexDataBean;
    }

    public void setMvIndexDataBean(mdsIndexDataBean mvIndexDataBean) {
        this.mvIndexDataBean = mvIndexDataBean;
    }

    public IMsgBaseIndexData getMvIMsgBaseIndexData() {
        return mvIMsgBaseIndexData;
    }

    public void setMvIMsgBaseIndexData(IMsgBaseIndexData mvIMsgBaseIndexData) {
        this.mvIMsgBaseIndexData = mvIMsgBaseIndexData;
        sendBaseIndexData();
    }

}
