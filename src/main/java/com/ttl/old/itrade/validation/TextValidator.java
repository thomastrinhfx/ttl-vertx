package com.ttl.old.itrade.validation;

/**
 * <p>
 * Title: Text validation
 * </p>
 * <p>
 * Description: The TextValidator class defined methods to set the
 * validation rules for text.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class TextValidator extends DefaultFieldValidator {
	/**
	 * Constructor for TextValidator class
	 * 
	 * @param pFieldName
	 *            The field name
	 * @param pFieldValue
	 *            The field value
	 */
	public TextValidator(String pFieldName, String pFieldValue) {
		super(pFieldName, pFieldValue);
		if (pFieldValue == null) {
			return;
		}
		char cLetters[] = pFieldValue.toCharArray();

		// Can only verify the first letter in case there are abbreviations
		// or names with hyphens.
		_bIsValid = Character.isLetter(cLetters[0]);
	}

	/**
	 * A static method for getting error code
	 * 
	 * @return 2
	 */
	public static int getErrCode() {

		// _sErrorMessage = "Wrong text format.";

		return 2;
	}
}
