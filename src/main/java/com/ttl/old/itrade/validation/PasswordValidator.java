package com.ttl.old.itrade.validation;

/**
 * <p>
 * Title: Password validation
 * </p>
 * <p>
 * Description: The PasswordValidator class defined methods to set the
 * validation rules for password.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class PasswordValidator extends AlphanumericValidator {

	/**
	 * Constructor for PasswordValidator class
	 * 
	 * @param pFieldName
	 *            The field name
	 * @param pFieldValue
	 *            The field value
	 */
	public PasswordValidator(String pFieldName, String pFieldValue) {
		super(pFieldName, pFieldValue);
		if (pFieldValue == null) {
			return;

			/*
			 * if (_bIsValid) _bIsValid = sFieldValue.length() <= 10;
			 */
		}
	}

	/**
	 * A static method for getting error code
	 * 
	 * @return 5
	 */
	public static int getErrCode() {

		// _sErrorMessage = "Wrong email address format.";
		// _iErrCode = 5;
		return 5;
	}
}
