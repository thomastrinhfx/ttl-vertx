package com.ttl.old.itrade.validation;

import java.util.Date;

/**
 * <p>
 * Title: Date validation
 * </p>
 * <p>
 * Description: The DateValidator class defined methods to set the validation
 * rules for date.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class DateValidator extends DefaultFieldValidator {

	/**
	 * Constructor for DateValidator class
	 * 
	 * @param pFieldName
	 *            The field name
	 * @param pFieldValue
	 *            The field value
	 */
	public DateValidator(String pFieldName, String pFieldValue) {
		super(pFieldName, pFieldValue);
		_bIsValid = true;
		if (pFieldValue == null) {
			return;
		}
		if (pFieldValue.trim().equals("")) {
			return;
		}
		try {
			Date date = new Date(pFieldValue);
		} catch (IllegalArgumentException e) {
			_bIsValid = false;
			return;
		}
	}

	/**
	 * A static method for getting error code
	 * 
	 * @return 12
	 */
	public static int getErrCode() {
		return 12;
	}
}
