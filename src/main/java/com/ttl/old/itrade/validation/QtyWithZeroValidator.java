package com.ttl.old.itrade.validation;

/**
 * <p>
 * Title: Quantity with zero validation
 * </p>
 * <p>
 * Description: The QtyWithZeroValidator class defined methods to set the
 * validation rules for quantity with zero.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class QtyWithZeroValidator extends NumberValidator {
	/**
	 * Constructor for QtyWithZeroValidator class
	 * 
	 * @param pFieldName
	 *            The field name
	 * @param pFieldValue
	 *            The field value
	 */
	public QtyWithZeroValidator(String pFieldName, String pFieldValue) {
		super(pFieldName, pFieldValue);
		if (pFieldValue == null) {
			return;

		}
		try {
			int i = new Integer(pFieldValue).intValue();
			if (i < 0) {
				_bIsValid = false;
				return;
			}
			_bIsValid = true;
		} catch (NumberFormatException e) {
			_bIsValid = false;
			return;
		}
	}

	/**
	 * A static method for getting error code
	 * 
	 * @return 9
	 */
	public static int getErrCode() {
		return 9;
	}
}
