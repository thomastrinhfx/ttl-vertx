package com.ttl.old.itrade.validation;

/**
 * <p>
 * Title: Quantity validation
 * </p>
 * <p>
 * Description: The QtyValidator class defined methods to set the validation
 * rules for quantity.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class QtyValidator extends NumberValidator {
	/**
	 * Constructor for QtyValidator class
	 * 
	 * @param pFieldName
	 *            The field name
	 * @param pFieldValue
	 *            The field value
	 */
	public QtyValidator(String pFieldName, String pFieldValue) {
		super(pFieldName, pFieldValue);
		if (pFieldValue == null) {
			return;
		}
		try {
			int i = new Integer(pFieldValue).intValue();
			if (i <= 0) {
				_bIsValid = false;
				return;
			}
			_bIsValid = true;
		} catch (NumberFormatException e) {
			_bIsValid = false;
			return;
		}
	}

	/**
	 * A static method for getting error code
	 * 
	 * @return 9
	 */
	public static int getErrCode() {
		return 9;
	}
}
