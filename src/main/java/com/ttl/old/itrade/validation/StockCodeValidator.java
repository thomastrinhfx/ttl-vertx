package com.ttl.old.itrade.validation;

import com.ttl.old.itrade.util.Log;

/**
 * <p>
 * Title: Stock code validation
 * </p>
 * <p>
 * Description: The StockCodeValidator class defined methods to set the
 * validation rules for stock code.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class StockCodeValidator extends DefaultFieldValidator {
	/**
	 * Constructor for StockCodeValidator class
	 * 
	 * @param pFieldName
	 *            The field name
	 * @param pFieldValue
	 *            The field value
	 */
	public StockCodeValidator(String pFieldName, String pFieldValue) {
		super(pFieldName, pFieldValue);
		if (pFieldValue == null) {
			return;
		}
		if (pFieldValue.length() == 0) {
			return;
		}
		_bIsValid = (pFieldValue.length() > 0 && pFieldValue.length() <= 5);
		Log.println("[StockCodeValidator: _bIsValid = " + _bIsValid + "]",
				Log.DEBUG_LOG);
		if (!_bIsValid) {
			return;
		}
		char c;
		for (int i = 0; i < pFieldValue.length(); i++) {
			c = pFieldValue.charAt(i);
			if (!Character.isDigit(c)) {
				Log.println("[StockCodeValidator: sFieldValue.charAt(i) = " + c
						+ "]", Log.DEBUG_LOG);
				_bIsValid = false;
				Log.println("[StockCodeValidator: _bIsValid = " + _bIsValid
						+ "]", Log.DEBUG_LOG);
				return;
			}
		}
	}

	/**
	 * A static method for getting error code
	 * 
	 * @return 7
	 */
	public static int getErrCode() {
		return 7;
	}
}
