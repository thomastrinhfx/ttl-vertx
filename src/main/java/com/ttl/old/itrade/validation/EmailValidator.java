package com.ttl.old.itrade.validation;

/**
 * <p>
 * Title: Email validation
 * </p>
 * <p>
 * Description: The EmailValidator class defined methods to set the validation
 * rules for email.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class EmailValidator extends DefaultFieldValidator {

	/**
	 * Constructor for EmailValidator class
	 * 
	 * @param pFieldName
	 *            The field name
	 * @param pFieldValue
	 *            The field value
	 */
	public EmailValidator(String pFieldName, String pFieldValue) {
		super(pFieldName, pFieldValue);
		if (pFieldValue == null) {
			return;

			// validate that the email address has an at sign, "@", in it. And
			// make sure that there is at least one character in front of the at
			// sign
		}
		_bIsValid = ((pFieldValue.indexOf("@") > -1) && (!pFieldValue
				.substring(0, 1).equals("@")));
	}

	/**
	 * A static method for getting error code
	 * 
	 * @return 2
	 */
	public static int getErrCode() {

		// _sErrorMessage = "Wrong email address format.";
		return 2;
	}
}
