package com.ttl.old.itrade.validation;

/**
 * <p>
 * Title: User ID validation
 * </p>
 * <p>
 * Description: The UserIdValidator class defined methods to set the validation
 * rules for user ID.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class UserIdValidator extends AlphanumericValidator {
	/**
	 * Constructor for UserIdValidator class
	 * 
	 * @param pFieldName
	 *            The field name
	 * @param pFieldValue
	 *            The field value
	 */
	public UserIdValidator(String pFieldName, String pFieldValue) {
		super(pFieldName, pFieldValue);
		if (pFieldValue == null) {
			return;

			/*
			 * if (_bIsValid) _bIsValid = sFieldValue.length() <= 10;
			 */

		}
		if (!_bIsValid) {
			return;

			/*
			 * char c; for (int i = 0; i < sFieldValue.length(); i++) { c =
			 * sFieldValue.charAt(i); if (Character.isLowerCase(c)) { try {
			 * Debug.println("[UserIdValidator: sFieldValue.charAt(i) = " + c +
			 * "]"); _bIsValid = false;
			 * Debug.println("[UserIdValidator: _bIsValid = " + _bIsValid +
			 * "]"); } catch (Exception e) { Log.println(e, Log.ERROR_LOG); }
			 * return; } }
			 */
		}
	}

	/**
	 * A static method for getting error code
	 * 
	 * @return 5
	 */
	public static int getErrCode() {

		// _iErrCode = 5;
		return 5;
	}
}
