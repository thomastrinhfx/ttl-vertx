package com.ttl.old.itrade.validation;

/**
 * <p>
 * Title: Price validation
 * </p>
 * <p>
 * Description: The PriceValidator class defined methods to set the validation
 * rules for price.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class PriceValidator extends NumberValidator {
	/**
	 * Constructor for PriceValidator class
	 * 
	 * @param pFieldName
	 *            The field name
	 * @param pFieldValue
	 *            The field value
	 */
	public PriceValidator(String pFieldName, String pFieldValue) {
		super(pFieldName, pFieldValue);
		if (pFieldValue == null) {
			return;
		}
		try {
			float f = new Float(pFieldValue).floatValue();
			if (f <= 0) {
				_bIsValid = false;
				return;
			}
			_bIsValid = true;
		} catch (NumberFormatException e) {
			_bIsValid = false;
			return;
		}
	}

	/**
	 * A static method for getting error code
	 * 
	 * @return 8
	 */
	public static int getErrCode() {
		return 8;
	}
}
