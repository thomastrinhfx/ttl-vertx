package com.ttl.old.itrade.validation;

/**
 * <p>
 * Title: Alphanumeric validation
 * </p>
 * <p>
 * Description: The AlphanumericValidator class defined methods to set the
 * validation rules for Alphanumeric.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class AlphanumericValidator extends DefaultFieldValidator {
	/**
	 * Constructor for AlphanumericValidator class
	 * 
	 * @param pFieldName
	 *            The field name
	 * @param pFieldValue
	 *            The field value
	 */
	public AlphanumericValidator(String pFieldName, String pFieldValue) {
		super(pFieldName, pFieldValue);
		if (pFieldValue == null) {
			return;
		}
		char c;
		for (int i = 0; i < pFieldValue.length(); i++) {
			c = pFieldValue.charAt(i);
			if (!(Character.isLetter(c) || Character.isDigit(c))) {
				_bIsValid = false;
				return;
			}
		}
	}

	/**
	 * A static method for getting error code
	 * 
	 * @return 4
	 */
	public static int getErrCode() {

		// _sErrorMessage = "Please input with alphanumericformat.";
		return 4;
	}
}
