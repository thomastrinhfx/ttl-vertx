package com.ttl.old.itrade.util;

/**
 * The Replacer class defined methods that are replace the specified part of source.
 * @author
 *
 */
public class Replacer
{
	/**
	 * Constructor for Replacer class.
	 */
	private Replacer() {}

	/**
	 * This method  make new string replace this string with all occurrences of old string and generated.
	 * @param pSource Will be to operate the old string.
	 * @param pKey The index to be replace.
	 * @param pReplacement The new string that are replace old index string.
	 * @return A new string.
	 */
	public static String replace(String pSource, String pKey, String pReplacement)
	{

		//Log.println("[Replacer.replace(): sSource = " + sSource + "]", Log.DEBUG_LOG);
		String  s = pSource;
		int		i = s.indexOf(pKey);

		while (i != -1)
		{
			//Log.println("[Replacer.replace(): i = " + i + ", s.substring(0, " + i + ") = " + s.substring(0, i) + ", sReplacement = " + sReplacement + ", s.substring(i + sKey.length(), s.length()) = " + s.substring(i + sKey.length(), s.length()) + "]", Log.DEBUG_LOG);
			s = s.substring(0, i) + pReplacement + s.substring(i + pKey.length(), s.length());

			i = s.indexOf(pKey, i + pReplacement.length());
		}
		//Log.println("[Replacer.replace(): s = " + s + "]", Log.DEBUG_LOG);
		return s;
	}
}
