package com.ttl.old.itrade.util;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

/**
 * The LocaleNegotiator class defined methods that are bundling with according language set Locale.
 * @author
 *
 */
public class LocaleNegotiator
{

	private ResourceBundle  chosenBundle;
	private Locale			chosenLocale;
	private String			chosenCharset;

	/**
	 * Constructs a new LocaleNegotiator	for the given bundle	name,	language
	 * list, and charset list.
	 *
	 * @param pBundleName the resource bundle name
	 * @param pLanguages the Accept-Language header
	 * @param pCharsetspCharsetspCharsets the Accept-Charset header
	 */
	public LocaleNegotiator(String pBundleName, String pLanguages, String pCharsets)
	{

		// Specify	default values:
		// English language,	ISO-8859-1 (Latin-1)	charset,	English bundle
		Locale			defaultLocale = new Locale("en", "US");
		String			defaultCharset = "ISO-8859-1";

		ResourceBundle  defaultBundle = null;
		try
		{
			defaultBundle = ResourceBundle.getBundle(pBundleName, defaultLocale);
		}
		catch (MissingResourceException e)
		{
			Log.println(e, Log.ERROR_LOG);
			return;
		}

		// If the client	didn't specify	acceptable languages, we can keep
		// the defaults.
		if (pLanguages == null)
		{
			chosenLocale = defaultLocale;
			chosenCharset = defaultCharset;
			chosenBundle = defaultBundle;
			return;		// quick	exit
		}

		// Use a tokenizer to separate acceptable languages
		StringTokenizer tokenizer = new StringTokenizer(pLanguages, ",");

		while (tokenizer.hasMoreTokens())
		{

			// Get the next acceptable	language.
			// (The language can	look something	like "en; qvalue=0.91")
			String			lang = tokenizer.nextToken();

			// Get the locale	for that	language
			Locale			loc = getLocaleForLanguage(lang);

			// Get the bundle	for this	locale.	Don't	let the search	fallback
			// to	match	other	languages!
			ResourceBundle  bundle = null;
			bundle = getBundleNoFallback(pBundleName, loc);

			// The returned bundle is null if there's	no	match.  In that case
			// we	can't	use this	language	since	the servlet	can't	speak	it.
			if (bundle == null)
			{
				continue;		// on to the next language

				// Find a charset	we	can use to display that	locale's	language.
			}
			String  charset = getCharsetForLocale(loc, pCharsets);

			// The returned charset	is	null if there's no match.	In	that case
			// we	can't	use this	language	since	the servlet	can't	encode it.
			if (charset == null)
			{
				continue;		// on to the	next language

				// If	we	get here, there are no problems with this	language.
			}
			chosenLocale = loc;
			chosenBundle = bundle;
			chosenCharset = charset;
			return;				// we're	done
		}

		// No matches, so we let the	defaults	stand
		chosenLocale = defaultLocale;
		chosenCharset = defaultCharset;
		chosenBundle = defaultBundle;
	}

	/**
	 * Gets the chosen bundle.
	 *
	 * @return the chosen bundle
	 */
	public ResourceBundle getBundle()
	{
		return chosenBundle;
	}

	/**
	 * Gets the chosen locale.
	 *
	 * @return the chosen locale
	 */
	public Locale getLocale()
	{
		return chosenLocale;
	}

	/**
	 * Gets the chosen charset.
	 *
	 * @return the chosen charset
	 */
	public String getCharset()
	{
		return chosenCharset;
	}

	/**
	 * This method get a Locale object for a given language string
	 * @param pLang The specified language.
	 * @return The langua corresponding Locale instance.
	 */
	private Locale getLocaleForLanguage(String pLang)
	{
		Locale  loc;
		int		semi, dash;

		// Cut off	any qvalue that might come	after	a semi-colon
		if ((semi = pLang.indexOf(';')) != -1)
		{
			pLang = pLang.substring(0, semi);
		}

		// Trim	any whitespace
		pLang = pLang.trim();

		// Create a Locale from the language.	 A	dash may	separate	the
		// language from	the country.
		if ((dash = pLang.indexOf('-')) == -1)
		{
			loc = new Locale(pLang, "");		// No dash, no country
		}
		else
		{
			loc = new Locale(pLang.substring(0, dash), pLang.substring(dash + 1));
		}

		return loc;
	}

	/**
	 * This method get a ResourceBundle	object for the	given	bundle name	and locale,
	 * or null if the bundle	can't	be	found.  The	resource	bundle must	match
	 * the	locale exactly.  Fallback matches are not	permitted.
	 * @param pBundleName The bundle name.
	 * @param pLocale Object of Locale.
	 * @return The instance of ResourceBundle.
	 */
	private ResourceBundle getBundleNoFallback(String pBundleName, Locale pLocale)
	{

		// First get the	fallback	bundle -- the bundle	that will be selected
		// if getBundle() can't find	a direct	match.  This bundle can	be
		// compared to the bundles returned by later calls to getBundle() in
		// order to detect when getBundle() finds a	direct match.
		ResourceBundle  fallback = null;
		try
		{
			fallback = ResourceBundle.getBundle(pBundleName);
		}
		catch (MissingResourceException e)
		{
			Log.println(e, Log.ERROR_LOG);
		}

		// Get the bundle	for the specified	locale
		ResourceBundle  bundle = null;
		try
		{
			bundle = ResourceBundle.getBundle(pBundleName, (Locale) pLocale);
		}
		catch (MissingResourceException e)
		{
			Log.println(e, Log.ERROR_LOG);
		}

		// Is	the bundle different	than our	fallback	bundle?
		if (bundle != fallback)
		{

			// We have a real match!
			return bundle;
		}

		// So	the bundle is the	same as our	fallback	bundle.
		// We	can still have	a match,	but only	if	our locale's language
		// matches the	default locale's language.
		else if (bundle == fallback && pLocale.getLanguage().equals(Locale.getDefault().getLanguage()))
		{

			// Another way to match
			return bundle;
		}
		return null;	// no	match
	}

	/**
	 * Gets the best charset	for a	given	locale, selecting	from a charset	list.
	 * Currently	ignores the	charset list.	Subclasses can	override	this
	 * method	to	take the	list into account.
	 *
	 * @param	pLocale the locale
	 * @param	pCharsets	a comma-separated	charset list
	 * @return the best charset	for the given locale	from the	given	list
	 */
	protected String getCharsetForLocale(Locale pLocale, String pCharsets)
	{

		// Note: This	method ignores	the client-specified	charsets
		return LocaleToCharsetMap.getCharset(pLocale);
	}
}
