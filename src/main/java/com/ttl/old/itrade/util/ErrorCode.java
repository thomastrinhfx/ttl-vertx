package com.ttl.old.itrade.util;

/**
 * The ErrorCode class defined methods that are descripte error of the field. 
 * @author
 *
 */
public class ErrorCode
{
    private String[] mvFieldNames;
    private String mvErrorID;
    private String mvDefaultErrorMessage; // the default error string if not definited in errorCode.properties

    /**
     * Constructor for ErrorCode class with two parameters .
     * @param pFieldNames The field name.
     * @param pErrorID The error ID.
     */
    public ErrorCode(String[] pFieldNames, int pErrorID)
    {
        this(pFieldNames, pErrorID, "");
    }

    /**
     * This Constructor will invoke other Constructor.
     * @param pFieldNames The field name.
     * @param pErrorID The error ID.
     * @param pDefaultErrorMessage The default error message.
     */
    public ErrorCode(String[] pFieldNames, int pErrorID, String pDefaultErrorMessage)
    {
        this(pFieldNames, Integer.toString(pErrorID), pDefaultErrorMessage);
    }

    /**
     * Constructor for ErrorCode class with two parameters .
     * @param pFieldNames The field name.
     * @param pErrorID The error ID.
     * @param pDefaultErrorMessage The default error message.
     */
    public ErrorCode(String[] pFieldNames, String pErrorID, String pDefaultErrorMessage)
    {
       mvFieldNames = pFieldNames;
       mvErrorID = pErrorID;
       mvDefaultErrorMessage = pDefaultErrorMessage;
    }

    /**
     * Gets the default error message.
     * @return The default error message.
     */
    public String getDefaultErrorMessage()
    {
        return mvDefaultErrorMessage;
    }

    /**
     * Gets the error ID.
     * @return The error ID.
     */
    public String getErrorID()
    {
        return mvErrorID;
    }

    /**
     * Sets the field name.
     * @param pFieldNames The field name.
     */
    public void setFieldNames(String[] pFieldNames)
    {
        this.mvFieldNames = pFieldNames;
    }

    /**
     * Sets the default error message.
     * @param pDefaultError The default error message.
     */
    public void setDefaultErrorMessage(String pDefaultError)
    {
        this.mvDefaultErrorMessage = pDefaultError;
    }

    /**
     * Sets the error ID.
     * @param pErrorID The error ID.
     */
    public void setErrorID(String pErrorID)
    {
        this.mvErrorID = pErrorID;
    }

    /**
     * Gets the field name.
     * @return The field name.
     */
    public String[] getFieldNames()
    {
        return mvFieldNames;
    }
}
