package com.ttl.old.itrade.util;

/**
 * This class will store a information of an order.
 */
public class OrderInfo
{
	private String  stockId = "";
	private String  price = "";
	private String  qty = "";
	private String  buyType = "";

	/**
	 * Constructor for CalendarTest class.
	 * @param pStockId The stock ID.
	 * @param pPrice The stock price.
	 * @param pQuantity The stock quantity.
	 * @param pBuyType The stock buy type.
	 */
	public OrderInfo(String pStockId, String pPrice, String pQuantity, String pBuyType)
	{
		this.stockId = pStockId;
		this.price = pPrice;
		this.qty = pQuantity;
		this.buyType = pBuyType;
	}

	/**
	 * Gets the stock ID.
	 * @return The stock ID.
	 */
	public String getStockId()
	{
		return this.stockId;
	}

	/**
	 * Gets the stock price.
	 * @return The stock price.
	 */
	public String getPrice()
	{
		return this.price;
	}

	/**
	 * Gets the stock quantity.
	 * @return The stock quantity.
	 */
	public String getQty()
	{
		return this.qty;
	}

	/**
	 * Gets the stock buy type.
	 * @return The stock buy type.
	 */
	public String getBuyType()
	{
		return this.buyType;
	}

	/**
	 * This method compare this string with the specified object.
	 * @param pObject the object to be comparison.
	 * @return If the String are equal, then return true; otherwise returns false.
	 */
	public boolean equals(Object pObject)
	{
		if (pObject == null ||!(pObject instanceof OrderInfo))
		{
			return false;

		}
		OrderInfo   orderInfo = (OrderInfo) pObject;
		if (this.stockId.equals(orderInfo.getStockId()) && this.price.equals(orderInfo.getPrice()) && this.qty.equals(orderInfo.getQty()) && this.buyType.equals(orderInfo.getBuyType()))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
