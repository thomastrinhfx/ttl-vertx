package com.ttl.old.itrade.util;

// Author  : Wilfred
// Date    : Sep 14, 2000.

import java.util.Properties;
import java.util.StringTokenizer;

import javax.servlet.http.HttpSession;

import com.ttl.old.itrade.hks.util.HKSInstrument;

/**
 * This LangUtil class contains methods that are working with Multiple languages
 * @author Wilfred.
 */
public class LangUtil
{
	private static Properties   langProperties = null;

    //Constant String
    private static final String lang = "lang";

	/**
	 * This method set the properties file which contains the "lang.x=..." property.
	 * @param pIniProp The properties file which contains the "lang.x=..." property.
	 */
	public static void setIniProperties(Properties pIniProp)
	{
		langProperties = new Properties();

		String  temp;
		int		count = 0;
		while ((temp = pIniProp.getProperty("lang." + count)) != null)
		{
			langProperties.put("lang." + count, temp);
			count++;
		}
	}

	/**
	 * This method set the language which in session contains specific "lang" key. 
	 * @param pSession The currently http session.
	 * @param pLang The session key.
	 */
    public static void setLang(HttpSession pSession, int pLang)
    {
        if (pSession != null && langProperties != null)
        {
            pSession.setAttribute(lang, String.valueOf(pLang));
        }
    }

    /**
     * This method get the language from session.
     * @param pSession The currently http session.
     * @return The language code.
     */
	public static int getLang(HttpSession pSession)
	{
		if (pSession == null || langProperties == null)
		{
			return 0;
		}
		else
		{
			Object  lvLangObject = pSession.getAttribute(lang);

			if (lvLangObject == null)
			{
				return 0;
			}
			else
			{
				String  lvLang = lvLangObject.toString();
				Object  lvTemp = langProperties.get("lang." + lvLang);
				if (lvTemp != null)
				{
					return Integer.parseInt(lvLang);
				}
				else
				{
					return 0;
				}
			}
		}
	}

	/**
	 * This method will get the accept language from _pIni base on the parameter lang, if not found then use default "english".
	 * @param pLang The properties key.
	 * @return The accept language.
	 */
	public static String getAcceptLanguage(int pLang)
	{
		if (langProperties == null)
		{
			Log.println("[ LangUtil.getAcceptLanguage() langProperties is null ]", Log.ERROR_LOG);
			return "en-US";
		}

		String  acceptLang = langProperties.getProperty("lang." + pLang);
		if (acceptLang == null)
		{
			Log.println("[ LangUtil.getAcceptLanguage() cannot find property: lang." + pLang + " ]", Log.ERROR_LOG);
			return "en-US";
		}
		else
		{
			StringTokenizer st = new StringTokenizer(acceptLang, ",");
			return st.nextToken() + "-" + st.nextToken();
		}
	}

	/**
	 * This method will get the accept charset from _pIni base on the parameter
	 * lang, if not found then use default "english"
	 * @param pLang The properties key.
	 * @return The accept charset.
	 */
	public static String getAcceptCharset(int pLang)
	{
		if (langProperties == null)
		{
			Log.println("[ LangUtil.getAcceptCharset() langProperties is null ]", Log.ERROR_LOG);
			return "en-US";
		}

		String  acceptLang = langProperties.getProperty("lang." + pLang);
		if (acceptLang == null)
		{
			Log.println("[ LangUtil.getAcceptCharset() cannot find property: lang." + pLang + " ]", Log.ERROR_LOG);
			return "ISO-8859-1";
		}
		else
		{
			StringTokenizer st = new StringTokenizer(acceptLang, ",");
			st.nextToken();
			st.nextToken();
			return st.nextToken();
		}
	}

	/**
	 * This method wll get the stock name with Multiple languages.
	 * @param pSession The currently http session.
	 * @param pHKSInstrument The Object of HKSInstrument.
	 * @return specific stock name.
	 */
   public static String getInstrumentName(HttpSession pSession, HKSInstrument pHKSInstrument)
   {
	   if (pHKSInstrument == null)
		   return " - ";
      int lvLang = getLang(pSession);
      if (lvLang == 1 || lvLang == 2)
         return pHKSInstrument.getChineseShortName();
      else
         return pHKSInstrument.getShortName();
   }
}
