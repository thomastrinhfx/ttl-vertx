package com.ttl.old.itrade.util;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import io.vertx.core.json.JsonObject;

public class VertxMessagetxnCodec implements MessageCodec<VertxMessagetxn, VertxMessagetxn>  {

	@Override
	public VertxMessagetxn decodeFromWire(int arg0, Buffer arg1) {
		// TODO Auto-generated method stub
		int _pos = arg0;
		int lenght = arg1.length();
		
		String JsonStr = arg1.getString(_pos+=4, _pos+=lenght);
		JsonObject contentJson = new JsonObject(JsonStr);
		String message = contentJson.getString("mvMessage");
		int key = contentJson.getInteger("mvKey");
		String result = contentJson.getString("mvResult");
		return new VertxMessagetxn(message, key, result);
	}

	@Override
	public void encodeToWire(Buffer buffer, VertxMessagetxn customMessage) {
		JsonObject jsonToEncode = new JsonObject();
	    jsonToEncode.put("mvMessage", customMessage.getMvMessage());
	    jsonToEncode.put("mvKey", customMessage.getMvKey());
	    jsonToEncode.put("mvResult", customMessage.getMvResult());

	    // Encode object to string
	    String jsonToStr = jsonToEncode.encode();

	    // Length of JSON: is NOT characters count
	    int length = jsonToStr.getBytes().length;

	    // Write data into given buffer
	    buffer.appendInt(length);
	    buffer.appendString(jsonToStr);
		
	}

	@Override
	public String name() {
		// TODO Auto-generated method stub
		return this.getClass().getSimpleName();
	}

	@Override
	public byte systemCodecID() {
		// TODO Auto-generated method stub
		return -1;
	}

	@Override
	public VertxMessagetxn transform(VertxMessagetxn arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
