package com.ttl.old.itrade.util;

/**
 * The Page class will defined some information of page.
 * @author 
 *
 */
public class Page {

	private int totalPage = 1;

	private int prePage = 1;

	private int nextPage = 1;

	private int totalRec = 0;

	private final int defaultPageSize = 10;

	private int pageSize = defaultPageSize;

	private int pageIndex = 1;

	private int[] pageNumbers;

	/**
	 * Gets the page index.
	 * @return The page index.
	 */
	public int getPageIndex() {
		return pageIndex;
	}
	
	private int[] pageRecords;

	/**
	 * Gets the page records.
	 * @return The page records.
	 */
	public int[] getPageRecords() {
		return pageRecords;
	}

	/**
	 * Sets the page records.
	 * @param pTotalRecord The page records.
	 */
	public void setPageRecords(int pTotalRecord) {
		if(pTotalRecord == 0){
			this.pageRecords = null;
		}else{
			this.pageRecords = new int[pTotalRecord + 1];
			for(int i = 0; i <= pTotalRecord; i ++){
				this.pageRecords[i] = i;
			}
		}
		this.pageRecords = pageRecords;
	}

	/**
	 * Sets the page index.
	 * @param pPageIndex The page index.
	 */
	public void setPageIndex(int pPageIndex) {
		if(pPageIndex > 0 && pPageIndex < this.totalPage){
			this.pageIndex = pPageIndex;
		}else if(pPageIndex >= this.totalPage){
			this.pageIndex = this.totalPage;
		}else if(pPageIndex < 0){
			this.pageIndex = 1;
		}
	}

	/**
	 * Gets the next page.
	 * @return The next page .
	 */
	public int getNextPage() {
		return nextPage;
	}

	/**
	 * Sets the next page.
	 * @param pNextPage The next page.
	 */
	public void setNextPage(int pNextPage) {
		this.nextPage = pNextPage > this.totalPage ? this.totalPage : pNextPage;
	}

	/**
	 * Gets the page size.
	 * @return The page size.
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * Sets the page size.
	 * @param pPageSize The page size.
	 */
	public void setPageSize(int pPageSize) {
		this.pageSize = pPageSize > 0 ? pPageSize : 10;
	}

	/**
	 * Gets the precedent page.
	 * @return The precedent page.
	 */
	public int getPrePage() {
		return prePage;
	}

	/**
	 * Sets the precedent page.
	 * @param pPrePage The precedent page.
	 */
	public void setPrePage(int pPrePage) {
		this.prePage = pPrePage < 1 ? 1 : pPrePage;
	}

	/**
	 * Gets the page total.
	 * @return The page total.
	 */
	public int getTotalPage() {
		return totalPage;
	}

	/**
	 * Sets the page total.
	 * @param pTotalPage The page total.
	 */
	public void setTotalPage(int pTotalPage) {
		this.totalPage = pTotalPage > 0 ? pTotalPage : 1;
	}

	/**
	 * Gets the rec total.
	 * @return The rec total.
	 */
	public int getTotalRec() {
		return totalRec;
	}

	/**
	 * Sets the rec total.
	 * @param pTotalRecord The rec total.
	 */
	public void setTotalRec(int pTotalRecord) {
		this.totalRec = pTotalRecord > -1 ? pTotalRecord : 0;
	}

	/**
	 * Gets the page number.
	 * @return The page number.
	 */
	public int[] getPageNumbers() {
		return pageNumbers;
	}

	/**
	 * Sets the page number.
	 * @param pPageNumbers The page number.
	 */
	public void setPageNumbers(int[] pPageNumbers) {
		this.pageNumbers = pPageNumbers;
	}
}
