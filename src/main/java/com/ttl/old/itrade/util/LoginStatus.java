package com.ttl.old.itrade.util;

/**
 * The LoginStatus class defined fields that are about login status.
 * @author
 *
 */
public class LoginStatus
{

	public String   currentLoginMachine = null;
	public boolean  isPasswordExpired = false;
	public boolean  needChangePassword = false;
	public boolean  needShowDisclaimer = false;
	public String   disclaimerFileName = null;
	public String   uniqueId = null;

}
