package com.ttl.old.itrade.util;

// Author  : Wilfred Chan
// Date    : Aug 30, 2000.

/**
 * This class will store a string value which is represent a key of a properties
 * file or Properties class. This class will be use in Presentation.
 * @author Wilfred Chan
 * @since  Aug 30, 2000.
 */
public class PropertyKey
{

	private String  key = null;

	/**
	 * Constructor for PropertyKey class.
	 * @param pKey a specified string.
	 */
	public PropertyKey(String pKey)
	{
		this.key = pKey;
	}

	/**
	 * Gets the property key.
	 * @return The property key.
	 */
	public String getKey()
	{
		return key;
	}

}
