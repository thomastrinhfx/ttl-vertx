package com.ttl.old.itrade.util;

import java.net.URLEncoder;

/**
 * The UrlEncoder class defined methods that are encoding specified URL.
 * @author
 *
 */
public class UrlEncoder
{
	/**
	 * Constructor for UrlEncoder class.
	 */
	private UrlEncoder() {}

	/**
	 * This method encode the specified url.
	 * @param pSource Required encoded string.
	 * @return Has been encoded string.
	 */
	public static String encode(String pSource)
	{
		String  s = Replacer.replace(URLEncoder.encode(pSource), ".", "%2E");
		return s;
	}
}

