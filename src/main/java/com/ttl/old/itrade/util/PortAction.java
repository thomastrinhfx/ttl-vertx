package com.ttl.old.itrade.util;

// Author  : Wilfred Chan
// Date    : Jul 25, 2000.

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import com.ttl.old.itrade.interfaces.PortActionListener;

/**
 * This class will start the listening the port specified
 */
public class PortAction extends Thread
{

	Thread				mySelf;
	Socket				socket;
	ServerSocket		serverSocket;
	PortActionListener  portActionListener = null;
	int					port;

	/**
	 * Constructor for PortAction class.
	 * @param pPort The port number.
	 * @param pPortActionListener Object of PortActionListener.
	 * @throws IOException If an I / O error.
	 */
	public PortAction(int pPort, PortActionListener pPortActionListener) throws IOException
	{

		this.port = pPort;
		this.portActionListener = pPortActionListener;

		try
		{

			// creating socket
			serverSocket = new ServerSocket(this.port);

			// mySelf = new Thread();
			// mySelf.start();
		}
		catch (IOException e)
		{
			Log.println(e, Log.ERROR_LOG);
			throw e;
		}
	}

	/**
	 * This method will call a server with a specific port and send a message
	 * @param pServerName The server name.
	 * @param pPort The server specific port.
	 * @param pMessage The need send message.
	 * @return The message for respones.
	 * @throws Exception If the program does not normally arise during the implementation.
	 */
	public static String callServer(String pServerName, int pPort, String pMessage) throws Exception
	{

		Socket			clientSocket = null;
		PrintWriter		out = null;
		BufferedReader  in = null;

		try
		{

			// set up socket, input stream and output stream;
			clientSocket = new Socket(pServerName, pPort);
			out = new PrintWriter(clientSocket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		}
		catch (UnknownHostException e)
		{
			Log.println(e, Log.ERROR_LOG);
		}
		catch (IOException e)
		{
			Log.println(e, Log.ERROR_LOG);
		}

		// out the message
		out.write(pMessage);

		// out.close();

		// get respone
		StringBuffer	sb = new StringBuffer();
		int				i;
		while ((i = in.read()) != -1)
		{
			sb.append((char) i);
		}
		out.close();
		in.close();

		return sb.toString();
	}
/**
 * This thread is called when some method to run.
 */
	public void run()
	{
		try
		{

			// waiting for client
			// System.out.print("Waiting...");
			socket = serverSocket.accept();

			// System.out.println("some one in");

			// call the PortActionListener
			if (portActionListener != null)
			{
				portActionListener.actionPerform(socket);
			}
			else
			{
				Log.println("[ PortAction.run() portActionListener is null ]", Log.ERROR_LOG);
			}

		}
		catch (IOException e)
		{
			Log.println(e, Log.ERROR_LOG);
		}
		finally
		{
			try
			{

				// mySelf.stop();
				socket.close();
				serverSocket.close();
			}
			catch (IOException e2)
			{
				Log.println(e2, Log.ERROR_LOG);
			}
		}
	}		// end of run method

}
