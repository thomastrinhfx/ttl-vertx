package com.ttl.old.itrade.util;

// Modified by Wilfred
// Date  : June 2, 2000.
// Modification  : Add a method to get the error message from errorCode.properties

// Modified by Wilfred
// Date  : June 19, 2000.
// Modification  : Comment all no cach when sending html page to client side

// Modified by Wilfred
// Date  : Jul 17, 2000.
// Modification  : Add an entry for each show page to display the current date and time

// Modified by Wilfred
// Date  : Aug 3, 2000.
// Modification  : Add a method which will return the error code bundle

// Modified by Wilfred
// Date  : Aug 18, 2000.
// Modification  : Add date and time format to constructor

// Modified by Wilfred
// Date  : Aug 30, 2000.
// Modification  : Check each elements of hashtable if it is an instance of
// PropertyKey class then get the key and then get it corresponding value from
// the properties file and substitude to the hashtable again.

// Modified by Wilfred
// Date  : June 14, 2000.
// Modification  : Add a method to get the property from <Action>.properties

import java.io.IOException;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.systekit.winvest.hks.util.Utils;

/**
 * The Presentation class defined methods that are finding corresponding page with conditions.
 * @author Wilfred
 *
 */
public class Presentation
{
	private Locale				_locale;
	private String				_sCharset;
	protected String			_htmlTemplatePath;
	private SimpleDateFormat	dateTimeFormatter;
	protected ResourceBundle	_bundle = null;
	protected ResourceBundle	_errBundle = null;

   public final static String DEFAULT_SYSTEM_ERROR = "1080";

   /**
    * Constructor for Presentation class.
    * @param pBundleName The bundle name.
    * @param pAcceptLanguage The accept language.
    * @param pAcceptCharset The accept charset.
    * @param pTemplatePath The template path.
    * @param pDateTimeFormat The format datetime.
    */
	public Presentation(String pBundleName, String pAcceptLanguage, String pAcceptCharset, String pTemplatePath, String pDateTimeFormat)
	{

		// LocaleNegotiator negotiator = new LocaleNegotiator(sBundleName, sAcceptLanguage, sAcceptCharset);
		LocaleNegotiator	negotiator = StaticLocaleNegotiator.getLocalenegotiator(pBundleName, pAcceptLanguage, pAcceptCharset);
		_locale = negotiator.getLocale();
		_sCharset = negotiator.getCharset();

		_htmlTemplatePath = pTemplatePath;
		dateTimeFormatter = new SimpleDateFormat(pDateTimeFormat);
		try
		{
			_bundle = negotiator.getBundle();
		}
		catch (NullPointerException e)
		{
			Log.println(e, Log.ERROR_LOG);
		}

		// Log.println("[Presentation(): _bundle == null = " + (_bundle == null) + "]", Log.ACCESS_LOG);

		// negotiator = new LocaleNegotiator("errorCode", sAcceptLanguage, sAcceptCharset);
		negotiator = StaticLocaleNegotiator.getLocalenegotiator("errorCode", pAcceptLanguage, pAcceptCharset);

		// Log.println("[Presentation(): getting error bundle done]", Log.ACCESS_LOG);

		_locale = negotiator.getLocale();
		_sCharset = negotiator.getCharset();
		try
		{
			_errBundle = negotiator.getBundle();
		}
		catch (NullPointerException e)
		{
			Log.println(e, Log.ERROR_LOG);
		}

		// Log.println("[Presentation(): _errBundle == null = " + (_errBundle == null) + "]", Log.ACCESS_LOG);
	}
	/**
	 * This method invoke corresponding method to show page, With request,response ,a property and map data.
	 * @param pRequest object that contains the request the client made of the servlet.
	 * @param pResponse object that contains the response the servlet returns to the client.
	 * @param pProperty an specified property.
	 * @param pData an data with map type.
	 * @throws Exception If the program does not normally arise during the implementation.
	 */
	public void showPage(HttpServletRequest pRequest, HttpServletResponse pResponse, String pProperty, Hashtable pData) throws Exception
	{
		_showPage(pRequest, pResponse, pProperty, pData);
	}

	/**
	 * This method invoke corresponding method to show page, With request,response and a property.
	 * @param pRequest object that contains the request the client made of the servlet.
	 * @param pResponse object that contains the response the servlet returns to the client.
	 * @param pProperty an specified property.
	 * @throws Exception If the program does not normally arise during the implementation.
	 */
	public void showPage(HttpServletRequest pRequest, HttpServletResponse pResponse, String pProperty) throws Exception
	{

		_showPage(pRequest, pResponse, pProperty, new Hashtable());
	}

	/**
	 * This method invoke corresponding method to show page, With request,response and map data.
	 * @param pRequest object that contains the request the client made of the servlet.
	 * @param pResponse object that contains the response the servlet returns to the client.
	 * @param pData an data with map type.
	 * @throws Exception If the program does not normally arise during the implementation.
	 */
	public void showPage(HttpServletRequest pRequest, HttpServletResponse pResponse, Hashtable pData) throws Exception
	{

		_showPage(pRequest, pResponse, null, pData);
	}

	/**
	 * This method invoke corresponding method to show page, With request and response.
	 * @param pRequest object that contains the request the client made of the servlet.
	 * @param pResponse object that contains the response the servlet returns to the client.
	 * @throws Exception If the program does not normally arise during the implementation.
	 */
	public void showPage(HttpServletRequest pRequest, HttpServletResponse pResponse) throws Exception
	{

		_showPage(pRequest, pResponse, null, new Hashtable());
	}

	/**
	 * This method to show page, With response and a property.
	 * @param pResponse object that contains the response the servlet returns to the client.
	 * @param pProperty an specified property.
	 * @throws MissingResourceException If you missing the resource.
	 */
	public void showPage(HttpServletResponse pResponse, String pProperty) throws MissingResourceException
	{
		try
		{

			// Commented by Wilfred Jun 19, 2000.

			pResponse.setHeader("Cache-Control", "no-store");	// HTTP 1.1
			pResponse.setHeader("Pragma", "no-cache");			// HTTP 1.0
			pResponse.setDateHeader("Expires", 0);				// prevents caching at the proxy server

			Log.println("redirect: " + _bundle.getString(pProperty), Log.ACCESS_LOG);
			pResponse.sendRedirect(_bundle.getString(pProperty));
		}
		catch (IOException e)
		{
			Log.println(e, Log.ERROR_LOG);
		}
	}

		/**
		 * This method to show page, With response , a property and a parameter.
		 * @param pResponse object that contains the response the servlet returns to the client.
		 * @param pProperty an specified property.
		 * @param pParameter an specified parameter.
		 * @throws MissingResourceException If you missing the resource.
		 */
        public void showPage(HttpServletResponse pResponse, String pProperty, String pParameter) throws MissingResourceException
        {
                try
                {

                        // Commented by Wilfred Jun 19, 2000.

                	pResponse.setHeader("Cache-Control", "no-store");	// HTTP 1.1
                	pResponse.setHeader("Pragma", "no-cache");			// HTTP 1.0
                	pResponse.setDateHeader("Expires", 0);				// prevents caching at the proxy server

                        Log.println("redirect: " + _bundle.getString(pProperty) + pParameter, Log.ACCESS_LOG);
                        pResponse.sendRedirect(_bundle.getString(pProperty)+pParameter);
                }
                catch (IOException e)
                {
                        Log.println(e, Log.ERROR_LOG);
                }
        }



    /**
     * This method to show page, With request,response, a property and file name.
	 * @param pRequest object that contains the request the client made of the servlet.
	 * @param pResponse object that contains the response the servlet returns to the client.
	 * @param pProperty an specified property.
     * @param pFileName The file name.
     * @throws Exception If the program does not normally arise during the implementation.
     */
	public void showPage(HttpServletRequest pRequest, HttpServletResponse pResponse, String pProperty, String pFileName) throws Exception
	{
		HtmlTemplate	page = null;
		String			fullPath = "";

		/*
		 * //String fullPath = _bundle.getString(property) + File.separator + fileName;
		 * fullPath = _htmlTemplatePath + _bundle.getString(property);
		 *
		 * try
		 * {
		 * page = this.getPage(fullPath, req);
		 * }
		 * catch (Exception ex)
		 * {
		 * Log.println(ex, Log.ERROR_LOG);
		 * }
		 */

		// if the page is null then try to get the default disclaimer
		if (page == null)
		{
			fullPath = _bundle.getString(pProperty);		// + File.separator + "disclaimer.htm";
			try
			{
				page = this.getPage(fullPath, pRequest);
			}
			catch (Exception ex)
			{
				Log.println(ex, Log.ERROR_LOG);
			}
		}

		pResponse.setContentType("text/html; charset=" + _sCharset);
		pResponse.setLocale(_locale);
		pResponse.setHeader("Cache-Control", "no-store");		// HTTP 1.1
		pResponse.setHeader("Pragma", "no-cache");			// HTTP 1.0
		pResponse.setDateHeader("Expires", 0);				// prevents caching at the proxy server

		PrintWriter out = pResponse.getWriter();
		page.print(out, new Hashtable(), _sCharset);
		out.close();
	}

	/**
	 * This method to show page, With response.
	 * @param pResponse object that contains the response the servlet returns to the client.
	 * @throws MissingResourceException If you missing the resource.
	 */
	public void showPage(HttpServletResponse pResponse) throws MissingResourceException
	{
		try
		{

			// Commented by Wilfreed Jun 19, 2000.

			pResponse.setHeader("Cache-Control", "no-store");	// HTTP 1.1
			pResponse.setHeader("Pragma", "no-cache");			// HTTP 1.0
			pResponse.setDateHeader("Expires", 0);				// prevents caching at the proxy server

			pResponse.sendRedirect(_bundle.getString("url"));
		}
		catch (IOException e)
		{
			Log.println(e, Log.ERROR_LOG);
		}
	}

	/**
	 * This method redirect to specified URL.
	 * @param pResponse object that contains the response the servlet returns to the client.
	 * @param pUrl the redirect location URL.
	 * @throws MissingResourceException If you missing the resource.
	 */
	public void redirectToURL(HttpServletResponse pResponse, String pUrl) throws MissingResourceException
	{
		try
		{

			// Commented by Wilfred Jun 19, 2000.

			pResponse.setHeader("Cache-Control", "no-store");	// HTTP 1.1
			pResponse.setHeader("Pragma", "no-cache");			// HTTP 1.0
			pResponse.setDateHeader("Expires", 0);				// prevents caching at the proxy server
			pResponse.sendRedirect(pUrl);
		}
		catch (IOException e)
		{
			Log.println(e, Log.ERROR_LOG);
		}
	}

	/**
	 * This method get error code of bundle resource.
	 * @return the instance of ResourceBundle.
	 */
	// Added by Wilfred start Aug 3, 2000.
	public ResourceBundle getErrorCodeBundle()
	{
		return _errBundle;
	}

	// Added by Wilfred end Aug 3, 2000.

	// Added by Wilfred start June 2, 2000.

	/**
	 * This method will return an error string, which is retrieved from errorCode.properties
	 * file, and return null if no such error message.
	 * @param pCode error code.
	 * @return an error message string.
	 */
	public String getErrorMesssage(int pCode)
	{
		try
		{
			return new String(_errBundle.getString(Integer.toString(pCode)).getBytes("ISO-8859-1"), "UTF-8");
		}
		catch (Exception ex)
		{
			Log.println(ex, Log.ERROR_LOG);
			return "";
		}
	}

	// Added by Wilfred end June 2, 2000.

	// Added by Wilfred start Sep 14, 2000.

	/**
	 * This method will return an property string, which is retrieved from <Action>.properties
	 * file, and return null if no such property.
	 * @param pProperty an specified property.
	 * @return an property string.
	 */
	public String getProperty(String pProperty)
	{
		try
		{
			return new String(_bundle.getString(pProperty).getBytes("ISO-8859-1"), "UTF-8");
		}
		catch (Exception ex)
		{
			Log.println(ex, Log.ERROR_LOG);
			return "";
		}
	}

	// Added by Wilfred end Sep 14, 2000.

	/**
	 * This method to show corresponding page by property,map data.
	 * @param pRequest object that contains the request the client made of the servlet.
	 * @param pResponse object that contains the response the servlet returns to the client.
	 * @param pProperty an specified property.
	 * @param pData an data with map type.
	 * @throws Exception If the program does not normally arise during the implementation.
	 */
	private void _showPage(HttpServletRequest pRequest, HttpServletResponse pResponse, String pProperty, Hashtable pData) throws Exception
	{
		if (pData == null)
		{
			pData = new Hashtable();

		}
//		SimpleDateFormat	dformatter = new SimpleDateFormat("MMMMM dd, yyyy HH:mm 'HKT'");
		try
		{
			Date	currentDate = new Date();
			pData.put("<!--viewDate-->", dateTimeFormatter.format(currentDate) + " HKT");
		}
		catch (Exception ex)
		{
			Log.println(ex, Log.ERROR_LOG);
		}


		Enumeration		lvEnumeration = pData.keys();
		String			sKey = null;
		String			sMessage = null;
		Object			oValue = null;
		String[]		sFieldNames = null;
		ErrorCode		errCode = null;
		MessageFormat   formatter = new MessageFormat("");

		while (lvEnumeration.hasMoreElements())
		{
			sKey = (String) lvEnumeration.nextElement();
			oValue = pData.get(sKey);

			if (oValue instanceof PropertyKey)
			{

				PropertyKey propertyKey = (PropertyKey) oValue;
				String		value = null;
				try
				{
					value = _bundle.getString(propertyKey.getKey());
				}
				catch (Exception ex)
				{
					Log.println("Presentation._showPage: Cannot get " + propertyKey.getKey() + " from bundle.", Log.ERROR_LOG);
					Log.println(ex, Log.ERROR_LOG);
					value = "";
				}
				pData.put(sKey, value);
			}
			else if (oValue instanceof ErrorCode)
			{

				errCode = (ErrorCode) oValue;
				try
				{
					sMessage = new String(_errBundle.getString(errCode.getErrorID()).getBytes("ISO-8859-1"), "UTF-8");
				}
				catch (Exception ex)
				{
					Log.println("Presentation._showPage: missing error mapping for " + errCode.getErrorID() + ", Default = " + sMessage, Log.ERROR_LOG);
					sMessage = errCode.getDefaultErrorMessage();
					if (sMessage == null || sMessage.equals(""))
						sMessage = new String(_errBundle.getString(DEFAULT_SYSTEM_ERROR).getBytes("ISO-8859-1"), "UTF-8");

				}
				Log.println("[Presentation._showPage(): errCode.errorCode: " + errCode.getErrorID() + " sMessage = " + sMessage + "]", Log.ACCESS_LOG);

				if (errCode.getFieldNames() != null && errCode.getFieldNames().length > 0)
				{

					sFieldNames = new String[errCode.getFieldNames().length];
					for (int i = 0; i < errCode.getFieldNames().length; i++)
					{
						try
						{
							sFieldNames[i] = _bundle.getString((String) errCode.getFieldNames()[i]);
						}
						catch (MissingResourceException e)
						{
							sFieldNames[i] = (String) errCode.getFieldNames()[i];
						}
						catch (NullPointerException e)
						{
							sFieldNames[i] = (String) errCode.getFieldNames()[i];
						}
					}

					formatter.applyPattern(sMessage);

					try
					{
						pData.put(sKey, formatter.format(sFieldNames));
					}
					catch (IllegalArgumentException e)
					{
						Log.println(e, Log.ERROR_LOG);
						pData.put(sKey, formatter.format(sMessage));
					}
				}
				else
				{
					pData.put(sKey, sMessage);
				}

			}		// end of if error code
		}			// end of while

		pResponse.setContentType("text/html; charset=" + "UTF-8");
		pResponse.setLocale(_locale);
		pResponse.setHeader("Cache-Control", "no-store");	// HTTP 1.1
		pResponse.setHeader("Pragma", "no-cache");			// HTTP 1.0
		pResponse.setDateHeader("Expires", 0);				// prevents caching at the proxy server

		java.io.PrintWriter out = null;
//		ServletOutputStream out = null;
		try
		{
			out = pResponse.getWriter();
//			out = response.getOutputStream();
		}
		catch (IOException e)
		{
			Log.println(e, Log.ERROR_LOG);
			return;
		}

		// response.setHeader("Content-Language", _locale.getLanguage());
		// response.setHeader("Vary", "Accept-Language");

		HtmlTemplate		template = null;

		if (pProperty != null)
		{

			try
			{
				_bundle.getString(pProperty);
			}
			catch (Exception ex)
			{
				Log.println("Presentation._showPage(): Cannot get Property: " + pProperty + " from bundle.", Log.ERROR_LOG);
				throw ex;
			}
			Log.println("[Presentation._showPage(): sProperty: " + pProperty + " templateFile: " + _bundle.getString(pProperty) + "]", Log.ACCESS_LOG);
			template = HtmlTemplateCache.getFile(_htmlTemplatePath + _bundle.getString(pProperty), pRequest);
		}
		else
		{
			if (!Utils.isNullStr(pData.get("<!--ResultXML-->")))
					template = HtmlTemplateCache.getFile(_htmlTemplatePath + "/HKSQueryStockName.xml", pRequest);
			else
			/*try
			{
				
				if (_bundle.getString("templateFile") == null)
				{
					Log.println("cannot get templateFile, because templateFile prop not found in bundle", Log.ACCESS_LOG);
				}
				else
				{
					Log.println("template file prop found", Log.ACCESS_LOG);
				}
			}
			catch (Exception ex)
			{
				Log.println("Presentation._showPage: Cannot get templateFile from bundle.", Log.ERROR_LOG);
				Log.println(ex, Log.ERROR_LOG);
			}*/
			if (Utils.isNullStr(pData.get("<!--ResultXML-->"))){
				//Log.println("[ Presentation._showPage(): (sProperty is null) templateFile: " + _bundle.getString("templateFile") + "]", Log.ACCESS_LOG);
				template = HtmlTemplateCache.getFile("itrade/MarketData_"+pRequest.getSession().getAttribute("mvLanguage").toString()+".html", pRequest);
			}
		}

		try
		{
			template.print(out, pData, _sCharset);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			Log.println(e, Log.ERROR_LOG);
			return;
		}
		out.close();
	}

	/**
	 * This method to get html template for page with file path and request.
	 * @param pFilePath the file path.
	 * @param pRequest object that contains the request the client made of the servlet.
	 * @return An instance of HtmlTemplate which are corresponding path.
	 * @throws Exception If the program does not normally arise during the implementation.
	 */
	public HtmlTemplate getPage(String pFilePath, HttpServletRequest pRequest) throws Exception
	{
		return HtmlTemplateCache.getFile(pFilePath, pRequest);
	}

	/**
	 * This method to get html template for page with property and map data.
	 * @param pProperty an specified property.
	 * @param pData an data with map type.
	 * @return A string that are corresponding page. 
	 * @throws Exception If the program does not normally arise during the implementation.
	 */
	public String getPage(String pProperty, Hashtable pData) throws Exception
	{
		HtmlTemplate		template;
		if (pProperty != null)
		{
			template = HtmlTemplateCache.getFile2(_htmlTemplatePath + _bundle.getString(pProperty));
		}
		else
		{
			template = HtmlTemplateCache.getFile2(_htmlTemplatePath + _bundle.getString("templateFile"));
		}
		return template.formatToString(pData);
	}

}
