package com.ttl.old.itrade.util;

// Author   : Wilfred Chan
// Date     : Apr. 25, 2000

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Vector;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.interfaces.IMain;
import com.systekit.common.sys.sysTrace;
import com.systekit.common.utl.SendMail;

/**
 * This class contains different kind of methods for printing error,
 * access log, and transaction log.
 */
public class Log
{


	private static Vector linkedList = new Vector();
	private static String path = null;
	//private static String newLine = new String("\r\n");

	private static sysTrace mvTrace;
	private static final SimpleDateFormat mvSimpleDateFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss:SSS");

	public static final int ERROR_LOG = 0;
	public static final int ACCESS_LOG = 1;
	public static final int TRANSACTION_LOG = 2;
	public static final int URL_SEND_LOG = 3;
	public static final int URL_RECEIVED_LOG = 4;
	public static final int DEBUG_LOG = 5;
	public static final int ALERT_LOG = 6;
        //public static final int TRANSACTION_LOG = 7;

	public static boolean mvDebugOn = false;

	private static Properties mvITradeProperties;

	/**
	 * This method will set the path for all log files, this method should be called
	 * at the very beginning
	 * @param pPath The path string.
	 */
	public static void setPath(String pPath)
	{
		path = pPath;
	}



	/**
	 * This method will print an message without newLine character.Log
	 * @param pMessage the message to be print.
	 * @param pLogType is one of the static integer of this class
	 */
	public static void print(String pMessage, int pLogType)
	{
		if (pLogType == DEBUG_LOG && !mvDebugOn)
			return;
		if (pMessage != null)
		{
			linkedList.add(new LogNode(Thread.currentThread().getName(), pMessage, pLogType, new Date()));
			Object node = linkedList.remove(0);
			logActionPerform(node);
		}

	}

	/**
	 * This method will print an exception with its stack trace
	 * @param pException an exception object.
	 * @param pLogType is one of the static integer of this class
	 */
	public static void print(Throwable pException, int pLogType)
	{
		if (pLogType == ERROR_LOG && !mvDebugOn)
			return;
		linkedList.add(new LogNode(Thread.currentThread().getName(), pException, pLogType, new Date()));
		Object node = linkedList.remove(0);
		logActionPerform(node);
		//startLogThread();
	}

	/**
	 * This method will do the samething as print(Exception)
	 * @param pException the exception you want to print
	 * @param pLogType is one of the static integer of this class
	 * @see <a href="ErrorLog.html#print(Exception)">print(Exception)</a>
	 */
	public static void println(Throwable pException, int pLogType)
	{
		print(pException, pLogType);
	}

	/**
	 * This method will print an message with newLine character.
	 * @param pMessage the message you want to print
	 * @param pLogType is one of the static integer of this class
	 */
	public static void println(String pMessage, int pLogType)
	{
		print(pMessage, pLogType);
	}

	/**
	 * This method to perform action about log.
	 * @param pNode  An object of Log Node.
	 */
	private static void logActionPerform(Object pNode)
	{
		if (pNode == null)
		{
			return;
		}
		try
		{

			LogNode logNode = (LogNode) pNode;
			int type = logNode.logType;
			String outputDir;
			String pathname = path;

			if (mvTrace == null)
			{
				setSysTrace();
			}

			int lvLogLevel;
			boolean lvIsDefaultValue = false;

			switch (type)
			{
				case ERROR_LOG:
					lvLogLevel = sysTrace.TRACE_LEVEL_ERROR;
					break;
				case ACCESS_LOG:
					lvLogLevel = sysTrace.TRACE_LEVEL_INFO;
					break;
                                case TRANSACTION_LOG:
                                        lvLogLevel = sysTrace.TRACE_LEVEL_MESSAGE;
                                        break;
				case URL_SEND_LOG:
					lvLogLevel = sysTrace.TRACE_LEVEL_NETWORK;
					break;
				case URL_RECEIVED_LOG:
					lvLogLevel = sysTrace.TRACE_LEVEL_NETWORK;
					break;
				case DEBUG_LOG:
					lvLogLevel = sysTrace.TRACE_LEVEL_DEBUG;
					break;
				case ALERT_LOG:
					lvLogLevel = sysTrace.TRACE_LEVEL_WARNING;
					break;
				default:
					mvTrace.trace("", logNode.mvThreadName, "Incorrect Log Type - ["+String.valueOf(type)+"]", sysTrace.TRACE_LEVEL_WARNING);
					lvLogLevel = sysTrace.TRACE_LEVEL_WARNING;
					break;
			}

			// write the date
			String date = mvSimpleDateFormat.format(logNode.logTime) + "\t";

			// write message if it is a message
			if (logNode.data instanceof String)
			{
				//out.write(logNode.data.toString().getBytes());
                                mvTrace.trace("", logNode.mvThreadName, logNode.data.toString(), lvLogLevel);


			}
			else
			{
				Throwable ex = (Throwable) logNode.data;

                if(lvLogLevel == sysTrace.TRACE_LEVEL_WARNING)
                    mvTrace.trace("", logNode.mvThreadName, ex, lvLogLevel);
                else
                mvTrace.trace("", logNode.mvThreadName, ex, sysTrace.TRACE_LEVEL_ERROR);
			}

		}
		catch (Exception ioex)
		{
			ioex.printStackTrace();
		}
	}

	/**
	 * This method is a running entrance method.
	 * @param args
	 */
	public static void main(String[] args)
	{
		Log.setPath("d:\\Programs\\CSITrade\\itrade\\logs");
		Exception ex = new Exception("Testing exception");
		Log.println("Test message", Log.ALERT_LOG);
		//Log.stop();
		Log.println(ex, Log.ALERT_LOG);






	}

	/**
	 * This method set system log message trace.
	 */
	public static void setSysTrace()
	{
		mvITradeProperties = ITradeServlet.getMvITradeProperties();

		try
		{
			mvTrace = new sysTrace(); //mvPathname = pathname;
			//BEGIN TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
			String lvDayRetain = IMain.getProperty("log.DayRetain");
			String lvFileNum = IMain.getProperty("log.FileNum");
			String lvFileMaxSize = IMain.getProperty("log.FileMaxSize");
			String lvSMTPRecipients = IMain.getProperty("log.SMTPRecipients");
			String lvSMTPMessageInterval = IMain.getProperty("log.SMTPMessageInterval");
			String lvSMTPHost = IMain.getProperty("log.SMTPHost");
			String lvSMTPUser = IMain.getProperty("log.SMTPUser");
			String lvSMTPPassword = IMain.getProperty("log.SMTPPassword");
			String lvDir = IMain.getProperty("log.dir");
                        boolean lvSystemSetErr = IMain.getProperty("log.SystemSetErr").equals("false")?false:true;
			sysTrace.svApplicationName = IMain.getProperty("log.FileName");
			//END TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
			sysTrace.setLogPath(lvDir);
			sysTrace.setLog(Integer.parseInt(lvDayRetain), Integer.parseInt(lvFileNum), Long.parseLong(lvFileMaxSize));

			File dir = new File(lvDir);

			if (!dir.exists())
			{
				if (!dir.mkdirs())
				{
					throw new IOException("Cannot create directory: " + dir.toString());
				}
			}

			//BEGIN TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
			mvTrace.setTraceLevel(Integer.parseInt(IMain.getProperty("log.Level")));
			mvTrace.initStatic(true, true, false);
			mvTrace.setsvTraceThreadPriority(Integer.parseInt(IMain.getProperty("log.ThreadPriority")));
			//END TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
			if (lvSMTPRecipients != null && !lvSMTPRecipients.equalsIgnoreCase(""))
			{
				SendMail sm = new SendMail(lvSMTPHost, lvSMTPUser, lvSMTPPassword);
				mvTrace.setMail(sm, lvSMTPRecipients.split(","), Integer.parseInt(lvSMTPMessageInterval));
			}
			else
			{
				mvTrace.setMail(null, null, Integer.parseInt(lvSMTPMessageInterval));
			}


            // BEGIN - Task #: TTL-HK-WLCW-00964. - Walter Lau 20100122  Error email content filter

            try{
            //Vector lvFilterVector = new Vector();
            String lvEmailContentFilterList = "thanhsu604@gmail.com,abc@gmail.com";
            java.util.ArrayList lvEmailContentFilterArrayList = new java.util.ArrayList();

            String[] lvEmailContentFilterArray = lvEmailContentFilterList.split(",");

            for(int i = 0; i < lvEmailContentFilterArray.length; i++)
            {
                String lvEmailContentFilter = (String)lvEmailContentFilterArray[i];

                lvEmailContentFilterArrayList.add(lvEmailContentFilter.trim());
            }

            String lvEmailSubject = mvITradeProperties.getProperty("log.EmailSubject");
            String lvErrorNotifyicationCommand = mvITradeProperties.getProperty("log.ErrorNotificationCommand");

            //BEGIN - Task #: TTL-HK-WLCW-00969. - Walter Lau 20100202  Email Sender
            String lvEmailSender = mvITradeProperties.getProperty("log.EmailSender");
            //END - Task #: TTL-HK-WLCW-00969. - Walter Lau 20100202  Email Sender

           mvTrace.setEmailSubject(lvEmailSubject);
          // mvTrace.setEmailContentFilter(lvEmailContentFilterArrayList);
           mvTrace.setErrorNotificationCommand(lvErrorNotifyicationCommand);
           //BEGIN - Task #: TTL-HK-WLCW-00969. - Walter Lau 20100202  Email Sender
           mvTrace.setEmailSender(lvEmailSender);
           //END - Task #: TTL-HK-WLCW-00969. - Walter Lau 20100202  Email Sender

            }catch(Exception ex)
            {
                ex.printStackTrace();
            }

           //END - Task #: TTL-HK-WLCW-00964. - Walter Lau 20100122  Error email content filter


			if (System.getProperty("LogException") == null || System.getProperty("LogException").equalsIgnoreCase("Y"))
                        {
                                try
                                {
                                        java.text.SimpleDateFormat lvDateFormat = new java.text.SimpleDateFormat("yyyyMMdd");
                                        String lvDate = lvDateFormat.format(new Date());
                                        if(lvSystemSetErr)
                                             System.setErr(new ITradePrintStream(new FileOutputStream(lvDir + "/ITrade-" + lvDate + "-EXCEPTION.log", true), true));
                                }
                                catch (Exception e)
                                {
                                        e.printStackTrace();
                                }
                        }
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

	}

}

/**
 * The ITradePrintStream class defined methods that are print stream about ITrade.
 * @author 
 */
class ITradePrintStream extends PrintStream
	{
		/**
		 * This method print OutputStream.
		 * @param pOutputStream The output byte stream.
		 */
		public ITradePrintStream(OutputStream pOutputStream)
		{
			super(pOutputStream);
		}
		/**
		 * This method print OutputStream,and auto flush.
		 * @param pOutputStream The output byte stream.
		 * @param pAutoFlush is auto flush or not.
		 */
		public ITradePrintStream(OutputStream pOutputStream, boolean pAutoFlush)
		{
			super(pOutputStream,pAutoFlush);
		}

		/**
		 * This method to print specified object.
		 * @param pObject the object to be print.
		 */
		public void println(Object pObject)
		{
			if( pObject instanceof Throwable)
			{
				super.println("[TIME] "+new Date());
			}
			super.println(pObject);
		}
	}



/**
 * This class will store data for a linked list node
 */

class LogNode
{
	public String mvThreadName;
	public Object data;
	public int logType;
	public Date logTime;

	/**
	 * Constructor for LogNode class.
	 * @param pThreadName The thread name.
	 * @param pObject An Object.
	 * @param pType Log type.
	 * @param pTime The log date.
	 */
	LogNode(String pThreadName, Object pObject, int pType, Date pTime)
	{
		mvThreadName = pThreadName;
		data = pObject;
		logType = pType;
		logTime = pTime;
	}
}
