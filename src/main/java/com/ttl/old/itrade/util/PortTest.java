package com.ttl.old.itrade.util;

import java.io.PrintWriter;
import java.net.Socket;

import com.ttl.old.itrade.interfaces.PortActionListener;

/**
 * The PortTest class defined methods that are testing port is available or not.
 * @author
 *
 */
public class PortTest implements PortActionListener
{
	PortAction  portAction;

	/**
	 * Constructor for PortTest class.
	 * @param pServerName The server name.
	 * @param pPort the server port.
	 * @param pStatus The specified status.
	 */
	public PortTest(String pServerName, int pPort, String pStatus)
	{
		try
		{
			if (pStatus.equalsIgnoreCase("server"))
			{
				System.out.print("Starting server...");
				portAction = new PortAction(pPort, this);
				portAction.start();
				System.out.println("done");
			}
			else
			{
				System.out.print("Calling server...");
				String  s = PortAction.callServer(pServerName, pPort, "Calling Server...");
				System.out.println("Reply from server: " + s);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * This method to perform print action.
	 * @param pSocket An Socket instance.
	 */
	public void actionPerform(Socket pSocket)
	{
		try
		{
			PrintWriter out = new PrintWriter(pSocket.getOutputStream(), true);
			out.print("In action perform");
			out.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * This is the entrance way to run a program.
	 * @param args Command-line arguments array.
	 */
	public static void main(String[] args)
	{
		new PortTest("formula_one", 8888, args[0]);
	}
}
