package com.ttl.old.itrade.util;

// Author  : Wilfred
// Date    : Jul 18, 2000.

import java.util.Properties;
import java.io.FileInputStream;

/**
 * This class will store tab properties of each action by loading the tab.properties file
 * @author Wilfred
 * @since Jul 18, 2000.
 */
public class TabProperties
{
	private static Properties   prop = null;
	private static String		defaultLocation = "Acct";

	/**
	 * This method load the properties file.
	 * @param pFilepath the full path of the properties file.
	 * @throws Exception If the program does not normally arise during the implementation.
	 */
	public static void load(String pFilepath) throws Exception
	{
		try
		{
			prop = new Properties();
			prop.load(new FileInputStream(pFilepath));
			String  s = prop.getProperty("default");
			if (s != null)
			{
				defaultLocation = s;
			}
		}
		catch (Exception ex)
		{
			Log.println(ex, Log.ERROR_LOG);
			prop = null;
			throw ex;
		}
	}

	/**
	 * This method will get the value by passing a key or null if the properties
	 * has not been load. If there is no such element then the default will be
	 * @param pKey A key of properties.
	 * @return  Keys corresponding to the value.
	 */
	public static String getValue(String pKey)
	{
		if (prop == null)
		{
			return defaultLocation;
		}
		else
		{
			String  temp = prop.getProperty(pKey);
			if (temp == null)
			{
				return defaultLocation;
			}
			else
			{
				return temp;
			}
		}
	}

}
