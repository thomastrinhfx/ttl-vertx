package com.ttl.old.itrade.util;

// Author  : Wilfred Chan
// Date    : Jul 25, 2000.

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/**
 * This class will store all information for MultiFunctionHandler
 * @author Wilfred Chan
 * @since Jul 25, 2000.
 */
public class MultiWinvestResult
{

	int		count;
	int		returnCode;			// return code of URLMultiFunctionReq
	String  returnMsg;			// return message of URLMultiFunctionReq
	Vector  functionNames;		// function names which are going to be call
	Vector  parameters;			// parameters of each functions
	Vector  results;			// contains WinvestResult object of each functions
	Vector  functionIds;		// function ids of each functions

	/**
	 * Constructor for MultiWinvestResult class.
	 */
	public MultiWinvestResult()
	{
		count = 0;
		returnCode = -1;
		returnMsg = null;
		functionNames = new Vector();
		parameters = new Vector();
		results = new Vector();
		functionIds = new Vector();
	}

	/**
	 * This method will add a winvest function call.
	 * @param pFunctionName name of winvest function that is about to be called
	 * @param parameters parameters of that function (Un-encoded)
	 */
	public void add(String pFunctionName, Hashtable parameters)
	{

		// encode all parameters
		String  parameter = URLUtilities.encodeParameters(parameters, "s");

		// add function name, parameter, and empty result to corresponding vector
		functionNames.addElement(pFunctionName);
		this.parameters.addElement(parameter);
		results.addElement(null);
		functionIds.addElement(String.valueOf(count));
		count++;
	}

	/**
	 * This method get enumeration of the components of functionIds.
	 * @return Enumeration of the components of functionIds.
	 */
	public Enumeration keys()
	{
		return functionIds.elements();
	}

	/**
	 * This method get function name.
	 * @param pId An index.
	 * @return Back at the specified index components from functionNames.
	 */
	public String getFunctionName(String pId)
	{
		return (String) functionNames.elementAt(Integer.parseInt(pId));
	}

	/**
	 * This method get parameter.
	 * @param pId An index.
	 * @return Back at the specified index components from parameters.
	 */
	public String getParameter(String pId)
	{
		return (String) parameters.elementAt(Integer.parseInt(pId));
	}

	/**
	 * This method get a result.
	 * @param pId An String index.
	 * @return Back at the specified index components from results.
	 */
	public WinvestResult getResult(String pId)
	{
		return (WinvestResult) results.elementAt(Integer.parseInt(pId));
	}

	/**
	 * This method direct get a result.
	 * @param pPsn An int index.
	 * @return Back at the specified index components from results.
	 */
	public WinvestResult getResult(int pPsn)
	{
		return (WinvestResult) results.elementAt(pPsn);
	}

	/**
	 * Gets  the returns code.
	 * @return The return code.
	 */
	public int getReturnCode()
	{
		return this.returnCode;
	}

	/**
	 * Gets the returns message.
	 * @return The returns message.
	 */
	public String getReturnMessage()
	{
		return this.returnMsg;
	}

	/**
	 * This method will set result of each functions
	 * @param pWinvestResult an WinvestResult object which can be get by calling
	 * doTransaction method of DefaultWinvestHandler class
	 */
	public void setResults(WinvestResult pWinvestResult)
	{

		// get return code and retrun message of the URLMultiFunctionReq
		this.returnCode = pWinvestResult.getReturnCode();
		this.returnMsg = pWinvestResult.getReturnMessage();

		// get all return values of each functions
		Enumeration keys = pWinvestResult.keys();
		String		key, functionId;
		while (keys.hasMoreElements())
		{
			key = (String) keys.nextElement();

			// if it is not irequest, which is just the pass back value and
			// since the key should has "i" as prefix, so check if it is
			if (!key.equalsIgnoreCase("irequest") && key.toLowerCase().startsWith("i"))
			{

				// get the function id by getting the substring from the second char.
				functionId = key.substring(1);

				// set the result of that function by constructing a WinvestResult of
				// the result string
				this.results.setElementAt(new WinvestResult((String) pWinvestResult.getValue(key)), Integer.parseInt(functionId));
			}
		}
	}

	/**
	 * This is a running entrance method.
	 * @param args Command Line parameters array.
	 */
	public static void main(String[] args)
	{
		MultiWinvestResult  mwr = new MultiWinvestResult();
		Hashtable			h = new Hashtable();
		h.put("sql", "select * from dual");
		mwr.add("function1", h);
		mwr.add("function1", h);
	}
}
