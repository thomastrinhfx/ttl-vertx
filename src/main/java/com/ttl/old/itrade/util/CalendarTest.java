package com.ttl.old.itrade.util;

import java.sql.Time;
import java.util.Calendar;

/**
 * The CalendarTest class defined methods to test the property of Calendar class.
 * @author
 *
 */
public class CalendarTest
{
	/**
	 * Constructor for CalendarTest class.
	 */
	public CalendarTest() {}

	/**
	 * This method is a Running entrance method.
	 * @param args The command line array.
	 */
	public static void main(String[] args)
	{

		/*
		 * Calendar c = Calendar.getInstance();
		 * 
		 * System.out.println("today: " + c);
		 * System.out.println("date: " + c.get(Calendar.DATE));
		 * System.out.println("Mon: " + Calendar.MONDAY);
		 * System.out.println("Fri: " + Calendar.FRIDAY);
		 * System.out.println("Sat: " + Calendar.SATURDAY);
		 * System.out.println("Sun: " + Calendar.SUNDAY);
		 * System.out.println("day of week: " + c.get(Calendar.DAY_OF_WEEK));
		 * c.set(Calendar.DATE, 33);
		 * System.out.println("date: " + c.get(Calendar.DATE));
		 * System.out.println("tomorrow: " + c.get(Calendar.DAY_OF_WEEK));
		 */
		Calendar	today = Calendar.getInstance();
		Calendar	nextStart = Calendar.getInstance();

		System.out.println("today: " + today.getTime());

		nextStart.set(Calendar.DATE, today.get(Calendar.DATE) + 1);
		nextStart.set(Calendar.HOUR_OF_DAY, 0);
		nextStart.set(Calendar.MINUTE, 1);
		nextStart.set(Calendar.SECOND, 0);

		System.out.println("nextStart: " + nextStart.getTime());
		Time	t0 = new Time(10, 1, 0);
		Time	t1 = new Time(10, 2, 0);
		System.out.println("time: " + (t0.before(t1)));

	}

}
