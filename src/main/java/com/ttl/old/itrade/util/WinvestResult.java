package com.ttl.old.itrade.util;

// Modified by Wilfred
// Date  : Jul 25, 2000.
// Modification  : add a method "keys" return an enumeration of all fields name

import java.util.Enumeration;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import wb.net.URLDecoder;

/**
 * This class is for return result from Winvest server.
 * @author
 */
public class WinvestResult
{
	private int					_iReturnCode;
	private String				_sReturnMessage;
	/***	added by May on 12-01-2004	- in order to TP_APP_ERR in multi-language	***/
	private String			_sErrorCode;
	/***************************************/
	private Object				_rawData;

	private java.util.Vector	_vFields;
	private java.util.Vector	_vValues;

	static final public Object  EmptyObject = new Object();

	/**
	 * Constructor for WinvestResult class default.
	 */
	public WinvestResult() {}

	/**
	 * This constructor needed to receives the return string.
	 * @param PreturnString the return string.
	 */
	public WinvestResult(String PreturnString)
	{
		_vFields = new java.util.Vector(50);
		_vValues = new java.util.Vector(50);
		Log.println("[ WinvestResult.init() returnStr: " + PreturnString + " ] ", Log.ACCESS_LOG);

		String			sTemp = null;
		String			sField = null;
		String			sValue = null;
		StringTokenizer strTokenizer1 = new StringTokenizer(PreturnString, "&\n");
		int				returnParaNum = strTokenizer1.countTokens();
      Log.println("[ WinvestResult: returnParaNum = " + returnParaNum + " ] ", Log.ERROR_LOG);

		for (int k = 0; k < returnParaNum; k++)
		{

			// Debug.println("[WinvestResult: k = " + k + "]");

			try
			{

				// get the key and value
				sTemp = strTokenizer1.nextToken();
				StringTokenizer strTokenizer2 = new StringTokenizer(sTemp, "=");
				sField = strTokenizer2.nextToken();
				sValue = strTokenizer2.nextToken("=");

				if (sField.equalsIgnoreCase("lreturncode"))
				{
					_iReturnCode = Integer.parseInt(sValue);
				}
				else if (sField.equalsIgnoreCase("sreturnmessage"))
				{
					try
					{
						String  s = URLDecoder.decode(sValue).trim();
						_sReturnMessage = URLDecoder.decode(s).trim();
					}
					catch (Exception e)
					{
						Log.println(e, Log.ERROR_LOG);
						_sReturnMessage = null;
					}
				}
				else
				{
					_vFields.addElement(sField);
					try
					{
						String  s = URLDecoder.decode(sValue);
						_vValues.addElement(URLDecoder.decode(s));

						// Debug.println("[WinvestResult: _vValues = " + URLDecoder.decode(s) + "]");
					}
					catch (Exception e)
					{
						Log.println(e, Log.ERROR_LOG);
						_vValues.addElement(null);
					}

					// Debug.println("[WinvestResult: _vFields = " + sField + "]");

				}
			}
			catch (NoSuchElementException e)
			{

				// Log.println(e, Log.ERROR_LOG);
				// Log.println("[ WinvestResult the field: " + sField + " does not have a value ]", Log.ERROR_LOG);
			}
		}
		Log.println("[WinvestResult: _sReturnMessage = " + _sReturnMessage + "]", Log.ACCESS_LOG);
		Log.println("[WinvestResult: _iReturnCode = " + _iReturnCode + "]", Log.ACCESS_LOG);
	}

	/**
	 * Sets the return code.
	 * @param pCode the return code.
	 */
	public void setReturnCode(int pCode)
	{
		_iReturnCode = pCode;
	}

	/**
	 * Sets the return message.
	 * @param pMessage the return message.
	 */
	public void setReturnMessage(String pMessage)
	{
		_sReturnMessage = pMessage;
	}

	/**
	 * This method give field add value.
	 * @param pField the specific field.
	 * @param pValue the given value.
	 */
	public void addFieldValue(String pField, String pValue)
	{
		_vFields.addElement(pField);
		_vValues.addElement(pValue);
	}

	/***	added by May on 12-01-2004	- in order to display TP_APP_ERR in multi-language	***/
	/**
	 * Sets the error code.
	 * @param pErrorCode the error code.
	 */
	public void setErrorCode(String pErrorCode)
	{
		_sErrorCode = pErrorCode;
	}

	/**
	 * Gets the error code.
	 * @return the error code.
	 */
	public String getErrorCode()
	{
		return _sErrorCode;
	}
	/**********************************************************/

	/**
	 * Sets the raw data.
	 * @param pRawData the raw data.
	 */
	public void setRawData(Object pRawData)
	{
		_rawData = pRawData;
	}

	/**
	 * Gets the raw data.
	 * @return the raw data.
	 */
	public Object getRawData()
	{
		return _rawData;
	}

	/**
	 * Gets the return code.
	 * @return the return code.
	 */
	public int getReturnCode()
	{
		return _iReturnCode;
	}

	/**
	 * Gets the return message.
	 * @return the return message.
	 */
	public String getReturnMessage()
	{
		return _sReturnMessage;
	}

	// Added by Wilfred start Jul 25, 2000.
	/**
	 * This method back to the enumeration of the components of this vector that are _vFields.
	 * @return Enumeration of the components of vector that are _vFields.
	 */
	public Enumeration keys()
	{
		if (_vFields == null)
		{
			return null;
		}
		else
		{
			return _vFields.elements();
		}
	}

	// Added by Wilfred end Jul 25, 2000.

	/**
	 * This method is overloaded to string.
	 */
	public String toString()
	{
		StringBuffer	sb = new StringBuffer("\n");
		for (int i = 0; i < _vFields.size(); i++)
		{
			sb.append("field: ").append(_vFields.elementAt(i)).append(" value: ").append(_vValues.elementAt(i)).append("\n");
		}

		return sb.toString();
	}

	/**
	 * This method according the key get specific object.
	 * @param pKeys the specified key.
	 * @return the specific object.
	 */
	public Object getValue(String pKeys)
	{
		int sFieldLocation = _vFields.indexOf(pKeys);

		if (sFieldLocation != -1)
		{
			return _vValues.elementAt(sFieldLocation);
		}
		else
		{
			return null;
		}
	}
}
