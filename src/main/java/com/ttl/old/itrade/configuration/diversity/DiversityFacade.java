/**
 * 
 *//*
package com.itrade.configuration.diversity;

import java.util.List;

import com.itrade.aastock.EDataFeedSource;
import com.itrade.base.data.DataPadding;
import com.itrade.base.data.ref.HashMapDataPadding;
import com.itrade.configuration.diversity.AppConfiguration.EResourceType;
import com.itrade.configuration.pattern.PatternContext.EPatternType;
import com.itrade.configuration.pattern.PatternEntry;
import com.itrade.util.StringUtils;

*//**
 * <p>Copyright: Copyright(c)Transaction Technologies Limited 20140709 by xuejie.xiao</p>
 * @author jay.wince
 * @Date    20140709
 * @version 1.0
 *//*
public final class DiversityFacade implements IDiversityConfiguration{
	private AppConfiguration mvBaseDiversityConfiguration;
	private AppConfiguration mvOverriderDiversityConfiguration;

	public DiversityFacade(final String pAppID) {
		this.mvBaseDiversityConfiguration = BaseAppDiversityConfiguration.getInstance();
		if (!(StringUtils.isNullStr(pAppID) || 
				BaseAppDiversityConfiguration.BASE_APP_ID.equals(pAppID))) {	
			this.mvOverriderDiversityConfiguration = new AppConfiguration(pAppID);
			this.mvOverriderDiversityConfiguration.setHandler(mvBaseDiversityConfiguration);
		}
	}
	
	private IDiversityConfiguration getDiversityConfiguration(){
		if (this.mvOverriderDiversityConfiguration == null) {
			return this.mvBaseDiversityConfiguration;
		}
		return this.mvOverriderDiversityConfiguration;
	}
	
	private BaseAppDiversityConfiguration getBaseAppDiversityConfiguration(){
		if (this.mvBaseDiversityConfiguration instanceof BaseAppDiversityConfiguration) {
			return (BaseAppDiversityConfiguration)this.mvBaseDiversityConfiguration;
		}
		throw new ClassCastException("this.mvBaseDiversityConfiguration must be derived  from "+BaseAppDiversityConfiguration.class.getSimpleName());
	}

 * Final settings which are forbidden to override.
 * 	
	
//START
	*//**
	 * @author jay.wince
	 * @since  20140724
	 *//*
	@Deprecated
	public HashMapDataPadding<String> companySettings(){
		return getBaseAppDiversityConfiguration().final_CompanySettings();
	}
//ENDING
	*//**
	 * @author jay.wince
	 * @since  20140724
	 *//*
	public HashMapDataPadding<String> dashboardPadding() {
		return getDiversityConfiguration().dashboardPadding();
	}

	@Override
	*//**
	 * @author jay.wince
	 * @since  20140724
	 *//*
	public HashMapDataPadding<String> fileSettingsInPreference() {
		return getDiversityConfiguration().fileSettingsInPreference();
	}

	@Override
	*//**
	 * @author jay.wince
	 * @since  20140724
	 *//*
	public String getResourceRef(EResourceType rt) {	
		return getDiversityConfiguration().getResourceRef(rt);
	}

	@Override
	*//**
	 * @author jay.wince
	 * @since  20140725
	 *//*
	public HashMapDataPadding<String> themePadding() {
		return getDiversityConfiguration().themePadding();
	}

	@Override
	*//**
	 * @author jay.wince
	 * @since  20140725
	 *//*
	public HashMapDataPadding<String> themeOptions() {
		return getDiversityConfiguration().themeOptions();
	}
	
	*//**
	 * @author pengjm
	 * @since  20140725
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> getAAStockThumbnailStockchartImageView() {
		return getDiversityConfiguration().getAAStockThumbnailStockchartImageView();
	}
	*//**
	 * @author pengjm
	 * @since  20140725
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> pagingStyle(String pModule) {
		return getDiversityConfiguration().pagingStyle(pModule);
	}
	*//**
	 * @author pengjm
	 * @since  20140725
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> loginOptions() {
		return getDiversityConfiguration().loginOptions();
	}
	*//**
	 * @author pengjm
	 * @since  20140725
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> login() {
		return getDiversityConfiguration().login();
	}
	*//**
	 * @author pengjm
	 * @since  20140725
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> orderColumns() {
		return getDiversityConfiguration().orderColumns();
	}
	
//BEGIN TASK # :TTL-GZ-kelly.kuang-00006 20151120[ITradeR5]Add raw columns config.(ITRADEFIVE-697)
	*//**
	 * @author kelly.kuang
	 * @since  20151120
	 * 
	 *//*
	@Override
	public String orderSettingRawColumns() {
		return getDiversityConfiguration().orderSettingRawColumns();
	}
	*//**
	 * @author kelly.kuang
	 * @since  20151120
	 * 
	 *//*
	@Override
	public String orderDefaultRawColumns() {
		return getDiversityConfiguration().orderDefaultRawColumns();
	}
//END TASK # :TTL-GZ-kelly.kuang-00006 20151120[ITradeR5]Add raw columns config.(ITRADEFIVE-697)
	
	*//**
	 * @author pengjm
	 * @since  20140725
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> getTypeSet() {
		return getDiversityConfiguration().getTypeSet();
	}
	*//**
	 * @author bo.li
	 * @since  20140726
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> getAAStockIndice() {
		return getDiversityConfiguration().getAAStockIndice();
	}
	*//**
	 * @author bo.li
	 * @since  20140726
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> getAAStockFavorite() {
		return getDiversityConfiguration().getAAStockFavorite();
	}
	*//**
	 * @author bo.li
	 * @since  20140726
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> showAAStockIndice() {
		return getDiversityConfiguration().showAAStockIndice();
	}
	*//**
	 * @author bo.li
	 * @since  20140726
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> getAAStockNews() {
		return getDiversityConfiguration().getAAStockNews();
	}
	*//**
	 * @author bo.li
	 * @since  20140726
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> getPriceAlertColumns() {
		return getDiversityConfiguration().getPriceAlertColumns();
	}
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]APP configuration 	(ITRADEFIVE-219)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140727
	 * 
	 *//*
	public  HashMapDataPadding<String> dashboard(){
		return getBaseAppDiversityConfiguration().dashboard();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]APP configuration 	(ITRADEFIVE-219)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]APP configuration 	(ITRADEFIVE-219)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140727
	 * 
	 *//*
	public HashMapDataPadding<String> version(){
		return getBaseAppDiversityConfiguration().version();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]APP configuration 	(ITRADEFIVE-219)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]APP configuration 	(ITRADEFIVE-219)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140727
	 * 
	 *//*
	public HashMapDataPadding<String> customerService(){
		return getBaseAppDiversityConfiguration().customerService();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]APP configuration 	(ITRADEFIVE-219)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]APP configuration 	(ITRADEFIVE-219)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140727
	 * 
	 *//*
	public HashMapDataPadding<String> customerServicePhoneNumber(){
		return getBaseAppDiversityConfiguration().customerServicePhoneNumber();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]APP configuration 	(ITRADEFIVE-219)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]UI configuration 	(ITRADEFIVE-220)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140727
	 * 
	 *//*
	public HashMapDataPadding<String> common(){
		return getDiversityConfiguration().common();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]UI configuration 	(ITRADEFIVE-220)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140727
	 * 
	 *//*
	public HashMapDataPadding<String> messageOptions(){
		return getDiversityConfiguration().messageOptions();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140727
	 * 
	 *//*
	public HashMapDataPadding<String> ctOptions(){
		return getDiversityConfiguration().ctOptions();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]APP configuration 	(ITRADEFIVE-219)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140727
	 * 
	 *//*
	public HashMapDataPadding<String> language(){
		return getDiversityConfiguration().language();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]APP configuration 	(ITRADEFIVE-219)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140727
	 * 
	 *//*
	public HashMapDataPadding<String> currency(){
		return getDiversityConfiguration().currency();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> market(){
		return getDiversityConfiguration().market();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> regex(){
		return getDiversityConfiguration().regex();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> channel(){
		return getDiversityConfiguration().channel();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> defaultValue(){
		return getDiversityConfiguration().defaultValue();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> enterOrderValue(){
		return getDiversityConfiguration().enterOrderValue();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> goodTillDate(){
		return getDiversityConfiguration().goodTillDate();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> modifyOrder(){
		return getDiversityConfiguration().modifyOrder();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> cancelOrder(){
		return getDiversityConfiguration().cancelOrder();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]UI configuration 	(ITRADEFIVE-220)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> orderMapping(){
		return getDiversityConfiguration().orderMapping();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]UI configuration 	(ITRADEFIVE-220)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]UI configuration 	(ITRADEFIVE-220)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> orderhistoryCloumns(){
		return getDiversityConfiguration().orderhistoryCloumns();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]UI configuration 	(ITRADEFIVE-220)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]UI configuration 	(ITRADEFIVE-220)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> historyMapping(){
		return getDiversityConfiguration().historyMapping();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]UI configuration 	(ITRADEFIVE-220)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]UI configuration 	(ITRADEFIVE-220)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> historyValue(){
		return getDiversityConfiguration().historyValue();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]UI configuration 	(ITRADEFIVE-220)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> tranhistoryCloumns(){
		return getDiversityConfiguration().tranhistoryCloumns();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> tranhistoryValue(){
		return getDiversityConfiguration().tranhistoryValue();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> summaryValue(){
		return getDiversityConfiguration().summaryValue();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> summaryCloumns(){
		return getDiversityConfiguration().summaryCloumns();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]UI configuration 	(ITRADEFIVE-220)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> accountBalanceCloumns() {
		return getDiversityConfiguration().accountBalanceCloumns();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]UI configuration 	(ITRADEFIVE-220)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> orderdetail() {
		return getDiversityConfiguration().orderdetail();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> IPOModule() {
		return getDiversityConfiguration().IPOModule();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> ipoPasswordConfirm() {
		return getDiversityConfiguration().ipoPasswordConfirm();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> hottestCloumns() {
		return getDiversityConfiguration().hottestCloumns();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> statusCloumns() {
		return getDiversityConfiguration().statusCloumns();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> listFocusCloumns() {
		return getDiversityConfiguration().listFocusCloumns();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> caPasswordConfirm() {
		return getDiversityConfiguration().caPasswordConfirm();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> caCloumns() {
		return getDiversityConfiguration().caCloumns();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> changePassword() {
		return getDiversityConfiguration().changePassword();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> storeOptions() {
		return getDiversityConfiguration().storeOptions();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> getAAStockSettings() {
		return getDiversityConfiguration().getAAStockSettings();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> getAAStockSnapshotDataFeedOptions() {
		return getDiversityConfiguration().getAAStockSnapshotDataFeedOptions();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> getAAStockSnapshotParas() {
		return getDiversityConfiguration().getAAStockSnapshotParas();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> passwordEncrypt(String pModule) {
		return getDiversityConfiguration().passwordEncrypt(pModule);
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]UI configuration 	(ITRADEFIVE-220)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> groupSupported(String pModule) {
		return getDiversityConfiguration().groupSupported(pModule);
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]UI configuration 	(ITRADEFIVE-220)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]UI configuration 	(ITRADEFIVE-220)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> pagesize(String pModule) {
		return getDiversityConfiguration().pagesize(pModule);
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]UI configuration 	(ITRADEFIVE-220)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]UI configuration 	(ITRADEFIVE-220)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public String displayMode(String nodeName) {
		return getDiversityConfiguration().displayMode(nodeName);
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]UI configuration 	(ITRADEFIVE-220)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public PatternEntry getPatternEntry(EPatternType type) {
		return getDiversityConfiguration().getPatternEntry(type);
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public String reject() {
		return getDiversityConfiguration().reject();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)

	@Override
	*//**
     * @author canyong.lin
     * @since 20140807
     *//*
	public HashMapDataPadding<String> sessionPadding() {
		 return getDiversityConfiguration().sessionPadding();
	}
	*//**
	 * @author bo.li
	 * @since  20140807
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> enterOrderStockIdHandle() {
		return getDiversityConfiguration().enterOrderStockIdHandle();
	}
	*//**
	 * @author bo.li
	 * @since  20140818
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> dataFeedProductionSettings() {
		return getDiversityConfiguration().dataFeedProductionSettings();
	}
	*//**
	 * @author bo.li
	 * @since  20140818
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> dataFeedUATSettings() {
		return getDiversityConfiguration().dataFeedUATSettings();
	}
	*//**
	 * @author bo.li
	 * @since  20140818
	 * 
	 *//*
	@Override
	public String getdataFeedUATSettings(EDataFeedSource source, String leaf) {
		return getDiversityConfiguration().getdataFeedUATSettings(source, leaf);
	}
	*//**
	 * @author bo.li
	 * @since  20140818
	 * 
	 *//*
	@Override
	public String getAAStockURISettings(EDataFeedSource source, String leaf) {
		return getDiversityConfiguration().getAAStockURISettings(source, leaf);
	}
//BEGIN TASK #:TTL-GZ-BO.LI-00030 20140824[ITradeR5]Indices group(ITRADEFIVE-237)
	*//**
	 * @author bo.li
	 * @since  20140823
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> AAStockIndiceShowGroup() {
		return getDiversityConfiguration().AAStockIndiceShowGroup();
	}
//END TASK #:TTL-GZ-BO.LI-00030 20140824[ITradeR5]Add TickerView(ITRADEFIVE-237)
//BEGIN TASK #:TTL-GZ-BO.LI-00039 2014-09-10[ITradeR5]TickerView group(ITRADEFIVE-250)
	*//**
	 * @author bo.li
	 * @since  20140910
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> TickerViewShowGroup() {
		return  getDiversityConfiguration().TickerViewShowGroup();
	}
//END TASK #:TTL-GZ-BO.LI-00039 2014-09-10[ITradeR5]TickerView group(ITRADEFIVE-250)
	*//**
	 * @author bo.li
	 * @since  20140920
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> FavoriteStockSearch() {
		return  getDiversityConfiguration().FavoriteStockSearch();
	}

	*//**
     * @author canyong.lin
     * @since 20140920
     *//*
	@Override
	public HashMapDataPadding<List<String>> atmosphereTopic() {
		return getDiversityConfiguration().atmosphereTopic();
	}
	*//**
     * @author canyong.lin
     * @since 20140924
     *//*
	@Override
	public HashMapDataPadding<String> aastockNewsPadding() {
		return getDiversityConfiguration().aastockNewsPadding();
	}
	
//BEGIN TASK #:TTL-GZ-Jay-00200 20140925[ITradeR5]AAStock streaming quote[ITRADEFIVE-286]
//BEGIN TASK #:TTL-GZ-kelly.kuang-00000 20151209[ITradeR5]Support multiMarket info.[ITRADEFIVE-701]
	@Override
	public DataPadding<String> getAAStockMarketInfoLoginSettings(String pMarketID) {
		return getDiversityConfiguration().getAAStockMarketInfoLoginSettings(pMarketID);
	}
//END TASK #:TTL-GZ-kelly.kuang-00000 20151209[ITradeR5]Support multiMarket info.[ITRADEFIVE-701]
//END   TASK #:TTL-GZ-Jay-00200 20140925[ITradeR5]AAStock streaming quote[ITRADEFIVE-286]

	*//**
	 *@author junming.peng
	 *@since 20141016
	 *//*
	@Override
	public HashMapDataPadding<String> placeOrderQuoteEnable() {
		return getDiversityConfiguration().placeOrderQuoteEnable();
	}

	*//**
	 * @author jay.wince
	 * @since  20141022
	 *//*
    @Override
    public HashMapDataPadding<String> MDSConnectionSettings()
    {
        return getDiversityConfiguration().MDSConnectionSettings();
    }
	*//**
     * @author bo.li
     * @since 20140108
     *//*
	@Override
	public HashMapDataPadding<String> supportMargin() {
		return getDiversityConfiguration().supportMargin();
	}
	*//**
     * @author bo.li
     * @since 20141117
     *//*
	@Override
	public HashMapDataPadding<String> potfolioFilter() {
		return getDiversityConfiguration().potfolioFilter();
	}
	*//**
     * @author bo.li
     * @since 20141117
     *//*
	@Override
	public HashMapDataPadding<String> BankAccountBalanceFilter() {
		return getDiversityConfiguration().BankAccountBalanceFilter();
	}
	
	*//**
     * @author junming.peng
     * @since  20141108
     * 
     *//*
    @Override
    public int getTradingMainDayTradeRecord()
    {
        return getDiversityConfiguration().getTradingMainDayTradeRecord();
    }

    *//**
     * @author piece.lin	
     * @since  20141119
     * 
     *//*
	@Override
	public HashMapDataPadding<String> tableFieldSorting(String pModel) {
		// TODO Auto-generated method stub
		return getDiversityConfiguration().tableFieldSorting(pModel);
	}

	*//**
     * @author piece.lin	
     * @since  20141119
     * 
     *//*
	@Override
	public HashMapDataPadding<String> settingsDataPadding(final String pModule) {
		// TODO Auto-generated method stub
		return getDiversityConfiguration().settingsDataPadding(pModule);
	}
	*//**
	 * @author bo.li
	 * @since  20141125
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> getFundManagementPadding(String pModel) {
		return getDiversityConfiguration().getFundManagementPadding(pModel);
	}
	
	*//**
	 * @author canyong.lin
	 * @since 20141201
	 *//*
	@Override
	public HashMapDataPadding<String> getTokenConfig(String fid) {
		return getDiversityConfiguration().getTokenConfig(fid);
	}
	*//**
     * @author junming.peng
     * @since 20141208
     *//*
    @Override
    public HashMapDataPadding<String> allowModifyOrder()
    {
        return getDiversityConfiguration().allowModifyOrder();
    }
    *//**
     * @author junming.peng
     * @since 20141208
     *//*
    @Override
    public HashMapDataPadding<String> nonAllowModifyOrder()
    {
        return getDiversityConfiguration().nonAllowModifyOrder();
    }
    *//**
     * @author junming.peng
     * @since 20141208
     *//*
    @Override
    public HashMapDataPadding<String> allowCancelOrder()
    {
        return getDiversityConfiguration().allowCancelOrder();
    }
	*//**
     * @author junming.peng
     * @since 20141215
     *//*
    @Override
    public HashMapDataPadding<String> getSortable(String pModule)
    {
        return getDiversityConfiguration().getSortable(pModule);
    }
    *//**
	 * @author bo.li
	 * @since  20141224
	 * 
	 *//*
	@Override
	public String getAAStockHostDataPadding(EDataFeedSource source,String leaf) {
		return  getDiversityConfiguration().getAAStockHostDataPadding(source,leaf);
	}
    *//**
	 * @author bo.li
	 * @since  20141224
	 * 
	 *//*
	@Override
	public String getAAStockURIDataPadding(EDataFeedSource source,String leaf) {
		return  getDiversityConfiguration().getAAStockURIDataPadding(source,leaf);
	}
	  *//**
     * @author jay.wince
     * @since  20141211
     *//*
    @Override
    public HashMapDataPadding<String> findFunctionWidgetById(String id)
    {
        return getDiversityConfiguration().findFunctionWidgetById(id);
}

    @Override
    *//**
     * Pending to implement if required.
     * @author jay.wince
     * @since  20141223
     * @return
     *//*
    public HashMapDataPadding<String> defaultDashboardDefinition()
    {
        return null;
    }

	@Override
	*//**
     * @author canyong.lin
     * @since 20150116
     *//*
	public HashMapDataPadding<String> actionMapping(String actionID,String param) 
	{
		return getDiversityConfiguration().actionMapping(actionID, param);
	}

	@Override
	*//**
     * @author canyong.lin
     * @since 20150116
     *//*
	public HashMapDataPadding<String> functionDefinition(String fid)
	{
		return getDiversityConfiguration().functionDefinition(fid);
	}

	@Override
	*//**
	 * @author canyong.lin
	 * @since 20150127
	 *//*
	public HashMapDataPadding<String> getTokenUsage(String fid) {
		return getDiversityConfiguration().getTokenUsage(fid);
	}

	@Override
	*//**
	 * @author canyong.lin
	 * @since 20150128
	 *//*
	public HashMapDataPadding<String> getFunctionGroup(String fid) {
		return getDiversityConfiguration().getFunctionGroup(fid);
	}

	@Override
	*//**
	 * @author canyong.lin
	 * @since 20150129
	 *//*
	public HashMapDataPadding<String> getCSRFConfig() {
		return getDiversityConfiguration().getCSRFConfig();
	}
	*//**
     * @author junming.peng
     * @since 20150210
     *//*
    @Override
    public String getTradingSituation()
    {
        return getDiversityConfiguration().getTradingSituation();
    }
    *//**
	 * @author kelly.kuang
	 * @since 20170411
	 *//*
    @Override
    public boolean getTradingSameSessionID()
    {
        return getDiversityConfiguration().getTradingSameSessionID();
    }
	*//**
     * @author junming.peng
     * @since 20150331
     *//*
    @Override
    public String getDNTopicName()
    {
        return getDiversityConfiguration().getDNTopicName();
    }
    *//**
     * @author canyong.lin
     * @since 20150413
     *//*
	@Override
	public String getPrimarykey(String fid) {
		return getDiversityConfiguration().getPrimarykey(fid);
	}

     
     * @Author junming.peng 
     * @Date 2015-06-09 13:58:02
     * (non-Javadoc)	
     * @see com.itrade.configuration.diversity.IBLDiversity#getHistoryFunctionTab()
     
    @Override
    public String getHistoryFunctionTab()
    {
        return getDiversityConfiguration().getHistoryFunctionTab();
    }

     
     * @Author junming.peng
     * @Date 2015-07-03 10:41:41
     * (non-Javadoc)	
     * @see com.itrade.configuration.diversity.IBLDiversity#getMarketOrderType(java.lang.String)
     
    @Override
    public String getMarketOrderType(String pMarket)
    {
        return getDiversityConfiguration().getMarketOrderType(pMarket);
    }
    
     * @Author bo.li
     * @Date 2015-07-23 17:16:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IBLDiversity#getFilledEnquiryStatus()
     
    @Override
    public String getFilledEnquiryStatus()
    {
        return getDiversityConfiguration().getFilledEnquiryStatus();
    }
     
     * @Author demonwx.gu
     * @Date 2015-07-28 10:41:41
     * (non-Javadoc)    
     * @see com.itrade.configuration.diversity.IBLDiversity#getSpecialClient(java.lang.String)
     
    @Override
    public String getSpecialClient(String pFlag)
    {
        return getDiversityConfiguration().getSpecialClient(pFlag);
    }
    
     * @Author jummyjw.liu
     * @Date 2015-08-19 10:41:41
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IDiversityConfiguration#getSupportBrowser()
     
    @Override
    public HashMapDataPadding<List<String>> getSupportBrowser()
    {
        return getDiversityConfiguration().getSupportBrowser();
    }
    
//BEGIN TASK # :TTL-GZ-kelly.kuang-00005 20151109[ITradeR5]get recovery function group config.(ITRADEFIVE-693)
    
     * @Author kelly.kuang
     * @Date 2015-11-09 14:37:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IBLDiversity#getRecoveryFunctionGroup()
     
    public HashMapDataPadding<String> getRecoveryFunctionGroup(String fid) {
		return getDiversityConfiguration().getRecoveryFunctionGroup(fid);
	}
	
    
     * @Author kelly.kuang
     * @Date 2015-11-09 14:37:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IBLDiversity#getRecoveryFunction()
     
	public HashMapDataPadding<String> getRecoveryFunction(String fgid,String select) {
		return getDiversityConfiguration().getRecoveryFunction(fgid,select);
	}
//END TASK # :TTL-GZ-kelly.kuang-00005 20151109[ITradeR5]get recovery function group config.(ITRADEFIVE-693)

//BEGIN TASK # :TTL-GZ-kelly.kuang-00005 20151113[ITradeR5]get recovery function config which is not in group.(ITRADEFIVE-693)
	
     * @Author kelly.kuang
     * @Date 2015-11-13 15:03:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IBLDiversity#getNormalRecoveryFunction()
     
	public HashMapDataPadding<String> getNormalRecoveryFunction(String fid) {
		return getDiversityConfiguration().getNormalRecoveryFunction(fid);
	}
//END TASK # :TTL-GZ-kelly.kuang-00005 20151113[ITradeR5]get recovery function config which is not in group.(ITRADEFIVE-693)

//BEGIN TASK # :TTL-GZ-kelly.kuang-00008 20151203[ITradeR5]Add GoodTillDate config.(ITRADEFIVE-713)
	
     * @Author kelly.kuang
     * @Date 2015-12-03 15:03:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IUIDiversity#displayTodayValueInGoodTillDateComboBox()
     
	@Override
	public boolean displayTodayValueInGoodTillDateComboBox() {
		return getDiversityConfiguration().displayTodayValueInGoodTillDateComboBox();
	}
	
	
     * @Author kelly.kuang
     * @Date 2015-12-03 15:03:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IBLDiversity#allowGoodTillDate()
     
	@Override
	public boolean allowGoodTillDate() {
		return getDiversityConfiguration().allowGoodTillDate();
	}
//END TASK # :TTL-GZ-kelly.kuang-00008 20151203[ITradeR5]Add GoodTillDate config.(ITRADEFIVE-713)
	
//BEGIN TASK #:TTL-GZ-kelly.kuang-00009 20151210[ITradeR5-CISI]get Daily Quota config.[ITRADEFIVE-714]
	
     * @Author kelly.kuang
     * @Date 2015-12-10 15:03:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IBLDiversity#getMarketQuotaBalance()
     
	@Override
	public HashMapDataPadding<String> getMarketQuotaBalance() {
		return getDiversityConfiguration().getMarketQuotaBalance();
	}
//END TASK #:TTL-GZ-kelly.kuang-00009 20151210[ITradeR5-CISI]get Daily Quota config.[ITRADEFIVE-714]
	
//BEGIN TASK #:TTL-GZ-STEVENZG.LI-00023 2015-12-29[ITradeR5]Support to resize the table's column width.(ITRADEFIVE-733)
	*//**
	 * @author stevenzg.li
	 * @Date 2015-12-28
	 *//*
	@Override
	public HashMapDataPadding<String> getResizable(String pModule) {
	    return getDiversityConfiguration().getResizable(pModule);
	}
//END TASK #:TTL-GZ-STEVENZG.LI-00023 2015-12-29[ITradeR5]Support to resize the table's column width.(ITRADEFIVE-733)

//BEGIN TASK #:TTL-GZ-STEVENZG.LI-00025 2016-01-07[ITradeR5-CISI]Add Identity Validation Function.(ITRADEFIVE-741)
	*//**
     * @author stevenzg.li
     * @Date 2016-01-07
     *//*
    @Override
    public HashMapDataPadding<String> getIdentityValidationSetting() {
        return getDiversityConfiguration().getIdentityValidationSetting();
    }
//END TASK #:TTL-GZ-STEVENZG.LI-00025 2016-01-07[ITradeR5-CISI]Add Identity Validation Function.(ITRADEFIVE-741)

    *//**
     * @author stevenzg.li
     * @Date 2016-01-08
     *//*
    @Override
    public HashMapDataPadding<String> getCustomerService() {
        return getDiversityConfiguration().getCustomerService();
    }

    *//**
     * @author stevenzg.li
     * @Date 2016-01-08
     *//*
    @Override
    public HashMapDataPadding<String> getCustomerServicePhoneNumber() {
        return getDiversityConfiguration().getCustomerServicePhoneNumber();
    }
    
//BEGIN TASK # :TTL-GZ-kelly.kuang-00039 20160318[ITradeR5-CISI]Get password field config.(ITRADEFIVE-787)
    
     * @Author kelly.kuang
     * @Date 2016-03-18 16:16:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IBLDiversity#getPlaceOrderConfirmFlow()
     
    @Override
	public HashMapDataPadding<String> getPlaceOrderConfirmFlow() {
		return getDiversityConfiguration().getPlaceOrderConfirmFlow();
	}
    
    
     * @Author kelly.kuang
     * @Date 2016-03-18 16:16:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IBLDiversity#getModifyOrderConfirmFlow()
     
    @Override
	public HashMapDataPadding<String> getModifyOrderConfirmFlow() {
		return getDiversityConfiguration().getModifyOrderConfirmFlow();
	}
    
    
     * @Author kelly.kuang
     * @Date 2016-03-18 16:16:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IBLDiversity#getCancelOrderConfirmFlow()
     
    @Override
	public HashMapDataPadding<String> getCancelOrderConfirmFlow() {
		return getDiversityConfiguration().getCancelOrderConfirmFlow();
	}
//END TASK # :TTL-GZ-kelly.kuang-00039 20160318[ITradeR5-CISI]Get password field config.(ITRADEFIVE-787)
    
//BEGIN TASK # :TTL-GZ-kelly.kuang-00039 20160405[ITradeR5-CISI]Add default confirm password config.(ITRADEFIVE-787)
    
     * @Author kelly.kuang
     * @Date 2016-04-05 11:04:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IBLDiversity#getDefaultEnterOrderConfirmFlow()
     
    @Override
	public HashMapDataPadding<String> getDefaultEnterOrderConfirmFlow() {
		return getDiversityConfiguration().getDefaultEnterOrderConfirmFlow();
	}
//END TASK # :TTL-GZ-kelly.kuang-00039 20160318[ITradeR5-CISI]Add default confirm password config.(ITRADEFIVE-787)

//BEGIN TASK #:TTL-GZ-STEVENZG.LI-00053 2016-07-13[ITradeR5]Add "Open Account" Link to Login Page.
    @Override
    public HashMapDataPadding<String> getOpenAccountInfo()
    {
        return getDiversityConfiguration().getOpenAccountInfo();
    }
//END TASK #:TTL-GZ-STEVENZG.LI-00053 2016-07-13[ITradeR5]Add "Open Account" Link to Login Page.

    *//**
     * @Author STEVENZG.LI
     * @Date 2016-09-08
     * @see com.itrade.configuration.diversity.IBLDiversity#getRunModeConfigInfo()
     *//*
    @Override
    public HashMapDataPadding<String> getRunModeConfigInfo()
    {
        return getDiversityConfiguration().getRunModeConfigInfo();
    }

    @Override
    public HashMapDataPadding<String> getAuthenticationOTPConfig(String pModule)
    {
        return getDiversityConfiguration().getAuthenticationOTPConfig(pModule);
    }

}
*/