/**
 * 
 *//*
package com.itrade.configuration.diversity;

import java.util.List;

import com.itrade.base.data.ref.HashMapDataPadding;
import com.itrade.configuration.diversity.AppConfiguration.EResourceType;

*//**
 * <p>Copyright: Copyright(c)Transaction Technologies Limited 20140708 by xuejie.xiao</p>
 * @author jay.wince
 * @Date    20140708
 * @version 1.0
 *//*
public interface IDiversityConfiguration extends IUIDiversity,IBLDiversity{
	//BEGIN TASK #:TTL-GZ-CANYONG.LIN-00032 20150313[ITrader5]"entities" node.(ITRADEFIVE-508)
	public final static String ENTITIES = "entities";
	//END   TASK #:TTL-GZ-CANYONG.LIN-00032 20150313[ITrader5]"entities" node.(ITRADEFIVE-508)
	*//**
	 * @author jay.wince
	 * @since  20140724
	 *//*
    public abstract HashMapDataPadding<String> fileSettingsInPreference();
    *//**
	 * @author jay.wince
	 * @since  20140724
	 *//*
    public abstract HashMapDataPadding<String> dashboardPadding();
    *//**
	 * @author jay.wince
	 * @since  20140725
	 *//*
    public abstract String getResourceRef(EResourceType rt);
    
    *//**
     * @author canyong.lin
     * @since 20140807
     *//*
    public abstract HashMapDataPadding<String> sessionPadding();
    
    *//**
     * @author canyong.lin
     * @since 20140920
     *//*
    public abstract HashMapDataPadding<List<String>> atmosphereTopic();

    
    *//**
     * @author jay.wince
     * @since  20141022
     *//*
    public abstract HashMapDataPadding<String> MDSConnectionSettings();

    
    *//**
     * @author jay.wince
     * @since  20141211
     * @param id
     * @return
     *//*
    public abstract HashMapDataPadding<String> findFunctionWidgetById(final String id);
    *//**
     * @author jay.wince
     * @since  20141223
     * @return
     *//*
    public abstract HashMapDataPadding<String> defaultDashboardDefinition();
    *//**
     * @author canyon.lin
     * @since 20150129
     *//*
    public abstract HashMapDataPadding<String> getCSRFConfig();
    
    *//**
     * @author jummyjw.liu
     * @since 20150819
     *//*
    public abstract HashMapDataPadding<List<String>> getSupportBrowser();
    
//BEGIN TASK #:TTL-GZ-STEVENZG.LI-00026 2016-01-08[ITradeR5-CISI]Add CITIC Disclaimer setting.(ITRADEFIVE-742)
    *//**
     * @author stevenzg.li
     * @since 2016-01-08
     *//*
    public abstract HashMapDataPadding<String> getCustomerService();
    
    *//**
     * @author stevenzg.li
     * @since 2016-01-08
     *//*
    public abstract HashMapDataPadding<String> getCustomerServicePhoneNumber();
    
//END TASK #:TTL-GZ-STEVENZG.LI-00026 2016-01-08[ITradeR5-CISI]Add CITIC Disclaimer setting.(ITRADEFIVE-742)
    public abstract HashMapDataPadding<String> language();
}
*/