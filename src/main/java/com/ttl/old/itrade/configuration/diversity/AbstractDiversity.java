/**
 * 
 *//*
package com.itrade.configuration.diversity;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import com.itrade.base.IUniqueIdentifier;
import com.itrade.base.data.DataPadding;
import com.itrade.base.data.ref.HashMapDataPadding;
import com.itrade.base.io.InputStreamProcessingTemplate;
import com.itrade.base.io.InputStreamProcessor;
import com.itrade.base.jesapi.ITradeAPI;
import com.itrade.configuration.Configuration;
import Log;
import com.itrade.util.StringUtils;
import com.itrade.web.engine.util.JSONParse;
import com.itrade5.util.StdLoggerUtil;

*//**
 * <p>Copyright: Copyright(c)Transaction Technologies Limited 20140708 by xuejie.xiao</p>
 * @author jay.wince
 * @Date   20140708
 * @version 1.0
 *//*
public abstract class AbstractDiversity<Handler> implements Configuration,IUniqueIdentifier {
	
	protected final String NODE_ENTRIES = "entries";
	protected final String NODE_ENTRY   = "entry";
	
	protected final static String LEADING_SLASH = "/";
	protected final static String DOUBLE_LEADING_SLASH = "//";
	protected final static String HYPEN_TEXT = "-";
    
	protected String            mvSID;
	protected Document     mvDocument;
	private   long 		 lastModified;
	protected Handler          handler;
	
	protected final String CONFIG_PATH_SUFFIX = "-diversity-config.xml";
	public AbstractDiversity(final String pSID) {
		this.mvSID = pSID.toLowerCase();//Jay on 2015-09-23: For AIX os,letter case is sensitive!
		this.loadConfiguration();
	}
	
	@Override
	public void loadConfiguration() {
		File f  = null;
		if (System.getProperty("testing.working.directory")==null) {
		    f = ITradeAPI.getSecureFile(getPath());		
		}else{
			f = new File(System.getProperty("testing.working.directory")+getPath());
		}		
		//check update or not.
		if ((f.lastModified()==lastModified) && mvDocument!=null) {
			return;
		}
		if (lastModified==Long.parseLong("0")) {
			lastModified = f.lastModified();
		}	
		Log.println(this.getClass().getName()+"#loadConfiguration:file path:"+f.getAbsolutePath(), Log.ACCESS_LOG);

//BEGIN TASK # :TTL-GZ-kelly.kuang-00003 20151016[ITradeR5]Support ITradeInclude tag in config xml.(ITRADEFIVE-681)
//BEGIN TASK # :TTL-GZ-kelly.kuang-00003 20151224[ITradeR5]Enhance load include config xml logic, separate 2 part to load config.(ITRADEFIVE-681)	
//kelly.kuang on 20151224:load main config xml
		InputStreamProcessingTemplate.process(f.getAbsolutePath(), new InputStreamProcessor() {
			@Override
			public void process(InputStream input) throws IOException {
				SAXReader reader = new SAXReader();
				reader.setEncoding(UTF8);
				try {
					mvDocument = reader.read(input);
				} catch (DocumentException e) {
					String errormsg = "Parse settings from "+getPath()+" error!"+e.getMessage();
					StdLoggerUtil.log(errormsg, 3);
					Log.println(errormsg, Log.ERROR_LOG);
					throw new IOException(errormsg, e);
				}		
			}
		});
		
//kelly.kuang on 20151224:load each include config xml
		if(mvDocument != null){
			List<Element> includeList = mvDocument.getRootElement().selectNodes(DOUBLE_LEADING_SLASH + "ITradeInclude");
			for(Element includeElement : includeList){
				String from = includeElement.attributeValue("file").trim();
				String to = includeElement.attributeValue("parentXPath").trim();
				try{
					final File fromMatchFile = findFromFile(f.getParent(),from);
					final Element toElement = findToElement(mvDocument,to);
					InputStreamProcessingTemplate.process(fromMatchFile.getAbsolutePath(), new InputStreamProcessor() {
						@Override
						public void process(InputStream input) throws IOException {
							SAXReader reader = new SAXReader();
							reader.setEncoding(UTF8);
							try {
								Document fromDocument = reader.read(input);
								List<Element> children = fromDocument.getRootElement().elements();
								for(Element child : children){
									toElement.add((Element)child.clone());
								}
							} catch (DocumentException e) {
								String errormsg = "Parse settings from "+fromMatchFile.getAbsolutePath()+" error! "+e.getMessage();
								StdLoggerUtil.log(errormsg, 3);
								Log.println(errormsg, Log.ERROR_LOG);
								throw new IOException(errormsg, e);
							}		
						}
					});
					includeElement.getParent().remove(includeElement);
				} catch (Exception e){
					String errormsg = "Parse settings from "+from+" error! "+e.getMessage();
					StdLoggerUtil.log(errormsg, 3);
					Log.println(errormsg, Log.ERROR_LOG);
				}
			}
		}
//END TASK # :TTL-GZ-kelly.kuang-00003 20151016[ITradeR5]Enhance load include config xml logic, separate 2 part to load config.(ITRADEFIVE-681)
//END TASK # :TTL-GZ-kelly.kuang-00003 20151016[ITradeR5]Support ITradeInclude tag in config xml.(ITRADEFIVE-681)
	}

//BEGIN TASK # :TTL-GZ-kelly.kuang-00003 20151016[ITradeR5]Support ITradeInclude tag in config xml.(ITRADEFIVE-681)

	*//**
     * get the element which the ITradeInclude xml will put into, according to the <ITradeInclude parentXPath=""/>.
     * @author kelly.kuang
     * @since  20151016
     * @param element
     *//*
	private Element findToElement(Document doc, String parentXPath) throws Exception{
		Element toElement = null;
//kelly.kuang on 20151016:empty and "/" use root element.
		if("".equalsIgnoreCase(parentXPath) || "/".equalsIgnoreCase(parentXPath)){
			toElement = doc.getRootElement();
		}else if(parentXPath.startsWith("/")){
			toElement = (Element) doc.selectSingleNode(parentXPath);
		}else{
			toElement = (Element) doc.getRootElement().selectSingleNode(parentXPath);
		}
		if(toElement==null){
			throw new Exception("Missing parent Element("+parentXPath+") of include xml.");
		}
		return toElement;
	}
	
	*//**
     * find the matched config xml according to the <ITradeInclude file=""/>.
     * @author kelly.kuang
     * @since  20151016
     * @param file list
     *//*
	private File findFromFile(String folderPath, String from) throws Exception {
		String fromFilePath  = folderPath + LEADING_SLASH + this.mvSID + "-" + from + getPathSuffix();
//kelly.kuang on 20151016:auto add prefix and suffix to find config file.
		File fromFile =new File(fromFilePath);
		if(fromFile.exists()){
			return fromFile;
		} else {
			throw new Exception("Missing include xml: "+fromFile.getName());
		}
	}
//END TASK # :TTL-GZ-kelly.kuang-00003 20151016[ITradeR5]Support ITradeInclude tag in config xml.(ITRADEFIVE-681)
	
	protected String check(String base,String overrider){
		if(base !="" ){
			if(overrider !=""){
				base = overrider;
			}
		}else{
			return overrider;
		}
		return base;
	}
	protected HashMapDataPadding<String> merge(HashMapDataPadding<String> base,
											   HashMapDataPadding<String> overrider){
		if(overrider == null  || overrider.original().size()==0){
			 return base;
			 return base == null ? new HashMapDataPadding<String>() : base;
		}
		if (base == null || base.original().size()==0) {
			return overrider;
			return overrider == null ? new HashMapDataPadding<String>() : overrider;
		}
		base.putAll(overrider);
		return base;
	}
	
	protected HashMapDataPadding<String> invokeMethod(String method,final String xpath){
        return invokeMethod0(method,xpath);
	}
	@SuppressWarnings("unchecked")
	protected HashMapDataPadding<String> invokeMethod0(String method,Object... args){
        if (method==null) {
			method = "getdataPadding";
		}
		Method method0 = null;
		Method method1 = null;
//TODO#:@Jay:Pending to discovery the issues here.		
		Class[] paramClasses = new Class[args.length];
		   
        for (int i = 0, j = args.length; i < j; i++) {    
        	paramClasses[i] = args[i].getClass();      	       
        }    
        
		try {
			method0 = this.getClass().getMethod(method, paramClasses);
			if (this.handler != null) {
				method1 = this.handler.getClass().getMethod(method,paramClasses);
			}
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		HashMapDataPadding<String> overrider  	 = null;
		HashMapDataPadding<String> base          = null;
		try {
			overrider= (HashMapDataPadding<String>)method0.invoke(this, args);
			if (!(method1 == null || this.handler == null)) {
				base = (HashMapDataPadding<String>)method1.invoke(this.handler,args);
			}
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return merge(base, overrider);
	
	}
	*//**
	 * 
	 * @param xpath  Relative to root element.
	 * @return
	 *//*
	protected HashMapDataPadding<String> pathResolve(final String xpath){
		return invokeMethod(null, xpath);
	}
	
BEGIN TASK #:TTL-GZ-Jay-00176 20140523[ITradeR5]Recursively handle support.		
    *//**
     * Parse the text that is from the map to document.
     * @author jay.wince
     * @since  20140526
     * @param map
     * @param key
     * @return
     *//*
	protected Document parseText(DataPadding<String> map,final String key) {
		return parseText(map.get(key));
	}
	protected Document parseText(String pXMLText){
	    Document document = null;
	    try {
            document = DocumentHelper.parseText(pXMLText);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
	    return document;
	}
	protected Element parseTextToElement(DataPadding<String> map,final String key){
		Document document = parseText(map, key);
		Element element = document.getRootElement();
		return element;
	}
	*//**
	 * Construct a map from a element.
	 * @author jay.wince
	 * @param element
	 * @since 20140526
	 * @return
	 *//*
	protected HashMapDataPadding<String> elementToMap(Element element){
		HashMapDataPadding<String> dataPadding = new HashMapDataPadding<String>();
	    if (element != null) {
// 1.No child elements ,retrieve the attribute(s) to populate. 			
		    if (element.elements().size()<1) {
		    	Iterator<Attribute> lvAttrIterator =element.attributeIterator();
				   while (lvAttrIterator.hasNext()) {
					   Attribute attribute = lvAttrIterator.next();
					   dataPadding.put(attribute.getName(), attribute.getText());
				   }
			}else {
				Iterator<Element> elementIterator = element.elementIterator();
// 2.Child elements to retrieve.				
				while (elementIterator.hasNext()) {
					Element childElement = (Element) elementIterator.next();
//@Jay:If has child elements,then keep the structure of the current node in order to further process.
					List<Element> lvLeafNodes = childElement.elements();
					if (lvLeafNodes.size()>0) {
						dataPadding.put(childElement.getName(), childElement.asXML());
// ---------------------Comment the code fragment above<-Jay on 20141212
// ---------------------Jay on 20141212:Parse the xml text more one level.
					    Map<String,String> lvLeafMap = new HashMap<String,String>();
                        for (Element lvLeafNode : lvLeafNodes)
                        {
                            lvLeafMap.put(lvLeafNode.getName(), lvLeafNode.getTextTrim());
                        }
                        dataPadding.put(childElement.getName().trim(), lvLeafMap);

					}else{
						if (childElement.attributeCount()>0) {//Retrieve the attributes if end with </xxx>
						   Iterator<Attribute> lvAttriIterator = childElement.attributeIterator();
						   while (lvAttriIterator.hasNext()) {
							   Attribute attribute = lvAttriIterator.next();
							   dataPadding.put(attribute.getName(), attribute.getText());
						   }
// SSOL-AttrParse					   
//							dataPadding.put(childElement.getName(),childElement.asXML());
// ESOL-AttrParse					   
						}else{
							dataPadding.put(childElement.getName(), childElement.getText());
						}
					}
				}
//3.Check attributes as well<-Jay on 20141211
				if (element.attributeCount()>0)
                {
				    Iterator<Attribute> lvAttrIterator =element.attributeIterator();
                    while (lvAttrIterator.hasNext()) 
                    {
                       Attribute attribute = lvAttrIterator.next();
                       dataPadding.put(attribute.getName(), attribute.getText());
                    }
                }
			}
	    }
	    return dataPadding;
	}
ENDING TASK #:TTL-GZ-Jay-00176 20140523[ITradeR5]Recursively handle support.	
	public  HashMapDataPadding<String> getdataPadding(String xpath){
		Element element = (Element)this.mvDocument.getRootElement().selectSingleNode(xpath);
		return elementToMap(element);
	}
	*//**
	 * Node structrue like this:
	 * <Parent-Node>       
	 *        <ChildNode-1>nodevalue1</ChildNode-1>
	 *        <ChildNode-2>nodevalue2</ChildNode-2>
	 * </Parent-Node>
	 * <default>
          <ChildNode-1>defaultValue1</ChildNode-1>
	 * </default>
	 * @param nodeName : Leaf node.
	 * @param paths    : a path array that route the node deeper into the leaf node,if relative,please use double leading slash.
	 * @author jay.wince
	 * @since
	 * @return
	 *//*
	public HashMapDataPadding<String> fallbackDefault(String nodeName,String... paths){
		String parentPath = null;		
		if(paths == null || paths.length == 0){
			parentPath = "";
		}else{
			parentPath = StringUtils.join(paths, "/");
		}
		String xpath = parentPath + nodeName;
		HashMapDataPadding<String> resultMap = getdataPadding(xpath);
		if(resultMap.original().size()==0){
 			xpath =  parentPath.replace(paths[paths.length-1],"") + "default/"+nodeName;
 			resultMap = getdataPadding(xpath);
		}
		return resultMap;
	}
	*//**
	 * Fall back to get the default node relative to the target node if the target node doesn't exist.
	 * Node structrue like this:
	 * <Parent-Node>
	 *        <default>default value</default>
	 *        <ChildNode-1>nodevalue1</ChildNode-1>
	 *        <ChildNode-2>nodevalue2</ChildNode-2>
	 * </Parent-Node>
	 * @author jay.wince
	 * @since  20140604
	 * @param root
	 * @param nodeName
	 * @param paths
	 * @return
	 *//*
	public String fallbackDefaultString(Node root,String nodeName,String... paths){
		String parentPath = null;		
		if(paths == null || paths.length == 0){
			parentPath = "";
		}else{
			parentPath = StringUtils.join(paths, "/");
			
		}
		if (parentPath.length()>0 && !parentPath.endsWith("/")) {
			parentPath = parentPath + "/";
		}
		if(nodeName==null) nodeName = "default";
		String xpath = parentPath + nodeName;
		Element element = (Element)root.selectSingleNode(xpath);
		String text = "";
		if(element!=null){
			text = element.getText();
		}else if(!"default".equals(nodeName)){
			xpath = parentPath + "default";
			element = (Element)root.selectSingleNode(xpath);
			text = element == null ? null:element.getText();
		}
		return text;
	}
	public abstract void setHandler(Handler handler);
	protected abstract String getPath();
	
//BEGIN TASK # :TTL-GZ-kelly.kuang-00003 20151016[ITradeR5]add getPathSuffix method to get the config path suffix.(ITRADEFIVE-681)
	*//**
     * get the path suffix.
     * @author kelly.kuang
     * @since  20151016
     * @param path suffix
     *//*
	protected abstract String getPathSuffix();
//END TASK # :TTL-GZ-kelly.kuang-00003 20151016[ITradeR5]add getPathSuffix method to get the config path suffix.(ITRADEFIVE-681)
	
	*//**
     * 1.Pending to implement:exception handle.
     * @author jay.wince
     * @since  20141219
     * @param pBase
     * @param pOverrider
     * @return
     *//*
    protected String mergeByJSON(final String pBase,final String pOverrider){
        String lvFinal = null;
        HashMapDataPadding<String> lvUrlBase      = elementToMap(parseText(pBase).getRootElement());
        HashMapDataPadding<String> lvUrlOverrider = elementToMap(parseText(pOverrider).getRootElement()); 
        lvFinal = JSONParse.writeJSON(merge(lvUrlBase, lvUrlOverrider).original());
        return lvFinal;
    }
	@Override
	public String toString() {
		return "app id:"+this.mvSID
				+ ",in the form of XML configuration:"+this.mvDocument.asXML();
	}
}
*/