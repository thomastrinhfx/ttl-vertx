package com.ttl.old.itrade.interfaces;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import com.ttl.old.itrade.hks.bean.HKSMarketIndexBean;
import com.ttl.old.itrade.hks.bean.HKSMultiOrderBean;
import com.ttl.old.itrade.hks.bean.HKSWorkMarketIndexBean;
import com.ttl.old.itrade.hks.util.HKSInstrument;
import com.ttl.old.itrade.spyder.Spider;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.ttl.old.itrade.util.SessionTracker;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLParser;

/**
 * The IMain abstract class defines methods that get config information and register request.
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public abstract class IMain
{ 
	public static Properties _pIni;
	
	//BEGIN TASK: TTL-GZ-XYL-00045 XuYuLong 20091211 [iTrade R5] Plugin redesign and implement
	public static Properties _pluginIni;
	/**
	 * Returns the plugin config information.
	 * @return plugin config information.
	 */
	public static Properties get_pluginIni() {
		return _pluginIni;
	}
	/**
	 * Sets the plugin config information.
	 * @param pIni plugin config information.
	 */
	public static void set_pluginIni(Properties pIni) {
		_pluginIni = pIni;
	}
	//END TASK: TTL-GZ-XYL-00045 XuYuLong 20091211 [iTrade R5] Plugin redesign and implement
	
	public static SessionTracker localSessionTracker;
	protected Hashtable actions;
	protected IMsgXMLParser ivParser;

	// BEGIN TASK #:  Giang Tran  2011/05/26 Add multi order Hash Map
	private static HashMap mvMultiOrderMap = new HashMap<String, List<HKSMultiOrderBean>>();
	// END TASK #:  Giang Tran  2011/05/26 Add multi order Hash Map
	
	 // BEGIN Task #:  VanTran  2010/05/20 Add clientWatchList Hash Map
	 public static HashMap mvClientWatchList = new HashMap<String, List<String>>();
	 /**
	 * @return the mvClientWatchList
	 */
	public static HashMap getMvClientWatchList() {
		return mvClientWatchList;
	}
	/**
	 * @param mvClientWatchList the mvClientWatchList to set
	 */
	public static void setMvClientWatchList(HashMap mvClientWatchList) {
		IMain.mvClientWatchList = mvClientWatchList;
	}	
	// END Task #:  VanTran  2010/05/20 Add clientWatchList Hash Map
	public static ConcurrentHashMap<String, SortedMap<Long, String>> stockHistMap = new ConcurrentHashMap<String, SortedMap<Long,String>>(){

		@Override
		public SortedMap<Long, String> put(String key, SortedMap<Long, String> value) {
			lastStockUpdated = System.currentTimeMillis();
			return super.put(key, value);
		}
		
	};
	
	private static long lastStockUpdated = 0;
	
	public static long getLastStockUpdated() {
		return lastStockUpdated;
	}
	
	// Begin Task: 2010-06-21 VanTran add concurrent hashmap for market index data
	@SuppressWarnings("unchecked")
	public static ConcurrentHashMap<String, HKSMarketIndexBean> mvMarketIndex = new ConcurrentHashMap<String, HKSMarketIndexBean>();
	// End Task: 2010-06-21 VanTran add concurrent hashmap for market index data
	
	// Begin Task: 2010-08-25 VanTran add concurrent hashmap for market status
	@SuppressWarnings("unchecked")
	public static ConcurrentHashMap<String, String> mvMarketStatus = new ConcurrentHashMap<String, String>();
	// End Task: 2010-08-25 VanTran add concurrent hashmap for market  status
	
	// Begin Task: 2010-09-26 - Giang Tran, add concurrent HashMap for world market index
	@SuppressWarnings("unchecked")
	public static ConcurrentHashMap<Integer, HKSWorkMarketIndexBean> mvWorldMarketIndex = new ConcurrentHashMap<Integer, HKSWorkMarketIndexBean>();
	// End Task: 2010-09-26 - Giang Tran, add concurrent HashMap for world market index
	
	// Begin Task: 2010-10-18 - Van Tran, add concurrent HashMap for  market index
	@SuppressWarnings("unchecked")
	public static ConcurrentHashMap<Integer, HashMap<String,List>> mvMarketHistoricalIndex = new ConcurrentHashMap<Integer, HashMap<String,List>>();
	// END Task: 2010-10-18 - Van Tran, add concurrent HashMap for  market index
	
	// Begin Task: 2011-01-13 - VanTran Add hasmap for best price
	public static ConcurrentHashMap<String, String> mvBestPrice = new ConcurrentHashMap<String, String>();
	// Begin Task: 2011-01-13 - VanTran Add hasmap for best price
	
	// Begin Task: 2010-11-17 - VanTran, flag of job clear pre historical data
	public static boolean mvIsRunClearPreHistoricalData =false;
	// End Task: 2010-11-17 - Van Tran, flag of job clear pre historical data
	
	/**
	 * Start authenticator local session.
	 */
	public abstract void startAuthenticator();
	/**
	 * This method registration specified request.
	 * @param pPath the path of request xml.
	 * @param pEntityID the entity ID.
	 * @param pTPAgentID the agent ID of TP.
	 * @param pTPAgentPassword the agent password of TP.
	 * @param pBranchID the branch ID.
	 * @param pFunctionName the function name.
	 */
    public abstract void startRequestRegistration(String pPath, String pEntityID, String pTPAgentID, String pTPAgentPassword, String pBranchID, String pFunctionName);
    /**
     * This method registration specified external request.
     * @param pPath the path of request xml.
 	 * @param pEntityID the entity ID.
 	 * @param pTPAgentID the agent ID of TP.
 	 * @param pTPAgentPassword the agent password of TP.
 	 * @param pBranchID the branch ID.
 	 * @param pFunctionName the function name.
 	 * @param pRequestXMLListStr Request XML list string
     */
    public abstract void startExternalRequestRegistration(String pPath, String pEntityID, String pTPAgentID, String pTPAgentPassword, String pBranchID, String pFunctionName, String pRequestXMLListStr);
	
	//BEGIN TASK: TTL-GZ-XYL-00045 XuYuLong 20091211 [iTrade R5] Plugin redesign and implement
    /**
	 * This method registration specified request of Plugin.
	 * @param pPath the path of request xml.
	 * @param pEntityID the entity ID.
	 * @param pTPAgentID the agent ID of TP.
	 * @param pTPAgentPassword the agent password of TP.
	 * @param pBranchID the branch ID.
	 * @param pFunctionName the function name.
	 */
	public abstract void startPluginRequestRegistration(String pPath, String pEntityID, String pTPAgentID, String pTPAgentPassword, String pBranchID, String pFunctionName);
	/**
	 * This method registration external specified request of Plugin.
	 * @param pPath the path of request xml.
	 * @param pEntityID the entity ID.
	 * @param pTPAgentID the agent ID of TP.
	 * @param pTPAgentPassword the agent password of TP.
	 * @param pBranchID the branch ID.
	 * @param pFunctionName the function name.
	 * @param pPluginRequestXMLListStringToExternalTP Plugin request XML list string to external TP
	 */
	public abstract void startPluginExternalRequestRegistration(String pPath, String pEntityID, String pTPAgentID, String pTPAgentPassword, String pBranchID, String pFunctionName, String pPluginRequestXMLListStringToExternalTP);
	//END TASK: TTL-GZ-XYL-00045 XuYuLong 20091211 [iTrade R5] Plugin redesign and implement
	/**
	 * Start Server socket.
	 * @param pAppletServerPort the Server Port.
	 */
	public abstract void startOthers(int pAppletServerPort);
	/**
	 * Start broadcast Manager.
	 */
	public abstract void startBroadcastManager();
	/**
	 * Start Login Thread to connect TP.
	 */
	public abstract void onTPConnect();
	/**IMain
	 * Start external Login Thread to connect TP.
	 */
	public abstract void onExternalTPConnect(); //F is Front Office : E is External Server

	/**
	 * Returns the local SessionTracker object.
	 * @return local SessionTracker object.
	 */
	public SessionTracker getLocalSessionTracker()
	{
		return localSessionTracker;
	}

	/**
	 * This method get IMsgXMLNode of request xml.
	 * @param pPath the path of request xml.
	 * @param pFileName the xml file name.
	 * @param pEntityID the entity ID.
	 * @param pAgentID the agent ID of TP.
	 * @param pAgentPassword the agent password of TP.
	 * @param pBranchID the branch ID.
	 * @return request XML result string.
	 */
	protected IMsgXMLNode getRequestXML(String pPath, String pFileName, String pEntityID, String pAgentID, String pAgentPassword, String pBranchID)
	{
		IMsgXMLNode lvNode = null;
        try{
            lvNode = ivParser.parseXML(readFile(pPath + pFileName));
        }catch (Error err)
        {
        	Log.println("Error: " + err, Log.ACCESS_LOG);
            err.printStackTrace();
        }
        catch (Exception e)
        {
        	Log.println("Exception; " + e, Log.ACCESS_LOG);
            e.printStackTrace();
        }
		IMsgXMLNode lvTmpNode = null;

		if (lvNode.getName().equalsIgnoreCase("COROL001Q01")) // this is operator/agent login message
		{
			// change operator id
			lvTmpNode = lvNode.getChildNode("OPERATORID");
			if (lvTmpNode != null)
			{
				lvTmpNode.setValue(pAgentID);
			}
			// change password
			lvTmpNode = lvNode.getChildNode("PASSWORD");
			if (lvTmpNode != null)
			{
				lvTmpNode.setValue(pAgentPassword);
			}
		}

		// change EntityID
		lvTmpNode = lvNode.getChildNode("MESSAGE");
		if (lvTmpNode != null)
		{
			lvTmpNode = lvTmpNode.getChildNode("ENTITYID");
			if (lvTmpNode != null)
			{
				lvTmpNode.setValue(pEntityID);
			}
		}

		lvTmpNode = lvNode.getChildNode("ENTITYID");
		if (lvTmpNode != null)
		{
			lvTmpNode.setValue(pEntityID);
		}
		// change branch id
		lvTmpNode = lvNode.getChildNode("BRANCHID");
		if (lvTmpNode != null)
		{
			lvTmpNode.setValue(pBranchID);
		}

		lvNode.setAttribute("oprId", pAgentID);
		lvNode.setAttribute("pwd", pAgentPassword);
		lvNode.setAttribute("issueLoc", pBranchID);

		return lvNode;
	}

   /**
	* Read a XML file and return the file content as a string
	* @param	pFileName	Full file path and name
	* @return	String		File content
	* @throws FileNotFoundException If the file does not exist, or if it is a directory, rather than a regular file, or because some other reason can not be opened for operation.
	* @throws IOException If an I / O error.
	*/
   public String readFile(String pFileName) throws FileNotFoundException, IOException
    {
	   BufferedReader  lvReader = new BufferedReader(new InputStreamReader(new FileInputStream(pFileName)));
	   StringBuffer	sb = new StringBuffer();
	   String			s = "";
	   while ((s = lvReader.readLine()) != null)
	   {
		   sb.append(s);
	   }
	   return sb.toString().replace('\t', ' ');
   }

   /**
    * This method add action to map.
    * @param pName The action name.
    * @param pAction The action object.
    */
   protected void addAction(String pName, Action pAction)
   {
	   actions.put(pName, pAction);
   }

   /**
    * Sets the config information
    * @param pIni the config information
    */
   public void setIni(Properties pIni)
   {
	   _pIni = pIni;
   }

   /**
    * Sets the Hasgtable object.
    * @param pActions Hasgtable object,it package some action object.
    */
   public void setActions(Hashtable pActions)
   {
	   actions = pActions;
   }

   /**
    * Sets the IMsgXMLParser object.
    * @param pParser the IMsgXMLParser object.
    */
   public void setParser(IMsgXMLParser pParser)
   {
	   ivParser = pParser;
   }

   /**
    * Returns the config information
    * @return config information
    */
   public Properties getIni()
   {
	   return _pIni;
   }

   /**
    * Returns the Hasgtable object.
    * @return Hasgtable object,it package some action object.
    */
   public Hashtable getActions()
   {
	   return actions;
   }

   /**
    * Returns the IMsgXMLParser object.
    * @return IMsgXMLParser object.
    */
   public IMsgXMLParser getParser()
   {
	   return ivParser;
   }
   
   //BEGIN TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
   /**
   	 * Getting ini property value by property key.
	 * Returning null object if the property key is a null object or a 0-length String object
	 * Returning the ini property value from plugin ini if it exists both in plugin ini and System-Default ini.
	 * @param String pKey : The property key.
	 * @return 
	 * 		(1)A null object if the property key is a null object or a 0-length String object;
	 * 		(2)The property value; 
	 * 		(3)A null object if the property key does not exist in ini.
	 * @author YuLong.Xu
	 */
	public static String getProperty(String pKey) {
		if (pKey == null || pKey.equals("")) {
			Log.print("Error: Getting property from ini by invalid key : " + pKey + ", returning: null.", Log.ERROR_LOG);
			return null;
		}
		return getProperty(pKey, null);
	}
	
	 /**
   	 * Getting ini property value by property key.
	 * Returning default value if the property key is a null object or a 0-length String object
	 * Returning the ini property value from plugin ini if it exists both in plugin ini and System-Default ini.
	 * @param String pKey : The property key.
	 * @return 
	 * 		(1)Default value if the property key is a null object or a 0-length String object;
	 * 		(2)The property value; 
	 * 		(3)Default value if the property key does not exist in ini.
	 * @author YuLong.Xu
	 */
	public static String getProperty(String pKey, String pDefaultValue) {
		if (pKey == null || pKey.equals("")) {
			Log.print("Warning: Getting property from ini by invalid key : " + pKey + ", returning default value: " + pDefaultValue + ".", Log.ALERT_LOG);
			return pDefaultValue;
		}
		if(_pluginIni != null){
			String lvValue = _pluginIni.getProperty(pKey);
			return lvValue != null ? lvValue : (_pIni.getProperty(pKey) != null ? _pIni.getProperty(pKey) : pDefaultValue);
		}else{
			String lvValue = _pIni.getProperty(pKey);
			return lvValue != null ? lvValue : pDefaultValue;
		}
	}
	//END TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
	
	// BEGIN - TASK : Nghia Nguyen 20100722 - historical trade data
	public static synchronized void addTradingHistData(String pInstrumentID, Long pTimestamp,
			String pData) {
		SortedMap<Long, String> tmp = stockHistMap.get(pInstrumentID);
		if (tmp == null) {
			tmp = new TreeMap<Long, String>();
		}
		// put data into the individual stock map
		tmp.put(pTimestamp, pData);
		// put the hashmap into the trade history map of all stocks
		//Log.println("Add to map:"+tmp, Log.DEBUG_LOG);
		stockHistMap.put(pInstrumentID, tmp);
		
		// write to a file for backup
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	    Calendar c1 = Calendar.getInstance();
	    String filePrefix = sdf.format(c1.getTime());
		writeHistDataToFile(pInstrumentID+"|"+pTimestamp+"|"+pData, filePrefix);
	}
	//END - TASK : Nghia Nguyen 20100722 - historical trade data
	
	// BEGIN - TASK : Van Tran  201103211 - add trading historical data using sequence as key
	public static synchronized void addTradingHistDataSeq(String pInstrumentID, Long pSequence,
			String pData) {
		SortedMap<Long, String> tmp = stockHistMap.get(pInstrumentID);
		if (tmp == null) {
			tmp = new TreeMap<Long, String>();
		}
		// put data into the individual stock map
		tmp.put(pSequence, pData);
		// put the hashmap into the trade history map of all stocks
		//Log.println("Add to map:"+tmp, Log.DEBUG_LOG);
		stockHistMap.put(pInstrumentID, tmp);
		
		// write to a file for backup
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	    Calendar c1 = Calendar.getInstance();
	    String filePrefix = sdf.format(c1.getTime());
		writeHistDataToFile(pInstrumentID+"|"+pSequence+"|"+pData, filePrefix);
	}
	// END - TASK : Van Tran  201103211 - add trading historical data using sequence as key
	
	//BEGIN - TASK : Nghia Nguyen 20100722 - loading historical trade data from file
	public static void loadStockHistData() {
		InputStream fstream = null;
		DataInputStream in = null;
		BufferedReader br = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		    Calendar c1 = Calendar.getInstance();
		    String filePrefix = sdf.format(c1.getTime());
		    File f = new File(IMain.getProperty("HistDataFileDir")+filePrefix+".txt");
		    if (!f.exists()) {
		    	f.createNewFile();
		    }
		    
			fstream = new FileInputStream(IMain.getProperty("HistDataFileDir")+filePrefix+".txt");
			// Get the object of DataInputStream
			in = new DataInputStream(fstream);
			br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			// Read File Line By Line
			String []tmp;
			SortedMap<Long, String> map;
			String lvMarketID;
			String lvInstrumentID;
			while ((strLine = br.readLine()) != null) {
				//System.out.println("strLine:"+strLine);
				//Log.println(strLine, Log.ERROR_LOG);
				tmp = strLine.split("\\|");
				lvMarketID = tmp[9];
				lvInstrumentID = tmp[0];
				map = stockHistMap.get(tmp[0]);
				if (map == null) {
					map = new TreeMap<Long, String>();
					// Set Open Price with first match price
					   HKSInstrument lvHKSInstrument = (HKSInstrument) TPManager.mvInstrumentVector
						.get(lvMarketID.concat("|").concat(lvInstrumentID.toUpperCase()));
					   if(lvHKSInstrument != null){
						   lvHKSInstrument.setMvOpenPrice(tmp[4]);
					   }
				}
				//System.out.println(tmp[0] +":"+tmp[1]);
				map.put(new Long(tmp[1]), strLine.substring((tmp[0] + tmp[1]).length()+2));
				stockHistMap.put(tmp[0],map);
			}
			
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
			Log.println(e, Log.ERROR_LOG);
		} finally {

			try {
				br.close();
				in.close();
				fstream.close();
			} catch (Exception e) {
				Log.println(e, Log.ERROR_LOG);
			}
		}
	}
    //END - TASK : Nghia Nguyen 20100722 - loading historical trade data from file
	
	//BEGIN - TASK : Van Tran 20100802 - add variable to store the FO server time
	public static long SERVER_TIME;
	//END - TASK : Van Tran 20100802 - add variable to store the FO server time
	
	// Begin Task: TTLVN - Giang Tran 20100926 - Add function to get the World market index
	public static void getWorkMarketIndex(){
		
		try {
			// Market Index List
			String indexs = IMain.getProperty("worldMarketIndex");
			String webSite = IMain.getProperty("worldDataURL");
			if(indexs != null && indexs.trim().length() > 0 
					&& webSite != null && webSite.trim().length() > 0){
				
				Spider spider = new Spider();
				spider.clear();
				
				String[] indexList = indexs.split(",");
				if(indexList != null && indexList.length > 0){
					for(int i = 0; i < indexList.length; i++){
						String url = webSite + indexList[i];
						URL base = new URL(url);
						spider.addURL(base);
					}
				}
				
				// call begin
				spider.begin();
				
				// get market data
			    List dataStr = spider.getMarketDataList();
			    
			    if(dataStr != null && dataStr.size() > 0){
			    	//writeDataToFile(dataList, "workmarketdata");
			    	HKSWorkMarketIndexBean bean = null;
			    	for(int i=0; i < dataStr.size(); i++){
			    		String marketData = dataStr.get(i).toString();
			    		if(marketData != null && marketData.trim().length() > 0){
			    			
			    			String[] tmp = marketData.split("\\|");
			    			if(tmp != null && tmp.length == 4){
			    				bean = new HKSWorkMarketIndexBean();
			    				bean.setIndexName(tmp[0].replaceAll("Average", "Avg").replaceAll("Industrial", "Indus."));
			    				bean.setIndexValue(tmp[1]);
			    				bean.setIndexChange(tmp[2]);
			    				bean.setPercentChange(tmp[3]);
			    				
			    				mvWorldMarketIndex.put(i, bean);
			    				
			    			}
			    		}
			    		
		        	}
			    	
			    }			    
			}
		}
		catch (Exception e){
			Log.println(e, Log.ERROR_LOG);
		}
		
	}
	// Begin Task: TTLVN - Giang Tran 20100926 - Add function to get the World market index
	
	//BEGIN - TASK : Nghia Nguyen 20100722 - historical trade data
	public static void writeHistDataToFile(String pData, String pFilePrefix) {
		FileWriter fstream = null;
		BufferedWriter out = null;
		String fileName = IMain.getProperty("HistDataFileDir")+pFilePrefix+".txt";
		try {
			fstream = new FileWriter(fileName,true);
	        out = new BufferedWriter(fstream);
	    	out.write(pData + System.getProperty("line.separator"));
		} catch (Exception e) {
			Log.print(e, Log.ERROR_LOG);
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				Log.print(e, Log.ERROR_LOG);
			}
		}
	}
	//END - TASK : Nghia Nguyen 20100722 - historical trade data
	public static void setMvMultiOrderMap(HashMap mvMultiOrderMap) {
		IMain.mvMultiOrderMap = mvMultiOrderMap;
	}
	public static HashMap getMvMultiOrderMap() {
		return mvMultiOrderMap;
	}
}
