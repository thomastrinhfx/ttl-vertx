package com.ttl.old.itrade.interfaces;

/**
 * The Action interface defined methods that do some base action.
 * @author
 *
 */
public interface Action
{ 
	/**
	 * This method inti environment,to do some base operation.
	 */
	void init();
	/**
	 * This method to get configuration of SingleSignOnPhase.
	 * @return The value of SingleSignOnPhase.
	 */
    public String getSingleSignOnPhase();
}
