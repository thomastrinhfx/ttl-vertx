package com.ttl.old.itrade.interfaces;
import java.net.Socket;

/**
 * This interface will determine what action should be perform when someone reach the port
 * @author Wilfred Chan
 * @since  Jul 25, 2000.
 */
public interface PortActionListener
{
	/**
	 * This method to do some operation when this listener to be triggered.
	 * @param pSocket An object of Socket.
	 */
	public void actionPerform(Socket pSocket);

}
