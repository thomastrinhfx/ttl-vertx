package com.ttl.old.itrade.base.dashboard.model;

import java.io.Serializable;
/**
 * 
 * @author jay.wince
 * @since 2013-12-02
 */
public class DashboardLayoutModel implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 973016966341277842L;
	private String id;
    private String className;
	private String thumbnail;
    private String html;
    private String description;
    private boolean ready;
    
    public DashboardLayoutModel(){};
    public DashboardLayoutModel(String layoutID){
    	this.id = layoutID;
    };
    public String getThumbnail() {
    	return thumbnail;
    }
    public void setThumbnail(String thumbnail) {
    	this.thumbnail = thumbnail;
    }
    public String getHtml() {
    	return html;
    }
    public void setHtml(String html) {
    	this.html = html;
    }
    public String getDescription() {
    	return description;
    }
    public void setDescription(String description) {
    	this.description = description;
    }
    public boolean isReady() {
    	return ready;
    }
    public void setReady(boolean ready) {
    	this.ready = ready;
    }
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getClassName() {
		return this.className;
	}

	public void setClassName(String className) {
		this.className = className;
	}
	
}
