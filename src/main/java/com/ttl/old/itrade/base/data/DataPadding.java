/*package com.itrade.base.data;

import static com.itrade.tag.ExceptionOptionTag.DATA_KEY;
import static com.itrade.tag.ExceptionOptionTag.ELEMENT_KEY;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.itrade.base.data.ref.HashMapDataPadding;
import com.itrade.base.exception.ITradeNoSuchElementException;
import com.itrade.base.jesapi.ITradeAPI;

*//**
 * 
 * @author jay.wince
 * @since  2014-01-22
 *//*
public abstract class DataPadding<V> implements Parameters<String, V> {
    protected Map<String, V> map = null;
    private boolean lock = false;
    public DataPadding (){super();};
    public DataPadding (Map<String,V> source){
    	this.map = source;
    }
    protected void throwNoSuchElementException(String paramString,String paraString2,Exception ex) {
    	DataPadding<Object> lvOptions = new HashMapDataPadding<Object>();
    	lvOptions.put(ELEMENT_KEY, paramString);
    	lvOptions.put(DATA_KEY, this);
    	ITradeAPI.exceptionHandler().raise(ITradeNoSuchElementException.class, map.getClass()+"#"+paraString2+"(String "+paramString+")", ex.getMessage(), lvOptions);
    }
    
    protected void throwNoSuchElementException(String paramString,String paraString2,String errorMsg) {
    	DataPadding<Object> lvOptions = new HashMapDataPadding<Object>();
    	lvOptions.put(ELEMENT_KEY, paramString);
    	lvOptions.put(DATA_KEY, this);
    	ITradeAPI.exceptionHandler().raise(ITradeNoSuchElementException.class, map.getClass()+"#"+paraString2+"(String "+paramString+")", errorMsg, lvOptions);
    }
    public Map<String, V> original(){
    	return map;
    }
	@Override
	public V get(String key) {
        V object = (V)map.get(key);
		return object;
	}
	public String getValue(String paramString) throws ITradeNoSuchElementException {
		String lvResultValue = null;
		try {
			lvResultValue = this.get(paramString).toString();
		} catch (Exception ex) {
			DataPadding<Object> lvOptions = new HashMapDataPadding<Object>();
			lvOptions.put(ELEMENT_KEY, paramString);
			lvOptions.put(DATA_KEY, this);
			ITradeAPI.exceptionHandler().raise(ITradeNoSuchElementException.class, map.getClass()+"#get(String "+paramString+")", ex.getMessage(), lvOptions);
		}
		return lvResultValue;
	}
	
	@Override
	public V getOptionalValue(String paramString) {
		return get(paramString);
	}

	@Override
	public void set(String key, V value) throws IllegalArgumentException {
		this.put(key, value);
	}

	@Override
	public void put(String key, V value) throws IllegalArgumentException {
		if (value == null) {
			throw new IllegalArgumentException("Key:"+key+", Value cann't be null!");
		}
		if (key == null) {
			throw new IllegalArgumentException("Key cann't be null!");
		}
		if (!isLocked()) {
			map.put(key, value);
		}
	}

	protected synchronized void lockEnable() {
		this.lock = true;
	}

	protected synchronized boolean isLocked() {
		return this.lock;
	}
	
	public void lock() {
		lockEnable();
	}

	*//**
	 * The boolean returned represents the value true if the value--"getValue(paramString)" is not null and is equal, ignoring case, to the string "true". 
	 *//*
	public boolean getBoolean(String paramString) throws ITradeNoSuchElementException
	{
		boolean value = false;// Default value for Boolean.
		try {
			value = Boolean.parseBoolean(getValue(paramString));
		} catch (ITradeNoSuchElementException e) {
			ITradeAPI.exceptionHandler().handle(this.getClass()+"#getBoolean", e.getErrorcode(),e.getMessage(), e);
			throw e;
		}
		return value;
	}
	

	public int getInt(String paramString) throws ITradeNoSuchElementException
	{
		int value = -1;// Default value for Integer.
		try {
			value = Integer.parseInt(getValue(paramString));
		} catch (NumberFormatException nfe) {
			throwNoSuchElementException(paramString,"getInt","Parse value error from:"+getValue(paramString)+" to integer value!");
		} catch (ITradeNoSuchElementException e) {
			ITradeAPI.exceptionHandler().handle(this.getClass()+"#getInt", e.getErrorcode(),e.getMessage(), e);
			throw e;
		}
		return value;
	}
	
	public long getLong(String paramString)throws ITradeNoSuchElementException 
	{
		long value = -1L;
		try {			
			Long.parseLong(getValue(paramString));
		} catch (NumberFormatException e) {
			throwNoSuchElementException(paramString,"getLong","Parse value error from:"+getValue(paramString)+" to long value!");
		} catch (ITradeNoSuchElementException e) {
			ITradeAPI.exceptionHandler().handle(this.getClass()+"#getLong", e.getErrorcode(),e.getMessage(), e);
			throw e;
		}
		return value;
	}

	public double getDouble(String paramString) throws ITradeNoSuchElementException 
	{
		double value = -1.00;
		try {
			value = Double.parseDouble(getValue(paramString));
		} catch (NumberFormatException nfe) {
			throwNoSuchElementException(paramString,"getDouble","Parse value error from:"+getValue(paramString)+" to double value!");
		} catch (ITradeNoSuchElementException e) {
			ITradeAPI.exceptionHandler().handle(this.getClass()+"#getDouble", e.getErrorcode(),e.getMessage(), e);
			throw e;
		}
		return value;
	}

	public float getFloat(String paramString) throws ITradeNoSuchElementException
	{
		float value = -1.f;
		try {
			value = Float.parseFloat(getValue(paramString));
		} catch (NumberFormatException nfe) {
			throwNoSuchElementException(paramString,"getFloat","Parse value error from:"+getValue(paramString)+" to float value!");
		} catch (ITradeNoSuchElementException e) {
			ITradeAPI.exceptionHandler().handle(this.getClass()+"#getFloat", e.getErrorcode(),e.getMessage(), e);
			throw e;
		}
		return value;
	}

	public Date getDate(String paramString1, String paramString2) throws ITradeNoSuchElementException 
	{
		Date value = null;
		SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat(paramString2);
		try {
			value = localSimpleDateFormat.parse(getValue(paramString1));
		} catch (java.text.ParseException e) {
			throwNoSuchElementException(paramString1,"getDate","Parse value error:"+getValue(paramString1)+" when applying the format:"+paramString2);
		} catch (ITradeNoSuchElementException e) {
			ITradeAPI.exceptionHandler().handle(this.getClass()+"#getDate", e.getErrorcode(),e.getMessage(), e);
			throw e;
		}
		return value;
	}

	public String[] getStringArray(final String key) throws ITradeNoSuchElementException
	{
		String[] value = null;
		try {
			value = getValue(key).split(",");
			//Further process:check if value's length is bigger than 0.
		} catch (ITradeNoSuchElementException e) {
			ITradeAPI.exceptionHandler().handle(this.getClass()+"#getDate", e.getErrorcode(),e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			throwNoSuchElementException(key,"getDate","Split value error:"+getValue(key)+" when split the raw value:["+getValue(key)+"]");
		}
		return value;
	}
	
//BEGIN TASK #:TTL-GZ-clover_he-00090.2 2014-02-21 [ITrade5]Copies all of the mappings from the specified DataPadding to this DataPadding. [ITRADEFIVE-85]	
	public DataPadding add(DataPadding pData) {
		if(pData!=null){			
			this.original().putAll(pData.original());
		}
		return this;
	}
//END TASK #:TTL-GZ-clover_he-00090.2 2014-02-21 [ITrade5]Copies all of the mappings from the specified DataPadding to this DataPadding. [ITRADEFIVE-85]
	
//BEGIN TASK #:TTL-GZ-clover_he-00090.2 2014-02-21 [ITrade5]Removes the mapping for a key from this DataPadding if it is present. [ITRADEFIVE-85]	
	public void remove(String paramString) {
		this.original().remove(paramString);
	}
//END TASK #:TTL-GZ-clover_he-00090.2 2014-02-21 [ITrade5]Removes the mapping for a key from this map if it is present. [ITRADEFIVE-85]

//BEGIN TASK #:TTL-GZ-clover_he-00090.3 2014-02-21 [ITrade5] Returns true if this map contains a mapping for the specified key. [ITRADEFIVE-85]	
	public boolean containsKey(String paramString){
		return this.original().containsKey(paramString);
	}
//END TASK #:TTL-GZ-clover_he-00090.3 2014-02-21 [ITrade5] Returns true if this map contains a mapping for the specified key. [ITRADEFIVE-85]	}
	
	public void putAll(DataPadding<V> all){
		this.map.putAll(all.original());
	}
	
	@Override
	public String toString() {
		return this.map.toString();
	}
}
*/