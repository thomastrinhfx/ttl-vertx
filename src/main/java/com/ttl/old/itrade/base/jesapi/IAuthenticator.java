/**
 * 
 */
package com.ttl.old.itrade.base.jesapi;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The IAuthenticator interface defines a set of methods for generating and
 * handling account credentials and session identifiers.
 * @author jay.wince
 * @since  2014-04-08
 */
public interface IAuthenticator {

	void clearCurrent();
	
	IUser login();
	
	IUser login(HttpServletRequest request, HttpServletResponse response);

	boolean verifyPassword(IUser user, String password);
	
	String generateStrongPassword();

	String generateStrongPassword(IUser user, String oldPassword);

	void changePassword(IUser user, String currentPassword, String newPassword, String newPassword2);

	IUser getCurrentUser();


	void setCurrentUser(IUser user);


	void removeUser(String accountName);

}
