package com.ttl.old.itrade.base.data;
/**
 * 
 * @author jay.wince
 * @since  2013-12-16
 */
public interface Parameters<K,V extends Object> {
	 public abstract V get(K key);
	 public V getOptionalValue(K paramString);
	 public abstract void set(K key, V value)throws IllegalArgumentException;
	 public abstract void put(K key, V value)throws IllegalArgumentException;
	 public abstract void lock();
}
