/*package com.itrade.base.jesapi;

import java.io.File;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itrade.aastock.IAARequestParser;
import com.itrade.aastock.generator.IAATokenGenerator;
import com.itrade.base.dashboard.strategy.IFunctionIDFilterStrategy;
import com.itrade.base.dashboard.theme.IThemeReadyStrategy;
import com.itrade.base.exception.ExceptionHandler;
import com.itrade.base.exception.ITradeInputstreamProcessException;
import com.itrade.base.io.InputStreamProcessingTemplate;
import com.itrade.base.io.InputStreamProcessor;
import com.itrade.base.jesapi.reference.TransactionBasedAuthenticator;
import com.itrade.base.jesapi.util.ITradeObjectFactory;
import com.itrade.configuration.FrontEndConfiguration;
import com.itrade.configuration.ITradeConfiguration;
import com.itrade.user.preference.persist.IUserPreferenceOperator;

*//**
 * 
 * @author jay.wince
 * @since  2013-12-03
 *//*


//UPDATE TASK : TTL-HK-WLCW-01694 - 07 Jan 2016 Walter Lau[Latest] ITrade Kill Thread after Application closed by Web Server(Tomcat or WebSphere)

public final class ITradeAPI {

	private static String WORKING_SPACE_PATH;
	private static ServletContext servletContext;
	
	private static String iTradeConfigurationImplName = System.getProperty("com.itrade.jesapi.ITradeConfiguration", "com.itrade.configuration.ITradeINIConfiguration");
	
	public static void loadConfiguration(final ServletContext context){
	    if (context == null)
        {
            String lvWorkingDirectory = System.getProperty("testing.working.directory");
            if (lvWorkingDirectory!=null)
            {
                WORKING_SPACE_PATH = lvWorkingDirectory;
            }            
        }else {
		WORKING_SPACE_PATH = context.getRealPath("/");
		servletContext = context;
        }
	    WORKING_SPACE_PATH = WORKING_SPACE_PATH.replace("\\", "/");
	    if (!(WORKING_SPACE_PATH.endsWith("/")))
        {
	        WORKING_SPACE_PATH +="/";
        }
		FrontEndConfiguration.getInstance().loadConfiguration();
// BEGIN TASK #:TTL-GZ-Jay-000127[ITrade5]Extract ITradeConfiguration to parse itrade.ini.
		iTradeConfiguration().loadConfiguration();
// END TASK #:TTL-GZ-Jay-000127[ITrade5]Extract ITradeConfiguration to parse itrade.ini.
		
	}
	
	public static IDashboardFeed dashboardFeed(){
		return ITradeObjectFactory.make(iTradeConfiguration().getDashboardImplementation(), "DashboardFeed");
	}
	
	public static IUserPreferenceOperator userPreferenceOperator(){
		return ITradeObjectFactory.make(iTradeConfiguration().getUserPrefOperatorImpl(), "IUserPreferenceOperator");
	}
	
//TTL-GZ-Jay-00119 2013-12-11[ITrade5]Itrade API : Http utilities.
	public static IHttpUtilities httpUtilities(){
		return ITradeObjectFactory.make(iTradeConfiguration().getHttpUtilityImplementation(), "HttpUtilities");
	}
	
	public static HttpServletRequest currentRequest() {
		return httpUtilities().getCurrentRequest();
	}
	
	public static HttpServletResponse currentResponse() {
		return httpUtilities().getCurrentResponse();
	}
	
// BEGIN TASK #:TTL-GZ-Jay-000127[ITrade5]Get instance of ITradeConfiguration.	
	public static ITradeConfiguration iTradeConfiguration(){
		return ITradeObjectFactory.make(iTradeConfigurationImplName, "ITradeConfiguration");
	}
// END TASK #:TTL-GZ-Jay-000127[ITrade5]Get instance of ITradeConfiguration.

//BEGIN TASK #:TTL-GZ-Jay-00145 2014-02-13[ITrade5]Exception handler support throughout the application.
	public static ExceptionHandler exceptionHandler(){
	    return ITradeObjectFactory.make(iTradeConfiguration().getExceptionHandlerImplementation(), "ExceptionHandler");	
	}
//END  TASK #:TTL-GZ-Jay-00145 2014-02-13[ITrade5]Exception handler support throughout the application.
//BEGIN TASK #:TTL-GZ-PENGJM-00212 2014-02-25[ITrade5]Get the dashboard filter strategy(ITRADEFIVE-110)	
	public static IFunctionIDFilterStrategy dashboardFilterStrategy(){
	    return ITradeObjectFactory.make(iTradeConfiguration().getDashboardFilterStrategyImplementation(), "IDashboardFilterStrategy");	
	}
//END TASK #:TTL-GZ-PENGJM-00212 2014-02-25[ITrade5]Get the dashboard filter strategy(ITRADEFIVE-110)
	
	@SuppressWarnings("rawtypes")
	public static IThemeReadyStrategy themeReadyStrategy(){
		return ITradeObjectFactory.make(iTradeConfiguration().getThemeReadyStrategyImpl(), IThemeReadyStrategy.class.getSimpleName());
	}
	
BEGIN  TASK #:TTL-GZ-Jay-00176 20140523[ITradeR5]AAStock data feed parse utility build.
	@SuppressWarnings("rawtypes")
	public static IAARequestParser AARequestParser(){
		return ITradeObjectFactory.make(iTradeConfiguration().getAAStockRequestParserImpl(), IAARequestParser.class.getSimpleName());
	}
	
	public static IAATokenGenerator AATokenGenerator(){
		return ITradeObjectFactory.make(iTradeConfiguration().getAATokenGeneratorImpl(), IAATokenGenerator.class.getSimpleName());
	}
ENDING  TASK #:TTL-GZ-Jay-00176 20140523[ITradeR5]AAStock data feed parse utility build.
	*//**
	 * load inputStream from file under working space.
	 * e.g: WEB-INF/test.txt and so on.
	 * TODO#:check the path whether it is secure.
	 * @param path relative to web context path.
	 * @see ITradeAPI#getSecureFile
	 * @return
	 *//*
	public static void inputstreamProcess(final String path,InputStreamProcessor processor) throws ITradeInputstreamProcessException{
            inputstreamProcess(WORKING_SPACE_PATH, path, processor);    
	}
        
        
    public static void inputstreamProcess(final String pWorkingSpacePath, final String path,InputStreamProcessor processor) throws ITradeInputstreamProcessException{
		InputStreamProcessingTemplate.process(pWorkingSpacePath+path, processor);
	}
        
        
	*//**
	 * load file under working space.
	 * e.g: WEB-INF/test.txt and so on.
	 * TODO#:check the path whether it is secure.
	 * @param path relative to web context path.
	 * @see ITradeAPI#inputstreamProcess
	 * @return
	 *//*
        
    public static File getSecureFile(final String path){
		//TODO#:
        return getSecureFile(WORKING_SPACE_PATH, path);
	}
        
        
	public static File getSecureFile(final String pWorkingSpacePath, final String path){
		//TODO#:
		return new File(pWorkingSpacePath+path);
	}
        
        
        
	
	public static void clearCurrent(){
		httpUtilities().clearCurrent();
	}
	
	public static void destroy(){
		servletContext = null;
	}
	
	public static String getWorkspacePath(){
		return WORKING_SPACE_PATH;
	}
	*//**
	 * @author canyong.lin
	 * @since 20141211
	 *//*
	public static TransactionBasedAuthenticator authenticator(){
		return TransactionBasedAuthenticator.getInstance();
	}
}
*/