/**
 * 
 */
package com.ttl.old.itrade.base.dashboard.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * <p>Copyright: Copyright(c)Transaction Technologies Limited 20140514by xuejie.xiao</p>
 * @author jay.wince
 * @Date    20140514
 * @version 1.0
 */
public class DashboardMgrModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String active;
	List<DashboardModel> data;
    private List<DashboardLayoutModel> layoutItems;
    
    private Map<String, Object> mvSettings;
    
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public List<DashboardModel> getDashboardmodels() {
		return data;
	}
	public DashboardModel getDashboardmodelById(final String pDashboardID){
		DashboardModel target = null;
		for(DashboardModel dashboardModel : data){
			if (dashboardModel.getId().equals(pDashboardID)) {
				target = dashboardModel;
				break;
			}
		}
		return target;
	}
	public void setDashboardmodels(List<DashboardModel> data) {
		this.data = data;
	}
    public void setLayouts(List<DashboardLayoutModel> layoutItems) {
		this.layoutItems = layoutItems;
	}
    public List<DashboardLayoutModel> getLayouts() {
		return layoutItems;
	}
	
    public Map<String, Object> getSettings(){
        return this.mvSettings;
    }
    
    public void setSettings(Map<String, Object> pSettings)
    {
        this.mvSettings = pSettings;
    }
}
