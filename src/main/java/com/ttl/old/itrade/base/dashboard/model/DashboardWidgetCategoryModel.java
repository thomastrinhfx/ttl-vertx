package com.ttl.old.itrade.base.dashboard.model;

import java.io.Serializable;
import java.util.List;

//import com.itrade.util.StringUtils;
/**
 * 
 * @author jay.wince
 * @since  2013-12-06
 */
public class DashboardWidgetCategoryModel implements Serializable,Comparable<DashboardWidgetCategoryModel> {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<DashboardWidgetModel> widgets;
    private int count;
    private String name;
    private String description;
    private String id;
    private boolean ready;
    private String index;
    
    
	public List<DashboardWidgetModel> getWidgets() {
//Jay on 20150304:Check the property "allowInDashboard" on DashboardWidgetModel	
	    for (int i = 0; i < this.widgets.size() ; i++)
        {
	        DashboardWidgetModel dashboardWidgetModel = this.widgets.get(i);
	        if ("-1".equals(dashboardWidgetModel.getAllowInDashboard()))
	        {
	            this.widgets.remove(dashboardWidgetModel);
	        }
        }
		return widgets;
	}
	public void setWidgets(List<DashboardWidgetModel> widgets) {
		this.widgets = widgets;
	}
	public int getCount() {		
		if (this.widgets==null || this.widgets.size()==0) {
			return count;
		};
		return this.widgets.size();             			
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isReady() {
		return ready;
	}
	public void setReady(boolean ready) {
		this.ready = ready;
	}
	
	
	public String getIndex() {
		return index;
	}
	public void setIndex(String index) {
		this.index = index;
	}
	/**
	 * 
	 * @param pBaseWidgetId
	 * @return
	 */
	private int calculate(final List<String> dashboardWidgetIDs,final String pBaseWidgetId){
		   int result = -1;
		   /*if (StringUtils.isNullStr(pBaseWidgetId)) {
			   return result;
		   }*/
		   for(String widStr : dashboardWidgetIDs){
			   if (widStr.contains(pBaseWidgetId)) {
				   result++;
			   }
		   }
		   return result+1;
	}
	
	/**
	 *  Remove the widget from the underlying category according to the formula:sum(baseWidgetID)>=allow		
	 * @param pBaseWidgetId
	 * @return
	 */
	public void selfCheck(List<String> widgetIDs){
		   List<DashboardWidgetModel> widgets = this.getWidgets();
		   int lvCategoryWidgetCounts = widgets.size();
		   for(int i = 0 ; i < lvCategoryWidgetCounts ; i++ ){
			   DashboardWidgetModel widget = widgets.get(i);
			   for(String widStr : widgetIDs){
				   if (widStr.contains(widget.getId())) {
					   int allow = -1;
					   if ("N".equalsIgnoreCase(widget.getAllowInDashboard())) {
						   allow = 100;//It's just a predicted value!
					   }else {
						   allow = Integer.parseInt(widget.getAllowInDashboard());
					   }
					   if (this.calculate(widgetIDs,widget.getId())>=allow) {
						   this.getWidgets().remove(widget);
						   i--;
						   lvCategoryWidgetCounts--;
					   }
					   break;
				   }
			   };		   
		   }
	}
	
	@Override
	public int compareTo(DashboardWidgetCategoryModel o) {
		return Integer.parseInt(getIndex())-Integer.parseInt(o.getIndex());
	}
    
}
