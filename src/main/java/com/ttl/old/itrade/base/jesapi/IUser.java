package com.ttl.old.itrade.base.jesapi;

import java.io.Serializable;
import java.security.Principal;
import java.util.Locale;
/**
 * @author junming.peng	
 * @since  2013-12-30
 */
public interface IUser extends Principal, Serializable {
	String getAccountID();
//BEGIN TASK #: TTL-GZ-Jay-00139 2014-01-23[ITrade5]Locale support for User interface.	
	/**
	 * @return the locale
	 */
	public Locale getLocale();

	/**
	 * @param locale the locale to set
	 */
	public void setLocale(Locale locale);
//END   TASK #: TTL-GZ-Jay-00139 2014-01-23[ITrade5]Locale support for User interface.
}
