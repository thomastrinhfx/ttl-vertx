package com.ttl.old.itrade.base.dashboard.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

//import com.itrade.util.StringUtils;
//import com.itrade.web.engine.util.JSONParse;

public class DashboardWidgetModel implements Serializable,Comparable<DashboardWidgetModel>{
    /**
     * 
     */
    private static final long serialVersionUID = -2395057115368765683L;
    private final static String V_PH_URL    = "&column={column}&Dwidth={width}";
    
// ----------------------------------------------------------Common Usage BEGIN    
    private String id;
    /**
     * Jay on 20141118:
     * Extend the url as the format below:    
     * Case 1: Share the same url among the defferent column views.
     * Case 2: 
     *   {
     *     "oneThird":"",
     *     "twoThird":"",
     *     "full":""
     *   }
     */
    private Map<String, String> url;
    private String editurl;
    private String open = "true";
    private String columnView;//Column view to support.
// ----------------------------------------------------------Common Usage ENDING
    
//    private String image;
    
//  --------------------Unused Now.    
    private String title;
// -----------------------------------------------------------Category Container BEGIN    
    private String description;
    /**    
     * Jay on 20141114:
     * idea pending to implement:
     *   oneThird@2
     *   twoThird@1
     *   full@4
     * Control the amount allowed to add Function Panel(amount per panel)
     */
    private String allowInDashboard;
// -----------------------------------------------------------Category Container ENDING
    
//  ----------------------------------------------------------Dashboard Container BEGIN    
    private String column;
    private String layoutId;
    private String index = "0";
//  ----------------------------------------------------------Dashboard Container ENDING   
    
// -----------------------------------------------------------Override Allowed BEGIN    

    private Map<String, String> toolOptions;

// -----------------------------------------------------------Override Allowed ENDING
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getTitle() {
        /*if (StringUtils.isNullStr(this.title))
        {
            return this.id;
        }*/
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Map<String, String> getUrl() {
        return url;
    }
    public void setUrl(String pUrl){
        @SuppressWarnings("unchecked")
		Map<String,String> pUrlMap =null;// JSONParse.readJSON(pUrl, Map.class);
        this.setUrl(pUrlMap);
    }
    public void setUrl(Map<String, String> pUrl){
    	Set<Entry<String, String>> lvEntries = pUrl.entrySet();
        HashMap<String,String> lvMap = new HashMap<String,String>();
        for (Entry<String, String> entry : lvEntries)
        {
            String lvUrl = entry.getValue();
            String lvKey = entry.getKey();
           /* if (!StringUtils.isNullStr(lvUrl) && lvUrl.indexOf(V_PH_URL)<0)
            {
                lvUrl += V_PH_URL;
            }*/
            lvMap.put(lvKey, lvUrl);
        }
        this.url = lvMap;
    }
/*  
    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }
*/  
    public String getColumn() {
        return column;
    }
    public void setColumn(String column) {
        this.column = column;
    }
    public String getEditurl() {
        return editurl;
    }
    public void setEditurl(String editurl) {
        this.editurl = editurl;
    }
    public String getOpen() {
        return open;
    }
    public void setOpen(String open) {
        this.open = open;
    }
    public String getIndex() {
        return index;
    }
    public void setIndex(String index) {
        this.index = index;
    }   
    
    /**
     * @return the allowInDashboard
     */
    public String getAllowInDashboard() {
       /* if (StringUtils.isNullStr(this.allowInDashboard)) {
            this.allowInDashboard = "-1";// Means do'not be allowed to add.
        }*/
        return allowInDashboard;
    }
    
    /**
     * @param allowInDashboard the allowInDashboard to set
     */
    public void setAllowInDashboard(String allowInDashboard) {
        this.allowInDashboard = allowInDashboard;
    }
    
    @Override
    public int compareTo(DashboardWidgetModel o) {
        return Integer.parseInt(this.index)-Integer.parseInt(o.index);
    }
     
    public void setLayoutId(String layoutId) {
        this.layoutId = layoutId;
    }
    
    public String getLayoutId() {
        return layoutId;
    }
    
    public void setColumnView(String columnView) {
        this.columnView = columnView;
    }
    
    public String getColumnView() {
        return columnView;
    }
    public Map<String, String> getToolOptions() {
		return toolOptions;
	}
    public void setToolOptions(Map<String, String> toolOptions)
    {
        this.toolOptions = toolOptions;
    }
    @SuppressWarnings("unchecked")
	public void setToolOptions(final String pToolOptionsJson){
    	//this.setToolOptions(JSONParse.readJSON(pToolOptionsJson,Map.class));
    }

}
