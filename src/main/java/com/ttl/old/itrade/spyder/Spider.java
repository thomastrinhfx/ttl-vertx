package com.ttl.old.itrade.spyder;
import java.util.*;
import java.net.*;
import java.io.*;
import javax.swing.text.*;
import javax.swing.text.html.*;

import com.ttl.old.itrade.util.Log;

/**
 * That class implements a reusable spider
 * 
 * @author Jeff Heaton(http://www.jeffheaton.com)
 * @version 1.0
 */
public class Spider {

  /**
   * A collection of URLs that resulted in an error
   */
  protected Collection workloadError = new ArrayList(3);

  /**
   * A collection of URLs that are waiting to be processed
   */
  protected Collection workloadWaiting = new ArrayList(3);

  /**
   * A collection of URLs that were processed
   */
  protected Collection workloadProcessed = new ArrayList(3);

  /**
   * A flag that indicates whether this process
   * should be canceled
   */
  protected boolean cancel = false;

  /**
   * The constructor
   * 
   * @param report A class that implements the ISpiderReportable
   * interface, that will receive information that the
   * spider finds.
   */
  public Spider()
  {
  }

  /**
   * Get the URLs that resulted in an error.
   * 
   * @return A collection of URL's.
   */
  public Collection getWorkloadError()
  {
    return workloadError;
  }

  /**
   * Get the URLs that were waiting to be processed.
   * You should add one URL to this collection to
   * begin the spider.
   * 
   * @return A collection of URLs.
   */
  public Collection getWorkloadWaiting()
  {
    return workloadWaiting;
  }

  /**
   * Get the URLs that were processed by this spider.
   * 
   * @return A collection of URLs.
   */
  public Collection getWorkloadProcessed()
  {
    return workloadProcessed;
  }    

  /**
   * Clear all of the workloads.
   */
  public void clear()
  {
    getWorkloadError().clear();
    getWorkloadWaiting().clear();
    getWorkloadProcessed().clear();
  }

  /**
   * Set a flag that will cause the begin
   * method to return before it is done.
   */
  public void cancel()
  {
    cancel = true;
  }
  

  /**
   * Add a URL for processing.
   * 
   * @param url
   */
  public void addURL(URL url)
  {
    if ( getWorkloadWaiting().contains(url) )
      return;
    if ( getWorkloadError().contains(url) )
      return;
    if ( getWorkloadProcessed().contains(url) )
      return;
    Log.print("Adding to workload: " + url , Log.ACCESS_LOG);
    getWorkloadWaiting().add(url);
  }

  /**
   * Called internally to process a URL
   * 
   * @param url The URL to be processed.
   */
  public void processURL(URL url)
  {
    try {
    	
    	Log.print("Processing: " + url , Log.ACCESS_LOG);
    	// get the URL's contents
		URLConnection connection = url.openConnection();
		if ( (connection.getContentType()!=null) &&
		       !connection.getContentType().toLowerCase().startsWith("text/") ) {
			getWorkloadWaiting().remove(url);
			getWorkloadProcessed().add(url);
			
			Log.print("Not processing because content type is: " +
			         connection.getContentType() , Log.ERROR_LOG);
		    return;
		 }
		  
		 // read the URL
		 InputStream is = connection.getInputStream();
		 Reader r = new InputStreamReader(is);
		 // parse the URL
		 HTMLEditorKit.Parser parse = new HTMLParse().getParser();
		 Parser callback = new Parser(url);
		 parse.parse(r,callback,true);
		  
		 if(callback.getMarketData() != null && callback.getMarketData().trim().length() > 0){
			 getMarketDataList().add(callback.getMarketData());
		 }
      
    } catch ( IOException e ) {
      getWorkloadWaiting().remove(url);
      getWorkloadError().add(url);
      
      Log.print("Error: " + url , Log.ERROR_LOG);
      return;
    }
    // mark URL as complete
    getWorkloadWaiting().remove(url);
    getWorkloadProcessed().add(url);
    Log.print("Complete: " + url , Log.ACCESS_LOG);

  }
  
  private List marketDataList;

  /**
   * Called to start the spider
   */
  public void begin()
  {
    cancel = false;
    
    marketDataList = new ArrayList();
    
    while ( !getWorkloadWaiting().isEmpty() && !cancel ) {
      Object list[] = getWorkloadWaiting().toArray();
      for ( int i=0;(i<list.length)&&!cancel;i++ )
        processURL((URL)list[i]);
    }
  }

/**
 * A HTML parser callback used by this class to detect links
 * 
 * @author Jeff Heaton
 * @version 1.0
 */
  public enum YahooStructure {TITLE, INDEX, CHANGE, CHANGE_DOWN, NO_DATA, CHANGE_PERCENT, CHANGE_PERCENT_DOWN};  
  public static String START_TAG = "yfi_quote_summary";
  public static String STOCK_TITLE_TAG = "title";
  public static String STOCK_INDEX_TAG = "yfs_l10_";
  public static String STOCK_INDEX_CHANGE_UP_TAG = "yfi-price-change-up";
  public static String STOCK_INDEX_CHANGE_DOWN_TAG = "yfi-price-change-down";
  public static String STOCK_INDEX_NO_CHANGE_TAG = "yfi-price-no-change";
  
  
  protected class Parser
  extends HTMLEditorKit.ParserCallback {
    protected URL base;
    protected boolean isGetData = false;
    protected YahooStructure state = YahooStructure.NO_DATA;
    
    private String marketData = "";

    public Parser(URL base)
    {
      this.base = base;
    }
    

    // hander start tag
    public void handleStartTag(HTML.Tag t, MutableAttributeSet a,int pos)
    {
    	getMarketIndexInfoFromYahoo(t,a,pos);    // handle the same way

    }
    
    // get data
    public void handleText(char[] data, int pos) {
    	if(isGetData){
	    	switch (state) {
				case TITLE:
					marketData += (String.valueOf(data) + "|");
				break;
				case INDEX:
					marketData += (String.valueOf(data) + "|");
				break;
				case CHANGE:
					
					if(String.valueOf(data) != null && String.valueOf(data).trim().length() >0){
						marketData += (String.valueOf(data) + "|");
					}
					// end write data
					//isGetData = false;
				break;
				case CHANGE_DOWN:
					if(String.valueOf(data) != null && String.valueOf(data).trim().length() >0){
						marketData += "-" + (String.valueOf(data) + "|");
					}
					// end write data
					//isGetData = false;
				break;
				case CHANGE_PERCENT:
					if(String.valueOf(data) != null && String.valueOf(data).trim().length() >0){
						String value = String.valueOf(data);
						//remove (, ) and %
						value = value.replaceAll("[()%]", "");
						
						marketData += value;
					}
					// end write data
					isGetData = false;
				break;
				case CHANGE_PERCENT_DOWN:
					if(String.valueOf(data) != null && String.valueOf(data).trim().length() >0){
						String value = String.valueOf(data);
						//remove (, ) and %
						value = value.replaceAll("[()%]", "");
						marketData += "-" + (value);
					}
					// end write data
					isGetData = false;
				break;
			}
    	}
    	
    }
    
    // get market data from yahoo finance
    public void getMarketIndexInfoFromYahoo(HTML.Tag t, MutableAttributeSet a,int pos){
    	
    	// set start get data
    	if(t.equals(HTML.Tag.DIV)){    	
	    	String value = (String)a.getAttribute(HTML.Attribute.CLASS);
	    	if(value!= null && value.equals(START_TAG)){
	    		isGetData = true;
	    	}
    	}
    	
    	if(isGetData){
	    	// get stock title
	    	if(t.equals(HTML.Tag.H2)){
	    		state = YahooStructure.TITLE;
	    		return;
	    	}
	    	
	    	// get index
	    	if(t.equals(HTML.Tag.SPAN)){
	    		String value = (String)a.getAttribute(HTML.Attribute.ID);
	    		if(value != null && value.startsWith(STOCK_INDEX_TAG)){
	    			state = YahooStructure.INDEX;
	    			return;
	    		} else if(value != null && value.startsWith("yfs_c10_")){
	    			state = YahooStructure.CHANGE;
	    			return;
	    		}
	    		else if(value != null && value.startsWith("yfs_p20_")){
	    			state = YahooStructure.CHANGE_PERCENT;
	    			return;
	    		}
	    	}
	    	
	    	// get index change
	    	if(t.equals(HTML.Tag.B)){
	    		String className = (String)a.getAttribute(HTML.Attribute.CLASS);
	    		if(className!= null && (className.trim().equals(STOCK_INDEX_CHANGE_UP_TAG)
	    				|| className.trim().equals(STOCK_INDEX_NO_CHANGE_TAG))){
	    			if(state != YahooStructure.CHANGE_PERCENT){
	    				state = YahooStructure.CHANGE;
	    			}	    			
	    			return;
	    		} else if(className!= null && className.trim().equals(STOCK_INDEX_CHANGE_DOWN_TAG)){
	    			if(state != YahooStructure.CHANGE_PERCENT){
	    				state = YahooStructure.CHANGE_DOWN;
	    			} else {
	    				state = YahooStructure.CHANGE_PERCENT_DOWN;
	    			}
	    			return;
	    		}
	    	}
    	}
    	
    	// set no data
    	state = YahooStructure.NO_DATA;
    }


	/**
	 * @param marketData the marketData to set
	 */
	public void setMarketData(String marketData) {
		this.marketData = marketData;
	}


	/**
	 * @return the marketData
	 */
	public String getMarketData() {
		return marketData;
	}
    
  }

 
/**
 * @param marketDataList the marketDataList to set
 */
public void setMarketDataList(List marketDataList) {
	this.marketDataList = marketDataList;
}

/**
 * @return the marketDataList
 */
public List getMarketDataList() {
	return marketDataList;
}
}
