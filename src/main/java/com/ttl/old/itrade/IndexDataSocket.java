/*package com.itrade;

import javax.inject.Inject;

import org.atmosphere.client.TrackMessageSizeInterceptor;
import org.atmosphere.config.service.DeliverTo;
import org.atmosphere.config.service.Disconnect;
import org.atmosphere.config.service.Get;
import org.atmosphere.config.service.ManagedService;
import org.atmosphere.config.service.Message;
import org.atmosphere.config.service.PathParam;
import org.atmosphere.config.service.Post;
import org.atmosphere.config.service.Ready;
import org.atmosphere.config.service.DeliverTo.DELIVER_TO;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.cpr.AtmosphereResourceFactory;
import org.atmosphere.cpr.BroadcasterFactory;
import org.atmosphere.cpr.HeaderConfig;
import org.atmosphere.cpr.MetaBroadcaster;
import org.atmosphere.interceptor.AtmosphereResourceLifecycleInterceptor;
import org.atmosphere.interceptor.AtmosphereResourceStateRecovery;
import org.atmosphere.interceptor.HeartbeatInterceptor;
import org.atmosphere.interceptor.IdleResourceInterceptor;
import org.atmosphere.interceptor.OnDisconnectInterceptor;
import org.atmosphere.interceptor.SuspendTrackerInterceptor;

import ProtocolDecoder;
import JacksonEncoder;
import ITradeUser;
import mdsIndexDataBean;
import mdsStockInfoBean;
import Log;
import ItradeResponseJSONBean;

import io.vertx.core.Vertx;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;

@ManagedService(path = "/ITradePushServer/IndexData/{clientID: [a-zA-Z0-9]+}", interceptors = {
		AtmosphereResourceLifecycleInterceptor.class, TrackMessageSizeInterceptor.class,
		SuspendTrackerInterceptor.class, AtmosphereResourceStateRecovery.class, HeartbeatInterceptor.class,
		OnDisconnectInterceptor.class, IdleResourceInterceptor.class })
public class IndexDataSocket {
	@Inject
	private BroadcasterFactory factory;
	
	@Inject
	private AtmosphereResourceFactory resourceFactory;
	
	@Inject
	private MetaBroadcaster metaBroadcaster;

	@PathParam("clientID")
	private String mvClientID;
	
	@Ready(encoders = { JacksonEncoder.class })
	@DeliverTo(DELIVER_TO.RESOURCE)
	public Object onReady(final AtmosphereResource resource) {
		Log.print("Browser:" + resource.uuid() + " connected!", Log.DEBUG_LOG);
		System.out.println("Resource uuid =  " + resource.uuid());
		ITradeAtmosphereAPI lvAPI = ITradeAtmosphereAPI.getInstance();
		lvAPI.setBroadcasterFactory(factory).setMetaBroadcaster(metaBroadcaster).setResourceFactory(resourceFactory);
		
		ITradeUser lvITradeUser = ITradeAtmosphereAPI.getInstance().connect(resource, mvClientID);

		ItradeResponseJSONBean lvResBean = new ItradeResponseJSONBean("success", 0, new mdsIndexDataBean());
		if (lvITradeUser == null) {
			lvResBean = new ItradeResponseJSONBean("Failed", 1, null);
		} else {
			lvResBean = new ItradeResponseJSONBean("success", 0, null);
			SharedData sd = Vertx.vertx().sharedData();
			LocalMap<String, String> map = sd.getLocalMap("User");
			map.put("user", mvClientID);
		}
		
		return lvResBean;
	}
	

	@Disconnect
	public void onDisconnect(AtmosphereResourceEvent event) {
		String transport = event.getResource().getRequest().getHeader(HeaderConfig.X_ATMOSPHERE_TRANSPORT);
		if (transport != null && org.atmosphere.util.Utils.resumableTransport(event.getResource().transport())
				&& transport.equalsIgnoreCase(HeaderConfig.DISCONNECT_TRANSPORT_MESSAGE)) {
			System.out.println("Browser closed the connection!");
		} else {
			System.out.println("Long-Polling Connection Resumed.");
		}

		String uuid = event.getResource().uuid();
		if (event.isCancelled()) {
			Log.println("Browser " + uuid + " unexpectedly disconnected.", Log.DEBUG_LOG);
		} else if (event.isClosedByClient()) {
			Log.println("Browser " + uuid + "@Suspend( contentType = MediaType.APPLICATION_JSON, period = MAX_SUSPEND_MSEC ) closed the connection.", Log.DEBUG_LOG);
		} else if (event.isClosedByApplication()) {
			Log.println("Application server closed the conenction: " + event.getResource().getBroadcaster().getID(),
					Log.ALERT_LOG);
		}

		if (org.atmosphere.util.Utils.resumableTransport(event.getResource().transport())) {

		}
	}

	@Message(encoders = { JacksonEncoder.class }, decoders = { ProtocolDecoder.class })
	public ItradeResponseJSONBean onTopicSubcrise(AtmosphereResource resource) {
		ITradeAtmosphereAPI lvAPI = ITradeAtmosphereAPI.getInstance();
		ItradeResponseJSONBean lvRespBean = null;

		if (resource == null) {
			lvRespBean = new ItradeResponseJSONBean("failure", 1, null);
		} else {
			lvRespBean = new ItradeResponseJSONBean("Success", 0, "Successfully Established");
			ITradeUser lvUser = lvAPI.retrive(resource);
			if (lvUser == null) {

			} else {
				lvUser.cancelTask();
				lvUser.startTicker();
			}
		}
		return lvRespBean;
	}

	@Message(encoders = { JacksonEncoder.class }, decoders = { ProtocolDecoder.class })
	public ItradeResponseJSONBean onBroadcastMessage(ItradeResponseJSONBean respBean) {
		return respBean;
	}


	@Get
	public void setMessageEncoding(AtmosphereResource resource) {
		resource.getResponse().setCharacterEncoding("UTF-8");

		if (resource.isResumed()) {

		}
	}

	@Post
	public void postEncoding(AtmosphereResource resource) {
		resource.getResponse().setCharacterEncoding("UTF-8");
	}
}
*/