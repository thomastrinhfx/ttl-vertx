package com.ttl.old.itrade;

import com.ttl.old.itrade.biz.model.ITradeUser;
import com.ttl.old.itrade.mds.action.StockInfoMdstxn;
import com.ttl.old.itrade.mds.bean.mdsStockInfoBean;
import com.ttl.old.itrade.util.Log;
import com.ttl.old.itrade.web.engine.model.ItradeResponseJSONBean;
import com.ttl.wtrade.utils.WTradeLogger;
import com.ttl.wtrade.utils.atmosphere.protocol.decoder.ProtocolDecoder;
import com.ttl.wtrade.utils.atmosphere.protocol.encoder.JacksonEncoder;
import org.atmosphere.client.TrackMessageSizeInterceptor;
import org.atmosphere.config.service.*;
import org.atmosphere.config.service.DeliverTo.DELIVER_TO;
import org.atmosphere.cpr.*;
import org.atmosphere.interceptor.*;

import javax.inject.Inject;
import java.io.IOException;

@ManagedService(path = "/ITradePushServer/StockInfo/{clientID: [a-zA-Z0-9]+}", interceptors = {
        AtmosphereResourceLifecycleInterceptor.class, TrackMessageSizeInterceptor.class,
        SuspendTrackerInterceptor.class, AtmosphereResourceStateRecovery.class, HeartbeatInterceptor.class,
        OnDisconnectInterceptor.class, IdleResourceInterceptor.class})
public class StockInfoSocket {
    @Inject
    private BroadcasterFactory factory;

    @Inject
    private AtmosphereResourceFactory resourceFactory;

    @Inject
    private MetaBroadcaster metaBroadcaster;

    @PathParam("clientID")
    private String mvClientID;

    @Ready(encoders = {JacksonEncoder.class})
    @DeliverTo(DELIVER_TO.RESOURCE)
    public Object onReady(final AtmosphereResource resource) {
        Log.print("Browser:" + resource.uuid() + " connected!", Log.DEBUG_LOG);
        WTradeLogger.print("StockInfoSocket", "Resource uuid = " + resource.uuid());
//		System.out.println("Resource uuid =  " + resource.uuid());
        ITradeAtmosphereAPI lvAPI = ITradeAtmosphereAPI.getInstance();
        lvAPI.setBroadcasterFactory(factory).setMetaBroadcaster(metaBroadcaster).setResourceFactory(resourceFactory);
        ITradeUser lvITradeUser = ITradeAtmosphereAPI.getInstance().connect(resource, mvClientID);

        ItradeResponseJSONBean lvResBean = new ItradeResponseJSONBean("success", 0, new mdsStockInfoBean());
		if (lvITradeUser == null) {
			lvResBean = new ItradeResponseJSONBean("Failed", 1, null);
		} else {
			StockInfoMdstxn.getInstance().addMvStockSessionMap("_ALL", mvClientID);
			lvResBean = new ItradeResponseJSONBean("success", 0, null);
//			SharedData sd = Vertx.vertx().sharedData();
//			LocalMap<String, String> map = sd.getLocalMap("User");
//			map.put("user", mvClientID);
		}

        return lvResBean;
    }

    @Disconnect
    public void onDisconnect(AtmosphereResourceEvent event) {
        String transport = event.getResource().getRequest().getHeader(HeaderConfig.X_ATMOSPHERE_TRANSPORT);
        if (transport != null && org.atmosphere.util.Utils.resumableTransport(event.getResource().transport())
                && transport.equalsIgnoreCase(HeaderConfig.DISCONNECT_TRANSPORT_MESSAGE)) {
            System.out.println("Browser closed the connection!");
        } else {
            System.out.println("Long-Polling Connection Resumed.");
        }

        String uuid = event.getResource().uuid();
        if (event.isCancelled()) {
            Log.println("Browser " + uuid + " unexpectedly disconnected.", Log.DEBUG_LOG);
        } else if (event.isClosedByClient()) {
            Log.println("Browser " + uuid + "@Suspend( contentType = MediaType.APPLICATION_JSON, period = MAX_SUSPEND_MSEC ) closed the connection.", Log.DEBUG_LOG);
        } else if (event.isClosedByApplication()) {
            Log.println("Application server closed the connection: " + event.getResource().getBroadcaster().getID(),
                    Log.ALERT_LOG);
        }

        if (org.atmosphere.util.Utils.resumableTransport(event.getResource().transport())) {

        }
    }

    @Message(encoders = {JacksonEncoder.class}, decoders = {ProtocolDecoder.class})
    public ItradeResponseJSONBean onTopicSubscribe(AtmosphereResource resource) throws IOException {
        ITradeAtmosphereAPI lvAPI = ITradeAtmosphereAPI.getInstance();
        ItradeResponseJSONBean lvRespBean = null;

        if (resource == null) {
            lvRespBean = new ItradeResponseJSONBean("failure", 1, null);
        } else {
            lvRespBean = new ItradeResponseJSONBean("Success", 0, "Successfully Established");
            ITradeUser lvUser = lvAPI.retrieve(resource);
            if (lvUser == null) {

            } else {
                lvUser.cancelTask();
                lvUser.startTicker();
            }
        }
        return lvRespBean;
    }

    @Message(encoders = {JacksonEncoder.class}, decoders = {ProtocolDecoder.class})
    public ItradeResponseJSONBean onBroadcastMessage(ItradeResponseJSONBean respBean) {
        return respBean;
    }


    @Get
    public void setMessageEncoding(AtmosphereResource resource) {
        resource.getResponse().setCharacterEncoding("UTF-8");

        if (resource.isResumed()) {

        }
    }

    @Post
    public void postEncoding(AtmosphereResource resource) {
        resource.getResponse().setCharacterEncoding("UTF-8");
    }
}
