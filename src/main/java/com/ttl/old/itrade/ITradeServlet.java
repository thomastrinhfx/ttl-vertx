package com.ttl.old.itrade;

import com.systekit.common.msg.IMsgXMLParser;
import com.systekit.common.msg.MsgManager;
import com.systekit.winvest.hks.util.Utils;
import com.ttl.old.itrade.hks.HKSClearPrevDateHistoricalDataTask;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPConnector;
import com.ttl.old.itrade.tp.TPConnectorThread;
import com.ttl.old.itrade.tp.TPExternalConnectorThread;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.ttl.wtrade.utils.WTradeLogger;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Properties;
import java.util.StringTokenizer;

public class ITradeServlet {
    public ITradeServlet() {
        System.out.println("init ItradeServlet()");
    }


    private final String ITRADE_SERVLET = "ITradeServlet";
    public final static String SUCCESS = "Success";
    public final static String ERROR = "Error";

    private static final String CONFIG_CLEAR_TIME = "timeToClearPrevData";
    private Hashtable mvAvailableActions;
    private static IMain ivStartUpMain;
    private static TPManager svTPManager;
    private static HashMap svTPExternalServerMap = new HashMap();
    private static boolean svITradeServeletStartedFlag = false;
    private static String svDefaultMarket;
    private static String svServerIP;
    private static long svSessionTimeout = 180000;

    private static int svReceiveBufferLen = 10000000;
    private static int svHeaderLength = 10;
    private static Properties mvITradeProperties;
    private static Properties mvPluginITradeProperties;
    private static boolean svDownloadTradeDateStarted = false;
    private static boolean svDownloadInstrumentStarted = false;

    private static long svReDownloadDataTime = 60000;
    private static int svReDownloadDataDownloadTimes = 3;
    private static boolean svReDownloadDataEnable = false;

    public static void setDownloadTradeDateStarted(boolean pDownloadTradeDateStarted) {
        setSvDownloadTradeDateStarted(pDownloadTradeDateStarted);
    }

    /**
     * Returns the start date of download Trade.
     *
     * @return The start date of download Trade.
     */
    public static boolean getDownloadTradeDateStarted() {
        return isSvDownloadTradeDateStarted();
    }

    public static IMain getIvStartUpMain() {
        return ivStartUpMain;
    }

    public static void setIvStartUpMain(IMain ivStartUpMain) {
        ITradeServlet.ivStartUpMain = ivStartUpMain;
    }

    public static String getItradeServerIP() {
        return getSvServerIP() == null ? "localhost" : getSvServerIP();
    }

    public static TPManager getItradeServerIP(String pRequestName) {
        TPManager lvTPManager = getSvTPManager();
        if (getSvTPExternalServerMap().containsKey(pRequestName))
            lvTPManager = (TPManager) getSvTPExternalServerMap().get(pRequestName);

        return lvTPManager;
    }

    public static TPManager getTpManager() {
        return getSvTPManager();
    }

    public static TPManager getTPManager(String pRequestName) {
        TPManager lvTPManager = getSvTPManager();
        if (getSvTPExternalServerMap().containsKey(pRequestName))
            lvTPManager = (TPManager) getSvTPExternalServerMap().get(pRequestName);

        return lvTPManager;
    }

    public static String getConfigClearTime() {
        return CONFIG_CLEAR_TIME;
    }

    public static TPManager getSvTPManager() {
        return svTPManager;
    }

    public static void setSvTPManager(TPManager svTPManager) {
        ITradeServlet.svTPManager = svTPManager;
    }

    public static HashMap getSvTPExternalServerMap() {
        return svTPExternalServerMap;
    }

    public static void setSvTPExternalServerMap(HashMap svTPExternalServerMap) {
        ITradeServlet.svTPExternalServerMap = svTPExternalServerMap;
    }

    public static boolean isSvITradeServeletStartedFlag() {
        return svITradeServeletStartedFlag;
    }

    public static void setSvITradeServeletStartedFlag(boolean svITradeServeletStartedFlag) {
        ITradeServlet.svITradeServeletStartedFlag = svITradeServeletStartedFlag;
    }

    public static String getSvDefaultMarket() {
        return svDefaultMarket;
    }

    public static void setSvDefaultMarket(String svDefaultMarket) {
        ITradeServlet.svDefaultMarket = svDefaultMarket;
    }

    public static String getSvServerIP() {
        return svServerIP;
    }

    public static void setSvServerIP(String svServerIP) {
        ITradeServlet.svServerIP = svServerIP;
    }

    public static long getSvSessionTimeout() {
        return svSessionTimeout;
    }

    public static void setSvSessionTimeout(long svSessionTimeout) {
        ITradeServlet.svSessionTimeout = svSessionTimeout;
    }

    public static int getSvReceiveBufferLen() {
        return svReceiveBufferLen;
    }

    public static void setSvReceiveBufferLen(int svReceiveBufferLen) {
        ITradeServlet.svReceiveBufferLen = svReceiveBufferLen;
    }

    public static int getSvHeaderLength() {
        return svHeaderLength;
    }

    public static void setSvHeaderLength(int svHeaderLength) {
        ITradeServlet.svHeaderLength = svHeaderLength;
    }

    public static Properties getMvPluginITradeProperties() {
        return mvPluginITradeProperties;
    }

    public static void setMvPluginITradeProperties(Properties mvPluginITradeProperties) {
        ITradeServlet.mvPluginITradeProperties = mvPluginITradeProperties;
    }

    public static boolean isSvDownloadTradeDateStarted() {
        return svDownloadTradeDateStarted;
    }

    public static void setSvDownloadTradeDateStarted(boolean svDownloadTradeDateStarted) {
        ITradeServlet.svDownloadTradeDateStarted = svDownloadTradeDateStarted;
    }

    public static boolean isSvDownloadInstrumentStarted() {
        return svDownloadInstrumentStarted;
    }

    public static void setSvDownloadInstrumentStarted(boolean svDownloadInstrumentStarted) {
        ITradeServlet.svDownloadInstrumentStarted = svDownloadInstrumentStarted;
    }

    public static long getSvReDownloadDataTime() {
        return svReDownloadDataTime;
    }

    public static void setSvReDownloadDataTime(long svReDownloadDataTime) {
        ITradeServlet.svReDownloadDataTime = svReDownloadDataTime;
    }

    public static int getSvReDownloadDataDownloadTimes() {
        return svReDownloadDataDownloadTimes;
    }

    public static void setSvReDownloadDataDownloadTimes(int svReDownloadDataDownloadTimes) {
        ITradeServlet.svReDownloadDataDownloadTimes = svReDownloadDataDownloadTimes;
    }

    public static boolean isSvReDownloadDataEnable() {
        return svReDownloadDataEnable;
    }

    public static void setSvReDownloadDataEnable(boolean svReDownloadDataEnable) {
        ITradeServlet.svReDownloadDataEnable = svReDownloadDataEnable;
    }

    public synchronized String init() {
        try {
            start();
            startTP();
            startExternalTPChannel();
            WTradeLogger.print(ITRADE_SERVLET,"Base directory: " + System.getProperty("basedirectory"));
//            System.out.println("Itrade Init: " + System.getProperty("basedirectory"));
            return SUCCESS;
        } catch (Exception e) {
            WTradeLogger.print(ITRADE_SERVLET,"Exception: " + e.getMessage(), WTradeLogger.ERROR_MESSAGE);
            return ERROR;
        }

    }

    private synchronized void start() throws Exception {
        WTradeLogger.print("ITradeServler", "===ItradeServlet Start====");
        try {
            setMvITradeProperties(new Properties());
            String lvTempProperties = new String();
            int hasSecond = 0;
            Path currentRelativePath = Paths.get("");
            String s = currentRelativePath.toAbsolutePath().toString();
            System.out.println("Current relative path is: " + s);
            System.setProperty("basedirectory", ".");
            String rootFolder = System.getProperty("basedirectory") + File.separator;
            System.out.println("====Root Folder: " + rootFolder + " ==========");
            FileInputStream lvIniFileFIS = new FileInputStream(rootFolder + "config/PluginITrade.ini");
            getMvITradeProperties().load(lvIniFileFIS);
            BufferedInputStream lvBISobject = new BufferedInputStream(lvIniFileFIS);
            BufferedReader lvBR = new BufferedReader(new InputStreamReader(lvBISobject));

            String lvLogProperties = lvBR.readLine();
            while (lvLogProperties != null) {
                if (lvLogProperties.indexOf("AgentPassword") < 0 && lvLogProperties.indexOf("SMTPPassword") < 0 && lvLogProperties.indexOf("#") != 0 && lvLogProperties.trim().length() > 0) {
                    lvTempProperties = lvTempProperties.concat(lvLogProperties).concat("\r\n");
                }
                lvLogProperties = lvBR.readLine();
            }

            FileInputStream lvIniFileFIS2 = new FileInputStream(rootFolder + "config/itrade.ini");
            setMvPluginITradeProperties(new Properties());
            getMvPluginITradeProperties().load(lvIniFileFIS2);

            BufferedInputStream lvBISObject2 = new BufferedInputStream(lvIniFileFIS2);

            BufferedReader lvBR2 = new BufferedReader(new InputStreamReader(lvBISObject2));
            String lvLogProperties2 = lvBR2.readLine();
            lvTempProperties = lvTempProperties + "=================Plugin ini=================" + "\r\n";
            while (lvLogProperties2 != null) {
                if (lvLogProperties2.indexOf("AgentPassword") < 0 && lvLogProperties2.indexOf("SMTPPassword") < 0 && lvLogProperties2.indexOf("#") != 0 && lvLogProperties2.trim().length() > 0) {
                    lvTempProperties = lvTempProperties.concat(lvLogProperties2).concat("\r\n");
                }
                lvLogProperties2 = lvBR2.readLine();
            }

            IMain._pIni = getMvITradeProperties();
            IMain._pluginIni = getMvPluginITradeProperties();
            Log.setPath(rootFolder + "logs/");
            Log.print(lvTempProperties, Log.ACCESS_LOG);
        } catch (IOException e) {
            Log.print(e, Log.ERROR_LOG);
            System.out.println("Exception IO when start and config IMain: " + e);
            return;
        }

        setSvServerIP(IMain.getProperty("ITradeServer.IP"));
        setSvDefaultMarket(IMain.getProperty("DefaultMarket"));
        if ("true".equalsIgnoreCase(IMain.getProperty("isDebugOn"))) {
            Log.mvDebugOn = true;
        } else {
            Log.mvDebugOn = false;
        }
        String startupClassname = IMain.getProperty("product.startupClassName");
        ivStartUpMain = (IMain) Class.forName(startupClassname).newInstance();
        setMvAvailableActions(new Hashtable());
        ivStartUpMain.setIni(mvITradeProperties);
        ivStartUpMain.set_pluginIni(getMvPluginITradeProperties());

        ivStartUpMain.setActions(getMvAvailableActions());
        ivStartUpMain.startAuthenticator();

        setSvITradeServeletStartedFlag(true);

    }


    private void startTP() {
        System.out.println("===================Start TP()==================");
        String TPXMLParser = IMain.getProperty("TPXMLparser.ClassName");
        int TPNumber = Integer.parseInt(IMain.getProperty("TPServer.Number"));
        String[] TPServerIP = new String[TPNumber];
        int[] TPServerPort = new int[TPNumber];
        for (int i = 0; i < TPNumber; i++) {
            TPServerIP[i] = IMain.getProperty("TPServer.IP_" + i);
            TPServerPort[i] = Integer.parseInt(IMain.getProperty("TPserver.port_" + i));
        }

        String rootFolder = System.getProperty("basedirectory") + File.separator;
        String TPRequestXMLPath = rootFolder + IMain.getProperty("TPrequestXML.path");

        String lvTPAgentID = IMain.getProperty("AgentID");
        String lvTPAgentPassword = IMain.getProperty("AgentPassword");
        String lvBranchID = IMain.getProperty("BranchID");
        String lvEntityID = IMain.getProperty("EntityID");
        int lvRequestWaitTimout = Integer.parseInt(IMain.getProperty("RequestWaitTimeout"));
        boolean lvTPExternalServerAvaliable = IMain.getProperty("ExternalTPServerAvailable").equals("true") ? true : false;
        //String lvTP1FunctionName = lvTPExternalServerAvaliable?IMain.getProperty("TPExternalServer.IP_1_Function"):"";
        String lvReqeustNameListStringToExternalTP = lvTPExternalServerAvaliable ? IMain.getProperty("ExternalTPServer1_RequestNameList") : "";
        boolean lvPluginExternalTPServerAvailable = IMain.getProperty("PluginExternalTPServerAvailable").equals("true") ? true : false;
        String lvPluginRequestNameListStringToExternalTP = lvPluginExternalTPServerAvailable ? IMain.getProperty("PluginExternalTPServer1_RequestNameList") : "";
        TPManager.svProduct = IMain.getProperty("product");

        File f = new File(".");

        Log.println("Working Directory: " + f.getAbsolutePath(), Log.ACCESS_LOG);
        Log.println("TP Agent ID: " + lvTPAgentID, Log.ACCESS_LOG);
        Log.println("TP Agent Password: " + lvTPAgentPassword, Log.ACCESS_LOG);
        Log.println("Branch ID: " + lvBranchID, Log.ACCESS_LOG);
        Log.println("TP XML Parser Class Name : " + TPXMLParser, Log.ACCESS_LOG);

        for (int i = 0; i < TPNumber; i++) {
            Log.println("TP Server IP " + i + ": " + TPServerIP[i], Log.ACCESS_LOG);
            Log.println("TP Server IP Port " + i + ": " + String.valueOf(TPServerPort[i]), Log.ACCESS_LOG);
        }

        //Log.println("TP Server Port: " + TPServerPort, Log.ACCESS_LOG);
        Log.println("TP Server Request XML Path: " + TPRequestXMLPath, Log.ACCESS_LOG);
        Log.println("TP Request Wait Timeout(ms): " + lvRequestWaitTimout, Log.ACCESS_LOG);

        // For Debug
        Log.println("TPXMLParser: " + TPXMLParser, Log.ACCESS_LOG);

        //The following settings must preserve in order to use JavaDomParser!!!!
        try {
            if (TPXMLParser.equals("com.systekit.common.msg.MsgJavaDomParser")) {
                System.setProperty("XMLPARSER", TPXMLParser);
                System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "org.apache.crimson.jaxp.DocumentBuilderFactoryImpl");

            } else if (TPXMLParser.equals("com.systekit.common.msg.MsgSpeedXMLParser")) {
                System.setProperty("XMLPARSER", TPXMLParser);
                System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "org.apache.crimson.jaxp.DocumentBuilderFactoryImpl");
            }
        } catch (Exception e) {
            System.out.println("Exception");
        }
        Log.println("ITrade Start Create Parser", Log.ACCESS_LOG);
        IMsgXMLParser lvParser = MsgManager.createParser();
        try {
            Log.println("ITrade Parser Name " + lvParser.getClass().getName(), Log.ACCESS_LOG);
            ;
        } catch (NullPointerException npe) {
            npe.printStackTrace();
            Log.println("ITrade Parser Name is Null ", Log.ERROR_LOG);
        } catch (Exception ex) {
            ex.printStackTrace();
            Log.println("ITrade Parser Name Error " + lvParser.getClass().getName(), Log.ERROR_LOG);
        }
        ivStartUpMain.setParser(lvParser);
        Log.println("ITrade End Create Parser ", Log.ACCESS_LOG);

        // create TPConnector
        //BEGIN TASK # TTL-HK-WLCW-00951 Walter Lau 18 Dec 2009 set ReceiveBufferLen to configurable
        TPConnector lvTPConnector = new TPConnector(TPServerIP, TPServerPort, getSvReceiveBufferLen(), getSvHeaderLength());
        Log.println("TP Connector Buffer Size Default is : " + getSvReceiveBufferLen(), Log.ACCESS_LOG);
        //END TASK # TTL-HK-WLCW-00951 Walter Lau 18 Dec 2009 set ReceiveBufferLen to configurable
        setSvTPManager(new TPManager(lvTPConnector, ivStartUpMain, "F"));

        // ensure to set broadcast manager after TPManager is created
        ivStartUpMain.startBroadcastManager();


        String lvPath = TPRequestXMLPath + "/";

        // added by mingl
        ivStartUpMain.startRequestRegistration(lvPath, lvEntityID, lvTPAgentID, lvTPAgentPassword, lvBranchID, lvReqeustNameListStringToExternalTP);

        //BEGIN TASK: TTL-GZ-XYL-00045 XuYuLong 20091211 [iTrade R5] Plugin redesign and implement
        if (getMvPluginITradeProperties() != null) {
            //BEGIN TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
            String lvPluginTPRequestXMLPathStr = rootFolder + IMain.getProperty("TPRequestXMLPath");
            //END TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
            if (lvPluginTPRequestXMLPathStr != null) {
                lvPluginTPRequestXMLPathStr = lvPluginTPRequestXMLPathStr + "/";
                File file = new File(lvPluginTPRequestXMLPathStr);
                boolean isPluginTPRequestXMLPathExist = file.isDirectory();
                if (isPluginTPRequestXMLPathExist) {
                    Log.println("TP Server Plugin Request XML Path: " + lvPluginTPRequestXMLPathStr, Log.ACCESS_LOG);
                    ivStartUpMain.startPluginRequestRegistration(lvPluginTPRequestXMLPathStr, lvEntityID, lvTPAgentID, lvTPAgentPassword, lvBranchID, lvPluginRequestNameListStringToExternalTP);
                } else {
                    Log.println("The Directory [ " + lvPluginTPRequestXMLPathStr + " ] For TP Server Plugin Request XML Path Does Not Exist.", Log.ACCESS_LOG);
                }
            }
        }
        //END TASK: TTL-GZ-XYL-00045 XuYuLong 20091211 [iTrade R5] Plugin redesign and implement

        getSvTPManager().svRequestWaitTimeOut = lvRequestWaitTimout;

        // added by mingl
        // start TPConnector Thread before TPManager run
        TPConnectorThread lvTPConnectorThread = new TPConnectorThread(getSvTPManager(), lvTPConnector);
        lvTPConnectorThread.setName("TPConnector");
        lvTPConnectorThread.start();
        getSvTPManager().start();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
            Log.println(ex, Log.ERROR_LOG);
        }

        //Start AppletGatewayThread
        //BEGIN TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
        int lvPortNumber = Integer.parseInt(IMain.getProperty("TPApplet.port"));
        String lvEnableTPApplet = Utils.isNullStr(IMain.getProperty("EnableTPApplet")) ? "false" : IMain.getProperty("EnableTPApplet");
        //END TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
        boolean lvEnableTPAppletPort = Boolean.getBoolean(lvEnableTPApplet);
        if (lvEnableTPAppletPort) {
            ivStartUpMain.startOthers(lvPortNumber);
            Log.println("ITrade Start TP Applet Port", Log.ACCESS_LOG);
        }

        // BEGIN TASK: TTL-VN VanTran 20101016 [iTrade R5] Add schedule to clear
        // prev historical data

        // First we must get a reference to a scheduler
        SchedulerFactory sf = new StdSchedulerFactory();

        try {
            Scheduler sched = sf.getScheduler();
            String timeInConfig = IMain.getProperty(getConfigClearTime());
            Log.print("Time In Config:" + timeInConfig, Log.DEBUG_LOG);

            // Assign default expression scheduled time if does not define in config
            if (timeInConfig == null || timeInConfig.trim().isEmpty()) {
                // every 7:30 AM from Monday -> Friday
                timeInConfig = "0 30 7 ? * 2,3,4,5,6";
            }
            Log.print("Time In Config New:" + timeInConfig, Log.DEBUG_LOG);
            // Trigger the job to run on the next round minute
            CronTrigger ct = new CronTrigger("job1", "group1", timeInConfig);

            // define the job and tie it to our HelloJob class
            JobDetail job = new JobDetail("job1", "group1",
                    HKSClearPrevDateHistoricalDataTask.class);

            // Tell quartz to schedule the job using our trigger
            sched.scheduleJob(job, ct);

            // Start up the scheduler (nothing can actually run until the
            // scheduler has been started)
            sched.start();
        } catch (SchedulerException e) {
            System.out.println("Exception" + e);
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //END TASK: TTL-VN VanTran 20101016 [iTrade R5] Add schedule  to clear prev historical data

        //BEGIN TASK: TTL-VN VanTran 20100112 [iTrade R5] Start market data broadcast server
        if (IMain.getProperty("EnableSocketChannel").equalsIgnoreCase("true")) {
            //startMarketDataClient();
        }
        //END TASK: TTL-VN VanTran 20100112 [iTrade R5] Start market data broadcast server

        System.out.println("-=                 iTrade Servlet.startTP() End                  =-");
        System.out.println("-=***************************************************************=-");
    }

    private void startExternalTPChannel() {
        try {
            //BEGIN TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
            boolean lvTPExternalServerAvaliable = IMain.getProperty("ExternalTPServerAvailable").equals("true") ? true : false;
            //END TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
            boolean lvPluginExternalTPServerAvailable = IMain.getProperty("PluginExternalTPServerAvailable").equals("true") ? true : false;
            if (lvTPExternalServerAvaliable) {
                System.out.println("-=          iTrade Servlet.startExternalTPChannel() Begin        =-");
                System.out.println("-=***************************************************************=-");

                //BEGIN TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
                int lvTPExternalServerCount = Integer.parseInt(IMain.getProperty("ExternalTPServerCount"));

                HashMap lvTempTPManager = new HashMap();
                for (int i = 0, k = 1; i < lvTPExternalServerCount; i++, k++) {
                    String lvID = "ExternalTPServer" + k;
                    String[] lvExternalTPServerIP = {IMain.getProperty(lvID.concat("_IP"))};
                    int[] lvExternalTPServerPort = {Integer.parseInt(IMain.getProperty(lvID.concat("_PORT")))};
                    String lvFunctionNames = IMain.getProperty(lvID.concat("_RequestNameList"));
                    String lvRequestXMLListStr = IMain.getProperty(lvID.concat("_RequestXMLList"));
                    //String lvIPPort = lvID.concat(":").concat(String.valueOf(lvExternalTPServerPort[i]));
                    String lvIPPort = String.valueOf(lvExternalTPServerIP[i]).concat(":").concat(String.valueOf(lvExternalTPServerPort[i]));
                    String lvTPRequestXMLPath = IMain.getProperty("TPrequestXML.path");
                    String lvTPAgentID = IMain.getProperty(lvID.concat("_AgentID"));
                    String lvTPAgentPassword = IMain.getProperty(lvID.concat("_AgentPassword"));
                    String lvBranchID = IMain.getProperty(lvID.concat("_BranchID"));
                    String lvEntityID = IMain.getProperty(lvID.concat("_EntityID"));
                    //END TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
                    //BEGIN TASK # TTL-HK-WLCW-00951 Walter Lau 18 Dec 2009 set ReceiveBufferLen to configurable
                    int lvReceiveBufferLen = Integer.parseInt(IMain.getProperty(lvID.concat("_ReceiveBufferLen")));
                    int lvHeaderLength = Integer.parseInt(IMain.getProperty(lvID.concat("_HeaderLength")));
                    //END TASK # TTL-HK-WLCW-00951 Walter Lau 18 Dec 2009 set ReceiveBufferLen to configurable

                    Log.println(lvIPPort.concat(" TP Agent ID: ") + lvTPAgentID, Log.ACCESS_LOG);
                    Log.println(lvIPPort.concat(" TP Agent Password: ") + lvTPAgentPassword, Log.ACCESS_LOG);
                    Log.println(lvIPPort.concat(" Branch ID: ") + lvBranchID, Log.ACCESS_LOG);

                    //String lvIPPort = mvITradeProperties.getProperty(lvIDIP);
                    if (lvFunctionNames != null && !lvFunctionNames.trim().equals("")) {
                        TPManager lvExternalTPManager;
                        if (!lvTempTPManager.containsKey(lvIPPort)) {
                            //BEGIN TASK # TTL-HK-WLCW-00951 Walter Lau 18 Dec 2009 set ReceiveBufferLen to configurable
                            TPConnector lvExternalTPConnector = new TPConnector(lvExternalTPServerIP, lvExternalTPServerPort, lvReceiveBufferLen, lvHeaderLength);
                            Log.println("TP External Server Connector Buffer Size Default is : " + lvReceiveBufferLen, Log.ACCESS_LOG);
                            //END TASK # TTL-HK-WLCW-00951 Walter Lau 18 Dec 2009 set ReceiveBufferLen to configurable
                            lvExternalTPManager = new TPManager(lvExternalTPConnector, ivStartUpMain, "E");
                            //BEGIN TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
                            int lvRequestWaitTimout = Integer.parseInt(IMain.getProperty("RequestWaitTimeout"));
                            lvExternalTPManager.svProduct = IMain.getProperty("product");
                            //END TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
                            lvExternalTPManager.svRequestWaitTimeOut = lvRequestWaitTimout;

                            lvTempTPManager.put(lvIPPort, lvExternalTPManager);

                            TPExternalConnectorThread lvTPExternalConnectorThread = new TPExternalConnectorThread(lvExternalTPManager, lvExternalTPConnector);
                            lvTPExternalConnectorThread.setName(lvIPPort.concat("TPConnectorExternal"));
                            lvTPExternalConnectorThread.start();

                            lvExternalTPManager.start();
                        } else {
                            lvExternalTPManager = (TPManager) lvTempTPManager.get(lvIPPort);
                        }

                        StringTokenizer lvFunctionNameTokenizer = new StringTokenizer(lvFunctionNames, ",");

                        //String lvFunctionNameArray[] = lvFunctionNames.split(",");
                        //for (int j = 0; j < lvFunctionNameArray.length; j++)
                        while (lvFunctionNameTokenizer.hasMoreTokens()) {
                            //BEGIN - Task #: TTL-HK-WLCW-00921 - Walter Lau 20090917 Splict TP Connect and Operator Login Theard for fix Redownload issue
                            String lvFunctionName = (String) lvFunctionNameTokenizer.nextToken();
                            getSvTPExternalServerMap().put(lvFunctionName.trim(), lvExternalTPManager);
                            //END - Task #: TTL-HK-WLCW-00921 - Walter Lau 20090917 Splict TP Connect and Operator Login Theard for fix Redownload issue
                        }
                    }

                    String rootFolder = System.getProperty("basedirectory") + File.separator + "config" + File.separator;
                    String lvPath = rootFolder + lvTPRequestXMLPath + "/";

                    // added by mingl
                    ivStartUpMain.startExternalRequestRegistration(lvPath, lvEntityID, lvTPAgentID, lvTPAgentPassword, lvBranchID, lvFunctionNames, lvRequestXMLListStr);
                }
                System.out.println("-=          iTrade Servlet.startExternalTPChannel() END          =-");
                System.out.println("-=***************************************************************=-");
            }
            startPluginExternalTPChannel(lvPluginExternalTPServerAvailable);

            if (!lvTPExternalServerAvaliable && !lvPluginExternalTPServerAvailable) {
                System.out.println("-=***************************************************************=-");
                System.out.println("-=        iTrade Servlet.ExternalTPChannel Not Avaliable         =-");
                System.out.println("-=***************************************************************=-");
            }
        } catch (Throwable ex) {
            Log.print(ex, Log.ERROR_LOG);
        }
    }

    private void startPluginExternalTPChannel(boolean pPluginExternalTPServerAvailable) {
        if (pPluginExternalTPServerAvailable) {
            System.out.println("-=       ITradeServlet.startPluginExternalTPChannel() Begin      =-");
            System.out.println("-=***************************************************************=-");
            int lvPluginExternalServerCount = Integer.parseInt(IMain.getProperty("PluginExternalTPServerCount"));
            HashMap lvTempTPManager = new HashMap();
            for (int i = 0, k = 1; i < lvPluginExternalServerCount; i++, k++) {
                String lvID = "PluginExternalTPServer" + k;
                String[] lvExternalTPServerIP = {IMain.getProperty(lvID.concat("_IP"))};
                int[] lvExternalTPServerPort = {Integer.parseInt(IMain.getProperty(lvID.concat("_PORT")))};
                String lvFunctionNames = IMain.getProperty(lvID.concat("_RequestNameList"));
                String lvRequestXMLListStr = IMain.getProperty(lvID.concat("_RequestXMLList"));
                String lvIPPort = String.valueOf(lvExternalTPServerIP[i]).concat(":").concat(String.valueOf(lvExternalTPServerPort[i]));
                String lvTPAgentID = IMain.getProperty(lvID.concat("_AgentID"));
                String lvTPAgentPassword = IMain.getProperty(lvID.concat("_AgentPassword"));
                String lvBranchID = IMain.getProperty(lvID.concat("_BranchID"));
                String lvEntityID = IMain.getProperty(lvID.concat("_EntityID"));
                int lvReceiveBufferLen = Integer.parseInt(IMain.getProperty(lvID.concat("_ReceiveBufferLen")));
                int lvHeaderLength = Integer.parseInt(IMain.getProperty(lvID.concat("_HeaderLength")));

                if (lvFunctionNames != null && !lvFunctionNames.trim().equals("")) {
                    TPManager lvExternalTPManager;
                    if (!lvTempTPManager.containsKey(lvIPPort)) {
                        TPConnector lvExternalTPConnector = new TPConnector(lvExternalTPServerIP, lvExternalTPServerPort, lvReceiveBufferLen, lvHeaderLength);
                        Log.println("TP External Server Connector Buffer Size Default is : " + lvReceiveBufferLen, Log.ACCESS_LOG);
                        lvExternalTPManager = new TPManager(lvExternalTPConnector, ivStartUpMain, "E");
                        int lvRequestWaitTimout = Integer.parseInt(IMain.getProperty("RequestWaitTimeout"));
                        lvExternalTPManager.svProduct = IMain.getProperty("product");
                        lvExternalTPManager.svRequestWaitTimeOut = lvRequestWaitTimout;
                        lvTempTPManager.put(lvIPPort, lvExternalTPManager);
                        TPExternalConnectorThread lvTPExternalConnectorThread = new TPExternalConnectorThread(lvExternalTPManager, lvExternalTPConnector);
                        lvTPExternalConnectorThread.setName(lvIPPort.concat("TPConnectorExternal"));
                        lvTPExternalConnectorThread.start();
                        lvExternalTPManager.start();
                    } else {
                        lvExternalTPManager = (TPManager) lvTempTPManager.get(lvIPPort);
                    }
                    StringTokenizer lvFunctionNameTokenizer = new StringTokenizer(lvFunctionNames, ",");
                    while (lvFunctionNameTokenizer.hasMoreTokens()) {
                        String lvFunctionName = (String) lvFunctionNameTokenizer.nextToken();
                        getSvTPExternalServerMap().put(lvFunctionName.trim(), lvExternalTPManager);
                    }
                }
                if (getMvPluginITradeProperties() != null) {
                    String rootFolder = System.getProperty("basedirectory") + File.separator + "config" + File.separator;
                    Object lvPluginTPRequestXMLPathObj = rootFolder + IMain.getProperty("TPRequestXMLPath");
                    if (lvPluginTPRequestXMLPathObj != null) {
                        String lvPluginTPRequestXMLPathStr = lvPluginTPRequestXMLPathObj.toString() + "/";
                        File file = new File(lvPluginTPRequestXMLPathStr);
                        boolean isPluginTPRequestXMLPathExist = file.isDirectory();
                        if (isPluginTPRequestXMLPathExist) {
                            Log.println("TP External Server Plugin Request XML Path: " + lvPluginTPRequestXMLPathStr, Log.ACCESS_LOG);
                            ivStartUpMain.startPluginExternalRequestRegistration(lvPluginTPRequestXMLPathStr, lvEntityID, lvTPAgentID, lvTPAgentPassword, lvBranchID, lvFunctionNames, lvRequestXMLListStr);
                        } else {
                            Log.println("The Directory [ " + lvPluginTPRequestXMLPathStr + " ] For TP Server Plugin Request XML Path Does Not Exist.", Log.ACCESS_LOG);
                        }
                    }
                }
            }
            System.out.println("-=        ITradeServlet.startPluginExternalTPChannel() End      =-");
            System.out.println("-=***************************************************************=-");
        }
    }

    public static Properties getMvITradeProperties() {
        return mvITradeProperties;
    }

    public static void setMvITradeProperties(Properties mvITradeProperties) {
        ITradeServlet.mvITradeProperties = mvITradeProperties;
    }

    public static boolean getDownloadInstrumentStarted() {
        return isSvDownloadInstrumentStarted();
    }

    public static void setDownloadInstrumentStarted(boolean svDownloadInstrumentStarted) {
        ITradeServlet.setSvDownloadInstrumentStarted(svDownloadInstrumentStarted);
    }

    public static long getReDownloadDataTime() {
        return getSvReDownloadDataTime();
    }

    public static void setReDownloadDataTime(long svReDownloadDataTime) {
        ITradeServlet.setSvReDownloadDataTime(svReDownloadDataTime);
    }

    public static int getReDownloadDataDownloadTimes() {
        return getSvReDownloadDataDownloadTimes();
    }

    public static void setReDownloadDataDownloadTimes(int svReDownloadDataDownloadTimes) {
        ITradeServlet.setSvReDownloadDataDownloadTimes(svReDownloadDataDownloadTimes);
    }

    public static boolean getReDownloadDataEnable() {
        return isSvReDownloadDataEnable();
    }

    public static void setReDownloadDataEnable(boolean svReDownloadDataEnable) {
        ITradeServlet.setSvReDownloadDataEnable(svReDownloadDataEnable);
    }


    public Hashtable getMvAvailableActions() {
        return mvAvailableActions;
    }

    public void setMvAvailableActions(Hashtable mvAvailableActions) {
        this.mvAvailableActions = mvAvailableActions;
    }
}









































