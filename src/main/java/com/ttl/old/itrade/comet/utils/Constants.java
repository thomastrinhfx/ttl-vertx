package com.ttl.old.itrade.comet.utils;

public interface Constants {
	public static final String XTYPE = "xtype";
	public static final String MARKET_INDEX_TYPE = "dynMarketIndex";
	public static final String WATCH_LIST_TYPE = "dynWatchList";
	public static final String ORDER_LIST_TYPE = "dynOrderList";
	public static final String WARNING_LIST_TYPE = "dynWarningList";
	public static final String CASH_BALANCE_TYPE = "dynCashBalance";
	public static final String WORLD_MARKET_INDEX_TYPE = "dynWorldMarketIndex";
	public static final String STOCK_WATCH_TYPE = "dynStockWatch";
	public static final String SEPERATE_CHAR = ",";
	public static final String SEPERATE_EXCHANGE_CHAR = "-";
	public static final String SERVER_TIME_PATTERN = "dd/MM/yyyy hh:mm:ss";
	public static final String SERVER_TIME_PATTERN_24H = "dd/MM/yyyy HH:mm:ss";
	public static final int CONNECTED_SUCCESS = 0;
	public static final int ERROR_FO_CONNECTED_FAILED = 1;
	public static final int ERROR_BO_CONNECTED_FAILED = 2;
	public static final int ERROR_SESSION_EXPIRE = 3;	
	
	public static final int ERROR_INVALID_ACTION = 4;	
	public static final int ERROR_NOT_REGISTERED = 5;	
	
	//Alert info
	public static final String ALERT_TYPE_ORDER = "order";
	public static final String ALERT_TYPE = "alertType";
	public static final String ALERT_TIME = "alertTime";
	public static final String ALERT_STOCK_ID = "stockId";
	public static final String ALERT_ORDER_GROUP_ID = "orderGroupId";
	public static final String ALERT_ORDER_ID = "orderId";
	public static final String ALERT_ORDER_STATUS = "orderStatus";
}
