/*package com.itrade.comet.cometManagement;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import EventMainProcessor;
import MainScheduler;
import DataTranslator;
import DataTranslatorFactory;
import com.itrade.comet.translator.DataTranslatorImpl;
import Constants;
import IMain;
import com.itrade.mds.restful.DataServiceReader;
import com.systekit.winvest.hks.config.mapping.TagName;



public class TtlcometServlet extends HttpServlet{

	private static final long serialVersionUID = -4754360224201107865L;
	private static final Logger logger = LogManager.getLogger(TtlcometServlet.class);
	//Type of action
	private final String INIT_ACTION = "init";
	private final String UPDATE_ACTION = "update";
	private final String REG_ACTION = "register";
	private final String UNREG_ACTION = "unregister";
	private final String LOGOUT_ACTION = "logout";
	
	//Suspend time out that came from the config file
	private Integer suspendTimeout;	
	//The number of request workers that came from the config file
	private Integer numRequestWorkers;
	//Manage the main processor
	private EventMainProcessor eventMainProcessor = null;
	
	//private int currentIndex = 0;
	//private int currentIndex = 0;
	
	public void init(ServletConfig config) throws ServletException {
		 this.initParamsFromConfig(config);
		 logger.info("Begin initialize");
	    	super.init(config);
	    	try {
	    		final ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
	    		//Load configuration
	    		this.initParamsFromConfig(config);
	    		//Set data translator
	    		final DataTranslator dataTranslator = new DataTranslatorImpl();//(DataTranslator) context.getBean("dataTranslator");
	    		DataTranslatorFactory.setDataTranslator(dataTranslator);    		
	        	//Initialize browser manager
	        	BrowserManager.init(numRequestWorkers);
	        	//Initialize event main processor
	        	eventMainProcessor.init(context);
	        	final List<RequestWorker> workers = BrowserManager.getWorkers();
	        	for (RequestWorker w : workers) {
	        		eventMainProcessor.addBroadcaster(w);
	        	}
	        	
	        	//BEGIN - TASK : Tan Huynh 20111224 - setup Data cache service
	        	boolean useDS = Boolean.valueOf(IMain.getProperty("UseMds"));
	        	if(useDS)
	        		new Thread(new DataServiceReader()).start();		
	    		//END - TASK : Tan Huynh 20111224 - setup Data cache service
	        	
	    	} catch (Exception ex) {
	    		logger.error("Initialize error", ex);
	    	}
	    	logger.info("End initialize");
		 
	}
	
	@Override
	protected void doGet(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}
	
    @Override
	protected void doPost(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException, IOException {
    	
    	//Some init params of response
    	response.setHeader("Cache-Control","no-cache");
		response.setHeader("Pragma","no-cache");
		response.setDateHeader ("Expires", 0);
		response.setHeader("Content-type", "application/json");
		//response.setHeader("Content-type", "text/javascript");
		response.setCharacterEncoding("utf-8");
		//Type of action
		final String action = request.getParameter("action");

		try
		{
			//Check login action
			String accountNo = String.valueOf(request.getSession().getAttribute(TagName.CLIENTID));
			logger.debug("Client already login with id = " + accountNo);
			if (INIT_ACTION.equals(action)) {
				//Init for the first action
				initAction(request, response, accountNo);
			} else if (UPDATE_ACTION.equals(action)) {
				//Update action
				updateAction(request, response, accountNo);
			} else if (REG_ACTION.equals(action)) {
				//Register action
				registerAction(request, response, accountNo);
			} else if (UNREG_ACTION.equals(action)) {
				//UnRegister action
				unRegisterAction(request, response, accountNo);
			} else if (LOGOUT_ACTION.equals(action)) {
				//Log out action
				logoutAction(request, response, accountNo);
			} else {
				//invalid action
				this.write(response, String.format("{'success':false, 'mvResult':'%d'}", Constants.ERROR_INVALID_ACTION));
				//this.write(response, demodata);
			}
		}
		catch(Exception ex)
		{
			logger.error(ex.getMessage(), ex);	
			//this.write(response, "Error");
		}
	}
	 
	private void write(final HttpServletResponse response, final String data) {
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(data);
			out.flush();
		} catch (Exception ex) {
			logger.error("Cannot write data to client ", ex);
		} finally {
			if (out != null)
				try {
					out.close();
				} catch (Exception ex) {}
		}		
	}
    
    // Get default param from config file
    private void initParamsFromConfig(ServletConfig conf)
	{
    	numRequestWorkers = Integer.parseInt(conf.getInitParameter("numRequestWorker"));
    	suspendTimeout = Integer.parseInt(conf.getInitParameter("suspendSessionTimeout"));
    	eventMainProcessor = new EventMainProcessor();
	}
    
    *//**
	 * Process register request.
	 * 
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @throws ServletException
	 * @throws IOException
	 *//*
	private void initAction(final HttpServletRequest request,
			final HttpServletResponse response, final String accountNo) 
			throws ServletException, IOException {
		try
		{
			//accountType = request.getSession().getAttribute(HKSTag.ACCOUNTTYPE).toString();
			// try to remove existing continuations
			try {
				logger.info(">>> expired existing continuations");
				int count = ContinuationUtils.expiredExistingContinuations(request);
				logger.info("<<< expired existing continuations: " + count);
			} catch (Exception e) {
				logger.error("Remove previous continuation failed:", e);
			}
			BrowserManager.register(accountNo, request.getSession(), suspendTimeout, "", response);
			logger.info(String.format("Client %s init success", accountNo));
		}
		catch(Exception ex)
		{
			logger.error("Cannot comet init action because not login yet ", ex);
		}
	}
	*//**
	 * Process poll request.
	 * 
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @throws ServletException
	 * @throws IOException
	 *//*
	private void updateAction(final HttpServletRequest request,
			final HttpServletResponse response, final String accountNo)
		throws ServletException, IOException {
		BrowserManager.dispatchPolling(accountNo, request, response);
	}	
	
	//Unregister dynamic update info
    private void unRegisterAction(final HttpServletRequest request,
			final HttpServletResponse response, final String accountNo) 
    	throws ServletException, IOException{		
    	int result = BrowserManager.registerDynamicUpdateInfo(accountNo, request, response, false);
    	if(result != Constants.CONNECTED_SUCCESS)
    		this.write(response, String.format("{'success':false, 'mvResult':'%d'}", result));
	}

    //Register dynamic update action
	private void registerAction(final HttpServletRequest request,
			final HttpServletResponse response, final String accountNo) 
		throws ServletException, IOException{
		int result = BrowserManager.registerDynamicUpdateInfo(accountNo, request, response, true);
		if(result != Constants.CONNECTED_SUCCESS)
			this.write(response, String.format("{'success':false, 'mvResult':'%d'}", result));
	}
	
	//Log out action
	private void logoutAction(final HttpServletRequest request,
			final HttpServletResponse response, final String accountNo){
		BrowserManager.unregisterByAccount(accountNo);		
	}
	
	*//**
	 * @see HttpServlet#destroy()
	 *//*
	@Override
	public void destroy() {
		logger.info("Begin destroying");
		MainScheduler.destroy();
		BrowserManager.destroy();
		eventMainProcessor.destroy();
		logger.info("End destroying");
	}
}
*/