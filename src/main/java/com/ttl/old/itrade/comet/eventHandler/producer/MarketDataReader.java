package com.ttl.old.itrade.comet.eventHandler.producer;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.ttl.old.itrade.comet.dataInfo.MarketInfo;
import com.ttl.old.itrade.comet.eventHandler.caching.MarketDataCachingManager;
import com.ttl.old.itrade.comet.eventHandler.generic.EventProducer;
import com.ttl.old.itrade.comet.eventHandler.generic.EventQueue;
import com.ttl.old.itrade.hks.bean.HKSMarketDataBean;
import com.ttl.old.itrade.hks.util.StockPriceInfoParser;
import com.ttl.old.itrade.hks.util.StringSplitter;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.AppletGateway;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

//import com.itrade.mds.restful.DataServiceReader;
import com.ttl.mds.model.BestPriceModel;
import com.ttl.mds.model.HistoricalDataModel;
import com.ttl.mds.model.StockInfoModel;



public class MarketDataReader extends EventProducer<MarketInfo>
{
	private static final Logger logger = LogManager.getLogger(MarketDataReader.class);
	private List<HKSMarketDataBean> mvMarketDataList = new ArrayList<HKSMarketDataBean>();
	private MarketInfo marketInfo = new MarketInfo();
	
	private Set<String> symbolsInfo = new TreeSet<String>();
	private Date lastModified = null;
	private Iterator<String> iterator = null;
	
	@Override
	public void product(EventQueue<MarketInfo> eventQueue) {
		boolean useDS = Boolean.valueOf(IMain.getProperty("UseMds"));
		if(useDS)
		{
			processStockInfoFromDS(eventQueue);
		}
		else
		{
			processStockInfoFromMarketProject(eventQueue);	
		}
	}
	
	private void processStockInfoFromMarketProject(
			EventQueue<MarketInfo> eventQueue) {
		try
		{
			synchronized(marketInfo)
			{
				Date lastModifiedCycle = lastModified;
				synchronized(AppletGateway.ivPendingMarketData)
				{
					if(AppletGateway.ivPendingMarketData!=null){
						mvMarketDataList.clear();
						String value = "";
						for(String key: AppletGateway.ivPendingMarketData.keySet())
						{
							value = AppletGateway.ivPendingMarketData.get(key);
							if(value !=null && !value.isEmpty()){
								String[] lvTempMarketDataArray = StringSplitter.split(value, "\\|");
								HKSMarketDataBean marketData = StockPriceInfoParser.HBBSStockPriceInfoParsing(lvTempMarketDataArray);
								if(lastModified == null || lastModified.before(marketData.getLstUpdate()))
								{
									mvMarketDataList.add(marketData);
									//Save for caching market data: key: marketId|instrumentId
									MarketDataCachingManager.put(key, marketData);
									if(lastModifiedCycle == null || lastModifiedCycle.before((marketData.getLstUpdate())))
									{
										//Get the last modified time in a cycle
										lastModifiedCycle = marketData.getLstUpdate();
									}
								}
							}
						}
						
						if(mvMarketDataList.size() > 0)
						{
							lastModified = lastModifiedCycle;
							marketInfo.setMarketData(mvMarketDataList);
							marketInfo.setLastUpdate(lastModified);
							eventQueue.addDataEvent(marketInfo);
						}
					}
				}
			}
		}
		catch (ConcurrentModificationException ex) {
			//logger.error("Pending List is modification", ex);
		}
		catch(Exception ex)
		{
			logger.error(ex);
		}			
	}
	
	private void processStockInfoFromDS(final EventQueue<MarketInfo> eventQueue)
	{
		try
		{
			synchronized(mvMarketDataList)
			{
				mvMarketDataList.clear();
				Date lastModifiedCycle = lastModified;
				symbolsInfo.clear();
				symbolsInfo.addAll(MarketDataCachingManager.getRegisterSymbols());
				symbolsInfo.addAll(MarketDataCachingManager.getWarningSymbols());
				StockInfoModel stockInfo = null;
				HistoricalDataModel histData = null;
				BestPriceModel bestPrice = null;
				String[] symbolInfoList = null;
				String marketId = null;
				String symbol = null;
				iterator = symbolsInfo.iterator();
				while(iterator.hasNext())
				{					
					try {
						String symbolInfo = iterator.next();
						symbolInfoList = symbolInfo.split("\\|");
						if(symbolInfoList != null && symbolInfoList.length == 2)
						{
							marketId = symbolInfoList[0];
							symbol = symbolInfoList[1];
							//stockInfo = DataServiceReader.getStockInfoModel(marketId, symbol);
							//histData = DataServiceReader.getHistoricalModel(marketId, symbol);
							//bestPrice = DataServiceReader.getBestPriceModel(marketId, symbol);
							if(stockInfo == null)
								continue;
							HKSMarketDataBean marketData = StockPriceInfoParser.StockInfoParsing(stockInfo, histData, bestPrice);
							if(lastModified == null || lastModified.before(marketData.getLstUpdate()))
							{
								mvMarketDataList.add(marketData);
								//Save for caching market data: key: marketId|instrumentId
								MarketDataCachingManager.put(symbolInfo, marketData);
								if(lastModifiedCycle == null || lastModifiedCycle.before((marketData.getLstUpdate())))
								{
									//Get the last modified time in a cycle
									lastModifiedCycle = marketData.getLstUpdate();
								}
							}
							
							
						}
					} catch (Exception e) {
						e.printStackTrace();
					}					
				}
				
				if(mvMarketDataList.size() > 0)
				{
					lastModified = lastModifiedCycle;
					marketInfo.setMarketData(new ArrayList<HKSMarketDataBean>(mvMarketDataList));
					marketInfo.setLastUpdate(lastModified);
					eventQueue.addDataEvent(marketInfo);
				}
			}
		}
		catch(Exception ex)
		{
			logger.error(ex);
		}
	}
}
