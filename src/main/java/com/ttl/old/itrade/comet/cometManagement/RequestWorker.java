package com.ttl.old.itrade.comet.cometManagement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ttl.old.itrade.comet.eventHandler.AbstractBroadcaster;
import com.ttl.old.itrade.comet.eventHandler.caching.AccountSummaryDataCachingManager;
import com.ttl.old.itrade.comet.eventHandler.caching.StockWatchDataCachingManager;
import com.ttl.old.itrade.comet.utils.Constants;
import com.ttl.old.itrade.hks.bean.HKSAccountBalanceEnquiryBean;
import com.ttl.old.itrade.hks.bean.HKSOrderBean;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.ttl.old.itrade.comet.dataInfo.AlertInfo;
import com.ttl.old.itrade.comet.dataInfo.CommonInfo;
import com.ttl.old.itrade.comet.dataInfo.MarketIndex;
import com.ttl.old.itrade.comet.dataInfo.MarketInfo;
import com.ttl.old.itrade.comet.dataInfo.OrderEnquiryInfo;
import com.ttl.old.itrade.comet.dataInfo.StockWatchInfo;
import com.ttl.old.itrade.comet.dataInfo.StockWatchList;
import com.ttl.old.itrade.comet.dataInfo.WorldMarketIndex;

public class RequestWorker extends AbstractBroadcaster {
	private static final Logger logger = LogManager
			.getLogger(RequestWorker.class);
	private final Map<String, Browser> mapClients;
	private boolean balancingClient = false;
	private final int workerId;
	
	/**
	 * Constructor.
	 */
	public RequestWorker(final int workerId) {
		super("RequestWorker#" + workerId);
		this.workerId = workerId;
		this.mapClients = new HashMap<String, Browser>(){

			private static final long serialVersionUID = -5614789304617781824L;

			@Override
			public Browser remove(Object key) {
				Browser removed = super.remove(key);
//				try {
//					// just for debug
//					Log.println(removed.toString(), Log.DEBUG_LOG);
//					// print stack trace
//					Exception e = new RuntimeException("### Remove client id: " + key);
//					Log.println(e, Log.DEBUG_LOG);
//				} catch (Exception e) {}
				return removed;
			}
			
			
		};
	}

	/**
	 * Validate clients.
	 * 
	 * @return Number of invalid clients.
	 */
	public final int validateClients() {
		final Collection<Browser> clients = copyClients();
		int numInvalid = 0;
		for (Browser c : clients) {
			if (c.checkAge())
				continue;
			synchronized (mapClients) {
				if (mapClients.remove(c.getAccount()) != null) {
					numInvalid++;
					AccountSummaryDataCachingManager.unregisterAccount(c
							.getAccount());
				}
			}
		}
		if(clients != null)
			clients.clear();
		return numInvalid;
	}

	/**
	 * @return the workerId
	 */
	public int getWorkerId() {
		return workerId;
	}

	/**
	 * @return the balancingClient
	 */
	public boolean isBalancingClient() {
		return balancingClient;
	}

	/**
	 * @param balancingClient
	 *            the balancingClient to set
	 */
	public void setBalancingClient(boolean balancingClient) {
		this.balancingClient = balancingClient;
	}

	/**
	 * Response server time for each client.
	 */
	public final void responseServerTime(final String serverTime) {
		Collection<Browser> clients = copyClients();
		for (Browser c : clients)
			c.response(serverTime);
		clients.clear();
	}

	/**
	 * Register a client.
	 * 
	 * @param client
	 *            Client
	 */
	public final int resgisterClient(final Browser client) {
		synchronized (mapClients) {
			final String account = client.getAccount();
			final String accountType = client.getAccountType();
			mapClients.put(account, client);
			AccountSummaryDataCachingManager.registerAccount(account,
					accountType);
			StockWatchDataCachingManager.registerStockWatch(account, "", "", "");
			return mapClients.size();
		}
	}

	/**
	 * Get a client based on a given account.
	 * 
	 * @param account
	 *            String
	 * @return Client or null
	 */
	public final Browser getClient(final String account) {
		synchronized (mapClients) {
			return mapClients.get(account);
		}
	}

	/**
	 * Check the worker handler a client with given account.
	 * 
	 * @param account
	 *            String
	 * @return True if the worker contains, otherwise false
	 */
	public final boolean containsClient(final String account) {
		synchronized (mapClients) {
			return mapClients.containsKey(account);
		}
	}

	/**
	 * Get number of clients handled by this worker.
	 * 
	 * @return int
	 */
	public final int getNumClients() {
		synchronized (mapClients) {
			return mapClients.size();
		}
	}

	/**
	 * Unregister a client by account.
	 * 
	 * @param account
	 *            String
	 * @return Client or null
	 */
	public final Browser unresgisterByAccount(final String account) {
		synchronized (mapClients) {
			final Browser c = mapClients.remove(account);
			if (c != null) {
				c.forceExpire();
				if (logger.isDebugEnabled())
					logger.debug(String.format(
							"RequestWorker#%d removes client %s", workerId,
							c.getAccount()));
			}
			return c;
		}
	}

	/**
	 * Check to remove clients if number of client > expectedRemainClients.
	 * 
	 * @param expectedRemainClients
	 *            int
	 * @return List of removed clients or null
	 */
	public final List<Browser> checkToRemove(final int expectedRemainClients) {
		final List<Browser> list = new ArrayList<Browser>();
		synchronized (mapClients) {
			if (mapClients.size() <= expectedRemainClients)
				return null;
			list.addAll(mapClients.values());
			mapClients.clear();
			while (mapClients.size() < expectedRemainClients) {
				final Browser c = list.remove(0);
				mapClients.put(c.getAccount(), c);
			}
			return list;
		}
	}

	@Override
	protected final void beforeStart() {
	}

	/**
	 * Response data to clients.
	 * 
	 * @param data
	 *            Object[]
	 */
	@Override
	protected final void broadcast(final Object... data) {		
		// Broadcast data to browser
		for (Object obj : data) {
			if (obj instanceof HKSAccountBalanceEnquiryBean) {
				responseAccountSummary((HKSAccountBalanceEnquiryBean) obj);
			} else if (obj instanceof MarketInfo) {
				// broadcast Market data - watch list and warning list
				responseMarketData((MarketInfo) obj);
			} else if (obj instanceof MarketIndex) {
				// broadcast Market index
				responseMarketIndex((MarketIndex) obj);
			} else if (obj instanceof WorldMarketIndex) {
				// broadcast World Market index
				responseWorldMarketIndex((WorldMarketIndex) obj);
			} else if (obj instanceof StockWatchList) {
				responseStockWatchInfo((StockWatchList) obj);
			} else if (obj instanceof OrderEnquiryInfo) {
				responseOrderEnquiryInfo((OrderEnquiryInfo) obj);
			} else if (obj instanceof AlertInfo) {
				responseAlertInfo((AlertInfo) obj);
			} else if (obj instanceof CommonInfo) {
				responseCommonInfo((CommonInfo) obj);
			}				
		}
	}	
	
	
	private void responseOrderEnquiryInfo(OrderEnquiryInfo obj) {
		Browser client = null;
		List<HKSOrderBean> orderList = obj.getOrderBeanList();
		if (orderList != null && orderList.size() > 0) {
			synchronized (mapClients) {
				client = mapClients.get(orderList.get(0).getMvClientID());
			}
			if (client != null) {
				client.responseOrderEnquiryInfo(orderList);
			}
		}

	}

	private void responseAlertInfo(AlertInfo obj) {
		Browser client = null;
		synchronized (mapClients) {
			client = mapClients.get(obj.getClientId());
		}
		if (client != null) {
			client.responseAlertInfo(obj.getAlerts());
		}
	}
	
	private void responseStockWatchInfo(StockWatchList obj) {
		Browser client = null;
		synchronized (mapClients) {
			Map<String, List<StockWatchInfo>> map = new HashMap<String, List<StockWatchInfo>>();
			for(StockWatchInfo info: obj.getStocks())
			{
				List<StockWatchInfo> list = map.get(info.getMvClientId());
				if(list == null)
				{
					list = new ArrayList<StockWatchInfo>();					
				}
				
				list.add(info);
				map.put(info.getMvClientId(), list);
			}
			
			for(String clientId: map.keySet())
			{
				client = mapClients.get(clientId);
				
				if (client != null) {
					StockWatchList swList = new StockWatchList();
					swList.setStocks(map.get(clientId));
					client.responseStockWatchInfo(swList);
				}
			}
		}
	}

	private void responseAccountSummary(
			final HKSAccountBalanceEnquiryBean accountSummary) {
		Browser client = null;
		synchronized (mapClients) {
			client = mapClients.get(accountSummary.getMvClientId());
		}
		if (client != null) {
			client.responseAccountSummary(accountSummary);
		}
	}

	private void responseMarketData(MarketInfo market) {
		Browser client = null;
		synchronized (mapClients) {
			for (String key : mapClients.keySet()) {
				client = mapClients.get(key);
				if (client != null) {
					client.responseMarketData(market);
				} else {
					mapClients.remove(key);
				}
			}
		}
	}

	private void responseMarketIndex(MarketIndex marketIndex) {
		Browser client = null;
		synchronized (mapClients) {
			for (String key : mapClients.keySet()) {
				client = mapClients.get(key);
				if (client != null) {
					client.responseMarketIndex(marketIndex);
				} else {
					mapClients.remove(key);
				}
			}
		}
	}

	private void responseCommonInfo(CommonInfo info) {
		Browser client = null;
		synchronized (mapClients) {
			for (String key : mapClients.keySet()) {
				client = mapClients.get(key);
				if (client != null) {
					client.responseCommonInfo(info);
				} else {
					mapClients.remove(key);
				}
			}
		}
	}
	
	private void responseWorldMarketIndex(WorldMarketIndex wMarketIndex) {
		Browser client = null;
		synchronized (mapClients) {
			for (String key : mapClients.keySet()) {
				client = mapClients.get(key);
				if (client != null) {
					client.responseWorldMarketIndex(wMarketIndex);
				} else {
					mapClients.remove(key);
				}
			}
		}
	}

	/**
	 * Copy clients.
	 * 
	 * @return List<Client> (never null)
	 */
	protected final Collection<Browser> copyClients() {
		final Collection<Browser> copy = new ArrayList<Browser>();
		synchronized (mapClients) {
			if(mapClients.size()> 0)
				copy.addAll(mapClients.values());
		}
		return copy;
	}

	/**
	 * Get all accounts handled by this worker.
	 * 
	 * @return List<String> (never null)
	 */
	public final List<String> getAllAccounts() {
		final List<String> list = new ArrayList<String>();
		synchronized (mapClients) {
			list.addAll(mapClients.keySet());
		}
		return list;
	}
	
	/**
	 * Get all specific accounts handled by this worker.
	 * 
	 * @return List<String> (never null)
	 */
	public final List<String> getAllAccounts(String filterType) {
		final List<String> list = new ArrayList<String>();
		synchronized (mapClients) {
			Browser client = null;
			for(String clientId: mapClients.keySet())
			{
				client = mapClients.get(clientId);
				if(client != null)
				{
					if(Constants.MARKET_INDEX_TYPE.equalsIgnoreCase(filterType))
					{
						if(client.isEnableMarketIndex())
							list.add(clientId);
					}
					else if(Constants.CASH_BALANCE_TYPE.equalsIgnoreCase(filterType))
					{
						if(client.isEnableAccountBalance())
							list.add(clientId);
					}
					else if(Constants.WORLD_MARKET_INDEX_TYPE.equalsIgnoreCase(filterType))
					{
						if(client.isEnableWorldMarketIndex())
							list.add(clientId);
					}
						
				}				
			}
		}
		return list;
	}
}
