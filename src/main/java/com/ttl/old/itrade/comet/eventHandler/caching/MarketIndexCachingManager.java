package com.ttl.old.itrade.comet.eventHandler.caching;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.ttl.old.itrade.comet.dataInfo.MarketIndex;
import com.ttl.old.itrade.hks.bean.HKSMarketIndexBean;


public class MarketIndexCachingManager {
	//private static final WTradeLogger logger = LogManager.getLogger(MarketIndexCachingManager.class);
	//private static Map<String, HKSMarketIndexBean> lastMarketIndex = new HashMap<String, HKSMarketIndexBean>();
	private static MarketIndex lastMarketIndex = new MarketIndex();
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	
	public static boolean updateMarketDataIfChanged(final List<HKSMarketIndexBean> marketIndex) {
		synchronized (lastMarketIndex) {
			HKSMarketIndexBean oMIndex = null;
			boolean listChanged = false;
			boolean itemChanged = false;
			for(HKSMarketIndexBean nMIndex: marketIndex)
			{
				itemChanged = false;
				oMIndex = lastMarketIndex.get(nMIndex.getMvMaketID());
				if(oMIndex != null)
				{
					if(MarketIndexCachingManager.isChanged(oMIndex, nMIndex))
					{
						itemChanged = true;
					}
				}
				else
				{					
					itemChanged = true;
				}
				
				nMIndex.setUpdated(itemChanged);
				if(itemChanged == true)
				{
					lastMarketIndex.put(nMIndex.getMvMaketID(), nMIndex);
					listChanged = true;
				}
			}
			return listChanged;
		}
	}

	private static boolean isChanged(HKSMarketIndexBean oMarketIndex, HKSMarketIndexBean nMarketIndex)
	{
		boolean isChanged = false;
		if(!nMarketIndex.getMvMarketIndex().equals(oMarketIndex.getMvMarketIndex())
			|| !nMarketIndex.getMvDifference().equals(oMarketIndex.getMvDifference())
			|| !nMarketIndex.getMvPercentage().equals(oMarketIndex.getMvPercentage())
			|| !nMarketIndex.getMvMarketTotalQty().equals(oMarketIndex.getMvMarketTotalQty())
			|| !nMarketIndex.getMvMarketTotalvalue().equals(oMarketIndex.getMvMarketTotalvalue())
			|| !nMarketIndex.getMvMarketAdvances().equals(oMarketIndex.getMvMarketAdvances())
			|| !nMarketIndex.getMvMarketDeclines().equals(oMarketIndex.getMvMarketDeclines())
			|| !nMarketIndex.getMvMarketNoChange().equals(oMarketIndex.getMvMarketNoChange())
			|| !nMarketIndex.getMvMarketStatus().equals(oMarketIndex.getMvMarketStatus()))
		{
			Date date = new Date();
			nMarketIndex.setMvMarketTime(sdf.format(date));
			isChanged = true;
			//System.out.println("Old:" + oMarketIndex.toString());
			//System.out.println("New:" + nMarketIndex.toString());
		}
		return isChanged;
		//return true;
	}

	public static MarketIndex getLastMarketIndex() {
		return lastMarketIndex;
	}

	public static void setLastMarketIndex(MarketIndex lastMarketIndex) {
		MarketIndexCachingManager.lastMarketIndex = lastMarketIndex;
	}
	
	public static void clearMarketIndex(){
		lastMarketIndex = new MarketIndex();
	}
	
}
