package com.ttl.old.itrade.comet.eventHandler;

import java.util.ArrayList;
import java.util.List;

import com.ttl.old.itrade.comet.dataInfo.AlertInfo;
import com.ttl.old.itrade.comet.dataInfo.CommonInfo;
import com.ttl.old.itrade.comet.dataInfo.OrderEnquiryInfo;
import com.ttl.old.itrade.comet.dataInfo.WorldMarketIndex;
import com.ttl.old.itrade.comet.eventHandler.generic.EventConsumer;
import com.ttl.old.itrade.comet.eventHandler.producer.AlertInfoReader;
import com.ttl.old.itrade.hks.bean.HKSAccountBalanceEnquiryBean;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.ttl.old.itrade.comet.dataInfo.MarketIndex;
import com.ttl.old.itrade.comet.dataInfo.MarketInfo;
import com.ttl.old.itrade.comet.dataInfo.StockWatchList;
import com.ttl.old.itrade.comet.eventHandler.generic.EventProcessor;
import com.ttl.old.itrade.comet.eventHandler.generic.NotificationAction;
import com.ttl.old.itrade.comet.eventHandler.producer.CommonInfoReader;
import com.ttl.old.itrade.comet.eventHandler.producer.MarketIndexReader;
//import com.itrade.comet.eventHandler.producer.OrderEnquiryReader;
//import com.itrade.comet.eventHandler.producer.StockInfoCollector;
//import com.itrade.comet.eventHandler.producer.StockWatchReader;
import com.ttl.old.itrade.comet.eventHandler.producer.WorldMarketIndexReader;
import com.ttl.old.itrade.interfaces.IMain;


public class EventMainProcessor {
	private static final Logger logger = LogManager.getLogger(EventMainProcessor.class);
	private final EventProcessor<HKSAccountBalanceEnquiryBean> accountEventProcessor;
	private final EventProcessor<MarketInfo> marketEventProcessor;
	private final EventProcessor<MarketIndex> marketIndexEventProcessor;
	private final EventProcessor<WorldMarketIndex> wmarketIndexEventProcessor;
	private final EventProcessor<StockWatchList> stockWatchEventProcessor;
	//private final EventProcessor<StockInfoCollector> stockCollectorEventProcessor;
	private final EventProcessor<OrderEnquiryInfo> orderEnquiryEventProcessor;
	private final EventProcessor<AlertInfo> alertEventProcessor;
	private final EventProcessor<CommonInfo> commonEventProcessor;
	private final List<AbstractBroadcaster> broadcasters;
	
	public EventMainProcessor() {
		logger.info("Constructor");		
		this.broadcasters = new ArrayList<AbstractBroadcaster>();		
		this.accountEventProcessor = new EventProcessor<HKSAccountBalanceEnquiryBean>();
		this.marketEventProcessor = new EventProcessor<MarketInfo>();
		this.marketIndexEventProcessor = new EventProcessor<MarketIndex>();
		this.wmarketIndexEventProcessor = new EventProcessor<WorldMarketIndex>();
		this.stockWatchEventProcessor = new EventProcessor<StockWatchList>();
		//this.stockCollectorEventProcessor = new EventProcessor<StockInfoCollector>();
		this.orderEnquiryEventProcessor = new EventProcessor<OrderEnquiryInfo>();
		this.alertEventProcessor = new EventProcessor<AlertInfo>();
		this.commonEventProcessor = new EventProcessor<CommonInfo>();
	}
	
	public final void init() {
		logger.info("Begin initializing event main processor");
		try {
			/*
			// Nha: disable because of bad performance for both iTrade & FO/BO severs
			//Account summary info - Initialize processors
			AccountSummaryReader aProducer = new AccountSummaryReader();
			aProducer.setIntervalInMiliseconds(getTickerInterver("accountInfoTicker", 3));
			accountEventProcessor.addProducer(aProducer);
			this.initAccountProcessor();
			// End Nha: diable because of bad performance for both iTrade & FO/BO severs
			*/
			
			/*
			// Nha: it now uses StockWatchReader to get watch list data
			//Market data info for watch list
			MarketDataReader mProducer = new MarketDataReader();
			//Response tick
			mProducer.setIntervalInMiliseconds(getTickerInterver("stockInfoTicker", 3));
			marketEventProcessor.addProducer(mProducer);
			//Initialize processors
			this.initMarketDataProcessor();
			*/
			
			//Market index info for header
			MarketIndexReader mIProducer = new MarketIndexReader();
			marketIndexEventProcessor.addProducer(mIProducer);
			mIProducer.setIntervalInMiliseconds(getTickerInterver("marketInfoTicker", 3));
			//Initialize processors
			this.initMarketIndexProcessor();
			
			//World Market index info
			WorldMarketIndexReader wMIProducer = new WorldMarketIndexReader();
			wmarketIndexEventProcessor.addProducer(wMIProducer);
			wMIProducer.setIntervalInMiliseconds(getTickerInterver("worldIndexTicker", 5));
			//Initialize processors
			this.initWorldMarketIndexProcessor();
			
			//Stock watch info
			//StockWatchReader stockWatchProducer = new StockWatchReader();
			//stockWatchEventProcessor.addProducer(stockWatchProducer);
			//stockWatchProducer.setIntervalInMiliseconds(getTickerInterver("stockWatchTicker", 3));
			//Initialize processors
			this.initStockWatchProcessor();
			
			/*
			// stock info collector
			StockInfoCollector sic = new StockInfoCollector();
			stockCollectorEventProcessor.addProducer(sic);
			sic.setIntervalInMiliseconds(getTickerInterver("stockWatchTicker", 3));
			stockCollectorEventProcessor.init();
			*/
			
			//Order Enquiry info
			//OrderEnquiryReader orderEnquiryProducer = new OrderEnquiryReader();
			//orderEnquiryEventProcessor.addProducer(orderEnquiryProducer);
			//orderEnquiryProducer.setIntervalInMiliseconds(getTickerInterver("orderEnquiryTicker", 2));
			//Initialize processors
			this.initOrderEnquiryProcessor();
			
			//Alert info
			AlertInfoReader alertInfoProducer = new AlertInfoReader();
			alertEventProcessor.addProducer(alertInfoProducer);
			alertInfoProducer.setIntervalInMiliseconds(getTickerInterver("alertInfoTicker", 2));
			//Initialize processors
			this.initAlertInfoProcessor();
			
			//Common info
			CommonInfoReader commonInfoProducer = new CommonInfoReader();
			commonEventProcessor.addProducer(commonInfoProducer);
			commonInfoProducer.setIntervalInMiliseconds(getTickerInterver("commonInfoTicker", 5));
			//Initialize processors
			this.initCommonInfoProcessor();
			
		} catch (Exception ex) {
			logger.error("Initialize event main processor error", ex);
		}
		logger.info("End initializing event main processor");		
	}
	
	private int getTickerInterver(String tickerKey, int defValue)
	{
		int result = defValue;
		try
		{
			result = Integer.parseInt(IMain.getProperty(tickerKey));
		}
		catch(Exception ex)
		{
			logger.error(String.format("Error in get tickerKey value: %s, use default value: %d", tickerKey, defValue));
		}
		
		return result;
	}
	
	
	private void initOrderEnquiryProcessor() {		
		logger.info("Init order enquiry processor");
		final NotificationAction<OrderEnquiryInfo> orderEnquiryPushing = new NotificationAction<OrderEnquiryInfo>() {
            public void sendData(final List<OrderEnquiryInfo> orders) {
                for (EventBroadcaster eb : broadcasters)
                    eb.broadcastOrderEnquiryInfo(orders);
            }
            public void destroy() {
				//Nothing
			}
        };
        orderEnquiryEventProcessor.addConsumer(new EventConsumer<OrderEnquiryInfo>(orderEnquiryPushing));
        orderEnquiryEventProcessor.init();
	}
	
	private void initAlertInfoProcessor() {		
		logger.info("Init alert info processor");
		final NotificationAction<AlertInfo> alertEnquiryPushing = new NotificationAction<AlertInfo>() {
            public void sendData(final List<AlertInfo> alerts) {
                for (EventBroadcaster eb : broadcasters)
                    eb.broadcastAlertInfo(alerts);
            }
            public void destroy() {
				//Nothing
			}
        };
        alertEventProcessor.addConsumer(new EventConsumer<AlertInfo>(alertEnquiryPushing));
        alertEventProcessor.init();
	}

	private void initCommonInfoProcessor() {		
		logger.info("Init common info processor");
		final NotificationAction<CommonInfo> commonPushing = new NotificationAction<CommonInfo>() {
            public void sendData(final List<CommonInfo> commons) {
                for (EventBroadcaster eb : broadcasters)
                    eb.broadcastCommonInfo(commons.get(0));
            }
            public void destroy() {
				//Nothing
			}
        };
        commonEventProcessor.addConsumer(new EventConsumer<CommonInfo>(commonPushing));
        commonEventProcessor.init();
	}
	
	public final void addBroadcaster(final AbstractBroadcaster broadcaster) {
		broadcasters.add(broadcaster);
	}
	
	/**
	 * Initialize account summary processor.
	 */
	private void initAccountProcessor() {
		logger.info("Init account processor");
		final NotificationAction<HKSAccountBalanceEnquiryBean> accSummaryPushing = new NotificationAction<HKSAccountBalanceEnquiryBean>() {
            public void sendData(final List<HKSAccountBalanceEnquiryBean> accounts) {
                for (EventBroadcaster eb : broadcasters)
                    eb.broadcastAccountSummary(accounts);
            }
            public void destroy() {
				//Nothing
			}
        };
        accountEventProcessor.addConsumer(new EventConsumer<HKSAccountBalanceEnquiryBean>(accSummaryPushing));
		accountEventProcessor.init();
	}
	
	//Init market data processor
	private void initMarketDataProcessor() {
		logger.info("Init market data processor");
		final NotificationAction<MarketInfo> accSummaryPushing = new NotificationAction<MarketInfo>() {
            public void destroy() {
				//Nothing
			}
			public void sendData(List<MarketInfo> data) {
				for (EventBroadcaster eb : broadcasters)
                    eb.broadcastMarket(data.get(0));
			}
        };
        marketEventProcessor.addConsumer(new EventConsumer<MarketInfo>(accSummaryPushing));
        marketEventProcessor.init();		
	}
	
	//Init market index processor
	private void initMarketIndexProcessor() {
		logger.info("Init market index processor");
		final NotificationAction<MarketIndex> marketIndexPushing = new NotificationAction<MarketIndex>() {
            public void destroy() {
				//Nothing
			}
			public void sendData(List<MarketIndex> data) {
				for (EventBroadcaster eb : broadcasters)
				{
					eb.broadcastMarketIndex(data.get(0));
				}
			}
        };
        
        marketIndexEventProcessor.addConsumer(new EventConsumer<MarketIndex>(marketIndexPushing));
        marketIndexEventProcessor.init();
	}
	
	//Init world market index processor
	private void initWorldMarketIndexProcessor() {
		logger.info("Init world market index processor");
		final NotificationAction<WorldMarketIndex> wmarketIndexPushing = new NotificationAction<WorldMarketIndex>() {
            public void destroy() {
				//Nothing
			}
			public void sendData(List<WorldMarketIndex> data) {
				for (EventBroadcaster eb : broadcasters)
				{
					eb.broadcastWorldMarketIndex(data.get(0));
				}
			}
        };
        
        wmarketIndexEventProcessor.addConsumer(new EventConsumer<WorldMarketIndex>(wmarketIndexPushing));
        wmarketIndexEventProcessor.init();
	}
	
	private void initStockWatchProcessor() {
		logger.info("Init world market index processor");
		final NotificationAction<StockWatchList> stockWatchPushing = new NotificationAction<StockWatchList>() {
            public void destroy() {
				//Nothing
			}
			public void sendData(List<StockWatchList> data) {
				for (EventBroadcaster eb : broadcasters)
				{
					eb.broadcastStockWatchInfo(data.get(0));
				}
			}
        };
        
        stockWatchEventProcessor.addConsumer(new EventConsumer<StockWatchList>(stockWatchPushing));
        stockWatchEventProcessor.init();
		
	}
	
	public final void destroy() {
		logger.info("Begin destroy");
		accountEventProcessor.destroy();
		marketEventProcessor.destroy();
		marketIndexEventProcessor.destroy();
		wmarketIndexEventProcessor.destroy();
		stockWatchEventProcessor.destroy();
		orderEnquiryEventProcessor.destroy();
		for (AbstractBroadcaster broadcaster : broadcasters)
			broadcaster.stop();
		logger.info("End destroy");
	}
}
