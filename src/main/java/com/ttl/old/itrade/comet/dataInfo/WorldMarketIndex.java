package com.ttl.old.itrade.comet.dataInfo;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSONArray;

import com.ttl.old.itrade.hks.bean.HKSWorkMarketIndexBean;


public class WorldMarketIndex {
	private Map<String, HKSWorkMarketIndexBean> wMarketIndex = new HashMap<String, HKSWorkMarketIndexBean>();
	//private static final WTradeLogger logger = LogManager.getLogger(WorldMarketIndex.class);
	//Get world market index by name
	public HKSWorkMarketIndexBean get(String wName)
	{
		return wMarketIndex.get(wName);
	}
	
	//put world market index by name
	public void put(String wName, HKSWorkMarketIndexBean bean)
	{
		wMarketIndex.put(wName, bean);
	}
	
	public Set<String> keySet()
	{
		return wMarketIndex.keySet();
	}
	
	public WorldMarketIndex clone()
	{
		synchronized(wMarketIndex)
		{
			WorldMarketIndex clone = new WorldMarketIndex();
			for(String key : wMarketIndex.keySet())
			{
				clone.put(key, wMarketIndex.get(key));
			}
			return clone;
		}
		
	}
	public JSONArray toJsonArray(boolean all)
	{
		synchronized (wMarketIndex) {
			JSONArray array = new JSONArray();
			HKSWorkMarketIndexBean bean = null;
			for(String key : wMarketIndex.keySet())
			{
				bean = wMarketIndex.get(key);
				//System.out.println("before if: " + bean.isValueChanged());
				if(all == true)
				{
					array.add(bean);
				}
				else
				{
					if(bean.isValueChanged() == true)
					{
						array.add(bean);
						//System.out.println("after if: " + bean.isValueChanged());
					}
				}
			}
			
			if(array.size() > 0) 
				return array;
			return null;
		}
		
	}
} 
