package com.ttl.old.itrade.comet.eventHandler.caching;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.ttl.old.itrade.comet.dataInfo.Alert;


public class AlertDataCachingManager {
		
	private static final Map<String, List<Alert>> alertDataList = new ConcurrentHashMap<String, List<Alert>>();
	
	public static void insertAlertInfo(String clientId, Alert orderEnquiry)
	{
		List<Alert> alertList = alertDataList.get(clientId);
		if(alertList == null)
		{
			alertList = new ArrayList<Alert>();
		}		
		alertList.add(orderEnquiry);
		alertDataList.put(clientId, alertList);
	}
	
	public static void insertAlertInfo(String clientId, List<Alert> alerts)
	{
		List<Alert> alertList = alertDataList.get(clientId);
		if(alertList == null)
		{
			alertList = new ArrayList<Alert>();
		}		
		alertList.addAll(alerts);
		alertDataList.put(clientId, alertList);
	}
	
	public static List<Alert> getAlertList(String clientId)
	{
		return alertDataList.get(clientId);
	}
}
