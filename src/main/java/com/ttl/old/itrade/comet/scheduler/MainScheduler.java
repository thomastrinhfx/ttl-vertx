/* 
 * Copyright 2010 Sacombank Securities Company. All rights reserved.
 * Program  : STrade 
 * Revision : 1.0  
 * 
 * History Change
 * -----------------------------------
 * Date         Author          Reason
 * 20/08/2010   bac.nh          Use Comet Technology
 * 
 */
package com.ttl.old.itrade.comet.scheduler;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class MainScheduler {
	private static final Logger logger = LogManager.getLogger(MainScheduler.class);
	private static final ScheduledThreadPoolExecutor scheduler = new ScheduledThreadPoolExecutor(1);
	private static int numTasks = 0;
	
	public static synchronized void schedule(final Runnable task, final int intervalInMiliSeconds) {		
		if (numTasks != 0) {
			scheduler.setCorePoolSize(scheduler.getCorePoolSize() + 1);
		}
		scheduler.scheduleAtFixedRate(task, 0, intervalInMiliSeconds, TimeUnit.MILLISECONDS);
		numTasks++;
		logger.info(String.format("Schedule task %s (number of threads in the scheduler is %d)", 
				task, scheduler.getCorePoolSize()));
	}
	
	public static void destroy() {
		logger.info("Begin destroy");		
		try {
			if (scheduler.isShutdown() || scheduler.isTerminated()) {
				logger.info("Scheduler already destroyed");
				return;
			}
			if (scheduler.isTerminating()) {
				logger.info("Scheduler is destroying");
				return;
			}
			scheduler.shutdown();
			if (scheduler.awaitTermination(60L, TimeUnit.SECONDS)) {
				logger.info("Shutdown the scheduler success");
			} else {
				logger.warn("Shutdown the scheduler timeout(60s)");
			}
		} catch (Exception e) {
			logger.error("", e);
		} finally {
			logger.info("End destroy");			
		}		
	}
}
