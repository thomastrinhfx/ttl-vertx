package com.ttl.old.itrade.comet.eventHandler.producer;

import java.util.ArrayList;
import java.util.List;

import com.ttl.old.itrade.comet.dataInfo.AlertInfo;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.ttl.old.itrade.comet.cometManagement.BrowserManager;
import com.ttl.old.itrade.comet.dataInfo.Alert;
import com.ttl.old.itrade.comet.eventHandler.caching.AlertDataCachingManager;
import com.ttl.old.itrade.comet.eventHandler.generic.EventProducer;
import com.ttl.old.itrade.comet.eventHandler.generic.EventQueue;

public class AlertInfoReader extends EventProducer<AlertInfo>
{
	private static final Logger logger = LogManager.getLogger(AlertInfoReader.class);
	
	@Override
	public void product(EventQueue<AlertInfo> eventQueue) {
		
		List<String> accountList = BrowserManager.getAllOnlineAccounts();			
		for(String pClientID : accountList)
		{
			try
			{
				List<Alert> alertList = AlertDataCachingManager.getAlertList(pClientID);
				if(alertList != null && alertList.size() > 0)
				{
					synchronized (alertList) {
						AlertInfo info = new AlertInfo(pClientID, new ArrayList<Alert>(alertList));
						eventQueue.addDataEvent(info);						
						alertList.clear();
					}
				}
			}
			catch(Exception ex)
			{
				logger.error("Error in getting alert info with account no " + pClientID);
				logger.info(ex);
			}
		}		
	}
}
