package com.ttl.old.itrade.comet.dataInfo;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.ttl.old.itrade.hks.bean.HKSMarketIndexBean;
import net.sf.json.JSONArray;


public class MarketIndex {
	private Map<String, HKSMarketIndexBean> indexList = new HashMap<String, HKSMarketIndexBean>();
	private boolean updated = false;
	
	public HKSMarketIndexBean get(String marketID)
	{
		return indexList.get(marketID);
	}
	
	public Set<String> keySet()
	{
		return indexList.keySet();
	}
	
	public void put(String key, HKSMarketIndexBean index)
	{
		indexList.put(key, index);
	}
	
	public boolean isUpdated() {
		return updated;
	}
	public void setUpdated(boolean updated) {
		this.updated = updated;
	}
	
	public JSONArray toJsonArray(boolean all)
	{
		HKSMarketIndexBean bean = null;
		JSONArray array = new JSONArray();
		for(String key: indexList.keySet())
		{
			bean = indexList.get(key);
			if(all == true)
			{
				array.add(bean);
			}
			else
			{
				//Only add updated bean			
				if(bean.isUpdated() == true)
				{
					array.add(bean);
				}
			}
		}
		
		if(array.size() > 0)
			return array;
		
		return null;
	}
} 
