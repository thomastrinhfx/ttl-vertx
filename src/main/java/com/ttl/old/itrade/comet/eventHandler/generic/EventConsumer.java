package com.ttl.old.itrade.comet.eventHandler.generic;

import java.util.List;

public class EventConsumer<T> {
	/**
	 * Notification action.
	 */
	private NotificationAction<T> notificationAction;
	/**
	 * Constructor.
	 * 
	 * @param notificationAction NotificationAction<T>
	 */
	public EventConsumer(final NotificationAction<T> notificationAction) {
		this.notificationAction = notificationAction;		
	}
	/**
	 * @return the notificationAction
	 */
	public NotificationAction<T> getNotificationAction() {
		return notificationAction;
	}
	/**
	 * @param notificationAction the notificationAction to set
	 */
	public void setNotificationAction(final NotificationAction<T> notificationAction) {
		this.notificationAction = notificationAction;
	}
	/**
	 * Consume data.
	 * 
	 * @param oldData Old data
	 * @param newData New data
	 */
	public void consume(final List<T> oldData, final List<T> newData) {
		notificationAction.sendData(newData);
	}
	/**
	 * Destroy consumer.
	 */
	public void destroy() {
		notificationAction.destroy();
	}
}
