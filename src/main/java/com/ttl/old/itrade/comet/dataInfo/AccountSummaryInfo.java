package com.ttl.old.itrade.comet.dataInfo;




public class AccountSummaryInfo {

	//private final int dataType;
	private String account;
	private String accountType;
	private long cashAvailable = 0;
	private long securitiesValue = 0;
	private long purchasingPower = 0;
	private long usingPower = 0;
	private long lastUpdated = System.currentTimeMillis();
	
	/**
	 * @return the account
	 */
	public String getAccount() {
		return account;
	}

	/**
	 * @param account the account to set
	 */
	public void setAccount(String account) {
		this.account = account;
	}
	
	
    public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	/**
	 * @return the cashAvailable
	 */
	public long getCashAvailable() {
		return cashAvailable;
	}

	/**
	 * @param cashAvailable the cashAvailable to set
	 */
	public void setCashAvailable(long cashAvailable) {
		this.cashAvailable = cashAvailable;
	}
	
    /**
	 * @return the securitiesValue
	 */
	public long getSecuritiesValue() {
		return securitiesValue;
	}

	/**
	 * @param securitiesValue the securitiesValue to set
	 */
	public void setSecuritiesValue(long securitiesValue) {
		this.securitiesValue = securitiesValue;
	}

	/**
     * @return the purchasingPower
     */
    public long getPurchasingPower() {
        return purchasingPower;
    }

    /**
     * @param purchasingPower the purchasingPower to set
     */
    public void setPurchasingPower(long purchasingPower) {
        this.purchasingPower = purchasingPower;
    }
    
    /**
	 * @return the usingPower
	 */
	public long getUsingPower() {
		return usingPower;
	}
	/**
	 * @param usingPower the usingPower to set
	 */
	public void setUsingPower(long usingPower) {
		this.usingPower = usingPower;
	}
	/**
	 * @return the lastUpdated
	 */
	public long getLastUpdated() {
		return lastUpdated;
	}

	/**
	 * @param lastUpdated the lastUpdated to set
	 */
	public void setLastUpdated(long lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public boolean updateIfChanged(final AccountSummaryInfo accSum) {
    	boolean isChanged = false;
    	if (this.cashAvailable != accSum.getCashAvailable()) {
    		this.cashAvailable = accSum.getCashAvailable();
    		isChanged = true;
    	}
    	if (this.securitiesValue != accSum.getSecuritiesValue()) {
    		this.securitiesValue = accSum.getSecuritiesValue();
    		isChanged = true;
    	}
    	if (this.purchasingPower != accSum.getPurchasingPower()) {
    		this.purchasingPower = accSum.getPurchasingPower();
    		isChanged = true;
    	}
    	if (this.usingPower != accSum.getUsingPower()) {
    		this.usingPower = accSum.getUsingPower();
    		isChanged = true;
    	}
    	if (isChanged)
    		lastUpdated = System.currentTimeMillis();
    	
    	return isChanged;
    }
}
