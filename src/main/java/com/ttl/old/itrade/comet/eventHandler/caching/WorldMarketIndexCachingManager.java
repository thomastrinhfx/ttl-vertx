package com.ttl.old.itrade.comet.eventHandler.caching;

import java.util.List;
import com.ttl.old.itrade.comet.dataInfo.WorldMarketIndex;
import com.ttl.old.itrade.hks.bean.HKSWorkMarketIndexBean;


public class WorldMarketIndexCachingManager {
	private static WorldMarketIndex lastWorldMarketIndex = new WorldMarketIndex();
	
	public static boolean updateMarketDataIfChanged(List<HKSWorkMarketIndexBean> wMarketIndex) {
		synchronized (lastWorldMarketIndex) {
			boolean listChanged = false;
			boolean itemChanged = false;
			HKSWorkMarketIndexBean oMIndex = null;
			for(HKSWorkMarketIndexBean nMIndex: wMarketIndex)
			{
				itemChanged = false;
				oMIndex = lastWorldMarketIndex.get(nMIndex.getIndexName());
				
				if(oMIndex != null)
				{
					if(WorldMarketIndexCachingManager.isChanged(oMIndex, nMIndex))
					{
						itemChanged = true;
					}
				}
				else
				{
					itemChanged = true;
				}
				
				nMIndex.setValueChanged(itemChanged);
				if(itemChanged == true)
				{
					lastWorldMarketIndex.put(nMIndex.getIndexName(), nMIndex);
					listChanged = true;
				}
				else
					oMIndex.setValueChanged(false);
				
			}
			return listChanged;
		}
	}

	private static boolean isChanged(HKSWorkMarketIndexBean oMarketIndex, HKSWorkMarketIndexBean nMarketIndex)
	{
		boolean isChanged = false;
		if(!nMarketIndex.getIndexName().equals(oMarketIndex.getIndexName())
			|| !nMarketIndex.getIndexChange().equals(oMarketIndex.getIndexChange())
			|| !nMarketIndex.getIndexValue().equals(oMarketIndex.getIndexValue())
			|| !nMarketIndex.getPercentChange().equals(oMarketIndex.getPercentChange()))
		{
			isChanged = true;
		}
		return isChanged;
		//return true;
	}

	public static WorldMarketIndex getLastWorldMarketIndex() {
		return lastWorldMarketIndex.clone();
	}

	public static void setLastWorldMarketIndex(WorldMarketIndex lastWorldMarketIndex) {
		WorldMarketIndexCachingManager.lastWorldMarketIndex = lastWorldMarketIndex;
	}
}
