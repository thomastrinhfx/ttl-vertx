package com.ttl.old.itrade.comet.eventHandler.caching;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class OrderEnquiryDataCachingManager {
	//private static final WTradeLogger logger = LogManager.getLogger(OrderEnquiryDataCachingManager.class);
		
	private static final Map<String, List<String>> orderEnquiryListString = new ConcurrentHashMap<String, List<String>>();
	public static void insertOrderEnquiryString(String clientId, String orderEnquiry)
	{
		List<String> orderList = orderEnquiryListString.get(clientId);
		if(orderList == null)
		{
			orderList = new ArrayList<String>();
		}		
		orderList.add(orderEnquiry);
		orderEnquiryListString.put(clientId, orderList);
	}
	
	public static List<String> getOrderEnquiryListString(String clientId)
	{
		return orderEnquiryListString.get(clientId);
	}
}
