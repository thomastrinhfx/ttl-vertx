package com.ttl.old.itrade.comet.dataInfo;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.ttl.old.itrade.comet.utils.Constants;

import net.sf.json.JSONObject;

public class OrderAlert extends Alert {
	private String stockId;
	private String orderGroupId;	
	private String orderId;
	private String orderStatus;	
	
	public OrderAlert()
	{
		alertType = Constants.ALERT_TYPE_ORDER;
		time = new Date();
	}
	
	public OrderAlert(String stockId, String orderGroupId, String orderId, String orderStatus)
	{
		alertType = Constants.ALERT_TYPE_ORDER;
		time = new Date();
		this.stockId = stockId;
		this.orderGroupId = orderGroupId;
		this.orderId = orderId;
		this.orderStatus = orderStatus;
	}
	
	public String getStockId() {
		return stockId;
	}

	public void setStockId(String stockId) {
		this.stockId = stockId;
	}

	public String getOrderGroupId() {
		return orderGroupId;
	}

	public void setOrderGroupId(String orderGroupId) {
		this.orderGroupId = orderGroupId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	@Override
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		obj.put(Constants.ALERT_TYPE, alertType);
		obj.put(Constants.ALERT_TIME, sdf.format(time));
		obj.put(Constants.ALERT_STOCK_ID, stockId);
		obj.put(Constants.ALERT_ORDER_ID, orderId);
		obj.put(Constants.ALERT_ORDER_GROUP_ID, orderGroupId);
		obj.put(Constants.ALERT_ORDER_STATUS, orderStatus);
		return obj;
	}
}
