package com.ttl.old.itrade.comet.eventHandler.generic;

import java.util.List;

public class DataChangedEvent<T> {
	private Object source;
	private List<T> oldValue;
	private List<T> newValue;
	public DataChangedEvent(Object source, List<T> oldValue, List<T> newValue) {
		this.source = source;
		this.oldValue = oldValue;
		this.newValue = newValue;
	}
	
	public DataChangedEvent(List<T> oldValue, List<T> newValue) {
		this(null, oldValue, newValue);
	}
	
	public DataChangedEvent(List<T> newValue) {
		this(null, null, newValue);
	}
	/**
	 * @return the source
	 */
	public Object getSource() {
		return source;
	}
	/**
	 * @param source the source to set
	 */
	public void setSource(Object source) {
		this.source = source;
	}

	/**
	 * @return the oldValue
	 */
	public List<T> getOldValue() {
		return oldValue;
	}

	/**
	 * @param oldValue the oldValue to set
	 */
	public void setOldValue(List<T> oldValue) {
		this.oldValue = oldValue;
	}

	/**
	 * @return the newValue
	 */
	public List<T> getNewValue() {
		return newValue;
	}

	/**
	 * @param newValue the newValue to set
	 */
	public void setNewValue(List<T> newValue) {
		this.newValue = newValue;
	}	
}
