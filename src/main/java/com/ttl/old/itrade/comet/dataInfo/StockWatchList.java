package com.ttl.old.itrade.comet.dataInfo;

import java.util.ArrayList;
import java.util.List;

public class StockWatchList {

	private List<StockWatchInfo> stocks;

	public List<StockWatchInfo> getStocks() {
		return stocks;
	}

	public void setStocks(List<StockWatchInfo> stocks) {
		this.stocks = stocks;
	}
	
	public StockWatchList clone()
	{
		StockWatchList list = new StockWatchList();
		list.setStocks(new ArrayList<StockWatchInfo>(this.getStocks()));
		return list;
		
	}
	
}
