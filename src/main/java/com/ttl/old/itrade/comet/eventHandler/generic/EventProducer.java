package com.ttl.old.itrade.comet.eventHandler.generic;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;




public abstract class EventProducer<T> {
	//3s to product data in event queue
	private int intervalInMiliseconds = 3000;
	private int startWorkingTime = -1;
	private int endWorkingTime = -1;
	private int startBreakTime = -1;
	private int endBreakTime = -1;
	//private boolean hasReset = false;
	private boolean enable = false;
	
	protected boolean isRunning = false;
	/**
	 * Product events and add to the eventQueue.
	 */
	public abstract void product(EventQueue<T> eventQueue);
	/**
	 * @return the intervalInMiliseconds
	 */
	public int getIntervalInMiliseconds() {
		return intervalInMiliseconds;
	}

	/**
	 * @param intervalInMiliseconds the intervalInMiliseconds to set
	 */
	public void setIntervalInMiliseconds(int intervalInMiliseconds) {
		this.intervalInMiliseconds = intervalInMiliseconds;
	}
	/**
	 * Set time range for processor running.
	 * 
	 * @param workingTimeRange The time range in format 'HH:mm HH:mm'
	 * @throws ParseException If the time range is incorrect
	 */
	public final void setWorkingTimeRange(final String workingTimeRange) throws ParseException {
		if (workingTimeRange == null || workingTimeRange.trim().length() == 0)
			throw new IllegalArgumentException("The time range must be not null");
		final String[] array = workingTimeRange.split(" ");
		if (array == null || array.length != 2)
			throw new IllegalArgumentException("The time range must be in format 'HH:mm HH:mm'");
		final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		final Calendar cal = Calendar.getInstance();
		cal.setTime(sdf.parse(array[0]));
		startWorkingTime = 100 * cal.get(Calendar.HOUR_OF_DAY) + cal.get(Calendar.MINUTE);
		cal.setTime(sdf.parse(array[1]));
		endWorkingTime = 100 * cal.get(Calendar.HOUR_OF_DAY) + cal.get(Calendar.MINUTE);
		if (startWorkingTime > endWorkingTime)
			throw new IllegalArgumentException("The end time must be not less than start time");
	}
	/**
	 * @param enable the enable to set
	 */
	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	/**
	 * @return the enable
	 */
	public boolean getEnable() {
		return enable;
	}
	
	/**
	 * @return the startTime
	 */
	public int getStartTime() {
		return startWorkingTime;
	}
	/**
	 * @return the endTime
	 */
	public int getEndTime() {
		return endWorkingTime;
	}
	/**
	 * Set time range for breaking.
	 * 
	 * @param breakingTimeRange The time rang in format 'HH:mm HH:mm'
	 * @throws ParseException If the time range is incorrect
	 */
	public final void setBreakingTimeRange(final String breakingTimeRange) throws ParseException {
		if (breakingTimeRange == null || breakingTimeRange.trim().length() == 0)
			throw new IllegalArgumentException("The time range must be not null");
		final String[] array = breakingTimeRange.split(" ");
		if (array == null || array.length != 2)
			throw new IllegalArgumentException("The time range must be in format 'HH:mm HH:mm'");
		final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		final Calendar cal = Calendar.getInstance();
		cal.setTime(sdf.parse(array[0]));
		startBreakTime = 100 * cal.get(Calendar.HOUR_OF_DAY) + cal.get(Calendar.MINUTE);
		cal.setTime(sdf.parse(array[1]));
		endBreakTime = 100 * cal.get(Calendar.HOUR_OF_DAY) + cal.get(Calendar.MINUTE);
		if (startBreakTime > endBreakTime)
			throw new IllegalArgumentException("The end time must be not less than start time");
	}	
	/**
	 * @return the sourceJdbcTemplate
	 */
//	public final JdbcTemplate getSourceJdbcTemplate() {
//		return sourceJdbcTemplate;
//	}
//	/**
//	 * @param sourceJdbcTemplate the sourceJdbcTemplate to set
//	 */
//	public final void setSourceJdbcTemplate(final JdbcTemplate sourceJdbcTemplate) {
//		this.sourceJdbcTemplate = sourceJdbcTemplate;
//	}
//	/**
//	 * @return the targetJdbcTemplate
//	 */
//	public final JdbcTemplate getTargetJdbcTemplate() {
//		return targetJdbcTemplate;
//	}
//	/**
//	 * @param targetJdbcTemplate the targetJdbcTemplate to set
//	 */
//	public final void setTargetJdbcTemplate(final JdbcTemplate targetJdbcTemplate) {
//		this.targetJdbcTemplate = targetJdbcTemplate;
//	}
	/**
	 * Check to run the producer.
	 * 
	 * @return True if can run, otherwise false
	 */
	protected boolean canRun() {		
		return !isRunning;
	}
	/**
	 * Destroy.
	 */
	protected void destroy() {		
	}
	
	@Override
	public String toString() {
		return String.format("[name=%s, interval=%s(ms), startWorkingTime=%s, endWorkingTime=%s]", 
				this.getClass().getName(), intervalInMiliseconds, startWorkingTime, endWorkingTime);
	}
}
