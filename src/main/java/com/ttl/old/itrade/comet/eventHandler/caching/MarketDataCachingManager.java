package com.ttl.old.itrade.comet.eventHandler.caching;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.ttl.old.itrade.hks.bean.HKSMarketDataBean;
import org.apache.commons.validator.GenericValidator;


public class MarketDataCachingManager {

	//For Project market data
	private static Map<String, HKSMarketDataBean> stockInfoCaching = new ConcurrentHashMap<String, HKSMarketDataBean>();
	
	//For DS
	private static List<String> registerSymbols = new ArrayList<String>();
	private static List<String> warningSymbols = new ArrayList<String>();
	private static List<String> stockWatchSymbols = new ArrayList<String>();
	
	public static void addRegisterSymbol(List<String> symbols)
	{
		for(String symbol: symbols)
		{
			if(!registerSymbols.contains(symbol))
				registerSymbols.add(symbol);
		}
	}
	
	public static void addWarningSymbol(List<String> symbols)
	{
		for(String symbol: symbols)
		{
			if(!warningSymbols.contains(symbol))
				warningSymbols.add(symbol);
		}
	}
	
	public static void addStockWatchSymbol(String symbol)
	{
		if(!stockWatchSymbols.contains(symbol))
			stockWatchSymbols.add(symbol);
	}
	
	public static List<String> getRegisterSymbols() {
		return registerSymbols;
	}

	public static void setRegisterSymbols(List<String> registerSymbols) {
		MarketDataCachingManager.registerSymbols = registerSymbols;
	}

	public static List<String> getWarningSymbols() {
		return warningSymbols;
	}

	public static void setWarningSymbols(List<String> warningSymbols) {
		MarketDataCachingManager.warningSymbols = warningSymbols;
	}

	public static List<String> getStockWatchSymbols() {
		return stockWatchSymbols;
	}

	public static void setStockWatchSymbols(List<String> stockWatchSymbols) {
		MarketDataCachingManager.stockWatchSymbols = stockWatchSymbols;
	}

	public static Set<String> getDynamicSymbols()
	{
		Set<String> symbols = new HashSet<String>();
		symbols.addAll(getRegisterSymbols());
		symbols.addAll(getWarningSymbols());
		symbols.addAll(getStockWatchSymbols());
		return symbols;
	}	
	
	public static void put(String stockKey, HKSMarketDataBean stockValue)
	{
		if(stockValue != null)
		{
			stockInfoCaching.put(stockKey, stockValue);
		}
	}
	
	public static HKSMarketDataBean get(String stockKey)
	{
		HKSMarketDataBean stockValue = null;
		if(!GenericValidator.isBlankOrNull(stockKey))
		{
			stockValue = stockInfoCaching.get(stockKey);
		}
		
		return stockValue;
	}

}
