package com.ttl.old.itrade.comet.eventHandler.producer;

import java.math.BigDecimal;
import java.util.List;

import com.ttl.old.itrade.comet.cometManagement.BrowserManager;
import com.ttl.old.itrade.comet.eventHandler.caching.AccountSummaryDataCachingManager;
import com.ttl.old.itrade.comet.eventHandler.generic.EventProducer;
import com.ttl.old.itrade.comet.eventHandler.generic.EventQueue;
import com.ttl.old.itrade.comet.utils.Constants;
import com.ttl.old.itrade.hks.bean.HKSAccountBalanceEnquiryBean;
import com.ttl.old.itrade.hks.txn.HKSAccountBalanceEnquiryTxn;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

//import com.itrade.hks.util.DisplayCurrency;


public class AccountSummaryReader extends EventProducer<HKSAccountBalanceEnquiryBean>
{
	private static final Logger logger = LogManager.getLogger(AccountSummaryReader.class);
	@Override
	public void product(EventQueue<HKSAccountBalanceEnquiryBean> eventQueue) {
		
		List<String> accountList = BrowserManager.getAllOnlineAccounts(Constants.CASH_BALANCE_TYPE);
		for(String pClientID : accountList)
		{
			try
			{
				HKSAccountBalanceEnquiryBean oInfo = AccountSummaryDataCachingManager.getAccountSummaryInfo(pClientID);
				if(oInfo != null)
				{				
					HKSAccountBalanceEnquiryBean nInfo = this.getCashBalance(oInfo.getMvClientId(), oInfo.getMvAccountType());
					if(AccountSummaryDataCachingManager.updateAccountSumIfChanged(nInfo))
					{
						eventQueue.addDataEvent(nInfo);
					}
				}
				else
				{
					AccountSummaryDataCachingManager.unregisterAccount(pClientID);
				}
			}
			catch(Exception ex)
			{
				logger.error("Error in getting account summary info with account no " + pClientID);
				logger.info(ex);
			}					
		}		
	}

	private HKSAccountBalanceEnquiryBean getCashBalance(String pClientID, String lvAccountType) {
		HKSAccountBalanceEnquiryBean mvAccountBalanceBean = new HKSAccountBalanceEnquiryBean();
		HKSAccountBalanceEnquiryTxn lvHKSAccountBalanceEnquiryTxn = new HKSAccountBalanceEnquiryTxn(pClientID,lvAccountType);
		
		lvHKSAccountBalanceEnquiryTxn.setMvIsQueryAdvanceMoney(true);
		lvHKSAccountBalanceEnquiryTxn.process();
		int lvReturnCode = lvHKSAccountBalanceEnquiryTxn.getReturnCode();
		Log.println("[ HKSAccountBalanceEnquiryAction.doAction() return code for user " + pClientID + " = " + lvReturnCode + "]", Log.ACCESS_LOG);
		if (lvReturnCode != TPErrorHandling.TP_NORMAL)
		{
		    if (lvReturnCode == TPErrorHandling.TP_APP_ERR)
		    {
		        Log.println("[ HKSAccountBalanceEnquiryAction.doAction(): AccountBalance Enquiry for " + pClientID + " fail with application error:" + lvHKSAccountBalanceEnquiryTxn.getErrorMessage() + " ]", Log.ERROR_LOG);
		    }
		    else if (lvReturnCode == TPErrorHandling.TP_SYS_ERR)
		    {
		        Log.println("[ HKSAccountBalanceEnquiryAction.doAction(): AccountBalance Enquiry for " + pClientID + " fail with system error:" + lvHKSAccountBalanceEnquiryTxn.getErrorMessage() + " ]", Log.ERROR_LOG);

		    }
		}else{
			 	// BEGIN - TASK#: KZ00001 - Kevin Zhan 20090416
			 	double lvCTodayBuy = lvHKSAccountBalanceEnquiryTxn.getCTodayBuy() == null? 0: Double.parseDouble(lvHKSAccountBalanceEnquiryTxn.getCTodayBuy());
			 	//double lvCTodaySell = lvHKSAccountBalanceEnquiryTxn.getCTodaySell() == null ? 0 : Double.parseDouble(lvHKSAccountBalanceEnquiryTxn.getCTodaySell());
			 	double lvCDueBuy = lvHKSAccountBalanceEnquiryTxn.getCDueBuy() == null? 0 : Double.parseDouble(lvHKSAccountBalanceEnquiryTxn.getCDueBuy());
			 	double lvHoldAmount = lvHKSAccountBalanceEnquiryTxn.getHoldAmount() == null? 0: Double.parseDouble(lvHKSAccountBalanceEnquiryTxn.getHoldAmount());
			 	//double lvReserveAmount = lvHKSAccountBalanceEnquiryTxn.getReserveAmount() == null? 0: Double.parseDouble(lvHKSAccountBalanceEnquiryTxn.getReserveAmount());
			 	
			 	double lvCTodayConfirmSell = lvHKSAccountBalanceEnquiryTxn.getCTodayConfirmSell() == null ? 0 : Double.parseDouble(lvHKSAccountBalanceEnquiryTxn.getCTodayConfirmSell());
			 	double lvCPendingSell = lvHKSAccountBalanceEnquiryTxn.getCUnsettleSell() == null? 0 : Double.parseDouble(lvHKSAccountBalanceEnquiryTxn.getCUnsettleSell());
			 	double lvCDueSell = lvHKSAccountBalanceEnquiryTxn.getCDueSell() == null? 0 : Double.parseDouble(lvHKSAccountBalanceEnquiryTxn.getCDueSell());
			 	
			 	double lvCTodayConfirmBuy = lvHKSAccountBalanceEnquiryTxn.getCTodayConfirmBuy() == null ? 0 : Double.parseDouble(lvHKSAccountBalanceEnquiryTxn.getCTodayConfirmBuy());
		        double lvCTodayConfirmBS = lvCTodayConfirmSell - lvCTodayConfirmBuy;
		        
		        double lvBuyingPower = lvHKSAccountBalanceEnquiryTxn.getBuyingPowerd() == null? 0 : Double.parseDouble(lvHKSAccountBalanceEnquiryTxn.getBuyingPowerd());
		        double lvMarketValue = lvHKSAccountBalanceEnquiryTxn.getMarketValue() == null? 0 : Double.parseDouble(lvHKSAccountBalanceEnquiryTxn.getMarketValue());
		        
		        //START HIEU LE - calculate REMAIN RESERVE
//		        double lvManualReserve = lvCashBalaceTxn.getMvManualReserve()== null? 0 : Double.parseDouble(lvCashBalaceTxn.getMvManualReserve());
//			 	double lvTodayOut = lvCashBalaceTxn.getMvTodayOut()== null? 0 : Double.parseDouble(lvCashBalaceTxn.getMvTodayOut());
//			 	double lvPendingWithdrawal = lvCashBalaceTxn.getMvPendingWithdrawal()== null? 0 : Double.parseDouble(lvCashBalaceTxn.getMvPendingWithdrawal());
			 	//END BY HIEU LE 
			 	
			 	BigDecimal lvTodayBuyDecimal = new BigDecimal(lvCTodayBuy);
			 	//BigDecimal lvPendingBuyDecimal = new BigDecimal(lvCTodaySell);
			 	BigDecimal lvDueBuyDecimal = new BigDecimal(lvCDueBuy);
			 	BigDecimal lvHoldAmountDecimal = new BigDecimal(lvHoldAmount);
			 	//BigDecimal lvReserveAmountDecimal = new BigDecimal(lvReserveAmount);
			 	// BEGIN TASK Change code to do not add lvPendingBuy
			 	//BigDecimal lvTotalHoldAmountDecimal = lvTodayBuyDecimal.add(lvPendingBuyDecimal).add(lvDueBuyDecimal).add(lvHoldAmountDecimal).add(lvReserveAmountDecimal);
			 	//BigDecimal lvTotalHoldAmountDecimal = lvTodayBuyDecimal.add(lvDueBuyDecimal).add(lvHoldAmountDecimal).add(lvReserveAmountDecimal);
			 	BigDecimal lvTotalHoldAmountDecimal = lvTodayBuyDecimal.add(lvDueBuyDecimal).add(lvHoldAmountDecimal);
			 	// END TASK Change code to do not add lvPendingBuy
			 	BigDecimal lvTodayConfirmSellDecimal = new BigDecimal(lvCTodayConfirmSell);
			 	BigDecimal lvPendingSellDecimal = new BigDecimal(lvCPendingSell);
		        BigDecimal lvDueSellDecimal = new BigDecimal(lvCDueSell);
			 	BigDecimal lvReceivableAmountDecimal = lvTodayConfirmSellDecimal.add(lvPendingSellDecimal).add(lvDueSellDecimal);       	 	

		        BigDecimal lvBuyingPowerDecimal = new BigDecimal(lvBuyingPower);
		        BigDecimal lvMarketValueDecimal = new BigDecimal(lvMarketValue);
		        BigDecimal lvTotalAccountValueDecimal = lvBuyingPowerDecimal.add(lvMarketValueDecimal);
			 	// END - TASK#: KZ00001 - Kevin Zhan 20090416
			 	
		        //START HIEU LE - calculate REMAIN RESERVE
//			 	BigDecimal lvManualReserveDecimal = new BigDecimal(lvManualReserve);
//			 	BigDecimal lvTodayOutDecimal = new BigDecimal(lvTodayOut);
//			 	BigDecimal lvPendingWithdrawalDecimal = new BigDecimal(lvPendingWithdrawal);
//			 	BigDecimal lvRemainReserveDecimal = lvManualReserveDecimal.subtract(lvTodayOutDecimal).subtract(lvPendingWithdrawalDecimal);
			 	//END BY HIEU LE
			 	
			 	//mvAccountBalanceBean = new HKSAccountBalanceEnquiryBean();
			 	
			 	//BEGIN - TASK#: TTLVN - Van Tran 20100826 VanTran add code for set the account type
			 	mvAccountBalanceBean.setMvAccountType(lvAccountType);
			 	//END - TASK#: TTLVN - Van Tran 20100826 VanTran add code for set the account type
			 	
			 	mvAccountBalanceBean.setMvCurrencyId(lvHKSAccountBalanceEnquiryTxn.getCurrencyId());
			 	
//			 	//Begin Task #: - TTL-GZ-ZZW-00019 Wind Zhao 20091230 [iTrade R5] Numeric locale for Vietnam
//			 	mvAccountBalanceBean.setMvSettledBalance(new DisplayCurrency(lvHKSAccountBalanceEnquiryTxn.getSettledBalance(), getSvCashValueDecimalPlaces())+"");
//			 	
//			 	// BEGIN - TASK#: KZ00002 - Kevin Zhan 20090416
//			 	mvAccountBalanceBean.setMvTotalHoldAmount(new DisplayCurrency(lvTotalHoldAmountDecimal+"", getSvCashValueDecimalPlaces()).toString());
//			 	mvAccountBalanceBean.setMvReceivableAmount(new DisplayCurrency(lvReceivableAmountDecimal+"", getSvCashValueDecimalPlaces()).toString());
//			 	try {
//			 		mvAccountBalanceBean.setMvAvailableBalance(new DisplayCurrency(lvHKSAccountBalanceEnquiryTxn.getAvailableBalance(), getSvCashValueDecimalPlaces()).toString());
//			 	} catch(NumberFormatException ex) {
//			 		Log.println("[ HKSAccountBalanceEnquiryAction Available Balance is null : " + pClientID + " ]", Log.ACCESS_LOG);
//			 		mvAccountBalanceBean.setMvAvailableBalance(new DisplayCurrency("", getSvCashValueDecimalPlaces()).toString());
//			 	}
//			 	mvAccountBalanceBean.setMvTodaySettlement(new DisplayCurrency(lvHKSAccountBalanceEnquiryTxn.getTodaySettlement(), getSvCashValueDecimalPlaces()).toString());
//			 	mvAccountBalanceBean.setMvLedgerBalace(new DisplayCurrency(lvHKSAccountBalanceEnquiryTxn.getLedgerBalace(), getSvCashValueDecimalPlaces()).toString());
//			 	mvAccountBalanceBean.setMvMarginableValue(new DisplayCurrency(lvHKSAccountBalanceEnquiryTxn.getMarginableValue(), getSvCashValueDecimalPlaces()).toString());
//			 	mvAccountBalanceBean.setMvInterest(new DisplayCurrency(lvHKSAccountBalanceEnquiryTxn.getInterest(), getSvCashValueDecimalPlaces()).toString());
//			 	mvAccountBalanceBean.setMvDPWD(new DisplayCurrency(lvHKSAccountBalanceEnquiryTxn.getDPWD(), getSvCashValueDecimalPlaces()).toString());
//			 	mvAccountBalanceBean.setMvDThreshold(new DisplayCurrency(lvHKSAccountBalanceEnquiryTxn.getDThreshold(), getSvCashValueDecimalPlaces()).toString());
//			 	mvAccountBalanceBean.setMvExtraCreditd(new DisplayCurrency(lvHKSAccountBalanceEnquiryTxn.getExtraCreditd(), getSvCashValueDecimalPlaces()).toString());
//			 	mvAccountBalanceBean.setMvRemaining(new DisplayCurrency(lvHKSAccountBalanceEnquiryTxn.getRemaining(), getSvCashValueDecimalPlaces()).toString());
//			 	mvAccountBalanceBean.setMvMarginPercentage(new DisplayCurrency(lvHKSAccountBalanceEnquiryTxn.getMarginPercentage(), getSvCashValueDecimalPlaces()).toString());
//			 	mvAccountBalanceBean.setMvClientId(lvHKSAccountBalanceEnquiryTxn.getClientId());
//			 	mvAccountBalanceBean.setMvTotalAccountValue(new DisplayCurrency(lvTotalAccountValueDecimal+"", getSvCashValueDecimalPlaces()).toString());
//			 	
//			 	// END - TASK#: KZ00002 - Kevin Zhan 20090416
//			 	mvAccountBalanceBean.setMvDueBalance(new DisplayCurrency(lvHKSAccountBalanceEnquiryTxn.getDueBalance(), getSvCashValueDecimalPlaces()).toString());
//		        
//			 	mvAccountBalanceBean.setMvTodayBS(new DisplayCurrency(String.valueOf(lvCTodayConfirmBS), getSvCashValueDecimalPlaces()).toString());
//			 	mvAccountBalanceBean.setMvPendingBalance(new DisplayCurrency(lvHKSAccountBalanceEnquiryTxn.getPendingBalance(), getSvCashValueDecimalPlaces()).toString());
//			 	mvAccountBalanceBean.setMvMarketValue(new DisplayCurrency(lvHKSAccountBalanceEnquiryTxn.getMarketValue(), getSvCashValueDecimalPlaces()).toString());
//			 	mvAccountBalanceBean.setMvWithdrawableAmount(new DisplayCurrency(lvHKSAccountBalanceEnquiryTxn.getDrawableBal(), getSvCashValueDecimalPlaces()).toString());
//			 	mvAccountBalanceBean.setMvBuyingPowerd(new DisplayCurrency(lvHKSAccountBalanceEnquiryTxn.getBuyingPowerd(), getSvCashValueDecimalPlaces()).toString());
//			 	mvAccountBalanceBean.setMvMarginValue(new DisplayCurrency(lvHKSAccountBalanceEnquiryTxn.getMarginValue(), getSvCashValueDecimalPlaces()).toString());
//			 	mvAccountBalanceBean.setMvCreditLimit(new DisplayCurrency(lvHKSAccountBalanceEnquiryTxn.getCreditLimit(), getSvCashValueDecimalPlaces()).toString());
//			 	BigDecimal lvMarginCallValue = new BigDecimal(lvHKSAccountBalanceEnquiryTxn.getMarginCall());
//			 	mvAccountBalanceBean.setMvMarginCall(new DisplayCurrency(lvMarginCallValue.max(new BigDecimal(0))+"", getSvCashValueDecimalPlaces()).toString());
//			 	
//			 	//BEGIN TASK: TTL-GZ-XYL-00061 XuYuLong 20091229 [iTrade R5] Extra fields in Account Balance
//			 	mvAccountBalanceBean.setMvHoldingAmt(new DisplayCurrency(lvHKSAccountBalanceEnquiryTxn.getHoldAmount(), getSvCashValueDecimalPlaces()).toString());
//			 	mvAccountBalanceBean.setMvUsable(new DisplayCurrency(lvHKSAccountBalanceEnquiryTxn.getUsable(), getSvCashValueDecimalPlaces()).toString());
//			 	//END TASK: TTL-GZ-XYL-00061 XuYuLong 20091229 [iTrade R5] Extra fields in Account Balance
//				mvAccountBalanceBean.setMvPendingWithdraw(new DisplayCurrency(lvHKSAccountBalanceEnquiryTxn.getMvPenddingWithdrawMoney(), getSvCashValueDecimalPlaces()).toString());
//        	 	mvAccountBalanceBean.setMvBuyHoldAmount(new DisplayCurrency(String.valueOf(lvCTodayBuy-lvCTodayConfirmBuy), getSvCashValueDecimalPlaces()).toString());
//        	 	mvAccountBalanceBean.setMvPendingSettled(new DisplayCurrency(String.valueOf(lvCDueBuy+lvCTodayConfirmBuy), getSvCashValueDecimalPlaces()).toString());
			 	
			 	// BEGIN - TASK#: KZ00003 - Kevin Zhan 20090416
			 	
			 	//End Task #: - TTL-GZ-ZZW-00019 Wind Zhao 20091230 [iTrade R5] Numeric locale for Vietnam
			 	//BEGIN TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
			 	String lvDisplayAvailableBalance = IMain.getProperty("DisplayAvailableBalance");
			 	//END TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
			 	if(lvDisplayAvailableBalance!=null && lvDisplayAvailableBalance.equals("true"))
		        {
			 		mvAccountBalanceBean.setMvAvailableBalanceDisableStart("");
			 		mvAccountBalanceBean.setMvAvailableBalanceDisableEnd("");
		        }
		        else
		        {
		        	mvAccountBalanceBean.setMvAvailableBalanceDisableStart("<!--");
		        	mvAccountBalanceBean.setMvAvailableBalanceDisableEnd("-->");
		        }
			 	//not show margin detail if it is not margin client
		        if (!"M".equalsIgnoreCase(lvAccountType))
		        {
		        	mvAccountBalanceBean.setMvForMarginClientStart("<!--");
		        	mvAccountBalanceBean.setMvForMarginClientEnd("-->");
		        }
			 	// END - TASK#: KZ00003 - Kevin Zhan 20090416
		        
		        //TOTALOUTSTANDINGADVANCEAMOUNT	
				//mvAccountBalanceBean.setMvTotalOutAdvance(new DisplayCurrency(lvHKSAccountBalanceEnquiryTxn.getMvTotalOutAdvance(), getSvCashValueDecimalPlaces()).toString());
		        
		        mvAccountBalanceBean.setMvAdvanceableAmount(lvHKSAccountBalanceEnquiryTxn.getMvAvailAdvanceMoney());
		        mvAccountBalanceBean.setMvOutstandingLoan(lvHKSAccountBalanceEnquiryTxn.getMvOutstandingLoan());
		        mvAccountBalanceBean.setMvCSettled(lvHKSAccountBalanceEnquiryTxn.getSettledBalance());
			 	mvAccountBalanceBean.setMvManualReserve(lvHKSAccountBalanceEnquiryTxn.getReserveAmount());	
			 	//BEGIN - TASK - TTLVN - Tan Huynh - Set default values for missing properties
			 	mvAccountBalanceBean.setMvDate("");
			 	mvAccountBalanceBean.setMvReceivableAmt("");
			 	//END - TASK - TTLVN - Tan Huynh - Set default values for missing properties
			 	
			 	//BEGIN HIEU LE
//			 	mvAccountBalanceBean.setMvRemainReserve(new DisplayCurrency(lvRemainReserveDecimal+"", getSvCashValueDecimalPlaces()).toString());
			 	//END HIEU LE
		        
			 	//mvAccountBalanceInfo.add(mvAccountBalanceBean);		        
		}
		return mvAccountBalanceBean;
	}
	
	private static int svCashValueDecimalPlaces = -1;
	
	/**
	 * Returns the cash value decimal places
	 * @return a int which will control how many decimal points will be keep
	 * when use svIntPriceDecimalPlace to format
	 */
	public static int getSvCashValueDecimalPlaces() {
		return svCashValueDecimalPlaces;
	}
	
	/**
	 * Sets the cash value decimal places which received from ini
	 * @param pCashValueDecimalPlaces The cash value decimal places which received from ini
	 */
	public static void setSvCashValueDecimalPlaces(int pCashValueDecimalPlaces) {
		svCashValueDecimalPlaces = pCashValueDecimalPlaces;
	}
}
