package com.ttl.old.itrade.comet.eventHandler.generic;

import java.util.ArrayList;
import java.util.List;

import com.ttl.old.itrade.comet.scheduler.MainScheduler;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


public class EventProcessor<T> {
	private static final Logger logger = LogManager.getLogger(EventProcessor.class);
	private final EventQueue<T> eventQueue;
	private final List<EventProducer<T>> producers;
	private final List<EventConsumer<T>> consumers;
	
	/**
	 * Default constructor.
	 */
	public EventProcessor() {
		producers = new ArrayList<EventProducer<T>>();
		consumers = new ArrayList<EventConsumer<T>>();
		eventQueue = new EventQueue<T>();
	}
	/**
	 * Add producer.
	 * 
	 * @param producer EventProducer<T>
	 */
	public void addProducer(final EventProducer<T> producer) {
		producers.add(producer);
	}
	/**
	 * Add consumer.
	 * 
	 * @param consumer EventConsumer<T>
	 */
	public void addConsumer(final EventConsumer<T> consumer) {
		consumers.add(consumer);
	}
	/**
	 * Constructor.
	 * 
	 * @param producers The producers
	 * @param consumers The consumers
	 */
	public EventProcessor(final List<EventProducer<T>> producers, final List<EventConsumer<T>> consumers) {
		logger.info("Constuctor");
		if (producers == null || producers.isEmpty())
			throw new IllegalArgumentException("No producer");
		if (consumers == null || consumers.isEmpty())
			throw new IllegalArgumentException("No consumer");
		this.producers = producers;
		this.consumers = consumers;
		eventQueue = new EventQueue<T>();				
	}
	/**
	 * Schedule the producers with a specific scheduler. <br/>
	 * NOTE: If there is no producer, this method will do nothing.
	 * 	
	 */
	public void init() {
		logger.info("Begin init");
		try {
			if (producers.isEmpty()) {
				logger.warn("No producer => Do NOT initialize the processor " + getClass());
				return;
			}			
			//Consumers listen on the queue
			eventQueue.addDataChangedListener(new DataChangedListener<T>() {
				public void onChanged(DataChangedEvent<T> evt) {
					if (consumers != null && !consumers.isEmpty()) {
						for (EventConsumer<T> consumer : consumers) {						
							consumer.consume(evt.getOldValue(), evt.getNewValue());
						}
					}
				}
			});
			eventQueue.start();
			//Schedule producers to create events then put to the queue
			for (final EventProducer<T> producer : producers) {				
				MainScheduler.schedule(new Runnable() {
					public void run() {
						if (producer.canRun()) {
							producer.isRunning = true;
							producer.product(eventQueue);
							producer.isRunning = false;
						}
					}
				}, producer.getIntervalInMiliseconds());
			}
		} finally {
			logger.info("End init");
		}
	}
	/**
	 * Destroy the processor.			
	 */
	public void destroy() {
		logger.info("Begin destroy");
		eventQueue.stop();		
		if (consumers != null && !consumers.isEmpty())
			for (EventConsumer<T> c : consumers)
				c.destroy();
		if (producers != null && !producers.isEmpty())
			for (EventProducer<T> p : producers)
				p.destroy();
		logger.info("End destroy");
	}	
}
