package com.ttl.old.itrade.comet.eventHandler.producer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.ttl.old.itrade.comet.eventHandler.generic.EventQueue;
import com.ttl.old.itrade.hks.util.DisplayNumber;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.ttl.old.itrade.comet.dataInfo.MarketIndex;
import com.ttl.old.itrade.comet.eventHandler.caching.MarketIndexCachingManager;
import com.ttl.old.itrade.comet.eventHandler.generic.EventProducer;
import com.ttl.old.itrade.comet.utils.Constants;
import com.ttl.old.itrade.hks.bean.HKSMarketIndexBean;
//import com.itrade.hks.util.DisplayCurrency;
import com.ttl.old.itrade.interfaces.IMain;
//import com.itrade.mds.restful.DataServiceReader;
import com.ttl.old.itrade.util.Log;
import com.ttl.mds.model.MarketIndexModel;



public class MarketIndexReader extends EventProducer<MarketIndex>
{
	private static final Logger logger = LogManager.getLogger(MarketIndexReader.class);
	List<HKSMarketIndexBean> mvMarketIndexList = new ArrayList<HKSMarketIndexBean>();

	@Override
	public void product(EventQueue<MarketIndex> eventQueue) {
		
		boolean useDS = Boolean.valueOf(IMain.getProperty("UseMds"));
		if(useDS)
		{
			processMarketIndexFromDS(eventQueue);
		}
		else
		{
			processMarketIndexFromMarketProject(eventQueue);
		}
	}
	
	private void processMarketIndexFromMarketProject(
			EventQueue<MarketIndex> eventQueue) {
		//mvMarletIndexList.clear();
		
		if(IMain.mvMarketIndex != null && IMain.mvMarketIndex.size() >0)
		{
			HKSMarketIndexBean marketIndexBean=null;
			HKSMarketIndexBean marketIndexResult=null;
			// clear 
			mvMarketIndexList.clear();
			//terator lvIt = AppletGateway.getIvMarketIndex(). .iterator();
			for ( String key : IMain.mvMarketIndex.keySet()) {
				try
				{
					marketIndexBean= IMain.mvMarketIndex.get(key);
					marketIndexResult = new HKSMarketIndexBean();
					String marketID =marketIndexBean.getMvMaketID().trim();
					marketIndexResult.setMvMaketID(marketID);
		    		
		    		String lvMarketIndex = marketIndexBean.getMvMarketIndex() == null ? "": marketIndexBean.getMvMarketIndex();
		    		marketIndexResult.setMvMarketIndex(lvMarketIndex);
		    		
					String lvDifference = marketIndexBean.getMvDifference() == null ? "": marketIndexBean.getMvDifference();
					String lvPercentage = marketIndexBean.getMvPercentage() == null ? "": marketIndexBean.getMvPercentage();
					
					//marketIndexResult.setMvPercentage(new DisplayCurrency(lvPercentage,2).toString());
					//marketIndexResult.setMvDifference(new DisplayCurrency(lvDifference, 2).toString());
		    		
					String lvMarketTotalValue = marketIndexBean.getMvMarketTotalvalue() == null ? "": marketIndexBean.getMvMarketTotalvalue();
					String lvMarketTotalStock = marketIndexBean.getMvMarketTotalQty()== null ? "": marketIndexBean.getMvMarketTotalQty();
					
					try{
						BigDecimal totalValue = new BigDecimal(lvMarketTotalValue);
						// format total trade value in billion
						//BEGIN - TASK : Van Tran 20101115 - correct value for HOSE exchange
						if("HO".equalsIgnoreCase(marketID)){
							totalValue =totalValue.divide(new BigDecimal(1000));
						}else {
							totalValue =totalValue.divide(new BigDecimal(1000000000));
						}
						//END - TASK : Van Tran 20101115 - correct value for HOSE exchange
	
						marketIndexResult.setMvMarketTotalvalue(new DisplayNumber(totalValue.toString(), 2).toString());
						}catch (NumberFormatException e) {
							Log.print(e, Log.ERROR_LOG);
						}
						
					marketIndexResult.setMvMarketTotalQty(new DisplayNumber(lvMarketTotalStock, 0).toString());
					
					String lvMarketAdvance = marketIndexBean.getMvMarketAdvances() == null ? "": marketIndexBean.getMvMarketAdvances();
					String lvMarketDecline = marketIndexBean.getMvMarketDeclines() == null ? "": marketIndexBean.getMvMarketDeclines();
					String lvMarketNoChange = marketIndexBean.getMvMarketNoChange() == null ? "": marketIndexBean.getMvMarketNoChange();
		    		
					marketIndexResult.setMvMarketAdvances(new DisplayNumber(lvMarketAdvance, 0).toString());
					marketIndexResult.setMvMarketDeclines(new DisplayNumber(lvMarketDecline, 0).toString());
					marketIndexResult.setMvMarketNoChange(new DisplayNumber(lvMarketNoChange, 0).toString());
		    		
		    		
					// Put market status info
					if(IMain.mvMarketStatus.containsKey(key)){
						marketIndexResult.setMvMarketStatus(IMain.mvMarketStatus.get(key).toString());
					}
					
					mvMarketIndexList.add(marketIndexResult);
				}
				catch(Exception ex)
				{
					logger.error(ex);
				}
			}
		}else {
			mvMarketIndexList.clear();
		}
		
		if(MarketIndexCachingManager.updateMarketDataIfChanged(mvMarketIndexList))
		{
			eventQueue.addDataEvent(MarketIndexCachingManager.getLastMarketIndex());
		}	
		
	}
	
	private void processMarketIndexFromDS(EventQueue<MarketIndex> eventQueue)
	{
		mvMarketIndexList.clear();
		
		String marketList = IMain.getProperty("SupportedMarket");
	
		if(marketList != null && !marketList.isEmpty())
		{
			String[] markets = marketList.split(Constants.SEPERATE_CHAR);
			MarketIndexModel marketIndexBean=null;
			HKSMarketIndexBean marketIndexResult=null;
			
			for ( String key : markets) {
				try
				{
					//marketIndexBean= DataServiceReader.getMarketIndexModel(key);//IMain.mvMarketIndex.get(key);
					if(marketIndexBean != null)
					{
						marketIndexResult = new HKSMarketIndexBean();
						String marketID = marketIndexBean.getMarketId().trim();
						marketIndexResult.setMvMaketID(marketID);
			    		
			    		String lvMarketIndex = marketIndexBean.getMarketIndex() == null ? "": marketIndexBean.getMarketIndex();
			    		marketIndexResult.setMvMarketIndex(lvMarketIndex);
			    		
						String lvDifference = marketIndexBean.getIndexChange() == null ? "": marketIndexBean.getIndexChange();
						String lvPercentage = marketIndexBean.getIndexPercent() == null ? "": marketIndexBean.getIndexPercent();
						
						//marketIndexResult.setMvPercentage(new DisplayCurrency(lvPercentage,2).toString());
						//marketIndexResult.setMvDifference(new DisplayCurrency(lvDifference, 2).toString());
			    		
						String lvMarketTotalValue = marketIndexBean.getMarketTotalValue() == null ? "": marketIndexBean.getMarketTotalValue();
						String lvMarketTotalStock = marketIndexBean.getMarketTotalQty() == null ? "": marketIndexBean.getMarketTotalQty();
						
						try{
							BigDecimal totalValue = new BigDecimal(lvMarketTotalValue);
							// format total trade value in billion
							//BEGIN - TASK : Van Tran 20101115 - correct value for HOSE exchange
							if("HO".equalsIgnoreCase(marketID)){
								totalValue =totalValue.divide(new BigDecimal(1000));
							}else {
								totalValue =totalValue.divide(new BigDecimal(1000000000));
							}
							//END - TASK : Van Tran 20101115 - correct value for HOSE exchange
	
							marketIndexResult.setMvMarketTotalvalue(new DisplayNumber(totalValue.toString(), 2).toString());
							}catch (NumberFormatException e) {
								Log.print(e, Log.ERROR_LOG);
							}
							
						marketIndexResult.setMvMarketTotalQty(new DisplayNumber(lvMarketTotalStock, 0).toString());
						
						String lvMarketAdvance = marketIndexBean.getMarketAdvance() == null ? "": marketIndexBean.getMarketAdvance();
						String lvMarketDecline = marketIndexBean.getMarketDecline() == null ? "": marketIndexBean.getMarketDecline();
						String lvMarketNoChange = marketIndexBean.getMarketNoChange() == null ? "": marketIndexBean.getMarketNoChange();
			    		
						marketIndexResult.setMvMarketAdvances(new DisplayNumber(lvMarketAdvance, 0).toString());
						marketIndexResult.setMvMarketDeclines(new DisplayNumber(lvMarketDecline, 0).toString());
						marketIndexResult.setMvMarketNoChange(new DisplayNumber(lvMarketNoChange, 0).toString());
			    		marketIndexResult.setMvMarketTime(marketIndexBean.getMarketTime());
			    		
						marketIndexResult.setMvMarketStatus(marketIndexBean.getMarketStatus());
						
						mvMarketIndexList.add(marketIndexResult);
					}
				}
				catch(Exception ex)
				{
					logger.error("Error in getting processMarketIndexFromDS with market " + key);
					logger.error(ex);
					ex.printStackTrace();
				}
				
			}
		}
		
		if(MarketIndexCachingManager.updateMarketDataIfChanged(mvMarketIndexList))
		{
			eventQueue.addDataEvent(MarketIndexCachingManager.getLastMarketIndex());
		}			
	}
}
