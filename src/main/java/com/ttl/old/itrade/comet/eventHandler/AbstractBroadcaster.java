package com.ttl.old.itrade.comet.eventHandler;

import java.util.ArrayList;
import java.util.List;

import com.ttl.old.itrade.comet.dataInfo.AlertInfo;
import com.ttl.old.itrade.comet.dataInfo.WorldMarketIndex;
import com.ttl.old.itrade.hks.bean.HKSAccountBalanceEnquiryBean;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.ttl.old.itrade.comet.dataInfo.CommonInfo;
import com.ttl.old.itrade.comet.dataInfo.MarketIndex;
import com.ttl.old.itrade.comet.dataInfo.MarketInfo;
import com.ttl.old.itrade.comet.dataInfo.OrderEnquiryInfo;
import com.ttl.old.itrade.comet.dataInfo.StockWatchList;


public abstract class AbstractBroadcaster implements Runnable, EventBroadcaster {
    private static final Logger logger = LogManager.getLogger(AbstractBroadcaster.class);
    private final String broadcasterName;
    private final List<Object> dataQueue;
    private boolean working = false;
    /**
     * Constructor.
     * 
     * @param broadcasterName The broadcaster name
     */
    public AbstractBroadcaster(final String broadcasterName) {
        logger.info("Constructor for " + broadcasterName);
        this.broadcasterName = broadcasterName;
        this.dataQueue = new ArrayList<Object>();
    }
    /**
     * Start the broadcaster.
     */
    public final void start() {
    	if (working) return;
    	working = true;
        beforeStart();
        final Thread thread = new Thread(this, broadcasterName);
        thread.setDaemon(true);
        thread.start();
    }
    /**
     * Stop the broadcaster.
     */
    public final void stop() {
    	if (!working) return;
        logger.info("Begin stop " + broadcasterName);
        working = false;
        synchronized (dataQueue) {
            while(!dataQueue.isEmpty()) {
                try {
                    dataQueue.wait();
                } catch (InterruptedException e) {}
            }
        }
        release();
        logger.info("End stop " + broadcasterName);        
    }
    /**
     * Release resource if any.
     */
    protected void release() {
    }
    /**
     * May setting up before starting the broadcaster.
     */
    protected abstract void beforeStart();    
    /**
     * Broadcast an object.
     * 
     * @param element Object
     */
    protected abstract void broadcast(Object... element);
        
    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    
    public final void run() {
        logger.info(String.format("%s already started", broadcasterName));
        while (working) {
            synchronized (dataQueue) {
                if (dataQueue.isEmpty()) {
                    try {
                        dataQueue.wait();
                    } catch (InterruptedException e) {}
                }
                if (!dataQueue.isEmpty()) {
                    final Object[] pendingObj = dataQueue.toArray();
                    dataQueue.clear();
                    broadcast(pendingObj);
                }
            }
        }
        synchronized (dataQueue) {
            dataQueue.notify();
        }
    }

    public void broadcastMarket(MarketInfo market) {
        synchronized (dataQueue) {
            dataQueue.add(market);
            dataQueue.notify();
        }
    }
   
    public void broadcastMarketIndex(MarketIndex market) {
        synchronized (dataQueue) {
            dataQueue.add(market);
            dataQueue.notify();
        }
    }
    
    public void broadcastWorldMarketIndex(WorldMarketIndex wMarket) {
        synchronized (dataQueue) {
            dataQueue.add(wMarket);
            dataQueue.notify();
        }
    }
    
    public void broadcastAccountSummary(final List<HKSAccountBalanceEnquiryBean> listAccountSummaryInfo) {
    	synchronized (dataQueue) {
    		for (HKSAccountBalanceEnquiryBean acc : listAccountSummaryInfo)
    			dataQueue.add(acc);
    		dataQueue.notify();
		}    	    
    }
    
    public void broadcastStockWatchInfo(StockWatchList stockWatch) {
    	synchronized (dataQueue) {
   			dataQueue.add(stockWatch);
    		dataQueue.notify();
		}    	    
    }
    
    public void broadcastOrderEnquiryInfo(List<OrderEnquiryInfo> orders){
    	synchronized (dataQueue) {
    		for (OrderEnquiryInfo order : orders)
    			dataQueue.add(order);
    		dataQueue.notify();
		}    	    
    }
    
    public void broadcastAlertInfo(List<AlertInfo> alerts){
    	synchronized (dataQueue) {
    		for (AlertInfo alert : alerts)
    			dataQueue.add(alert);
    		dataQueue.notify();
		}    	    
    }
    
    public void broadcastCommonInfo(CommonInfo common){
    	synchronized (dataQueue) {
   			dataQueue.add(common);
    		dataQueue.notify();
		}    	    
    }
    
    public void broadcastWarningDataInfo(MarketInfo market) {
        synchronized (dataQueue) {
            dataQueue.add(market);
            dataQueue.notify();
        }
    }
    
}
