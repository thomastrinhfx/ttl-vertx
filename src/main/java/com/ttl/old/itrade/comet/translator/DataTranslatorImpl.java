/*package com.itrade.comet.translator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


import Browser;
import Alert;
import CommonInfo;
import MarketIndex;
import MarketInfo;
import StockWatchInfo;
import StockWatchList;
import WorldMarketIndex;
import AccountSummaryDataCachingManager;
import MarketDataCachingManager;
import MarketIndexCachingManager;
import StockWatchDataCachingManager;
import WorldMarketIndexCachingManager;
import Constants;
import HKSAccountBalanceEnquiryBean;
import HKSHistoricalChartDataBean;
import HKSMarketDataBean;
import HKSOrderBean;

public class DataTranslatorImpl   implements DataTranslator {
	
	public String translateAccountSummaryToJson(final HKSAccountBalanceEnquiryBean accSum) {	
		JSONObject json = new JSONObject();
		json.put("mvAccountBalanceInfo", accSum);
		String result = json.toString();
		return result;
		
	}

	public String translateInitDataToJson(String accountNo, Browser browser)
	{
		JSONObject json = new JSONObject();
		//Add market data list 
		if(browser.isEnableWatchList())
		{
			JSONArray array = new JSONArray();
			List<String> stockKeys = browser.getSymbolInfo();
			for(String stockKey: stockKeys)
			{
				HKSMarketDataBean bean = MarketDataCachingManager.get(stockKey);
				if(bean != null)
					array.add(bean);
			}
			if(array.size() > 0)
				json.put("mvMarketDataList", array);
		}
		
		//Account balance
		if(browser.isEnableAccountBalance())
		{
			json.put("mvAccountBalanceInfo", AccountSummaryDataCachingManager.getAccountSummaryInfo(accountNo));
		}
		
		//Market index 
		if(browser.isEnableMarketIndex())
		{
			json.put("mvMarketIndexList", MarketIndexCachingManager.getLastMarketIndex().toJsonArray(true));
		}

		if(browser.isEnableWorldMarketIndex())
		{
			json.put("mvWorldMarketIndexList", WorldMarketIndexCachingManager.getLastWorldMarketIndex().toJsonArray(true));
		}
		
		if(browser.isEnableStockwatchInfo())
		{
			StockWatchList swl = StockWatchDataCachingManager.getStockWatchList(accountNo);
			JSONArray jsonList = buildStockWatchListJSON(swl);
			
			if(jsonList.size() > 0)
				json.put("mvStockWatchList", jsonList);
		}
		
		json.put("success", true);
		String result = json.toString();
		return result;
		
	}
	
	@Override
	public String translateChangedDataToJson(String accountNo, Browser browser, Set<String> types) {
		JSONObject json = new JSONObject();
		// Account balance
		if (types.contains(Constants.CASH_BALANCE_TYPE)) {
			json.put("mvAccountBalanceInfo", AccountSummaryDataCachingManager.getAccountSummaryInfo(accountNo));
		}

		// Market index
		if (types.contains(Constants.MARKET_INDEX_TYPE)) {
			json.put("mvMarketIndexList", MarketIndexCachingManager.getLastMarketIndex().toJsonArray(true));
		}

		if (types.contains(Constants.WORLD_MARKET_INDEX_TYPE)) {
			json.put("mvWorldMarketIndexList",
					WorldMarketIndexCachingManager.getLastWorldMarketIndex().toJsonArray(true));
		}

		if (types.contains(Constants.STOCK_WATCH_TYPE) || types.contains(Constants.WATCH_LIST_TYPE)) {
			StockWatchList swl = StockWatchDataCachingManager.getStockWatchList(accountNo);
			// just return changed records only
			List<StockWatchInfo> changedRecords = new ArrayList<StockWatchInfo>();
			if(!browser.getChangedStockWatchIds().isEmpty()){
				for (StockWatchInfo swi : swl.getStocks()) {
					if(browser.getChangedStockWatchIds().contains(swi.getMvStockWatchID())){
						changedRecords.add(swi);
					}
				}
				// reset changed ids
				browser.getChangedStockWatchIds().clear();
			}
			// filter the changed records
			swl.setStocks(changedRecords);
//			if(changedRecords.size() == 1){
//				System.out.println("###Write1: " + changedRecords.get(0).getMvMarketDataBean().toString());
//			}else {
//				System.out.println("###Write: " + changedRecords.toString());
//			}
			JSONArray jsonList = buildStockWatchListJSON(swl);

			if (jsonList.size() > 0)
				json.put("mvStockWatchList", jsonList);
		}

		json.put("success", true);
		String result = json.toString();
		return result;

	}
	
	public String translateMarketDataToJson(MarketInfo market, List<String> watchList)
	{
		JSONObject json = new JSONObject();	
		JSONArray array = new JSONArray();
		MarketInfo mvMarket = market.clone();
		if(mvMarket == null || mvMarket.getMarketData().size() == 0 || watchList.isEmpty())
		{
			return null;
		}
		else
		{
			List<HKSMarketDataBean> changedList = mvMarket.getMarketData();
			if(changedList != null && changedList.size() > 0)
			{
				String stockKey = "";
				for(HKSMarketDataBean bean : changedList)
				{
					stockKey = bean.getMvMarketID() + "|" + bean.getMvSymbol();
					if(watchList.contains(stockKey))
					{
						array.add(bean);
					}
				}
				
				changedList.clear();
			}
		}
		
		String result = null;
		if(array.size() > 0)
		{
			json.put("mvMarketDataList", array);
			result = json.toString();
			array.clear();
		}		 
		
		mvMarket = null;
		return result;		
	}

	@Override
	public String translateMarketIndexToJson(
			MarketIndex marketIndex) {
		JSONObject json = new JSONObject();	
		
		json.put("mvMarketIndexList", marketIndex.toJsonArray(false));
		String result = json.toString();
		return result;	
	}

	@Override
	public String translateWorldMarketIndexToJson(
			WorldMarketIndex wMarketIndex) {
		JSONObject json = new JSONObject();	
		
		json.put("mvWorldMarketIndexList", wMarketIndex.toJsonArray(false));
		String result = json.toString();
		return result;	
	}
	
	@Override
	public String translateStockWatchInfoToJson(
			StockWatchList stockWatchList) {
		JSONObject json = new JSONObject();	
		//System.out.println("Translate: " + stockWatchList.getStocks());
		JSONArray jsonList = buildStockWatchListJSON(stockWatchList);
		
		String result = null;
		if(jsonList.size() > 0)
		{
			json.put("mvStockWatchList", jsonList);
			result = json.toString();
		}		
		
		return result;	
	}

	private JSONArray buildStockWatchListJSON(StockWatchList stockWatchList) {
		JSONArray jsonList = new JSONArray();
		StockWatchList stockWatch = stockWatchList.clone();
		for(StockWatchInfo stockInfo: stockWatch.getStocks())
		{
			String curSymb = stockInfo.getCurrentSymbol();
			String symbol = stockInfo.getMvMarketDataBean() != null? stockInfo.getMvMarketDataBean().getMvSymbol(): "";
			if(curSymb != null && curSymb.equals(symbol)){
				JSONObject jsonItem = new JSONObject();
				jsonItem.put("mvMarketDataBean", stockInfo.getMvMarketDataBean());
				jsonItem.put("mvStockWatchID", stockInfo.getMvStockWatchID());
				//
				// just return history data in case of stock watch subscriber 
				if(StockWatchInfo.STOCK_WATCH.equals(stockInfo.getWatchType())){
					JSONArray jsonHistList = new JSONArray();
					for(HKSHistoricalChartDataBean bean : stockInfo.getMvHistoricalChartDataBeanList())
					{
						jsonHistList.add(bean);			
					}
					
					jsonItem.put("mvHistoricalChartDataBeanList", jsonHistList);
				}
				jsonList.add(jsonItem);
			}
		}
		//System.out.println("buildStockWatchListJSON: " + jsonList);
		return jsonList;
	}

	@Override
	public String translateOrderEnquiryInfoToJson(List<HKSOrderBean> beanList) {
		JSONObject json = new JSONObject();	
		JSONArray jsonList = new JSONArray();
		for(HKSOrderBean bean : beanList)
		{
			jsonList.add(bean);			
		}
		
		String result = null;
		if(jsonList.size() > 0)
		{
			json.put("mvDynamicUpdatedOrderList", jsonList);
			result = json.toString();
		}

		return result;
	}

	@Override
	public String translateWarningMarketDataToJson(MarketInfo market,
			List<String> registerList) {
		
		JSONObject json = new JSONObject();	
		JSONArray jsonList = new JSONArray();
		MarketInfo marketInfo = market.clone();

		if(marketInfo == null || marketInfo.getMarketData().size() == 0 || registerList.isEmpty())
		{
			return null;
		}
		else
		{
			List<HKSMarketDataBean> changedList = marketInfo.getMarketData();
			if(changedList != null && changedList.size() > 0)
			{
				for(HKSMarketDataBean bean : changedList)
				if(registerList.contains(bean.getMvSymbol()))
				{
					jsonList.add(bean);
				}
			}
		}
		
		String result = null;
		if(jsonList.size() > 0)
		{
			json.put("mvDynamicUpdatedWarningList", jsonList);
			result = json.toString();
		}
		return result;
		
	}

	@Override
	public String translateAlertInfoToJson(List<Alert> alerts) {
		JSONObject json = new JSONObject();	
		JSONArray jsonList = new JSONArray();
		for(Alert bean : alerts)
		{
			jsonList.add(bean.toJson());			
		}
		
		String result = null;
		if(jsonList.size() > 0)
		{
			json.put("mvAlert", jsonList);
			result = json.toString();
		}
		 
		return result;
	}
	
	@Override
	public String translateCommonInfoToJson(CommonInfo info) {
		JSONObject json = new JSONObject();	
				
		String result = null;
		if(info != null)
		{
			json.put("connection", info);
			//json.put("foConnection", info.isSvIsConnected());
			//json.put("boConnection", info.isSvExternalIsConnected());
			result = json.toString();
		}
		 
		return result;
	}
	
}
*/