package com.ttl.old.itrade.comet.eventHandler;

import java.util.List;

import com.ttl.old.itrade.comet.dataInfo.AlertInfo;
import com.ttl.old.itrade.comet.dataInfo.CommonInfo;
import com.ttl.old.itrade.comet.dataInfo.MarketIndex;
import com.ttl.old.itrade.comet.dataInfo.MarketInfo;
import com.ttl.old.itrade.comet.dataInfo.OrderEnquiryInfo;
import com.ttl.old.itrade.comet.dataInfo.StockWatchList;
import com.ttl.old.itrade.comet.dataInfo.WorldMarketIndex;
import com.ttl.old.itrade.hks.bean.HKSAccountBalanceEnquiryBean;


public interface EventBroadcaster {
	public void broadcastMarket(MarketInfo market);
	//public void broadcastSession(SessionInfo sessionInfo);
	//public void broadcastStock(List<StockInfo> stocks);
	//public void broadcastOrder(List<OrderList> orders);
	public void broadcastAccountSummary(List<HKSAccountBalanceEnquiryBean> listAccountSummaryInfo);
	public void broadcastMarketIndex(MarketIndex marketIndex);
	public void broadcastWorldMarketIndex(WorldMarketIndex worldMarketIndex);
	public void broadcastStockWatchInfo(StockWatchList stockWatchInfo);
	public void broadcastOrderEnquiryInfo(List<OrderEnquiryInfo> orders);
	public void broadcastWarningDataInfo(MarketInfo market);
	public void broadcastAlertInfo(List<AlertInfo> alerts);
	public void broadcastCommonInfo(CommonInfo commons);
	
}
