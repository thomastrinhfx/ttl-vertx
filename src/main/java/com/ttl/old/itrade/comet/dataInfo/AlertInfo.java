package com.ttl.old.itrade.comet.dataInfo;

import java.util.List;

public class AlertInfo {
	
	private String clientId;
	private List<Alert> alerts;	
	
	public AlertInfo(String clientId, List<Alert> alertList)
	{
		this.clientId = clientId;
		this.alerts = alertList;
	}
	
	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public List<Alert> getAlerts() {
		return alerts;
	}

	public void setAlerts(List<Alert> alerts) {
		this.alerts = alerts;
	}

	
	//public abstract JSONObject toJson();
}
