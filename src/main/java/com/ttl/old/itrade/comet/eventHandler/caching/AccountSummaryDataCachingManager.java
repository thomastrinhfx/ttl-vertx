package com.ttl.old.itrade.comet.eventHandler.caching;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ttl.old.itrade.hks.bean.HKSAccountBalanceEnquiryBean;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


public class AccountSummaryDataCachingManager {
	private static final Logger logger = LogManager.getLogger(AccountSummaryDataCachingManager.class);
	private static final Map<String, HKSAccountBalanceEnquiryBean> mapAccountSum = new HashMap<String, HKSAccountBalanceEnquiryBean>();
	
	public static void registerAccount(final String account, final String accountType) {
		synchronized (mapAccountSum) {
			final HKSAccountBalanceEnquiryBean accSum = new HKSAccountBalanceEnquiryBean();
			accSum.setMvClientId(account);
			accSum.setMvAccountType(accountType);
			mapAccountSum.put(account, accSum);
		}
	}
	
	public static HKSAccountBalanceEnquiryBean unregisterAccount(final String accountNo) {
		if (logger.isDebugEnabled())
			logger.debug("Remove caching account summary for account " + accountNo);
		synchronized (mapAccountSum) {
			return mapAccountSum.remove(accountNo);			
		}
	}
	
	public static boolean updateAccountSumIfChanged(final HKSAccountBalanceEnquiryBean nAccSum) {
		synchronized (mapAccountSum) {
			boolean isChanged = false;
			final HKSAccountBalanceEnquiryBean oAccSum = mapAccountSum.get(nAccSum.getMvClientId());
			if (oAccSum != null && AccountSummaryDataCachingManager.updateIfChanged(oAccSum, nAccSum)) {				
				isChanged = true;
				mapAccountSum.put(nAccSum.getMvClientId(), nAccSum);
			}
			return isChanged;
		}
	}
	
	private static boolean updateIfChanged(HKSAccountBalanceEnquiryBean oldBean, HKSAccountBalanceEnquiryBean newBean)
	{
		boolean isChanged = false;
		try {
			if(!newBean.getMvCurrencyId().equals(oldBean.getMvCurrencyId())
					|| !newBean.getMvSettledBalance().equals(oldBean.getMvSettledBalance())
					|| !newBean.getMvDueBalance().equals(oldBean.getMvDueBalance())
					|| !newBean.getMvTodayBS().equals(oldBean.getMvTodayBS())
					|| !newBean.getMvPendingBalance().equals(oldBean.getMvPendingBalance())
					|| !newBean.getMvMarketValue().equals(oldBean.getMvMarketValue())
					|| !newBean.getMvWithdrawableAmount().equals(oldBean.getMvWithdrawableAmount())
					|| !newBean.getMvBuyingPowerd().equals(oldBean.getMvBuyingPowerd())
					|| !newBean.getMvMarginValue().equals(oldBean.getMvMarginValue())
					|| !newBean.getMvCreditLimit().equals(oldBean.getMvCreditLimit())
					|| !newBean.getMvMarginCall().equals(oldBean.getMvMarginCall())
					|| !newBean.getMvTotalHoldAmount().equals(oldBean.getMvTotalHoldAmount())
					|| !newBean.getMvReceivableAmount().equals(oldBean.getMvReceivableAmount())
					|| !newBean.getMvAvailableBalance().equals(oldBean.getMvAvailableBalance())
					|| !newBean.getMvTodaySettlement().equals(oldBean.getMvTodaySettlement())
					|| !newBean.getMvLedgerBalace().equals(oldBean.getMvLedgerBalace())
					|| !newBean.getMvMarginableValue().equals(oldBean.getMvMarginableValue())
					|| !newBean.getMvInterest().equals(oldBean.getMvInterest())
					|| !newBean.getMvDPWD().equals(oldBean.getMvDPWD())
					|| !newBean.getMvDThreshold().equals(oldBean.getMvDThreshold())
					|| !newBean.getMvExtraCreditd().equals(oldBean.getMvExtraCreditd())
					|| !newBean.getMvRemaining().equals(oldBean.getMvRemaining())
					|| !newBean.getMvMarginPercentage().equals(oldBean.getMvMarginPercentage())
					|| !newBean.getMvClientId().equals(oldBean.getMvClientId())
					|| !newBean.getMvTotalAccountValue().equals(oldBean.getMvTotalAccountValue())
					|| !newBean.getMvAvailableBalanceDisableStart().equals(oldBean.getMvAvailableBalanceDisableStart())
					|| !newBean.getMvAvailableBalanceDisableEnd().equals(oldBean.getMvAvailableBalanceDisableEnd())
					//|| !newBean.getMvForMarginClientStart().equals(oldBean.getMvForMarginClientStart())
					//|| !newBean.getMvForMarginClientEnd().equals(oldBean.getMvForMarginClientEnd())
					|| !newBean.getMvDate().equals(oldBean.getMvDate())
					|| !newBean.getMvUsable().equals(oldBean.getMvUsable())
					|| !newBean.getMvHoldingAmt().equals(oldBean.getMvHoldingAmt())
					|| !newBean.getMvReceivableAmt().equals(oldBean.getMvReceivableAmt())
					|| !newBean.getMvAccountType().equals(oldBean.getMvAccountType())
					|| !newBean.getMvOutstandingLoan().equals(oldBean.getMvOutstandingLoan())
					|| !newBean.getMvCSettled().equals(oldBean.getMvCSettled())
					|| !newBean.getMvManualReserve().equals(oldBean.getMvManualReserve())
					|| !newBean.getMvAdvanceableAmount().equals(oldBean.getMvAdvanceableAmount())
					|| !newBean.getMvPendingWithdraw().equals(oldBean.getMvPendingWithdraw())
					|| !newBean.getMvPendingSettled().equals(oldBean.getMvPendingSettled())
					|| !newBean.getMvBuyHoldAmount().equals(oldBean.getMvBuyHoldAmount())
				)
				{
					isChanged = true;
				}
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("UpdateIfChanged():", e);
			isChanged = true;
		}
		
				
		return isChanged;
	}
	
	public static List<HKSAccountBalanceEnquiryBean> getAccountSummaryInfo(final String... accounts) {
		synchronized (mapAccountSum) {
			final List<HKSAccountBalanceEnquiryBean> retList = new ArrayList<HKSAccountBalanceEnquiryBean>();
			for (String acc : accounts) {
				final HKSAccountBalanceEnquiryBean asi = mapAccountSum.get(acc);
				if (asi != null) {
					retList.add(asi);
				}
			}
			return retList;
		}
	}
	
	public static HKSAccountBalanceEnquiryBean getAccountSummaryInfo(final String account) {
		synchronized (mapAccountSum) {
			HKSAccountBalanceEnquiryBean asi = null;
			if(mapAccountSum.get(account) != null)
			{
				asi = mapAccountSum.get(account);			
			}
			else
			{
				asi = new HKSAccountBalanceEnquiryBean();
				mapAccountSum.put(account, asi);
			}
			return asi;
		}
	}
	
}
