package com.ttl.old.itrade.comet.eventHandler.producer;

import java.util.ArrayList;
import java.util.List;

import com.ttl.old.itrade.comet.dataInfo.CommonInfo;
import com.ttl.old.itrade.comet.eventHandler.generic.EventQueue;
import com.ttl.old.itrade.tp.TPConnector;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.ttl.old.itrade.comet.eventHandler.generic.EventProducer;

public class CommonInfoReader extends EventProducer<CommonInfo>
{
	private static final Logger logger = LogManager.getLogger(CommonInfoReader.class);
	
	public static List<String> notifications = new ArrayList<String>();
	
	@Override
	public void product(EventQueue<CommonInfo> eventQueue) {
		boolean foConnection = TPConnector.svIsConnected;
		boolean boConnection = TPConnector.svExternalIsConnected;
		
		try
		{
			if(foConnection == false || boConnection == false)
			{
				CommonInfo info = new CommonInfo(foConnection, boConnection);
				eventQueue.addDataEvent(info);
			}
			if(!notifications.isEmpty()){
				String msg = notifications.remove(0);
				if(msg != null && !"".equals(msg)){
					CommonInfo info = new CommonInfo(foConnection, boConnection);
					info.setMsgNotification(msg);
					eventQueue.addDataEvent(info);
				}
			}
		}
		catch(Exception ex)
		{
			logger.error("Error in getting common info with account no ");
			logger.info(ex);
		}
				
	}
}
