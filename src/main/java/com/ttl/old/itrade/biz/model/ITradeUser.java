package com.ttl.old.itrade.biz.model;

import java.io.Serializable;

import com.ttl.old.itrade.web.ticker.TickerViewProccessor;


public class ITradeUser implements Serializable{
	public ITradeUser(String accounid) {
		this.setMvAccountID(accounid);
	}
	public ITradeUser() {
		
	}
	
	public String getMvSeesionID() {
		return mvSeesionID;
	}
	public void setMvSeesionID(String mvSeesionID) {
		this.mvSeesionID = mvSeesionID;
	}
	public TickerViewProccessor getMvTickerViewProccessor() {
		return mvTickerViewProccessor;
	}
	public void setMvTickerViewProccessor(TickerViewProccessor mvTickerViewProccessor) {
		this.mvTickerViewProccessor = mvTickerViewProccessor;
	}


	private static final long serialVersionUID =1L;
	private String mvAccountID="";
	private String mvAccountName="";
	private String mvFullName="";
	private String mvLoginIpAddress="";
	public String getMvAccountID() {
		return mvAccountID;
	}
	public void setMvAccountID(String mvAccountID) {
		this.mvAccountID = mvAccountID;
	}
	public String getMvAccountName() {
		return mvAccountName;
	}
	public void setMvAccountName(String mvAccountName) {
		this.mvAccountName = mvAccountName;
	}
	public String getMvFullName() {
		return mvFullName;
	}
	public void setMvFullName(String mvFullName) {
		this.mvFullName = mvFullName;
	}
	public String getMvLoginIpAddress() {
		return mvLoginIpAddress;
	}
	public void setMvLoginIpAddress(String mvLoginIpAddress) {
		this.mvLoginIpAddress = mvLoginIpAddress;
	}
	
	
	private String mvSeesionID;

	private TickerViewProccessor mvTickerViewProccessor;
		
	public void startTicker() {
		if(mvTickerViewProccessor ==null) {
			mvTickerViewProccessor = new TickerViewProccessor(this);
		}
		if(!mvTickerViewProccessor.isStarted()) {
			this.mvTickerViewProccessor.start();
		}
	}
	
	public void stopTicker() {
		if(mvTickerViewProccessor!= null && mvTickerViewProccessor.isStarted()) {
			this.mvTickerViewProccessor.stop();
		}
	}
	
	public void cancelTask() {
		if(mvTickerViewProccessor != null && mvTickerViewProccessor.isStarted()) {
			this.mvTickerViewProccessor.cancelTask();
		}
	}
	
	
}
