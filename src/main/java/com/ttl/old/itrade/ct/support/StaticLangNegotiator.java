/**
 * 
 *//*
package com.itrade.ct.support;

import com.itrade.base.jesapi.ITradeAPI;
import com.itrade.util.StringUtils;
import com.itrade.web.engine.ITradeServlet;
import com.itrade.web.helper.ITradeSessionManager;

*//**
 * <p>Copyright: Copyright(c)Transaction Technologies Limited 2014 by xuejie.xiao</p>
 * @author jay.wince
 * @version 1.0
 *//*
public class StaticLangNegotiator {
	
    public static int getCurrentLangCode(){
    	int langCode = ITradeServlet.svCompanyConfig.defaultLangcode();
		if (ITradeAPI.httpUtilities().getCurrentRequest()!=null) {
			String lvLangCode = ITradeAPI.httpUtilities().getCurrentRequest().getParameter(HKSRequestParameterKey.LANG);
			if (!StringUtils.isNullStr(lvLangCode)) {
				langCode = Integer.parseInt(lvLangCode);
			}else {
				if (ITradeSessionManager.currentUser() != null) {
					langCode = ITradeSessionManager.getCurrentLang();
				}
			}
		}
		return langCode; 
    }
    
    public static String getCurrentLangText(){
    	return ITradeServlet.svCompanyConfig.mappingLangText(getCurrentLangCode());
    }
}
*/