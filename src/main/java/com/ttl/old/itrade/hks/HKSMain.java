// ----------------------------------------------
// Modified by Bowen Chau on 7 Mar 2006
// Added new action name of account registration
// ----------------------------------------------

package com.ttl.old.itrade.hks;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.GenericRequest;
import com.ttl.old.itrade.hks.request.HKSModifyOrderRequest;
import com.ttl.old.itrade.hks.request.HKSPlaceOrderRequest;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.AppletGateway;
//import com.itrade.tp.TPBroadcastManager;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.ttl.old.itrade.util.SessionTracker;

/**
 * The HKSMain class defines methods that go to start login thread and register request when tomcat starting.
 * @author
 *
 */
public class HKSMain extends IMain
{
   public static Vector svAvailableGoodTillDateList;
   public static HashMap svAvailableGoodTillDateMap;
   //BEGIN Task #: WL00619 Walter Lau 27 July 2007
   public static Vector svAvailableMarketIDList;
   //End Task #: WL00619 Walter Lau 27 July 2007
   public static Date svCurrentTradeDate;
   public static HashMap svCurrentTradeDateMap;
   public static SessionTracker svSessionTracker = null;

   /**
    * This method to start authenticator local session.
    */
   public void startAuthenticator()
   {
      localSessionTracker = new SessionTracker();
      svSessionTracker = localSessionTracker;
   }

	// BEGIN Task #: al00001 13-08-08
   /**
    * Rturns the config ITrade.ini information.
    * @return In config ITrade.ini information.
    */
	public Properties getProperties(){
		return _pIni;
	}
	// END Task #: al00001 13-08-08

	/**
	 * This method to registration specified request.
	 * @param pPath The path of request xml.
	 * @param pEntityID The entity ID.
	 * @param pTPAgentID The agent ID of TP.
	 * @param pTPAgentPassword The agent password of TP.
	 * @param pBranchID The branch ID.
	 * @param pFunctionName The function name.
	 */
	public void startRequestRegistration(String pPath, String pEntityID, String pTPAgentID, String pTPAgentPassword, String pBranchID, String pFunctionName){
		String lvPath = pPath;
		String lvEntityID = pEntityID;
	    String lvTPAgentID = pTPAgentID;
	    String lvTPAgentPassword = pTPAgentPassword;
	    String lvBranchID = pBranchID;
	    TPManager lvTPManager = ITradeServlet.getTpManager();
	      
	    lvTPManager.registerRequest(RequestName.HKSModifyOrderRequest, new HKSModifyOrderRequest(lvTPManager, getRequestXML(lvPath, "HKSModifyOrder.xml", lvEntityID, lvTPAgentID, lvTPAgentPassword, lvBranchID)));
	    lvTPManager.registerRequest(RequestName.HKSModifyOrderRequest_TW, new HKSModifyOrderRequest(lvTPManager, getRequestXML(lvPath, "HKSModifyOrder_zh_TW.xml", lvEntityID, lvTPAgentID, lvTPAgentPassword, lvBranchID)));
		lvTPManager.registerRequest(RequestName.HKSModifyOrderRequest_CN, new HKSModifyOrderRequest(lvTPManager, getRequestXML(lvPath, "HKSModifyOrder_zh_CN.xml", lvEntityID, lvTPAgentID, lvTPAgentPassword, lvBranchID)));
		lvTPManager.registerRequest(RequestName.HKSModifyOrderRequest_VI, new HKSModifyOrderRequest(lvTPManager, getRequestXML(lvPath, "HKSModifyOrder_vi_VN.xml", lvEntityID, lvTPAgentID, lvTPAgentPassword, lvBranchID)));

		lvTPManager.registerRequest(RequestName.HKSPlaceOrderRequest, new HKSPlaceOrderRequest(lvTPManager, getRequestXML(lvPath, "HKSPlaceOrder.xml", lvEntityID, lvTPAgentID, lvTPAgentPassword, lvBranchID)));
		lvTPManager.registerRequest(RequestName.HKSPlaceOrderRequest_TW, new HKSPlaceOrderRequest(lvTPManager, getRequestXML(lvPath, "HKSPlaceOrder_zh_TW.xml", lvEntityID, lvTPAgentID, lvTPAgentPassword, lvBranchID)));
		lvTPManager.registerRequest(RequestName.HKSPlaceOrderRequest_CN, new HKSPlaceOrderRequest(lvTPManager, getRequestXML(lvPath, "HKSPlaceOrder_zh_CN.xml", lvEntityID, lvTPAgentID, lvTPAgentPassword, lvBranchID)));
		lvTPManager.registerRequest(RequestName.HKSPlaceOrderRequest_VI, new HKSPlaceOrderRequest(lvTPManager, getRequestXML(lvPath, "HKSPlaceOrder_vi_VN.xml", lvEntityID, lvTPAgentID, lvTPAgentPassword, lvBranchID)));

	    String lvRequestToDefaultTPServerCountStr = IMain.getProperty("RequestToDefaultTPServerCount");
	    if(lvRequestToDefaultTPServerCountStr != null){
	    	boolean lvTPExternalServerAvaliable = IMain.getProperty("ExternalTPServerAvailable").equals("true") ? true : false;
			int lvRequestToDefaultTPServerCountInt = Integer.parseInt(lvRequestToDefaultTPServerCountStr.toString());
			for(int i = 1; i <= lvRequestToDefaultTPServerCountInt; i ++){
				if(lvTPExternalServerAvaliable){
					//Register the request name if it is not included in external request names,
					if(pFunctionName.indexOf(IMain.getProperty("Request" + i + "_Name")) < 0) {
						lvTPManager.registerRequest(IMain.getProperty("Request" + i + "_Name"), new GenericRequest(lvTPManager, getRequestXML(lvPath, IMain.getProperty("Request" + i + "_XML"), lvEntityID, lvTPAgentID, lvTPAgentPassword, lvBranchID)));
					}
				}else{
					lvTPManager.registerRequest(IMain.getProperty("Request" + i + "_Name"), new GenericRequest(lvTPManager, getRequestXML(lvPath, IMain.getProperty("Request" + i + "_XML"), lvEntityID, lvTPAgentID, lvTPAgentPassword, lvBranchID)));
				}
			}
		}	
	}

   /**
    * This method registration specified external request.
    * @param pPath The path of request xml.
	* @param pEntityID The entity ID.
	* @param pTPAgentID The agent ID of TP.
	* @param pTPAgentPassword The agent password ofgetTPManager TP.
	* @param pBranchID The branch ID.
	* @param pFunctionName The function name.
	* @param pRequestXMLListStr The request xml list string.
    */
   public void startExternalRequestRegistration(String pPath, String pEntityID, String pTPAgentID, String pTPAgentPassword, String pBranchID, String pFunctionName, String pRequestXMLListStr){
	   String lvPath = pPath;
	   String lvEntityID = pEntityID;
       String lvTPAgentID = pTPAgentID;
       String lvTPAgentPassword = pTPAgentPassword;
       String lvBranchID = pBranchID;

       if(pFunctionName != null && pRequestXMLListStr != null){
    	   StringTokenizer lvRequestNameTokenizer = new StringTokenizer(pFunctionName, ",");
    	   StringTokenizer lvRequestXMLTokenizer = new StringTokenizer(pRequestXMLListStr, ",");
    	   if(lvRequestNameTokenizer.countTokens() != lvRequestXMLTokenizer.countTokens()){
    		   System.out.println("Error: The count of Request to External TP is not equal to the one of XML, please check the setting in itrade.ini.");
			   Log.println("Error: The count of Request to External TP is not equal to the one of XML, please check the setting in itrade.ini.", Log.ERROR_LOG);
    	   }else{
				while(lvRequestNameTokenizer.hasMoreTokens()){
					String lvRequestName = (String) lvRequestNameTokenizer.nextToken().trim();
					String lvReqeustXML = (String) lvRequestXMLTokenizer.nextToken().trim();
					ITradeServlet.getTPManager(lvRequestName).registerRequest(lvRequestName,
							   new GenericRequest(ITradeServlet.getTPManager(lvRequestName),
									   getRequestXML(lvPath, lvReqeustXML, lvEntityID, lvTPAgentID, lvTPAgentPassword, lvBranchID)));
				}
				
    	   }
       }
	}

   /**
    * This method to start Server socket.
    * @param pAppletServerPort The Server Port.
    */
   public void startOthers(int pAppletServerPort){
	   final int lvAppletServerPort = pAppletServerPort;
       Thread lvAppletManegerThread = new Thread(){
    	   public void run(){
    		   boolean lvServerSocketInitialized = false;
    		   ServerSocket lvServerSocket = null;

    		   while (true){
	               while (!lvServerSocketInitialized){	
	            	   try{
	            		   lvServerSocket = new ServerSocket(lvAppletServerPort);
	            	   }catch (IOException ex1){
	            		   ex1.printStackTrace();
	            		   Log.println(ex1, Log.ERROR_LOG);
	                   }
	                   lvServerSocketInitialized = true;
	               }
	
	               Socket lvSocket = null;
	               try{
	            	   lvSocket = lvServerSocket.accept();
	            	   lvSocket.setKeepAlive(AppletGateway.getAppletGatewaySocketKeepAlive());
	                   lvSocket.setSoLinger(AppletGateway.getAppletGatewaySocketLinger(), AppletGateway.getAppletGatewaySocketLingerTime());
	               }catch (IOException ex){
	            	   ex.printStackTrace();
	            	   Log.println(ex, Log.ERROR_LOG);
	               }
	               AppletGateway lvGateway = new AppletGateway(lvSocket);
	               lvGateway.start();
    		   }
    	   }
       };
      lvAppletManegerThread.start();
   }

   /**
    * This method start broadcast Manager.
    */
   public void startBroadcastManager(){
	   TPManager lvTPManager = ITradeServlet.getTpManager();
	   //lvTPManager.setBroadcastManager(new TPBroadcastManager());
   }

   /**
    * This method start Login Thread to connect TP.
    */
   public void onTPConnect(){
	   //BEGIN Task #: WL00619 Walter Lau 27 July 2007
       //BEGIN - Task #: TTL-HK-WLCW-00913 - Walter Lau 20090913 Redownload Trade Date and Instrument
       //BEGIN - Task #: TTL-HK-WLCW-00921 - Walter Lau 20090917 Splict TP Connect and Operator Login Theard for fix Redownload issue
	   //BEGIN TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
	   System.out.println("Re Download Time: "+ _pIni.getProperty("ReDownloadDataTime"));
	   String x = IMain.getProperty("ReDownloadDataTime");
	   int lvReDownloadDataTime = Integer.parseInt(IMain.getProperty("ReDownloadDataTime"));
       int lvReDownloadDataDownloadTimes = Integer.parseInt(IMain.getProperty("ReDownloadDataDownloadTimes"));
       boolean lvReDownloadDataEnable = false;
       String lvTemp = _pIni.getProperty("ReDownloadDataEnable");
       if (lvTemp != null)	lvReDownloadDataEnable = (new Boolean(lvTemp)).booleanValue();
	   HKSOperatorLoginThread lvLoginThread = new HKSOperatorLoginThread(ITradeServlet.getTpManager(), Integer.parseInt(IMain.getProperty("NumberOfAvailableGoodTillDate")), IMain.getProperty("Product"), IMain.getProperty("UsingStockCodeFormatMarket"), lvReDownloadDataTime, "F", lvReDownloadDataDownloadTimes, lvReDownloadDataEnable);

	   //END TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
       //END - Task #: TTL-HK-WLCW-00921 - Walter Lau 20090917 Splict TP Connect and Operator Login Theard for fix Redownload issue
       //END - Task #: TTL-HK-WLCW-00913 - Walter Lau 20090913 Redownload Trade Date and Instrument
       //END Task #: WL00619 Walter Lau 27 July 2007
       lvLoginThread.start();
   }

   //BEGIN - Task #: TTL-HK-WLCW-00921 - Walter Lau 20090917 Splict TP Connect and Operator Login Theard for fix Redownload issue
   /**
    * This method start external Login Thread to connet TP.
    */
   public void onExternalTPConnect(){
	   //BEGIN Task #: WL00619 Walter Lau 27 July 2007
	   //BEGIN - Task #: TTL-HK-WLCW-00913 - Walter Lau 20090913 Redownload Trade Date and Instrument
	   //BEGIN TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
	   HKSExternalOperatorLoginThread lvExternalLoginThread = new HKSExternalOperatorLoginThread(ITradeServlet.getTpManager(), Integer.parseInt(IMain.getProperty("NumberOfAvailableGoodTillDate")), IMain.getProperty("Product"), IMain.getProperty("UsingStockCodeFormatMarket"));
	   //END TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
	   //END - Task #: TTL-HK-WLCW-00913 - Walter Lau 20090913 Redownload Trade Date and Instrument
	   //END Task #: WL00619 Walter Lau 27 July 2007
	   lvExternalLoginThread.start();
   }
   //END - Task #: TTL-HK-WLCW-00921 - Walter Lau 20090917 Splict TP Connect and Operator Login Theard for fix Redownload issue
   
   
   //BEGIN TASK: TTL-GZ-XYL-00045 XuYuLong 20091211 [iTrade R5] Plugin redesign and implement
   /**
	 * This method to registration specified request of Plugin.
	 * @param pPath The path of request xml.
	 * @param pEntityID The entity ID.
	 * @param pTPAgentID The agent ID of TP.
	 * @param pTPAgentPassword The agent password of TP.
	 * @param pBranchID The branch ID.
	 * @param pPluginRequestNameListStringToExternalTP Plugin request name list string to external TP.
	 */
   public void startPluginRequestRegistration(String pPath, String pEntityID, String pTPAgentID, String pTPAgentPassword, String pBranchID, String pPluginRequestNameListStringToExternalTP){
	   String lvPath = pPath;
	   String lvEntityID = pEntityID;
	   String lvTPAgentID = pTPAgentID;
	   String lvTPAgentPassword = pTPAgentPassword;
	   String lvBranchID = pBranchID;
	   TPManager lvTPManager = ITradeServlet.getTpManager();
	   
	    String lvPluginRequestToDefaultTPServerCountStr = IMain.getProperty("PluginRequestCount");
	    if(lvPluginRequestToDefaultTPServerCountStr != null){
	    	boolean lvPluginExternalTPServerAvailable = IMain.getProperty("PluginExternalTPServerAvailable").equals("true") ? true : false;
			int lvPluginRequestToDefaultTPServerCountInt = Integer.parseInt(lvPluginRequestToDefaultTPServerCountStr.toString());
			for(int i = 1; i <= lvPluginRequestToDefaultTPServerCountInt; i ++){
				if(lvPluginExternalTPServerAvailable){
					//Register the plug in request name if it is not included in plug in external request names,
					if(pPluginRequestNameListStringToExternalTP.indexOf(IMain.getProperty("PluginRequest" + i + "_Name")) < 0) {
						lvTPManager.registerRequest(IMain.getProperty("PluginRequest" + i + "_Name"), new GenericRequest(lvTPManager, getRequestXML(lvPath, IMain.getProperty("PluginRequest" + i + "_XML"), lvEntityID, lvTPAgentID, lvTPAgentPassword, lvBranchID)));
					}
				}else{
					lvTPManager.registerRequest(IMain.getProperty("PluginRequest" + i + "_Name"), new GenericRequest(lvTPManager, getRequestXML(lvPath, IMain.getProperty("PluginRequest" + i + "_XML"), lvEntityID, lvTPAgentID, lvTPAgentPassword, lvBranchID)));
				}
			}
		}
   }
   
   /**
	 * This method registration external specified request of Plugin.
	 * @param pPath The path of request xml.
	 * @param pEntityID The entity ID.
	 * @param pTPAgentID The agent ID of TP.
	 * @param pTPAgentPassword The agent password of TP.
	 * @param pBranchID The branch ID.
	 * @param pFunctionName The function name.
	 * @param pPluginRequestXMLListStringToExternalTP Request XML List String To External TP.
	 */
   public void startPluginExternalRequestRegistration(String pPath, String pEntityID, String pTPAgentID, String pTPAgentPassword, String pBranchID, String pPluginRequestNameListStringToExternalTP, String pPluginRequestXMLListStringToExternalTP){
	   String lvPath = pPath;
       String lvEntityID = pEntityID;
       String lvTPAgentID = pTPAgentID;
       String lvTPAgentPassword = pTPAgentPassword;
       String lvBranchID = pBranchID;
	   
       if(pPluginRequestNameListStringToExternalTP != null && pPluginRequestXMLListStringToExternalTP != null){
    	   StringTokenizer lvPluginRequestNameTokenizer = new StringTokenizer(pPluginRequestNameListStringToExternalTP, ",");
    	   StringTokenizer lvPluginRequestXMLTokenizer = new StringTokenizer(pPluginRequestXMLListStringToExternalTP, ",");
    	   if(lvPluginRequestNameTokenizer.countTokens() != lvPluginRequestXMLTokenizer.countTokens()){
    		   System.out.println("Error: The count of Request to External TP is not equal to the one of XML, please check the setting in itrade.ini.");
			   Log.println("Error: The count of Request to External TP is not equal to the one of XML, please check the setting in itrade.ini.", Log.ERROR_LOG);
    	   }else{
				while(lvPluginRequestNameTokenizer.hasMoreTokens()){
					String lvRequestName = (String) lvPluginRequestNameTokenizer.nextToken().trim();
					String lvReqeustXML = (String) lvPluginRequestXMLTokenizer.nextToken().trim();
					ITradeServlet.getTPManager(lvRequestName).registerRequest(lvRequestName,
							   new GenericRequest(ITradeServlet.getTPManager(lvRequestName),
									   getRequestXML(lvPath, lvReqeustXML, lvEntityID, lvTPAgentID, lvTPAgentPassword, lvBranchID)));
				}
				
    	   }
       }
   }
   //END TASK: TTL-GZ-XYL-00045 XuYuLong 20091211 [iTrade R5] Plugin redesign and implement
}

