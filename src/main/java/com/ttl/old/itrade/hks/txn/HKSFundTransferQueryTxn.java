package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;

/** The HKSFundTransFerQueryTxn class definition for all method
 * bank account Fund Transfer Query
 * @author not attributable
 *
 */
public class HKSFundTransferQueryTxn
{
    String mvTransferID;
    String mvReplyStatus;
    String mvReplyCode;
    String mvReplyMessage;

    int mvReturnCode;
    String mvReturnMessage;
    String mvErrorCode;
    String mvErrorMessage;
    /**
     * This variable is used to the tp Error
     */
    TPErrorHandling tpError;
    /**
     * Default constructor for HKSFundTransferQueryTxn class
     */
    public HKSFundTransferQueryTxn()
    {
        tpError = new TPErrorHandling();
    }
    /**
     * Default constructor for HKSFundTransferQueryTxn class
     * @param pTransferID the Transfer ID
     */
    public HKSFundTransferQueryTxn(String pTransferID)
    {
        mvTransferID = pTransferID;
        tpError = new TPErrorHandling();
    }
    /**
     * This method process bank account Fund Transfer Query
     */
    public void process()
    {
        Log.println("[ HKSFundTransferSubmitTxn.process() starts [" + mvTransferID + "]", Log.ACCESS_LOG);
        try
        {
            Hashtable lvTxnMap = new Hashtable();
            IMsgXMLNode lvRetNode;
            TPManager ivTPManager = ITradeServlet.getTPManager(RequestName.HKSFundTransferQuery);
            TPBaseRequest lvRequest = ivTPManager.getRequest(RequestName.HKSFundTransferQuery);

            lvTxnMap.put("TXNID", mvTransferID);

            Log.println("[ HKSFundTransferSubmitTxn.process() finished constructing lvTxnMap [" + mvTransferID + "]", Log.ACCESS_LOG);

            lvRetNode = lvRequest.send(lvTxnMap);

            setReturnCode(tpError.checkError(lvRetNode));
            if (mvReturnCode != TPErrorHandling.TP_NORMAL)
            {
                setErrorMessage(tpError.getErrDesc());
                if (mvReturnCode == TPErrorHandling.TP_APP_ERR)
                {
                    setErrorCode(tpError.getErrCode());
                }
            }
            else
            {
                mvReplyStatus = lvRetNode.getChildNode("REPLYSTATUS").getValue();
                mvReplyCode = lvRetNode.getChildNode("REPLYCODE").getValue();
                mvReplyMessage = lvRetNode.getChildNode("REPLYMESSAGE").getValue();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    /**
     * Get method for system return code
     * @return system return code
     */
    public int getReturnCode()
    {
        return mvReturnCode;
    }
    /**
     * Set method for system return code
     * @param pReturnCode the system return code
     */
    public void setReturnCode(int pReturnCode)
    {
        mvReturnCode = pReturnCode;
    }
    /**
     * Get method for system return message
     * @return system return message
     */
    public String getReturnMessage()
    {
        return mvReturnMessage;
    }
    /**
     * Set method for system return message
     * @param pReturnMessage the system return message
     */
    public void setReturnMessage(String pReturnMessage)
    {
        mvReturnMessage = pReturnMessage;
    }
    /**
     * Get method for systme error code
     * @return systme error code
     */
    public String getErrorCode()
    {
        return mvErrorCode;
    }
    /**
     * Set method for systme error code
     * @param pErrorCode the systme error code
     */
    public void setErrorCode(String pErrorCode)
    {
        mvErrorCode = pErrorCode;
    }
    /**
     * Get method for systme error message
     * @return systme error message
     */
    public String getErrorMessage()
    {
        return mvErrorMessage;
    }
    /**
     * Set method for systme error message
     * @param pErrorMessage the systme error message
     */
    public void setErrorMessage(String pErrorMessage)
    {
        mvErrorMessage = pErrorMessage;
    }
    /**
     * Get method for reply status 
     * @return reply status 
     */
    public String getReplyStatus()
    {
        return mvReplyStatus;
    }
    /**
     * Get method for reply code
     * @return reply code
     */
    public String getReplyCode()
    {
        return mvReplyCode;
    }
    /**
     * Get method for reply message
     * @return reply message
     */
    public String getReplyMessage()
    {
        return mvReplyMessage;
    }
}
