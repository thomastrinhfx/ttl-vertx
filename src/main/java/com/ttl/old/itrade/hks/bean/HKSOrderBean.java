package com.ttl.old.itrade.hks.bean;

/**
 * The HKSNewAddPortfolioBean class define variables that to save values for action
 * 
 * @author
 * 
 */
public class HKSOrderBean {

	private int mvOrderBeanID;
	private String mvStockName;
	private String mvDateTime;
	private String mvInstrumentId;
	private String mvShortName;
	private String mvClientRemarks;
	private String mvInvestorGroupId;
	private String mvInvestorClassId;
	private String mvRepOrterGroupId;
	private String mvStatusInternal;
	private String mvCreateTime;
	private String mvReporTime;
	private String mvReporTackTime;
	private String mvOgackTime;
	private String mvTradeTime;
	private String mvLastTradeTime;
	private String mvStopTriggerTime;
	private String mvAllorNothing = "N";
	private String mvHostId;
	private String mvInActive;
	private String mvNotifiedFlag;
	private String mvIsPostExecutedOrder;
	private String mvApprovalTime;
	private String mvSupervisorId;
	private String mvApprovalRemark;
	private String mvHoldConfirmQty;
	private String mvTradeConsideration;
	private String mvPrevtradeConsideration;
	private String mvIsReleased;
	private String mvSupervisorrejected;
	private String mvPendAction;
	private String mvExceededAmt;
	private String mvLastModifiedUserId;
	private String mvOrderID;
	private String mvOrderGroupID;
	private String mvDNSeq;
	private String mvUserID;
	private String mvEntityID;
	private String mvOrderType;
	private String mvStockID;
	private String mvClientID;
	private String mvBS;
	private String mvPrice;
	private String mvQty;
	private String mvPendingPrice;
	private String mvPendingQty;
	private String mvCancelQty;
	private String mvOSQty;
	private String mvFilledQty;
	private String mvFilledPrice;
	private String mvAON;
	private String mvStatus;
	private String mvStatus_Internal;
	private String mvRejectReason;
	private String mvRejectReasonDetail;
	private String mvApprovalReason;
	private String mvInputTime;
	private String mvModifiedTime;
	private String mvBrokerID;
	private String mvOrigin;
	private String mvHedge;
	private String mvShortsell;
	private String mvValidityDate;
	private String mvStopOrderType;
	private String mvStopPrice;
	private String mvStopOrderExpiryDate;
	private String mvIsPriceWarningResubmit;
	private String mvChannelID;
	private String mvBranchID;
	private String mvIsOddLot;
	private String mvIsManualTrade;
	private String mvActivationDate;
	private String mvGoodTillDate;
	private String mvAvgPrice;
	private String mvRemark;
	private String mvContactPhone;
	private String mvGrossAmt;
	private String mvNetAmt;
	private String mvSCRIP;
	private String mvModifyOrderID;
	private String mvCurrencyID;
	private String mvMarketID;
	private String mvInstrumentShortName;
	private String mvModifiedDate;
	private String mvModifiedDateTime;
	private String mvStopType;
	private String mvModifyIcon;
	private String mvCancelIcon;
	private String mvAction;
	private String mvShowModifyIcon;
	private String mvShowCancelIcon;
	private String mvStatusTarget;

	// Begin Task #: Wind 31 May 2009
	private String mvBSValue;
	private String mvPriceValue;
	private String mvStopPriceValue;
	private String mvOrderTypeValue;
	private String mvNetAmtValue;
	private String mvAvgPriceValue;
	private String mvQtyValue;
	private String mvCancelQtyValue;
	private String mvStopTypeValue;
	private int mvLotSize;
	private String mvOSQtyValue;
	// End Task #: Wind 31 May 2009
	// =========================================================================================

	private String matchedDate;
	private String mvBankID;
	private String mvBankACID;

	/**
	 * This method returns the id
	 * 
	 * @return the id.
	 */
	public String getMvStatusTarget() {
		return mvStatusTarget;
	}

	/**
	 * This method sets the id
	 * 
	 * @param pId The id.
	 */
	public void setMvStatusTarget(String pStatusTarget) {
		mvStatusTarget = pStatusTarget;
	}

	/**
	 * This method returns the out standing quantity value
	 * 
	 * @return the out standing quantity value.
	 */
	public String getMvOSQtyValue() {
		return mvOSQtyValue;
	}

	/**
	 * This method sets the out standing quantity value
	 * 
	 * @param pOSQtyValue The out standing quantity value.
	 */
	public void setMvOSQtyValue(String pOSQtyValue) {
		mvOSQtyValue = pOSQtyValue;
	}

	/**
	 * This method returns the stop price value
	 * 
	 * @return the stop price value.
	 */
	public String getMvStopPriceValue() {
		return mvStopPriceValue;
	}

	/**
	 * This method sets the stop price value
	 * 
	 * @param pStopPriceValue The stop price value.
	 */
	public void setMvStopPriceValue(String pStopPriceValue) {
		mvStopPriceValue = pStopPriceValue;
	}

	/**
	 * This method returns the lot size
	 * 
	 * @return the lot size.
	 */
	public int getMvLotSize() {
		return mvLotSize;
	}

	/**
	 * This method sets the lot size
	 * 
	 * @param pLotSize The lot size.
	 */
	public void setMvLotSize(int pLotSize) {
		mvLotSize = pLotSize;
	}

	/**
	 * This method returns the stop type value
	 * 
	 * @return the stop type value.
	 */
	public String getMvStopTypeValue() {
		return mvStopTypeValue;
	}

	/**
	 * This method sets the stop type value
	 * 
	 * @param pStopTypeValue The stop type value.
	 */
	public void setMvStopTypeValue(String pStopTypeValue) {
		mvStopTypeValue = pStopTypeValue;
	}

	/**
	 * This method returns the cancel quantity value
	 * 
	 * @return the cancel quantity value.
	 */
	public String getMvCancelQtyValue() {
		return mvCancelQtyValue;
	}

	/**
	 * This method sets the cancel quantity value
	 * 
	 * @param pCancelQtyValue The cancel quantity value.
	 */
	public void setMvCancelQtyValue(String pCancelQtyValue) {
		mvCancelQtyValue = pCancelQtyValue;
	}

	/**
	 * This method returns the quantity value
	 * 
	 * @return the quantity value.
	 */
	public String getMvQtyValue() {
		return mvQtyValue;
	}

	/**
	 * This method sets the quantity value
	 * 
	 * @param pQtyValue The quantity value.
	 */
	public void setMvQtyValue(String pQtyValue) {
		mvQtyValue = pQtyValue;
	}

	/**
	 * This method returns the B(Buy) or S(Sell)
	 * 
	 * @return the B(Buy) or S(Sell). [0] B is Buy. [1] S is sell.
	 */
	public String getMvBSValue() {
		return mvBSValue;
	}

	/**
	 * This method sets the B(Buy) or S(Sell)
	 * 
	 * @param pBSValue The B(Buy) or S(Sell).
	 */
	public void setMvBSValue(String pBSValue) {
		mvBSValue = pBSValue;
	}

	/**
	 * This method returns the price value
	 * 
	 * @return the price value.
	 */
	public String getMvPriceValue() {
		return mvPriceValue;
	}

	/**
	 * This method sets the price value
	 * 
	 * @param pPriceValue The price value.
	 */
	public void setMvPriceValue(String pPriceValue) {
		mvPriceValue = pPriceValue;
	}

	/**
	 * This method returns the order type value
	 * 
	 * @return the order type value.
	 */
	public String getMvOrderTypeValue() {
		return mvOrderTypeValue;
	}

	/**
	 * This method sets the order type value
	 * 
	 * @param pOrderTypeValue The order type value.
	 */
	public void setMvOrderTypeValue(String pOrderTypeValue) {
		mvOrderTypeValue = pOrderTypeValue;
	}

	/**
	 * This method returns the net amount value
	 * 
	 * @return the net amount value.
	 */
	public String getMvNetAmtValue() {
		return mvNetAmtValue;
	}

	/**
	 * This method sets the net amount value
	 * 
	 * @param pNetAmtValue The net amount value.
	 */
	public void setMvNetAmtValue(String pNetAmtValue) {
		mvNetAmtValue = pNetAmtValue;
	}

	/**
	 * This method returns the average price value
	 * 
	 * @return the average price value.
	 */
	public String getMvAvgPriceValue() {
		return mvAvgPriceValue;
	}

	/**
	 * This method sets the average price value
	 * 
	 * @param pAvgPriceValue The average price value.
	 */
	public void setMvAvgPriceValue(String pAvgPriceValue) {
		mvAvgPriceValue = pAvgPriceValue;
	}

	/**
	 * This method returns the action
	 * 
	 * @return the action.
	 */
	public String getMvAction() {
		return mvAction;
	}

	/**
	 * This method sets the action
	 * 
	 * @param pAction The action.
	 */
	public void setMvAction(String pAction) {
		mvAction = pAction;
	}

	/**
	 * This method returns the show modify icon
	 * 
	 * @return the show modify icon.
	 */
	public String getMvShowModifyIcon() {
		return mvShowModifyIcon;
	}

	/**
	 * This method sets the show modify icon
	 * 
	 * @param pShowModifyIcon The show modify icon.
	 */
	public void setMvShowModifyIcon(String pShowModifyIcon) {
		mvShowModifyIcon = pShowModifyIcon;
	}

	/**
	 * This method returns the show cancel icon
	 * 
	 * @return the cancel icon.
	 */
	public String getMvShowCancelIcon() {
		return mvShowCancelIcon;
	}

	/**
	 * This method sets the show cancel icon
	 * 
	 * @param pShowCancelIcon The show cancel icon.
	 */
	public void setMvShowCancelIcon(String pShowCancelIcon) {
		mvShowCancelIcon = pShowCancelIcon;
	}

	/**
	 * This method returns the modify icon
	 * 
	 * @return the show modify icon.
	 */
	public String getMvModifyIcon() {
		return mvModifyIcon;
	}

	/**
	 * This method sets the modify icon
	 * 
	 * @param pModifyIcon The modify icon.
	 */
	public void setMvModifyIcon(String pModifyIcon) {
		mvModifyIcon = pModifyIcon;
	}

	/**
	 * This method returns the stop type
	 * 
	 * @return the stop type.
	 */
	public String getMvStopType() {
		return mvStopType;
	}

	/**
	 * This method sets the stop type
	 * 
	 * @param pStopType The stop type.
	 */

	public void setMvStopType(String pStopType) {
		mvStopType = pStopType;
	}

	/**
	 * This method returns the cancel icon
	 * 
	 * @return the cancel icon.
	 */
	public String getMvCancelIcon() {
		return mvCancelIcon;
	}

	/**
	 * This method sets the cancel icon
	 * 
	 * @param pCancelIcon The cancel icon.
	 */
	public void setMvCancelIcon(String pCancelIcon) {
		mvCancelIcon = pCancelIcon;
	}

	/**
	 * This method returns the order bean id
	 * 
	 * @return the order bean id.
	 */
	public int getMvOrderBeanID() {
		return mvOrderBeanID;
	}

	/**
	 * This method sets the order bean id
	 * 
	 * @param pOrderBeanID The order bean id.
	 */
	public void setMvOrderBeanID(int pOrderBeanID) {
		mvOrderBeanID = pOrderBeanID;
	}

	/**
	 * This method returns the date time
	 * 
	 * @return the date time.
	 */
	public String getMvDateTime() {
		return mvDateTime;
	}

	/**
	 * This method sets the date time
	 * 
	 * @param pDateTime The date time.
	 */
	public void setMvDateTime(String pDateTime) {
		mvDateTime = pDateTime;
	}

	/**
	 * This method returns the stock name
	 * 
	 * @return the stock name.
	 */
	public String getMvStockName() {
		return mvStockName;
	}

	/**
	 * This method sets the stock name
	 * 
	 * @param pStockName The stock name.
	 */
	public void setMvStockName(String pStockName) {
		mvStockName = pStockName;
	}

	/**
	 * This method returns the instrument id
	 * 
	 * @return the instrument id.
	 */
	public String getMvInstrumentId() {
		return mvInstrumentId;
	}

	/**
	 * This method sets the instrument id
	 * 
	 * @param pInstrumentId The instrument id.
	 */
	public void setMvInstrumentId(String pInstrumentId) {
		mvInstrumentId = pInstrumentId;
	}

	/**
	 * This method returns the short name
	 * 
	 * @return the short name.
	 */
	public String getMvShortName() {
		return mvShortName;
	}

	/**
	 * This method sets the short name
	 * 
	 * @param pShortName The short name.
	 */
	public void setMvShortName(String pShortName) {
		mvShortName = pShortName;
	}

	/**
	 * This method returns the client remarks
	 * 
	 * @return the client remarks.
	 */
	public String getMvClientRemarks() {
		return mvClientRemarks;
	}

	/**
	 * This method sets the client remarks
	 * 
	 * @param pClientRemarks The client remarks.
	 */
	public void setMvClientRemarks(String pClientRemarks) {
		mvClientRemarks = pClientRemarks;
	}

	/**
	 * This method returns the investor group id
	 * 
	 * @return the investor group id.
	 */
	public String getMvInvestorGroupId() {
		return mvInvestorGroupId;
	}

	/**
	 * This method sets the investor group id
	 * 
	 * @param pInvestorGroupId The investor group id.
	 */
	public void setMvInvestorGroupId(String pInvestorGroupId) {
		mvInvestorGroupId = pInvestorGroupId;
	}

	/**
	 * This method returns the investor class id
	 * 
	 * @return the investor class id.
	 */
	public String getMvInvestorClassId() {
		return mvInvestorClassId;
	}

	/**
	 * This method sets the investor class id
	 * 
	 * @param pInvestorClassId The investor class id.
	 */
	public void setMvInvestorClassId(String pInvestorClassId) {
		mvInvestorClassId = pInvestorClassId;
	}

	/**
	 * This method returns the reporter group id
	 * 
	 * @return the reporter group id.
	 */
	public String getMvRepOrterGroupId() {
		return mvRepOrterGroupId;
	}

	/**
	 * This method sets the reporter group id
	 * 
	 * @param pRepOrterGroupId The reporter group id.
	 */
	public void setMvRepOrterGroupId(String pRepOrterGroupId) {
		mvRepOrterGroupId = pRepOrterGroupId;
	}

	/**
	 * This method returns the status internal
	 * 
	 * @return the status internal.
	 */
	public String getMvStatusInternal() {
		return mvStatusInternal;
	}

	/**
	 * This method sets the status internal
	 * 
	 * @param pStatusInternal The status internal.
	 */
	public void setMvStatusInternal(String pStatusInternal) {
		mvStatusInternal = pStatusInternal;
	}

	/**
	 * This method returns the create time
	 * 
	 * @return the create time.
	 */
	public String getMvCreateTime() {
		return mvCreateTime;
	}

	/**
	 * This method sets the create time
	 * 
	 * @param pCreateTime The create time.
	 */
	public void setMvCreateTime(String pCreateTime) {
		mvCreateTime = pCreateTime;
	}

	/**
	 * This method returns the report time
	 * 
	 * @return the report time.
	 */
	public String getMvReporTime() {
		return mvReporTime;
	}

	/**
	 * This method sets the report time
	 * 
	 * @param pReporTime The report time.
	 */
	public void setMvReporTime(String pReporTime) {
		mvReporTime = pReporTime;
	}

	/**
	 * This method returns the report tack time
	 * 
	 * @return the report tack time.
	 */
	public String getMvReporTackTime() {
		return mvReporTackTime;
	}

	/**
	 * This method sets the report tack time
	 * 
	 * @param pReporTackTime The report tack time.
	 */
	public void setMvReporTackTime(String pReporTackTime) {
		mvReporTackTime = pReporTackTime;
	}

	/**
	 * This method returns the ogack time
	 * 
	 * @return the ogack time.
	 */

	public String getMvOgackTime() {
		return mvOgackTime;
	}

	/**
	 * This method sets the ogack time
	 * 
	 * @param pOgackTime The ogack time.
	 */

	public void setMvOgackTime(String pOgackTime) {
		mvOgackTime = pOgackTime;
	}

	/**
	 * This method returns the trade time
	 * 
	 * @return the trade time.
	 */
	public String getMvTradeTime() {
		return mvTradeTime;
	}

	/**
	 * This method sets the trade time
	 * 
	 * @param pTradeTime The trade time.
	 */
	public void setMvTradeTime(String pTradeTime) {
		mvTradeTime = pTradeTime;
	}

	/**
	 * This method returns the last trade time
	 * 
	 * @return the last trade time.
	 */
	public String getMvLastTradeTime() {
		return mvLastTradeTime;
	}

	/**
	 * This method sets the last trade time
	 * 
	 * @param pTradeTime The last trade time.
	 */
	public void setMvLastTradeTime(String pLastTradeTime) {
		mvLastTradeTime = pLastTradeTime;
	}

	/**
	 * This method returns the stop trigger time
	 * 
	 * @return the stop trigger time.
	 */
	public String getMvStopTriggerTime() {
		return mvStopTriggerTime;
	}

	/**
	 * This method sets the stop trigger time
	 * 
	 * @param pStopTriggerTime The stop trigger time.
	 */
	public void setMvStopTriggerTime(String pStopTriggerTime) {
		mvStopTriggerTime = pStopTriggerTime;
	}

	/**
	 * This method returns the all or nothing
	 * 
	 * @return the all or nothing.
	 */
	public String getMvAllorNothing() {
		return mvAllorNothing;
	}

	/**
	 * This method sets the all or nothing
	 * 
	 * @param pAllorNothing The all or nothing.
	 */
	public void setMvAllorNothing(String pAllorNothing) {
		mvAllorNothing = pAllorNothing;
	}

	/**
	 * This method returns the host id
	 * 
	 * @return the host id.
	 */
	public String getMvHostId() {
		return mvHostId;
	}

	/**
	 * This method sets the host id
	 * 
	 * @param pHostId The host id.
	 */
	public void setMvHostId(String pHostId) {
		mvHostId = pHostId;
	}

	/**
	 * This method returns the in active
	 * 
	 * @return the in active.
	 */
	public String getMvInActive() {
		return mvInActive;
	}

	/**
	 * This method sets the in active
	 * 
	 * @param pInActive The in active.
	 */
	public void setMvInActive(String pInActive) {
		mvInActive = pInActive;
	}

	/**
	 * This method returns the notified flag.
	 * 
	 * @return the notified flag.
	 */
	public String getMvNotifiedFlag() {
		return mvNotifiedFlag;
	}

	/**
	 * This method sets the notified flag
	 * 
	 * @param pNotifiedFlag The notified flag.
	 */
	public void setMvNotifiedFlag(String pNotifiedFlag) {
		mvNotifiedFlag = pNotifiedFlag;
	}

	/**
	 * This method returns the is post executed order
	 * 
	 * @return the is post executed order.
	 */
	public String getMvIsPostExecutedOrder() {
		return mvIsPostExecutedOrder;
	}

	/**
	 * This method sets the is post executed order
	 * 
	 * @param pIsPostExecutedOrder The is post executed order.
	 */
	public void setMvIsPostExecutedOrder(String pIsPostExecutedOrder) {
		mvIsPostExecutedOrder = pIsPostExecutedOrder;
	}

	/**
	 * This method returns the approval time
	 * 
	 * @return the approval time.
	 */
	public String getMvApprovalTime() {
		return mvApprovalTime;
	}

	/**
	 * This method sets the approval time
	 * 
	 * @param pApprovalTime The approval time.
	 */
	public void setMvApprovalTime(String pApprovalTime) {
		mvApprovalTime = pApprovalTime;
	}

	/**
	 * This method returns the super visor id
	 * 
	 * @return the super visor id.
	 */
	public String getMvSupervisorId() {
		return mvSupervisorId;
	}

	/**
	 * This method sets the super visor id
	 * 
	 * @param pSupervisorId The super visor id.
	 */
	public void setMvSupervisorId(String pSupervisorId) {
		mvSupervisorId = pSupervisorId;
	}

	/**
	 * This method returns the approval remark
	 * 
	 * @return the approval remark.
	 */
	public String getMvApprovalRemark() {
		return mvApprovalRemark;
	}

	/**
	 * This method sets the approval remark
	 * 
	 * @param pApprovalRemark The approval remark.
	 */
	public void setMvApprovalRemark(String pApprovalRemark) {
		mvApprovalRemark = pApprovalRemark;
	}

	/**
	 * This method returns the hold confirm quantity
	 * 
	 * @return the hold confirm quantity.
	 */
	public String getMvHoldConfirmQty() {
		return mvHoldConfirmQty;
	}

	/**
	 * This method sets the hold confirm quantity
	 * 
	 * @param pHoldConfirmQty The hold confirm quantity.
	 */
	public void setMvHoldConfirmQty(String pHoldConfirmQty) {
		mvHoldConfirmQty = pHoldConfirmQty;
	}

	/**
	 * This method returns the trade consideration
	 * 
	 * @return the trade consideration.
	 */
	public String getMvTradeConsideration() {
		return mvTradeConsideration;
	}

	/**
	 * This method sets the trade consideration
	 * 
	 * @param pTradeConsideration The trade consideration.
	 */
	public void setMvTradeConsideration(String pTradeConsideration) {
		mvTradeConsideration = pTradeConsideration;
	}

	/**
	 * This method returns the prevtrade consideration
	 * 
	 * @return the prevtrade consideration.
	 */
	public String getMvPrevtradeConsideration() {
		return mvPrevtradeConsideration;
	}

	/**
	 * This method sets the prevtrade consideration
	 * 
	 * @param pPrevtradeConsideration The prevtrade consideration.
	 */
	public void setMvPrevtradeConsideration(String pPrevtradeConsideration) {
		mvPrevtradeConsideration = pPrevtradeConsideration;
	}

	/**
	 * This method returns the is released
	 * 
	 * @return the is released.
	 */
	public String getMvIsReleased() {
		return mvIsReleased;
	}

	/**
	 * This method sets the is released
	 * 
	 * @param pIsReleased The is released.
	 */
	public void setMvIsReleased(String pIsReleased) {
		mvIsReleased = pIsReleased;
	}

	/**
	 * This method returns the super visor rejected
	 * 
	 * @return the super visor rejected.
	 */
	public String getMvSupervisorrejected() {
		return mvSupervisorrejected;
	}

	/**
	 * This method sets the super visor rejected
	 * 
	 * @param pSupervisorrejected The super visor rejected.
	 */
	public void setMvSupervisorrejected(String pSupervisorrejected) {
		mvSupervisorrejected = pSupervisorrejected;
	}

	/**
	 * This method returns the pend action
	 * 
	 * @return the pend action.
	 */
	public String getMvPendAction() {
		return mvPendAction;
	}

	/**
	 * This method sets the pend action
	 * 
	 * @param pPendAction The pend action.
	 */
	public void setMvPendAction(String pPendAction) {
		mvPendAction = pPendAction;
	}

	/**
	 * This method returns the exceeded amount
	 * 
	 * @return the exceeded amount.
	 */
	public String getMvExceededAmt() {
		return mvExceededAmt;
	}

	/**
	 * This method sets the exceeded amount
	 * 
	 * @param pExceededAmt The exceeded amount.
	 */
	public void setMvExceededAmt(String pExceededAmt) {
		mvExceededAmt = pExceededAmt;
	}

	/**
	 * This method returns the last modified user id
	 * 
	 * @return the last modified user id.
	 */
	public String getMvLastModifiedUserId() {
		return mvLastModifiedUserId;
	}

	/**
	 * This method sets the last modified user id
	 * 
	 * @param pLastModifiedUserId The last modified user id.
	 */
	public void setMvLastModifiedUserId(String pLastModifiedUserId) {
		mvLastModifiedUserId = pLastModifiedUserId;
	}

	/**
	 * This method returns the order id
	 * 
	 * @return the order id.
	 */
	public String getMvOrderID() {
		return mvOrderID;
	}

	/**
	 * This method sets the order id
	 * 
	 * @param pOrderID The order id.
	 */
	public void setMvOrderID(String pOrderID) {
		mvOrderID = pOrderID;
	}

	/**
	 * This method returns the order group id
	 * 
	 * @return the order group id.
	 */
	public String getMvOrderGroupID() {
		return mvOrderGroupID;
	}

	/**
	 * This method sets the order group id
	 * 
	 * @param pOrderGroupID The group order id.
	 */
	public void setMvOrderGroupID(String pOrderGroupID) {
		mvOrderGroupID = pOrderGroupID;
	}

	/**
	 * This method returns the DNSeq
	 * 
	 * @return the DNSeq.
	 */
	public String getMvDNSeq() {
		return mvDNSeq;
	}

	/**
	 * This method sets the DNSeq
	 * 
	 * @param pDNSeq The DNSeq.
	 */
	public void setMvDNSeq(String pDNSeq) {
		mvDNSeq = pDNSeq;
	}

	/**
	 * This method returns the user id
	 * 
	 * @return the user id.
	 */
	public String getMvUserID() {
		return mvUserID;
	}

	/**
	 * This method sets the user id
	 * 
	 * @param pUserID The user id.
	 */
	public void setMvUserID(String pUserID) {
		mvUserID = pUserID;
	}

	/**
	 * This method returns the entity id
	 * 
	 * @return the entity id.
	 */
	public String getMvEntityID() {
		return mvEntityID;
	}

	/**
	 * This method sets the entity id
	 * 
	 * @param pEntityID The entity id.
	 */
	public void setMvEntityID(String pEntityID) {
		mvEntityID = pEntityID;
	}

	/**
	 * This method returns the order type
	 * 
	 * @return the order type.
	 */
	public String getMvOrderType() {
		return mvOrderType;
	}

	/**
	 * This method sets the order type
	 * 
	 * @param pOrderType The order type.
	 */
	public void setMvOrderType(String pOrderType) {
		mvOrderType = pOrderType;
	}

	/**
	 * This method returns the stock id
	 * 
	 * @return the stock id.
	 */
	public String getMvStockID() {
		return mvStockID;
	}

	/**
	 * This method sets the stock id
	 * 
	 * @param pStockID The stock id.
	 */
	public void setMvStockID(String pStockID) {
		mvStockID = pStockID;
	}

	/**
	 * This method returns the client id
	 * 
	 * @return the client id.
	 */
	public String getMvClientID() {
		return mvClientID;
	}

	/**
	 * This method sets the client id
	 * 
	 * @param pClientID The client id.
	 */
	public void setMvClientID(String pClientID) {
		mvClientID = pClientID;
	}

	/**
	 * This method returns the B(Buy) or S(Sell)
	 * 
	 * @return the B(Buy) or S(Sell). [0] B is Buy. [1] S is Sell.
	 */
	public String getMvBS() {
		return mvBS;
	}

	/**
	 * This method sets the B(Buy) or S(Sell)
	 * 
	 * @param pBS The B(Buy) or S(Sell).
	 */
	public void setMvBS(String pBS) {
		mvBS = pBS;
	}

	/**
	 * This method returns the price
	 * 
	 * @return the price.
	 */
	public String getMvPrice() {
		return mvPrice;
	}

	/**
	 * This method sets the price
	 * 
	 * @param pPrice The price.
	 */
	public void setMvPrice(String pPrice) {
		mvPrice = pPrice;
	}

	/**
	 * This method returns the quantity
	 * 
	 * @return the quantity.
	 */
	public String getMvQty() {
		return mvQty;
	}

	/**
	 * This method sets the quantity
	 * 
	 * @param pQty The quantity.
	 */
	public void setMvQty(String pQty) {
		mvQty = pQty;
	}

	/**
	 * This method returns the pending price
	 * 
	 * @return the pending price.
	 */
	public String getMvPendingPrice() {
		return mvPendingPrice;
	}

	/**
	 * This method sets the pending price
	 * 
	 * @param pPendingPrice The pending price.
	 */
	public void setMvPendingPrice(String pPendingPrice) {
		mvPendingPrice = pPendingPrice;
	}

	/**
	 * This method returns the pending quantity
	 * 
	 * @return the pending quantity.
	 */
	public String getMvPendingQty() {
		return mvPendingQty;
	}

	/**
	 * This method sets the pending quantity
	 * 
	 * @param pPendingQty The pending quantity.
	 */
	public void setMvPendingQty(String pPendingQty) {
		mvPendingQty = pPendingQty;
	}

	/**
	 * This method returns the cancel quantity
	 * 
	 * @return the cancel quantity.
	 */
	public String getMvCancelQty() {
		return mvCancelQty;
	}

	/**
	 * This method sets the cancel quantity
	 * 
	 * @param pCancelQty The cancel quantity.
	 */
	public void setMvCancelQty(String pCancelQty) {
		mvCancelQty = pCancelQty;
	}

	/**
	 * This method returns the out standing quantity
	 * 
	 * @return the out standing quantity.
	 */
	public String getMvOSQty() {
		return mvOSQty;
	}

	/**
	 * This method sets the out standing quantity
	 * 
	 * @param pOSQty The out standing quantity.
	 */
	public void setMvOSQty(String pOSQty) {
		mvOSQty = pOSQty;
	}

	/**
	 * This method returns the filled quantity
	 * 
	 * @return the filled quantity.
	 */
	public String getMvFilledQty() {
		return mvFilledQty;
	}

	/**
	 * This method sets the filled quantity
	 * 
	 * @param pFilledQty The filled quantity.
	 */
	public void setMvFilledQty(String pFilledQty) {
		mvFilledQty = pFilledQty;
	}

	/**
	 * This method returns the all or nothing
	 * 
	 * @return the all or nothing.
	 */
	public String getMvAON() {
		return mvAON;
	}

	/**
	 * This method sets the all or nothing
	 * 
	 * @param pAON The all or nothing.
	 */
	public void setMvAON(String pAON) {
		mvAON = pAON;
	}

	/**
	 * This method returns the status
	 * 
	 * @return the status.
	 */
	public String getMvStatus() {
		return mvStatus;
	}

	/**
	 * This method sets the status
	 * 
	 * @param pStatus The status.
	 */
	public void setMvStatus(String pStatus) {
		mvStatus = pStatus;
	}

	/**
	 * This method returns the status internal
	 * 
	 * @return the status internal.
	 */
	public String getMvStatus_Internal() {
		return mvStatus_Internal;
	}

	/**
	 * This method sets the status internal
	 * 
	 * @param pStatus_Internal The status internal.
	 */
	public void setMvStatus_Internal(String pStatus_Internal) {
		mvStatus_Internal = pStatus_Internal;
	}

	/**
	 * This method returns the reject reason
	 * 
	 * @return the reject reason.
	 */
	public String getMvRejectReason() {
		return mvRejectReason;
	}

	/**
	 * This method sets the reject reason
	 * 
	 * @param pRejectReason The reject reason.
	 */
	public void setMvRejectReason(String pRejectReason) {
		mvRejectReason = pRejectReason;
	}

	/**
	 * This method returns the approval reason
	 * 
	 * @return the approval reason.
	 */
	public String getMvApprovalReason() {
		return mvApprovalReason;
	}

	/**
	 * This method sets the approval reason
	 * 
	 * @param pApprovalReason The approval reason.
	 */
	public void setMvApprovalReason(String pApprovalReason) {
		mvApprovalReason = pApprovalReason;
	}

	/**
	 * This method returns the input time
	 * 
	 * @return the input time.
	 */
	public String getMvInputTime() {
		return mvInputTime;
	}

	/**
	 * This method sets the input time
	 * 
	 * @param pInputTime The input time.
	 */
	public void setMvInputTime(String pInputTime) {
		mvInputTime = pInputTime;
	}

	/**
	 * This method returns the modified time
	 * 
	 * @return the modified time.
	 */
	public String getMvModifiedTime() {
		return mvModifiedTime;
	}

	/**
	 * This method sets the modified time
	 * 
	 * @param pModifiedTime The modified time.
	 */
	public void setMvModifiedTime(String pModifiedTime) {
		mvModifiedTime = pModifiedTime;
	}

	/**
	 * This method returns the broker id
	 * 
	 * @return the broker id.
	 */
	public String getMvBrokerID() {
		return mvBrokerID;
	}

	/**
	 * This method sets the broker id
	 * 
	 * @param pBrokerID The broker id.
	 */
	public void setMvBrokerID(String pBrokerID) {
		mvBrokerID = pBrokerID;
	}

	/**
	 * This method returns the origin
	 * 
	 * @return the origin.
	 */
	public String getMvOrigin() {
		return mvOrigin;
	}

	/**
	 * This method sets the origin
	 * 
	 * @param pOrigin The origin.
	 */
	public void setMvOrigin(String pOrigin) {
		mvOrigin = pOrigin;
	}

	/**
	 * This method returns the hedge
	 * 
	 * @return the hedge.
	 */
	public String getMvHedge() {
		return mvHedge;
	}

	/**
	 * This method sets the hedge
	 * 
	 * @param pHedge The hedge.
	 */
	public void setMvHedge(String pHedge) {
		mvHedge = pHedge;
	}

	/**
	 * This method returns the short sell
	 * 
	 * @return the short sell.
	 */
	public String getMvShortsell() {
		return mvShortsell;
	}

	/**
	 * This method sets the short sell
	 * 
	 * @param pShortsell The short sell.
	 */
	public void setMvShortsell(String pShortsell) {
		mvShortsell = pShortsell;
	}

	/**
	 * This method returns the validity date
	 * 
	 * @return the validity date.
	 */
	public String getMvValidityDate() {
		return mvValidityDate;
	}

	/**
	 * This method sets the validity date
	 * 
	 * @param pValidityDate The validity date.
	 */
	public void setMvValidityDate(String pValidityDate) {
		mvValidityDate = pValidityDate;
	}

	/**
	 * This method returns the stop order type
	 * 
	 * @return the stop order type.
	 */
	public String getMvStopOrderType() {
		return mvStopOrderType;
	}

	/**
	 * This method sets the stop order type
	 * 
	 * @param pStopOrderType The stop order type.
	 */
	public void setMvStopOrderType(String pStopOrderType) {
		mvStopOrderType = pStopOrderType;
	}

	/**
	 * This method returns the stop price
	 * 
	 * @return the stop price.
	 */
	public String getMvStopPrice() {
		return mvStopPrice;
	}

	/**
	 * This method sets the stop price
	 * 
	 * @param pStopPrice The stop price.
	 */
	public void setMvStopPrice(String pStopPrice) {
		mvStopPrice = pStopPrice;
	}

	/**
	 * This method returns the stop order expiry date
	 * 
	 * @return the stop order expiry date.
	 */
	public String getMvStopOrderExpiryDate() {
		return mvStopOrderExpiryDate;
	}

	/**
	 * This method sets the stop order expiry date
	 * 
	 * @param pStopOrderExpiryDate The stop order expiry date.
	 */
	public void setMvStopOrderExpiryDate(String pStopOrderExpiryDate) {
		mvStopOrderExpiryDate = pStopOrderExpiryDate;
	}

	/**
	 * This method returns the is price warning resubmit
	 * 
	 * @return the is price warning resubmit.
	 */
	public String getMvIsPriceWarningResubmit() {
		return mvIsPriceWarningResubmit;
	}

	/**
	 * This method sets the is price warning resubmit
	 * 
	 * @param pIsPriceWarningResubmit The is price warning resubmit.
	 */
	public void setMvIsPriceWarningResubmit(String pIsPriceWarningResubmit) {
		mvIsPriceWarningResubmit = pIsPriceWarningResubmit;
	}

	/**
	 * This method returns the channel id
	 * 
	 * @return the channel id.
	 */
	public String getMvChannelID() {
		return mvChannelID;
	}

	/**
	 * This method sets the channel id
	 * 
	 * @param pChannelID The channel id.
	 */
	public void setMvChannelID(String pChannelID) {
		mvChannelID = pChannelID;
	}

	/**
	 * This method returns the branch id
	 * 
	 * @return the branch id.
	 */
	public String getMvBranchID() {
		return mvBranchID;
	}

	/**
	 * This method sets the branch id
	 * 
	 * @param pBranchID The branch id.
	 */
	public void setMvBranchID(String pBranchID) {
		mvBranchID = pBranchID;
	}

	/**
	 * This method returns the is odd lot
	 * 
	 * @return the is odd lot.
	 */
	public String getMvIsOddLot() {
		return mvIsOddLot;
	}

	/**
	 * This method sets the is manual trade odd lot
	 * 
	 * @param pIsOddLot The is manual trade odd lot.
	 */
	public void setMvIsOddLot(String pIsOddLot) {
		mvIsOddLot = pIsOddLot;
	}

	/**
	 * This method returns the is manual trade
	 * 
	 * @return the is manual trade.
	 */
	public String getMvIsManualTrade() {
		return mvIsManualTrade;
	}

	/**
	 * This method sets the is manual trade
	 * 
	 * @param pIsManualTrade The is manual trade.
	 */
	public void setMvIsManualTrade(String pIsManualTrade) {
		mvIsManualTrade = pIsManualTrade;
	}

	/**
	 * This method returns the activation date
	 * 
	 * @return the activation date.
	 */
	public String getMvActivationDate() {
		return mvActivationDate;
	}

	/**
	 * This method sets the activation date
	 * 
	 * @param pActivationDate The activation date.
	 */
	public void setMvActivationDate(String pActivationDate) {
		mvActivationDate = pActivationDate;
	}

	/**
	 * This method returns the good till date
	 * 
	 * @return the good till date.
	 */
	public String getMvGoodTillDate() {
		return mvGoodTillDate;
	}

	/**
	 * This method sets the good till date
	 * 
	 * @param pGoodTillDate The good till date.
	 */
	public void setMvGoodTillDate(String pGoodTillDate) {
		mvGoodTillDate = pGoodTillDate;
	}

	/**
	 * This method returns the average price
	 * 
	 * @return the average price.
	 */
	public String getMvAvgPrice() {
		return mvAvgPrice;
	}

	/**
	 * This method sets the average price
	 * 
	 * @param pAvgPrice The average price.
	 */
	public void setMvAvgPrice(String pAvgPrice) {
		mvAvgPrice = pAvgPrice;
	}

	/**
	 * This method returns the remark
	 * 
	 * @return the remark.
	 */
	public String getMvRemark() {
		return mvRemark;
	}

	/**
	 * This method sets the remark
	 * 
	 * @param pRemark The remark.
	 */
	public void setMvRemark(String pRemark) {
		mvRemark = pRemark;
	}

	/**
	 * This method returns the contact phone
	 * 
	 * @return the contact phone.
	 */
	public String getMvContactPhone() {
		return mvContactPhone;
	}

	/**
	 * This method sets the contact phone
	 * 
	 * @param pContactPhone The contact phone.
	 */
	public void setMvContactPhone(String pContactPhone) {
		mvContactPhone = pContactPhone;
	}

	/**
	 * This method returns the gross amount
	 * 
	 * @return the gross amount.
	 */
	public String getMvGrossAmt() {
		return mvGrossAmt;
	}

	/**
	 * This method sets the gross amount
	 * 
	 * @param pGrossAmt The gross amount.
	 */
	public void setMvGrossAmt(String pGrossAmt) {
		mvGrossAmt = pGrossAmt;
	}

	/**
	 * This method returns the net amount
	 * 
	 * @return the net amount.
	 */
	public String getMvNetAmt() {
		return mvNetAmt;
	}

	/**
	 * This method sets the net amount
	 * 
	 * @param pNetAmt The net amount.
	 */
	public void setMvNetAmt(String pNetAmt) {
		mvNetAmt = pNetAmt;
	}

	/**
	 * This method returns the scrip
	 * 
	 * @return the scrip.
	 */
	public String getMvSCRIP() {
		return mvSCRIP;
	}

	/**
	 * This method sets the scrip
	 * 
	 * @param pSCRIP The scrip.
	 */
	public void setMvSCRIP(String pSCRIP) {
		mvSCRIP = pSCRIP;
	}

	/**
	 * This method returns the modify order id
	 * 
	 * @return the modify order id.
	 */
	public String getMvModifyOrderID() {
		return mvModifyOrderID;
	}

	/**
	 * This method sets the modify order id
	 * 
	 * @param pModifyOrderID The modify order id.
	 */
	public void setMvModifyOrderID(String pModifyOrderID) {
		mvModifyOrderID = pModifyOrderID;
	}

	/**
	 * This method returns the currency id
	 * 
	 * @return the currency id.
	 */
	public String getMvCurrencyID() {
		return mvCurrencyID;
	}

	/**
	 * This method sets the currency id
	 * 
	 * @param pCurrencyID The currency id.
	 */
	public void setMvCurrencyID(String pCurrencyID) {
		mvCurrencyID = pCurrencyID;
	}

	/**
	 * This method returns the market id
	 * 
	 * @return the market id.
	 */
	public String getMvMarketID() {
		return mvMarketID;
	}

	/**
	 * This method sets the market id
	 * 
	 * @param pMarketID The market id.
	 */
	public void setMvMarketID(String pMarketID) {
		mvMarketID = pMarketID;
	}

	/**
	 * This method returns the instrument short name
	 * 
	 * @return the instrument short name.
	 */
	public String getMvInstrumentShortName() {
		return mvInstrumentShortName;
	}

	/**
	 * This method sets the instrument short name
	 * 
	 * @param pInstrumentShortName The instrument short name.
	 */
	public void setMvInstrumentShortName(String pInstrumentShortName) {
		mvInstrumentShortName = pInstrumentShortName;
	}

	/**
	 * This method returns the modified date
	 * 
	 * @return the modified date.
	 */
	public String getMvModifiedDate() {
		return mvModifiedDate;
	}

	/**
	 * This method sets the modified date
	 * 
	 * @param pModifiedDate The modified date.
	 */
	public void setMvModifiedDate(String pModifiedDate) {
		mvModifiedDate = pModifiedDate;
	}

	/**
	 * This method returns the modified date time
	 * 
	 * @return the modified date time.
	 */
	public String getMvModifiedDateTime() {
		return mvModifiedDateTime;
	}

	/**
	 * This method sets the modified date time
	 * 
	 * @param pModifiedDateTime The modified date time.
	 */
	public void setMvModifiedDateTime(String pModifiedDateTime) {
		mvModifiedDateTime = pModifiedDateTime;
	}

	// === For testing...
	private String stockCode;
	private String stockName;
	private String price;

	/**
	 * This method returns the stock code
	 * 
	 * @return the stock code.
	 */
	public String getStockCode() {
		return stockCode;
	}

	/**
	 * This method sets the stock code
	 * 
	 * @param pStockCode The stock code.
	 */
	public void setStockCode(String pStockCode) {
		stockCode = pStockCode;
	}

	/**
	 * This method returns the stock name
	 * 
	 * @return the stock name.
	 */
	public String getStockName() {
		return stockName;
	}

	/**
	 * This method sets the stock name
	 * 
	 * @param pStockName The stock name.
	 */
	public void setStockName(String pStockName) {
		stockName = pStockName;
	}

	/**
	 * This method returns the price
	 * 
	 * @return the price.
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * This method sets the price
	 * 
	 * @param pPrice The price.
	 */
	public void setPrice(String pPrice) {
		price = pPrice;
	}

	/**
	 * This method returns the reject reason detail
	 * 
	 * @return the reject reason detail.
	 */
	public String getMvRejectReasonDetail() {
		return mvRejectReasonDetail;
	}

	/**
	 * This method sets the reject reason detail
	 * 
	 * @param pRejectReasonDetail The reject reason detail.
	 */
	public void setMvRejectReasonDetail(String pRejectReasonDetail) {
		mvRejectReasonDetail = pRejectReasonDetail;
	}

	public String getMatchedDate() {
		return matchedDate;
	}

	public void setMatchedDate(String matchedDate) {
		this.matchedDate = matchedDate;
	}

	public String getMvFilledPrice() {
		return mvFilledPrice;
	}

	public void setMvFilledPrice(String mvFilledPrice) {
		this.mvFilledPrice = mvFilledPrice;
	}

	public String getMvBankID() {
		return mvBankID;
	}

	public void setMvBankID(String mvBankID) {
		this.mvBankID = mvBankID;
	}

	public String getMvBankACID() {
		return mvBankACID;
	}

	public void setMvBankACID(String mvBankACID) {
		this.mvBankACID = mvBankACID;
	}

}
