package com.ttl.old.itrade.hks.txn;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.tag.ITagXsfTagName;
import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * The HKSMarketDataDetailTxn class definition for all method
 * Access to market data from the host details
 * @author not attributable
 *
 */
public class HKSMarketDataDetailTxn extends BaseTxn
{

	private String mvClientID;
	private String mvMarketID;
	private String mvInstrumentList;
	private String mvLoopCounter;

	private HKSMarketDataDetails[] mvHKSMarketDataDetails;
	/**
	 * Default constructor for HKSMarketDataDetailTxn class
	 * @param pClientID the client id
	 * @param pMarketID the market id
	 * @param pInstrumentList the instrument list
	 */
	public HKSMarketDataDetailTxn(String pClientID, String pMarketID, String pInstrumentList)
    {
       super();
       mvClientID = pClientID;
       mvMarketID = pMarketID;
       mvInstrumentList = pInstrumentList;
    }
	/**
	 * This method process Access to market data from the host details
	 * @return Access to market data from the host details
	 */
	public Vector process()
    {
    	Hashtable lvTxnMap = new Hashtable();
    	lvTxnMap.put(TagName.CLIENTID, getClientID());
    	lvTxnMap.put(TagName.MARKETID, getMarketID());
    	lvTxnMap.put("SYMBOLID", getInstrumentList());

    	Vector lvReturn = new Vector();
    	Vector lvRows = new Vector();
    	if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSMarketDataDetailRequest, lvTxnMap))
    	{
    		IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList(ITagXsfTagName.ROW);
            for (int i = 0; i < lvRowList.size(); i++)
            {
               IMsgXMLNode lvRow = lvRowList.getNode(i);
               HashMap lvModel = new HashMap();
               lvModel.put(TagName.MARKETID, lvRow.getChildNode(TagName.MARKETID).getValue());
               lvModel.put(TagName.INSTRUMENTID, lvRow.getChildNode(TagName.INSTRUMENTID).getValue());
               lvModel.put(TagName.NOMINALPRICE, lvRow.getChildNode(TagName.NOMINALPRICE).getValue());
               lvModel.put(TagName.REFERENCEPRICE, lvRow.getChildNode(TagName.REFERENCEPRICE).getValue());
               lvModel.put(TagName.HIGHPRICE, lvRow.getChildNode(TagName.HIGHPRICE).getValue());
               lvModel.put(TagName.LOWPRICE, lvRow.getChildNode(TagName.LOWPRICE).getValue());
               lvModel.put(TagName.OPENINGPRICE, lvRow.getChildNode(TagName.OPENINGPRICE).getValue());
               lvModel.put(TagName.CLOSINGPRICE, lvRow.getChildNode(TagName.CLOSINGPRICE).getValue());
               lvModel.put("CEILING_PRICE", lvRow.getChildNode("CEILING_PRICE").getValue());
               lvModel.put("FLOOR_PRICE", lvRow.getChildNode("FLOOR_PRICE").getValue());
               lvModel.put(TagName.VOLUME, lvRow.getChildNode(TagName.VOLUME).getValue());

               lvModel.put("BEST_BID_1_PRICE", lvRow.getChildNode("BEST_BID_1_PRICE").getValue());
               lvModel.put("BEST_BID_2_PRICE", lvRow.getChildNode("BEST_BID_2_PRICE").getValue());
               lvModel.put("BEST_BID_3_PRICE", lvRow.getChildNode("BEST_BID_3_PRICE").getValue());
               lvModel.put("BEST_OFFER_1_PRICE", lvRow.getChildNode("BEST_OFFER_1_PRICE").getValue());
               lvModel.put("BEST_OFFER_2_PRICE", lvRow.getChildNode("BEST_OFFER_2_PRICE").getValue());
               lvModel.put("BEST_OFFER_3_PRICE", lvRow.getChildNode("BEST_OFFER_3_PRICE").getValue());
               lvModel.put("BEST_BID_1_VOLUME", lvRow.getChildNode("BEST_BID_1_VOLUME").getValue());
               lvModel.put("BEST_BID_2_VOLUME", lvRow.getChildNode("BEST_BID_2_VOLUME").getValue());
               lvModel.put("BEST_BID_3_VOLUME", lvRow.getChildNode("BEST_BID_3_VOLUME").getValue());
               lvModel.put("BEST_OFFER_1_VOLUME", lvRow.getChildNode("BEST_OFFER_1_VOLUME").getValue());
               lvModel.put("BEST_OFFER_2_VOLUME", lvRow.getChildNode("BEST_OFFER_2_VOLUME").getValue());
               lvModel.put("BEST_OFFER_3_VOLUME", lvRow.getChildNode("BEST_OFFER_3_VOLUME").getValue());
               lvRows.add(lvModel);
            }
            lvReturn.add(lvRows);

            HashMap lvDetail = null;
            IMsgXMLNodeList lvDetailList = mvReturnNode.getNodeList(TagName.DetailNode);
            for (int i = 0; i < lvDetailList.size(); i++)
            {
            	IMsgXMLNode lvDetailNode = lvDetailList.getNode(i);
                lvDetail = new HashMap();

                lvDetail.put(TagName.MARKETID, lvDetailNode.getChildNode(TagName.MARKETID).getValue());
        		lvDetail.put(TagName.INSTRUMENTID, lvDetailNode.getChildNode(TagName.INSTRUMENTID).getValue());
        		lvDetail.put(TagName.TURNOVER, lvDetailNode.getChildNode(TagName.TURNOVER).getValue());
        		lvDetail.put("YEAR_HIGH", lvDetailNode.getChildNode("YEAR_HIGH").getValue());
        		lvDetail.put("YEAR_LOW", lvDetailNode.getChildNode("YEAR_LOW").getValue());
        		lvDetail.put("EPS", lvDetailNode.getChildNode("EPS").getValue());
        		lvDetail.put("P_E", lvDetailNode.getChildNode("P_E").getValue());
        		lvDetail.put("P_BV", lvDetailNode.getChildNode("P_BV").getValue());
        		lvDetail.put("MARKET_CAPITAL", lvDetailNode.getChildNode("MARKET_CAPITAL").getValue());

        		if ( lvDetailNode.getChildNode("MARKETSTATUS") != null )
        			lvDetail.put("MARKETSTATUS", lvDetailNode.getChildNode("MARKETSTATUS").getValue());
        		else
        			lvDetail.put("MARKETSTATUS", "UNKNOWN");
            }
            lvReturn.add(lvDetail);
    	}

    	return lvReturn;
    }
	/**
	 * Get method for client id
	 * @return client id
	 */
	public String getClientID()
    {
        return mvClientID;
    }
	/**
	 * Set method for client id
	 * @param pClientID the client id
	 */
	public void setClientID(String pClientID)
    {
        mvClientID = pClientID;
    }
	/**
	 * Get method for market id
	 * @return market id
	 */
	public String getMarketID()
    {
        return mvMarketID;
    }
	/**
	 * Set method for market id
	 * @param pMarketID the market id
	 */
	public void setMarketID(String pMarketID)
    {
        mvMarketID = pMarketID;
    }
	/**
	 * Get method for instrument list
	 * @return instrument list
	 */
	public String getInstrumentList()
	{
		return mvInstrumentList;
	}
	/**
	 * Set method for instrument list
	 * @param pInstrumentList the instrument list
	 */
	public void setInstrumentList(String pInstrumentList)
	{
		mvInstrumentList = pInstrumentList;
	}
	/**
	 * Get method for loop counter
	 * @return Loop counter counts the number of
	 */
	public int getLoopCounter()
    {
        int lvCnt;
        try
        {
            lvCnt = Integer.parseInt(mvLoopCounter);
        }
        catch (Exception e)
        {
            return -1;
        }
        return lvCnt;
    }
	/**
	 * Set method for loop counter
	 * @param pLoopCounter the loop counter
	 */
	public void setLoopCounter(String pLoopCounter)
    {
        mvLoopCounter = pLoopCounter;
    }

}
