package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.hks.util.AgentUtils;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.ErrorCode;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * This HKSLoginTxn class definition for all method
 * Dealing with the user's login request and its corresponding
 * @author Tree Lam
 * @since 20080625
 */
public class HKSLoginTxn extends BaseTxn
{
    private String mvClientID;
    private String mvPassword;
    private String mvEBPassword;
    private String mvRemoveEBPassword;
    private String mvHostString;
    private String mvPasswordExpired = "N";
    private String mvNeedChangePassword = "N";
    private String mvShowDisclaimer = "N";
    private String mvFullName = "";
    private String mvThirdPartyInfo;
    private String mvAAStockCliName;
    private String mvAAStockCliPwd;
    private String mvAAStockCliLevel;
    private String mvTradingAccSeq;
    private String mvAccountSeq;
    private String mvAccountType;
    private String mvSex;
    private String mvServerIP;
    private String mvLastLoginDate;
    private String mvLastLoginTime;
    private String mvLastLoginStatus;
    private String mvLoginFailedCount;
    private String mvCheckWWWEnabled;
    private boolean mvSkipPasswordChecking;
    private String mvUserIDRegistered;
    private String mvSingleSignOnPhase;
    private String mvPasswordExpiredDate;
    private String mvAuthenSerial;
    
    
    
    public String getAuthenSerial() {
		return mvAuthenSerial;
	}

	public void setAuthenSerial(String pAuthenSerial) {
		this.mvAuthenSerial = pAuthenSerial;
	}

	public String getPasswordExpiredDate() {
		return mvPasswordExpiredDate;
	}

	public void setPasswordExpiredDate(String mvPasswordExpiredDate) {
		this.mvPasswordExpiredDate = mvPasswordExpiredDate;
	}
	//Begin Task #RC00181 - Rice Cheng 20081229
    private String mvLanguage;
    //End Task #RC00181 - Rice Cheng 20081229

    /**
     * The login TXN object is for doing the client login
     * @param pClientId the Client ID
     * @param pPassword the client Password
     */
    public HKSLoginTxn(String pClientId, String pPassword, String pLanguage)
    {
    	//First Password is normal password , second password is EBPassword for First Time Login Check(pin mailer)
    	//Begin Task #RC00181 - Rice Cheng 20090108
        //this(clientId, password, password, "","","N", false, "0");
    	this(pClientId, pPassword, pPassword, "","","N", false, "0", pLanguage);
        //End Task #RC00181 - Rice Cheng 20090108
    }

    /**
     * The login TXN object is for doing the client login
     * @param pClientId the Client ID
     * @param pPassword the client Password
     * @param PHostString If the host has any paramter to pass to itrade, use this, otherwise this field is not used
     * @param pServerIP The Itrade server IP, used for duplicate login checking
     * @param pCheckWWWEnabled the Check whether the client is enabled for the itrade system
     */
    public HKSLoginTxn(String pClientId, String pPassword, String ebPassword, String PHostString, String pServerIP, String pCheckWWWEnabled, boolean pSkipPasswordChecking, String pSingleSignOnPhase)
    {
    	//Begin Task #RC00181 - Rice Cheng 20090108
//        setClientId(clientId);
//        setPassword(password);
//        setEBPassword(ebPassword);
//        setHostString(hostString);
//        setITradeServerIP(pServerIP);
//        setCheckWWWEnabled(pCheckWWWEnabled);
//        setSkipPasswordChecking(pSkipPasswordChecking);
//        setSingleSignOnPhase(pSingleSignOnPhase);
    	this(pClientId, pPassword, ebPassword, PHostString, pServerIP, pCheckWWWEnabled, pSkipPasswordChecking, pSingleSignOnPhase, "");
        //End Task #RC00181 - Rice Cheng 20090108
    }
    /**
     * The login TXN object is for doing the client login
     * @param pClientId the Client ID
     * @param pPassword the client Password
     * @param pEbPassword the enable client password
     * @param pHostString the If the host has any paramter to pass to itrade, use this, otherwise this field is not used
     * @param pServerIP The Itrade server IP, used for duplicate login checking
     * @param pCheckWWWEnabled the Check whether the client is enabled for the itrade system
     * @param pSkipPasswordChecking the skip client password checking
     * @param pSingleSignOnPhase the single sign on phase
     * @param pLanguage the user locale
     */
    //Begin Task #RC00181 - Rice Cheng 20090108
    public HKSLoginTxn(String pClientId, String pPassword, String pEbPassword, String pHostString, String pServerIP, String pCheckWWWEnabled, boolean pSkipPasswordChecking, String pSingleSignOnPhase, String pLanguage)
    {
        setClientId(pClientId);
        setPassword(pPassword);
        setEBPassword(pEbPassword);
        setHostString(pHostString);
        setITradeServerIP(pServerIP);
        setCheckWWWEnabled(pCheckWWWEnabled);
        if(IMain.getProperty("isDebugOn").equals("true") )
        {
            setSkipPasswordChecking(true);
        }
        else
        {
        	setSkipPasswordChecking(pSkipPasswordChecking);        	
        }
        
   
        setSingleSignOnPhase(pSingleSignOnPhase);
        
        setLanguage(pLanguage);
    }
    //End Task #RC00181 - Rice Cheng 20090108
    
    /**
     * This method process Dealing with the user's login request and its corresponding
     *
     */
    public void process()
    {
    	
        Log.println("[ HKSLoginTXN._process() CLIENTID : " + getClientId()+ " ]", Log.ACCESS_LOG);
    	Hashtable lvTxnMap = new Hashtable();
    	lvTxnMap.put(TagName.CLIENTID, getClientId());
    	lvTxnMap.put(TagName.PASSWORD, getPassword());
        lvTxnMap.put("EBPASSWORD", getEBPassword());
    	lvTxnMap.put("HOSTSTRING", getHostString());
    	lvTxnMap.put("INTERNET", "Y");
    	lvTxnMap.put("ITRADESERVERIP", getITradeServerIP());
    	lvTxnMap.put("CHECKWWWENABLED", getCheckWWWEnabled());
        lvTxnMap.put("SKIPPASSWORDCHECKING", getSkipPasswordChecking()?"Y":"N");
        lvTxnMap.put("SINGLESIGNONPHASE", getSingleSignOnPhase());

        //Begin Task #RC00181 - Rice Cheng 20081229
    	if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSLoginRequest, lvTxnMap, getLanguage()))
    	//if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSLoginRequest, lvTxnMap))
    	//End Task #RC00181 - Rice Cheng 20081229
    	{
    		//HIEU LE: init branchIDs
    		String clientID = getClientId();
    		String branchID = AgentUtils.getBranchID(clientID);
    		AgentUtils.addBranchIDs(clientID, branchID);
    		Log.print("[HIEU LE] HKSLoginTxn check and add branchID: " + branchID, 1);
    		//END HIEU LE:
    		
    		setPasswordExpired(mvReturnNode.getChildNode(TagName.ISEXPIRYDATE).getValue());
    		setNeedChangePassword(mvReturnNode.getChildNode(TagName.NEEDCHANGEPASSWORD).getValue());
    		setShowDisclaimer(mvReturnNode.getChildNode(TagName.SHOWDISCLAIMER).getValue());
    		set3rdPartyInfo(mvReturnNode.getChildNode("ALLOW3RDPARTYINFO").getValue());
    		setFullName(mvReturnNode.getChildNode(TagName.FULLNAME).getValue());
    		setTradingAccSeq(mvReturnNode.getChildNode(TagName.TRADINGACCSEQ).getValue());
            setAccountSeq(mvReturnNode.getChildNode(TagName.ACCOUNTSEQ).getValue());
    		setAAStockCliName(mvReturnNode.getChildNode(TagName.AASTOCKCLINAME).getValue());
    		setAAStockCliPwd(mvReturnNode.getChildNode(TagName.AASTOCKCLIPWD).getValue());
    		setAAStockCliLevel(mvReturnNode.getChildNode(TagName.AASTOCKCLILEVEL).getValue());
    		setAccountType(mvReturnNode.getChildNode(TagName.ACCOUNTTYPE).getValue());
    		setSex(mvReturnNode.getChildNode(TagName.SEX).getValue());
    		setLastLoginDate(mvReturnNode.getChildNode("LASTLOGINDATE").getValue());
    		setLastLoginTime(mvReturnNode.getChildNode("LASTLOGINTIME").getValue());
            setLastLoginStatus(mvReturnNode.getChildNode("WWWLASTLOGINSTATUS").getValue());
    		setLoginFailedCount(mvReturnNode.getChildNode("LOGINFAILEDCOUNT").getValue());
            setUserIDRegistered(mvReturnNode.getChildNode("USERIDREGISTERED").getValue());
            setRemoveEBPassword(mvReturnNode.getChildNode("REMOVEEBPASSWORD").getValue());
            //Begin Task # add password expired date and authentication serial number - Nghia Nguyen 20100730
            setPasswordExpiredDate(mvReturnNode.getChildNode(TagName.EXPIRYDATE).getValue());
            setAuthenSerial(mvReturnNode.getChildNode(TagName.SERIALNO).getValue());
            //Begin Task # add password expired date and authentication serial number - Nghia Nguyen 20100730
    	}
    	else
    	{
    		IMsgXMLNode lvErrorCodeNode = mvReturnNode.getChildNode("C_ERROR_CODE");
    		if (lvErrorCodeNode == null || lvErrorCodeNode.getValue().equals(""))
    		{
    			setErrorCode(new ErrorCode(new String[0], "LOGIN_DEFAULT_ERROR", ""));
    		}
    		else
    		{
    			setErrorCode(new ErrorCode(new String[0], lvErrorCodeNode.getValue(), ""));
    		}
    	}
    }

    /**
     * Get the client ID
     * @return Client ID
     */
    public String getClientId()
    {
        return mvClientID;
    }

    /**
     * Set the client ID
     * @param pClientId the client ID
     */
    public void setClientId(String pClientId)
    {
        this.mvClientID = pClientId;
    }

    /**
     * Get the password for the client
     * @return Client password
     */
    private String getPassword()
    {
        return mvPassword;
    }

    /**
     * Set the password for login
     * @param pPassword the client password
     */
    public void setPassword(String pPassword)
    {
        this.mvPassword = pPassword;
    }

    /**
     *Get method encrypt client password
     * @return encrypt client password
     */
    public String getEBPassword()
    {
        return mvEBPassword;
    }
    /**
     *Set method encrypt client password
     * @param pEBPassword String the encrypt client password
     */
    public void setEBPassword(String pEBPassword)
    {
            this.mvEBPassword = pEBPassword;
    }

    /**
     *Get method for remove encrypt client password
     * @return remove encrypt client password
     */
    public String getRemoveEBPassword()
    {
            return mvRemoveEBPassword;
    }

    /**
     * Set method for remove encrypt client password
     * @param pRemoveEBPassword the remove encrypt client password
     */
    public void setRemoveEBPassword(String pRemoveEBPassword)
    {
            this.mvRemoveEBPassword = pRemoveEBPassword;
    }


    /**
     * Set the host string
     * @param pHostString the host string
     */
    public void setHostString(String pHostString) {
    	this.mvHostString = pHostString;
    }

    /**
     * Get the host string
     * @return The host string
     */
    public String getHostString() {
    	return mvHostString;
    }

    /**
     * Get the password expired flag
     * @return Y or N
     */
    public String getPasswordExpired()
    {
        return mvPasswordExpired;
    }

    /**
     * Set the password expired flag
     * @param passwordExpired the client password expired
     */
    public void setPasswordExpired(String passwordExpired)
    {
        this.mvPasswordExpired = passwordExpired;
    }

    /**
     * Get whether change password is required
     * @return Y or N
     */
    public String getNeedChangePassword()
    {
        return mvNeedChangePassword;
    }

    /**
     * Set the change password flag
     * @param pNeedChangePassword the change password flag
     */
    public void setNeedChangePassword(String pNeedChangePassword)
    {
        this.mvNeedChangePassword = pNeedChangePassword;
    }

    /**
     * Get whether disclaimer should be shown
     * @return Y or N
     */
    public String getShowDisclaimer()
    {
        return mvShowDisclaimer;
    }

    /**
     * Set show disclaimer
     * @param pShowDisclaimer the show disclaimer
     */
    public void setShowDisclaimer(String pShowDisclaimer)
    {
        this.mvShowDisclaimer = pShowDisclaimer;
    }

    /**
     * Get 3rd party info
     * @return The info in string format
     */
    public String get3rdPartyInfo()
    {
        return mvThirdPartyInfo;
    }

    /**
     * Set the 3rd party info
     * @param pThirdPartyInfo 3rd party info
     */
    public void set3rdPartyInfo(String pThirdPartyInfo)
    {
        this.mvThirdPartyInfo = pThirdPartyInfo;
    }

    /**
     * Set the full name
     * @param pFullname the full name
     */
    public void setFullName(String pFullname)
    {
        mvFullName = pFullname;
    }

    /**
     * Set the AAStocks client name
     * @param pAAStockCliName the AAStocks client name
     */
    public void setAAStockCliName(String pAAStockCliName)
    {
        mvAAStockCliName = pAAStockCliName;
    }

    /**
     * Set the AAStocks client password
     * @param pAAStockCliPwd the AAStocks client password
     */
    public void setAAStockCliPwd(String pAAStockCliPwd)
    {
        mvAAStockCliPwd = pAAStockCliPwd;
    }

    /**
     * Set the AAStocks client level
     * @param pAAStockCliLevel the AAStocks client level
     */
    public void setAAStockCliLevel(String pAAStockCliLevel)
    {
        mvAAStockCliLevel = pAAStockCliLevel;
    }

    /**
     * Set the trading account sequence
     * @param pTradingAccSeq the trading account sequence
     */
    public void setTradingAccSeq(String pTradingAccSeq)
    {
        mvTradingAccSeq = pTradingAccSeq;
    }

    /**
     * Set the cash account sequence
     * @param pAccountSeq the account sequence
     */
    public void setAccountSeq(String pAccountSeq)
    {
       mvAccountSeq = pAccountSeq;
    }

    /**
     * Set the account type
     * @param pAccountType the account type
     */
    public void setAccountType(String pAccountType)
    {
        mvAccountType = pAccountType;
    }

    /**
     * Get the client full name
     * @return Client full name
     */
    public String getFullname()
    {
        return mvFullName;
    }

    /**
     * Get the AAStocks client name
     * @return AAStocks client name
     */
    public String getAAStockCliName()
    {
        return mvAAStockCliName;
    }

    /**
     * Get the AAStocks client password
     * @return AAStocks client password
     */
    public String getAAStockCliPwd()
    {
        return mvAAStockCliPwd;
    }

    /**
     * Get the AAStocks client level
     * @return AAStocks client level
     */
    public String getAAStockCliLevel()
    {
        return mvAAStockCliLevel;
    }

    /**
     * Get the trading account sequence
     * @return Trading account sequence
     */
    public String getTradingAccSeq()
    {
        return mvTradingAccSeq;
    }

    /**
     * Get the cash account sequence
     * @return Cash account sequence
     */
    public String getAccountSeq()
    {
       return mvAccountSeq;
    }

    /**
     * Get the account type
     * @return C- Cash, X - Custodian, M - Margin, K- Bank
     */
    public String getAccountType()
    {
        return mvAccountType;
    }

    /**
     * Get the client sex
     * @return M or F
     */
	public String getSex() {
		return mvSex;
	}

	/**
	 * Set the client sex
	 * @param pSex the client sex
	 */
	public void setSex(String pSex) {
		this.mvSex = pSex;
	}

	/**
	 * Set the last login date
	 * @param pLastLoginDate the last login date
	 */
	public void setLastLoginDate(String pLastLoginDate) {
		this.mvLastLoginDate = pLastLoginDate;
	}
	/**
	 * Set method for last login status
	 * @param pLastLoginStatus the last login status
	 */
        public void setLastLoginStatus(String pLastLoginStatus)
        {
                this.mvLastLoginStatus = pLastLoginStatus;
        }

	/**
	 * Get the last login date
	 * @return The last login date
	 */
	public String getLastLoginDate() {
		return mvLastLoginDate;
	}

        public String getLastLoginStatus()
        {
                return mvLastLoginStatus;
        }

	/**
	 * Get the last login time
	 * @return The last login time
	 */
	public String getLastLoginTime() {
		return mvLastLoginTime;
	}

	/**
	 * Set the last login time
	 * @param pLastLoginTime the last login time
	 */
	public void setLastLoginTime(String pLastLoginTime) {
		this.mvLastLoginTime = pLastLoginTime;
	}

	/**
	 * Set the login failed count
	 * @param pLoginFailedCount the login failed count
	 */
	public void setLoginFailedCount(String pLoginFailedCount) {
		this.mvLoginFailedCount = pLoginFailedCount;
	}

	/**
	 * Get the login failed count
	 * @return The login failed count
	 */
	public String getLoginFailedCount() {
		return mvLoginFailedCount;
	}

	/**
	 * Set method for the user id registered
	 * @param pUserIDRegistered the user id registered
	 */
        public void setUserIDRegistered(String pUserIDRegistered)
        {
                mvUserIDRegistered = pUserIDRegistered;
        }
    /**
     * Get method for the user id registered
     * @return user id registered
     */
        public String getUserIDRegistered()
        {
                return mvUserIDRegistered;
        }


	/**
	 * Set the itrade server IP
	 * @param pITradeServerIP the the itrade server IP
	 */
	public void setITradeServerIP(String pITradeServerIP) {
		this.mvServerIP = pITradeServerIP;
	}

	/**
	 * Get the itrade server IP
	 * @return The itrade server IP
	 */
	public String getITradeServerIP() {
		return mvServerIP;
	}

	/**
	 * Set method for Check whether the client is enabled for the itrade system
	 * @param pCheckWWWEnabled the Check whether the client is enabled for the itrade system
	 */
	public void setCheckWWWEnabled(String pCheckWWWEnabled) {
		this.mvCheckWWWEnabled = pCheckWWWEnabled;
	}

	/**
	 * Get method for Check whether the client is enabled for the itrade system
	 * @return Check whether the client is enabled for the itrade system
	 */
	public String getCheckWWWEnabled() {
		return this.mvCheckWWWEnabled;
	}

	/**
	 * Set method for ship client password checking
	 * @param pSkipPasswordChecking the ship client password checking
	 */
        public void setSkipPasswordChecking(boolean pSkipPasswordChecking)
        {
                mvSkipPasswordChecking = pSkipPasswordChecking;
        }
        /**
         * Get method for ship client password checking
         * @return ship client password checking
         */
        public boolean getSkipPasswordChecking()
        {
                return mvSkipPasswordChecking;
        }
	/**
	 * Get method for single sign on phase
	 * @return single sign on phase
	 */
        public String getSingleSignOnPhase()
        {
                return mvSingleSignOnPhase;
        }
        /**
         * Set method for single sign on phase
         * @param pSingleSignOnPhaseOn the single sign on phase
         */
        public void setSingleSignOnPhase(String pSingleSignOnPhaseOn)
        {
                mvSingleSignOnPhase = pSingleSignOnPhaseOn;
        }
        
        //Begin Task #RC00181 - Rice Cheng 20081229
        /**
         * Get method for user locale
         * @return user locale
         */
        public String getLanguage()
        {
        	return mvLanguage;
        }
        /**
         * Set method for user locale
         * @param pLanguage the user locale
         */
        public void setLanguage(String pLanguage)
        {
        	mvLanguage = pLanguage;
        }
        //End Task #RC00181 - Rice Cheng 20081229
}
