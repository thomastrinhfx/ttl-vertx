package com.ttl.old.itrade.hks.txn;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.hks.util.TextFormatter;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.tag.ITagXsfTagName;
import com.systekit.winvest.hks.config.mapping.TagName;
import com.systekit.winvest.hks.util.Utils;

/**
 * The HKSOrderDetailsEnquiryTxn class definition for all method 
 * From the host on the Order Details Query
 * @author not attributable
 *
 */
public class HKSOrderDetailsEnquiryTxn extends BaseTxn
{
   private String mvOrderGroupID;
   private String mvIsHistory; 
   private String mvOrderID;
   private String mvByOrderID;
   /**
    *  Constructor for HKSOrderDetailsEnquiryTxn class
    * @param pOrderGroupID the order group id
    */
   public HKSOrderDetailsEnquiryTxn(String pOrderGroupID)
   {
      super();
      mvOrderGroupID = pOrderGroupID;
      mvIsHistory = "N";
      mvByOrderID = "N";
   }
   /**
    * Constructor for HKSOrderDetailsEnquiryTxn class
    * @param pOrderGroupID the order group id
    * @param pOrderID the order id
    * @param pIsHistory the is history
    */
   public HKSOrderDetailsEnquiryTxn(String pOrderGroupID, String pOrderID, String pIsHistory)
   {
      super();
      mvOrderGroupID = pOrderGroupID;
      mvOrderID = pOrderID;
      mvIsHistory = pIsHistory;
      mvByOrderID = "Y";
   }
   /**
    * The method process for from the host on the Order Details Query
    * @return Order Details query result set
    */
   public Vector process()
   {
      Hashtable lvTxnMap = new Hashtable();
      lvTxnMap.put(TagName.ORDERGROUPID, Utils.isNullStr(mvOrderGroupID) ? "" : mvOrderGroupID);
      lvTxnMap.put(TagName.ORDERID, Utils.isNullStr(mvOrderID) ? "" : mvOrderID);
      lvTxnMap.put("ISHISTORY", Utils.isNullStr(mvIsHistory) ? "N" : mvIsHistory);
      lvTxnMap.put("BYORDERID", Utils.isNullStr(mvByOrderID) ? "N" : mvByOrderID);

      Vector lvReturn = new Vector();
      if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSOrderDetailsEnquiry, lvTxnMap))
      {
         IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList(ITagXsfTagName.ROW);
         for (int i = 0; i < lvRowList.size(); i++)
         {
            IMsgXMLNode lvRow = lvRowList.getNode(i);
            HashMap lvModel = new HashMap();
            lvModel.put(TagName.ORDERGROUPID, lvRow.getChildNode(TagName.ORDERGROUPID).getValue());
            lvModel.put("ACTIONTIME", TextFormatter.getFormattedTime_IWS(lvRow.getChildNode("ACTIONTIME").getValue(), "HH:mm:ss, yyyy-MM-dd"));
            //Begin Task: WL00619 Walter Lau 2007 July 27
            lvModel.put(TagName.MARKETID, lvRow.getChildNode(TagName.MARKETID).getValue());
            //End Task: WL00619 Walter Lau 2007 July 27
            lvModel.put(TagName.STOCKID, lvRow.getChildNode(TagName.STOCKID).getValue());
            lvModel.put(TagName.BS, lvRow.getChildNode(TagName.BS).getValue());
            //BEGIN TASK:-TTL-GZ-WSJ-00003 SHAOJIAN WAN 20091224 [ItradeR5]Enter Order, no trading account error
		    //Begin Task: CL00055 Charlie Lau 20081206
            if(lvRow.getChildNode(TagName.ORDERTYPE)!=null && !"".equals(lvRow.getChildNode(TagName.ORDERTYPE)))
            lvModel.put(TagName.ORDERTYPE, lvRow.getChildNode(TagName.ORDERTYPE).getValue());
            if(lvRow.getChildNode(TagName.GOODTILLDATE)!=null && !"".equals(lvRow.getChildNode(TagName.GOODTILLDATE)))
            lvModel.put(TagName.GOODTILLDATE, lvRow.getChildNode(TagName.GOODTILLDATE).getValue());
            if(lvRow.getChildNode(TagName.STOPORDERTYPE)!=null && !"".equals(lvRow.getChildNode(TagName.STOPORDERTYPE)))
            lvModel.put(TagName.STOPORDERTYPE, lvRow.getChildNode(TagName.STOPORDERTYPE).getValue());
            if(lvRow.getChildNode(TagName.STOPORDERTYPE)!=null && !"".equals(lvRow.getChildNode(TagName.STOPORDERTYPE)))
            lvModel.put(TagName.STOPPRICE, lvRow.getChildNode(TagName.STOPPRICE).getValue());
            if(lvRow.getChildNode(TagName.CANCELLEDQTY)!=null && !"".equals(lvRow.getChildNode(TagName.ORDERTYPE)))
            lvModel.put(TagName.CANCELLEDQTY, lvRow.getChildNode(TagName.CANCELLEDQTY).getValue());
            if(lvRow.getChildNode(TagName.FILLEDQTY)!=null && !"".equals(lvRow.getChildNode(TagName.FILLEDQTY)))
            lvModel.put(TagName.FILLEDQTY, lvRow.getChildNode(TagName.FILLEDQTY).getValue());
            if(lvRow.getChildNode(TagName.NETAMT)!=null && !"".equals(lvRow.getChildNode(TagName.NETAMT)))
            lvModel.put(TagName.NETAMT, lvRow.getChildNode(TagName.NETAMT).getValue());
			//End Task: CL00055
            //END TASK:-TTL-GZ-WSJ-00003 SHAOJIAN WAN 20091224 [ItradeR5]Enter Order, no trading account error
            lvModel.put(TagName.PRICE, lvRow.getChildNode(TagName.PRICE).getValue());
            lvModel.put(TagName.QUANTITY, lvRow.getChildNode(TagName.QUANTITY).getValue());
            lvModel.put(TagName.STATUS, lvRow.getChildNode(TagName.STATUS).getValue());
            lvModel.put(TagName.ACTION, lvRow.getChildNode(TagName.ACTION).getValue());
            lvModel.put(TagName.REMARK, lvRow.getChildNode(TagName.REMARK).getValue());
            
            // BEGIN TASK: Giang Tran: Add the settlement account for Order Detail
            lvModel.put(TagName.BANKID, lvRow.getChildNode(TagName.BANKID).getValue());
            lvModel.put(TagName.BANKACID, lvRow.getChildNode(TagName.BANKACID).getValue());
            // END TASK: Giang Tran: Add the settlement account for Order Detail
            
            /*lvModel.put(TagName.TRADEDATE, new Timestamp(Long.parseLong(lvRow.getChildNode(TagName.TRADEDATE).getValue())));
            lvModel.put(TagName.AUDITREMARKS, lvRow.getChildNode(TagName.AUDITREMARKS).getValue());*/
            lvReturn.add(lvModel);
         }
      }
      return lvReturn;
   }

   // Start task#: XYL10018 YuLong Xu 20 Aug 2008
   /**
    * The method for last order detail
    * @return last order detail return set
    */
	public Vector getLastOrderDetail() {
		Hashtable lvTxnMap = new Hashtable();
	      lvTxnMap.put(TagName.ORDERGROUPID, mvOrderGroupID);

	      Vector lvReturn = new Vector();
	      if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSOrderDetailsEnquiry, lvTxnMap))
	      {
	         IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList(ITagXsfTagName.ROW);
	         for (int i = 0; i < lvRowList.size(); i++)
	         {
	        	if((i + 1) == lvRowList.size()){

	        		IMsgXMLNode lvRow = lvRowList.getNode(i);
	        		HashMap lvModel = new HashMap();
	        		lvModel.put(TagName.ORDERGROUPID, lvRow.getChildNode(TagName.ORDERGROUPID).getValue());
	              //lvModel.put("ACTIONTIME", new Timestamp(Long.parseLong(lvRow.getChildNode("ACTIONTIME").getValue())));
	        		lvModel.put("ACTIONTIME", TextFormatter.getFormattedTime_IWS(lvRow.getChildNode("ACTIONTIME").getValue(), "HH:mm:ss, yyyy-MM-dd"));
	        		lvModel.put(TagName.STOCKID, lvRow.getChildNode(TagName.STOCKID).getValue());
	        		lvModel.put(TagName.BS, lvRow.getChildNode(TagName.BS).getValue());
	        		lvModel.put(TagName.PRICE, lvRow.getChildNode(TagName.PRICE).getValue());
	        		lvModel.put(TagName.QUANTITY, lvRow.getChildNode(TagName.QUANTITY).getValue());
	        		lvModel.put(TagName.STATUS, lvRow.getChildNode(TagName.STATUS).getValue());
	        		lvReturn.add(lvModel);
	        	}
	         }
	      }
	      return lvReturn;
	}
	// Start task#: XYL10018 YuLong Xu 20 Aug 2008
}
