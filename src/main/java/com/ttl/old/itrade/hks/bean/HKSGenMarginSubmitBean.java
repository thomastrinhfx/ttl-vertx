package com.ttl.old.itrade.hks.bean;

/**
 * The HKSGenMarginSubmitBean class define variables that to save values 
 * for action 
 * @author
 *
 */
public class HKSGenMarginSubmitBean {
	private String mvFormattedStockID;
	private String mvStockName;
	private String mvFormattedOfferPrice;
	private String mvFormattedQuantity;
	private String mvAmount;
	private String mvSubTotal;
	private String mvFormattedSubtotal;
	private String mvFiancneFee;
	private String mvFormattedFinancefee;
	private String mvMarginPercentage;
	private String mvLoanAmt;
	private String mvFormattedLoanamt;
	private String mvDepositAmt;
	private String mvFormattedDepositamt;
	private String mvFlatFee;
	private String mvFormattedFlatfee;
	private String mvInterestValueDate;
	private String mvAllotmentDate;
	private String mvPhone;
	private String mvSMS;
	private String mvMobile;
	private String mvSMSLang;
	private String mvEmail;
	private String mvApplyMethod;
	private String mvApplicationInputMethod;
	private String mvEntitlementId;
	private String mvInstrumentId;
	private String mvInputQty;
	private String mvMinOfferPrice;
	private String mvMaxOfferPrice;
	private String mvDepositRate;
	private String mvLendingPerecentage;
	private String mvInterestRate;
	private String mvInterestRateBasis;
	private String mvErrorMsg;
	private String mvVerifyPassword;
	
	/**
     * This method returns the stock id.
     * @return the stock id is formatted.
     */
	public String getMvFormattedStockID() {
		return mvFormattedStockID;
	}
	
	/**
     * This method sets the stock id.
     * @param pFormattedStockID The stock id is formatted.
     */
	public void setMvFormattedStockID(String pFormattedStockID) {
		mvFormattedStockID = pFormattedStockID;
	}
	
	/**
     * This method returns the stock name.
     * @return the stock name.
     */
	public String getMvStockName() {
		return mvStockName;
	}
	
	/**
     * This method sets the stock name.
     * @param pStockName The stock name.
     */
	public void setMvStockName(String pStockName) {
		mvStockName = pStockName;
	}
	
	/**
     * This method returns the offer price.
     * @return the offer price is formatted.
     */
	public String getMvFormattedOfferPrice() {
		return mvFormattedOfferPrice;
	}
	
	/**
     * This method sets the offer price.
     * @param mvFormattedOfferPrice The offer price is formatted.
     */
	public void setMvFormattedOfferPrice(String pFormattedOfferPrice) {
		mvFormattedOfferPrice = pFormattedOfferPrice;
	}
	
	/**
     * This method returns the quantity.
     * @return the quantity is formatted.
     */
	public String getMvFormattedQuantity() {
		return mvFormattedQuantity;
	}
	
	/**
     * This method sets the quantity.
     * @param pFormattedQuantity The quantity is formatted.
     */
	public void setMvFormattedQuantity(String pFormattedQuantity) {
		mvFormattedQuantity = pFormattedQuantity;
	}
	
	/**
     * This method returns the amount.
     * @return the amount.
     */
	public String getMvAmount() {
		return mvAmount;
	}
	
	/**
     * This method sets the amount.
     * @param pAmount The amount.
     */
	public void setMvAmount(String pAmount) {
		mvAmount = pAmount;
	}
	
	/**
     * This method returns the sub total.
     * @return the sub total.
     */
	public String getMvSubTotal() {
		return mvSubTotal;
	}
	
	/**
     * This method sets the sub total.
     * @param pSubTotal The sub total.
     */
	public void setMvSubTotal(String pSubTotal) {
		mvSubTotal = pSubTotal;
	}
	
	/**
     * This method returns the sub total.
     * @return the sub total is formatted.
     */
	public String getMvFormattedSubtotal() {
		return mvFormattedSubtotal;
	}
	
	/**
     * This method sets the sub total.
     * @param pFormattedSubtotal The sub total is formatted.
     */
	public void setMvFormattedSubtotal(String pFormattedSubtotal) {
		mvFormattedSubtotal = pFormattedSubtotal;
	}
	
	/**
     * This method returns the finance fee.
     * @return the finance fee.
     */
	public String getMvFiancneFee() {
		return mvFiancneFee;
	}
	
	/**
     * This method sets the finance fee.
     * @param pFiancneFee The finance fee.
     */
	public void setMvFiancneFee(String pFiancneFee) {
		mvFiancneFee = pFiancneFee;
	}
	
	/**
     * This method returns the finance fee.
     * @return the finance fee is formatted.
     */
	public String getMvFormattedFinancefee() {
		return mvFormattedFinancefee;
	}
	
	/**
     * This method sets the finance fee.
     * @param pFormattedFinanceFee The Finance fee is formatted.
     */
	public void setMvFormattedFinancefee(String pFormattedFinancefee) {
		mvFormattedFinancefee = pFormattedFinancefee;
	}
	
	/**
     * This method returns the margin percentage.
     * @return the margin percentage.
     */
	public String getMvMarginPercentage() {
		return mvMarginPercentage;
	}
	
	/**
     * This method sets the margin percentage.
     * @param pMarginPercentage The margin percentage.
     */
	public void setMvMarginPercentage(String pMarginPercentage) {
		mvMarginPercentage = pMarginPercentage;
	}
	
	/**
     * This method returns the loan amount.
     * @return the loan amount.
     */
	public String getMvLoanAmt() {
		return mvLoanAmt;
	}
	
	/**
     * This method sets the loan amount.
     * @param pLoanAmt The loan amount.
     */
	public void setMvLoanAmt(String pLoanAmt) {
		mvLoanAmt = pLoanAmt;
	}
	
	/**
     * This method returns the loan amount.
     * @return the loan amount is formatted.
     */
	public String getMvFormattedLoanamt() {
		return mvFormattedLoanamt;
	}
	
	/**
     * This method sets the loan amount.
     * @param pFormattedLoanamt The loan amount is formatted.
     */
	public void setMvFormattedLoanamt(String pFormattedLoanamt) {
		mvFormattedLoanamt = pFormattedLoanamt;
	}
	
	/**
     * This method returns the deposit amount.
     * @return the deposit amount.
     */
	public String getMvDepositAmt() {
		return mvDepositAmt;
	}
	
	/**
     * This method sets the deposit amount.
     * @param pDepositAmt The deposit amount.
     */
	public void setMvDepositAmt(String pDepositAmt) {
		mvDepositAmt = pDepositAmt;
	}
	
	/**
     * This method returns the deposit amount.
     * @return the deposit amount is formatted.
     */
	public String getMvFormattedDepositamt() {
		return mvFormattedDepositamt;
	}
	
	/**
     * This method sets the deposit amount.
     * @param pFormattedDepositamt The deposit amount is formatted.
     */
	public void setMvFormattedDepositamt(String pFormattedDepositamt) {
		mvFormattedDepositamt = pFormattedDepositamt;
	}
	
	/**
     * This method returns the flat fee.
     * @return the flat fee.
     */
	public String getMvFlatFee() {
		return mvFlatFee;
	}
	
	/**
     * This method sets the flat fee.
     * @param pFlatFee The flat fee.
     */
	public void setMvFlatFee(String pFlatFee) {
		mvFlatFee = pFlatFee;
	}
	
	/**
     * This method returns the flat fee.
     * @return the flat fee is formatted.
     */
	public String getMvFormattedFlatfee() {
		return mvFormattedFlatfee;
	}
	
	/**
     * This method sets the flat fee.
     * @param pFormattedFlatfee The flat fee is formatted.
     */
	public void setMvFormattedFlatfee(String pFormattedFlatfee) {
		mvFormattedFlatfee = pFormattedFlatfee;
	}
	
	/**
     * This method returns the interest value date.
     * @return the interest value date.
     */
	public String getMvInterestValueDate() {
		return mvInterestValueDate;
	}
	
	/**
     * This method sets the interest value date.
     * @param pInterestValueDate The interest value date.
     */
	public void setMvInterestValueDate(String pInterestValueDate) {
		mvInterestValueDate = pInterestValueDate;
	}
	
	/**
     * This method returns the allotment date.
     * @return the allotment date.
     */
	public String getMvAllotmentDate() {
		return mvAllotmentDate;
	}
	
	/**
     * This method sets the allotment date.
     * @param pAllotmentDate The allotment value date.
     */
	public void setMvAllotmentDate(String pAllotmentDate) {
		mvAllotmentDate = pAllotmentDate;
	}
	
	/**
     * This method returns the telephone.
     * @return the telephone.
     */
	public String getMvPhone() {
		return mvPhone;
	}
	
	/**
     * This method sets the telephone.
     * @param pPhone The telephone.
     */
	public void setMvPhone(String pPhone) {
		mvPhone = pPhone;
	}
	
	/**
     * This method returns the SMS.
     * @return the SMS.
     */
	public String getMvSMS() {
		return mvSMS;
	}
	
	/**
     * This method sets the SMS.
     * @param pSMS The SMS.
     */
	public void setMvSMS(String pSMS) {
		mvSMS = pSMS;
	}
	
	/**
     * This method returns the mobile.
     * @return the mobile.
     */
	public String getMvMobile() {
		return mvMobile;
	}
	
	/**
     * This method sets the mobile.
     * @param pMobile The mobile.
     */
	public void setMvMobile(String pMobile) {
		mvMobile = pMobile;
	}
	
	/**
     * This method returns the SMS language.
     * @return the SMS language.
     */
	public String getMvSMSLang() {
		return mvSMSLang;
	}
	
	/**
     * This method sets the SMS language.
     * @param pSMSLang The SMS language.
     */
	public void setMvSMSLang(String pSMSLang) {
		mvSMSLang = pSMSLang;
	}
	
	/**
     * This method returns the email.
     * @return the email.
     */
	public String getMvEmail() {
		return mvEmail;
	}
	
	/**
     * This method sets the email.
     * @param pEmail The email.
     */
	public void setMvEmail(String pEmail) {
		mvEmail = pEmail;
	}
	
	/**
     * This method returns the apply method.
     * @return the apply method.
     */
	public String getMvApplyMethod() {
		return mvApplyMethod;
	}
	
	/**
     * This method sets the apply method.
     * @param pApplyMethod The apply method.
     */
	public void setMvApplyMethod(String pApplyMethod) {
		mvApplyMethod = pApplyMethod;
	}
	
	/**
     * This method returns the application apply method.
     * @return the application apply method.
     */
	public String getMvApplicationInputMethod() {
		return mvApplicationInputMethod;
	}
	
	/**
     * This method sets the application apply method.
     * @param pApplicationInputMethod The application apply method.
     */
	public void setMvApplicationInputMethod(String pApplicationInputMethod) {
		mvApplicationInputMethod = pApplicationInputMethod;
	}
	
	/**
     * This method returns the entitlement id of margin apply.
     * @return the entitlement id of margin apply.
     */
	public String getMvEntitlementId() {
		return mvEntitlementId;
	}
	
	/**
     * This method sets the entitlement id of margin apply.
     * @param pEntitlementId The entitlement id of margin apply.
     */
	public void setMvEntitlementId(String pEntitlementId) {
		mvEntitlementId = pEntitlementId;
	}
	
	/**
     * This method returns the id of instrument.
     * @return the id of instrument.
     */
	public String getMvInstrumentId() {
		return mvInstrumentId;
	}
	
	/**
     * This method sets the id of instrument.
     * @param pInstrumentId The id of instrument.
     */
	public void setMvInstrumentId(String pInstrumentId) {
		mvInstrumentId = pInstrumentId;
	}
	
	/**
     * This method returns the input quantity.
     * @return the input quantity.
     */
	public String getMvInputQty() {
		return mvInputQty;
	}
	
	/**
     * This method sets the input quantity.
     * @param pInputQty The input quantity.
     */
	public void setMvInputQty(String pInputQty) {
		mvInputQty = pInputQty;
	}
	
	/**
     * This method returns the min offer price.
     * @return the min offer price.
     */
	public String getMvMinOfferPrice() {
		return mvMinOfferPrice;
	}
	
	/**
     * This method sets the min offer price.
     * @param pMinOfferPrice The min offer price.
     */
	public void setMvMinOfferPrice(String pMinOfferPrice) {
		mvMinOfferPrice = pMinOfferPrice;
	}
	
	/**
     * This method returns the max offer price.
     * @return the max offer price.
     */
	public String getMvMaxOfferPrice() {
		return mvMaxOfferPrice;
	}
	
	/**
     * This method sets the max offer price.
     * @param pMaxOfferPrice The max offer price.
     */
	public void setMvMaxOfferPrice(String pMaxOfferPrice) {
		mvMaxOfferPrice = pMaxOfferPrice;
	}
	
	/**
     * This method returns the deposit rate.
     * @return the deposit rate.
     */
	public String getMvDepositRate() {
		return mvDepositRate;
	}
	
	/**
     * This method sets the deposit rate.
     * @param pDepositRate The deposit rate.
     */
	public void setMvDepositRate(String pDepositRate) {
		mvDepositRate = pDepositRate;
	}
	
	/**
     * This method returns the lending percentage.
     * @return the lending percentage.
     */
	public String getMvLendingPerecentage() {
		return mvLendingPerecentage;
	}
	
	/**
     * This method sets the lending percentage.
     * @param pLendingPerecentage The lending percentage.
     */
	public void setMvLendingPerecentage(String pLendingPerecentage) {
		mvLendingPerecentage = pLendingPerecentage;
	}
	
	/**
     * This method returns the interest rate.
     * @return the interest rate.
     */
	public String getMvInterestRate() {
		return mvInterestRate;
	}
	
	/**
     * This method sets the interest rate.
     * @param pInterestRate The interest rate.
     */
	public void setMvInterestRate(String pInterestRate) {
		mvInterestRate = pInterestRate;
	}
	
	/**
     * This method returns the interest rate basis.
     * @return the interest rate basis.
     */
	public String getMvInterestRateBasis() {
		return mvInterestRateBasis;
	}
	
	/**
     * This method sets the interest rate basis.
     * @param pInterestRateBasis The interest rate basis.
     */
	public void setMvInterestRateBasis(String pInterestRateBasis) {
		mvInterestRateBasis = pInterestRateBasis;
	}
	
	/**
     * This method returns the error message.
     * @return the error message.
     */
	public String getMvErrorMsg() {
		return mvErrorMsg;
	}
	
	/**
     * This method sets the error message
     * @param pErrorMsg The error message.
     */
	public void setMvErrorMsg(String pErrorMsg) {
		mvErrorMsg = pErrorMsg;
	}
	
	/**
     * This method returns the verify password.
     * @return the verify password.
     */
	public String getMvVerifyPassword() {
		return mvVerifyPassword;
	}
	
	/**
     * This method sets the verify password.
     * @param pVerifyPassword The verify password.
     */
	public void setMvVerifyPassword(String pVerifyPassword) {
		mvVerifyPassword = pVerifyPassword;
	}
}