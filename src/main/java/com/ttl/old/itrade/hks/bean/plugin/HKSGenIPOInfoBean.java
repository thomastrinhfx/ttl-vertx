package com.ttl.old.itrade.hks.bean.plugin;

/**
 * The HKSGenIPOInfoBean class define variables that to save values 
 * for action 
 * @author Wind.Zhao
 * @create date 20100206
 */

public class HKSGenIPOInfoBean {
	private String mvAppStatus;
	private String mvListingDate;
	private String mvStockCode;
	private String mvStockName;
	private String mvEntitlementID;
	private boolean mvContains;
	private String mvOfferPrice;
	private String mvConfirmedPrice;
	
	/**
     * This method returns the apply status.
     * @return the apply status.
     */
	public String getMvAppStatus() {
		return mvAppStatus;
	}
	
	/**
     * This method sets the apply status.
     * @param pAppStatus The apply status.
     */
	public void setMvAppStatus(String pAppStatus) {
		mvAppStatus = pAppStatus;
	}
	
	/**
     * This method returns the listing date.
     * @return the listing date.
     */
	public String getMvListingDate() {
		return mvListingDate;
	}
	
	/**
     * This method sets the listing date.
     * @param pListingDate The listing date.
     */
	public void setMvListingDate(String pListingDate) {
		mvListingDate = pListingDate;
	}
	
	/**
     * This method returns the id of stock.
     * @return the id of stock.
     */
	public String getMvStockCode() {
		return mvStockCode;
	}
	
	/**
     * This method sets the id of stock.
     * @param pStockCode The id of stock.
     */
	public void setMvStockCode(String pStockCode) {
		mvStockCode = pStockCode;
	}
	
	/**
     * This method returns the name of stock.
     * @return the name of stock.
     */
	public String getMvStockName() {
		return mvStockName;
	}
	
	/**
     * This method sets the name of stock.
     * @param pStockName The name of stock.
     */
	public void setMvStockName(String pStockName) {
		mvStockName = pStockName;
	}
	
	/**
     * This method returns the entitlement id.
     * @return the entitlement id.
     */
	public String getMvEntitlementID() {
		return mvEntitlementID;
	}
	
	/**
     * This method sets the entitlement id.
     * @param pEntitlementID The entitlement id.
     */
	public void setMvEntitlementID(String pEntitlementID) {
		mvEntitlementID = pEntitlementID;
	}
	
	/**
     * This method returns the contains if true or false.
     * @return the contains if true or false.
     */
	public boolean isMvContains() {
		return mvContains;
	}
	
	/**
     * This method sets the contains if true or false.
     * @param pContains The contains if true or false.
     */
	public void setMvContains(boolean pContains) {
		mvContains = pContains;
	}
	
	/**
     * This method returns the offer price.
     * @return the offer price.
     */
	public String getMvOfferPrice() {
		return mvOfferPrice;
	}
	
	/**
     * This method sets the offer price.
     * @param pOfferPrice The offer price.
     */
	public void setMvOfferPrice(String pOfferPrice) {
		mvOfferPrice = pOfferPrice;
	}
	
	/**
     * This method returns the confirmed price.
     * @return the confirmed price.
     */
	public String getMvConfirmedPrice() {
		return mvConfirmedPrice;
	}
	
	/**
     * This method sets the confirmed price.
     * @param pConfirmedPrice The confirmed price.
     */
	public void setMvConfirmedPrice(String pConfirmedPrice) {
		mvConfirmedPrice = pConfirmedPrice;
	}
}
