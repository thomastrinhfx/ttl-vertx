package com.ttl.old.itrade.hks.txn;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

import java.util.Hashtable;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.ttl.old.itrade.util.Presentation;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * This class processes the script option status enquiry
 * @author not attributable
 *
 */
public class HKSScripOptionStatusEnquiryTxn
{
    private String mvClientId;
    private String mvTradingAccSeq;
    private String mvAccountSeq;
    private int mvReturnCode;
    private String mvErrorCode;
    private String mvErrorMessage;

    private HKSScripOptionDetails[]   mvHKSScripOptionDetails;


    public static final String LOOP = "LOOP";
    public static final String LOOP_ELEMENT = "LOOP_ELEMENT";


    TPErrorHandling				tpError;
    Presentation presentation = null;

    /**
     * Default constructor for HKSScripOptionStatusEnquiryTxn class
     * @param pPresentation the Presentation class
     */
    public HKSScripOptionStatusEnquiryTxn(Presentation pPresentation)
    {
        tpError = new TPErrorHandling();
        presentation = pPresentation;
    }
    ;

    /**
     * Default constructor for HKSScripOptionStatusEnquiryTxn class
     * @param pPresentation the Presentation class
     * @param pClientId the client id
     * @param pTradingAccSeq the trading account sequence
     * @param pAccountSeq the account sequence
     */
    public HKSScripOptionStatusEnquiryTxn(Presentation pPresentation, String pClientId, String pTradingAccSeq, String pAccountSeq)
    {
        this(pPresentation);
        setClientId(pClientId);
        setTradingAccSeq(pTradingAccSeq);
        setAccountSeq(pAccountSeq);
    }
    /**
     * The method process the Script Option Status Enquiry
     * @param pPresentation the Presentation class
     */
    public void process(Presentation pPresentation)
    {
       try
       {
           Hashtable		lvTxnMap = new Hashtable();

           IMsgXMLNode		lvRetNode;
           TPManager ivTPManager = ITradeServlet.getTPManager(RequestName.HKSScripOptionStatusRequest);
           TPBaseRequest   lvScripOptionicationStatusEnquiry = ivTPManager.getRequest(RequestName.HKSScripOptionStatusRequest);

           lvTxnMap.put(TagName.CLIENTID, getClientId());
           lvTxnMap.put(TagName.TRADINGACCSEQ, getTradingAccSeq());
           lvTxnMap.put(TagName.ACCOUNTSEQ, getAccountSeq());

           lvRetNode = lvScripOptionicationStatusEnquiry.send(lvTxnMap);

           setReturnCode(tpError.checkError(lvRetNode));
           if (mvReturnCode != tpError.TP_NORMAL)
           {
               setErrorMessage(tpError.getErrDesc());
               if (mvReturnCode == tpError.TP_APP_ERR)
               {
                   setErrorCode(tpError.getErrCode());
               }
           }
           else
           {
               IMsgXMLNodeList lvStatusNodeList = lvRetNode.getChildNode(LOOP).getNodeList(LOOP_ELEMENT);

               if (lvStatusNodeList == null || lvStatusNodeList.size() == 0)
               {
                   mvHKSScripOptionDetails = new HKSScripOptionDetails[0];
                   return;
               }

               int lvSize = lvStatusNodeList.size();

               mvHKSScripOptionDetails = new HKSScripOptionDetails[lvSize];

               for (int i = 0; i < lvSize; i++)
               {
                     mvHKSScripOptionDetails[i] = new HKSScripOptionDetails();
                     mvHKSScripOptionDetails[i].setClientID(lvStatusNodeList.getNode(i).getChildNode(TagName.CLIENTID).getValue());
                     mvHKSScripOptionDetails[i].setTradingAccSeq(lvStatusNodeList.getNode(i).getChildNode(TagName.TRADINGACCSEQ).getValue());
                     mvHKSScripOptionDetails[i].setCurrencyID(lvStatusNodeList.getNode(i).getChildNode(TagName.CURRENCYID).getValue());
                     mvHKSScripOptionDetails[i].setEntitlementID(lvStatusNodeList.getNode(i).getChildNode(TagName.ENTITLEMENTID).getValue());
                     mvHKSScripOptionDetails[i].setInstrumentID(lvStatusNodeList.getNode(i).getChildNode(TagName.INSTRUMENTID).getValue());
                     mvHKSScripOptionDetails[i].setMarketID(lvStatusNodeList.getNode(i).getChildNode(TagName.MARKETID).getValue());
                     mvHKSScripOptionDetails[i].setProductID(lvStatusNodeList.getNode(i).getChildNode(TagName.PRODUCTID).getValue());
                     mvHKSScripOptionDetails[i].setBookCloseDate(lvStatusNodeList.getNode(i).getChildNode(TagName.BOOKCLOSEDATE).getValue());
                     mvHKSScripOptionDetails[i].setBookCloseQty(lvStatusNodeList.getNode(i).getChildNode(TagName.BOOKCLOSEQTY).getValue());
                     mvHKSScripOptionDetails[i].setScripOptionQty(lvStatusNodeList.getNode(i).getChildNode("SCRIPOPTIONQTY").getValue());
                     mvHKSScripOptionDetails[i].setStatus(lvStatusNodeList.getNode(i).getChildNode("STATUS").getValue());
                     mvHKSScripOptionDetails[i].setStatusDescription(lvStatusNodeList.getNode(i).getChildNode("STATUSDESCRIPTION").getValue());
                     mvHKSScripOptionDetails[i].setDividendAmount(lvStatusNodeList.getNode(i).getChildNode("DIVIDENDAMOUNT").getValue());
                     mvHKSScripOptionDetails[i].setMaxDividendAmount(lvStatusNodeList.getNode(i).getChildNode("MAXDIVIDENDAMOUNT").getValue());
                     mvHKSScripOptionDetails[i].setDividendDistributed(lvStatusNodeList.getNode(i).getChildNode("DISTRIBUTEDYEAR").getValue());
                     mvHKSScripOptionDetails[i].setNumberOfSharesHeld(lvStatusNodeList.getNode(i).getChildNode("NUMBEROFSHARESHELD").getValue());
                     mvHKSScripOptionDetails[i].setNumOfSharesHeldBalanceDate(lvStatusNodeList.getNode(i).getChildNode("BALANCEDATE").getValue());
                     mvHKSScripOptionDetails[i].setDividendPerShare(lvStatusNodeList.getNode(i).getChildNode(TagName.DIVIDENDPERSHARE).getValue());
                     mvHKSScripOptionDetails[i].setConversionPrice(lvStatusNodeList.getNode(i).getChildNode("CONVERSIONPRICE").getValue());
                     mvHKSScripOptionDetails[i].setClosingDate(lvStatusNodeList.getNode(i).getChildNode("CLOSINGDATE").getValue());
                     mvHKSScripOptionDetails[i].setMaxResultQty(lvStatusNodeList.getNode(i).getChildNode("MAXRESULTQTY").getValue());
                     mvHKSScripOptionDetails[i].setRemainingDividendAmount(lvStatusNodeList.getNode(i).getChildNode("REMAININGDIVIDENDAMOUNT").getValue());
                     mvHKSScripOptionDetails[i].setRegisteredQty(lvStatusNodeList.getNode(i).getChildNode(TagName.REGISTEREDQTY).getValue());
                     mvHKSScripOptionDetails[i].setDividendDeliver(lvStatusNodeList.getNode(i).getChildNode("DIVIDENDDELIVER").getValue());
                     mvHKSScripOptionDetails[i].setDividendRatioPer(lvStatusNodeList.getNode(i).getChildNode("DIVIDENDRATIOPER").getValue());
                     mvHKSScripOptionDetails[i].setSettleCurrencyID(lvStatusNodeList.getNode(i).getChildNode(TagName.SETTLECURRENCYID).getValue());

               }
           }
       }
       catch (Exception e)
       {
           Log.println( e , Log.ERROR_LOG);
       }
    }

    /**
     * Get method for Client ID
     * @return Client ID
     */
    public String getClientId()
    {
        return mvClientId;
    }

    /**
     * Set method for Client ID
     * @param pClientId the Client ID
     */
    public void setClientId(String pClientId)
    {
        mvClientId = pClientId;
    }
    /**
     * Get method for trading account sequence
     * @return trading account sequence
     */
    public String getTradingAccSeq()
    {
        return mvTradingAccSeq;
    }

    /**
     * Set method for trading account sequence
     * @param pTradingAccSeq the trading account sequence
     */
    public void setTradingAccSeq(String pTradingAccSeq)
    {
        mvTradingAccSeq = pTradingAccSeq;
    }
    /**
     * Get method for account sequence
     * @return account sequence
     */
    public String getAccountSeq()
    {
        return mvAccountSeq;
    }

    /**
     * Set method for account sequence
     * @param pAccountSeq the account sequence
     */
    public void setAccountSeq(String pAccountSeq)
    {
        mvAccountSeq = pAccountSeq;
    }






    /**
     * Get method for IPO Application Status Details
     * @return Array of IPO Application Status Details
     */
    public HKSScripOptionDetails[] getScripOptionDetails()
    {
        return mvHKSScripOptionDetails;
    }

    /**
     * Set method for IPO Application Status Details
     * @param pHKSScripOptionDetails the Array of IPO Application Status Details
     */
    public void setScripOptionDetails(HKSScripOptionDetails[] pHKSScripOptionDetails)
    {
        mvHKSScripOptionDetails = pHKSScripOptionDetails;
    }

    /**
     * Get method for Return Code
     * @return Return Code
     */
    public int getReturnCode()
    {
        return mvReturnCode;
    }

    /**
     * Set method for Return Code
     * @param pReturnCode the Return Code
     */
    public void setReturnCode(int pReturnCode)
    {
        mvReturnCode = pReturnCode;
    }

    /**
     * Get method for Error Code
     * @return Error Code
     */
    public String getErrorCode()
    {
        return mvErrorCode;
    }

    /**
     * Set method for Error Code
     * @param pErrorCode the Error Code
     */
    public void setErrorCode(String pErrorCode)
    {
        mvErrorCode = pErrorCode;
    }

    /**
     * Get method for Error Message
     * @return Error Message
     */
    public String getErrorMessage()
    {
        return mvErrorMessage;
    }

    /**
     * Set method for Error Message
     * @param pErrorMessage the Error Message
     */
    public void setErrorMessage(String pErrorMessage)
    {
        mvErrorMessage = pErrorMessage;
    }
    /**
     * This method for localized error system message
     * @param pErrorCode the error system code
     * @param pDefaultMesg the default message
     * @return  localized error system message
     */
   public String getLocalizedErrorMessage(String pErrorCode, String pDefaultMesg)
   {
      String lvRetMessage;

      try
      {
         if (pErrorCode.equals("0") || pErrorCode.equals("999"))
         {
            lvRetMessage = pDefaultMesg;
         }
         else
         {
            lvRetMessage = (String) presentation.getErrorCodeBundle().getString(pErrorCode);
         }
      }
      catch (Exception ex)
      {
         Log.println("Presentation._showPage: missing error mapping for " + pErrorCode + ", Default = " + pDefaultMesg, Log.ERROR_LOG);

         lvRetMessage = pDefaultMesg;
      }

      return lvRetMessage;
   }
}
