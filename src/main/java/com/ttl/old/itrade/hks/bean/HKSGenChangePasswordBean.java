package com.ttl.old.itrade.hks.bean;

/**
 * The HKSGenChangePasswordBean class define variables that to save values 
 * for action 
 * @author 
 *
 */
public class HKSGenChangePasswordBean {
	private String pClientID;
	private String pClientName;
	private String pErrorMsg;
	private String pFirstTimeLogin;
	private String pRandomSeed;
	private String pDateTime;
	private String pPasswordMinLength;
	private String pPasswordMaxLength;
	private String pDisplaygotologinpageStart;
	private String pDisplaygotologinpageEnd;
	
	/**
     * This method returns the client id.
     * @return the client id.
     */
	public String getPClientID() {
		return pClientID;
	}
	
	/**
     * This method sets client id.
     * @param pClientID The client id.
     */
	public void setPClientID(String pClientID) {
		this.pClientID = pClientID;
	}
	
	/**
     * This method returns the client name.
     * @return the client name.
     */
	public String getPClientName() {
		return pClientName;
	}
	
	/**
     * This method sets client name.
     * @param pClientName The client name.
     */
	public void setPClientName(String pClientName) {
		this.pClientName = pClientName;
	}
	
	/**
     * This method returns the error message when change password.
     * @return the error message.
     */
	public String getPErrorMsg() {
		return pErrorMsg;
	}
	
	/**
     * This method sets the error message when change password
     * @param pErrorMsg The error message.
     */
	public void setPErrorMsg(String pErrorMsg) {
		this.pErrorMsg = pErrorMsg;
	}
	
	/**
     * This method returns the first time login to ITrade or not.
     * @return if the first time login to ITrade or not.
     */
	public String getPFirstTimeLogin() {
		return pFirstTimeLogin;
	}
	
	/**
     * This method sets the first time login to ITrade or not
     * @param pFirstTimeLogin The first time login to ITrade or not.
     */
	public void setPFirstTimeLogin(String pFirstTimeLogin) {
		this.pFirstTimeLogin = pFirstTimeLogin;
	}
	
	/**
     * This method returns the random seed.
     * @return if the random seed.
     */
	public String getPRandomSeed() {
		return pRandomSeed;
	}
	
	/**
     * This method sets the random seed
     * @param pRandomSeed The random seed.
     */
	public void setPRandomSeed(String pRandomSeed) {
		this.pRandomSeed = pRandomSeed;
	}
	
	/**
     * This method returns the date time.
     * @return if the date time.
     */
	public String getPDateTime() {
		return pDateTime;
	}
	
	/**
     * This method sets the date time
     * @param dateTime The date time.
     */
	public void setPDateTime(String pDateTime) {
		this.pDateTime = pDateTime;
	}
	
	/**
     * This method returns the password min length.
     * @return the password min length.
     */
	public String getPPasswordMinLength() {
		return pPasswordMinLength;
	}
	
	/**
     * This method sets the password min length.
     * @param pPasswordMinLength The password min length.
     */
	public void setPPasswordMinLength(String pPasswordMinLength) {
		this.pPasswordMinLength = pPasswordMinLength;
	}
	
	/**
     * This method returns the password max length.
     * @return the password max length.
     */
	public String getPPasswordMaxLength() {
		return pPasswordMaxLength;
	}
	
	/**
     * This method sets the password max length.
     * @param pPasswordMaxLength The password max length.
     */
	public void setPPasswordMaxLength(String pPasswordMaxLength) {
		this.pPasswordMaxLength = pPasswordMaxLength;
	}
	
	/**
     * This method returns if display goto login page or not.
     * @return if display goto login page or not.
     */
	public String getPDisplaygotologinpageStart() {
		return pDisplaygotologinpageStart;
	}
	
	/**
     * This method sets if display goto login page or not.
     * @param pDisplaygotologinpageStart If display goto login page or not.
     */
	public void setPDisplaygotologinpageStart(String pDisplaygotologinpageStart) {
		this.pDisplaygotologinpageStart = pDisplaygotologinpageStart;
	}
	
	/**
     * This method returns if display goto login page or not.
     * @return if display goto login page or not.
     */
	public String getPDisplaygotologinpageEnd() {
		return pDisplaygotologinpageEnd;
	}
	
	/**
     * This method sets if display goto login page or not.
     * @param pDisplaygotologinpageEnd If display goto login page or not.
     */
	public void setPDisplaygotologinpageEnd(String pDisplaygotologinpageEnd) {
		this.pDisplaygotologinpageEnd = pDisplaygotologinpageEnd;
	}
	
}