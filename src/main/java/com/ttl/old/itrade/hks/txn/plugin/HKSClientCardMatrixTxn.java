/**
 * @author: hao.nguyen
 * @create date : 20120426
 */
//Begin Task #: TTL-MAS-2708 Hao Nguyen 20120426 for [ITrade4] auth cardmatrix online
package com.ttl.old.itrade.hks.txn.plugin;

import java.util.Hashtable;

import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;

/**
 * The HKSClientCardMatrixTxn class definition for all method
 *  On-line query auth cardmatrix processing
 * @author hao.nguyen
 * @since 20120426
 */

public class HKSClientCardMatrixTxn extends BaseTxn {
	
	private String SERIALNUMBER="SERIALNUMBER";
	private String CLIENTID="CLIENTID";
//	private String CARDDATA="CARDDATA";
//	private String GENDAY="GENDAY";
//	private String GENERATOR="GENERATOR";
//	private String ISPRINT="ISPRINT";
	private String ATTEMPT="ATTEMPT";
	private String ATTEMPTLIMIT="ATTEMPTLIMIT";
	private String ISLOCK="ISLOCK";	
	private String WORDMATRIX01="WORDMATRIX01";
	private String WORDMATRIX02="WORDMATRIX02";
	
	
	private String language;
	private String mvErrorMessageCode=null;
	private String mvErrorMessage	=null;
		
	private String mvAttemp;
	private String mvIsLock; 
	private String mvAttempLimit;
	
	private String  lvClientId = "077C081448";		 
	private String  mvSerialnumber="22333";	  	 
	private String  mvWordMatrixKey01="4,F";
	private String  mvWordMatrixValue01="C";
	private String  mvWordMatrixKey02="3,G";
	private String  mvWordMatrixValue02="2";	
	
	private boolean isLog	=	true;

	public HKSClientCardMatrixTxn(String lvClientId,
			String mvSerialnumber, String mvWordMatrixKey01,
			String mvWordMatrixValue01, String mvWordMatrixKey02,
			String mvWordMatrixValue02, String language) {
		super();
		this.language = language;
		this.lvClientId = lvClientId;
		this.mvSerialnumber = mvSerialnumber;
		this.mvWordMatrixKey01 = mvWordMatrixKey01;
		this.mvWordMatrixValue01 = mvWordMatrixValue01;
		this.mvWordMatrixKey02 = mvWordMatrixKey02;
		this.mvWordMatrixValue02 = mvWordMatrixValue02;
	}
	
	private void logCardMardTnx( boolean isLog,String msg){		
		if(isLog)
			Log.println(msg,Log.ALERT_LOG);
	}
	
	/*
	 * 
	 * process create  mvWordMatrixKey01 and mvWordMatrixKey02 when need
	 * if errorCode=
	 */

	public void process() {
		Hashtable<String,String> lvTxnMap = new Hashtable<String,String>();
		lvTxnMap.put(CLIENTID, lvClientId);
		lvTxnMap.put(SERIALNUMBER, mvSerialnumber);
		lvTxnMap.put(WORDMATRIX01, mvWordMatrixKey01 +":"+mvWordMatrixValue01);
		lvTxnMap.put(WORDMATRIX02, mvWordMatrixKey02 +":"+mvWordMatrixValue02);	
		if(null != IMain.getProperty("max.auth.attempt.failed")){
			lvTxnMap.put(ATTEMPTLIMIT, IMain.getProperty("max.auth.attempt.failed"));
		}else{
			lvTxnMap.put(ATTEMPTLIMIT, ""+5);
		}		
			
		this.logCardMardTnx(isLog,"TTTT_HKSClientCardMatrixTxn_CALL_BO_BEGIN_lvClientId="+lvClientId+",mvWordMatrixKey01="+mvWordMatrixKey01+",mvWordMatrixKey02="+mvWordMatrixKey02);
		
		if (TPErrorHandling.TP_NORMAL == process("HKSTruthClientCardMatrix", lvTxnMap,this.language)) {
			mvReturnNode.getValue();			
			try {
				String errorCode = mvReturnNode.getChildNode("ERRORCODE").getValue();							
				String errorMsg = mvReturnNode.getChildNode("ERRORMSG").getValue();
				mvAttemp = mvReturnNode.getChildNode(ATTEMPT).getValue();
				//mvAttempLimit = mvReturnNode.getChildNode(ATTEMPTLIMIT).getValue();
				mvAttempLimit = IMain.getProperty("max.auth.attempt.failed");
				mvIsLock = mvReturnNode.getChildNode(ISLOCK).getValue();
				mvWordMatrixKey01 = mvReturnNode.getChildNode("WORDMATRIXKey01").getValue();
				mvWordMatrixKey02 = mvReturnNode.getChildNode("WORDMATRIXKey02").getValue();
								
				this.setErrorMessageCode(errorCode);
				this.setMvErrorMessage(errorMsg);
				
				this.logCardMardTnx(isLog,"TTTT_HKSClientCardMatrixTxn_RESPONSE"+errorCode+"="+errorCode+",errorMsg="+errorMsg+",mvWordMatrixKey01="+mvWordMatrixKey01+",mvWordMatrixKey02="+mvWordMatrixKey02);
				
			} catch (NullPointerException e) {
				this.logCardMardTnx(isLog,"TTTT_HKSClientCardMatrixTxn_TP XML Response : node not exists");
				setErrorMessageCode("COMMUNICATION_ERROR");
			}			
			
		}else
        { // Unhandled Business Exception
			
			setErrorMessageCode("SERVER_ERROR");			
            //Instrument  not found
            if (mvReturnNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase("CARD001")) {
            	setErrorMessageCode("NONEXISTING_STOCKID");
            }
         }
		this.logCardMardTnx(isLog,"TTTT_HKSClientCardMatrixTxn_CALL_BO_END_lvClientId="+lvClientId+",mvWordMatrixKey01="+mvWordMatrixKey01+",mvWordMatrixKey02="+mvWordMatrixKey02);
		
	}


	/**
	 * Get the Error Message Code
	 * @return mvErrorMessageCode
	 */
	public String getErrorMessageCode() {
		return mvErrorMessageCode;
	}
	/**
	 * Set the Error Message Code
	 * @param pErrorMessageCode
	 */
	public void setErrorMessageCode(String pErrorMessageCode) {
		this.mvErrorMessageCode = pErrorMessageCode;
	}
		
	

	public String getMvErrorMessage() {
		return mvErrorMessage;
	}


	public void setMvErrorMessage(String mvErrorMessage) {
		this.mvErrorMessage = mvErrorMessage;
	}


	public String getMvAttemp() {
		return mvAttemp;
	}

	
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getMvErrorMessageCode() {
		return mvErrorMessageCode;
	}

	public void setMvErrorMessageCode(String mvErrorMessageCode) {
		this.mvErrorMessageCode = mvErrorMessageCode;
	}

	public String getMvAttempLimit() {
		return mvAttempLimit;
	}

	public void setMvAttempLimit(String mvAttempLimit) {
		this.mvAttempLimit = mvAttempLimit;
	}

	public String getLvClientId() {
		return lvClientId;
	}

	public void setLvClientId(String lvClientId) {
		this.lvClientId = lvClientId;
	}

	public String getMvWordMatrixValue01() {
		return mvWordMatrixValue01;
	}

	public void setMvWordMatrixValue01(String mvWordMatrixValue01) {
		this.mvWordMatrixValue01 = mvWordMatrixValue01;
	}

	public String getMvWordMatrixValue02() {
		return mvWordMatrixValue02;
	}

	public void setMvWordMatrixValue02(String mvWordMatrixValue02) {
		this.mvWordMatrixValue02 = mvWordMatrixValue02;
	}

	public boolean isLog() {
		return isLog;
	}

	public void setLog(boolean isLog) {
		this.isLog = isLog;
	}

	
	public void setMvAttemp(String mvAttemp) {
		this.mvAttemp = mvAttemp;
	}

	public void setMvIsLock(String mvIsLock) {
		this.mvIsLock = mvIsLock;
	}

	public void setMvSerialnumber(String mvSerialnumber) {
		this.mvSerialnumber = mvSerialnumber;
	}

	public void setMvWordMatrixKey01(String mvWordMatrixKey01) {
		this.mvWordMatrixKey01 = mvWordMatrixKey01;
	}

	public void setMvWordMatrixKey02(String mvWordMatrixKey02) {
		this.mvWordMatrixKey02 = mvWordMatrixKey02;
	}

	public String getMvIsLock() {
		return mvIsLock;
	}


	public String getMvSerialnumber() {
		return mvSerialnumber;
	}


	public String getMvWordMatrixKey01() {
		return mvWordMatrixKey01;
	}


	public String getMvWordMatrixKey02() {
		return mvWordMatrixKey02;
	}
		
	
}

//End Task #: TTL-MAS-2708 Hao Nguyen 20120426 for [ITrade4] auth cardmatrix online
