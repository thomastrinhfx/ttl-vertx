package com.ttl.old.itrade.hks.txn;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * The HKSInstrumentNewsAndResearchReportEnquiryTxn class definition for all
 * Instrument News And Research Report Enquiry
 * @author not attributable
 *
 */
public class HKSInstrumentNewsAndResearchReportEnquiryTxn extends BaseTxn
{
        private String mvTxnType;
//	private String						mvOrderLoopCounter;
//	private String						mvDwLoopCounter;
       private String						mvLoopCounter;
       private int							mvReturnCode;
       private String						mvErrorCode;
       private String						mvErrorMessage;
       /**
        * This variable is used to the tp Error
        */
       TPErrorHandling mvTpError;
       /**
        * Constructor for HKSInstrumentNewsAndResearchReportEnquiryTxn class
        */
       public HKSInstrumentNewsAndResearchReportEnquiryTxn()
        {
                mvTpError = new TPErrorHandling();
        }
        ;

        /**
         * This method process Instrument News And Research Report Enquiry
         * @param pClientID the client id
         * @param pLang The user locale
         * @return a vector of transcations
         */
   public Vector process(String pClientID, String pLang)
   {
           Vector lvReturn = new Vector();
           try
           {

      Hashtable lvTxnMap = new Hashtable();

      lvTxnMap.put(TagName.CLIENTID, pClientID);
      lvTxnMap.put(TagName.LANGUAGEID, pLang);


      IMsgXMLNode		lvRetNode;
      TPManager ivTPManager = ITradeServlet.getTPManager(RequestName.HKSInstrumentNewsAndResearchReportEnquiry);
      TPBaseRequest   lvTransactionHistoryRequest = ivTPManager.getRequest(RequestName.HKSInstrumentNewsAndResearchReportEnquiry);

      lvRetNode = lvTransactionHistoryRequest.send(lvTxnMap);

                        setReturnCode(mvTpError.checkError(lvRetNode));
                        if (mvReturnCode != TPErrorHandling.TP_NORMAL)
                        {
                                setErrorMessage(mvTpError.getErrDesc());
                                if (mvReturnCode == TPErrorHandling.TP_APP_ERR)
                                {
                                        setErrorCode(mvTpError.getErrCode());
                                }
                        }
                        else
                        {

                                IMsgXMLNodeList lvRowList = lvRetNode.getNodeList("Row");


                         for (int i = 0; i < lvRowList.size(); i++)
                         {
                            IMsgXMLNode lvNode = lvRowList.getNode(i);
                            HashMap lvModel = new HashMap();
                            lvModel.put(TagName.REPORTID, lvNode.getChildNode(TagName.REPORTID).getValue());
                            lvModel.put(TagName.MARKETID, lvNode.getChildNode(TagName.MARKETID).getValue());
                            lvModel.put(TagName.INSTRUMENTID, lvNode.getChildNode(TagName.INSTRUMENTID).getValue());
                            lvModel.put("DESCRIPTION", lvNode.getChildNode("DESCRIPTION").getValue());
                            lvModel.put("LINK", lvNode.getChildNode("LINK").getValue());
                            lvModel.put("CATEGORYID", lvNode.getChildNode("CATEGORYID").getValue());
                            lvReturn.add(lvModel);
                         }
                      }
/*
                                   lvModel.setProductID(trim(pRS.getString("PRODUCTID")));
                                 lvModel.setMarketID(trim(pRS.getString("MARKETID")));
                                 lvModel.setReportID(trim(pRS.getString("REPORTID")));
                                 lvModel.setInstrumentID(trim(pRS.getString("INSTRUMENTID")));
                                 lvModel.setEffectiveDate(pRS.getDate("EFFECTIVEDATE"));
                                 lvModel.setExpiryDate(pRS.getDate("EXPIRYDATE"));
                                 lvModel.setClientID(trim(pRS.getString("CLIENTID")));
                                 lvModel.setInvestorGroupID(trim(pRS.getString("INVESTORGROUPID")));
                                 lvModel.setInvestorClassID(trim(pRS.getString("INVESTORCLASSID")));
                                 lvModel.setPriority(pRS.getShort("PRIORITY"));
                                 lvModel.setCategoryID(trim(pRS.getString("CATEGORYID")));
                                 lvModel.setState(trim(pRS.getString("STATE")));
                                 lvModel.setLanguageID(pRS.getShort("LANGUAGEID"));
                                 lvModel.setLink(pRS.getString("LINK"));
                                 lvModel.setDescription(pRS.getString("DESCRIPTION"));

*/



                }
                catch (Exception e)
                {
                        e.printStackTrace();
                }






      return lvReturn;
   }
   /**
    * Set method for system return code
    * @param pReturnCode the system return code
    */
   public void setReturnCode(int pReturnCode)
        {
                mvReturnCode = pReturnCode;
        }
   /**
    * Set method for system error code
    * @param pErrorCode the system error code
    */
        public void setErrorCode(String pErrorCode)
        {
                mvErrorCode = pErrorCode;
        }




                /**
                 * Set method for Error Message
                 * @param pErrorMessage the systme Error Message
                 */
                public void setErrorMessage(String pErrorMessage)
                {
                        mvErrorMessage = pErrorMessage;
                }

}
