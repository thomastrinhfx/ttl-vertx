package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;

/**
 * The HKSCheckMulitpleClientIDTxn class defines methodsis that all
 * Check the user account is valid
 * 
 * @author not attributable
 *
 */
public class HKSCheckMulitpleClientIDTxn extends BaseTxn{
        private String mvClientID;
        private String mvCINO;
        private int mvCINOClientCount;
        private String mvEBAccountExisting;
	//private String mvResult;
    /**
     * This variable is used to the tp Error
     */
	TPErrorHandling  tpError;

	private int mvReturnCode;
	private String mvErrorCode;
	private String mvErrorMessage;
	/**
	 * Default constructor for HKSCheckMulitpleClientIDTxn class
	 */
	public HKSCheckMulitpleClientIDTxn(){
		tpError = new TPErrorHandling();
	}
	/**
	 * Default constructor for HKSCheckMulitpleClientIDTxn class
	 * @param pClientID the Client ID
	 */
	public HKSCheckMulitpleClientIDTxn(String pClientID){
		tpError = new TPErrorHandling();
		//setCINO(pCINO);
                setClientID(pClientID);
	}

	/**
	 * The method process  Check the user account is valid
	 * 
	 */
        public void process()
        {
                try {
                        Log.println("[ HKSCheckMulitpleClientIDTxn.process(): CINO " + getCINO() + " starts", Log.ACCESS_LOG);
                        Hashtable lvTxnMap = new Hashtable();
                        String lvCINO;
                        String lvEBAccountExisting;
                        int lvCINOClientCount;

                        IMsgXMLNode lvRetNode;
                        TPManager ivTPManager = ITradeServlet.getTPManager(RequestName.HKSCheckMultileClientIDRequest);
                        TPBaseRequest lvHKSCINORequest = ivTPManager.getRequest(RequestName.HKSCheckMultileClientIDRequest);

                        lvTxnMap.put("CLIENTID", getClientID());

                        lvRetNode = lvHKSCINORequest.send(lvTxnMap);

                        setReturnCode(tpError.checkError(lvRetNode));
                        if(mvReturnCode != TPErrorHandling.TP_NORMAL) {
                                setErrorMessage(tpError.getErrDesc());
                                if(mvReturnCode == TPErrorHandling.TP_APP_ERR) {
                                        setErrorCode(tpError.getErrCode());
                                }
                        }
                        else {
                                lvCINO = lvRetNode.getChildNode("CINO").getValue();
                                lvCINOClientCount = Integer.parseInt(lvRetNode.getChildNode("CINOCLIENTCOUNT").getValue());
                                lvEBAccountExisting = lvRetNode.getChildNode("EBAccountExisting").getValue();

                                setCINO(lvCINO);
                                setCINOClientCount(lvCINOClientCount);
                                setEBAccountExisting(lvEBAccountExisting);
                        }

                        if(getCINOClientCount() > 1) {

                        }

                } catch(Exception e) {
                        Log.println("[ HKSCheckMulitpleClientIDTxn.process(): CINO " + getCINO() + " error" + e.toString(), Log.ERROR_LOG);
                }
        }


        /**
         * Set method for the System Return Code
         * @param pReturnCode the System Return Code
         */
	public void setReturnCode(int pReturnCode)
	{
	    mvReturnCode = pReturnCode;
	}
		/**
		 * Set method for the System Error Code
		 * @param pErrorCode the System Error Code
		 */
        public void setErrorCode(String pErrorCode)
        {
                mvErrorCode = pErrorCode;
        }
        /**
         * Set method for the System Error Message
         * @param pErrorMessage the System Error Message
         */
        public void setErrorMessage(String pErrorMessage)
        {
                mvErrorMessage = pErrorMessage;
        }
        /**
         * Get method for the Client Count
         * @return the Client Count
         */
        public int getCINOClientCount()
        {
                return mvCINOClientCount;
        }
        /**
         * Set method for the CINO Client Count
         * @param pCINOClientCount the CINO Client Count
         */
        public void setCINOClientCount(int pCINOClientCount)
        {
                mvCINOClientCount = pCINOClientCount;
        }
        /**
         * Set method for the CINO
         * @return the CINO
         */
        public String getCINO()
        {
                return mvCINO;
        }

        /**
         * Set method for the CINO
         * @param pCINO the CINO
         */
        public void setCINO(String pCINO)
        {
                mvCINO = pCINO;
        }
        /**
         * Get method for the Client ID
         * @return the Client ID
         */
        public String getClientID()
        {
                return mvClientID;
        }
        /**
         * Set method for the Client ID
         * @param pClientID the Client ID
         */
        public void setClientID(String pClientID)
        {
                mvClientID = pClientID;
        }
        /**
         * Set method for the Client Existing E-Account balance
         * @return the Client Existing E-Account balance
         */
        public String getEBAccountExisting()
        {
                return mvEBAccountExisting;
        }
        /**
         * Get method for the Client Existing E-Account balance
         * @param pEBAccountExisting the Client Existing E-Account balance
         */
        public void setEBAccountExisting(String pEBAccountExisting)
        {
                mvEBAccountExisting = pEBAccountExisting;
        }

}
