package com.ttl.old.itrade.hks.txn;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.bean.HKSOverdueDebtBean;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;

/**
 * Get overdue debt and upcoming debt for margin.
 * 
 * @author duonghuynh
 */
public class HKSOverdueDebtTxn {
	/**
	 * This variable is used to the tp Error
	 */
	TPErrorHandling tpError;

	private int returnCode;
	private String errorCode;
	private String errorMessage;

	private String clientId;
	private String queryDate;
	private int loanDuration;
	private int remindDays;
	private int forceSellDays;

	private HKSOverdueDebtBean overdueDebt = null;
	private List<Map<String, String>> upcomingDebt;

	public static final String TYPE_OVERDUEDEBT = "OVERDUEDEBT";
	public static final String TYPE_UPCOMINGDEBT = "UPCOMINGDEBT";

	/**
	 * Default constructor for HKSMarginListTxn class
	 */
	public HKSOverdueDebtTxn() {
		tpError = new TPErrorHandling();
	}

	/**
	 * 
	 * @param queryDate
	 * @param pClientId
	 * @param loanDuration
	 * @param remindDays
	 */
	public HKSOverdueDebtTxn(String queryDate, String pClientId, int loanDuration, int remindDays) {
		setQueryDate(queryDate);
		setClientId(pClientId);
		setLoanDuration(loanDuration);
		setRemindDays(remindDays);
		tpError = new TPErrorHandling();
	}

	/**
	 * 
	 * @param queryDate
	 * @param pClientId
	 * @param loanDuration
	 * @param remindDays
	 * @param forceSellDays
	 */
	public HKSOverdueDebtTxn(String queryDate, String pClientId, int loanDuration, int remindDays, int forceSellDays) {
		setQueryDate(queryDate);
		setClientId(pClientId);
		setLoanDuration(loanDuration);
		setRemindDays(remindDays);
		setForceSellDays(forceSellDays);
		tpError = new TPErrorHandling();
	}

	/**
	 * Get overdue debt, result is stored in overdueDebt
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void getOverdueDebtData() {

		Hashtable lvTxnMap = new Hashtable();

		IMsgXMLNode lvRetNode;
		TPManager ivTPManager = ITradeServlet.getTPManager("HKSOverdueDebt");
		TPBaseRequest lvRequest = ivTPManager.getRequest("HKSOverdueDebt");

		lvTxnMap.put("CLIENTID", getClientId());
		lvTxnMap.put("QUERYDATE", getQueryDate());
		lvTxnMap.put("LOANDURATION", String.valueOf(getLoanDuration()));
		lvTxnMap.put("REMINDDAYS", String.valueOf(getRemindDays()));
		lvTxnMap.put("FORCESELLDAYS", String.valueOf(getForceSellDays()));
		lvTxnMap.put("TYPE", TYPE_OVERDUEDEBT);

		lvRetNode = lvRequest.send(lvTxnMap);

		setReturnCode(tpError.checkError(lvRetNode));
		if (returnCode != TPErrorHandling.TP_NORMAL) {
			setErrorMessage(tpError.getErrDesc());
			if (returnCode == TPErrorHandling.TP_APP_ERR) {
				setErrorCode(tpError.getErrCode());
			}
		} else {
			overdueDebt = new HKSOverdueDebtBean();
			overdueDebt.setAdvanceRequest(lvRetNode.getChildNode("AdvanceRequest").getValue());
			overdueDebt.setCashSupplement(lvRetNode.getChildNode("CashSupplement").getValue());
			overdueDebt.setForceSell(lvRetNode.getChildNode("ForceSell").getValue());
			overdueDebt.setOverdueDebt(lvRetNode.getChildNode("OverdueDebt").getValue());
			overdueDebt.setProcessedDebt(lvRetNode.getChildNode("ProcessedDebt").getValue());
			overdueDebt.setSellStockRequest(lvRetNode.getChildNode("SellStockRequest").getValue());
			overdueDebt.setUpcomingDueDebt(lvRetNode.getChildNode("UpcomingDueDebt").getValue());
			overdueDebt.setCashReserve(lvRetNode.getChildNode("CashReserve").getValue());
			overdueDebt.setLoanDuration(String.valueOf(loanDuration));
			overdueDebt.setRemindDays(String.valueOf(remindDays));
			overdueDebt.setForceSellDays(String.valueOf(forceSellDays));
		}

	}

	/**
	 * Get upcoming debt, result is stored in upcomingDebt
	 */
	public void getUpcomingDebtData() {
		Hashtable<String, String> lvTxnMap = new Hashtable<String, String>();

		IMsgXMLNode lvRetNode;
		TPManager ivTPManager = ITradeServlet.getTPManager("HKSOverdueDebt");
		TPBaseRequest lvRequest = ivTPManager.getRequest("HKSOverdueDebt");

		lvTxnMap.put("CLIENTID", getClientId());
		lvTxnMap.put("QUERYDATE", getQueryDate());
		lvTxnMap.put("LOANDURATION", String.valueOf(getLoanDuration()));
		lvTxnMap.put("REMINDDAYS", String.valueOf(getRemindDays()));
		lvTxnMap.put("FORCESELLDAYS", String.valueOf(getForceSellDays()));
		lvTxnMap.put("TYPE", TYPE_UPCOMINGDEBT);

		lvRetNode = lvRequest.send(lvTxnMap);

		setReturnCode(tpError.checkError(lvRetNode));
		if (returnCode != TPErrorHandling.TP_NORMAL) {
			setErrorMessage(tpError.getErrDesc());
			if (returnCode == TPErrorHandling.TP_APP_ERR) {
				setErrorCode(tpError.getErrCode());
			}
		} else {
			upcomingDebt = new ArrayList<Map<String, String>>();
			IMsgXMLNodeList lvRowList = lvRetNode.getNodeList("Row");			
			for (int i = 0; i < lvRowList.size(); i++) {
				IMsgXMLNode lvRow = lvRowList.getNode(i);
				Map<String, String> dataRow = new HashMap<String, String>();				
				dataRow.put("Date", lvRow.getChildNode("DEBTDATE").getValue());
				dataRow.put("Value", lvRow.getChildNode("DEBTVALUE").getValue());
				upcomingDebt.add(dataRow);
			}
			
		}
	}

	/**
	 * @return the tpError
	 */
	public TPErrorHandling getTpError() {
		return tpError;
	}

	/**
	 * @param tpError the tpError to set
	 */
	public void setTpError(TPErrorHandling tpError) {
		this.tpError = tpError;
	}

	/**
	 * @return the returnCode
	 */
	public int getReturnCode() {
		return returnCode;
	}

	/**
	 * @param returnCode the returnCode to set
	 */
	public void setReturnCode(int returnCode) {
		this.returnCode = returnCode;
	}

	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the clientId
	 */
	public String getClientId() {
		return clientId;
	}

	/**
	 * @param clientId the clientId to set
	 */
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	/**
	 * @return the loanDuration
	 */
	public int getLoanDuration() {
		return loanDuration;
	}

	/**
	 * @param loanDuration the loanDuration to set
	 */
	public void setLoanDuration(int loanDuration) {
		this.loanDuration = loanDuration;
	}

	/**
	 * @return the remindDays
	 */
	public int getRemindDays() {
		return remindDays;
	}

	/**
	 * @param remindDays the remindDays to set
	 */
	public void setRemindDays(int remindDays) {
		this.remindDays = remindDays;
	}

	/**
	 * @return the forceSellDays
	 */
	public int getForceSellDays() {
		return forceSellDays;
	}

	/**
	 * @param forceSellDays the forceSellDays to set
	 */
	public void setForceSellDays(int forceSellDays) {
		this.forceSellDays = forceSellDays;
	}

	/**
	 * @return the overdueDebt
	 */
	public HKSOverdueDebtBean getOverdueDebt() {
		return overdueDebt;
	}

	/**
	 * @param overdueDebt the overdueDebt to set
	 */
	public void setOverdueDebt(HKSOverdueDebtBean overdueDebt) {
		this.overdueDebt = overdueDebt;
	}

	/**
	 * @return the queryDate
	 */
	public String getQueryDate() {
		return queryDate;
	}

	/**
	 * @param queryDate the queryDate to set
	 */
	public void setQueryDate(String queryDate) {
		this.queryDate = queryDate;
	}

	public List<Map<String, String>> getUpcomingDebt() {
		return upcomingDebt;
	}

	public void setUpcomingDebt(List<Map<String, String>> upcomingDebt) {
		this.upcomingDebt = upcomingDebt;
	}

}
