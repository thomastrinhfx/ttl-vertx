package com.ttl.old.itrade.hks.txn;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.ttl.old.itrade.hks.bean.HKSMarketIndexBean;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.tag.ITagXsfTagName;
import com.systekit.winvest.hks.config.mapping.TagName;

public class HKSGetMarketStatusInfoTxn extends BaseTxn {

	private String lvMarketID;
	@SuppressWarnings("unchecked")
	private Hashtable lvTxnMap;
	private String lvMarketStatus;
	private String lvMarketTime;
	
	private List<HKSMarketIndexBean> marketIndexList = new ArrayList<HKSMarketIndexBean>();
	/**
	 * @param lvMarketStatus the lvMarketStatus to set
	 */
	public void setLvMarketStatus(String lvMarketStatus) {
		this.lvMarketStatus = lvMarketStatus;
	}

	/**
	 * @return the lvMarketStatus
	 */
	public String getLvMarketStatus() {
		return lvMarketStatus;
	}

	/**
	 * @param lvMarketTime the lvMarketTime to set
	 */
	public void setLvMarketTime(String lvMarketTime) {
		this.lvMarketTime = lvMarketTime;
	}

	/**
	 * @return the lvMarketTime
	 */
	public String getLvMarketTime() {
		return lvMarketTime;
	}

	public HKSGetMarketStatusInfoTxn()
	{
		
	}

	@SuppressWarnings("unchecked")
	public void getMarketStatusInfo(String pMarketID)
	{
		Log.println("Start getMarketStatusInfo Txn",Log.ACCESS_LOG);
		this.lvMarketID=pMarketID;
		try {
			lvTxnMap = new Hashtable();
		    lvTxnMap.put(TagName.MARKETID, lvMarketID);
		    lvTxnMap.put(TagName.TYPE, "MARKETSTATUS");
		    if (TPErrorHandling.TP_NORMAL == process("HKSQueryMarketStatusInfoRequest", lvTxnMap)) {
		    	IMsgXMLNodeList lvRowList = mvReturnNode
				.getNodeList(ITagXsfTagName.CHILD_ROW);
		    	if(lvRowList.size()>0){
		    		IMsgXMLNode lvRow = lvRowList.getNode(0);
		    		lvMarketStatus= lvRow.getChildNode(TagName.STATUS).getValue();
		    		
		    		if(lvMarketStatus != null){
		    			lvMarketStatus = lvMarketStatus.trim();
					}
		    		
		    		lvMarketTime= lvRow.getChildNode("MARKETTIME").getValue();
		    	}
		    }else {
		    	Log.println(mvReturnNode.toString(),Log.ACCESS_LOG);
		    }
		} catch (Exception e) {
			Log.println(e,Log.ERROR_LOG);
		}finally{
			Log.println("End getMarketStatusInfo Txn",Log.DEBUG_LOG);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void getMarketIndexInfo(){
		Log.println("Start getMarketIndexInfo Txn",Log.ACCESS_LOG );
		//this.lvMarketID=pMarketID;
		try {
			lvTxnMap = new Hashtable();
		    lvTxnMap.put(TagName.TYPE, "MARKETINDEX");
		    if (TPErrorHandling.TP_NORMAL == process("HKSQueryMarketStatusInfoRequest", lvTxnMap)) {
		    	IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList(ITagXsfTagName.CHILD_ROW);
		    	HKSMarketIndexBean marketIndexBean;
		    	for(int index = 0; index < lvRowList.size();index++){
		    		marketIndexBean = new  HKSMarketIndexBean();
		    		IMsgXMLNode lvRow = lvRowList.getNode(index);
		    		String lvmarketID=lvRow.getChildNode(TagName.MARKETID).getValue().trim();
		    		marketIndexBean.setMvMaketID(lvmarketID);
		    		
		    		String lvMarketIndex = lvRow.getChildNode(TagName.MARKETINDEX).getValue() == null ? "": lvRow.getChildNode(TagName.MARKETINDEX).getValue();
		    		marketIndexBean.setMvMarketIndex(lvMarketIndex);
		    		
					String lvDifference = lvRow.getChildNode("INDEXCHANGE").getValue() == null ? "": lvRow.getChildNode("INDEXCHANGE").getValue();
					String lvPercentage = lvRow.getChildNode("INDEXPERCENT").getValue() == null ? "": lvRow.getChildNode("INDEXPERCENT").getValue();
					
					marketIndexBean.setMvPercentage(lvPercentage);
		    		marketIndexBean.setMvDifference(lvDifference);
		    		
					String lvMarketTotalValue = lvRow.getChildNode("MARKETTOTALVALUE").getValue() == null ? "": lvRow.getChildNode("MARKETTOTALVALUE").getValue();
					
					String lvMarketTotalStock;
					// BEGIN - Task #: TTL-VN VanTran For HOSE need to get total market qty with MARKETTOTALSTOCK
					/*if(lvmarketID.equalsIgnoreCase("HO")){
						lvMarketTotalStock = lvRow.getChildNode("MARKETTOTALSTOCK").getValue() == null ? "": lvRow.getChildNode("MARKETTOTALSTOCK").getValue();
					}else{*/
						lvMarketTotalStock = lvRow.getChildNode("MARKETTOTALQTY").getValue() == null ? "": lvRow.getChildNode("MARKETTOTALQTY").getValue();
					//}
					// END - Task #: TTL-VN VanTran For HOSE need to get total market qty with MARKETTOTALSTOCK
					//String lvMarketTotalStock = lvRow.getChildNode("MARKETTOTALQTY").getValue() == null ? "": lvRow.getChildNode("MARKETTOTALQTY").getValue();

					marketIndexBean.setMvMarketTotalvalue(lvMarketTotalValue);
						
					marketIndexBean.setMvMarketTotalQty(lvMarketTotalStock);
		    		
					String lvMarketAdvance = lvRow.getChildNode("MARKETADVANCES").getValue() == null ? "": lvRow.getChildNode("MARKETADVANCES").getValue();
					String lvMarketDecline = lvRow.getChildNode("MARKETDECLINES").getValue() == null ? "": lvRow.getChildNode("MARKETDECLINES").getValue();
					String lvMarketNoChange = lvRow.getChildNode("MARKETNOCHANGE").getValue() == null ? "": lvRow.getChildNode("MARKETNOCHANGE").getValue();
		    		
					marketIndexBean.setMvMarketAdvances(lvMarketAdvance);
		    		marketIndexBean.setMvMarketDeclines(lvMarketDecline);
		    		marketIndexBean.setMvMarketNoChange(lvMarketNoChange);
		    		
		    		marketIndexBean.setMvMarketStatus(lvRow.getChildNode(TagName.STATUS).getValue());
		    		marketIndexBean.setMvMarketTime(lvRow.getChildNode("MARKETTIME").getValue());
					
		    		marketIndexList.add(marketIndexBean);
		    	}    	
		    }else {
		    	Log.println(mvReturnNode.toString(),Log.ACCESS_LOG);
		    }
		} catch (Exception e) {
			Log.println(e,Log.ERROR_LOG);
		}finally{
			Log.println("End getMarketIndexInfo Txn",Log.ACCESS_LOG);
		}
	}

	public void setMarketIndexList(List<HKSMarketIndexBean> marketIndexList) {
		this.marketIndexList = marketIndexList;
	}

	public List<HKSMarketIndexBean> getMarketIndexList() {
		return marketIndexList;
	}
}
