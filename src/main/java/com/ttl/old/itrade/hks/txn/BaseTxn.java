package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.ErrorCode;
import com.systekit.common.msg.IMsgXMLNode;

/**
 * The BaseTxn class defines methods that are common to all 
 * transactions, including issuing server requests and handling 
 * server errors.
 * 
 * @author alan.huang
 * 
 */
public class BaseTxn {
	protected TPErrorHandling mvTPError;
	protected IMsgXMLNode mvReturnNode;
	protected ErrorCode mvErrorCode;

	/**
	 * Constructor for the base transaction class
	 */
	public BaseTxn() {
		mvTPError = new TPErrorHandling();
	}

	/**
	 * This method processes a request with a given request name and a
	 * set of parameters, using the default locale.
	 * @param pRequestName The name of the request, which corresponds to
	 *        a particular XML template.
	 * @param pTxnDataTable The list of parameters which are used to 
	 *        instantiate the request XML template.
	 * @return TPErrorHandling.TP_NORMAL if there is no error;
	 *         Error indicator if there is server error.
	 * @see TPErrorHandling       
	 */
	public final int process(String pRequestName, Hashtable pTxnDataTable) {
		// Begin Task #RC00181 - Rice Cheng 20081229
		// mvReturnNode =
		// ITradeServlet.getTPManager().getRequest(pRequestName).send(pTxnDataTable);
		// return mvTPError.checkError(mvReturnNode);
		return process(pRequestName, pTxnDataTable, TPBaseRequest.svLanguage_EN);
		// End Task #RC00181 - Rice Cheng 20081229
	}

	/**
	 * 
	 * This method processes a request with a given request name, a set of
	 * parameters and a given locale.
	 * @param pRequestName The name of the request, which corresponds to
	 *        a particular XML template.
	 * @param pTxnDataTable The list of parameters which are used to 
	 *        instantiate the request XML template.
	 * @param pLanguage The user locale.
	 * @return TPErrorHandling.TP_NORMAL if there is no error;
	 *         Error indicator if there is server error.
	 * @see TPErrorHandling  
	 */
	public final int process(String pRequestName, Hashtable pTxnDataTable, String pLanguage) {
		// Begin Task #RC00181 - Rice Cheng 20081229
		// mvReturnNode =
		// ITradeServlet.getTPManager().getRequest(pRequestName).send(pTxnDataTable);
		//mvReturnNode = ITradeServlet.getTPManager().getRequest(pRequestName).send(pTxnDataTable, pLanguage);
		mvReturnNode = ITradeServlet.getTPManager(pRequestName).getRequest(pRequestName).send(pTxnDataTable, pLanguage);
		//Log.println(" mvReturnNode for: " + pRequestName + " " +  mvReturnNode,	Log.ACCESS_LOG);
		// End Task 3RC00181 - Rice Cheng 20081229
		return mvTPError.checkError(mvReturnNode);
	}

	/**
	 * Returns the code indicating whether there is a server error.
	 * @return the code indicator.
	 * @see TPErrorHandling
	 */
	public final int getReturnCode() {
		return mvTPError.getStatus();
	}

	/**
	 * Returns the error message received from the server.
	 * @return the error message.
	 * @see TPErrorHandling
	 */
	public final String getErrorMessage() {
		return mvTPError.getErrDesc();
	}

	/**
	 * Sets the error code which can be retrieved by the client.
	 * @param pErrorCode The error code received from the server.
	 * @see TPErrorHandling
	 */
	protected final void setErrorCode(ErrorCode pErrorCode) {
		mvErrorCode = pErrorCode;
	}

	/**
	 * Returns the error code.
	 * @return the error code.
	 */
	public final ErrorCode getErrorCode() {
		if (mvErrorCode == null)
			return new ErrorCode(new String[0], 1080, "");
		return mvErrorCode;
	}
}
