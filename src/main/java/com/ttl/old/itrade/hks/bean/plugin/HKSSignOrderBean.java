package com.ttl.old.itrade.hks.bean.plugin;

public class HKSSignOrderBean {

	private String marketID;
	private String stockCode;
	private String stockName;
	private String settledBal;
	private String drawable;
	private String floor;
	private String holdQty;
	private String location;
		
	/**
	 * @return the marketID
	 */
	public String getMarketID() {
		return marketID;
	}
	/**
	 * @param marketID the marketID to set
	 */
	public void setMarketID(String marketID) {
		this.marketID = marketID;
	}
	
	/**
	 * @return the stockCode
	 */
	public String getStockCode() {
		return stockCode;
	}
	/**
	 * @param stockCode the stockCode to set
	 */
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	/**
	 * @return the stockName
	 */
	public String getStockName() {
		return stockName;
	}
	/**
	 * @param stockName the stockName to set
	 */
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	/**
	 * @return the settledBal
	 */
	public String getSettledBal() {
		return settledBal;
	}
	/**
	 * @param settledBal the settledBal to set
	 */
	public void setSettledBal(String settledBal) {
		this.settledBal = settledBal;
	}
	/**
	 * @return the drawable
	 */
	public String getDrawable() {
		return drawable;
	}
	/**
	 * @param drawable the drawable to set
	 */
	public void setDrawable(String drawable) {
		this.drawable = drawable;
	}
	/**
	 * @return the floor
	 */
	public String getFloor() {
		return floor;
	}
	/**
	 * @param floor the floor to set
	 */
	public void setFloor(String floor) {
		this.floor = floor;
	}
	/**
	 * @return the holdQty
	 */
	public String getHoldQty() {
		return holdQty;
	}
	/**
	 * @param holdQty the holdQty to set
	 */
	public void setHoldQty(String holdQty) {
		this.holdQty = holdQty;
	}
	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}
}
