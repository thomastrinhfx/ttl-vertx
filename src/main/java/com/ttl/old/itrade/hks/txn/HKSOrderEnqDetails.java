// ************************************
// Modified by Bowen Chau on 7 Mar 2006
// Added PendingQty methods
// ************************************

package com.ttl.old.itrade.hks.txn;

/**
 * Title:        Order Enquiry Details
 * Description:  This class stores the order enquiry details
 * Copyright:    Copyright (c) 2002
 * Company:      SYSTEK IT Ltd.
 * @author
 * @version 1.0
 * @since 2002.12.03
 */

import java.util.Hashtable;

import com.ttl.old.itrade.hks.util.TextFormatter;
/**
 * The HKSOrderEnqDetails class definition for all method 
 *  stores the order enquiry details
 * @author not attributable
 *
 */
public class HKSOrderEnqDetails
{

	//Start Task: yulong.xu 20080731
	/**
	 * Get method for instrument id
	 * @return instrument id
	 */
	public String getInstrumentId() {
		return mvInstrumentId;
	}
	/**
	 * Set method for instrument id
	 * @param pInstrumentId the instrument id
	 */
	public void setInstrumentId(String pInstrumentId) {
		this.mvInstrumentId = pInstrumentId;
	}
	/**
	 * Get method for short name
	 * @return short name
	 */
	public String getShortName() {
		return mvShortName;
	}
	/**
	 * Set method for short name
	 * @param pShortName the short name
	 */
	public void setShortName(String pShortName) {
		this.mvShortName = pShortName;
	}
	/**
	 * Get method for client remarks
	 * @return client remarks
	 */
	public String getClientRemarks() {
		return mvClientRemarks;
	}
	/**
	 * Set method for client remarks
	 * @param pClientRemarks the client remarks
	 */
	public void setClientRemarks(String pClientRemarks) {
		this.mvClientRemarks = pClientRemarks;
	}
	/**
	 * Get method for investor group id
	 * @return investor group id
	 */
	public String getInvestorGroupId() {
		return mvInvestorGroupId;
	}
	/**
	 * Set method for investor group id
	 * @param pInvestorGroupId the investor group id
	 */
	public void setInvestorGroupId(String pInvestorGroupId) {
		this.mvInvestorGroupId = pInvestorGroupId;
	}
	/**
	 * Get method for investor class id
	 * @return investor class id
	 */
	public String getInvestorClassId() {
		return mvInvestorClassId;
	}
	/**
	 * Set method for investor class id
	 * @param pInvestorClassId the investor class id
	 */
	public void setInvestorClassId(String pInvestorClassId) {
		this.mvInvestorClassId = pInvestorClassId;
	}
	/**
	 * Get method for RepOrter group id
	 * @return RepOrter group id
	 */
	public String getRepOrterGroupId() {
		return mvRepOrterGroupId;
	}
	/**
	 * Set method for RepOrter group id
	 * @param pRepOrterGroupId the RepOrter group id
	 */
	public void setRepOrterGroupId(String pRepOrterGroupId) {
		this.mvRepOrterGroupId = pRepOrterGroupId;
	}
	/**
	 * Get method for status internal
	 * @return  status internal
	 */
	public String getStatusInternal() {
		return mvStatusInternal;
	}
	/**
	 * Set method for status internal
	 * @param pStatusInternal the status internal
	 */
	public void setStatusInternal(String pStatusInternal) {
		this.mvStatusInternal = pStatusInternal;
	}
	/**
	 * Get method for create time
	 * @return create time
	 */
	public String getCreateTime() {
		return mvCreateTime;
	}
	/**
	 * Set method for create time
	 * @param pCreateTime the create time
	 */
	public void setCreateTime(String pCreateTime) {
		this.mvCreateTime = pCreateTime;
	}
	/**
	 * Get method for reporter time
	 * @return reporter time
	 */
	public String getReporTime() {
		return mvReporTime;
	}
	/**
	 * Set method for reporter time
	 * @param pReporTime the reporter time
	 */
	public void setReporTime(String pReporTime) {
		this.mvReporTime = pReporTime;
	}
	/**
	 * Get method for reporter tack time 
	 * @return reporter tack time 
	 */
	public String getReporTackTime() {
		return mvReporTackTime;
	}
	/**
	 * Set method for reporter tack time 
	 * @param pReporTackTime the reporter tack time 
	 */
	public void setReporTackTime(String pReporTackTime) {
		this.mvReporTackTime = pReporTackTime;
	}
	/**
	 * Get method for ogack time
	 * @return ogack time
	 */
	public String getOgackTime() {
		return mvOgackTime;
	}
	/**
	 * Set method for ogack time
	 * @param pOgackTime the ogack time
	 */
	public void setOgackTime(String pOgackTime) {
		this.mvOgackTime = pOgackTime;
	}
	/**
	 * Get method for trading time
	 * @return trading time
	 */
	public String getTradeTime() {
		return mvTradeTime;
	}
	/**
	 * Set method for trading time
	 * @param pTradeTime the trading time
	 */
	public void setTradeTime(String pTradeTime) {
		this.mvTradeTime = pTradeTime;
	}
	/**
	 * Get method for last trading time
	 * @return last trading time
	 */
	public String getLastTradeTime() {
		return mvLastTradeTime;
	}
	/**
	 * Set method for last trading time
	 * @param pLastTradeTime the last trading time
	 */
	public void setLastTradeTime(String pLastTradeTime) {
		this.mvLastTradeTime = pLastTradeTime;
	}
	/**
	 * Get method for stop trigger time
	 * @return stop trigger time
	 */
	public String getStopTriggerTime() {
		return mvStopTriggerTime;
	}
	/**
	 * Set method for stop trigger time
	 * @param pStopTriggerTime the stop trigger time
	 */
	public void setStopTriggerTime(String pStopTriggerTime) {
		this.mvStopTriggerTime = pStopTriggerTime;
	}
	/**
	 * Get method for all or nothing
	 * @return all or nothing
	 */
	public String getAllorNothing() {
		return mvAllorNothing;
	}
	/**
	 * Set method for all or nothing
	 * @param pAllorNothing the all or nothing
	 */
	public void setAllorNothing(String pAllorNothing) {
		this.mvAllorNothing = pAllorNothing;
	}
	/**
	 * Get method for host id
	 * @return host id
	 */
	public String getHostId() {
		return mvHostId;
	}
	/**
	 * Set method for host id
	 * @param pHostId the host id
	 */
	public void setHostId(String pHostId) {
		this.mvHostId = pHostId;
	}
	/**
	 * Get method for in active
	 * @return in active
	 */
	public String getInActive() {
		return mvInActive;
	}
	/**
	 * Set method for in active
	 * @param pInActive the in active
	 */
	public void setInActive(String pInActive) {
		this.mvInActive = pInActive;
	}
	/**
	 * Get method for notified flag
	 * @return notified flag
	 */
	public String getNotifiedFlag() {
		return mvNotifiedFlag;
	}
	/**
	 * Set method for notified flag
	 * @param pNotifiedFlag the notified flag
	 */
	public void setNotifiedFlag(String pNotifiedFlag) {
		this.mvNotifiedFlag = pNotifiedFlag;
	}
	/**
	 * Get method for is post executed order
	 * @return is post executed order
	 */
	public String getIsPostExecutedOrder() {
		return mvIsPostExecutedOrder;
	}
	/**
	 * Set method for is post executed order
	 * @param pIsPostExecutedOrder the is post executed order
	 */
	public void setIsPostExecutedOrder(String pIsPostExecutedOrder) {
		this.mvIsPostExecutedOrder = pIsPostExecutedOrder;
	}
	/**
	 * Get method for supervisor id
	 * @return supervisor id
	 */
	public String getSupervisorId() {
		return mvSupervisorId;
	}
	/**
	 * Set method for supervisor id
	 * @param pSupervisorId the supervisor id
	 */
	public void setSupervisorId(String pSupervisorId) {
		this.mvSupervisorId = pSupervisorId;
	}
	/**
	 * Get method for approval remark
	 * @return approval remark
	 */
	public String getApprovalRemark() {
		return mvApprovalRemark;
	}
	/**
	 * Set method for approval remark
	 * @param pApprovalRemark the approval remark
	 */
	public void setApprovalRemark(String pApprovalRemark) {
		this.mvApprovalRemark = pApprovalRemark;
	}
	/**
	 * Get method for hold confirm quantity
	 * @return hold confirm quantity
	 */
	public String getHoldConfirmQty() {
		return mvHoldConfirmQty;
	}
	/**
	 * Set method for hold confirm quantity
	 * @param pHoldConfirmQty the hold confirm quantity
	 */
	public void setHoldConfirmQty(String pHoldConfirmQty) {
		this.mvHoldConfirmQty = pHoldConfirmQty;
	}
	/**
	 * Get method for trading consideration
	 * @return trading consideration
	 */
	public String getTradeConsideration() {
		return mvTradeConsideration;
	}
	/**
	 * Set method for trading consideration
	 * @param pTradeConsideration the trading consideration
	 */
	public void setTradeConsideration(String pTradeConsideration) {
		this.mvTradeConsideration = pTradeConsideration;
	}
	/**
	 * Get method for pend action
	 * @return pend action
	 */
	public String getPendAction() {
		return mvPendAction;
	}
	/**
	 * Set method for pend action
	 * @param pPendAction the pend action
	 */
	public void setPendAction(String pPendAction) {
		this.mvPendAction = pPendAction;
	}
	/**
	 * Get method for exceeded amount
	 * @return exceeded amount
	 */
	public String getExceededAmt() {
		return mvExceededAmt;
	}
	/**
	 * Set method for exceeded amount
	 * @param pExceededAmt the exceeded amount
	 */
	public void setExceededAmt(String pExceededAmt) {
		this.mvExceededAmt = pExceededAmt;
	}
	/**
	 * Get method for last modified user id
	 * @return last modified user id
	 */
	public String getLastModifiedUserId() {
		return mvLastModifiedUserId;
	}
	/**
	 * Set method for last modified user id
	 * @param pLastModifiedUserId the last modified user id
	 */
	public void setLastModifiedUserId(String pLastModifiedUserId) {
		this.mvLastModifiedUserId = pLastModifiedUserId;
	}
	/**
	 * Get method for approval time
	 * @return approval time
	 */
	public String getApprovalTime() {
		return mvApprovalTime;
	}
	/**
	 * Set method for approval time
	 * @param pApprovalTime the approval time
	 */
	public void setApprovalTime(String pApprovalTime) {
		this.mvApprovalTime = pApprovalTime;
	}
	/**
	 * Get method for prevtrade consideration
	 * @return prevtrade consideration
	 */
	public String getPrevtradeConsideration() {
		return mvPrevtradeConsideration;
	}
	/**
	 * Set method for prevtrade consideration
	 * @param pPrevtradeConsideration prevtrade consideration
	 */
	public void setPrevtradeConsideration(String pPrevtradeConsideration) {
		this.mvPrevtradeConsideration = pPrevtradeConsideration;
	}
	/**
	 * Get method for is released
	 * @return is released
	 */
	public String getIsReleased() {
		return mvIsReleased;
	}
	/**
	 * Set method for is released
	 * @param pIsReleased the is released
	 */
	public void setIsReleased(String pIsReleased) {
		this.mvIsReleased = pIsReleased;
	}
	/**
	 * Get method for Supervisorre jected
	 * @return Supervisorre jected
	 */
	public String getSupervisorrejected() {
		return mvSupervisorrejected;
	}
	/**
	 * Set method for Supervisorre jected
	 * @param pSupervisorrejected the Supervisorre jected
	 */
	public void setSupervisorrejected(String pSupervisorrejected) {
		this.mvSupervisorrejected = pSupervisorrejected;
	}

	/**
	 * Get method for date time
	 * @return date time
	 */
	public String getMvDateTime() {
		return mvDateTime;
	}
	/**
	 * Set method for date time
	 * @param mvDateTime the date time
	 */
	public void setMvDateTime(String mvDateTime) {
		this.mvDateTime = mvDateTime;
	}

    private String mvDateTime;
	private String mvInstrumentId;
	private String mvShortName;
	private String mvClientRemarks;
	private String mvInvestorGroupId;
	private String mvInvestorClassId;
	private String mvRepOrterGroupId;
	private String mvStatusInternal;
	private String mvCreateTime;
	private String mvReporTime;
	private String mvReporTackTime;
	private String mvOgackTime;
	private String mvTradeTime;
	private String mvLastTradeTime;
	private String mvStopTriggerTime;
	private String mvAllorNothing;
	private String mvHostId;
	private String mvInActive;
	private String mvNotifiedFlag;
	private String mvIsPostExecutedOrder;
	private String mvApprovalTime;
	private String mvSupervisorId;
	private String mvApprovalRemark;
	private String mvHoldConfirmQty;
	private String mvTradeConsideration;
	private String mvPrevtradeConsideration;
	private String mvIsReleased;
	private String mvSupervisorrejected;
	private String mvPendAction;
	private String mvExceededAmt;
	private String mvLastModifiedUserId;

	//End Task: yulong.xu 20080731

	private String mvOrderID;
	private String mvOrderGroupID;
	private String mvDNSeq;
	private String mvUserID;
	private String mvEntityID;
	private String mvOrderType;
	private String mvStockID;
	private String mvClientID;
	private String mvBS;
	private String mvPrice;
	private String mvQty;
	private String mvPendingPrice;
	private String mvPendingQty;
	private String mvCancelQty;
    private String mvOSQty;
	private String mvFilledQty;
	private String mvAON;
	private String mvStatus;
    private String mvStatus_Internal;
	private String mvRejectReason;
	private String mvApprovalReason;
//    private String mvCreateTime;
	private String mvInputTime;
    private String mvModifiedTime;
	private String mvBrokerID;
    //private String mvAllOrNothing;
	private String mvOrigin;
	private String mvHedge;
	private String mvShortsell;
    //private String mvHostID;
	private String mvValidityDate;
	private String mvStopOrderType;
	private String mvStopPrice;
	private String mvStopOrderExpiryDate;
	private String mvIsPriceWarningResubmit;
	private String mvChannelID;
	private String mvBranchID;
	private String mvIsOddLot;
   private String mvIsManualTrade;
   private String mvActivationDate;
   private String mvGoodTillDate;
   private String mvAvgPrice;
   private String mvRemark;
   private String mvContactPhone;
   private String mvGrossAmt;
   private String mvNetAmt;
   private String mvSCRIP;
   private String mvModifyOrderID;
   private String mvCurrencyID;
   //BEGIN Task #: WL00619 Walter Lau 27 July 2007
   private String mvMarketID;
   //END Task #: WL00619 Walter Lau 27 July 2007

   // BEGIN - TASK#: CL00030 - Charlie Liu 20081023
   private String mvInstrumentShortName;
   private String mvModifiedDate;
   private String mvModifiedDateTime;
   //END - TASK# CL00030
   
   // BEGIN - TASK: Giang Tran 20100507: Add the settlement account for order details
   private String mvBankID;
   /**
	* @return the mvBankID
	*/
	public String getMvBankID() {
		return mvBankID;
	}
	/**
	 * @param mvBankID the mvBankID to set
	 */
	public void setMvBankID(String mvBankID) {
		this.mvBankID = mvBankID;
	}
	
	private String mvBankACID;
	/**
	 * @return the mvBankACID
	 */
	public String getMvBankACID() {
		return mvBankACID;
	}
	/**
	 * @param mvBankACID the mvBankACID to set
	 */
	public void setMvBankACID(String mvBankACID) {
		this.mvBankACID = mvBankACID;
	}
   // END - TASK: Giang Tran 20100507: Add the settlement account for order details


//
//	private String		orderId;
//	private String		clientId;
//	private String		seriesId;
//	private String		product;
//	private String		contractMonth;
//	private String		strike;
//	private String		callPut;
//	private String		bs;
//	private String		price;
//	private String		averagePrice;
//	private String		quantity;
//	private String		osQuantity;
//	private String		filledQuantity;
//	private String		status;
//	private String		validity;
//	private String		validityDate;
//	private String		pendAction;
//	private String		pendQuantity;
//	private String		pendValidity;
//	private String		pendValidityDate;
//	private String		cust;
//	private String		brokerId;
//	private String		createTime;
//	private String		modifyTime;
//	private String		entityId;
//	private String		userId;
//	private String		dnSeq;
//	private String		uuId;
//	private String		orderType;
//	private String		position;
//	private String		pendPosition;
//	private String		ao;
//	private String		cnv;
//	private String		stop;
//	private String		stopPrice;
//	private String		pendStopPrice;
//	private String		remarks;
//	private String		pendRemarks;
//	private String		rejectReason;
//
//
	private Hashtable   mvHMap;
//
//	public String getOrderId()
//	{
//		return orderId;
//	}
//
//	public void setOrderId(String orderId)
//	{
//		this.orderId = orderId;
//	}
//
//
//	public String getClientId()
//	{
//		return clientId;
//	}
//
//	public void setClientId(String clientId)
//	{
//		this.clientId = clientId;
//	}
//
//
//	public String getSeriesId()
//	{
//		return seriesId;
//	}
//
//	public void setSeriesId(String seriesId)
//	{
//		this.seriesId = seriesId;
//	}
//
//
//	public String getProduct()
//	{
//		return product;
//	}
//
//	public void setProduct(String product)
//	{
//		this.product = product;
//	}
//
//
//	public String getContractMonth()
//	{
//		return contractMonth;
//	}
//
//	public void setContractMonth(String contractMonth)
//	{
//		this.contractMonth = contractMonth;
//	}
//
//
//	public String getStrike()
//	{
//		return strike;
//	}
//
//	public void setStrike(String strike)
//	{
//		this.strike = strike;
//	}
//
//
//	public String getCallPut()
//	{
//		return callPut;
//	}
//
//	public void setCallPut(String callPut)
//	{
//		this.callPut = callPut;
//	}
//
//
//	public String getBS()
//	{
//		return bs;
//	}
//
//	public void setBS(String bs)
//	{
//		this.bs = bs;
//	}
//
//
//	public String getPrice()
//	{
//		return price;
//	}
//
//	public void setPrice(String price)
//	{
//		/***	comment by May on 16-01-2004
//		this.price = price;
//		*************************************/
//
//		/***	added by May on 16-01-2004	***/
//		this.price = TextFormatter.convertPrice(price);
//		/*****************************************/
//
//	}
//
//
//	public String getAveragePrice()
//	{
//		return averagePrice;
//	}
//
//	public void setAveragePrice(String averagePrice)
//	{
//		/***	comment by MAy on 20-01-2004
//		this.averagePrice = averagePrice;
//		**************************************/
//
//		/***	added by May on 27-01-2004	***/
//		this.averagePrice = TextFormatter.convertPrice(averagePrice);
//		/******************************************/
//	}
//
//
//	public String getQuantity()
//	{
//		return quantity;
//	}
//
//	public void setQuantity(String quantity)
//	{
//		this.quantity = quantity;
//	}
//
//
//	public String getOsQuantity()
//	{
//		return osQuantity;
//	}
//
//	public void setOsQuantity(String osQuantity)
//	{
//		this.osQuantity = osQuantity;
//	}
//
//
//	public String getFilledQuantity()
//	{
//		return filledQuantity;
//	}
//
//	public void setFilledQuantity(String filledQuantity)
//	{
//		this.filledQuantity = filledQuantity;
//	}
//
//
//	public String getStatus()
//	{
//		return status;
//	}
//
//	public void setStatus(String status)
//	{
//		this.status = status;
//	}
//
//
//	public String getValidity()
//	{
//		return validity;
//	}
//
//	public void setValidity(String validity)
//	{
//		this.validity = validity;
//	}
//
//
//	public String getValidityDate()
//	{
//		return validityDate;
//	}
//
//	public void setValidityDate(String validityDate)
//	{
//		this.validityDate = validityDate;
//	}
//
//
//	public String getPendAction()
//	{
//		return pendAction;
//	}
//
//	public void setPendAction(String pendAction)
//	{
//		this.pendAction = pendAction;
//	}
//
//
//	public String getPendQuantity()
//	{
//		return pendQuantity;
//	}
//
//	public void setPendQuantity(String pendQuantity)
//	{
//		this.pendQuantity = pendQuantity;
//	}
//
//
//	public String getPendValidity()
//	{
//		return pendValidity;
//	}
//
//	public void setPendValidity(String pendValidity)
//	{
//		this.pendValidity = pendValidity;
//	}
//
//	public String getPendValidityDate()
//	{
//		return pendValidityDate;
//	}
//
//	public void setPendValidityDate(String pendValidityDate)
//	{
//		this.pendValidityDate = pendValidityDate;
//	}
//
//
//	public String getCust()
//	{
//		return cust;
//	}
//
//	public void setCust(String cust)
//	{
//		this.cust = cust;
//	}
//
//
//	public String getBrokerId()
//	{
//		return brokerId;
//	}
//
//	public void setBrokerId(String brokerId)
//	{
//		this.brokerId = brokerId;
//	}
//
//
//	public String getCreateTime()
//	{
//		return createTime;
//	}
//
//	public void setCreateTime(String createTime)
//	{
//		this.createTime = createTime;
//	}
//
//
//	public String getModifyTime()
//	{
//		return modifyTime;
//	}
//
//	public void setModifyTime(String modifiyTime)
//	{
//		this.modifyTime = modifyTime;
//	}
//
//
//	public String getEntityId()
//	{
//		return entityId;
//	}
//
//	public void setEntityId(String entityId)
//	{
//		this.entityId = entityId;
//	}
//
//
//	public String getUserId()
//	{
//		return userId;
//	}
//
//	public void setUserId(String userId)
//	{
//		this.userId = userId;
//	}
//
//
//	public String getDnSeq()
//	{
//		return dnSeq;
//	}
//
//	public void setDnSeq(String dnSeq)
//	{
//		this.dnSeq = dnSeq;
//	}
//
//
//	public String getUuId()
//	{
//		return uuId;
//	}
//
//	public void setUuId(String uuId)
//	{
//		this.uuId = uuId;
//	}
//
//
//	public String getOrderType()
//	{
//		return orderType;
//	}
//
//	public void setOrderType(String orderType)
//	{
//		this.orderType = orderType;
//	}
//
//
//	public String getPosition()
//	{
//		return position;
//	}
//
//	public void setPosition(String position)
//	{
//		this.position = position;
//	}
//
//
//	public String getPendPosition()
//	{
//		return pendPosition;
//	}
//
//	public void setPendPosition(String pendPosition)
//	{
//		this.pendPosition = pendPosition;
//	}
//
//
//	public String getAO()
//	{
//		return ao;
//	}
//
//	public void setAO(String ao)
//	{
//		this.ao = ao;
//	}
//
//
//	public String getCnv()
//	{
//		return cnv;
//	}
//
//	public void setCnv(String cnv)
//	{
//		this.cnv = cnv;
//	}
//
//
//	public String getStop()
//	{
//		return stop;
//	}
//
//	public void setStop(String stop)
//	{
//		this.stop = stop;
//	}
//
//
//	public String getStopPrice()
//	{
//		return stopPrice;
//	}
//
//	public void setStopPrice(String stopPrice)
//	{
//		/***	comment by May on 27-01-2004
//		this.stopPrice = stopPrice;
//		***********************************/
//
//		/***	added by May on 27-01-2004	***/
//		this.stopPrice = TextFormatter.convertPrice(stopPrice);
//		/******************************************/
//	}
//
//
//	public String getPendStopPrice()
//	{
//		return pendStopPrice;
//	}
//
//	public void setPendStopPrice(String pendStopPrice)
//	{
//		this.pendStopPrice = pendStopPrice;
//	}
//
//
//	public String getRemarks()
//	{
//		return remarks;
//	}
//
//	public void setRemarks(String remarks)
//	{
//		this.remarks = remarks;
//	}
//
//
//	public String getPendRemarks()
//	{
//		return pendRemarks;
//	}
//
//	public void setPendRemarks(String pendRemarks)
//	{
//		this.pendRemarks = pendRemarks;
//	}
//
//
//	public String getRejectReason()
//	{
//		return rejectReason;
//	}
//
//	public void setRejectReason(String rejectReason)
//	{
//		this.rejectReason = rejectReason;
//	}
	/**
	 * Get method for order group id
	 * @return order group id
	 */
	public String getOrderGroupID()
	{
		return mvOrderGroupID;
	}
	/**
	 * Get method for order id
	 * @return order id
	 */
	public String getOrderID()
	{
		return mvOrderID;
	}

//	public String getOriginalOrderID()
//	{
//		return mvOriginalOrderID;
//	}
//
//	public String getModifyOrderID()
//	{
//		return mvModifyOrderID;
//	}
//
//	public String getOrderSeqNum()
//	{
//		return mvOrderSeqNum;
//	}
	/**
	 * Get method for entity id
	 * @return entity id
	 */
	public String getEntityID()
	{
		return mvEntityID;
	}
	/**
	 * Get method for branch id
	 * @return branch id
	 */
	public String getBranchID()
	{
		return mvBranchID;
	}
	/**
	 * Get method for channel id
	 * @return channel id
	 */
	public String getChannelID()
	{
		return mvChannelID;
	}

//	public String getTerminalID()
//	{
//		return mvTerminalID;
//	}
	/**
	 * Get method for user id
	 * @return user id
	 */
	public String getUserID()
	{
		return mvUserID;
	}

//	public int getTradingAccSeq()
//	{
//		return mvTradingAccSeq;
//	}
//
	/**
	 * Get method for client id
	 * @return client id
	 */
	public String getClientID()
	{
		return mvClientID;
	}
	/**
	 * Get method for status 
	 * @return status
	 */
	public String getStatus()
	{
		return mvStatus;
	}
	/**
	 * Get method for stock id
	 * @return stock id
	 */
	public String getStockID()
	{
		return mvStockID;
	}
	/**
	 * Get method for buy or sell
	 * @return buy or sell
	 */
	public String getBS()
	{
		return mvBS;
	}
	/**
	 * Get method for order type
	 * @return order type
	 */
	public String getOrderType()
	{
		return mvOrderType;
	}
	/**
	 * Get method for price per share
	 * @return price per share
	 */
	public String getPrice()
	{
		return mvPrice;
	}
	/**
	 * Get method for quantity
	 * @return quantity
	 */
	public String getQty()
	{
		return mvQty;
	}
	/**
	 * Get method for pending price 
	 * @return pending price
	 */
	public String getPendingPrice() {
		return mvPendingPrice;
	}

	// Added by Bowen Chau on 7 Mar 2006
	/**
	 * Get method for pending quantity
	 * @return pending quantity
	 */
	public String getPendingQty() {
		return mvPendingQty;
	}
	/**
	 * Get method for cancel quantity
	 * @return cancel quantity
	 */
	public String getCancelQty()
	{
		return mvCancelQty;
	}
	/**
	 * Get method for Security quantity
	 * @return security quantity
	 */
	public String getOSQty()
	{
		return mvOSQty;
	}
	/**
	 * Get method for filled quantity
	 * @return filled quantity
	 */
	public String getFilledQty()
	{
		return mvFilledQty;
	}
	/**
	 * Get method for all or nothing
	 * @return all or nothing
	 */
	public String getAON()
	{
		return mvAON;
	}
	/**
	 * Get method for origin
	 * @return origin
	 */
	public String getOrigin()
	{
		return mvOrigin;
	}
	/**
	 * Get method for hedge
	 * @return hedge
	 */
	public String getHedge()
	{
		return mvHedge;
	}
	/**
	 * Get method for short sell
	 * @return short sell
	 */
	public String getShortsell()
	{
		return mvShortsell;
	}

//	public String getBrokerComment()
//	{
//		return mvBrokerComment;
//	}
	/**
	 * Get method for is manual trading
	 * @return is manual trading
	 */
	public String getIsManualTrade()
	{
		return mvIsManualTrade;
	}
	/**
	 * Get method for is odd lot
	 * @return is odd lot
	 */
	public String getIsOddLot()
	{
		return mvIsOddLot;
	}

//	public String getDirectFlag()
//	{
//		return mvDirectFlag;
//	}
//
//	public String getSettleType()
//	{
//		return mvSettleType;
//	}
//
//	public String getTradeIndicator()
//	{
//		return mvTradeIndicator;
//	}
//
//	public String getMTBrokerNum()
//	{
//		return mvMTBrokerNum;
//	}
//
//	public String getOverseaPurchase()
//	{
//		return mvOverseaPurchase;
//	}
	/**
	 * Get method for validity date
	 * @return validity date
	 */
	public String getValidityDate()
	{
		return mvValidityDate;
	}
	/**
	 * Get method for stop order type
	 * @return stop order type
	 */
	public String getStopOrderType()
	{
		return mvStopOrderType;
	}
	/**
	 * Get method for stop price
	 * @return stop price
	 */
	public String getStopPrice()
	{
		return mvStopPrice;
	}
	/**
	 * Get method for stop order ExpDate
	 * @return stop order ExpDate
	 */
	public String getStopOrderExpDate()
	{
		return mvStopOrderExpiryDate;
	}
	/**
	 * Get method for reject reason
	 * @return reject reason
	 */
	public String getRejectReason()
	{
		return mvRejectReason;
	}

//	public String getConfirmFlag()
//	{
//		return mvConfirmFlag;
//	}
//
//	public String getPendAction()
//	{
//		return mvPendAction;
//	}
//
//	public double getPendPrice()
//	{
//		return mvPendPrice;
//	}
//
//	public long getPendQty()
//	{
//		return mvPendQty;
//	}
//
//	public String getPendTCOrigin()
//	{
//		return mvPendTCOrigin;
//	}
//
//	public String getPendTCHedge()
//	{
//		return mvPendTCHedge;
//	}
//
//	public String getPendTCShortsell()
//	{
//		return mvPendTCShortsell;
//	}
//
//	public String getPendCancelID()
//	{
//		return mvPendCancelID;
//	}
	/**
	 * Get method for broker id
	 * @return broker id
	 */
	public String getBrokerID()
	{
		return mvBrokerID;
	}

//	public String getSupervisorID()
//	{
//		return mvSupervisorID;
//	}
	/**
	 * Get method for approval reason
	 * @return approval reason
	 */
	public String getApprovalReason()
	{
		return mvApprovalReason;
	}

//	public Timestamp getApprovalTime()
//	{
//		return mvApprovalTime;
//	}
//
//	public double getTradeConsideration()
//	{
//		return mvTradeConsideration;
//	}
//
//	public double getHoldAmt()
//	{
//		return mvHoldAmt;
//	}
//
//	public long getHoldQty()
//	{
//		return mvHoldQty;
//	}
//
//	public double getHoldRequestAmt()
//	{
//		return mvHoldRequestAmt;
//	}
//
//	public long getHoldRequestQty()
//	{
//		return mvHoldRequestQty;
//	}
//
//	public double getHoldConfirmAmt()
//	{
//		return mvHoldConfirmAmt;
//	}
//
//	public double getHoldConfirmSSAmt()
//	{
//		return mvHoldConfirmSSAmt;
//	}
//
//	public long getHoldConfirmQty()
//	{
//		return mvHoldConfirmQty;
//	}
//
//	public long getHoldConfirmSSQty()
//	{
//		return mvHoldConfirmSSQty;
//	}
	/**
	 * Get method for is price warning result submit
	 * @return is price warning result submit
	 */
	public String getIsPriceWarningResubmit()
	{
		return mvIsPriceWarningResubmit;
	}
	/**
	 * Get method for input time
	 * @return input time
	 */
	public String getInputTime()
	{
		return mvInputTime;
	}
	/**
	 * Get method for modified time
	 * @return modified time
	 */
	public String getModifiedTime()
	{
		return mvModifiedTime;
	}

	// BEGIN - TASK#: CL00023 - Charlie Liu 20081008
	/**
	 * Get method for modified date
	 * @return modified date
	 */
	public String getModifiedDate()
	{
		return mvModifiedDate;
	}
	//END - TASK# CL00023
	/**
	 * Get method for DN sequence
	 * @return DN sequence
	 */
	public String getDNSeq()
	{
		return mvDNSeq;
	}
	/**
	 * Get method for activation date
	 * @return activation date
	 */
    public String getActivationDate()
    {
        return mvActivationDate;
    }
    /**
     * Get method for good till date
     * @return good till date
     */
    public String getGoodTillDate()
    {
        return mvGoodTillDate;
    }
    /**
     * Get method for AVG price
     * @return AVG price
     */
    public String getAvgPrice()
    {
        return mvAvgPrice;
    }
    /**
     * Get method for script flag
     * @return script flag
     */
    public String getSCRIP()
    {
        return mvSCRIP;
    }
    /**
     * Get method for net amount
     * @return net amount
     */
    public String getNetAmt()
    {
       return mvNetAmt;
    }
    /**
     * Get method for gross amount
     * @return gross amount
     */
    public String getGrossAmt()
    {
       return mvGrossAmt;
    }
    /**
     * Get method for contact phone
     * @return contact phone
     */
    public String getContactPhone()
    {
       return mvContactPhone;
    }
    /**
     * Get method for remark
     * @return remark
     */
    public String getRemark()
    {
       return mvRemark;
    }
    /**
     * Get method for modify order id 
     * @return modify order id
     */
    public String getMofifyOrderID()
    {
    	return mvModifyOrderID;
    }

    //BEGIN Task #: WL00619 Walter Lau 27 July 2007
    /**
     * Get method for market id
     * @return market id
     */
    public String getMarketID()
	{
		return mvMarketID;
	}
        //End Task #: WL00619 Walter Lau 27 July 2007

//	public BigDecimal getUUID()
//	{
//		return mvUUID;
//	}
    /**
     * Set method for group order id
     * @param pOrderGroupID the group order id
     */
	public void setOrderGroupID(String pOrderGroupID)
	{
		mvOrderGroupID = pOrderGroupID;
	}
	/**
	 * Set method for order id
	 * @param pOrderID the order id
	 */
	public void setOrderID(String pOrderID)
	{
		mvOrderID = pOrderID;
	}

//	public void setOriginalOrderID(String pOriginalOrderID)
//	{
//		mvOriginalOrderID = pOriginalOrderID;
//	}
//
//	public void setModifyOrderID(String pModifyOrderID)
//	{
//		mvModifyOrderID = pModifyOrderID;
//	}
//
//	public void setOrderSeqNum(String pOrderSeqNum)
//	{
//		mvOrderSeqNum = pOrderSeqNum;
//	}
	/**
	 * Set method for entity id
	 * @param pEntityID the entity id
	 */
	public void setEntityID(String pEntityID)
	{
		mvEntityID = pEntityID;
	}
	/**
	 * Set method for branch id
	 * @param pBranchID the branch id
	 */
	public void setBranchID(String pBranchID)
	{
		mvBranchID = pBranchID;
	}
	/**
	 * Set method for channel id
	 * @param pChannelID the channel id
	 */
	public void setChannelID(String pChannelID)
	{
		mvChannelID = pChannelID;
	}

//	public void setTerminalID(String pTerminalID)
//	{
//		mvTerminalID = pTerminalID;
//	}
	/**
	 * Set method for user id
	 * @param pUserID the user id
	 */
	public void setUserID(String pUserID)
	{
		mvUserID = pUserID;
	}

//	public void setTradingAccSeq(int pTradingAccSeq)
//	{
//		mvTradingAccSeq = pTradingAccSeq;
//	}
	/**
	 * Set method for client id
	 * @param pClientID the client id
	 */
	public void setClientID(String pClientID)
	{
		mvClientID = pClientID;
	}
	/**
	 * Set method for status
	 * @param pStatus the status
	 */
	public void setStatus(String pStatus)
	{
		mvStatus = pStatus;
	}
	/**
	 * Set method for stock id
	 * @param pStockID the stock id
	 */
	public void setStockID(String pStockID)
	{
		mvStockID = pStockID;
	}
	/**
	 * Set method for buy or sell
	 * @param pBS the buy or sell
	 */
	public void setBS(String pBS)
	{
		mvBS = pBS;
	}
	/**
	 * Set method for order type
	 * @param pOrderType the order type
	 */
	public void setOrderType(String pOrderType)
	{
		mvOrderType = pOrderType;
	}
	/**
	 * Set method for price per share
	 * @param pPrice the price per share
	 */
	public void setPrice(String pPrice)
	{
		mvPrice = pPrice;
		mvPrice = TextFormatter.convertPrice(pPrice);
	}
	/**
	 * Set method for quantity
	 * @param pQty the quantity
	 */
	public void setQty(String pQty)
	{
		mvQty = pQty;
	}
	/**
	 * Set method for pending price
	 * @param pPendingPrice the pending price
	 */
	public void setPendingPrice(String pPendingPrice) {
		mvPendingPrice = pPendingPrice;
	}

	// Added by Bowen Chau on 7 Mar 2006
	/**
	 * Set method for pending quantity
	 * @param pPendingQty the pending quantity
	 */
	public void setPendingQty(String pPendingQty) {
		mvPendingQty = pPendingQty;
	}
	/**
	 * Set method for cancel quantity
	 * @param pCancelQty the cancel quantity
	 */
	public void setCancelQty(String pCancelQty)
	{
		mvCancelQty = pCancelQty;
	}
	/**
	 * Set method for OS quantity
	 * @param pOSQty the OS quantity
	 */
	public void setOSQty(String pOSQty)
	{
		mvOSQty = pOSQty;
	}
	/**
	 * Set method for filled quantity
	 * @param pFilledQty the filled quantity
	 */
	public void setFilledQty(String pFilledQty)
	{
		mvFilledQty = pFilledQty;
	}
	/**
	 * Set method for all or nothing
	 * @param pAON the all or nothing
	 */
	public void setAON(String pAON)
	{
		mvAON = pAON;
	}
	/**
	 * Set method for origin
	 * @param pOrigin the origin
	 */
	public void setOrigin(String pOrigin)
	{
		mvOrigin = pOrigin;
	}
	/**
	 * Set method for hedge
	 * @param pHedge the hedge
	 */
	public void setHedge(String pHedge)
	{
		mvHedge = pHedge;
	}
	/**
	 * Set method for short sell
	 * @param pShortsell the short sell
	 */
	public void setShortsell(String pShortsell)
	{
		mvShortsell = pShortsell;
	}

//	public void setBrokerComment(String pBrokerComment)
//	{
//		mvBrokerComment = pBrokerComment;
//	}
	/**
	 * Set method for is manual trading
	 * @param pIsManualTrade the is manual trading
	 */
	public void setIsManualTrade(String pIsManualTrade)
	{
		mvIsManualTrade = pIsManualTrade;
	}
	/**
	 * Set method for is odd lot
	 * @param pIsOddLot the is odd lot
	 */
	public void setIsOddLot(String pIsOddLot)
	{
		mvIsOddLot = pIsOddLot;
	}

//	public void setDirectFlag(String pDirectFlag)
//	{
//		mvDirectFlag = pDirectFlag;
//	}
//
//	public void setSettleType(String pSettleType)
//	{
//		mvSettleType = pSettleType;
//	}
//
//	public void setTradeIndicator(String pTradeIndicator)
//	{
//		mvTradeIndicator = pTradeIndicator;
//	}
//
//	public void setMTBrokerNum(String pMTBrokerNum)
//	{
//		mvMTBrokerNum = pMTBrokerNum;
//	}
//
//	public void setOverseaPurchase(String pOverseaPurchase)
//	{
//		mvOverseaPurchase = pOverseaPurchase;
//	}
	/**
	 * Set method for validity date
	 * @param pValidityDate the validity date 
	 */
	public void setValidityDate(String pValidityDate)
	{
		mvValidityDate = pValidityDate;
	}
	/**
	 * Set method for stop order type
	 * @param pStopOrderType the stop order type
	 */
	public void setStopOrderType(String pStopOrderType)
	{
		mvStopOrderType = pStopOrderType;
	}
	/**
	 * Set method for stop price
	 * @param pStopPrice the stop price
	 */
	public void setStopPrice(String pStopPrice)
	{
		mvStopPrice = pStopPrice;
	}
	/**
	 * Set method for stop order expiry date
	 * @param pStopOrderExpiryDate
	 */
	public void setStopOrderExpDate(String pStopOrderExpiryDate)
	{
		mvStopOrderExpiryDate = pStopOrderExpiryDate;
	}
	/**
	 * Set method for reject reason
	 * @param pRejectReason the reject reason
	 */
	public void setRejectReason(String pRejectReason)
	{
		mvRejectReason = pRejectReason;
	}

//	public void setConfirmFlag(String pConfirmFlag)
//	{
//		mvConfirmFlag = pConfirmFlag;
//	}
//
//	public void setPendAction(String pPendAction)
//	{
//		mvPendAction = pPendAction;
//	}
//
//	public void setPendPrice(double pPendPrice)
//	{
//		mvPendPrice = pPendPrice;
//	}
//
//	public void setPendQty(long pPendQty)
//	{
//		mvPendQty = pPendQty;
//	}
//
//	public void setPendTCOrigin(String pPendTCOrigin)
//	{
//		mvPendTCOrigin = pPendTCOrigin;
//	}
//
//	public void setPendTCHedge(String pPendTCHedge)
//	{
//		mvPendTCHedge = pPendTCHedge;
//	}
//
//	public void setPendTCShortsell(String pPendTCShortsell)
//	{
//		mvPendTCShortsell = pPendTCShortsell;
//	}
//
//	public void setPendCancelID(String pPendCancelID)
//	{
//		mvPendCancelID = pPendCancelID;
//	}
	/**
	 * Set method for broker id
	 * @param pBrokerID the broker id
	 */
	public void setBrokerID(String pBrokerID)
	{
		mvBrokerID = pBrokerID;
	}

//	public void setSupervisorID(String pSupervisorID)
//	{
//		mvSupervisorID = pSupervisorID;
//	}
	/**
	 * Set method for approval reason
	 * @param pApprovalReason the approval reason
	 */
	public void setApprovalReason(String pApprovalReason)
	{
		mvApprovalReason = pApprovalReason;
	}

//	public void setApprovalTime(Timestamp pApprovalTime)
//	{
//		mvApprovalTime = pApprovalTime;
//	}
//
//	public void setTradeConsideration(double pTradeConsideration)
//	{
//		mvTradeConsideration = pTradeConsideration;
//	}
//
//	public void setHoldAmt(double pHoldAmt)
//	{
//		mvHoldAmt = pHoldAmt;
//	}
//
//	public void setHoldQty(long pHoldQty)
//	{
//		mvHoldQty = pHoldQty;
//	}
//
//	public void setHoldRequestAmt(double pHoldRequestAmt)
//	{
//		mvHoldRequestAmt = pHoldRequestAmt;
//	}
//
//	public void setHoldRequestQty(long pHoldRequestQty)
//	{
//		mvHoldRequestQty = pHoldRequestQty;
//	}
//
//	public void setHoldConfirmAmt(double pHoldConfirmAmt)
//	{
//		mvHoldConfirmAmt = pHoldConfirmAmt;
//	}
//
//	public void setHoldConfirmSSAmt(double pHoldConfirmSSAmt)
//	{
//		mvHoldConfirmSSAmt = pHoldConfirmSSAmt;
//	}
//
//	public void setHoldConfirmQty(long pHoldConfirmQty)
//	{
//		mvHoldConfirmQty = pHoldConfirmQty;
//	}
//
//	public void setHoldConfirmSSQty(long pHoldConfirmSSQty)
//	{
//		mvHoldConfirmSSQty = pHoldConfirmSSQty;
//	}
	/**
	 * Set method for is price warning result submit
	 * @param pIsPriceWarningResubmit the is price warning result submit
	 */
	public void setIsPriceWarningResubmit(String pIsPriceWarningResubmit)
	{
		mvIsPriceWarningResubmit = pIsPriceWarningResubmit;
	}
	/**
	 * Set method for input time
	 * @param pInputTime the input time
	 */
	public void setInputTime(String pInputTime)
	{
		mvInputTime = pInputTime;
	}
	/**
	 * Set method for modify time
	 * @param pModifiedTime the modify time
	 */
	public void setModifiedTime(String pModifiedTime)
	{
		mvModifiedTime = pModifiedTime;
	}

	// BEGIN - TASK#: CL00023 - Charlie Liu 20081008
	/**
	 * Set method for modified date
	 * @param pModifiedDate the modified date
	 */
	public void setModifiedDate(String pModifiedDate)
	{
		mvModifiedDate = pModifiedDate;
	}
	//END - TASK# CL00023
	/**
	 * Set method for DN sequence
	 * @param pDNSeq the DN sequence
	 */
	public void setDNSeq(String pDNSeq)
	{
		mvDNSeq = pDNSeq;
	}
	/**
	 * Set method for activation date
	 * @param pActivationDate the activation date
	 */
    public void setActivationDate(String pActivationDate)
    {
        mvActivationDate = pActivationDate;
    }
    /**
     * Set method for good till date
     * @param pGoodTillDate the good till date
     */
    public void setGoodTillDate(String pGoodTillDate)
    {
        mvGoodTillDate = pGoodTillDate;
    }
    /**
     * Set method for AVG price
     * @param pAvgPrice the AVG price
     */
    public void setAvgPrice(String pAvgPrice)
    {
        mvAvgPrice = pAvgPrice;
        mvAvgPrice = TextFormatter.convertPrice(pAvgPrice);
    }
    /**
     * Set method for script flag
     * @param pSCRIP the script flag
     */
    public void setSCRIP(String pSCRIP)
    {
        mvSCRIP = pSCRIP;
    }
    /**
     * Set method for net amount
     * @param pNetAmt the net amount
     */
    public void setNetAmt(String pNetAmt)
    {
        mvNetAmt = pNetAmt;
    }
    /**
     * Set method for gross amount
     * @param pGrossAmt gross amount
     */
    public void setGrossAmt(String pGrossAmt)
    {
        mvGrossAmt = pGrossAmt;
    }
    /**
     * Set method for contact phone
     * @param pContactPhone the contact phone
     */
    public void setContactPhone(String pContactPhone)
    {
        mvContactPhone = pContactPhone;
    }
    /**
     * Set method for remark
     * @param pRemark the remark
     */
    public void setRemark(String pRemark)
    {
        mvRemark = pRemark;
    }
    /**
     * Set method for modify order id
     * @param pModifyOrderID the modify order id
     */
    public void setModifyOrderID(String pModifyOrderID)
    {
    	mvModifyOrderID = pModifyOrderID;
    }

       //BEGIN Task #: WL00619 Walter Lau 27 July 2007
    /**
     * Set method for market id
     * @param pMarketID the market id
     */
	public void setMarketID(String pMarketID)
	{
		mvMarketID = pMarketID;
	}
        //End Task #: WL00619 Walter Lau 27 July 2007
	/**
	 * Set method for currency id
	 * @param pCurrencyID the currency id
	 */
    public void setCurrencyID(String pCurrencyID) {
    	mvCurrencyID = pCurrencyID;
    }
    /**
     * Get method for currency id
     * @return currency id
     */
    public String getCurrencyID() {
    	return mvCurrencyID;
    }



//	public void setUUID(BigDecimal pUUID)
//	{
//		mvUUID = pUUID;
//	}
    /**
     * Get method for hashtable
     * @return hashtable
     */
	public Hashtable getHashtable()
	{
		return mvHMap;
	}
	/**
	 * Set method for hashtable
	 * @param pHMap the  hashtable
	 */
	public void setHashtable(Hashtable pHMap)
	{
		mvHMap = pHMap;
	}

	// BEGIN - TASK#: CL00030 - Charlie Liu 20081023
	/**
	 * Get method for instrument short name
	 * @return instrument short name
	 */
    public String getInstrumentShortName()
	{
		return mvInstrumentShortName;
	}
    /**
     * Set method for instrument short name
     * @param pInstrumentShortName the instrument short name
     */
	public void setInstrumentShortName(String pInstrumentShortName)
	{
		mvInstrumentShortName = pInstrumentShortName;
	}
	/**
	 * Get method for modified date time
	 * @return modified date time
	 */
    public String getModifiedDateTime()
	{
		return mvModifiedDateTime;
	}
    /**
     * Set method for modified date time
     * @param pModifiedDateTime the modified date time
     */
	public void setModifiedDateTime(String pModifiedDateTime)
	{
		mvModifiedDateTime = pModifiedDateTime;
	}
	// END - TASK#  CL00030

}
