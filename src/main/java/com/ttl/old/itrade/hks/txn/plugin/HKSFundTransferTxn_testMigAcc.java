//BEGIN TASK #: - TTL-CN-XYL-00051 20100120 [iTrade R5] Fund Transfer for ORS
package com.ttl.old.itrade.hks.txn.plugin;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.ttl.old.itrade.hks.bean.HKSComboBoxBean;
import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.ErrorCode;
import com.ttl.old.itrade.util.Log;
import com.systekit.winvest.hks.config.mapping.TagName;

public class HKSFundTransferTxn_testMigAcc extends BaseTxn {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String mvClientID;
	private String mvFundTransferFrom;
	private String mvTargetAccount;
	private String mvAccount;
	private String mvAmount;
	private String mvIsPasswordConfirm;
	private String mvIsSecurityCodeConfirm;
	private String mvPassword;
	private String mvSecurityCode;

	private String mvDestBankID;
	private String mvBankId;
	private String mvDestClientID;
	private String mvTransferType;
	private String mvRemark;
	private String mvPersonCharged;
	
	private String mvResult;
	/**
	 * @return the mvResult
	 */
	public String getMvResult() {
		return mvResult;
	}

	/**
	 * @param mvResult the mvResult to set
	 */
	public void setMvResult(String mvResult) {
		this.mvResult = mvResult;
	}

	/**
	 * @return the mvReturnCode
	 */
	public String getMvReturnCode() {
		return mvReturnCode;
	}

	/**
	 * @param mvReturnCode the mvReturnCode to set
	 */
	public void setMvReturnCode(String mvReturnCode) {
		this.mvReturnCode = mvReturnCode;
	}

	/**
	 * @return the mvReturnMsg
	 */
	public String getMvReturnMsg() {
		return mvReturnMsg;
	}

	/**
	 * @param mvReturnMsg the mvReturnMsg to set
	 */
	public void setMvReturnMsg(String mvReturnMsg) {
		this.mvReturnMsg = mvReturnMsg;
	}
	private String mvReturnCode;
	private String mvReturnMsg;
	
	/**
	 * @return the mvDestBankID
	 */
	public String getMvDestBankID() {
		return mvDestBankID;
	}

	/**
	 * @param mvDestBankID the mvDestBankID to set
	 */
	public void setMvDestBankID(String mvDestBankID) {
		this.mvDestBankID = mvDestBankID;
	}

	
	/**
	 * constructor for HKSFundTransferTxn
	 * @param pClientID, pFundTransferFrom, pTargetAccount, pAccount, pAmount, pIsPasswordConfirm, pIsSecurityCodeConfirm, pPassword, pSecurityCode
	 */
	public HKSFundTransferTxn_testMigAcc(String pClientID, String pBankID,
			String pFundTransferFrom, String pDestBankId, String pDestClientId,
			String pAmount, String pIsPasswordConfirm,
			String pIsSecurityCodeConfirm, String pPassword,
			String pSecurityCode, String mvTransferType, String mvRemark, String mvPersonCharged) {
		super();
		this.setMvClientID(pClientID);
		this.setMvBankId(pBankID);
		this.setMvFundTransferFrom(pFundTransferFrom);
		this.setMvDestBankID(pDestBankId);
		this.setMvDestClientID(pDestClientId);
		this.setMvAmount(pAmount);
		this.setMvIsPasswordConfirm(pIsPasswordConfirm);
		this.setMvIsSecurityCodeConfirm(pIsSecurityCodeConfirm);
		this.setMvPassword(pPassword);
		this.setMvSecurityCode(pSecurityCode);
		this.setMvTransferType(mvTransferType);
		this.setMvRemark(mvRemark);
		this.mvPersonCharged = mvPersonCharged;
	}
	
	/**
	 * @return the mvBankId
	 */
	public String getMvBankId() {
		return mvBankId;
	}

	/**
	 * @param mvBankId the mvBankId to set
	 */
	public void setMvBankId(String mvBankId) {
		this.mvBankId = mvBankId;
	}

	/**
	 * @return the mvDestClientID
	 */
	public String getMvDestClientID() {
		return mvDestClientID;
	}

	/**
	 * @param mvDestClientID the mvDestClientID to set
	 */
	public void setMvDestClientID(String mvDestClientID) {
		this.mvDestClientID = mvDestClientID;
	}

	/**
	 * To get the transfer target account list.
	 * @return mvTransferTargetAccountList : A List for transfer target accounts.
	 */
	public List getTransferTargetAccountList(){
		
		List<HKSComboBoxBean> mvTransferTargetAccountList = null;
		
		//get bank acc list for the associated client
		HKSQueryBankInformationTxn mvQueryBankInfoTxn = new HKSQueryBankInformationTxn(mvClientID, "1");
		List lvBankInfoList = mvQueryBankInfoTxn.process();
		
		if(lvBankInfoList != null && lvBankInfoList.size() > 0){
			
			mvTransferTargetAccountList =new ArrayList<HKSComboBoxBean>();
			HKSComboBoxBean lvTargetAccountORSBean = new HKSComboBoxBean();
			lvTargetAccountORSBean.setMvID("ORS");
			lvTargetAccountORSBean.setMvOptionDisplay("ORS");
			lvTargetAccountORSBean.setMvOptionValue("ORS");
			mvTransferTargetAccountList.add(lvTargetAccountORSBean);
			
			Map lvBankInfoMap = null;
			String bankID = "";
			for(int i=0; i<lvBankInfoList.size(); i++){
				lvTargetAccountORSBean = new HKSComboBoxBean();
				
				lvBankInfoMap = (Hashtable)lvBankInfoList.get(i);
				bankID = lvBankInfoMap.get(TagName.BANKID).toString();
				lvTargetAccountORSBean.setMvID(bankID);
				lvTargetAccountORSBean.setMvOptionDisplay(bankID);
				lvTargetAccountORSBean.setMvOptionValue(bankID);
				mvTransferTargetAccountList.add(lvTargetAccountORSBean);
			}
		}else
		{
			setErrorCode(mvQueryBankInfoTxn.getErrorCode());
		}
		return mvTransferTargetAccountList;
	}
	
	public String getAvailableAmount() {
		String result = "";
		Hashtable lvTxnMap = new Hashtable();
	    lvTxnMap.put("CLIENTID", mvClientID);
	    /*lvTxnMap.put("BANKID", bankId);*/
	    
	    if (TPErrorHandling.TP_NORMAL == process("QueryAvailableAmount", lvTxnMap)) {
	    	result = mvReturnNode.getChildNode("AVAILABLE_BALANCE").getValue();
	    }
		return result;
	}
	
	public Hashtable getReceiverACCInfomation() {
		Hashtable listReceiverInfo = new Hashtable();
		
		Hashtable lvTxnMap = new Hashtable();
	    lvTxnMap.put("CLIENTID", mvClientID);
	    
	    String balance = "0";
	    String[] receiverInfo = null; 
	    if (TPErrorHandling.TP_NORMAL == process("QueryAvailableAmount", lvTxnMap)) {
	    	balance = mvReturnNode.getChildNode("AVAILABLE_BALANCE").getValue();
	    	String total = mvReturnNode.getChildNode("COUNTER").getValue();
	    	
	    	if(total != null && !total.equals("")){
	    		receiverInfo = new String[Integer.parseInt(total)];
	    		for(int i=0; i < Integer.parseInt(total); i++ ){
	    			receiverInfo[i] = mvReturnNode.getChildNode("NODE" + i).getValue();
	    		}
	    	}	    	
	    }
	    
	    // add to return value
	    listReceiverInfo.put("AVAILABLE_BALANCE", balance);
	    listReceiverInfo.put("RECEIVERS", receiverInfo);
	    
	    return listReceiverInfo;
	}
	
	/**
	 * Calling TP with the parameter Hashtable to process fund transfer.
	 */
	@SuppressWarnings("unchecked")
	public void doFundTransfer(){
		Log.println("[ HKSFundTransferTxn.process() starts [" + this.getMvClientID() + "]", Log.ACCESS_LOG);
		
		/**
	      * String test insert and update account maintenance
	      *
	      */
	     
		 String lvInsert = "<LINKFUNCS msgId=\"LINKFUNCS\" issueTime=\"\" issueLoc=\" \" issueMetd=\"\" oprId=\"COMPANY\" pwd=\"\" resvr=\"\" language=\"en\" country=\"US\" entityId=\"VDSC\"><FuncName>CORCL001</FuncName><OverrideApproval>N</OverrideApproval><LoopMessage><MessageContent><UPD msgId=\"UPD\" issueTime=\"\" issueLoc=\" \" issueMetd=\"\" oprId=\"COMPANY\" pwd=\"\" resvr=\"\" language=\"en\" country=\"US\" entityId=\"VDSC\"><FuncName>CORCL001</FuncName><ObjectName>SCCLIENT</ObjectName><Operation>I</Operation><Version /><LoopMasterKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"STATE\" Value=\"\" /></LoopMasterKey><LoopModMasterData><Field Name=\"PLACEOFISSUE\" Value=\"VN\" /><Field Name=\"COUNTRYOFRESIDENCE\" Value=\"VN\" /><Field Name=\"CLIENTINTRODUCER\" Value=\"1111111111111\" /><Field Name=\"CLIENTTYPE\" Value=\"I\" /><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"ACOPENDATE\" Value=\"\" /><Field Name=\"ISSTAFF\" Value=\"Y\" /><Field Name=\"OCCUPATIONID\" Value=\"4\" /><Field Name=\"BASECCY\" Value=\"VND\" /><Field Name=\"CNAME\" Value=\"TTL En\" /><Field Name=\"BUSINESSNATUREID\" Value=\"\" /><Field Name=\"CITYOFISSUE\" Value=\"HO\" /><Field Name=\"LASTNAME\" Value=\"TTL Test\" /><Field Name=\"BRANCHID\" Value=\"0\" /><Field Name=\"REGISTRATIONCAPITAL\" Value=\"\" /><Field Name=\"REMARKS\" Value=\"Remark2\" /><Field Name=\"NATIONID\" Value=\"VN\" /><Field Name=\"LASTACCOUNTSEQ\" Value=\"1\" /><Field Name=\"EXPIRY\" Value=\"2012-10-27\" /><Field Name=\"REGISTRATIONTYPE\" Value=\"1\" /><Field Name=\"FIRSTNAME\" Value=\"TTL Test\" /><Field Name=\"CLIENTREMARKS\" Value=\"remark1\" /><Field Name=\"IDNUMBER\" Value=\"123456789\" /><Field Name=\"SEX\" Value=\"M\" /><Field Name=\"BIRTHDAY\" Value=\"1995-05-08\" /><Field Name=\"SUBACCGROUP\" Value=\"1\" /><Field Name=\"ISSUEDATE\" Value=\"2010-05-03\" /><Field Name=\"DISCRETIONARYFLAG\" Value=\"Y\" /><Field Name=\"ISDIRECTMARKETINGREFUSED\" Value=\"Y\" /><Field Name=\"ISHOUSE\" Value=\"N\" /><Field Name=\"EMPLOYERNAME\" Value=\"TTL\" /><Field Name=\"ACCOUNTTYPE\" Value=\"N\" /><Field Name=\"IDTYPE\" Value=\"1\" /><Field Name=\"OBJECTIVEID\" Value=\"1\" /></LoopModMasterData><LoopDetail><DetailNode><ObjectName>SCCLIENTADDRESS</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"ADDRESSTYPEID\" Value=\"8\" /></LoopDetailKey><LoopModDetailData><Field Name=\"ADDRESS1\" Value=\"testAddress1\" /><Field Name=\"ADDRESS2\" Value=\"111\" /><Field Name=\"ADDRESS3\" Value=\"111\" /><Field Name=\"ADDRESS4\" Value=\"HCM\" /><Field Name=\"ADDRESS5\" Value=\"084\" /><Field Name=\"COUNTRY\" Value=\"VN\" /><Field Name=\"COUNTRYTYPE\" Value=\"L\" /></LoopModDetailData></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCCLIENTSIGNATURE</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"FILENAME\" Value=\"logo.gif\" /></LoopDetailKey><LoopModDetailData><Field Name=\"DESCRIPTION\" Value=\"asdfsadasd\" /><Field Name=\"ISAGENT\" Value=\"Y\" /><Field Name=\"FILEVALUE\" Value=\"R0lGODlh1gBHAPcAAAAAAP////7+/uoBAuoHCusJDuoMEewSF+0ZHOsdIeweIu0mKvIqLfmjpPrLzOsABusBCuwCCewDDOoDC+0FC+0GD+sGDu0JEeoJEesKFOwMEuwMFesPFe0RGeoRGe0THesTG/AUHu4UGusUHO0WHu0XH+8YIO4YIusZIusZH/EbJOwaI+4bIe4bIu4bJO0bJOwbIeobIu4cJe0cIe0cIuscIuscJO4dI+wdJe0eJuseI+seJe8fJ+8fJe0fJOwfJ+4hKuwhJu0jLOsiKvAkLO8nMO4nLesoL+4rM+8uNu8zOe82Pe86QfA+RO9DSPFFTPBJUPFLUvJRVvNVW+9UWPBYXvFhZfNlavR1efV+gfSChvWFivWJjfaNkPWVmPianfaZnPeeoviipvenqfisrvq5vPrCxP3X2Pzb3P3h4v3m5+0FEe0KFvALF+0OGewTHu4YJO4aJukaJO8cJ+0bJusbKOwdJ+sdJ+0eKeweKPJdZPRsc/iRl/WQlfqmq/iorPmus/mwtfq+wfvGyfvO0fzT1fze4PeUm/zv8P3i5f719v309f3m6f/2+f/8/f/9/v/+//z//+3u7vv//v3//P7++////fv7+v769/78+/7w7fvm4/rJxfri4P3p5/708/3k4/vm5eYCAPrZ2f7s7P7x8f75+f39/fj4+Pf39/T09PHx8e7u7u3t7erq6ujo6Obm5uXl5ePj4+Dg4N/f393d3dnZ2dbW1tTU1NHR0c7OzszMzMvLy8jIyMbGxsHBwb29vbu7u7i4uLGxsa2traqqqqioqKSkpJ+fn5ubm5mZmZWVlZKSko6OjouLi4iIiIaGhoODg4CAgH19fXp6end3d3R0dHJycnFxcW5ubmpqamZmZmVlZWJiYl5eXlpaWlhYWFVVVVFRUU5OTkpKSkdHR0RERD8/Pzo6OjY2NjMzMzAwMC8vLywsLCsrKykpKSYmJiIiIh4eHhoaGhYWFhISEhEREQ4ODgwMDAoKCgkJCQcHBwUFBQMDAwEBAf///yH5BAEAAP8ALAAAAADWAEcAAAj/AAMIHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePIEOKHEmypMmTKFOqXMmypcuXMGPKnEmzps2bOGdCCgCpkgABOYMKnQhUoCVIloxC2unzZyVHmUz55GmUKlWkRYdqvbmTINJIPQUKKKUG1KY0htKUkgp0pyUBSQMcXVppq92YO7PyjBTgEaRIjgwNasBFy5YtXPr8cZBGUaW3OyMndXu3sksBkE4hnZTUkqVSZbJMUZJkCZMmTZyohqJnCxk1PNtCohRAr+XbKR/xpeo5zRUhMe7w+CFEiJEjR4oYMVJkAZAqYzQFqGvJEU9Lj3BrL9m1K1DdhIpk/5hzww4NHTJk/OCho0eeHD9svCByxQFVAXzjbt8fUvZ0sY5gIYEJONyQ3gsuvCCfDDe4kOAcNcjABCB1FXUUfxiCRIlng5CC1RlIENADDjXIV0MNL8jwAg044NDCCz+4cEcRYdS1FFgqqYLKjjumgkpEqPjIoyqXCBAkj0gmqeSRp/xU20FAXbKkKlSqIuSORdpGUFECnOKll5d8KWaTtjlZ0I2WmKHEFR561oABGwihwAtz0KHADTa4QIMLCrgwBw42GHiEGJOplBQ12VijqDXYUGOLQ6cAk801i2KDTS6uPJPoopx22uk12WhDzTPEyHLKQFnp0gyolC5ajTbddP/jjTaLJvqMMK8cdMorvyDzjDTUTFONp4pWQw010SjDiypQCvRYAGdAUcIEUhTCFyZaFMBBDjgooMAKLtxQAx10RGjDDDekeEMSZfiUnUrQ8APAvPP2Qw0qWhrkSjn0AuAPAPHYcos8/f7b78Hz+mPwPvjgU885z+Ra1CnFuHPPPgcbjPC8+9xDDznDFNRKM+fMU889+aS8z8oss5xyw/bEo80tCFEyiQCK7FEDHiY8sIAYlwTgCBccTPCBDTjgcUcLNeygQA8yxCFDDVK/CIUh09WVkiviIOzOowsJMIw+CifsjzOX6OJOxhuznTE+2bQykC/xsK2xv23P+w4vA7X/kg3G9N59d+D98tON3HoBVZcYPtx5BxAHYGAFITsF0sQGE2jQQQklmPDG5jrQ8AIdNsiQAHlYZGKoQMcAjvc+zuRL0CrhDA6PLgHswg7hedPbD94I33OMQKtkk/fgvPs+DbOoNKMPwshvrHE9xmi5VBpPiA6DCFCU0cACCHBhCCSliDEFEim4wYYHJtjBwQE53ICnHXjMmYQgcq1UyzkJ03sOLQsBBj0yNo1U5G530etd8v41DlkEoBbr6J/Z/qUwf/SjgoP7FzoelQt1ZCyB0qMXN35kEEuEQQFziMMcUgAELSDCEFIQBQOyMIjqjKIMY2hAGMYgiC0gAQQqwIEK/1pgggNs4ANYWERLmIExjeEjGQkRQCq0Mbh5/CIAp9DF7vCmsH7wox9gDKMYwWi3eQQjALkY4MHo8Y1nPMMZ0LCGNrghq3OosV/uwB0xXKcxC47xgr+TIL3KkaszqWEKN0iAC3QwhwyIIgl/IEUDllAAGERBC2RwABo4sQUpgKERWrAACN7AAQt4AAlOQAISCJGSrNgigsADADlikZBftKNg/tiGKoCiC1jirR7Z+JU0hknMYUZjGt5ARz5iCYB9hOwX+LjbOojhCitRaRWtaIUrXhGLZNzjYPXQhQCUsbF8pEMc3/DGN9b5DXGUIx30kNfB1JGLgwwCCTIQHQpIMP+FQyhhAErIAhiy4AQ3FCAEMaBPF5iAACko4QEVOEAS9hAINSjCAU34wrtUcopoyDNh+CCGQYCCims8r1/zEMZAtHiwPKaiSjCFKStk4Qx8EM4fxxDbSek1DS3ppRbggIc7hgqPdNwiFdXoIwDs0QxauCKbUNUmLGQxjHfwzh29MMgp+ICHFO2ABB3oQiZI0YcoKGEJUogCHkhAAjdkYApmIIUD+KCFLoRBEKF4xCcEAQY1cOEKimjWRyABFKDgAh5lM9g3XFFYguTiloHzhzdWsVJ23M0du3CILM5BQXo14xTCWGb/zPELWLQiFWESi1gEQItc3AIXuuCFLlDRim3/BLJe35DbQsIBvH/J44oF+YQVREeDG6zgBEKYQhjMIIgGgCEMUegADXYwhDgcIAZVaIAZzpCITaDBDGCQAggecAUrREENXVEJUm/7L3tcUS+nWMYF+3WPkImll4TDKkOAwopr3NZf0rjELr7Z23eAoxvWONYzmgGNBStDGcUIBqay8opvdBYA/VDGqRayDHKM48PjKMc2aKbaAKjhCXeKAw7ysIMKDGAAcohCFrxghSCkwEEw6AEK3GCBDPwgCahJwhwMIAE3nCAHQGjCIGRXkl/MQ5DaYBZBaIEOhI3DFaplaf/0q5CioIIZ/+1HgG/xZLcFjh/6wMc96mEPe9AD/x7mwMYvgDKLcpRtXvgIBpNrIwBW1IIWswh0LWJBQoIUYglM+4ECPlCBFlDBC2YohBrKcAALDEEGeOhqDnyQgxWMYAMXqEAGOJCCOaSHBD9gAhhMgRIuBWAVVLzbPLI6kFMc46P/4kf1CIJfPNKaIahQxn/9IQ1UvIIcC8xgwXwHj5DNwhx3q4cvwvakkaJKIINQQg18cAc2HEAPZgAFKBzQgD6IpgkkaEEHDICBCligAJo7wQ+C8IM8nGcGOEAyE1x4kuy4WoAGo6A18CUQV5iDd+igJa+3SC+s7rnWyLjzvKSRCls/T6kKXDY8bBELzgauHu9tSFZGjio13SAGF/9QAh8E0YcqLMAACtjDIDxBhQFgwAVLiMIUqACFJeSBxxwwQQ7yOQcTxMAITNgCIh6+kd0QhBXdQNjtBEKxj86LH8lwtQC03PBfL+QSyQgkBSkegFZYox6CXHb/Ak6vfTAjFuUQ+1KBS/KCuPraTwKKmuLAhieAAQsLoMAALoCFQTggvAh4ghbKkAhTYCITivCEGQ5xhSRwYA1vmMMJeDAEITjBC0o0iX4IQgwC00sf0TgVK5B9sHXYwjZaNph+mY6KZQyugAJhhTCoUQ51wGMe86CHPfRBfH4Y34tWrxc3aAGOg90j5AppxS54sQtd7MIXtyhSQQixBAwwoQtNWEP/5JQwhjLsgQMEgEIgPhEJyGsCDYQoBCMUoRs0fAEKB3jABKDwBBRAgQys5hJcg3C1EADCgHbA0w/PEDQFwXXzwmUMsQrTMDhkVxSX8AqzkAvC8Au+AAzD8IHJ0AwMZg3eUDexBA6yoA0Hsw/F0FgIIQDFkA7ssA40+A7hADYEkQZKAAR8EAUQsAMewAR+kAUpAFDr5wmD4AV7MAVNoATIgVZY8AdokAmgoQd6UAZRAAJUYAaqYxJOtyXHYFP9sg/GcAoqGEuYdRAOCAAQ2GXHdjD9wAynYAvFQAzEIAx4iGUGISVBgk3IUGb0Mg6vQA2x1A/TQEI+JRDSIIYGAw9e/1cbiOAEWRAGRwQCQNAFeiABAyAFhkAIWKBtoIYBF9AGGbABBYABGXBK0MEIjZAJgpACKKAHhuAIZjIS6VUQsXBwB9MNwZAOCIMNBNeAW0RB7IALDJEKsYAMakRB/WAMAQAN+UB80jg8JVaNAXALsGQw49AKzyBI7BAMhaYlskAOw9YOvXB3psAFhbAHD5ACHaAHVrABD/AEaKAFIGABG9ABI8ACMsAt3MIDdAAHH1BKbbAEWgAGTeAGQLAHiAATXZIMF0cv9RAPOzUv9EB3wngw8zANybAMHvmRILkM0yAOCNg/+BAy0cBeAOANsOAlXUImqrUKzCCGeDMOq3AMGf/UDtOwDMmADD6ZDD2JDMoADhU5L/RUbQMBCqTgBKKEBFhgBGvwAmXQBwOwAUHELXRiAzVQXC+gAujyAjhAAhpgARjgBi8wBGIlE1QGPQjjDVJmEGuIYV/0R2GUdgnzWwLADPxwN/gwDtPwl9EQDc3ADMpADMWwDNtwR1dnk8dgeoKDD212D/cQmf/VL+UAC3X3CGrQBBbwBlKABSBAAVXgAElwAXhQIDfQAniSIiryAjUgP2C5AzKwAjBgAzRQBGUgEOlViyfRJWCmdv1iD2eEEHGZcYLTL+QACwFADPLyL2LHMC8jmW4mfBfGMdmgCsAAWXbJlmrnD91gQNWGGZ//8QQQ8AZYkAUZ8ABZ4AA/wAFdpSA44CAyYAN0sAOJRJ93IAMqcAc9sAMtYAdVQAptgR8kcYslJgC2wHDJAwDhwAoJEZcgpED6gAynggv00FnVyUwLCgC6JgCqQIhsqWxtgw/I4IIC8QiWgAhScAEj8AVckAEQkAWb0AQVcAeBsgIMQgMo4popApYusAI4IAMx8AIKMARkQBuXUW2pIA1sdzf7QAxMVpwZtzH8EDfEYw2VaZwIcw4OhEbpcGHHyZ3+0g+HcxBHIQBbIAIiQAZhcAERcAWQ0AUDkALGNZ+y6QLzmSI4iiBTUwNz4AI1oAekMHqYURsGuhFakxC9YFUU/9RF81IOCqd1AsEL8OAvGHSpGGQ2msoP7QANylkUsbANjKiplopLpXoP4cALXNIL3/BNFlQwgHRnFYRh8CANXTpSSdEJTwACgFAGIAABUEAWTRABp5kAKmADpWMDLaAiCYAgtvkCLEAHOHA/K3GoetE88NAONEiD7OAO71AMG2YmWXEL4/AO23qu6Hqu6HAO5EAO4XANzUAMuVBofQMMzIAN39Cu55AO6KAOM7gO/IoO5dCu3mANyhAMCocqryAMzKAN44AO7eAO7RAP8lCx8vAO7ZCx6TAO1nAMs8UQllAGRBAGhWAED1AEhPAIZMADGMADKUQnMHADdKAiDAIDc/9wBwpiAzwABpDwCIlqEkhqbbnnWrdQtEWLC7iQColYG6hQC7hgtFAbtVBrC382C7HACuCZEJfACrAwC7RQC7YgMEYbtrYAaLPwCqxAr7yZCq4gC06LtNfXC77QC7mAtLhgC7PACndXQt7RBX3wCVHwABzwBY7wBUCAAAaQA3OgA3dSIjSgn/NJA3diAyqABZ9QWIeqEiYaReHZuXz2EJKad1uiWk5SJng3uhGRL3trpgNhCY3gAAKgBRggAVQAGiIQBByQAUEwB135AjeAAzqgSHigrKa2B4yQFLQxeiSxtFAiO43FdNR2uqobvUhJcrUIvRMRFtMBCQ6ABBJAAu3/QgZOcAUdUAB4UCcvoANxQANzUAelQwdBgAUeEgnZkbmam7rLqxGri7ovGLpIeRD12xl5sQWYUwXsRwqkEAY1IAEp4AO+G6Q14AI4wAJLAAaBpRTYuxHWerrWeHcZHL2ma3f4K8JCS8IfrJt8ARcBMAkCoQlckARs4AcBQAkCMAmCwAQRAAJTk5p2MAdHgAUOIMBi4Rl3sb8eccIlXHcidxFxUSaFgAVVMAqdEQBoYAUX8AFzYgRNsAeCEHrXsRT2KxJNzMEvSMJmfBHWW8LVmMZGfKBlfG1IbKgBgAlpUQnvQj5f4ARSkAV/MAoXvBWSmjj/CxEh7Ly8qcbWOMiJ/yy6/EvGiyxYtrEUc1wGo5AJcUHEYTwTgTy6cTzIbcxne8a8j5yIIdzIjgzJA7ETkYCkkGEQmdxqe+uCz8vJ1WvKqJI4pWvCzHvInrzG4krLsrxaiKzInlttz/IIQOEZr7wSv/zISczBzivC08u5zsy/aazIm9vGokzGkbEU2YEjcvEWNdHMxPzMHfzGcCxY6Mx0ShzM0oy602zLgwzGlEC/U4zJS6G8LdHOZ3zO0tvBptuN4aB1SkwQ6hAPTBbL6VzM2fy5JnrImxtF4vwIdywQkhAP5lAZ22zLgowQGe3Bp1wL9pBZb5zB7Ey68sx0yFsVmCwQuwAABWgX2wDT8v+8yB1tEDNdCyD9yMUAAMpAzQ+3DfZQgCfNyKM8EKxgD+EwEd1YDBvx0s9QEXoRDB7EoA4aDABgDku9X2LhRkscDPag1QuR1dcWD+FgD4gDzQRxcHhnD05dzQXRjTotEGRdzsUQD7qFELVwMGeU1NsQDxDRjT8dADNNTmdkDuYw1AEQDFsNugOx19uQO/ZQDacsWHsd1VxdzgUBAOqg1gWtFwfnoMSMvXJdFJwN16X8QACA2fEA2BTBCmSd1BmNEBhNyGbSjQ5qJtVglLuA1VX90+TE2TQtALrYjVG92zHdCsUdALzF2SRN1yYY1f2yDQLQjfMSDL49L4NddsVN3PT/og7KDdjkdHDd6EEjTdhL1doDIdfhEA97TS+RXQtVTdnW7dPZ7dOPvdoCEQ+dbd328NPCDQCRvQv2INzbzVusQE7FYNhgDTC7YN2zvcRFMdMlRk7PwAqtjdVLHdawvdQeRNSXHQDmANhhPRDIbdwAUAwY7trQzQq7zQsAwwq8hdRKreEBUOICceIAwAsH9+AAEAwjHgDkpAz2YNxQ3dPPINIsbtzuHQAxPuMj3go9vd1hbeM4rtrfvQsvTdkI/uQAEAAH7eL4LRBInthCnuJKDeYZXduOXRS4XW05jd5Y/dMYHeJyXRshbuEpPhBBLhAUjt4x7eSRPedkndPKMNNZ/z3nAcDmIs7i3n3myhDkwe1GMH3Zht3o671U9uCghQ7TAg4UAn7o82IOis7ol/3S6pDgP37mWB3ZFF7bip57MQMAlD3kB7PoES7hAgHZAcAL9rANFr7ipW4OHQ7mwx3iSb1UWbHbu6DnKq7eAxHjL97pL03sVY7fjM7sCs7WQ57YYn5Gcn3ZSK7kmQ7Wkd3ptRDlU57Vsj3s+R3V3VgNW87cAADbrv7lYb7b243eAJBZCp7mA8HoIGsQwWCCxB4Au+3c7h7cHz4QHkTZu03ZSK2Lkd3c6vDcTl7V0p3ROX1w9nDQ7p57FC/iXz7kVC3c4F7p+j3TMbPkMG3Y6C7f88FC2YgO8tge4SFu7D6+VGck4Ogt2QZOEFg924ZN5PNy3KfdEPo8ESU+06JdEDON8Q/h8zAB1Yve2TZx1qr+1lUB14QsEJGw9A4R3PptEHUT2RNB9TCB6MJ5E7tg8AqhHzkfET0ryRly99lr961r9yy8zEusm3gf+BqxIWI/EfTb9YKf+H9fEJbwLBnhGYWv+JLvFXnhGQLgF52cEMqMEJDf+Z7/+aAP+hAR+qHP+aL/EKR/+iWk+h3h+WBfG0TMEQEBADs=\" /></LoopModDetailData></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCCLIENTCONTACT</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"INTERNALKEY\" Value=\"0\" /></LoopDetailKey><LoopModDetailData><Field Name=\"CONTACTTYPEID\" Value=\"S\" /><Field Name=\"NUMBER\" Value=\"1234567\" /><Field Name=\"TO\" Value=\"\" /><Field Name=\"CONTACTPERSON\" Value=\"\" /><Field Name=\"POSITION\" Value=\"\" /></LoopModDetailData></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCCLIENTRELATEDCLIENTGROUP</ObjectName><MasterObjectName /><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"RELATEDCLIENTGROUP\" Value=\"1\" /></LoopDetailKey><LoopModDetailData /></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCCLIENTJOINTACC</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"INTERNALKEY\" Value=\"1\" /></LoopDetailKey><LoopModDetailData><Field Name=\"TITLE\" Value=\"1\" /><Field Name=\"JOINTNAME\" Value=\"TTL Test\" /><Field Name=\"CNAME\" Value=\"TTL En\" /><Field Name=\"BIRTHDAY\" Value=\"1995-05-08\" /><Field Name=\"IDTYPE\" Value=\"1\" /><Field Name=\"IDNUMBER\" Value=\"123456789\" /><Field Name=\"ISSUEDATE\" Value=\"\" /><Field Name=\"EXPIRYDATE\" Value=\"\" /><Field Name=\"PLACEOFISSUE\" Value=\"VN\" /><Field Name=\"CITYOFISSUE\" Value=\"\" /><Field Name=\"SEX\" Value=\"M\" /><Field Name=\"OFFICETEL\" Value=\"\" /><Field Name=\"MOBILENO\" Value=\"\" /><Field Name=\"HOMETEL\" Value=\"\" /><Field Name=\"OCCUPATIONID\" Value=\"4\" /><Field Name=\"PAGERNO\" Value=\"\" /></LoopModDetailData></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCCLIENTNOTIFICATION</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"CONTACTINTERNALKEY\" Value=\"0\" /></LoopDetailKey><LoopModDetailData><Field Name=\"LANGUAGEID\" Value=\"V\" /><Field Name=\"GROUP\" Value=\"1\" /><Field Name=\"FEE\" Value=\"12\" /><Field Name=\"TYPE\" Value=\"X\" /><Field Name=\"NOTIFICATIONTYPE\" Value=\"S\" /><Field Name=\"NUMBER\" Value=\"1234567\" /></LoopModDetailData></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCCLIENTAGENT</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"AGENTNAME\" Value=\"AAAAAAAAA\" /></LoopDetailKey><LoopModDetailData><Field Name=\"TITLE\" Value=\"1\" /><Field Name=\"CNAME\" Value=\"AAA En\" /><Field Name=\"CLIENTAGENTSTATUS\" Value=\"N\" /><Field Name=\"PASSWORD\" Value=\"111111\" /><Field Name=\"STARTDATE\" Value=\"2010-09-20\" /><Field Name=\"IDTYPE\" Value=\"1\" /><Field Name=\"PERSONALID\" Value=\"123456789\" /><Field Name=\"PLACEOFISSUE\" Value=\"VN\" /><Field Name=\"CITYOFISSUE\" Value=\"HO\" /><Field Name=\"ISSUEDATE\" Value=\"2010-10-04\" /><Field Name=\"EXPIRYDATE\" Value=\"2010-10-30\" /><Field Name=\"ATTORNEY\" Value=\"F\" /><Field Name=\"TEL\" Value=\"123456789\" /><Field Name=\"MOBILE\" Value=\"1234567\" /><Field Name=\"ADDRESS2\" Value=\"34234\" /><Field Name=\"ADDRESS1\" Value=\"qwer\" /><Field Name=\"ADDRESS4\" Value=\"234234234\" /><Field Name=\"ADDRESS3\" Value=\"234234\" /><Field Name=\"COUNTRY\" Value=\"VN\" /></LoopModDetailData></Row></LoopRows><LoopDetail><DetailNode><ObjectName>SCCLIENTAGENTPRODUCT</ObjectName><MasterObjectName>SCCLIENTAGENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /><Field DetailFieldName=\"AGENTNAME\" MasterFieldName=\"AGENTNAME\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"AGENTNAME\" Value=\"AAAAAAAAAB\" /><Field Name=\"PRODUCTID\" Value=\"HKS\" /></LoopDetailKey><LoopModDetailData /></Row></LoopRows><LoopDetail /></DetailNode></LoopDetail></DetailNode><DetailNode><ObjectName>SCCLIENTSTATUS</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows /><LoopDetail /></DetailNode><DetailNode><ObjectName>SCCLIENTBENEFICIARY</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"INTERNALKEY\" Value=\"1\" /></LoopDetailKey><LoopModDetailData><Field Name=\"TITLE\" Value=\"1\" /><Field Name=\"BENEFICIARYNAME\" Value=\"sadf\" /><Field Name=\"CNAME\" Value=\"dsadfsdf\" /><Field Name=\"IDTYPE\" Value=\"1\" /><Field Name=\"PLACEOFISSUE\" Value=\"VN\" /><Field Name=\"CITYOFISSUE\" Value=\"HO\" /><Field Name=\"IDNUMBER\" Value=\"asdasd\" /><Field Name=\"ISSUEDATE\" Value=\"2010-10-05\" /><Field Name=\"EXPIRYDATE\" Value=\"2010-10-25\" /><Field Name=\"OFFICETEL\" Value=\"123123\" /><Field Name=\"MOBILENO\" Value=\"123123\" /><Field Name=\"TEL\" Value=\"1231231\" /><Field Name=\"PAGERNO\" Value=\"123123\" /><Field Name=\"ADDRESS1\" Value=\"123\" /><Field Name=\"ADDRESS3\" Value=\"12312\" /><Field Name=\"ADDRESS2\" Value=\"123\" /><Field Name=\"COUNTRY\" Value=\"VN\" /><Field Name=\"ADDRESS4\" Value=\"3123\" /></LoopModDetailData></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCACCOUNT</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"ACCOUNTSEQ\" Value=\"1\" /></LoopDetailKey><LoopModDetailData><Field Name=\"ACOPENDATE\" Value=\"\" /><Field Name=\"INTERESTID\" Value=\"C0.2D0.0\" /><Field Name=\"LASTINTERFACESEQ\" Value=\"1\" /><Field Name=\"BANKID\" Value=\"\" /><Field Name=\"BANKACID\" Value=\"\" /><Field Name=\"PICKUPCHEQUEFLAG\" Value=\"N\" /><Field Name=\"PICKUPBRANCHID\" Value=\"\" /><Field Name=\"CHEQUENAME\" Value=\"TTL Test\" /><Field Name=\"ISSUECHEQUEFLAG\" Value=\"N\" /></LoopModDetailData></Row></LoopRows><LoopDetail><DetailNode><ObjectName>SCACCOUNTTRUSTEDCASHTRFACC</ObjectName><MasterObjectName>SCACCOUNT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /><Field DetailFieldName=\"ACCOUNTSEQ\" MasterFieldName=\"ACCOUNTSEQ\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"ACCOUNTSEQ\" Value=\"1\" /><Field Name=\"TRUSTED_CLIENTID\" Value=\"C000002\" /><Field Name=\"TRUSTED_ACCOUNTSEQ\" Value=\"1\" /></LoopDetailKey><LoopModDetailData /></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCACCOUNTINTERFACE</ObjectName><MasterObjectName>SCACCOUNT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /><Field DetailFieldName=\"ACCOUNTSEQ\" MasterFieldName=\"ACCOUNTSEQ\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"ACCOUNTSEQ\" Value=\"1\" /><Field Name=\"INTERFACESEQ\" Value=\"1\" /></LoopDetailKey><LoopModDetailData><Field Name=\"CURRENCYID\" Value=\"VND\" /><Field Name=\"BANKID\" Value=\"OCB\" /><Field Name=\"BANKNAME\" Value=\"OCB1\" /><Field Name=\"BANKACID\" Value=\"111111111\" /><Field Name=\"OWNERNAME\" Value=\"TTL Test\" /><Field Name=\"IDNUMBER\" Value=\"123456789\" /><Field Name=\"PLACEISSUE\" Value=\"HCM\" /><Field Name=\"ISSUEDATE\" Value=\"2010-10-05\" /><Field Name=\"EFFECTIVEDATE\" Value=\"2010-10-06\" /><Field Name=\"EXPIRYDATE\" Value=\"2010-10-22\" /><Field Name=\"ISDEFAULT\" Value=\"N\" /><Field Name=\"AUTODEBITEXTERNALFLAG\" Value=\"N\" /><Field Name=\"WAIVEFLAG\" Value=\"N\" /><Field Name=\"TRANSFERFLAG\" Value=\"N\" /><Field Name=\"AUTOCREDITEXTERNALFLAG\" Value=\"N\" /><Field Name=\"REGISTEREDFLAG\" Value=\"\" /></LoopModDetailData></Row></LoopRows><LoopDetail /></DetailNode></LoopDetail></DetailNode></LoopDetail></UPD></MessageContent></LoopMessage></LINKFUNCS>";
		
	     //String lvInser1 = "<LINKFUNCS msgId=\"LINKFUNCS\" issueTime=\"\" issueLoc=\" \" issueMetd=\"\" oprId=\"COMPANY\" pwd=\"\" resvr=\"\" language=\"en\" country=\"US\" entityId=\"VDSC\"><FuncName>CORCL001</FuncName><OverrideApproval>N</OverrideApproval><LoopMessage><MessageContent><UPD msgId=\"UPD\" issueTime=\"\" issueLoc=\" \" issueMetd=\"\" oprId=\"COMPANY\" pwd=\"\" resvr=\"\" language=\"en\" country=\"US\" entityId=\"VDSC\"><FuncName>CORCL001</FuncName><ObjectName>SCCLIENT</ObjectName><Operation>I</Operation><Version /><LoopMasterKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"STATE\" Value=\"\" /></LoopMasterKey><LoopModMasterData><Field Name=\"PLACEOFISSUE\" Value=\"VN\" /><Field Name=\"COUNTRYOFRESIDENCE\" Value=\"VN\" /><Field Name=\"CLIENTINTRODUCER\" Value=\"1111111111111\" /><Field Name=\"CLIENTTYPE\" Value=\"I\" /><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"ACOPENDATE\" Value=\"\" /><Field Name=\"ISSTAFF\" Value=\"Y\" /><Field Name=\"OCCUPATIONID\" Value=\"4\" /><Field Name=\"BASECCY\" Value=\"VND\" /><Field Name=\"CNAME\" Value=\"TTL En\" /><Field Name=\"BUSINESSNATUREID\" Value=\"\" /><Field Name=\"CITYOFISSUE\" Value=\"HO\" /><Field Name=\"LASTNAME\" Value=\"TTL Test\" /><Field Name=\"BRANCHID\" Value=\"0\" /><Field Name=\"REGISTRATIONCAPITAL\" Value=\"\" /><Field Name=\"REMARKS\" Value=\"Remark2\" /><Field Name=\"NATIONID\" Value=\"VN\" /><Field Name=\"LASTACCOUNTSEQ\" Value=\"1\" /><Field Name=\"EXPIRY\" Value=\"2012-10-27\" /><Field Name=\"REGISTRATIONTYPE\" Value=\"1\" /><Field Name=\"FIRSTNAME\" Value=\"TTL Test\" /><Field Name=\"CLIENTREMARKS\" Value=\"remark1\" /><Field Name=\"IDNUMBER\" Value=\"123456789\" /><Field Name=\"SEX\" Value=\"M\" /><Field Name=\"BIRTHDAY\" Value=\"1995-05-08\" /><Field Name=\"SUBACCGROUP\" Value=\"1\" /><Field Name=\"ISSUEDATE\" Value=\"2010-05-03\" /><Field Name=\"DISCRETIONARYFLAG\" Value=\"Y\" /><Field Name=\"ISDIRECTMARKETINGREFUSED\" Value=\"Y\" /><Field Name=\"ISHOUSE\" Value=\"N\" /><Field Name=\"EMPLOYERNAME\" Value=\"TTL\" /><Field Name=\"ACCOUNTTYPE\" Value=\"N\" /><Field Name=\"IDTYPE\" Value=\"1\" /><Field Name=\"OBJECTIVEID\" Value=\"1\" /></LoopModMasterData><LoopDetail><DetailNode><ObjectName>SCCLIENTADDRESS</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"ADDRESSTYPEID\" Value=\"8\" /></LoopDetailKey><LoopModDetailData><Field Name=\"ADDRESS1\" Value=\"testAddress1\" /><Field Name=\"ADDRESS2\" Value=\"111\" /><Field Name=\"ADDRESS3\" Value=\"111\" /><Field Name=\"ADDRESS4\" Value=\"HCM\" /><Field Name=\"ADDRESS5\" Value=\"084\" /><Field Name=\"COUNTRY\" Value=\"VN\" /><Field Name=\"COUNTRYTYPE\" Value=\"L\" /></LoopModDetailData></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCCLIENTSIGNATURE</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"FILENAME\" Value=\"logo.gif\" /></LoopDetailKey><LoopModDetailData><Field Name=\"DESCRIPTION\" Value=\"asdfsadasd\" /><Field Name=\"ISAGENT\" Value=\"Y\" /><Field Name=\"FILEVALUE\" Value=\"R0lGODlh1gBHAPcAAAAAAP////7+/uoBAuoHCusJDuoMEewSF+0ZHOsdIeweIu0mKvIqLfmjpPrLzOsABusBCuwCCewDDOoDC+0FC+0GD+sGDu0JEeoJEesKFOwMEuwMFesPFe0RGeoRGe0THesTG/AUHu4UGusUHO0WHu0XH+8YIO4YIusZIusZH/EbJOwaI+4bIe4bIu4bJO0bJOwbIeobIu4cJe0cIe0cIuscIuscJO4dI+wdJe0eJuseI+seJe8fJ+8fJe0fJOwfJ+4hKuwhJu0jLOsiKvAkLO8nMO4nLesoL+4rM+8uNu8zOe82Pe86QfA+RO9DSPFFTPBJUPFLUvJRVvNVW+9UWPBYXvFhZfNlavR1efV+gfSChvWFivWJjfaNkPWVmPianfaZnPeeoviipvenqfisrvq5vPrCxP3X2Pzb3P3h4v3m5+0FEe0KFvALF+0OGewTHu4YJO4aJukaJO8cJ+0bJusbKOwdJ+sdJ+0eKeweKPJdZPRsc/iRl/WQlfqmq/iorPmus/mwtfq+wfvGyfvO0fzT1fze4PeUm/zv8P3i5f719v309f3m6f/2+f/8/f/9/v/+//z//+3u7vv//v3//P7++////fv7+v769/78+/7w7fvm4/rJxfri4P3p5/708/3k4/vm5eYCAPrZ2f7s7P7x8f75+f39/fj4+Pf39/T09PHx8e7u7u3t7erq6ujo6Obm5uXl5ePj4+Dg4N/f393d3dnZ2dbW1tTU1NHR0c7OzszMzMvLy8jIyMbGxsHBwb29vbu7u7i4uLGxsa2traqqqqioqKSkpJ+fn5ubm5mZmZWVlZKSko6OjouLi4iIiIaGhoODg4CAgH19fXp6end3d3R0dHJycnFxcW5ubmpqamZmZmVlZWJiYl5eXlpaWlhYWFVVVVFRUU5OTkpKSkdHR0RERD8/Pzo6OjY2NjMzMzAwMC8vLywsLCsrKykpKSYmJiIiIh4eHhoaGhYWFhISEhEREQ4ODgwMDAoKCgkJCQcHBwUFBQMDAwEBAf///yH5BAEAAP8ALAAAAADWAEcAAAj/AAMIHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePIEOKHEmypMmTKFOqXMmypcuXMGPKnEmzps2bOGdCCgCpkgABOYMKnQhUoCVIloxC2unzZyVHmUz55GmUKlWkRYdqvbmTINJIPQUKKKUG1KY0htKUkgp0pyUBSQMcXVppq92YO7PyjBTgEaRIjgwNasBFy5YtXPr8cZBGUaW3OyMndXu3sksBkE4hnZTUkqVSZbJMUZJkCZMmTZyohqJnCxk1PNtCohRAr+XbKR/xpeo5zRUhMe7w+CFEiJEjR4oYMVJkAZAqYzQFqGvJEU9Lj3BrL9m1K1DdhIpk/5hzww4NHTJk/OCho0eeHD9svCByxQFVAXzjbt8fUvZ0sY5gIYEJONyQ3gsuvCCfDDe4kOAcNcjABCB1FXUUfxiCRIlng5CC1RlIENADDjXIV0MNL8jwAg044NDCCz+4cEcRYdS1FFgqqYLKjjumgkpEqPjIoyqXCBAkj0gmqeSRp/xU20FAXbKkKlSqIuSORdpGUFECnOKll5d8KWaTtjlZ0I2WmKHEFR561oABGwihwAtz0KHADTa4QIMLCrgwBw42GHiEGJOplBQ12VijqDXYUGOLQ6cAk801i2KDTS6uPJPoopx22uk12WhDzTPEyHLKQFnp0gyolC5ajTbddP/jjTaLJvqMMK8cdMorvyDzjDTUTFONp4pWQw010SjDiypQCvRYAGdAUcIEUhTCFyZaFMBBDjgooMAKLtxQAx10RGjDDDekeEMSZfiUnUrQ8APAvPP2Qw0qWhrkSjn0AuAPAPHYcos8/f7b78Hz+mPwPvjgU885z+Ra1CnFuHPPPgcbjPC8+9xDDznDFNRKM+fMU889+aS8z8oss5xyw/bEo80tCFEyiQCK7FEDHiY8sIAYlwTgCBccTPCBDTjgcUcLNeygQA8yxCFDDVK/CIUh09WVkiviIOzOowsJMIw+CifsjzOX6OJOxhuznTE+2bQykC/xsK2xv23P+w4vA7X/kg3G9N59d+D98tON3HoBVZcYPtx5BxAHYGAFITsF0sQGE2jQQQklmPDG5jrQ8AIdNsiQAHlYZGKoQMcAjvc+zuRL0CrhDA6PLgHswg7hedPbD94I33OMQKtkk/fgvPs+DbOoNKMPwshvrHE9xmi5VBpPiA6DCFCU0cACCHBhCCSliDEFEim4wYYHJtjBwQE53ICnHXjMmYQgcq1UyzkJ03sOLQsBBj0yNo1U5G530etd8v41DlkEoBbr6J/Z/qUwf/SjgoP7FzoelQt1ZCyB0qMXN35kEEuEQQFziMMcUgAELSDCEFIQBQOyMIjqjKIMY2hAGMYgiC0gAQQqwIEK/1pgggNs4ANYWERLmIExjeEjGQkRQCq0Mbh5/CIAp9DF7vCmsH7wox9gDKMYwWi3eQQjALkY4MHo8Y1nPMMZ0LCGNrghq3OosV/uwB0xXKcxC47xgr+TIL3KkaszqWEKN0iAC3QwhwyIIgl/IEUDllAAGERBC2RwABo4sQUpgKERWrAACN7AAQt4AAlOQAISCJGSrNgigsADADlikZBftKNg/tiGKoCiC1jirR7Z+JU0hknMYUZjGt5ARz5iCYB9hOwX+LjbOojhCitRaRWtaIUrXhGLZNzjYPXQhQCUsbF8pEMc3/DGN9b5DXGUIx30kNfB1JGLgwwCCTIQHQpIMP+FQyhhAErIAhiy4AQ3FCAEMaBPF5iAACko4QEVOEAS9hAINSjCAU34wrtUcopoyDNh+CCGQYCCims8r1/zEMZAtHiwPKaiSjCFKStk4Qx8EM4fxxDbSek1DS3ppRbggIc7hgqPdNwiFdXoIwDs0QxauCKbUNUmLGQxjHfwzh29MMgp+ICHFO2ABB3oQiZI0YcoKGEJUogCHkhAAjdkYApmIIUD+KCFLoRBEKF4xCcEAQY1cOEKimjWRyABFKDgAh5lM9g3XFFYguTiloHzhzdWsVJ23M0du3CILM5BQXo14xTCWGb/zPELWLQiFWESi1gEQItc3AIXuuCFLlDRim3/BLJe35DbQsIBvH/J44oF+YQVREeDG6zgBEKYQhjMIIgGgCEMUegADXYwhDgcIAZVaIAZzpCITaDBDGCQAggecAUrREENXVEJUm/7L3tcUS+nWMYF+3WPkImll4TDKkOAwopr3NZf0rjELr7Z23eAoxvWONYzmgGNBStDGcUIBqay8opvdBYA/VDGqRayDHKM48PjKMc2aKbaAKjhCXeKAw7ysIMKDGAAcohCFrxghSCkwEEw6AEK3GCBDPwgCahJwhwMIAE3nCAHQGjCIGRXkl/MQ5DaYBZBaIEOhI3DFaplaf/0q5CioIIZ/+1HgG/xZLcFjh/6wMc96mEPe9AD/x7mwMYvgDKLcpRtXvgIBpNrIwBW1IIWswh0LWJBQoIUYglM+4ECPlCBFlDBC2YohBrKcAALDEEGeOhqDnyQgxWMYAMXqEAGOJCCOaSHBD9gAhhMgRIuBWAVVLzbPLI6kFMc46P/4kf1CIJfPNKaIahQxn/9IQ1UvIIcC8xgwXwHj5DNwhx3q4cvwvakkaJKIINQQg18cAc2HEAPZgAFKBzQgD6IpgkkaEEHDICBCligAJo7wQ+C8IM8nGcGOEAyE1x4kuy4WoAGo6A18CUQV5iDd+igJa+3SC+s7rnWyLjzvKSRCls/T6kKXDY8bBELzgauHu9tSFZGjio13SAGF/9QAh8E0YcqLMAACtjDIDxBhQFgwAVLiMIUqACFJeSBxxwwQQ7yOQcTxMAITNgCIh6+kd0QhBXdQNjtBEKxj86LH8lwtQC03PBfL+QSyQgkBSkegFZYox6CXHb/Ak6vfTAjFuUQ+1KBS/KCuPraTwKKmuLAhieAAQsLoMAALoCFQTggvAh4ghbKkAhTYCITivCEGQ5xhSRwYA1vmMMJeDAEITjBC0o0iX4IQgwC00sf0TgVK5B9sHXYwjZaNph+mY6KZQyugAJhhTCoUQ51wGMe86CHPfRBfH4Y34tWrxc3aAGOg90j5AppxS54sQtd7MIXtyhSQQixBAwwoQtNWEP/5JQwhjLsgQMEgEIgPhEJyGsCDYQoBCMUoRs0fAEKB3jABKDwBBRAgQys5hJcg3C1EADCgHbA0w/PEDQFwXXzwmUMsQrTMDhkVxSX8AqzkAvC8Au+AAzD8IHJ0AwMZg3eUDexBA6yoA0Hsw/F0FgIIQDFkA7ssA40+A7hADYEkQZKAAR8EAUQsAMewAR+kAUpAFDr5wmD4AV7MAVNoATIgVZY8AdokAmgoQd6UAZRAAJUYAaqYxJOtyXHYFP9sg/GcAoqGEuYdRAOCAAQ2GXHdjD9wAynYAvFQAzEIAx4iGUGISVBgk3IUGb0Mg6vQA2x1A/TQEI+JRDSIIYGAw9e/1cbiOAEWRAGRwQCQNAFeiABAyAFhkAIWKBtoIYBF9AGGbABBYABGXBK0MEIjZAJgpACKKAHhuAIZjIS6VUQsXBwB9MNwZAOCIMNBNeAW0RB7IALDJEKsYAMakRB/WAMAQAN+UB80jg8JVaNAXALsGQw49AKzyBI7BAMhaYlskAOw9YOvXB3psAFhbAHD5ACHaAHVrABD/AEaKAFIGABG9ABI8ACMsAt3MIDdAAHH1BKbbAEWgAGTeAGQLAHiAATXZIMF0cv9RAPOzUv9EB3wngw8zANybAMHvmRILkM0yAOCNg/+BAy0cBeAOANsOAlXUImqrUKzCCGeDMOq3AMGf/UDtOwDMmADD6ZDD2JDMoADhU5L/RUbQMBCqTgBKKEBFhgBGvwAmXQBwOwAUHELXRiAzVQXC+gAujyAjhAAhpgARjgBi8wBGIlE1QGPQjjDVJmEGuIYV/0R2GUdgnzWwLADPxwN/gwDtPwl9EQDc3ADMpADMWwDNtwR1dnk8dgeoKDD212D/cQmf/VL+UAC3X3CGrQBBbwBlKABSBAAVXgAElwAXhQIDfQAniSIiryAjUgP2C5AzKwAjBgAzRQBGUgEOlViyfRJWCmdv1iD2eEEHGZcYLTL+QACwFADPLyL2LHMC8jmW4mfBfGMdmgCsAAWXbJlmrnD91gQNWGGZ//8QQQ8AZYkAUZ8ABZ4AA/wAFdpSA44CAyYAN0sAOJRJ93IAMqcAc9sAMtYAdVQAptgR8kcYslJgC2wHDJAwDhwAoJEZcgpED6gAynggv00FnVyUwLCgC6JgCqQIhsqWxtgw/I4IIC8QiWgAhScAEj8AVckAEQkAWb0AQVcAeBsgIMQgMo4popApYusAI4IAMx8AIKMARkQBuXUW2pIA1sdzf7QAxMVpwZtzH8EDfEYw2VaZwIcw4OhEbpcGHHyZ3+0g+HcxBHIQBbIAIiQAZhcAERcAWQ0AUDkALGNZ+y6QLzmSI4iiBTUwNz4AI1oAekMHqYURsGuhFakxC9YFUU/9RF81IOCqd1AsEL8OAvGHSpGGQ2msoP7QANylkUsbANjKiplopLpXoP4cALXNIL3/BNFlQwgHRnFYRh8CANXTpSSdEJTwACgFAGIAABUEAWTRABp5kAKmADpWMDLaAiCYAgtvkCLEAHOHA/K3GoetE88NAONEiD7OAO71AMG2YmWXEL4/AO23qu6Hqu6HAO5EAO4XANzUAMuVBofQMMzIAN39Cu55AO6KAOM7gO/IoO5dCu3mANyhAMCocqryAMzKAN44AO7eAO7RAP8lCx8vAO7ZCx6TAO1nAMs8UQllAGRBAGhWAED1AEhPAIZMADGMADKUQnMHADdKAiDAIDc/9wBwpiAzwABpDwCIlqEkhqbbnnWrdQtEWLC7iQColYG6hQC7hgtFAbtVBrC382C7HACuCZEJfACrAwC7RQC7YgMEYbtrYAaLPwCqxAr7yZCq4gC06LtNfXC77QC7mAtLhgC7PACndXQt7RBX3wCVHwABzwBY7wBUCAAAaQA3OgA3dSIjSgn/NJA3diAyqABZ9QWIeqEiYaReHZuXz2EJKad1uiWk5SJng3uhGRL3trpgNhCY3gAAKgBRggAVQAGiIQBByQAUEwB135AjeAAzqgSHigrKa2B4yQFLQxeiSxtFAiO43FdNR2uqobvUhJcrUIvRMRFtMBCQ6ABBJAAu3/QgZOcAUdUAB4UCcvoANxQANzUAelQwdBgAUeEgnZkbmam7rLqxGri7ovGLpIeRD12xl5sQWYUwXsRwqkEAY1IAEp4AO+G6Q14AI4wAJLAAaBpRTYuxHWerrWeHcZHL2ma3f4K8JCS8IfrJt8ARcBMAkCoQlckARs4AcBQAkCMAmCwAQRAAJTk5p2MAdHgAUOIMBi4Rl3sb8eccIlXHcidxFxUSaFgAVVMAqdEQBoYAUX8AFzYgRNsAeCEHrXsRT2KxJNzMEvSMJmfBHWW8LVmMZGfKBlfG1IbKgBgAlpUQnvQj5f4ARSkAV/MAoXvBWSmjj/CxEh7Ly8qcbWOMiJ/yy6/EvGiyxYtrEUc1wGo5AJcUHEYTwTgTy6cTzIbcxne8a8j5yIIdzIjgzJA7ETkYCkkGEQmdxqe+uCz8vJ1WvKqJI4pWvCzHvInrzG4krLsrxaiKzInlttz/IIQOEZr7wSv/zISczBzivC08u5zsy/aazIm9vGokzGkbEU2YEjcvEWNdHMxPzMHfzGcCxY6Mx0ShzM0oy602zLgwzGlEC/U4zJS6G8LdHOZ3zO0tvBptuN4aB1SkwQ6hAPTBbL6VzM2fy5JnrImxtF4vwIdywQkhAP5lAZ22zLgowQGe3Bp1wL9pBZb5zB7Ey68sx0yFsVmCwQuwAABWgX2wDT8v+8yB1tEDNdCyD9yMUAAMpAzQ+3DfZQgCfNyKM8EKxgD+EwEd1YDBvx0s9QEXoRDB7EoA4aDABgDku9X2LhRkscDPag1QuR1dcWD+FgD4gDzQRxcHhnD05dzQXRjTotEGRdzsUQD7qFELVwMGeU1NsQDxDRjT8dADNNTmdkDuYw1AEQDFsNugOx19uQO/ZQDacsWHsd1VxdzgUBAOqg1gWtFwfnoMSMvXJdFJwN16X8QACA2fEA2BTBCmSd1BmNEBhNyGbSjQ5qJtVglLuA1VX90+TE2TQtALrYjVG92zHdCsUdALzF2SRN1yYY1f2yDQLQjfMSDL49L4NddsVN3PT/og7KDdjkdHDd6EEjTdhL1doDIdfhEA97TS+RXQtVTdnW7dPZ7dOPvdoCEQ+dbd328NPCDQCRvQv2INzbzVusQE7FYNhgDTC7YN2zvcRFMdMlRk7PwAqtjdVLHdawvdQeRNSXHQDmANhhPRDIbdwAUAwY7trQzQq7zQsAwwq8hdRKreEBUOICceIAwAsH9+AAEAwjHgDkpAz2YNxQ3dPPINIsbtzuHQAxPuMj3go9vd1hbeM4rtrfvQsvTdkI/uQAEAAH7eL4LRBInthCnuJKDeYZXduOXRS4XW05jd5Y/dMYHeJyXRshbuEpPhBBLhAUjt4x7eSRPedkndPKMNNZ/z3nAcDmIs7i3n3myhDkwe1GMH3Zht3o671U9uCghQ7TAg4UAn7o82IOis7ol/3S6pDgP37mWB3ZFF7bip57MQMAlD3kB7PoES7hAgHZAcAL9rANFr7ipW4OHQ7mwx3iSb1UWbHbu6DnKq7eAxHjL97pL03sVY7fjM7sCs7WQ57YYn5Gcn3ZSK7kmQ7Wkd3ptRDlU57Vsj3s+R3V3VgNW87cAADbrv7lYb7b243eAJBZCp7mA8HoIGsQwWCCxB4Au+3c7h7cHz4QHkTZu03ZSK2Lkd3c6vDcTl7V0p3ROX1w9nDQ7p57FC/iXz7kVC3c4F7p+j3TMbPkMG3Y6C7f88FC2YgO8tge4SFu7D6+VGck4Ogt2QZOEFg924ZN5PNy3KfdEPo8ESU+06JdEDON8Q/h8zAB1Yve2TZx1qr+1lUB14QsEJGw9A4R3PptEHUT2RNB9TCB6MJ5E7tg8AqhHzkfET0ryRly99lr961r9yy8zEusm3gf+BqxIWI/EfTb9YKf+H9fEJbwLBnhGYWv+JLvFXnhGQLgF52cEMqMEJDf+Z7/+aAP+hAR+qHP+aL/EKR/+iWk+h3h+WBfG0TMEQEBADs=\" /></LoopModDetailData></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCCLIENTCONTACT</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"INTERNALKEY\" Value=\"0\" /></LoopDetailKey><LoopModDetailData><Field Name=\"CONTACTTYPEID\" Value=\"S\" /><Field Name=\"NUMBER\" Value=\"1234567\" /><Field Name=\"TO\" Value=\"\" /><Field Name=\"CONTACTPERSON\" Value=\"\" /><Field Name=\"POSITION\" Value=\"\" /></LoopModDetailData></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCCLIENTRELATEDCLIENTGROUP</ObjectName><MasterObjectName /><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"RELATEDCLIENTGROUP\" Value=\"1\" /></LoopDetailKey><LoopModDetailData /></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCCLIENTJOINTACC</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"INTERNALKEY\" Value=\"1\" /></LoopDetailKey><LoopModDetailData><Field Name=\"TITLE\" Value=\"1\" /><Field Name=\"JOINTNAME\" Value=\"TTL Test\" /><Field Name=\"CNAME\" Value=\"TTL En\" /><Field Name=\"BIRTHDAY\" Value=\"1995-05-08\" /><Field Name=\"IDTYPE\" Value=\"1\" /><Field Name=\"IDNUMBER\" Value=\"123456789\" /><Field Name=\"ISSUEDATE\" Value=\"\" /><Field Name=\"EXPIRYDATE\" Value=\"\" /><Field Name=\"PLACEOFISSUE\" Value=\"VN\" /><Field Name=\"CITYOFISSUE\" Value=\"\" /><Field Name=\"SEX\" Value=\"M\" /><Field Name=\"OFFICETEL\" Value=\"\" /><Field Name=\"MOBILENO\" Value=\"\" /><Field Name=\"HOMETEL\" Value=\"\" /><Field Name=\"OCCUPATIONID\" Value=\"4\" /><Field Name=\"PAGERNO\" Value=\"\" /></LoopModDetailData></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCCLIENTNOTIFICATION</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"CONTACTINTERNALKEY\" Value=\"0\" /></LoopDetailKey><LoopModDetailData><Field Name=\"LANGUAGEID\" Value=\"V\" /><Field Name=\"GROUP\" Value=\"1\" /><Field Name=\"FEE\" Value=\"12\" /><Field Name=\"TYPE\" Value=\"X\" /><Field Name=\"NOTIFICATIONTYPE\" Value=\"S\" /><Field Name=\"NUMBER\" Value=\"1234567\" /></LoopModDetailData></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCCLIENTAGENT</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"AGENTNAME\" Value=\"AAAAAAAAA\" /></LoopDetailKey><LoopModDetailData><Field Name=\"TITLE\" Value=\"1\" /><Field Name=\"CNAME\" Value=\"AAA En\" /><Field Name=\"CLIENTAGENTSTATUS\" Value=\"N\" /><Field Name=\"PASSWORD\" Value=\"111111\" /><Field Name=\"STARTDATE\" Value=\"2010-09-20\" /><Field Name=\"IDTYPE\" Value=\"1\" /><Field Name=\"PERSONALID\" Value=\"123456789\" /><Field Name=\"PLACEOFISSUE\" Value=\"VN\" /><Field Name=\"CITYOFISSUE\" Value=\"HO\" /><Field Name=\"ISSUEDATE\" Value=\"2010-10-04\" /><Field Name=\"EXPIRYDATE\" Value=\"2010-10-30\" /><Field Name=\"ATTORNEY\" Value=\"F\" /><Field Name=\"TEL\" Value=\"123456789\" /><Field Name=\"MOBILE\" Value=\"1234567\" /><Field Name=\"ADDRESS2\" Value=\"34234\" /><Field Name=\"ADDRESS1\" Value=\"qwer\" /><Field Name=\"ADDRESS4\" Value=\"234234234\" /><Field Name=\"ADDRESS3\" Value=\"234234\" /><Field Name=\"COUNTRY\" Value=\"VN\" /></LoopModDetailData></Row></LoopRows><LoopDetail><DetailNode><ObjectName>SCCLIENTAGENTPRODUCT</ObjectName><MasterObjectName>SCCLIENTAGENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /><Field DetailFieldName=\"AGENTNAME\" MasterFieldName=\"AGENTNAME\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"AGENTNAME\" Value=\"AAAAAAAAAB\" /><Field Name=\"PRODUCTID\" Value=\"HKS\" /></LoopDetailKey><LoopModDetailData /></Row></LoopRows><LoopDetail /></DetailNode></LoopDetail></DetailNode><DetailNode><ObjectName>SCCLIENTSTATUS</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows /><LoopDetail /></DetailNode><DetailNode><ObjectName>SCCLIENTBENEFICIARY</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"INTERNALKEY\" Value=\"1\" /></LoopDetailKey><LoopModDetailData><Field Name=\"TITLE\" Value=\"1\" /><Field Name=\"BENEFICIARYNAME\" Value=\"sadf\" /><Field Name=\"CNAME\" Value=\"dsadfsdf\" /><Field Name=\"IDTYPE\" Value=\"1\" /><Field Name=\"PLACEOFISSUE\" Value=\"VN\" /><Field Name=\"CITYOFISSUE\" Value=\"HO\" /><Field Name=\"IDNUMBER\" Value=\"asdasd\" /><Field Name=\"ISSUEDATE\" Value=\"2010-10-05\" /><Field Name=\"EXPIRYDATE\" Value=\"2010-10-25\" /><Field Name=\"OFFICETEL\" Value=\"123123\" /><Field Name=\"MOBILENO\" Value=\"123123\" /><Field Name=\"TEL\" Value=\"1231231\" /><Field Name=\"PAGERNO\" Value=\"123123\" /><Field Name=\"ADDRESS1\" Value=\"123\" /><Field Name=\"ADDRESS3\" Value=\"12312\" /><Field Name=\"ADDRESS2\" Value=\"123\" /><Field Name=\"COUNTRY\" Value=\"VN\" /><Field Name=\"ADDRESS4\" Value=\"3123\" /></LoopModDetailData></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCACCOUNT</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"ACCOUNTSEQ\" Value=\"1\" /></LoopDetailKey><LoopModDetailData><Field Name=\"ACOPENDATE\" Value=\"\" /><Field Name=\"INTERESTID\" Value=\"C0.2D0.0\" /><Field Name=\"LASTINTERFACESEQ\" Value=\"1\" /><Field Name=\"BANKID\" Value=\"\" /><Field Name=\"BANKACID\" Value=\"\" /><Field Name=\"PICKUPCHEQUEFLAG\" Value=\"N\" /><Field Name=\"PICKUPBRANCHID\" Value=\"\" /><Field Name=\"CHEQUENAME\" Value=\"TTL Test\" /><Field Name=\"ISSUECHEQUEFLAG\" Value=\"N\" /></LoopModDetailData></Row></LoopRows><LoopDetail><DetailNode><ObjectName>SCACCOUNTTRUSTEDCASHTRFACC</ObjectName><MasterObjectName>SCACCOUNT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /><Field DetailFieldName=\"ACCOUNTSEQ\" MasterFieldName=\"ACCOUNTSEQ\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"ACCOUNTSEQ\" Value=\"1\" /><Field Name=\"TRUSTED_CLIENTID\" Value=\"C000002\" /><Field Name=\"TRUSTED_ACCOUNTSEQ\" Value=\"1\" /></LoopDetailKey><LoopModDetailData /></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCACCOUNTINTERFACE</ObjectName><MasterObjectName>SCACCOUNT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /><Field DetailFieldName=\"ACCOUNTSEQ\" MasterFieldName=\"ACCOUNTSEQ\" /></LoopMDFieldName><LoopRows><Row><Operation>I</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"797979\" /><Field Name=\"ACCOUNTSEQ\" Value=\"1\" /><Field Name=\"INTERFACESEQ\" Value=\"1\" /></LoopDetailKey><LoopModDetailData><Field Name=\"CURRENCYID\" Value=\"VND\" /><Field Name=\"BANKID\" Value=\"OCB\" /><Field Name=\"BANKNAME\" Value=\"OCB1\" /><Field Name=\"BANKACID\" Value=\"111111111\" /><Field Name=\"OWNERNAME\" Value=\"TTL Test\" /><Field Name=\"IDNUMBER\" Value=\"123456789\" /><Field Name=\"PLACEISSUE\" Value=\"HCM\" /><Field Name=\"ISSUEDATE\" Value=\"2010-10-05\" /><Field Name=\"EFFECTIVEDATE\" Value=\"2010-10-06\" /><Field Name=\"EXPIRYDATE\" Value=\"2010-10-22\" /><Field Name=\"ISDEFAULT\" Value=\"N\" /><Field Name=\"AUTODEBITEXTERNALFLAG\" Value=\"N\" /><Field Name=\"WAIVEFLAG\" Value=\"N\" /><Field Name=\"TRANSFERFLAG\" Value=\"N\" /><Field Name=\"AUTOCREDITEXTERNALFLAG\" Value=\"N\" /><Field Name=\"REGISTEREDFLAG\" Value=\"\" /></LoopModDetailData></Row></LoopRows><LoopDetail /></DetailNode></LoopDetail></DetailNode></LoopDetail></UPD></MessageContent></LoopMessage></LINKFUNCS>";
	     String lvUpdate = "<LINKFUNCS msgId=\"LINKFUNCS\" issueTime=\"\" issueLoc=\" \" issueMetd=\"\" oprId=\"COMPANY\" pwd=\"\" resvr=\"\" language=\"en\" country=\"US\" entityId=\"VDSC\"><FuncName>CORCL001</FuncName><OverrideApproval>N</OverrideApproval><LoopMessage><MessageContent><UPD msgId=\"UPD\" issueTime=\"\" issueLoc=\" \" issueMetd=\"\" oprId=\"COMPANY\" pwd=\"\" resvr=\"\" language=\"en\" country=\"US\" entityId=\"VDSC\"><FuncName>CORCL001</FuncName><ObjectName>SCCLIENT</ObjectName><Operation>U</Operation><Version /><LoopMasterKey><Field Name=\"CLIENTID\" Value=\"C797979\" /><Field Name=\"STATE\" Value=\"\" /></LoopMasterKey><LoopModMasterData><Field Name=\"PLACEOFISSUE\" Value=\"VN\" /><Field Name=\"COUNTRYOFRESIDENCE\" Value=\"VN\" /><Field Name=\"CLIENTINTRODUCER\" Value=\"1111111111111\" /><Field Name=\"CLIENTTYPE\" Value=\"I\" /><Field Name=\"CLIENTID\" Value=\"C797979\" /><Field Name=\"ACOPENDATE\" Value=\"\" /><Field Name=\"ISSTAFF\" Value=\"Y\" /><Field Name=\"OCCUPATIONID\" Value=\"4\" /><Field Name=\"BASECCY\" Value=\"VND\" /><Field Name=\"CNAME\" Value=\"TTL En\" /><Field Name=\"BUSINESSNATUREID\" Value=\"\" /><Field Name=\"CITYOFISSUE\" Value=\"HO\" /><Field Name=\"LASTNAME\" Value=\"TTL Test\" /><Field Name=\"BRANCHID\" Value=\"0\" /><Field Name=\"REGISTRATIONCAPITAL\" Value=\"\" /><Field Name=\"REMARKS\" Value=\"Remark2\" /><Field Name=\"NATIONID\" Value=\"VN\" /><Field Name=\"LASTACCOUNTSEQ\" Value=\"1\" /><Field Name=\"EXPIRY\" Value=\"2012-10-27\" /><Field Name=\"REGISTRATIONTYPE\" Value=\"1\" /><Field Name=\"FIRSTNAME\" Value=\"TTL Test\" /><Field Name=\"CLIENTREMARKS\" Value=\"remark1\" /><Field Name=\"IDNUMBER\" Value=\"123456789\" /><Field Name=\"SEX\" Value=\"M\" /><Field Name=\"BIRTHDAY\" Value=\"1995-05-08\" /><Field Name=\"SUBACCGROUP\" Value=\"1\" /><Field Name=\"ISSUEDATE\" Value=\"2010-05-03\" /><Field Name=\"DISCRETIONARYFLAG\" Value=\"Y\" /><Field Name=\"ISDIRECTMARKETINGREFUSED\" Value=\"Y\" /><Field Name=\"ISHOUSE\" Value=\"N\" /><Field Name=\"EMPLOYERNAME\" Value=\"TTL VN 123\" /><Field Name=\"ACCOUNTTYPE\" Value=\"N\" /><Field Name=\"IDTYPE\" Value=\"1\" /><Field Name=\"OBJECTIVEID\" Value=\"1\" /></LoopModMasterData><LoopDetail><DetailNode><ObjectName>SCCLIENTADDRESS</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>U</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"C797979\" /><Field Name=\"ADDRESSTYPEID\" Value=\"8\" /></LoopDetailKey><LoopModDetailData><Field Name=\"ADDRESS1\" Value=\"Test address after update\" /><Field Name=\"ADDRESS2\" Value=\"111\" /><Field Name=\"ADDRESS3\" Value=\"111\" /><Field Name=\"ADDRESS4\" Value=\"HCM\" /><Field Name=\"ADDRESS5\" Value=\"084\" /><Field Name=\"COUNTRY\" Value=\"VN\" /><Field Name=\"COUNTRYTYPE\" Value=\"L\" /></LoopModDetailData></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCCLIENTSIGNATURE</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>U</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"C797979\" /><Field Name=\"FILENAME\" Value=\"logo.gif\" /></LoopDetailKey><LoopModDetailData><Field Name=\"DESCRIPTION\" Value=\"asdfsadasd\" /><Field Name=\"ISAGENT\" Value=\"Y\" /><Field Name=\"FILEVALUE\" Value=\"R0lGODlh1gBHAPcAAAAAAP////7+/uoBAuoHCusJDuoMEewSF+0ZHOsdIeweIu0mKvIqLfmjpPrLzOsABusBCuwCCewDDOoDC+0FC+0GD+sGDu0JEeoJEesKFOwMEuwMFesPFe0RGeoRGe0THesTG/AUHu4UGusUHO0WHu0XH+8YIO4YIusZIusZH/EbJOwaI+4bIe4bIu4bJO0bJOwbIeobIu4cJe0cIe0cIuscIuscJO4dI+wdJe0eJuseI+seJe8fJ+8fJe0fJOwfJ+4hKuwhJu0jLOsiKvAkLO8nMO4nLesoL+4rM+8uNu8zOe82Pe86QfA+RO9DSPFFTPBJUPFLUvJRVvNVW+9UWPBYXvFhZfNlavR1efV+gfSChvWFivWJjfaNkPWVmPianfaZnPeeoviipvenqfisrvq5vPrCxP3X2Pzb3P3h4v3m5+0FEe0KFvALF+0OGewTHu4YJO4aJukaJO8cJ+0bJusbKOwdJ+sdJ+0eKeweKPJdZPRsc/iRl/WQlfqmq/iorPmus/mwtfq+wfvGyfvO0fzT1fze4PeUm/zv8P3i5f719v309f3m6f/2+f/8/f/9/v/+//z//+3u7vv//v3//P7++////fv7+v769/78+/7w7fvm4/rJxfri4P3p5/708/3k4/vm5eYCAPrZ2f7s7P7x8f75+f39/fj4+Pf39/T09PHx8e7u7u3t7erq6ujo6Obm5uXl5ePj4+Dg4N/f393d3dnZ2dbW1tTU1NHR0c7OzszMzMvLy8jIyMbGxsHBwb29vbu7u7i4uLGxsa2traqqqqioqKSkpJ+fn5ubm5mZmZWVlZKSko6OjouLi4iIiIaGhoODg4CAgH19fXp6end3d3R0dHJycnFxcW5ubmpqamZmZmVlZWJiYl5eXlpaWlhYWFVVVVFRUU5OTkpKSkdHR0RERD8/Pzo6OjY2NjMzMzAwMC8vLywsLCsrKykpKSYmJiIiIh4eHhoaGhYWFhISEhEREQ4ODgwMDAoKCgkJCQcHBwUFBQMDAwEBAf///yH5BAEAAP8ALAAAAADWAEcAAAj/AAMIHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePIEOKHEmypMmTKFOqXMmypcuXMGPKnEmzps2bOGdCCgCpkgABOYMKnQhUoCVIloxC2unzZyVHmUz55GmUKlWkRYdqvbmTINJIPQUKKKUG1KY0htKUkgp0pyUBSQMcXVppq92YO7PyjBTgEaRIjgwNasBFy5YtXPr8cZBGUaW3OyMndXu3sksBkE4hnZTUkqVSZbJMUZJkCZMmTZyohqJnCxk1PNtCohRAr+XbKR/xpeo5zRUhMe7w+CFEiJEjR4oYMVJkAZAqYzQFqGvJEU9Lj3BrL9m1K1DdhIpk/5hzww4NHTJk/OCho0eeHD9svCByxQFVAXzjbt8fUvZ0sY5gIYEJONyQ3gsuvCCfDDe4kOAcNcjABCB1FXUUfxiCRIlng5CC1RlIENADDjXIV0MNL8jwAg044NDCCz+4cEcRYdS1FFgqqYLKjjumgkpEqPjIoyqXCBAkj0gmqeSRp/xU20FAXbKkKlSqIuSORdpGUFECnOKll5d8KWaTtjlZ0I2WmKHEFR561oABGwihwAtz0KHADTa4QIMLCrgwBw42GHiEGJOplBQ12VijqDXYUGOLQ6cAk801i2KDTS6uPJPoopx22uk12WhDzTPEyHLKQFnp0gyolC5ajTbddP/jjTaLJvqMMK8cdMorvyDzjDTUTFONp4pWQw010SjDiypQCvRYAGdAUcIEUhTCFyZaFMBBDjgooMAKLtxQAx10RGjDDDekeEMSZfiUnUrQ8APAvPP2Qw0qWhrkSjn0AuAPAPHYcos8/f7b78Hz+mPwPvjgU885z+Ra1CnFuHPPPgcbjPC8+9xDDznDFNRKM+fMU889+aS8z8oss5xyw/bEo80tCFEyiQCK7FEDHiY8sIAYlwTgCBccTPCBDTjgcUcLNeygQA8yxCFDDVK/CIUh09WVkiviIOzOowsJMIw+CifsjzOX6OJOxhuznTE+2bQykC/xsK2xv23P+w4vA7X/kg3G9N59d+D98tON3HoBVZcYPtx5BxAHYGAFITsF0sQGE2jQQQklmPDG5jrQ8AIdNsiQAHlYZGKoQMcAjvc+zuRL0CrhDA6PLgHswg7hedPbD94I33OMQKtkk/fgvPs+DbOoNKMPwshvrHE9xmi5VBpPiA6DCFCU0cACCHBhCCSliDEFEim4wYYHJtjBwQE53ICnHXjMmYQgcq1UyzkJ03sOLQsBBj0yNo1U5G530etd8v41DlkEoBbr6J/Z/qUwf/SjgoP7FzoelQt1ZCyB0qMXN35kEEuEQQFziMMcUgAELSDCEFIQBQOyMIjqjKIMY2hAGMYgiC0gAQQqwIEK/1pgggNs4ANYWERLmIExjeEjGQkRQCq0Mbh5/CIAp9DF7vCmsH7wox9gDKMYwWi3eQQjALkY4MHo8Y1nPMMZ0LCGNrghq3OosV/uwB0xXKcxC47xgr+TIL3KkaszqWEKN0iAC3QwhwyIIgl/IEUDllAAGERBC2RwABo4sQUpgKERWrAACN7AAQt4AAlOQAISCJGSrNgigsADADlikZBftKNg/tiGKoCiC1jirR7Z+JU0hknMYUZjGt5ARz5iCYB9hOwX+LjbOojhCitRaRWtaIUrXhGLZNzjYPXQhQCUsbF8pEMc3/DGN9b5DXGUIx30kNfB1JGLgwwCCTIQHQpIMP+FQyhhAErIAhiy4AQ3FCAEMaBPF5iAACko4QEVOEAS9hAINSjCAU34wrtUcopoyDNh+CCGQYCCims8r1/zEMZAtHiwPKaiSjCFKStk4Qx8EM4fxxDbSek1DS3ppRbggIc7hgqPdNwiFdXoIwDs0QxauCKbUNUmLGQxjHfwzh29MMgp+ICHFO2ABB3oQiZI0YcoKGEJUogCHkhAAjdkYApmIIUD+KCFLoRBEKF4xCcEAQY1cOEKimjWRyABFKDgAh5lM9g3XFFYguTiloHzhzdWsVJ23M0du3CILM5BQXo14xTCWGb/zPELWLQiFWESi1gEQItc3AIXuuCFLlDRim3/BLJe35DbQsIBvH/J44oF+YQVREeDG6zgBEKYQhjMIIgGgCEMUegADXYwhDgcIAZVaIAZzpCITaDBDGCQAggecAUrREENXVEJUm/7L3tcUS+nWMYF+3WPkImll4TDKkOAwopr3NZf0rjELr7Z23eAoxvWONYzmgGNBStDGcUIBqay8opvdBYA/VDGqRayDHKM48PjKMc2aKbaAKjhCXeKAw7ysIMKDGAAcohCFrxghSCkwEEw6AEK3GCBDPwgCahJwhwMIAE3nCAHQGjCIGRXkl/MQ5DaYBZBaIEOhI3DFaplaf/0q5CioIIZ/+1HgG/xZLcFjh/6wMc96mEPe9AD/x7mwMYvgDKLcpRtXvgIBpNrIwBW1IIWswh0LWJBQoIUYglM+4ECPlCBFlDBC2YohBrKcAALDEEGeOhqDnyQgxWMYAMXqEAGOJCCOaSHBD9gAhhMgRIuBWAVVLzbPLI6kFMc46P/4kf1CIJfPNKaIahQxn/9IQ1UvIIcC8xgwXwHj5DNwhx3q4cvwvakkaJKIINQQg18cAc2HEAPZgAFKBzQgD6IpgkkaEEHDICBCligAJo7wQ+C8IM8nGcGOEAyE1x4kuy4WoAGo6A18CUQV5iDd+igJa+3SC+s7rnWyLjzvKSRCls/T6kKXDY8bBELzgauHu9tSFZGjio13SAGF/9QAh8E0YcqLMAACtjDIDxBhQFgwAVLiMIUqACFJeSBxxwwQQ7yOQcTxMAITNgCIh6+kd0QhBXdQNjtBEKxj86LH8lwtQC03PBfL+QSyQgkBSkegFZYox6CXHb/Ak6vfTAjFuUQ+1KBS/KCuPraTwKKmuLAhieAAQsLoMAALoCFQTggvAh4ghbKkAhTYCITivCEGQ5xhSRwYA1vmMMJeDAEITjBC0o0iX4IQgwC00sf0TgVK5B9sHXYwjZaNph+mY6KZQyugAJhhTCoUQ51wGMe86CHPfRBfH4Y34tWrxc3aAGOg90j5AppxS54sQtd7MIXtyhSQQixBAwwoQtNWEP/5JQwhjLsgQMEgEIgPhEJyGsCDYQoBCMUoRs0fAEKB3jABKDwBBRAgQys5hJcg3C1EADCgHbA0w/PEDQFwXXzwmUMsQrTMDhkVxSX8AqzkAvC8Au+AAzD8IHJ0AwMZg3eUDexBA6yoA0Hsw/F0FgIIQDFkA7ssA40+A7hADYEkQZKAAR8EAUQsAMewAR+kAUpAFDr5wmD4AV7MAVNoATIgVZY8AdokAmgoQd6UAZRAAJUYAaqYxJOtyXHYFP9sg/GcAoqGEuYdRAOCAAQ2GXHdjD9wAynYAvFQAzEIAx4iGUGISVBgk3IUGb0Mg6vQA2x1A/TQEI+JRDSIIYGAw9e/1cbiOAEWRAGRwQCQNAFeiABAyAFhkAIWKBtoIYBF9AGGbABBYABGXBK0MEIjZAJgpACKKAHhuAIZjIS6VUQsXBwB9MNwZAOCIMNBNeAW0RB7IALDJEKsYAMakRB/WAMAQAN+UB80jg8JVaNAXALsGQw49AKzyBI7BAMhaYlskAOw9YOvXB3psAFhbAHD5ACHaAHVrABD/AEaKAFIGABG9ABI8ACMsAt3MIDdAAHH1BKbbAEWgAGTeAGQLAHiAATXZIMF0cv9RAPOzUv9EB3wngw8zANybAMHvmRILkM0yAOCNg/+BAy0cBeAOANsOAlXUImqrUKzCCGeDMOq3AMGf/UDtOwDMmADD6ZDD2JDMoADhU5L/RUbQMBCqTgBKKEBFhgBGvwAmXQBwOwAUHELXRiAzVQXC+gAujyAjhAAhpgARjgBi8wBGIlE1QGPQjjDVJmEGuIYV/0R2GUdgnzWwLADPxwN/gwDtPwl9EQDc3ADMpADMWwDNtwR1dnk8dgeoKDD212D/cQmf/VL+UAC3X3CGrQBBbwBlKABSBAAVXgAElwAXhQIDfQAniSIiryAjUgP2C5AzKwAjBgAzRQBGUgEOlViyfRJWCmdv1iD2eEEHGZcYLTL+QACwFADPLyL2LHMC8jmW4mfBfGMdmgCsAAWXbJlmrnD91gQNWGGZ//8QQQ8AZYkAUZ8ABZ4AA/wAFdpSA44CAyYAN0sAOJRJ93IAMqcAc9sAMtYAdVQAptgR8kcYslJgC2wHDJAwDhwAoJEZcgpED6gAynggv00FnVyUwLCgC6JgCqQIhsqWxtgw/I4IIC8QiWgAhScAEj8AVckAEQkAWb0AQVcAeBsgIMQgMo4popApYusAI4IAMx8AIKMARkQBuXUW2pIA1sdzf7QAxMVpwZtzH8EDfEYw2VaZwIcw4OhEbpcGHHyZ3+0g+HcxBHIQBbIAIiQAZhcAERcAWQ0AUDkALGNZ+y6QLzmSI4iiBTUwNz4AI1oAekMHqYURsGuhFakxC9YFUU/9RF81IOCqd1AsEL8OAvGHSpGGQ2msoP7QANylkUsbANjKiplopLpXoP4cALXNIL3/BNFlQwgHRnFYRh8CANXTpSSdEJTwACgFAGIAABUEAWTRABp5kAKmADpWMDLaAiCYAgtvkCLEAHOHA/K3GoetE88NAONEiD7OAO71AMG2YmWXEL4/AO23qu6Hqu6HAO5EAO4XANzUAMuVBofQMMzIAN39Cu55AO6KAOM7gO/IoO5dCu3mANyhAMCocqryAMzKAN44AO7eAO7RAP8lCx8vAO7ZCx6TAO1nAMs8UQllAGRBAGhWAED1AEhPAIZMADGMADKUQnMHADdKAiDAIDc/9wBwpiAzwABpDwCIlqEkhqbbnnWrdQtEWLC7iQColYG6hQC7hgtFAbtVBrC382C7HACuCZEJfACrAwC7RQC7YgMEYbtrYAaLPwCqxAr7yZCq4gC06LtNfXC77QC7mAtLhgC7PACndXQt7RBX3wCVHwABzwBY7wBUCAAAaQA3OgA3dSIjSgn/NJA3diAyqABZ9QWIeqEiYaReHZuXz2EJKad1uiWk5SJng3uhGRL3trpgNhCY3gAAKgBRggAVQAGiIQBByQAUEwB135AjeAAzqgSHigrKa2B4yQFLQxeiSxtFAiO43FdNR2uqobvUhJcrUIvRMRFtMBCQ6ABBJAAu3/QgZOcAUdUAB4UCcvoANxQANzUAelQwdBgAUeEgnZkbmam7rLqxGri7ovGLpIeRD12xl5sQWYUwXsRwqkEAY1IAEp4AO+G6Q14AI4wAJLAAaBpRTYuxHWerrWeHcZHL2ma3f4K8JCS8IfrJt8ARcBMAkCoQlckARs4AcBQAkCMAmCwAQRAAJTk5p2MAdHgAUOIMBi4Rl3sb8eccIlXHcidxFxUSaFgAVVMAqdEQBoYAUX8AFzYgRNsAeCEHrXsRT2KxJNzMEvSMJmfBHWW8LVmMZGfKBlfG1IbKgBgAlpUQnvQj5f4ARSkAV/MAoXvBWSmjj/CxEh7Ly8qcbWOMiJ/yy6/EvGiyxYtrEUc1wGo5AJcUHEYTwTgTy6cTzIbcxne8a8j5yIIdzIjgzJA7ETkYCkkGEQmdxqe+uCz8vJ1WvKqJI4pWvCzHvInrzG4krLsrxaiKzInlttz/IIQOEZr7wSv/zISczBzivC08u5zsy/aazIm9vGokzGkbEU2YEjcvEWNdHMxPzMHfzGcCxY6Mx0ShzM0oy602zLgwzGlEC/U4zJS6G8LdHOZ3zO0tvBptuN4aB1SkwQ6hAPTBbL6VzM2fy5JnrImxtF4vwIdywQkhAP5lAZ22zLgowQGe3Bp1wL9pBZb5zB7Ey68sx0yFsVmCwQuwAABWgX2wDT8v+8yB1tEDNdCyD9yMUAAMpAzQ+3DfZQgCfNyKM8EKxgD+EwEd1YDBvx0s9QEXoRDB7EoA4aDABgDku9X2LhRkscDPag1QuR1dcWD+FgD4gDzQRxcHhnD05dzQXRjTotEGRdzsUQD7qFELVwMGeU1NsQDxDRjT8dADNNTmdkDuYw1AEQDFsNugOx19uQO/ZQDacsWHsd1VxdzgUBAOqg1gWtFwfnoMSMvXJdFJwN16X8QACA2fEA2BTBCmSd1BmNEBhNyGbSjQ5qJtVglLuA1VX90+TE2TQtALrYjVG92zHdCsUdALzF2SRN1yYY1f2yDQLQjfMSDL49L4NddsVN3PT/og7KDdjkdHDd6EEjTdhL1doDIdfhEA97TS+RXQtVTdnW7dPZ7dOPvdoCEQ+dbd328NPCDQCRvQv2INzbzVusQE7FYNhgDTC7YN2zvcRFMdMlRk7PwAqtjdVLHdawvdQeRNSXHQDmANhhPRDIbdwAUAwY7trQzQq7zQsAwwq8hdRKreEBUOICceIAwAsH9+AAEAwjHgDkpAz2YNxQ3dPPINIsbtzuHQAxPuMj3go9vd1hbeM4rtrfvQsvTdkI/uQAEAAH7eL4LRBInthCnuJKDeYZXduOXRS4XW05jd5Y/dMYHeJyXRshbuEpPhBBLhAUjt4x7eSRPedkndPKMNNZ/z3nAcDmIs7i3n3myhDkwe1GMH3Zht3o671U9uCghQ7TAg4UAn7o82IOis7ol/3S6pDgP37mWB3ZFF7bip57MQMAlD3kB7PoES7hAgHZAcAL9rANFr7ipW4OHQ7mwx3iSb1UWbHbu6DnKq7eAxHjL97pL03sVY7fjM7sCs7WQ57YYn5Gcn3ZSK7kmQ7Wkd3ptRDlU57Vsj3s+R3V3VgNW87cAADbrv7lYb7b243eAJBZCp7mA8HoIGsQwWCCxB4Au+3c7h7cHz4QHkTZu03ZSK2Lkd3c6vDcTl7V0p3ROX1w9nDQ7p57FC/iXz7kVC3c4F7p+j3TMbPkMG3Y6C7f88FC2YgO8tge4SFu7D6+VGck4Ogt2QZOEFg924ZN5PNy3KfdEPo8ESU+06JdEDON8Q/h8zAB1Yve2TZx1qr+1lUB14QsEJGw9A4R3PptEHUT2RNB9TCB6MJ5E7tg8AqhHzkfET0ryRly99lr961r9yy8zEusm3gf+BqxIWI/EfTb9YKf+H9fEJbwLBnhGYWv+JLvFXnhGQLgF52cEMqMEJDf+Z7/+aAP+hAR+qHP+aL/EKR/+iWk+h3h+WBfG0TMEQEBADs=\" /></LoopModDetailData></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCCLIENTCONTACT</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>U</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"C797979\" /><Field Name=\"INTERNALKEY\" Value=\"0\" /></LoopDetailKey><LoopModDetailData><Field Name=\"CONTACTTYPEID\" Value=\"S\" /><Field Name=\"NUMBER\" Value=\"1234567\" /><Field Name=\"TO\" Value=\"\" /><Field Name=\"CONTACTPERSON\" Value=\"\" /><Field Name=\"POSITION\" Value=\"\" /></LoopModDetailData></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCCLIENTRELATEDCLIENTGROUP</ObjectName><MasterObjectName /><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>U</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"C797979\" /><Field Name=\"RELATEDCLIENTGROUP\" Value=\"1\" /></LoopDetailKey><LoopModDetailData /></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCCLIENTJOINTACC</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>U</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"C797979\" /><Field Name=\"INTERNALKEY\" Value=\"1\" /></LoopDetailKey><LoopModDetailData><Field Name=\"TITLE\" Value=\"1\" /><Field Name=\"JOINTNAME\" Value=\"TTL Test\" /><Field Name=\"CNAME\" Value=\"TTL En\" /><Field Name=\"BIRTHDAY\" Value=\"1995-05-08\" /><Field Name=\"IDTYPE\" Value=\"1\" /><Field Name=\"IDNUMBER\" Value=\"123456789\" /><Field Name=\"ISSUEDATE\" Value=\"\" /><Field Name=\"EXPIRYDATE\" Value=\"\" /><Field Name=\"PLACEOFISSUE\" Value=\"VN\" /><Field Name=\"CITYOFISSUE\" Value=\"\" /><Field Name=\"SEX\" Value=\"M\" /><Field Name=\"OFFICETEL\" Value=\"\" /><Field Name=\"MOBILENO\" Value=\"\" /><Field Name=\"HOMETEL\" Value=\"\" /><Field Name=\"OCCUPATIONID\" Value=\"4\" /><Field Name=\"PAGERNO\" Value=\"\" /></LoopModDetailData></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCCLIENTNOTIFICATION</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>U</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"C797979\" /><Field Name=\"CONTACTINTERNALKEY\" Value=\"0\" /></LoopDetailKey><LoopModDetailData><Field Name=\"LANGUAGEID\" Value=\"V\" /><Field Name=\"GROUP\" Value=\"1\" /><Field Name=\"FEE\" Value=\"12\" /><Field Name=\"TYPE\" Value=\"X\" /><Field Name=\"NOTIFICATIONTYPE\" Value=\"S\" /><Field Name=\"NUMBER\" Value=\"1234567\" /></LoopModDetailData></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCCLIENTAGENT</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>U</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"C797979\" /><Field Name=\"AGENTNAME\" Value=\"AAAAAAAAA\" /></LoopDetailKey><LoopModDetailData><Field Name=\"TITLE\" Value=\"1\" /><Field Name=\"CNAME\" Value=\"AAA En\" /><Field Name=\"CLIENTAGENTSTATUS\" Value=\"N\" /><Field Name=\"PASSWORD\" Value=\"111111\" /><Field Name=\"STARTDATE\" Value=\"2010-09-20\" /><Field Name=\"IDTYPE\" Value=\"1\" /><Field Name=\"PERSONALID\" Value=\"123456789\" /><Field Name=\"PLACEOFISSUE\" Value=\"VN\" /><Field Name=\"CITYOFISSUE\" Value=\"HO\" /><Field Name=\"ISSUEDATE\" Value=\"2010-10-04\" /><Field Name=\"EXPIRYDATE\" Value=\"2010-10-30\" /><Field Name=\"ATTORNEY\" Value=\"F\" /><Field Name=\"TEL\" Value=\"123456789\" /><Field Name=\"MOBILE\" Value=\"1234567\" /><Field Name=\"ADDRESS2\" Value=\"34234\" /><Field Name=\"ADDRESS1\" Value=\"qwer\" /><Field Name=\"ADDRESS4\" Value=\"234234234\" /><Field Name=\"ADDRESS3\" Value=\"234234\" /><Field Name=\"COUNTRY\" Value=\"VN\" /></LoopModDetailData></Row></LoopRows><LoopDetail><DetailNode><ObjectName>SCCLIENTAGENTPRODUCT</ObjectName><MasterObjectName>SCCLIENTAGENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /><Field DetailFieldName=\"AGENTNAME\" MasterFieldName=\"AGENTNAME\" /></LoopMDFieldName><LoopRows><Row><Operation>U</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"C797979\" /><Field Name=\"AGENTNAME\" Value=\"AAAAAAAAAB\" /><Field Name=\"PRODUCTID\" Value=\"HKS\" /></LoopDetailKey><LoopModDetailData /></Row></LoopRows><LoopDetail /></DetailNode></LoopDetail></DetailNode><DetailNode><ObjectName>SCCLIENTSTATUS</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows /><LoopDetail /></DetailNode><DetailNode><ObjectName>SCCLIENTBENEFICIARY</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>U</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"C797979\" /><Field Name=\"INTERNALKEY\" Value=\"1\" /></LoopDetailKey><LoopModDetailData><Field Name=\"TITLE\" Value=\"1\" /><Field Name=\"BENEFICIARYNAME\" Value=\"sadf\" /><Field Name=\"CNAME\" Value=\"dsadfsdf\" /><Field Name=\"IDTYPE\" Value=\"1\" /><Field Name=\"PLACEOFISSUE\" Value=\"VN\" /><Field Name=\"CITYOFISSUE\" Value=\"HO\" /><Field Name=\"IDNUMBER\" Value=\"asdasd\" /><Field Name=\"ISSUEDATE\" Value=\"2010-10-05\" /><Field Name=\"EXPIRYDATE\" Value=\"2010-10-25\" /><Field Name=\"OFFICETEL\" Value=\"123123\" /><Field Name=\"MOBILENO\" Value=\"123123\" /><Field Name=\"TEL\" Value=\"1231231\" /><Field Name=\"PAGERNO\" Value=\"123123\" /><Field Name=\"ADDRESS1\" Value=\"123\" /><Field Name=\"ADDRESS3\" Value=\"12312\" /><Field Name=\"ADDRESS2\" Value=\"123\" /><Field Name=\"COUNTRY\" Value=\"VN\" /><Field Name=\"ADDRESS4\" Value=\"3123\" /></LoopModDetailData></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCACCOUNT</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopRows><Row><Operation>U</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"C797979\" /><Field Name=\"ACCOUNTSEQ\" Value=\"1\" /></LoopDetailKey><LoopModDetailData><Field Name=\"ACOPENDATE\" Value=\"\" /><Field Name=\"INTERESTID\" Value=\"C0.2D0.0\" /><Field Name=\"LASTINTERFACESEQ\" Value=\"1\" /><Field Name=\"BANKID\" Value=\"\" /><Field Name=\"BANKACID\" Value=\"\" /><Field Name=\"PICKUPCHEQUEFLAG\" Value=\"N\" /><Field Name=\"PICKUPBRANCHID\" Value=\"\" /><Field Name=\"CHEQUENAME\" Value=\"TTL Test\" /><Field Name=\"ISSUECHEQUEFLAG\" Value=\"N\" /></LoopModDetailData></Row></LoopRows><LoopDetail><DetailNode><ObjectName>SCACCOUNTTRUSTEDCASHTRFACC</ObjectName><MasterObjectName>SCACCOUNT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /><Field DetailFieldName=\"ACCOUNTSEQ\" MasterFieldName=\"ACCOUNTSEQ\" /></LoopMDFieldName><LoopRows><Row><Operation>U</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"C797979\" /><Field Name=\"ACCOUNTSEQ\" Value=\"1\" /><Field Name=\"TRUSTED_CLIENTID\" Value=\"C000002\" /><Field Name=\"TRUSTED_ACCOUNTSEQ\" Value=\"1\" /></LoopDetailKey><LoopModDetailData /></Row></LoopRows><LoopDetail /></DetailNode><DetailNode><ObjectName>SCACCOUNTINTERFACE</ObjectName><MasterObjectName>SCACCOUNT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /><Field DetailFieldName=\"ACCOUNTSEQ\" MasterFieldName=\"ACCOUNTSEQ\" /></LoopMDFieldName><LoopRows><Row><Operation>U</Operation><LoopDetailKey><Field Name=\"CLIENTID\" Value=\"C797979\" /><Field Name=\"ACCOUNTSEQ\" Value=\"1\" /><Field Name=\"INTERFACESEQ\" Value=\"1\" /></LoopDetailKey><LoopModDetailData><Field Name=\"CURRENCYID\" Value=\"VND\" /><Field Name=\"BANKID\" Value=\"OCB\" /><Field Name=\"BANKNAME\" Value=\"OCB1\" /><Field Name=\"BANKACID\" Value=\"111111111\" /><Field Name=\"OWNERNAME\" Value=\"TTL Test\" /><Field Name=\"IDNUMBER\" Value=\"123456789\" /><Field Name=\"PLACEISSUE\" Value=\"HCM\" /><Field Name=\"ISSUEDATE\" Value=\"2010-10-05\" /><Field Name=\"EFFECTIVEDATE\" Value=\"2010-10-06\" /><Field Name=\"EXPIRYDATE\" Value=\"2010-10-22\" /><Field Name=\"ISDEFAULT\" Value=\"N\" /><Field Name=\"AUTODEBITEXTERNALFLAG\" Value=\"N\" /><Field Name=\"WAIVEFLAG\" Value=\"N\" /><Field Name=\"TRANSFERFLAG\" Value=\"N\" /><Field Name=\"AUTOCREDITEXTERNALFLAG\" Value=\"N\" /><Field Name=\"REGISTEREDFLAG\" Value=\"\" /></LoopModDetailData></Row></LoopRows><LoopDetail /></DetailNode></LoopDetail></DetailNode></LoopDetail></UPD></MessageContent></LoopMessage></LINKFUNCS>";
	     
	     String lvUpdateApproval="<LINKFUNCS msgId=\"LINKFUNCS\" issueTime=\"20101004080058\" issueLoc=\" \" issueMetd=\"01\" oprId=\"COMPANY\" pwd=\"\" resvr=\"0000000000000000000000024\" language=\"en\" country=\"US\" entityId=\"VDSC\"><FuncName>CORCL001A</FuncName><OverrideApproval>N</OverrideApproval><LoopMessage><MessageContent><APPROVALRPL msgId=\"APPROVALRPL\" issueTime=\"20101004080058\" issueLoc=\" \" issueMetd=\"01\" oprId=\"COMPANY\" pwd=\"\" resvr=\"0000000000000000000000025\" language=\"en\" country=\"US\" entityId=\"VDSC\"><FuncName>CORCL001A</FuncName><ApprovalName>CORCL001</ApprovalName><ApprovalState>A</ApprovalState><PendAction>U</PendAction><SupervisorID>COMPANY</SupervisorID><SupervisorPassword>ttl123456</SupervisorPassword><ObjectName>SCCLIENT</ObjectName><Version>2010-10-04 19:46:33.579</Version><LoopKeyValue><Field Name=\"CLIENTID\" Value=\"C797979\" /><Field Name=\"STATE\" Value=\"P\" /></LoopKeyValue><LoopDetail><ObjectName>SCCLIENTSTATUS</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopDetailFieldName><Field Name=\"CLIENTID\" /><Field Name=\"CLIENTSTATUS\" /><Field Name=\"STATE\" /></LoopDetailFieldName><LoopRowValue><RowValue v0=\"C797979\" PENDACTION=\"X\" v1=\"N\" v2=\"P\" /></LoopRowValue><LoopDetail /><ObjectName>SCCLIENTRELATEDCLIENTGROUP</ObjectName><MasterObjectName /><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopDetailFieldName><Field Name=\"CLIENTID\" /><Field Name=\"RELATEDCLIENTGROUP\" /><Field Name=\"STATE\" /></LoopDetailFieldName><LoopRowValue><RowValue v0=\"C797979\" PENDACTION=\"X\" v1=\"1\" v2=\"P\" /></LoopRowValue><LoopDetail /><ObjectName>SCACCOUNT</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopDetailFieldName><Field Name=\"CLIENTID\" /><Field Name=\"ACOPENDATE\" /><Field Name=\"ACCOUNTSEQ\" /><Field Name=\"INTERESTID\" /><Field Name=\"LASTINTERFACESEQ\" /><Field Name=\"BANKID\" /><Field Name=\"BANKACID\" /><Field Name=\"PICKUPCHEQUEFLAG\" /><Field Name=\"PICKUPBRANCHID\" /><Field Name=\"CHEQUENAME\" /><Field Name=\"ISSUECHEQUEFLAG\" /><Field Name=\"STATE\" /></LoopDetailFieldName><LoopRowValue><RowValue v0=\"C797979\" PENDACTION=\"X\" v1=\"\" v2=\"1\" v3=\"C0.2D0.0\" v4=\"1\" v5=\"\" v6=\"\" v7=\"N\" v8=\"\" v9=\"TTL Test\" v10=\"N\" v11=\"P\" /></LoopRowValue><LoopDetail><ObjectName>SCACCOUNTTRUSTEDCASHTRFACC</ObjectName><MasterObjectName>SCACCOUNT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /><Field DetailFieldName=\"ACCOUNTSEQ\" MasterFieldName=\"ACCOUNTSEQ\" /></LoopMDFieldName><LoopDetailFieldName><Field Name=\"CLIENTID\" /><Field Name=\"ACCOUNTSEQ\" /><Field Name=\"TRUSTED_CLIENTID\" /><Field Name=\"TRUSTED_ACCOUNTSEQ\" /><Field Name=\"STATE\" /></LoopDetailFieldName><LoopRowValue><RowValue v0=\"C797979\" PENDACTION=\"U\" v1=\"1\" v2=\"C000002\" v3=\"1\" v4=\"P\" /></LoopRowValue><LoopDetail /><ObjectName>SCACCOUNTINTERFACE</ObjectName><MasterObjectName>SCACCOUNT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /><Field DetailFieldName=\"ACCOUNTSEQ\" MasterFieldName=\"ACCOUNTSEQ\" /></LoopMDFieldName><LoopDetailFieldName><Field Name=\"CLIENTID\" /><Field Name=\"ACCOUNTSEQ\" /><Field Name=\"INTERFACESEQ\" /><Field Name=\"CURRENCYID\" /><Field Name=\"BANKID\" /><Field Name=\"BANKNAME\" /><Field Name=\"BANKACID\" /><Field Name=\"OWNERNAME\" /><Field Name=\"IDNUMBER\" /><Field Name=\"PLACEISSUE\" /><Field Name=\"ISSUEDATE\" /><Field Name=\"EFFECTIVEDATE\" /><Field Name=\"EXPIRYDATE\" /><Field Name=\"ISDEFAULT\" /><Field Name=\"WAIVEFLAG\" /><Field Name=\"AUTODEBITEXTERNALFLAG\" /><Field Name=\"AUTOCREDITEXTERNALFLAG\" /><Field Name=\"TRANSFERFLAG\" /><Field Name=\"REGISTEREDFLAG\" /><Field Name=\"STATE\" /></LoopDetailFieldName><LoopRowValue><RowValue v0=\"C797979\" PENDACTION=\"U\" v1=\"1\" v2=\"1\" v3=\"VND\" v4=\"OCB\" v5=\"OCB1\" v6=\"111111111\" v7=\"TTL Test\" v8=\"123456789\" v9=\"HCM\" v10=\"2010-10-05 00:00:00.000000\" v11=\"2010-10-06\" v12=\"2010-10-22\" v13=\"N\" v14=\"N\" v15=\"N\" v16=\"N\" v17=\"N\" v18=\"\" v19=\"P\" /></LoopRowValue><LoopDetail /></LoopDetail><ObjectName>SCCLIENTADDRESS</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopDetailFieldName><Field Name=\"CLIENTID\" /><Field Name=\"ADDRESSTYPEID\" /><Field Name=\"ADDRESS1\" /><Field Name=\"ADDRESS2\" /><Field Name=\"ADDRESS3\" /><Field Name=\"ADDRESS4\" /><Field Name=\"ADDRESS5\" /><Field Name=\"COUNTRY\" /><Field Name=\"COUNTRYTYPE\" /><Field Name=\"STATE\" /></LoopDetailFieldName><LoopRowValue><RowValue v0=\"C797979\" PENDACTION=\"U\" v1=\"8\" v2=\"Test address\" v3=\"111\" v4=\"111\" v5=\"HCM\" v6=\"084\" v7=\"VN\" v8=\"L\" v9=\"P\" /></LoopRowValue><LoopDetail /><ObjectName>SCCLIENTCONTACT</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopDetailFieldName><Field Name=\"CLIENTID\" /><Field Name=\"INTERNALKEY\" /><Field Name=\"CONTACTTYPEID\" /><Field Name=\"NUMBER\" /><Field Name=\"TO\" /><Field Name=\"CONTACTPERSON\" /><Field Name=\"POSITION\" /><Field Name=\"STATE\" /></LoopDetailFieldName><LoopRowValue><RowValue v0=\"C797979\" PENDACTION=\"X\" v1=\"0\" v2=\"S\" v3=\"1234567\" v4=\"\" v5=\"\" v6=\"\" v7=\"P\" /></LoopRowValue><LoopDetail /><ObjectName>SCCLIENTJOINTACC</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopDetailFieldName><Field Name=\"CLIENTID\" /><Field Name=\"INTERNALKEY\" /><Field Name=\"TITLE\" /><Field Name=\"JOINTNAME\" /><Field Name=\"CNAME\" /><Field Name=\"BIRTHDAY\" /><Field Name=\"IDTYPE\" /><Field Name=\"IDNUMBER\" /><Field Name=\"ISSUEDATE\" /><Field Name=\"EXPIRYDATE\" /><Field Name=\"PLACEOFISSUE\" /><Field Name=\"CITYOFISSUE\" /><Field Name=\"SEX\" /><Field Name=\"OFFICETEL\" /><Field Name=\"MOBILENO\" /><Field Name=\"HOMETEL\" /><Field Name=\"PAGERNO\" /><Field Name=\"OCCUPATIONID\" /><Field Name=\"STATE\" /></LoopDetailFieldName><LoopRowValue><RowValue v0=\"C797979\" PENDACTION=\"X\" v1=\"1\" v2=\"1\" v3=\"TTL Test\" v4=\"TTL En\" v5=\"1995-05-08\" v6=\"1\" v7=\"123456789\" v8=\"\" v9=\"\" v10=\"VN\" v11=\"\" v12=\"M\" v13=\"\" v14=\"\" v15=\"\" v16=\"\" v17=\"4\" v18=\"P\" /></LoopRowValue><LoopDetail /><ObjectName>SCCLIENTAGENT</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopDetailFieldName><Field Name=\"CLIENTID\" /><Field Name=\"TITLE\" /><Field Name=\"AGENTNAME\" /><Field Name=\"CNAME\" /><Field Name=\"CLIENTAGENTSTATUS\" /><Field Name=\"PASSWORD\" /><Field Name=\"STARTDATE\" /><Field Name=\"IDTYPE\" /><Field Name=\"PERSONALID\" /><Field Name=\"PLACEOFISSUE\" /><Field Name=\"CITYOFISSUE\" /><Field Name=\"ISSUEDATE\" /><Field Name=\"EXPIRYDATE\" /><Field Name=\"ATTORNEY\" /><Field Name=\"TEL\" /><Field Name=\"MOBILE\" /><Field Name=\"ADDRESS1\" /><Field Name=\"ADDRESS2\" /><Field Name=\"ADDRESS3\" /><Field Name=\"ADDRESS4\" /><Field Name=\"COUNTRY\" /><Field Name=\"STATE\" /></LoopDetailFieldName><LoopRowValue><RowValue v0=\"C797979\" PENDACTION=\"X\" v1=\"1\" v2=\"AAAAAAAAA\" v3=\"AAA En\" v4=\"N\" v5=\"111111\" v6=\"2010-09-20\" v7=\"1\" v8=\"123456789\" v9=\"VN\" v10=\"HO\" v11=\"2010-10-04\" v12=\"2010-10-30\" v13=\"F\" v14=\"123456789\" v15=\"1234567\" v16=\"qwer\" v17=\"34234\" v18=\"234234\" v19=\"234234234\" v20=\"VN\" v21=\"P\" /></LoopRowValue><LoopDetail><ObjectName>SCCLIENTAGENTPRODUCT</ObjectName><MasterObjectName>SCCLIENTAGENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /><Field DetailFieldName=\"AGENTNAME\" MasterFieldName=\"AGENTNAME\" /></LoopMDFieldName><LoopDetailFieldName><Field Name=\"CLIENTID\" /><Field Name=\"AGENTNAME\" /><Field Name=\"PRODUCTID\" /><Field Name=\"STATE\" /></LoopDetailFieldName><LoopRowValue><RowValue v0=\"C797979\" PENDACTION=\"X\" v1=\"AAAAAAAAAB\" v2=\"HKS\" v3=\"P\" /></LoopRowValue><LoopDetail /></LoopDetail><ObjectName>SCCLIENTNOTIFICATION</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopDetailFieldName><Field Name=\"CLIENTID\" /><Field Name=\"CONTACTINTERNALKEY\" /><Field Name=\"LANGUAGEID\" /><Field Name=\"GROUP\" /><Field Name=\"FEE\" /><Field Name=\"TYPE\" /><Field Name=\"NOTIFICATIONTYPE\" /><Field Name=\"NUMBER\" /><Field Name=\"STATE\" /></LoopDetailFieldName><LoopRowValue><RowValue v0=\"C797979\" PENDACTION=\"X\" v1=\"0\" v2=\"V\" v3=\"1\" v4=\"12\" v5=\"X\" v6=\"S\" v7=\"1234567\" v8=\"P\" /></LoopRowValue><LoopDetail /><ObjectName>SCCLIENTBENEFICIARY</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopDetailFieldName><Field Name=\"CLIENTID\" /><Field Name=\"INTERNALKEY\" /><Field Name=\"TITLE\" /><Field Name=\"BENEFICIARYNAME\" /><Field Name=\"CNAME\" /><Field Name=\"IDTYPE\" /><Field Name=\"PLACEOFISSUE\" /><Field Name=\"CITYOFISSUE\" /><Field Name=\"IDNUMBER\" /><Field Name=\"ISSUEDATE\" /><Field Name=\"EXPIRYDATE\" /><Field Name=\"OFFICETEL\" /><Field Name=\"MOBILENO\" /><Field Name=\"TEL\" /><Field Name=\"PAGERNO\" /><Field Name=\"ADDRESS1\" /><Field Name=\"ADDRESS2\" /><Field Name=\"ADDRESS3\" /><Field Name=\"ADDRESS4\" /><Field Name=\"COUNTRY\" /><Field Name=\"STATE\" /></LoopDetailFieldName><LoopRowValue><RowValue v0=\"C797979\" PENDACTION=\"X\" v1=\"1\" v2=\"1\" v3=\"sadf\" v4=\"dsadfsdf\" v5=\"1\" v6=\"VN\" v7=\"HO\" v8=\"asdasd\" v9=\"2010-10-05\" v10=\"2010-10-25\" v11=\"123123\" v12=\"123123\" v13=\"1231231\" v14=\"123123\" v15=\"123\" v16=\"123\" v17=\"12312\" v18=\"3123\" v19=\"VN\" v20=\"P\" /></LoopRowValue><LoopDetail /><ObjectName>SCCLIENTSIGNATURE</ObjectName><MasterObjectName>SCCLIENT</MasterObjectName><LoopMDFieldName><Field DetailFieldName=\"CLIENTID\" MasterFieldName=\"CLIENTID\" /></LoopMDFieldName><LoopDetailFieldName><Field Name=\"CLIENTID\" /><Field Name=\"FILENAME\" /><Field Name=\"DESCRIPTION\" /><Field Name=\"ISAGENT\" /><Field Name=\"FILEVALUE\" /><Field Name=\"STATE\" /></LoopDetailFieldName><LoopRowValue><RowValue v0=\"C797979\" PENDACTION=\"X\" v1=\"logo.gif\" v2=\"asdfsadasd\" v3=\"Y\" v4=\"R0lGODlh1gBHAPcAAAAAAP////7+/uoBAuoHCusJDuoMEewSF+0ZHOsdIeweIu0mKvIqLfmjpPrLzOsABusBCuwCCewDDOoDC+0FC+0GD+sGDu0JEeoJEesKFOwMEuwMFesPFe0RGeoRGe0THesTG/AUHu4UGusUHO0WHu0XH+8YIO4YIusZIusZH/EbJOwaI+4bIe4bIu4bJO0bJOwbIeobIu4cJe0cIe0cIuscIuscJO4dI+wdJe0eJuseI+seJe8fJ+8fJe0fJOwfJ+4hKuwhJu0jLOsiKvAkLO8nMO4nLesoL+4rM+8uNu8zOe82Pe86QfA+RO9DSPFFTPBJUPFLUvJRVvNVW+9UWPBYXvFhZfNlavR1efV+gfSChvWFivWJjfaNkPWVmPianfaZnPeeoviipvenqfisrvq5vPrCxP3X2Pzb3P3h4v3m5+0FEe0KFvALF+0OGewTHu4YJO4aJukaJO8cJ+0bJusbKOwdJ+sdJ+0eKeweKPJdZPRsc/iRl/WQlfqmq/iorPmus/mwtfq+wfvGyfvO0fzT1fze4PeUm/zv8P3i5f719v309f3m6f/2+f/8/f/9/v/+//z//+3u7vv//v3//P7++////fv7+v769/78+/7w7fvm4/rJxfri4P3p5/708/3k4/vm5eYCAPrZ2f7s7P7x8f75+f39/fj4+Pf39/T09PHx8e7u7u3t7erq6ujo6Obm5uXl5ePj4+Dg4N/f393d3dnZ2dbW1tTU1NHR0c7OzszMzMvLy8jIyMbGxsHBwb29vbu7u7i4uLGxsa2traqqqqioqKSkpJ+fn5ubm5mZmZWVlZKSko6OjouLi4iIiIaGhoODg4CAgH19fXp6end3d3R0dHJycnFxcW5ubmpqamZmZmVlZWJiYl5eXlpaWlhYWFVVVVFRUU5OTkpKSkdHR0RERD8/Pzo6OjY2NjMzMzAwMC8vLywsLCsrKykpKSYmJiIiIh4eHhoaGhYWFhISEhEREQ4ODgwMDAoKCgkJCQcHBwUFBQMDAwEBAf///yH5BAEAAP8ALAAAAADWAEcAAAj/AAMIHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePIEOKHEmypMmTKFOqXMmypcuXMGPKnEmzps2bOGdCCgCpkgABOYMKnQhUoCVIloxC2unzZyVHmUz55GmUKlWkRYdqvbmTINJIPQUKKKUG1KY0htKUkgp0pyUBSQMcXVppq92YO7PyjBTgEaRIjgwNasBFy5YtXPr8cZBGUaW3OyMndXu3sksBkE4hnZTUkqVSZbJMUZJkCZMmTZyohqJnCxk1PNtCohRAr+XbKR/xpeo5zRUhMe7w+CFEiJEjR4oYMVJkAZAqYzQFqGvJEU9Lj3BrL9m1K1DdhIpk/5hzww4NHTJk/OCho0eeHD9svCByxQFVAXzjbt8fUvZ0sY5gIYEJONyQ3gsuvCCfDDe4kOAcNcjABCB1FXUUfxiCRIlng5CC1RlIENADDjXIV0MNL8jwAg044NDCCz+4cEcRYdS1FFgqqYLKjjumgkpEqPjIoyqXCBAkj0gmqeSRp/xU20FAXbKkKlSqIuSORdpGUFECnOKll5d8KWaTtjlZ0I2WmKHEFR561oABGwihwAtz0KHADTa4QIMLCrgwBw42GHiEGJOplBQ12VijqDXYUGOLQ6cAk801i2KDTS6uPJPoopx22uk12WhDzTPEyHLKQFnp0gyolC5ajTbddP/jjTaLJvqMMK8cdMorvyDzjDTUTFONp4pWQw010SjDiypQCvRYAGdAUcIEUhTCFyZaFMBBDjgooMAKLtxQAx10RGjDDDekeEMSZfiUnUrQ8APAvPP2Qw0qWhrkSjn0AuAPAPHYcos8/f7b78Hz+mPwPvjgU885z+Ra1CnFuHPPPgcbjPC8+9xDDznDFNRKM+fMU889+aS8z8oss5xyw/bEo80tCFEyiQCK7FEDHiY8sIAYlwTgCBccTPCBDTjgcUcLNeygQA8yxCFDDVK/CIUh09WVkiviIOzOowsJMIw+CifsjzOX6OJOxhuznTE+2bQykC/xsK2xv23P+w4vA7X/kg3G9N59d+D98tON3HoBVZcYPtx5BxAHYGAFITsF0sQGE2jQQQklmPDG5jrQ8AIdNsiQAHlYZGKoQMcAjvc+zuRL0CrhDA6PLgHswg7hedPbD94I33OMQKtkk/fgvPs+DbOoNKMPwshvrHE9xmi5VBpPiA6DCFCU0cACCHBhCCSliDEFEim4wYYHJtjBwQE53ICnHXjMmYQgcq1UyzkJ03sOLQsBBj0yNo1U5G530etd8v41DlkEoBbr6J/Z/qUwf/SjgoP7FzoelQt1ZCyB0qMXN35kEEuEQQFziMMcUgAELSDCEFIQBQOyMIjqjKIMY2hAGMYgiC0gAQQqwIEK/1pgggNs4ANYWERLmIExjeEjGQkRQCq0Mbh5/CIAp9DF7vCmsH7wox9gDKMYwWi3eQQjALkY4MHo8Y1nPMMZ0LCGNrghq3OosV/uwB0xXKcxC47xgr+TIL3KkaszqWEKN0iAC3QwhwyIIgl/IEUDllAAGERBC2RwABo4sQUpgKERWrAACN7AAQt4AAlOQAISCJGSrNgigsADADlikZBftKNg/tiGKoCiC1jirR7Z+JU0hknMYUZjGt5ARz5iCYB9hOwX+LjbOojhCitRaRWtaIUrXhGLZNzjYPXQhQCUsbF8pEMc3/DGN9b5DXGUIx30kNfB1JGLgwwCCTIQHQpIMP+FQyhhAErIAhiy4AQ3FCAEMaBPF5iAACko4QEVOEAS9hAINSjCAU34wrtUcopoyDNh+CCGQYCCims8r1/zEMZAtHiwPKaiSjCFKStk4Qx8EM4fxxDbSek1DS3ppRbggIc7hgqPdNwiFdXoIwDs0QxauCKbUNUmLGQxjHfwzh29MMgp+ICHFO2ABB3oQiZI0YcoKGEJUogCHkhAAjdkYApmIIUD+KCFLoRBEKF4xCcEAQY1cOEKimjWRyABFKDgAh5lM9g3XFFYguTiloHzhzdWsVJ23M0du3CILM5BQXo14xTCWGb/zPELWLQiFWESi1gEQItc3AIXuuCFLlDRim3/BLJe35DbQsIBvH/J44oF+YQVREeDG6zgBEKYQhjMIIgGgCEMUegADXYwhDgcIAZVaIAZzpCITaDBDGCQAggecAUrREENXVEJUm/7L3tcUS+nWMYF+3WPkImll4TDKkOAwopr3NZf0rjELr7Z23eAoxvWONYzmgGNBStDGcUIBqay8opvdBYA/VDGqRayDHKM48PjKMc2aKbaAKjhCXeKAw7ysIMKDGAAcohCFrxghSCkwEEw6AEK3GCBDPwgCahJwhwMIAE3nCAHQGjCIGRXkl/MQ5DaYBZBaIEOhI3DFaplaf/0q5CioIIZ/+1HgG/xZLcFjh/6wMc96mEPe9AD/x7mwMYvgDKLcpRtXvgIBpNrIwBW1IIWswh0LWJBQoIUYglM+4ECPlCBFlDBC2YohBrKcAALDEEGeOhqDnyQgxWMYAMXqEAGOJCCOaSHBD9gAhhMgRIuBWAVVLzbPLI6kFMc46P/4kf1CIJfPNKaIahQxn/9IQ1UvIIcC8xgwXwHj5DNwhx3q4cvwvakkaJKIINQQg18cAc2HEAPZgAFKBzQgD6IpgkkaEEHDICBCligAJo7wQ+C8IM8nGcGOEAyE1x4kuy4WoAGo6A18CUQV5iDd+igJa+3SC+s7rnWyLjzvKSRCls/T6kKXDY8bBELzgauHu9tSFZGjio13SAGF/9QAh8E0YcqLMAACtjDIDxBhQFgwAVLiMIUqACFJeSBxxwwQQ7yOQcTxMAITNgCIh6+kd0QhBXdQNjtBEKxj86LH8lwtQC03PBfL+QSyQgkBSkegFZYox6CXHb/Ak6vfTAjFuUQ+1KBS/KCuPraTwKKmuLAhieAAQsLoMAALoCFQTggvAh4ghbKkAhTYCITivCEGQ5xhSRwYA1vmMMJeDAEITjBC0o0iX4IQgwC00sf0TgVK5B9sHXYwjZaNph+mY6KZQyugAJhhTCoUQ51wGMe86CHPfRBfH4Y34tWrxc3aAGOg90j5AppxS54sQtd7MIXtyhSQQixBAwwoQtNWEP/5JQwhjLsgQMEgEIgPhEJyGsCDYQoBCMUoRs0fAEKB3jABKDwBBRAgQys5hJcg3C1EADCgHbA0w/PEDQFwXXzwmUMsQrTMDhkVxSX8AqzkAvC8Au+AAzD8IHJ0AwMZg3eUDexBA6yoA0Hsw/F0FgIIQDFkA7ssA40+A7hADYEkQZKAAR8EAUQsAMewAR+kAUpAFDr5wmD4AV7MAVNoATIgVZY8AdokAmgoQd6UAZRAAJUYAaqYxJOtyXHYFP9sg/GcAoqGEuYdRAOCAAQ2GXHdjD9wAynYAvFQAzEIAx4iGUGISVBgk3IUGb0Mg6vQA2x1A/TQEI+JRDSIIYGAw9e/1cbiOAEWRAGRwQCQNAFeiABAyAFhkAIWKBtoIYBF9AGGbABBYABGXBK0MEIjZAJgpACKKAHhuAIZjIS6VUQsXBwB9MNwZAOCIMNBNeAW0RB7IALDJEKsYAMakRB/WAMAQAN+UB80jg8JVaNAXALsGQw49AKzyBI7BAMhaYlskAOw9YOvXB3psAFhbAHD5ACHaAHVrABD/AEaKAFIGABG9ABI8ACMsAt3MIDdAAHH1BKbbAEWgAGTeAGQLAHiAATXZIMF0cv9RAPOzUv9EB3wngw8zANybAMHvmRILkM0yAOCNg/+BAy0cBeAOANsOAlXUImqrUKzCCGeDMOq3AMGf/UDtOwDMmADD6ZDD2JDMoADhU5L/RUbQMBCqTgBKKEBFhgBGvwAmXQBwOwAUHELXRiAzVQXC+gAujyAjhAAhpgARjgBi8wBGIlE1QGPQjjDVJmEGuIYV/0R2GUdgnzWwLADPxwN/gwDtPwl9EQDc3ADMpADMWwDNtwR1dnk8dgeoKDD212D/cQmf/VL+UAC3X3CGrQBBbwBlKABSBAAVXgAElwAXhQIDfQAniSIiryAjUgP2C5AzKwAjBgAzRQBGUgEOlViyfRJWCmdv1iD2eEEHGZcYLTL+QACwFADPLyL2LHMC8jmW4mfBfGMdmgCsAAWXbJlmrnD91gQNWGGZ//8QQQ8AZYkAUZ8ABZ4AA/wAFdpSA44CAyYAN0sAOJRJ93IAMqcAc9sAMtYAdVQAptgR8kcYslJgC2wHDJAwDhwAoJEZcgpED6gAynggv00FnVyUwLCgC6JgCqQIhsqWxtgw/I4IIC8QiWgAhScAEj8AVckAEQkAWb0AQVcAeBsgIMQgMo4popApYusAI4IAMx8AIKMARkQBuXUW2pIA1sdzf7QAxMVpwZtzH8EDfEYw2VaZwIcw4OhEbpcGHHyZ3+0g+HcxBHIQBbIAIiQAZhcAERcAWQ0AUDkALGNZ+y6QLzmSI4iiBTUwNz4AI1oAekMHqYURsGuhFakxC9YFUU/9RF81IOCqd1AsEL8OAvGHSpGGQ2msoP7QANylkUsbANjKiplopLpXoP4cALXNIL3/BNFlQwgHRnFYRh8CANXTpSSdEJTwACgFAGIAABUEAWTRABp5kAKmADpWMDLaAiCYAgtvkCLEAHOHA/K3GoetE88NAONEiD7OAO71AMG2YmWXEL4/AO23qu6Hqu6HAO5EAO4XANzUAMuVBofQMMzIAN39Cu55AO6KAOM7gO/IoO5dCu3mANyhAMCocqryAMzKAN44AO7eAO7RAP8lCx8vAO7ZCx6TAO1nAMs8UQllAGRBAGhWAED1AEhPAIZMADGMADKUQnMHADdKAiDAIDc/9wBwpiAzwABpDwCIlqEkhqbbnnWrdQtEWLC7iQColYG6hQC7hgtFAbtVBrC382C7HACuCZEJfACrAwC7RQC7YgMEYbtrYAaLPwCqxAr7yZCq4gC06LtNfXC77QC7mAtLhgC7PACndXQt7RBX3wCVHwABzwBY7wBUCAAAaQA3OgA3dSIjSgn/NJA3diAyqABZ9QWIeqEiYaReHZuXz2EJKad1uiWk5SJng3uhGRL3trpgNhCY3gAAKgBRggAVQAGiIQBByQAUEwB135AjeAAzqgSHigrKa2B4yQFLQxeiSxtFAiO43FdNR2uqobvUhJcrUIvRMRFtMBCQ6ABBJAAu3/QgZOcAUdUAB4UCcvoANxQANzUAelQwdBgAUeEgnZkbmam7rLqxGri7ovGLpIeRD12xl5sQWYUwXsRwqkEAY1IAEp4AO+G6Q14AI4wAJLAAaBpRTYuxHWerrWeHcZHL2ma3f4K8JCS8IfrJt8ARcBMAkCoQlckARs4AcBQAkCMAmCwAQRAAJTk5p2MAdHgAUOIMBi4Rl3sb8eccIlXHcidxFxUSaFgAVVMAqdEQBoYAUX8AFzYgRNsAeCEHrXsRT2KxJNzMEvSMJmfBHWW8LVmMZGfKBlfG1IbKgBgAlpUQnvQj5f4ARSkAV/MAoXvBWSmjj/CxEh7Ly8qcbWOMiJ/yy6/EvGiyxYtrEUc1wGo5AJcUHEYTwTgTy6cTzIbcxne8a8j5yIIdzIjgzJA7ETkYCkkGEQmdxqe+uCz8vJ1WvKqJI4pWvCzHvInrzG4krLsrxaiKzInlttz/IIQOEZr7wSv/zISczBzivC08u5zsy/aazIm9vGokzGkbEU2YEjcvEWNdHMxPzMHfzGcCxY6Mx0ShzM0oy602zLgwzGlEC/U4zJS6G8LdHOZ3zO0tvBptuN4aB1SkwQ6hAPTBbL6VzM2fy5JnrImxtF4vwIdywQkhAP5lAZ22zLgowQGe3Bp1wL9pBZb5zB7Ey68sx0yFsVmCwQuwAABWgX2wDT8v+8yB1tEDNdCyD9yMUAAMpAzQ+3DfZQgCfNyKM8EKxgD+EwEd1YDBvx0s9QEXoRDB7EoA4aDABgDku9X2LhRkscDPag1QuR1dcWD+FgD4gDzQRxcHhnD05dzQXRjTotEGRdzsUQD7qFELVwMGeU1NsQDxDRjT8dADNNTmdkDuYw1AEQDFsNugOx19uQO/ZQDacsWHsd1VxdzgUBAOqg1gWtFwfnoMSMvXJdFJwN16X8QACA2fEA2BTBCmSd1BmNEBhNyGbSjQ5qJtVglLuA1VX90+TE2TQtALrYjVG92zHdCsUdALzF2SRN1yYY1f2yDQLQjfMSDL49L4NddsVN3PT/og7KDdjkdHDd6EEjTdhL1doDIdfhEA97TS+RXQtVTdnW7dPZ7dOPvdoCEQ+dbd328NPCDQCRvQv2INzbzVusQE7FYNhgDTC7YN2zvcRFMdMlRk7PwAqtjdVLHdawvdQeRNSXHQDmANhhPRDIbdwAUAwY7trQzQq7zQsAwwq8hdRKreEBUOICceIAwAsH9+AAEAwjHgDkpAz2YNxQ3dPPINIsbtzuHQAxPuMj3go9vd1hbeM4rtrfvQsvTdkI/uQAEAAH7eL4LRBInthCnuJKDeYZXduOXRS4XW05jd5Y/dMYHeJyXRshbuEpPhBBLhAUjt4x7eSRPedkndPKMNNZ/z3nAcDmIs7i3n3myhDkwe1GMH3Zht3o671U9uCghQ7TAg4UAn7o82IOis7ol/3S6pDgP37mWB3ZFF7bip57MQMAlD3kB7PoES7hAgHZAcAL9rANFr7ipW4OHQ7mwx3iSb1UWbHbu6DnKq7eAxHjL97pL03sVY7fjM7sCs7WQ57YYn5Gcn3ZSK7kmQ7Wkd3ptRDlU57Vsj3s+R3V3VgNW87cAADbrv7lYb7b243eAJBZCp7mA8HoIGsQwWCCxB4Au+3c7h7cHz4QHkTZu03ZSK2Lkd3c6vDcTl7V0p3ROX1w9nDQ7p57FC/iXz7kVC3c4F7p+j3TMbPkMG3Y6C7f88FC2YgO8tge4SFu7D6+VGck4Ogt2QZOEFg924ZN5PNy3KfdEPo8ESU+06JdEDON8Q/h8zAB1Yve2TZx1qr+1lUB14QsEJGw9A4R3PptEHUT2RNB9TCB6MJ5E7tg8AqhHzkfET0ryRly99lr961r9yy8zEusm3gf+BqxIWI/EfTb9YKf+H9fEJbwLBnhGYWv+JLvFXnhGQLgF52cEMqMEJDf+Z7/+aAP+hAR+qHP+aL/EKR/+iWk+h3h+WBfG0TMEQEBADs=\" v5=\"P\" /></LoopRowValue><LoopDetail /></LoopDetail></APPROVALRPL></MessageContent></LoopMessage></LINKFUNCS>";
		
		 Hashtable lvParamMap = new Hashtable();
		 /*lvParamMap.put("CLIENTID", this.getMvClientID());
		 
		 lvParamMap.put("BANKID", this.getMvBankId());
		 lvParamMap.put("DESTBANKID", this.getMvDestBankID());
		 lvParamMap.put("DESTCLIENTID", this.getMvDestClientID());*/
		 
		 // set transfer type and personal in charged
		 String personCharged = "N";
		 if(mvTransferType.equals("1")){
			 mvTransferType = "E";
			 // set personal in charged
			 if(mvPersonCharged.equals("2")){
				 personCharged = "Y";
			 }			 
			 
			 if(mvRemark != null && mvRemark.trim().length() > 0){
				 lvParamMap.put("STRINGQUERY", lvUpdateApproval);
			 } else {
				 lvParamMap.put("STRINGQUERY", lvUpdate);
			 }
			 
			 
		 } else if(mvTransferType.equals("2")){
			 mvTransferType = "I";
			 
			 lvParamMap.put("STRINGQUERY", lvInsert);
		 }
		 
		 /*lvParamMap.put("TRANSFERTYPE", this.getMvTransferType());
		 lvParamMap.put(TagName.WAIVEALLFLAG, personCharged);
		 
		 // set remark is blank when no input remark
		 if(this.getMvRemark() == null || this.getMvRemark().trim().length() == 0){
			 mvRemark = "";
		 }
		 lvParamMap.put("REMARK", this.getMvRemark().trim());
		 	
		 lvParamMap.put("AMOUNT", this.getMvAmount());
		 if(this.getMvPassword() != null && this.getMvPassword().length() > 0){
			 lvParamMap.put("PASSWORD", this.getMvPassword());
			 lvParamMap.put(TagName.PASSWORDVERIFICATION, "Y");
		 }else{
			 lvParamMap.put(TagName.PASSWORDVERIFICATION, "N");
		 }
	     if(this.getMvIsSecurityCodeConfirm() != null && this.getMvIsSecurityCodeConfirm().equalsIgnoreCase("true")){
	    	 lvParamMap.put("SCODEVERIFICATION", "Y");
	    	 //lvParamMap.put("SECURITYCODE", this.getMvSecurityCode());
	    	 lvParamMap.put("SECURITYCODE", "");
	    	 lvParamMap.put(TagName.PASSWORDVERIFICATION, "N");
	     }else{
	    	 lvParamMap.put("SCODEVERIFICATION", "N");
	     }*/
	     
	     
	     
	     
	     Log.println("[ HKSFundTransferTxn.process() finished constructing lvParamMap [" + this.getMvClientID() + "]", Log.ACCESS_LOG);
	     
	     if (TPErrorHandling.TP_NORMAL == super.process("TestAccountMaintenance", lvParamMap)){
	    	 mvResult = mvReturnNode.getChildNode(TagName.RESULT).getValue();
	    	 mvReturnCode = mvReturnNode.getChildNode("RETURNCODE").getValue();
	    	 mvReturnMsg = mvReturnNode.getChildNode(TagName.RETURNMESSAGE).getValue();
	     }else{
             if (mvReturnNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase("HKSEF0034")) {
            	 setErrorCode(new ErrorCode(new String[0], "HKSEF0037", ""));
             }
             if (mvReturnNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase("TPBASEREQERROR")) {
            	 setErrorCode(new ErrorCode(new String[0], "0100", "No connection with TP"));
             }
             //TODO: To do other logic for failue case.
	     }
	     Log.println("[ HKSFundTransferTxn.process() ends [" + this.getMvClientID() + "]", Log.ACCESS_LOG);
	}
	
	/**
	 * Getting mvClientID
	 * @return mvClientID
	 */
	public String getMvClientID() {
		return mvClientID;
	}

	/**
	 * Setting mvClientID
	 * @param mvClientID
	 */
	public void setMvClientID(String mvClientID) {
		this.mvClientID = mvClientID;
	}
	
	/**
	 * Getting mvFundTransferFrom
	 * @return mvFundTransferFrom
	 */
	public String getMvFundTransferFrom() {
		return mvFundTransferFrom;
	}
	/**
	 * Setting mvFundTransferFrom
	 * @param mvFundTransferFrom
	 */
	public void setMvFundTransferFrom(String mvFundTransferFrom) {
		this.mvFundTransferFrom = mvFundTransferFrom;
	}
	/**
	 * Getting mvTargetAccount
	 * @return mvTargetAccount
	 */
	public String getMvTargetAccount() {
		return mvTargetAccount;
	}
	/**
	 * Setting mvTargetAccount
	 * @param mvTargetAccount
	 */
	public void setMvTargetAccount(String mvTargetAccount) {
		this.mvTargetAccount = mvTargetAccount;
	}
	/**
	 * Getting mvAccount
	 * @return
	 */
	public String getMvAccount() {
		return mvAccount;
	}
	/**
	 * Setting mvAccount
	 * @param mvAccount
	 */
	public void setMvAccount(String mvAccount) {
		this.mvAccount = mvAccount;
	}
	/**
	 * Getting mvAmount
	 * @return
	 */
	public String getMvAmount() {
		return mvAmount;
	}
	/**
	 * Setting mvAmount
	 * @param mvAmount
	 */
	public void setMvAmount(String mvAmount) {
		this.mvAmount = mvAmount;
	}
	/**
	 * Getting mvIsPasswordConfirm
	 * @return
	 */
	public String getMvIsPasswordConfirm() {
		return mvIsPasswordConfirm;
	}
	/**
	 * Setting mvIsPasswordConfirm
	 * @param mvIsPasswordConfirm
	 */
	public void setMvIsPasswordConfirm(String mvIsPasswordConfirm) {
		this.mvIsPasswordConfirm = mvIsPasswordConfirm;
	}
	/**
	 * Getting mvIsSecurityCodeConfirm
	 * @return
	 */
	public String getMvIsSecurityCodeConfirm() {
		return mvIsSecurityCodeConfirm;
	}
	/**
	 * Setting mvIsSecurityCodeConfirm
	 * @param mvIsSecurityCodeConfirm
	 */
	public void setMvIsSecurityCodeConfirm(String mvIsSecurityCodeConfirm) {
		this.mvIsSecurityCodeConfirm = mvIsSecurityCodeConfirm;
	}
	/**
	 * Getting mvPassword
	 * @return
	 */
	public String getMvPassword() {
		return mvPassword;
	}
	/**
	 * Setting mvPassword
	 * @param mvPassword
	 */
	public void setMvPassword(String mvPassword) {
		this.mvPassword = mvPassword;
	}
	/**
	 * Getting mvSecurityCode
	 * @return
	 */
	public String getMvSecurityCode() {
		return mvSecurityCode;
	}
	/**
	 * Setting mvSecurityCode
	 * @param mvSecurityCode
	 */
	public void setMvSecurityCode(String mvSecurityCode) {
		this.mvSecurityCode = mvSecurityCode;
	}

	/**
	 * @param mvTransferType the mvTransferType to set
	 */
	public void setMvTransferType(String mvTransferType) {
		this.mvTransferType = mvTransferType;
	}

	/**
	 * @return the mvTransferType
	 */
	public String getMvTransferType() {
		return mvTransferType;
	}

	/**
	 * @param mvRemark the mvRemark to set
	 */
	public void setMvRemark(String mvRemark) {
		this.mvRemark = mvRemark;
	}

	/**
	 * @return the mvRemark
	 */
	public String getMvRemark() {
		return mvRemark;
	}

	/**
	 * @param mvPersonCharged the mvPersonCharged to set
	 */
	public void setMvPersonCharged(String mvPersonCharged) {
		this.mvPersonCharged = mvPersonCharged;
	}

	/**
	 * @return the mvPersonCharged
	 */
	public String getMvPersonCharged() {
		return mvPersonCharged;
	}
}
//END TASK #: - TTL-CN-XYL-00051 20100120 [iTrade R5] Fund Transfer for ORS
