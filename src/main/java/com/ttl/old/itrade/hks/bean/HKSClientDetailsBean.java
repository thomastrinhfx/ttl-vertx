package com.ttl.old.itrade.hks.bean;

/**
 * The HKSClientDetailsBean class define variables that to save values 
 * for action 
 * @author 
 *
 */
public class HKSClientDetailsBean {

	private String mvSettlementAccountDisplayName;
	private String mvSettlementAccountValue;
	private String mvBankID;
	private String mvBankACID;
	
	/**
     * This method returns the bank id of the client.
     * @return the bank id.
     */
	public String getMvBankID() {
		return mvBankID;
	}
	/**
     * This method sets the bank id of the client
     * @param pBankID The bank id of the client.
     */
	public void setMvBankID(String pBankID) {
		this.mvBankID = pBankID;
	}
	/**
     * This method returns the bank id of the client.
     * @return the bank id of the client.
     */
	public String getMvBankACID() {
		return mvBankACID;
	}
	/**
     * This method sets the bank account id of the client
     * @param pBankACID The bank account id of the client.
     */
	public void setMvBankACID(String pBankACID) {
		this.mvBankACID = pBankACID;
	}
	/**
     * This method returns the settlement account display name of the client.
     * @return the settlement account display name of the client.
     */
	public String getMvSettlementAccountDisplayName() {
		return mvSettlementAccountDisplayName;
	}
	/**
     * This method sets the settlement account display name of the client
     * @param pSettlementAccountDisplayName The settlement account display name of the client.
     */
	public void setMvSettlementAccountDisplayName(
			String pSettlementAccountDisplayName) {
		this.mvSettlementAccountDisplayName =pSettlementAccountDisplayName;
	}
	/**
     * This method returns the settlement account value of the client.
     * @return the settlement account value of the client.
     * @type String.
     */
	public String getMvSettlementAccountValue() {
		return mvSettlementAccountValue;
	}
	/**
     * This method sets the settlement account value of the client
     * @param pSettlementAccountValue The settlement account value of the client.
     */
	public void setMvSettlementAccountValue(String pSettlementAccountValue) {
		this.mvSettlementAccountValue = pSettlementAccountValue;
	}
}
