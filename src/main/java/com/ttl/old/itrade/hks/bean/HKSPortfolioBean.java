package com.ttl.old.itrade.hks.bean;

/**
 * The HKSPortfolioBean class define variables that to save values for action
 * 
 * @author
 * 
 */
public class HKSPortfolioBean {

	private String mvStockID;
	private String mvStockName;
	private String mvQueuingBuy;
	private String mvQueuingSell;
	private String mvTTodayConfirmBuy;
	private String mvTTodayConfirmSell;
	private String mvTManualHold;
	// HIEU LE: more value for MAS
	private String normalHold;
	private String conditionalHold;
	private String maintenancePercentage;
	private String maintenanceValue;
	// END HIEU LE
	private String mvTradableQty;
	private String mvMarketValue;
	private String mvMarginPercentage;
	private String mvMartginValue;
	private String mvBuySell;
	private String mvMarketID;

	// BEGIN TASK #:- TTL-GZ-XYL-00038 XuYuLong 20091119 ITrade .R5 VN Adding extra fields
	private String mvLedgerBal;
	private String mvAvgPrice;
	private String mvWAC;
	private String mvMarketPrice;
	private String mvPL;
	private String mvPLPercent;
	private String mvPdBuy;
	private String mvPdSell;
	private String mvHoldingAmt;

	// BEGIN TASK #:- TTL-VN VanTran 20102306 ITrade .R5 VN Adding extra fields
	private String mvProfitPrc;
	private String mvLossPrc;

	// BEGIN TASK #:- TTL-VN VanTran 20100930 ITrade VN Adding extra fields
	private String mvTEntitlementQty;
	private String mvTAwaitingTraceCert;
	private String mvTAwaitingDepositCert;
	private String mvTAwaitingWithdrawalCert;
	private String mvTMortgageQty;

	private String mvTTodayUnsettleBuy;
	private String mvTTodayUnsettleSell;
	private String mvTT1UnsettleBuy;
	private String mvTT2UnsettleBuy;
	private String mvTT3UnsettleBuy;
	private String mvTT1UnsettleSell;
	private String mvTT2UnsettleSell;
	private String mvTT3UnsettleSell;
	private String mvTDueBuy;
	private String mvTSettled;

	// BEGIN TASK #:- TTL-VN Giang Tran 20101118 ITrade VN Adding extra fields
	// VDSC: totalPendingBuy = T0ConfirmBuy + Pending Buy
	private String totalPendingBuy;
	private String mvTUnSettleSell;

	/**
	 * This method returns the ledger balance.
	 * 
	 * @return the ledger balance is formatted.
	 */
	public String getMvLedgerBal() {
		return mvLedgerBal;
	}

	/**
	 * This method sets the ledger balance.
	 * 
	 * @param pLedgerBal The ledger balance.
	 */
	public void setMvLedgerBal(String pLedgerBal) {
		mvLedgerBal = pLedgerBal;
	}

	/**
	 * This method returns the average price.
	 * 
	 * @return the average price is formatted.
	 */
	public String getMvAvgPrice() {
		return mvAvgPrice;
	}

	/**
	 * This method sets the average price.
	 * 
	 * @param pAvgPrice The average price.
	 */
	public void setMvAvgPrice(String pAvgPrice) {
		mvAvgPrice = pAvgPrice;
	}

	/**
	 * This method returns the WAC.
	 * 
	 * @return the WAC is formatted.
	 */
	public String getMvWAC() {
		return mvWAC;
	}

	/**
	 * This method sets the WAC.
	 * 
	 * @param pWAC The WAC.
	 */
	public void setMvWAC(String pWAC) {
		mvWAC = pWAC;
	}

	/**
	 * This method returns the market price.
	 * 
	 * @return the market price is formatted.
	 */
	public String getMvMarketPrice() {
		return mvMarketPrice;
	}

	/**
	 * This method sets the market price.
	 * 
	 * @param pMarketPrice The market price.
	 */
	public void setMvMarketPrice(String pMarketPrice) {
		mvMarketPrice = pMarketPrice;
	}

	/**
	 * This method returns the PL.
	 * 
	 * @return the PL is formatted.
	 */
	public String getMvPL() {
		return mvPL;
	}

	/**
	 * This method sets the PL.
	 * 
	 * @param pPL The PL.
	 */
	public void setMvPL(String pPL) {
		mvPL = pPL;
	}

	/**
	 * This method returns the PLPercent.
	 * 
	 * @return the PLPercent is formatted.
	 */
	public String getMvPLPercent() {
		return mvPLPercent;
	}

	/**
	 * This method sets the PLPercent.
	 * 
	 * @param pPLPercent The PLPercent.
	 */
	public void setMvPLPercent(String pPLPercent) {
		mvPLPercent = pPLPercent;
	}

	/**
	 * This method returns the PdBuy.
	 * 
	 * @return the PdBuy is formatted.
	 */
	public String getMvPdBuy() {
		return mvPdBuy;
	}

	/**
	 * This method sets the PdBuy.
	 * 
	 * @param pPdBuy The PdBuy.
	 */
	public void setMvPdBuy(String pPdBuy) {
		mvPdBuy = pPdBuy;
	}

	/**
	 * This method returns the PdSell.
	 * 
	 * @return the PdSell is formatted.
	 */
	public String getMvPdSell() {
		return mvPdSell;
	}

	/**
	 * This method sets the PdSell.
	 * 
	 * @param pPdSell The PdSell.
	 */
	public void setMvPdSell(String pPdSell) {
		mvPdSell = pPdSell;
	}

	/**
	 * This method returns the holding amount.
	 * 
	 * @return the holding amount is formatted.
	 */
	public String getMvHoldingAmt() {
		return mvHoldingAmt;
	}

	/**
	 * This method sets the holding amount.
	 * 
	 * @param pHoldingAmt The holding amount.
	 */
	public void setMvHoldingAmt(String pHoldingAmt) {
		mvHoldingAmt = pHoldingAmt;
	}

	// END TASK #:- TTL-GZ-XYL-00038 XuYuLong 20091119 ITrade .R5 VN Adding extra fields

	// Begin Task #:- TTL-CN-ZZW-0001 Wind Zhao 20090901
	private String mvCurrencyID;

	/**
	 * This method returns the currency ID.
	 * 
	 * @return the currency ID is formatted.
	 */
	public String getMvCurrencyID() {
		return mvCurrencyID;
	}

	/**
	 * This method sets the currency ID.
	 * 
	 * @param pCurrencyID The currency ID.
	 */
	public void setMvCurrencyID(String pCurrencyID) {
		mvCurrencyID = pCurrencyID;
	}

	// End Task #:- TTL-CN-ZZW-0001 Wind Zhao 20090901

	/**
	 * This method returns the market id.
	 * 
	 * @return the market id is formatted.
	 */
	public String getMvMarketID() {
		return mvMarketID;
	}

	/**
	 * This method sets the market id.
	 * 
	 * @param pMarketID The market id.
	 */
	public void setMvMarketID(String pMarketID) {
		mvMarketID = pMarketID;
	}

	/**
	 * This method returns the stock id.
	 * 
	 * @return the stock id is formatted.
	 */
	public String getMvStockID() {
		return mvStockID;
	}

	/**
	 * This method sets the stock ID.
	 * 
	 * @param pStockID The stock ID.
	 */
	public void setMvStockID(String pStockID) {
		mvStockID = pStockID;
	}

	/**
	 * This method returns the stock name.
	 * 
	 * @return the stock name is formatted.
	 */
	public String getMvStockName() {
		return mvStockName;
	}

	/**
	 * This method sets the stock name.
	 * 
	 * @param pStockName The stock name.
	 */
	public void setMvStockName(String pStockName) {
		mvStockName = pStockName;
	}

	/**
	 * This method returns the queuing buy.
	 * 
	 * @return the queuing buy is formatted.
	 */
	public String getMvQueuingBuy() {
		return mvQueuingBuy;
	}

	/**
	 * This method sets the queuing buy.
	 * 
	 * @param pQueuingBuy The queuing buy.
	 */
	public void setMvQueuingBuy(String pQueuingBuy) {
		mvQueuingBuy = pQueuingBuy;
	}

	/**
	 * This method returns the queuing sell.
	 * 
	 * @return the queuing sell is formatted.
	 */
	public String getMvQueuingSell() {
		return mvQueuingSell;
	}

	/**
	 * This method sets the queuing sell.
	 * 
	 * @param pQueuingSell The queuing sell.
	 */
	public void setMvQueuingSell(String pQueuingSell) {
		mvQueuingSell = pQueuingSell;
	}

	/**
	 * This method returns the today confirm buy.
	 * 
	 * @return the today confirm buy is formatted.
	 */
	public String getMvTTodayConfirmBuy() {
		return mvTTodayConfirmBuy;
	}

	/**
	 * This method sets the today confirm buy.
	 * 
	 * @param pTTodayConfirmBuy The today confirm buy.
	 */
	public void setMvTTodayConfirmBuy(String pTTodayConfirmBuy) {
		mvTTodayConfirmBuy = pTTodayConfirmBuy;
	}

	/**
	 * This method returns the today confirm sell.
	 * 
	 * @return the today confirm sell buy is formatted.
	 */
	public String getMvTTodayConfirmSell() {
		return mvTTodayConfirmSell;
	}

	/**
	 * This method sets the today confirm sell.
	 * 
	 * @param pTTodayConfirmSell The today confirm sell.
	 */
	public void setMvTTodayConfirmSell(String pTTodayConfirmSell) {
		mvTTodayConfirmSell = pTTodayConfirmSell;
	}

	/**
	 * This method returns the manual hold.
	 * 
	 * @return the manual is formatted.
	 */
	public String getMvTManualHold() {
		return mvTManualHold;
	}

	/**
	 * This method sets the manual hold sell.
	 * 
	 * @param pTManualHold The manual hold.
	 */
	public void setMvTManualHold(String pTManualHold) {
		mvTManualHold = pTManualHold;
	}

	/**
	 * This method returns the tradable quantity.
	 * 
	 * @return the tradable quantity is formatted.
	 */
	public String getMvTradableQty() {
		return mvTradableQty;
	}

	/**
	 * This method sets the tradable quantity.
	 * 
	 * @param pTradableQty The tradable quantity.
	 */
	public void setMvTradableQty(String pTradableQty) {
		mvTradableQty = pTradableQty;
	}

	/**
	 * This method returns the market value.
	 * 
	 * @return the market value is formatted.
	 */
	public String getMvMarketValue() {
		return mvMarketValue;
	}

	/**
	 * This method sets the market value.
	 * 
	 * @param pMarketValue The market value.
	 */
	public void setMvMarketValue(String pMarketValue) {
		mvMarketValue = pMarketValue;
	}

	/**
	 * This method returns the margin percentage.
	 * 
	 * @return the margin percentage is formatted.
	 */
	public String getMvMarginPercentage() {
		return mvMarginPercentage;
	}

	/**
	 * This method sets the margin percentage.
	 * 
	 * @param pMarginPercentage The margin percentage.
	 */
	public void setMvMarginPercentage(String pMarginPercentage) {
		mvMarginPercentage = pMarginPercentage;
	}

	/**
	 * This method returns the martgin value.
	 * 
	 * @return the martgin value is formatted.
	 */
	public String getMvMartginValue() {
		return mvMartginValue;
	}

	/**
	 * This method sets the martgin value.
	 * 
	 * @param pMartginValue The martgin value.
	 */
	public void setMvMartginValue(String pMartginValue) {
		mvMartginValue = pMartginValue;
	}

	/**
	 * This method returns the buy sell.
	 * 
	 * @return the buy sell is formatted.
	 */
	public String getMvBuySell() {
		return mvBuySell;
	}

	/**
	 * This method sets the buy sell.
	 * 
	 * @param pBuySell The buy sell.
	 */
	public void setMvBuySell(String pBuySell) {
		mvBuySell = pBuySell;
	}

	/**
	 * @param mvProfitPrc the mvProfitPrc to set
	 */
	public void setMvProfitPrc(String mvProfitPrc) {
		this.mvProfitPrc = mvProfitPrc;
	}

	/**
	 * @return the mvProfitPrc
	 */
	public String getMvProfitPrc() {
		return mvProfitPrc;
	}

	/**
	 * @param mvLossPrc the mvLossPrc to set
	 */
	public void setMvLossPrc(String mvLossPrc) {
		this.mvLossPrc = mvLossPrc;
	}

	/**
	 * @return the mvLossPrc
	 */
	public String getMvLossPrc() {
		return mvLossPrc;
	}

	public void setMvTEntitlementQty(String mvTEntitlementQty) {
		this.mvTEntitlementQty = mvTEntitlementQty;
	}

	public String getMvTEntitlementQty() {
		return mvTEntitlementQty;
	}

	public void setMvTAwaitingTraceCert(String mvTAwaitingTraceCert) {
		this.mvTAwaitingTraceCert = mvTAwaitingTraceCert;
	}

	public String getMvTAwaitingTraceCert() {
		return mvTAwaitingTraceCert;
	}

	public String getMvTAwaitingDepositCert() {
		return mvTAwaitingDepositCert;
	}

	public void setMvTAwaitingDepositCert(String mvTAwaitingDepositCert) {
		this.mvTAwaitingDepositCert = mvTAwaitingDepositCert;
	}

	public String getMvTAwaitingWithdrawalCert() {
		return mvTAwaitingWithdrawalCert;
	}

	public void setMvTAwaitingWithdrawalCert(String mvTAwaitingWithdrawalCert) {
		this.mvTAwaitingWithdrawalCert = mvTAwaitingWithdrawalCert;
	}

	/**
	 * @param totalPendingBuy the totalPendingBuy to set
	 */
	public void setTotalPendingBuy(String totalPendingBuy) {
		this.totalPendingBuy = totalPendingBuy;
	}

	/**
	 * @return the totalPendingBuy
	 */
	public String getTotalPendingBuy() {
		return totalPendingBuy;
	}

	/**
	 * @return the mvTMortgageQty
	 */
	public String getMvTMortgageQty() {
		return mvTMortgageQty;
	}

	/**
	 * @param mvTMortgageQty the mvTMortgageQty to set
	 */
	public void setMvTMortgageQty(String mvTMortgageQty) {
		this.mvTMortgageQty = mvTMortgageQty;
	}

	/**
	 * @return the mvTTodayUnsettleBuy
	 */
	public String getMvTTodayUnsettleBuy() {
		return mvTTodayUnsettleBuy;
	}

	/**
	 * @param mvTTodayUnsettleBuy the mvTTodayUnsettleBuy to set
	 */
	public void setMvTTodayUnsettleBuy(String mvTTodayUnsettleBuy) {
		this.mvTTodayUnsettleBuy = mvTTodayUnsettleBuy;
	}

	/**
	 * @return the mvTTodayUnsettleSell
	 */
	public String getMvTTodayUnsettleSell() {
		return mvTTodayUnsettleSell;
	}

	/**
	 * @param mvTTodayUnsettleSell the mvTTodayUnsettleSell to set
	 */
	public void setMvTTodayUnsettleSell(String mvTTodayUnsettleSell) {
		this.mvTTodayUnsettleSell = mvTTodayUnsettleSell;
	}

	/**
	 * @return the mvTT2UnsettleBuy
	 */
	public String getMvTT2UnsettleBuy() {
		return mvTT2UnsettleBuy;
	}

	/**
	 * @param mvTT2UnsettleBuy the mvTT2UnsettleBuy to set
	 */
	public void setMvTT2UnsettleBuy(String mvTT2UnsettleBuy) {
		this.mvTT2UnsettleBuy = mvTT2UnsettleBuy;
	}

	/**
	 * @return the mvTT1UnsettleSell
	 */
	public String getMvTT1UnsettleSell() {
		return mvTT1UnsettleSell;
	}

	/**
	 * @param mvTT1UnsettleSell the mvTT1UnsettleSell to set
	 */
	public void setMvTT1UnsettleSell(String mvTT1UnsettleSell) {
		this.mvTT1UnsettleSell = mvTT1UnsettleSell;
	}

	/**
	 * @return the mvTT3UnsettleSell
	 */
	public String getMvTT3UnsettleSell() {
		return mvTT3UnsettleSell;
	}

	/**
	 * @param mvTT3UnsettleSell the mvTT3UnsettleSell to set
	 */
	public void setMvTT3UnsettleSell(String mvTT3UnsettleSell) {
		this.mvTT3UnsettleSell = mvTT3UnsettleSell;
	}

	/**
	 * @return the mvTT1UnsettleBuy
	 */
	public String getMvTT1UnsettleBuy() {
		return mvTT1UnsettleBuy;
	}

	/**
	 * @param mvTT1UnsettleBuy the mvTT1UnsettleBuy to set
	 */
	public void setMvTT1UnsettleBuy(String mvTT1UnsettleBuy) {
		this.mvTT1UnsettleBuy = mvTT1UnsettleBuy;
	}

	/**
	 * @return the mvTT3UnsettleBuy
	 */
	public String getMvTT3UnsettleBuy() {
		return mvTT3UnsettleBuy;
	}

	/**
	 * @param mvTT3UnsettleBuy the mvTT3UnsettleBuy to set
	 */
	public void setMvTT3UnsettleBuy(String mvTT3UnsettleBuy) {
		this.mvTT3UnsettleBuy = mvTT3UnsettleBuy;
	}

	/**
	 * @return the mvTT2UnsettleSell
	 */
	public String getMvTT2UnsettleSell() {
		return mvTT2UnsettleSell;
	}

	/**
	 * @param mvTT2UnsettleSell the mvTT2UnsettleSell to set
	 */
	public void setMvTT2UnsettleSell(String mvTT2UnsettleSell) {
		this.mvTT2UnsettleSell = mvTT2UnsettleSell;
	}

	/**
	 * @return the mvTDueBuy
	 */
	public String getMvTDueBuy() {
		return mvTDueBuy;
	}

	/**
	 * @param mvTDueBuy the mvTDueBuy to set
	 */
	public void setMvTDueBuy(String mvTDueBuy) {
		this.mvTDueBuy = mvTDueBuy;
	}

	/**
	 * @return the mvTSettled
	 */
	public String getMvTSettled() {
		return mvTSettled;
	}

	/**
	 * @param mvTSettled the mvTSettled to set
	 */
	public void setMvTSettled(String mvTSettled) {
		this.mvTSettled = mvTSettled;
	}

	public String getNormalHold() {
		return normalHold;
	}

	public void setNormalHold(String normalHold) {
		this.normalHold = normalHold;
	}

	public String getConditionalHold() {
		return conditionalHold;
	}

	public void setConditionalHold(String conditionalHold) {
		this.conditionalHold = conditionalHold;
	}

	public String getMaintenancePercentage() {
		return maintenancePercentage;
	}

	public void setMaintenancePercentage(String maintenancePercentage) {
		this.maintenancePercentage = maintenancePercentage;
	}

	public String getMaintenanceValue() {
		return maintenanceValue;
	}

	public void setMaintenanceValue(String maintenanceValue) {
		this.maintenanceValue = maintenanceValue;
	}

	public String getMvTUnSettleSell() {
		return mvTUnSettleSell;
	}

	public void setMvTUnSettleSell(String mvTUnSettleSell) {
		this.mvTUnSettleSell = mvTUnSettleSell;
	}

}
