package com.ttl.old.itrade.hks.bean;

/**
 * The HKSTradeEnquiryBean class define variables that to save values 
 * for action 
 * @author
 *
 */
public class HKSTradeEnquiryBean {

	private int mvTradeEnquiryBeanID;
	private String mvTradeDateTime;
	private String mvQuantity;
	private String mvPrice;
	
	/**
	* This method returns the trade enquiry bean id.
	* @return the trade enquiry bean id is  not formatted.
    */
	public int getMvTradeEnquiryBeanID() {
		return mvTradeEnquiryBeanID;
	}
	
	/**
     * This method sets the trade enquiry bean id.
     * @param pTradeEnquiryBeanID The trade enquiry bean id.
     */
	public void setMvTradeEnquiryBeanID(int pTradeEnquiryBeanID) {
		mvTradeEnquiryBeanID = pTradeEnquiryBeanID;
	}
	
	/**
	* This method returns the trade date time.
	* @return the trade date time is  not formatted.
    */
	public String getMvTradeDateTime() {
		return mvTradeDateTime;
	}
	
	/**
     * This method sets the trade date time.
     * @param pTradeDateTime The trade date time.
     */
	public void setMvTradeDateTime(String pTradeDateTime) {
		mvTradeDateTime = pTradeDateTime;
	}
	
	/**
	* This method returns the quantity.
	* @return the quantity is  not formatted.
    */
	public String getMvQuantity() {
		return mvQuantity;
	}
	
	/**
     * This method sets the quantity.
     * @param pQuantity The quantity.
     */
	public void setMvQuantity(String pQuantity) {
		mvQuantity = pQuantity;
	}
	
	/**
	* This method returns the price.
	* @return the price is not formatted.
    */
	public String getMvPrice() {
		return mvPrice;
	}
	
	/**
     * This method sets the price.
     * @param pPrice The price.
     */
	public void setMvPrice(String pPrice) {
		mvPrice = pPrice;
	}
}
