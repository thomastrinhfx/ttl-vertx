package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;
/**
 * This class processes the register DN
 * @author not attributable
 *
 */
public class HKSRegisterDNTxn extends BaseTxn
{
   private Vector mvTopicNameList;
   /**
    * Constructor for HKSRegisterDNTxn class
    */
   public HKSRegisterDNTxn()
   {
	   super();
	   mvTopicNameList = new Vector();
   }
   /**
    * Constructor for HKSRegisterDNTxn class
    * @param pTopicNameList the topic name list
    */
   public HKSRegisterDNTxn(Vector pTopicNameList)
   {
      super();
      mvTopicNameList = pTopicNameList;
   }
   /**
    * This method process the register DN
    */
   public void process()
   {
      Hashtable lvTxnMap = new Hashtable();
      String lvTopicName = "";
      for (int i = 0; i < mvTopicNameList.size(); i++)
      {
    	  if (i != 0)
    		  lvTopicName += "|" + mvTopicNameList.get(i);
    	  else
    		  lvTopicName += (String) mvTopicNameList.get(i);
      }
      lvTxnMap.put("TOPICNAME", lvTopicName);

      if (TPErrorHandling.TP_NORMAL != process(RequestName.HKSRegisterDN, lvTxnMap))
      {
    	  Log.println("Register DN Failed!", Log.ERROR_LOG);
      }
   }
   /**
    * The method for add topic name
    * @param pTopicName the topic name
    */
   public void addTopicName(String pTopicName)
   {
	   mvTopicNameList.add(pTopicName);
   }
}
