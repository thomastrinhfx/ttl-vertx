package com.ttl.old.itrade.hks.txn.plugin;

import java.util.Hashtable;

import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.winvest.hks.config.mapping.TagName;

public class DemoBLMQueryTxn extends BaseTxn {
	private String language;
	private String stockID;
	private String mvClosingPrice;
	private String mvErrorMessageCode=null;

	/**
	 * constructor for DemoBLMQueryTxn class
	 * @param stockID
	 */
	public DemoBLMQueryTxn(String pStockID) {
		super();
		this.stockID = pStockID;
	}
	/**
	 * Get the Closing Price
	 * @return mvClosingPrice
	 */
	public String getClosingPrice() {
		return mvClosingPrice;
	}
	/**
	 * Set the Closing Price
	 * @param pClosingPrice
	 */
	public void setMvClosingPrice(String pClosingPrice) {
		mvClosingPrice = pClosingPrice;
	}
	/**
	 * Get the Error Message Code
	 * @return mvErrorMessageCode
	 */
	public String getErrorMessageCode() {
		return mvErrorMessageCode;
	}
	/**
	 * Set the Error Message Code
	 * @param pErrorMessageCode
	 */
	public void setErrorMessageCode(String pErrorMessageCode) {
		this.mvErrorMessageCode = pErrorMessageCode;
	}
	
	public void process() {
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put("INSTRUMENTID", stockID);
		lvTxnMap.put("OPERATORLIMIT", "P");
		lvTxnMap.put("DISPLAYINTERNETBALANCE", "Y");
		if (TPErrorHandling.TP_NORMAL == process("Demo", lvTxnMap,this.language)) 
		{
			String lvClosingPrice=null;
			try {
				IMsgXMLNode lvLoop = mvReturnNode.getChildNode("LOOP");
				IMsgXMLNodeList lvInstrumentNodeList = lvLoop
						.getNodeList(TagName.LOOP_ELEMENT);
				IMsgXMLNode lvClosingPriceNode = null;
				for (int i = 0; i < lvInstrumentNodeList.size(); i++) {
					lvClosingPriceNode = lvInstrumentNodeList.getNode(i)
							.getChildNode("CLOSINGPRICE");
					if (lvClosingPriceNode != null) {
						break;
					}
				}
				lvClosingPrice = lvClosingPriceNode.getValue();
			} catch (NullPointerException e) {
				Log.println("TP XML Response : node not exists",Log.ERROR_LOG);
				setErrorMessageCode("COMMUNICATION_ERROR");
			}
			if(lvClosingPrice==null || lvClosingPrice==""){
				setErrorMessageCode("NONEXISTING_STOCKID");
				return;
			}
			setMvClosingPrice(lvClosingPrice);
		}else
        { // Unhandled Business Exception
			
			setErrorMessageCode("SERVER_ERROR");
			
            //Instrument  not found
            if (mvReturnNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase("CORE10017")) {
            	setErrorMessageCode("NONEXISTING_STOCKID");
               }
         }
	}
}