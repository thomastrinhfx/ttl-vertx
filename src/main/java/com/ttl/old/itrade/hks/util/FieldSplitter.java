package com.ttl.old.itrade.hks.util;


/**
 * Title:        FieldSplitter
 * Description:  This class splits the field name and field value from "|" separated strings
 * Copyright:    Copyright (c) 2002
 * Company:      Transaction Technologies Ltd.
 * @author
 * @version 1.0
 * @since 2002.12.02
 */


import java.util.*;

import com.systekit.common.msg.*;

public class FieldSplitter
{

	/*
	 * public static void main(String[] argv) {
	 * String fieldname = "ORDERID|CLIENTID|TRADINGACCSEQ|SERIESID|BS|PRICE|QTY|OSQTY|FILLEDQTY|STATUS|VALIDITY|VALIDITYDATE|PENDACTION|PENDQTY|PENDVALIDITY|PENDVALIDITYDATE|CUST|BROKERID|CREATETIME|MODIFYTIME|ENTITYID|USERID|DNSEQ|UUID|ORDERTYPE|POSITION|PENDPOSITION|AO|CNV|STOP|STOPPRICE|PENDSTOPPRICE|REMARKS|PENDREMARKS";
	 * String values = "1296|1000000011|1|HSIH2|B|10000.0|1|1|0|ACK|Fok|||0|||N||2002-11-22 15:23:10.917|2002-11-22 15:23:10.917|SYSTEKIT|1|2|41|O|L||N|N|N|0.0|0.0|tests|";
	 * Hashtable abc = FieldSplitter.tokenize(fieldname, values, "|");
	 *
	 * //		System.out.println(abc.get("MODIFYTIME"));
	 * System.out.println(TableFormatter.makeHtmlTableRow(values, "|"));
	 * }
	 */
	public static Hashtable tokenize(String fieldname, String values, String token)
	{

		Hashtable   lvFields = new Hashtable();
		String		newValues = values;
		int			i = 0;

		while (i != -1)
		{
			i = newValues.indexOf(token + token);
			newValues = newValues.substring(0, i + 1) + " " + newValues.substring(i + 1);
		}

		StringTokenizer nameToken = new StringTokenizer(fieldname, token);
		StringTokenizer valueToken = new StringTokenizer(newValues, token);

		while (nameToken.hasMoreTokens())
		{
			String f = nameToken.nextToken().trim();
		 String v = "";
		 if (valueToken.hasMoreTokens())
		 {
				v = valueToken.nextToken().trim();
			}
			lvFields.put(f, v);
		}

		return lvFields;
	}

	/**
	 * Constructs Field Name and Value Mapping
	 * @param pFieldNameMapNode
	 * @param pValueMapNode
	 * @return Field Name and value Mapping
	 */
	public static Hashtable tokenizeNodes(IMsgXMLNode pFieldNameMapNode, IMsgXMLNode pValueMapNode)
	{
		Hashtable lvOutputMap = new Hashtable();
		int i = 0;
		String lvAttributeName;
		String lvFieldName;
		String lvValue;
		do {
			lvAttributeName = "v"+i;
			lvFieldName = pFieldNameMapNode.getAttribute(lvAttributeName);
			lvValue = pValueMapNode.getAttribute(lvAttributeName);
			if (lvFieldName != null)
				lvOutputMap.put(lvFieldName, lvValue);
			i++;
		} while (lvFieldName != null);
		return lvOutputMap;
	}
	
	/**
	 * Construct Field names and values mapping
	 * Handle old message format for order enquiry
	 * @author Bowen Chau
	 * @version 29 Mar 2006
	 * @param IMsgXMLNode pFieldNameMapNode
	 * @param IMsgXMLNode pValueMapNode
	 * @return Hashtable lvOutputMap
	 */
	
	public static Hashtable tokenizeOrderEnquiryNodes (IMsgXMLNode pFieldNameMapNode, IMsgXMLNode pValueMapNode) {
		Hashtable lvOutputMap = new Hashtable();
		String lvFieldNameString = pFieldNameMapNode.getValue().toString();
		String lvFieldValueString = pValueMapNode.getValue().toString();
		
		// Do not use StringTokenizer since it cannot include empty string between separator.
		//String[] lvFieldNameTokens = lvFieldNameString.split("\\|");
		//String[] lvFieldValueTokens = lvFieldValueString.split("\\|");
		String[] lvFieldNameTokens = StringSplitter.split(lvFieldNameString, "\\|");
		String[] lvFieldValueTokens = StringSplitter.split(lvFieldValueString, "\\|");
		
		String lvFieldName = "";
		String lvFieldValue = "";
		
		// Check if the number of field names and values is the same
		if (lvFieldNameTokens.length == lvFieldValueTokens.length) {
			for (int i = 0; i < lvFieldNameTokens.length; i++) {
				lvFieldName = lvFieldNameTokens[i];
				lvFieldValue = lvFieldValueTokens[i];
				
				lvOutputMap.put(lvFieldName, lvFieldValue);
			}
			
		} else {
			// Do nothing
		}
		
		return lvOutputMap;
	}
}
