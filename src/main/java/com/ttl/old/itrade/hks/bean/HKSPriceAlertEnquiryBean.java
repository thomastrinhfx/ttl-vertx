package com.ttl.old.itrade.hks.bean;
/**
 * The HKSPriceAlertEnquiryBean class define variables that to save values 
 * for action 
 * @author
 *
 */
public class HKSPriceAlertEnquiryBean {
	private String mvClientID;
	private String mvMarketID;
	private String mvInstrumentID;
	private String mvInstrumentName;
	private String mvSign;
	private String mvAlertPrice;
	private String mvCreationTime;
	private String mvNotified;
	private String mvNotificationTime;
	
	// BEGIN - Task #: TTL-GZ-XYL-00090 - YuLong Xu 20090820 Add log - Issue #: Adding Delete Action Field
	private String mvAlertID;
	private int mvBeanID;
	
	/**
	 * This method returns the bean id.
	 * @return the bean id is not formatted.
     */
	public int getMvBeanID() {
		return mvBeanID;
	}
	
	/**
     * This method sets the bean ID.
     * @param pBeanID The bean ID.
     */
	public void setMvBeanID(int pBeanID) {
		mvBeanID = pBeanID;
	}
	
	/**
	 * This method returns the alert id.
	 * @return the alert id is not formatted.
     */
	public String getMvAlertID() {
		return mvAlertID;
	}
	
	/**
     * This method sets the alert id.
     * @param pAlertID The alert id.
     */
	public void setMvAlertID(String pAlertID) {
		mvAlertID = pAlertID;
	}
	// BEGIN - Task #: YuLong Xu 20090820

	/**
	 * This method returns the instrument id.
	 * @return the instrument id is not formatted.
     */
	public String getMvInstrumentID() {
		return mvInstrumentID;
	}

	/**
     * This method sets the instrument id.
     * @param pInstrumentID The instrument id.
     */
	public void setMvInstrumentID(String pInstrumentID) {
		mvInstrumentID = pInstrumentID;
	}

	/**
	 * This method returns the instrument name.
	 * @return the instrument name is not formatted.
     */
	public String getMvInstrumentName() {
		return mvInstrumentName;
	}

	/**
     * This method sets the instrument name.
     * @param pInstrumentName The instrument name.
     */
	public void setMvInstrumentName(String pInstrumentName) {
		mvInstrumentName = pInstrumentName;
	}

	/**
	 * This method returns the Sign.
	 * @return the Sign is not formatted.
     */
	public String getMvSign() {
		return mvSign;
	}

	/**
     * This method sets the Sign.
     * @param pSign The Sign.
     */
	public void setMvSign(String pSign) {
		mvSign = pSign;
	}

	/**
	 * This method returns the alert price.
	 * @return the alert price is not formatted.
     */
	public String getMvAlertPrice() {
		return mvAlertPrice;
	}

	/**
     * This method sets the alert price.
     * @param pAlertPrice The alert price.
     */
	public void setMvAlertPrice(String pAlertPrice) {
		mvAlertPrice = pAlertPrice;
	}

	/**
	 * This method returns the creation time.
	 * @return the creation time is not formatted.
     */
	public String getMvCreationTime() {
		return mvCreationTime;
	}

	/**
     * This method sets the creation time.
     * @param pCreationTime The creation time.
     */
	public void setMvCreationTime(String pCreationTime) {
		mvCreationTime = pCreationTime;
	}

	/**
	 * This method returns the notified.
	 * @return the notified is not formatted.
     */
	public String getMvNotified() {
		return mvNotified;
	}

	/**
     * This method sets the notified.
     * @param pNotified The notified.
     */
	public void setMvNotified(String pNotified) {
		mvNotified = pNotified;
	}

	/**
	 * This method returns the notification time.
	 * @return the notification time is not formatted.
     */
	public String getMvNotificationTime() {
		return mvNotificationTime;
	}
	/**
     * This method sets the notification time.
     * @param pNotificationTime The notification time.
     */
	public void setMvNotificationTime(String pNotificationTime) {
		mvNotificationTime = pNotificationTime;
	}

	/**
	 * This method returns the client id.
	 * @return the client id time is not formatted.
     */
	public String getMvClientID() {
		return mvClientID;
	}

	/**
     * This method sets the client id.
     * @param pClientID The client id.
     */
	public void setMvClientID(String pClientID) {
		mvClientID = pClientID;
	}

	/**
	 * This method returns the market id.
	 * @return the market id time is formatted.
     */
	public String getMvMarketID() {
		return mvMarketID;
	}

	/**
     * This method sets the market id.
     * @param pMarketID The market id.
     */
	public void setMvMarketID(String pMarketID) {
		mvMarketID = pMarketID;
	}

}
