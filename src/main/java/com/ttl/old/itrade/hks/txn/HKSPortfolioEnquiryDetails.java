package com.ttl.old.itrade.hks.txn;

/**
 * <p>Title: Portfolio Enquiry Details</p>
 * <p>Description: This class stores the portfolio enquiry details</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

import java.util.Hashtable;

//Tree Lam 20080625 Comments Only
/**
 * This class the HKSPortfolioEnquiryTxn class entity
 * @author not attributable
 */
public class HKSPortfolioEnquiryDetails
{

        private String mvClientId;
        private String mvInstrumentId;
        //BEGIN Task #:- al00001 albert lai 20080828
        private String mvInstrumentName;
		private String mvInstrumentShortName;
		//END Task #:- al00001 albert lai 20080828
//	private String mvInstrumentChineseShortName;
        private String mvLedgerQty;
        private String mvTTodayBuy;
        private String mvTTodayConfirmBuy;
        private String mvTTodaySell;
        private String mvTTodayConfirmSell;
        private String mvTradableQty;
        private String mvTInactiveBuy;
        private String mvTInactiveSell;
        private String mvNominalPrice;
        private String mvClosingPrice;
        private String mvCostPerShare;
        private String mvCostQty;
        private String mvMarketValue;

    private String mvMarginPercentage;
    private String mvMarginValue;

    private String mvTSettled;
    private String mvTDueBuy;
    private String mvTDueSell;
    private String mvTUnSettleBuy;
    private String mvTUnSettleSell;
    private String mvTManualHold;
    //HIEU LE
    private String normalHold;
    private String conditionalHold;
    private String maintenancePercentage;
    private String maintenanceValue;
    //END HIEU LE
    private String mvTNonimeeQty;
    private String mvTCostAmount;
    private String mvTTodayCostAmount;
	private String mvTCostQty;
	private String mvTTodayCostQty;
	
	private String mvTotalAverageCost;
	private String mvTotalCostAmount;

    //Begin Task #:- TTL-CN-ZZW-0001 Wind Zhao 20090901 
    private String mvCurrencyID ;

    private String mvTTodayUnsettleBuy;
    private String mvTTodayUnsettleSell;
    private String mvTT1UnsettleBuy;
    private String mvTT2UnsettleBuy;
    private String mvTT3UnsettleBuy;    
    private String mvTT1UnsettleSell;
    private String mvTT2UnsettleSell;
    private String mvTT3UnsettleSell;
    

  //BEGIN TASK #:- TTL-GZ-XYL-00038 XuYuLong 20091119 ITrade .R5 VN Adding extra fields
    private String mvMktPrice;
    private String mvUnitPrice;
    private String mvTValue;
    private String mvPdBuy;
    private String mvPdSell;
    private String mvHoldAmt;
    
    //BEGIN TASK #:- TTL-VN VanTran 20100930 ITrade VN Adding extra fields
    private String mvTPendingEntitlementQty;
    private String mvTPendingEntitlementCostAmt;
    private String mvTAwaitingTraceCert;
    private String mvTAwaitingDepositCert;
    private String mvTAwaitingWithdrawalCert;
    private String mvTMortgateQty;
    
    
   
    /**
     * Get method for unit price
     * @return unit price
     */
    public String getMvUnitPrice() {
		return mvUnitPrice;
	}
    /**
     * Set method for unit price
     * @param pUnitPrice the unit price
     */
	public void setMvUnitPrice(String pUnitPrice) {
		this.mvUnitPrice = pUnitPrice;
	}
	/**
	 * Get method for T value
	 * @return T value
	 */
	public String getMvTValue() {
		return mvTValue;
	}
	/**
	 * Set method for T value
	 * @param pTValue the T value
	 */
	public void setMvTValue(String pTValue) {
		this.mvTValue = pTValue;
	}
	/**
	 * Get method for market price
	 * @return market price
	 */
	public String getMvMktPrice() {
		return mvMktPrice;
	}
	/**
	 * Set method for market price
	 * @param pMktPrice the market price
	 */
	public void setMvMktPrice(String pMktPrice) {
		this.mvMktPrice = pMktPrice;
	}
	/**
	 * Get method for product buy
	 * @return product buy
	 */
	public String getMvPdBuy() {
		return mvPdBuy;
	}
	/**
	 * Set method for product buy
	 * @param pPdBuy the product buy
	 */
	public void setMvPdBuy(String pPdBuy) {
		this.mvPdBuy = pPdBuy;
	}
	/**
	 * Get method for product sell
	 * @return product sell
	 */
	public String getMvPdSell() {
		return mvPdSell;
	}
	/**
	 * Set method for product sell
	 * @param pPdSell the product sell
	 */
	public void setMvPdSell(String pPdSell) {
		this.mvPdSell = pPdSell;
	}
	/**
	 * Get method for hold amount
	 * @return hold amount
	 */
	public String getMvHoldAmt() {
		return mvHoldAmt;
	}
	/**
	 * Set method for hold amount
	 * @param pHoldAmt the hold amount
	 */
	public void setMvHoldAmt(String pHoldAmt) {
		this.mvHoldAmt = pHoldAmt;
	}
	//END TASK #:- TTL-GZ-XYL-00038 XuYuLong 20091119 ITrade .R5 VN Adding extra fields
	/**
	 * Get method for currency id
	 * @return currency id
	 */
    public String getMvCurrencyID() {
    	return mvCurrencyID;
    }
    /**
     * Set method for currency id
     * @param pCurrencyID the currency id
     */
    public void setMvCurrencyID(String pCurrencyID) {
    	this.mvCurrencyID = pCurrencyID;
    }
    //End Task #:- TTL-CN-ZZW-0001 Wind Zhao 20090901 

	//Begin Task: WL00619 Walter Lau 2007 July 27
    private String mvMarketID;
    private String mvProductID;
    //End Task: WL00619 Walter Lau 2007 July 27

//	private String mvHyperLink;

//	private String		seriesId;
//	private String		product;
//	private String		contractMonth;
//	private String		longQty;
//	private String		shortQty;
//	private String		net;
//	/***  comment by May on 15-04-2004
//	private String		tradePrice;
//   **************************************/
//	/***  added by May on 15-04-2004    ***/
//	private String    bidPriceAvg;
//	private String    askPriceAvg;
//	/**************************************/
//	private String		marketPrice;
//	private String		openPosition;
//	private String		CCY;
//	/***	comment by May on 02-02-2004
//	private String		tradeDate;
//	*************************************/

        private Hashtable   mvHMap;

        /**
         * Get the client ID
         * @return Client ID
         */
        public String getClientId()
        {
                return mvClientId;
        }

        //Begin Task: WL00619 Walter Lau 2007 July 27
        /**
         * Get the Product ID
         * @return Product ID
         */
        public String getProductID()
        {
                return mvProductID;
        }

        /**
         * Get the Market ID
         * @return Market ID
         */
        public String getMarketID()
        {
                return mvMarketID;
        }
        //End Task: WL00619 Walter Lau 2007 July 27

        /**
         * Get the Instrument ID
         * @return Instrument ID
         */
        public String getInstrumentId()
        {
                return mvInstrumentId;
        }

//	public String getInstrumentShortName()
//	{
//		return mvInstrumentShortName;
//	}
//
//	public String getInstrumentChineseShortName()
//	{
//		return mvInstrumentChineseShortName;
//	}

        /**
         * Get the Ledger quantity
         * @return Ledger quantity
         */
        public String getLedgerQty()
        {
                return mvLedgerQty;
        }

        /**
         * Get Today Buy quantity
         * @return Today Buy quantity
         */
        public String getTTodayBuy()
        {
                return mvTTodayBuy;
        }

        /**
         * Get Today Confirm Buy quantity
         * @return Today Confirm Buy quantity
         */
        public String getTTodayConfirmBuy()
        {
                return mvTTodayConfirmBuy;
        }

        /**
         * Get Today Sell quantity
         * @return Today Sell quantity
         */
        public String getTTodaySell()
        {
                return mvTTodaySell;
        }

        /**
         * Get Today Confirm Sell quantity
         * @return Today Confirm Sell quantity
         */
        public String getTTodayConfirmSell()
        {
                return mvTTodayConfirmSell;
        }

        /**
         * Get Today Trading quantity
         * @return Today Trading quantity
         */
        public String getTradableQty()
        {
                return mvTradableQty;
        }

        /**
         * Get Manual Hold quantity
         * @return Manual Hold quantity
         */
        public String getManualHold()
        {
                return mvTManualHold;
        }

        /**
         * Get Nominal Price
         * @return Nominal Price
         */
        public String getNominalPrice()
        {
                return mvNominalPrice;
        }

        /**
         * Get Closing Price
         * @return Closing Price
         */
        public String getClosingPrice()
        {
                return mvClosingPrice;
        }


        /**
         * Get Cost Per Share
         * @return Cost Per Share
         */
        public String getCostPerShare()
        {
                return mvCostPerShare;
        }

        /**
         * Get Cost quantity
         * @return Cost quantity
         */
        public String getCostQty()
        {
                return mvCostQty;
        }

        /**
         * Get Market Value
         * @return Market Value
         */
        public String getMarketValue() {
                return mvMarketValue;
        }

        /**
         * Get Margin Percentage
         * @return Margin Percentage
         */
    public String getMarginPercentage()
    {
        return mvMarginPercentage;
    }

    /**
     * Get Margin Value
     * @return Margin Value
     */
    public String getMarginValue()
    {
        return mvMarginValue;
    }

    /**
     * Get Total Average Cost
     * @return Total Average Cost
     */
        public String getTotalAverageCost()
        {
                return mvTotalAverageCost;
        }

        /**
         * Get Total Cost Amount
         * @return Total Cost Amount
         */
        public String getTotalCostAmount()
        {
                return mvTotalCostAmount;
        }
        //BEGIN Task #:- al00001 albert lai 20080828
        /**
         * Get method for instrument short name
         * @return instrument short name
         */
		public String getInstrumentShortName() {
			return mvInstrumentShortName;
		}

		/**
		 * Get method for instrument name
		 * @return instrument name
		 */
        public String getInstrumentName() {
			return mvInstrumentName;
		}
      //END Task #:- al00001 albert lai 20080828

/**********************************************/

        /**
         * Set Client ID
         * @param pClientId the Client ID
         */
        public void setClientId(String pClientId)
        {
                mvClientId = pClientId;
        }

        /**
         * Set Instrument ID
         * @param pInstrumentId the Instrument ID
         */
        public void setInstrumentId(String pInstrumentId)
        {
                mvInstrumentId = pInstrumentId;
        }

        //Begin Task: WL00619 Walter Lau 2007 July 27
        /**
         * Set Product ID
         * @param pProductID the Product ID
         */
        public void setProductID(String pProductID)
        {
                mvProductID = pProductID;
        }

        /**
         * Set Market ID
         * @param pMarketID the Market ID
         */
        public void setMarketID(String pMarketID)
        {
                mvMarketID = pMarketID;
        }
        //End Task: WL00619 Walter Lau 2007 July 27

//	public void setInstrumentShortName(String pInstrumentShortName)
//	{
//		mvInstrumentShortName = pInstrumentShortName;
//	}
//
//	public void setInstrumentChineseShortName(String pInstrumentChineseShortName)
//	{
//		mvInstrumentChineseShortName = pInstrumentChineseShortName;
//	}

        /**
         * Set Ledger quantity
         * @param pLedgerQty the Ledger quantity
         */
        public void setLedgerQty(String pLedgerQty)
        {
                mvLedgerQty = pLedgerQty;
        }

        /**
         * Set Today Buy quantity
         * @param pTTodayBuy the Today Buy quantity
         */
        public void setTTodayBuy(String pTTodayBuy)
        {
                mvTTodayBuy = pTTodayBuy;
        }

        /**
         * Set Today Confirm Buy quantity
         * @param pTTodayConfirmBuy the Today Confirm Buy quantity
         */
        public void setTTodayConfirmBuy(String pTTodayConfirmBuy)
        {
                mvTTodayConfirmBuy = pTTodayConfirmBuy;
        }

        /**
         * Set Today Sell quantity
         * @param pTTodaySell the Today Sell quantity
         */
        public void setTTodaySell(String pTTodaySell)
        {
                mvTTodaySell = pTTodaySell;
        }

        /**
         * Set Today Confirm Sell quantity
         * @param pTTodayConfirmSell the Today Confirm Sell quantity
         */
        public void setTTodayConfirmSell(String pTTodayConfirmSell)
        {
                mvTTodayConfirmSell = pTTodayConfirmSell;
        }

        /**
         * Set Trading quantity
         * @param pTradableQty the Trading quantity
         */
        public void setTradableQty(String pTradableQty)
        {
                mvTradableQty = pTradableQty;
        }

        /**
         * Set Nominal Price
         * @param pNominalPrice the Nominal Price
         */
        public void setNominalPrice(String pNominalPrice)
        {
                mvNominalPrice = pNominalPrice;
        }

        /**
         * Set Closing Price
         * @param pClosingPrice the Closing Price
         */
        public void setClosingPrice(String pClosingPrice)
        {
                mvClosingPrice = pClosingPrice;
        }

        /**
         * Set Cost Per Share
         * @param pCostPerShare the Cost Per Share
         */
        public void setCostPerShare(String pCostPerShare)
        {
                mvCostPerShare = pCostPerShare;
        }

        /**
         * Set Cost quantity
         * @param pCostQty the Cost quantity
         */
        public void setCostQty(String pCostQty)
        {
                mvCostQty = pCostQty;
        }

        /**
         * Set market value
         * @param pMarketValue the market value
         */
        public void setMarketValue(String pMarketValue) {
                mvMarketValue = pMarketValue;
        }

        /**
         * Set margin percentage
         * @param pMarginPercentage the margin percentage
         */
    public void setMarginPercentage(String pMarginPercentage)
    {
        mvMarginPercentage = pMarginPercentage;
    }

    /**
     * Set margin value
     * @param pMarginValue the margin value
     */
    public void setMarginValue(String pMarginValue)
    {
        mvMarginValue = pMarginValue;
    }

    /**
     * Get the Hashtable that stores the values
     * @return Hashtable that stores the values
     */
        public Hashtable getHashtable()
        {
                return mvHMap;
        }

		/**
		 * Set the Hashtable that stores the values
		 * @param pHMap the Hashtable that stores the values
		 */
        public void setHashtable(Hashtable pHMap)
        {
                mvHMap = pHMap;
        }

        /**
         * Set Settled quantity
         * @param pTSettled the Settled quantity
         */
   public void setTSettled(String pTSettled)
   {
      mvTSettled = pTSettled;
   }

   /**
    * Get Settled quantity
    * @return Settled quantity
    */
   public String getTSettled()
   {
      return mvTSettled;
   }

   /**
    * Set Due Buy quantity
    * @param pTDueBuy the Buy quantity
    */
   public void setTDueBuy(String pTDueBuy)
   {
      mvTDueBuy = pTDueBuy;
   }

   /**
    * Get Due Buy quantity
    * @return Due Buy quantity
    */
   public String getTDueBuy()
   {
      return mvTDueBuy;
   }

   /**
    * Set Due Sell quantity
    * @param pTDueSell the Due Sell quantity
    */
   public void setTDueSell(String pTDueSell)
   {
      mvTDueSell = pTDueSell;
   }

   /**
    * Get Due Sell quantity
    * @return Due Sell quantity
    */
   public String getTDueSell()
   {
      return mvTDueSell;
   }

   /**
    * Set Unsettle buy quantity
    * @param pTUnSettleBuy the Unsettle buy quantity
    */
   public void setTUnSettleBuy(String pTUnSettleBuy)
   {
      mvTUnSettleBuy = pTUnSettleBuy;
   }

   /**
    * Get Unsettle Buy quantity
    * @return Unsettle Buy quantity
    */
   public String getTUnSettleBuy()
   {
      return mvTUnSettleBuy;
   }

   /**
    * Set Unsettle sell quantity
    * @param pTUnSettleSell the Unsettle sell quantity
    */
   public void setTUnSettleSell(String pTUnSettleSell)
   {
      mvTUnSettleSell = pTUnSettleSell;
   }

   /**
    * Get Unsettle Sell quantity
    * @return Unsettle Sell quantity
    */
   public String getTUnSettleSell()
   {
      return mvTUnSettleSell;
   }

   /**
    * Set Manual Hold quantity
    * @param pTManualHold the Manual Hold quantity
    */
   public void setTManualHold(String pTManualHold)
   {
      mvTManualHold = pTManualHold;
   }

   // BEGIN - TASK#: CL00015 - Charlie Liu 20080830
   /**
    * Get Manual Hold quantity
    * @return Manual Hold quantity
    */
   public String getTManualHold()
   {
      return mvTManualHold;
   }
   // END - TASK#  CL00015

   // Added by Bowen Chau on 22 Mar 2006
   /**
    * Set Nominee quantity
    * @param pTNomineeQty the Nominee quantity
    */
   public void setTNomineeQty (String pTNomineeQty) {
           mvTNonimeeQty = pTNomineeQty;
   }

   /**
    * Get Nominee quantity
    * @return Nominee quantity
    */
   public String getTNomineeQty() {
           return mvTNonimeeQty;
   }

   /**
    * Set inactive sell quantity
    * @param pTInactiveSell the inactive sell quantity
    */
   public void setTInactiveSell(String pTInactiveSell) {
           mvTInactiveSell = pTInactiveSell;
   }

   /**
    * Get Inactive Sell quantity
    * @return Inactive Sell quantity
    */
   public String getTInactiveSell() {
           return mvTInactiveSell;
   }

   /**
    * Set inactive buy quantity
    * @param pTInactiveBuy the inactive buy quantity
    */
   public void setTInactiveBuy(String pTInactiveBuy) {
           mvTInactiveBuy = pTInactiveBuy;
   }

   /**
    * Get Inactive Buy quantity
    * @return Inactive Buy quantity
    */
   public String getTInactiveBuy() {
           return mvTInactiveBuy;
   }

   /**
    * Get Cost Amount
    * @return Cost Amount
    */
   public String getTCostAmount()
   {
           return mvTCostAmount;
   }

   		/**
   		 * Get Cost quantity
   		 * @return Cost quantity
   		 */
        public String getTCostQty()
        {
                          return mvTCostQty;
        }


        /**
         * Set Cost Amount
         * @param pTCostAmount the Cost Amount
         */
   public void setTCostAmount(String pTCostAmount)
   {
           mvTCostAmount = pTCostAmount;
   }

		/**
		 * Set Cost quantity
		 * @param pTCostQty the Cost quantity
		 */
	    public void setTCostQty(String pTCostQty)
	    {
	               mvTCostQty = pTCostQty;
	    }
	
	    /**
	     * Set Total Average Cost
	     * @param pTotalAverageCost the Total Average Cost
	     */
	    public void setTotalAverageCost(String pTotalAverageCost)
	    {
	            mvTotalAverageCost = pTotalAverageCost;
	    }
	
	    /**
	     * Set Total Cost Amount
	     * @param pTotalCostAmount the Total Cost Amount
	     */
	    public void setTotalCostAmount(String pTotalCostAmount)
	     {
	             mvTotalCostAmount = pTotalCostAmount;
	     }
	
	  //BEGIN Task #:- al00001 albert lai 20080828
	    /**
	     * Set method for instrument name
	     * @param pInstrumentName the instrument name
	     */
		public void setInstrumentName(String pInstrumentName) {
			this.mvInstrumentName = pInstrumentName;
		}
	
		/**
		 * Set method for instrument short name
		 * @param pInstrumentShortName the instrument short name
		 */
		public void setInstrumentShortName(String pInstrumentShortName) {
			this.mvInstrumentShortName = pInstrumentShortName;
		}
		//END Task #:- al00001 albert lai 20080828
	
		public void setMvTAwaitingTraceCert(String mvTAwaitingTraceCert) {
			this.mvTAwaitingTraceCert = mvTAwaitingTraceCert;
		}
		public String getMvTAwaitingTraceCert() {
			return mvTAwaitingTraceCert;
		}
		/**
		 * @param mvTAwaitingDepositCert the mvTAwaitingDepositCert to set
		 */
		public void setMvTAwaitingDepositCert(String mvTAwaitingDepositCert) {
			this.mvTAwaitingDepositCert = mvTAwaitingDepositCert;
		}
		/**
		 * @return the mvTAwaitingDepositCert
		 */
		public String getMvTAwaitingDepositCert() {
			return mvTAwaitingDepositCert;
		}
		/**
		 * @param mvTAwaitingWithdrawalCert the mvTAwaitingWithdrawalCert to set
		 */
		public void setMvTAwaitingWithdrawalCert(
				String mvTAwaitingWithdrawalCert) {
			this.mvTAwaitingWithdrawalCert = mvTAwaitingWithdrawalCert;
		}
		/**
		 * @return the mvTAwaitingWithdrawalCert
		 */
		public String getMvTAwaitingWithdrawalCert() {
			return mvTAwaitingWithdrawalCert;
		}
		/**
		 * @param mvTMortgateQty the mvTMortgateQty to set
		 */
		public void setMvTMortgateQty(String mvTMortgateQty) {
			this.mvTMortgateQty = mvTMortgateQty;
		}
		/**
		 * @return the mvTMortgateQty
		 */
		public String getMvTMortgateQty() {
			return mvTMortgateQty;
		}
		/**
		 * @return the mvTTodayUnsettleBuy
		 */
		public String getMvTTodayUnsettleBuy() {
			return mvTTodayUnsettleBuy;
		}
		/**
		 * @param mvTTodayUnsettleBuy the mvTTodayUnsettleBuy to set
		 */
		public void setMvTTodayUnsettleBuy(String mvTTodayUnsettleBuy) {
			this.mvTTodayUnsettleBuy = mvTTodayUnsettleBuy;
		}
		/**
		 * @return the mvTTodayUnsettleSell
		 */
		public String getMvTTodayUnsettleSell() {
			return mvTTodayUnsettleSell;
		}
		/**
		 * @param mvTTodayUnsettleSell the mvTTodayUnsettleSell to set
		 */
		public void setMvTTodayUnsettleSell(String mvTTodayUnsettleSell) {
			this.mvTTodayUnsettleSell = mvTTodayUnsettleSell;
		}
		/**
		 * @return the mvTT2UnsettleBuy
		 */
		public String getMvTT2UnsettleBuy() {
			return mvTT2UnsettleBuy;
		}
		/**
		 * @param mvTT2UnsettleBuy the mvTT2UnsettleBuy to set
		 */
		public void setMvTT2UnsettleBuy(String mvTT2UnsettleBuy) {
			this.mvTT2UnsettleBuy = mvTT2UnsettleBuy;
		}
		/**
		 * @return the mvTT1UnsettleBuy
		 */
		public String getMvTT1UnsettleBuy() {
			return mvTT1UnsettleBuy;
		}
		/**
		 * @param mvTT1UnsettleBuy the mvTT1UnsettleBuy to set
		 */
		public void setMvTT1UnsettleBuy(String mvTT1UnsettleBuy) {
			this.mvTT1UnsettleBuy = mvTT1UnsettleBuy;
		}
		/**
		 * @return the mvTT3UnsettleBuy
		 */
		public String getMvTT3UnsettleBuy() {
			return mvTT3UnsettleBuy;
		}
		/**
		 * @param mvTT3UnsettleBuy the mvTT3UnsettleBuy to set
		 */
		public void setMvTT3UnsettleBuy(String mvTT3UnsettleBuy) {
			this.mvTT3UnsettleBuy = mvTT3UnsettleBuy;
		}
		/**
		 * @return the mvTT1UnsettleSell
		 */
		public String getMvTT1UnsettleSell() {
			return mvTT1UnsettleSell;
		}
		/**
		 * @param mvTT1UnsettleSell the mvTT1UnsettleSell to set
		 */
		public void setMvTT1UnsettleSell(String mvTT1UnsettleSell) {
			this.mvTT1UnsettleSell = mvTT1UnsettleSell;
		}
		/**
		 * @return the mvTT2UnsettleSell
		 */
		public String getMvTT2UnsettleSell() {
			return mvTT2UnsettleSell;
		}
		/**
		 * @param mvTT2UnsettleSell the mvTT2UnsettleSell to set
		 */
		public void setMvTT2UnsettleSell(String mvTT2UnsettleSell) {
			this.mvTT2UnsettleSell = mvTT2UnsettleSell;
		}
		/**
		 * @return the mvTT3UnsettleSell
		 */
		public String getMvTT3UnsettleSell() {
			return mvTT3UnsettleSell;
		}
		/**
		 * @param mvTT3UnsettleSell the mvTT3UnsettleSell to set
		 */
		public void setMvTT3UnsettleSell(String mvTT3UnsettleSell) {
			this.mvTT3UnsettleSell = mvTT3UnsettleSell;
		}
		/**
		 * @return the mvTTodayCostQty
		 */
		public String getMvTTodayCostQty() {
			return mvTTodayCostQty;
		}
		/**
		 * @param mvTTodayCostQty the mvTTodayCostQty to set
		 */
		public void setMvTTodayCostQty(String mvTTodayCostQty) {
			this.mvTTodayCostQty = mvTTodayCostQty;
		}
		/**
		 * @return the mvTTodayCostAmount
		 */
		public String getMvTTodayCostAmount() {
			return mvTTodayCostAmount;
		}
		/**
		 * @param mvTTodayCostAmount the mvTTodayCostAmount to set
		 */
		public void setMvTTodayCostAmount(String mvTTodayCostAmount) {
			this.mvTTodayCostAmount = mvTTodayCostAmount;
		}
		/**
		 * @return the mvTPendingEntitlementCostAmt
		 */
		public String getMvTPendingEntitlementCostAmt() {
			return mvTPendingEntitlementCostAmt;
		}
		/**
		 * @param mvTPendingEntitlementCostAmt the mvTPendingEntitlementCostAmt to set
		 */
		public void setMvTPendingEntitlementCostAmt(
				String mvTPendingEntitlementCostAmt) {
			this.mvTPendingEntitlementCostAmt = mvTPendingEntitlementCostAmt;
		}
		/**
		 * @return the mvTPendingEntitlementQty
		 */
		public String getMvTPendingEntitlementQty() {
			return mvTPendingEntitlementQty;
		}
		/**
		 * @param mvTPendingEntitlementQty the mvTPendingEntitlementQty to set
		 */
		public void setMvTPendingEntitlementQty(String mvTPendingEntitlementQty) {
			this.mvTPendingEntitlementQty = mvTPendingEntitlementQty;
		}
		public String getNormalHold() {
			return normalHold;
		}
		public void setNormalHold(String normalHold) {
			this.normalHold = normalHold;
		}
		public String getConditionalHold() {
			return conditionalHold;
		}
		public void setConditionalHold(String conditionalHold) {
			this.conditionalHold = conditionalHold;
		}
		public String getMaintenancePercentage() {
			return maintenancePercentage;
		}
		public void setMaintenancePercentage(String maintenancePercentage) {
			this.maintenancePercentage = maintenancePercentage;
		}
		public String getMaintenanceValue() {
			return maintenanceValue;
		}
		public void setMaintenanceValue(String maintenanceValue) {
			this.maintenanceValue = maintenanceValue;
		}

}
