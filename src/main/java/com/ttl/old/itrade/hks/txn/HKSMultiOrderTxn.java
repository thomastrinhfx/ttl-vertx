package com.ttl.old.itrade.hks.txn;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.ttl.old.itrade.hks.bean.HKSMultiOrderBean;
import com.ttl.old.itrade.hks.util.TextFormatter;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.winvest.hks.config.mapping.TagName;

public class HKSMultiOrderTxn extends BaseTxn{
	
	private String mvBS;
	private String mvStockCode;
	private String mvQuantity;
	private String mvOrderTypeValue;
	private String mvPrice;
	private String mvStopPrice;
	private String mvGoodTillDate;
	private String mvGrossAmt;
	private String mvMarketID;
	private String mvClientID;
	
	// use for version bank
	private String mvBankID;
	private String mvBankACID;
	
	private String refId;
	private String activationDate;
	private String mvResult;
	private String mvTotalRecord;
	private String mvLatestId;
	private String mvSelIdList;
	
	public HKSMultiOrderTxn(){}
	
	public HKSMultiOrderTxn(String clientId, String bs, String stockCode, String marketId, String qty, String orderType, 
			String price, String stopPrice, String goodTillDate, String grossAmt, String pRefId, String bankId, String bankACID){
		mvClientID = clientId;
		mvBS = bs;
		mvStockCode = stockCode;
		mvMarketID = marketId;
		mvQuantity = qty;
		mvOrderTypeValue = orderType;
		mvPrice = price;
		mvStopPrice = stopPrice;
		mvGoodTillDate = goodTillDate;
		mvGrossAmt = grossAmt;
		mvBankID = bankId;
		mvBankACID = bankACID;
		refId = pRefId;
	}
	
	/**
	 * Function send message to add or update order
	 */
	public void addOrUpdateOrder(){
		try{
			Log.print("HKSMultiOrderTxn.addOrUpdateOrder() : START", Log.ACCESS_LOG);
			// set params parameter
			Hashtable lvParamMap = new Hashtable();
			lvParamMap.put(TagName.CLIENTID, mvClientID);
			lvParamMap.put(TagName.BS, mvBS);
			lvParamMap.put(TagName.INSTRUMENTID, mvStockCode);
			lvParamMap.put(TagName.MARKETID, mvMarketID);
			lvParamMap.put(TagName.QUANTITY, mvQuantity);
			lvParamMap.put(TagName.ORDERTYPE, mvOrderTypeValue);
			lvParamMap.put(TagName.PRICE, mvPrice);
			//lvParamMap.put(TagName.STOPPRICE, mvStopPrice);
			
			// convert goodtill date
			if (mvGoodTillDate != null && mvGoodTillDate.trim().length() > 0) {
				mvGoodTillDate = TextFormatter.getFormatedDate(mvGoodTillDate,
					"yyyy-MM-dd", IMain.getProperty("dateFormat"));
			} else {
				mvGoodTillDate = "";
			}
			
			lvParamMap.put(TagName.GOODTILLDATE, mvGoodTillDate);
			
			lvParamMap.put(TagName.GROSSAMT, mvGrossAmt);
			lvParamMap.put(TagName.BANKID, (mvBankID != null && mvBankID.trim().length() >0) ? mvBankID.trim(): "");
			lvParamMap.put(TagName.BANKACID, (mvBankACID != null && mvBankACID.trim().length() >0) ? mvBankACID.trim(): "");
			lvParamMap.put(TagName.REFID, (refId != null && refId.trim().length() >0) ? refId.trim(): "");
			
			if (TPErrorHandling.TP_NORMAL == super.process("HKSAddOrderToMultiGroup", lvParamMap)){
				setMvResult(mvReturnNode.getChildNode(TagName.RESULT).getValue());
				mvLatestId = mvReturnNode.getChildNode(TagName.REFID).getValue();
			}
			
		} catch(Exception ex) {
			Log.print("HKSMultiOrderTxn.addOrUpdateOrder() : ERROR: " + ex.toString(), Log.ERROR_LOG);
			mvResult = "FAILED";
			
		} finally {
			Log.print("HKSMultiOrderTxn.addOrUpdateOrder() : END", Log.ACCESS_LOG);
		}
	}
	
	/**
	 * Function send message to remove selected order
	 */
	public void removeSelectedOrders(){
		try{
			Log.print("HKSMultiOrderTxn.removeSelectedOrders() : START", Log.ACCESS_LOG);
			// set params parameter
			Hashtable lvParamMap = new Hashtable();
			lvParamMap.put(TagName.CLIENTID, mvClientID);
			lvParamMap.put("SELECTEDORDERID", mvSelIdList);
			
			if (TPErrorHandling.TP_NORMAL == super.process("HKSRemoveSelectedOrdersFromMultiGroup", lvParamMap)){
				setMvResult(mvReturnNode.getChildNode(TagName.RESULT).getValue());
			}
			
		} catch(Exception ex) {
			Log.print("HKSMultiOrderTxn.removeSelectedOrdersr() : ERROR: " + ex.toString(), Log.ERROR_LOG);
			mvResult = "FAILED";
			
		} finally {
			Log.print("HKSMultiOrderTxn.removeSelectedOrders() : END", Log.ACCESS_LOG);
		}
	}
	

	public List<HKSMultiOrderBean> getMultiOrderList() {
		List<HKSMultiOrderBean> result = new ArrayList<HKSMultiOrderBean>();
		try{
			Log.print("HKSMultiOrderTxn.getMultiOrderList() : START", Log.ACCESS_LOG);
			// set params parameter
			Hashtable lvParamMap = new Hashtable();
			lvParamMap.put(TagName.CLIENTID, mvClientID);
			if (TPErrorHandling.TP_NORMAL == super.process("HKSGetMultiOrderList", lvParamMap)){
				if (mvReturnNode.getChildNode("TOTALRECORD") != null) {
					setMvTotalRecord(mvReturnNode.getChildNode("TOTALRECORD")
							.getValue());
				}
				
				IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList("ChildRow");
				// get order list
				for (int i = 0; i < lvRowList.size(); i++) {
					HKSMultiOrderBean order = new HKSMultiOrderBean();
					IMsgXMLNode lvRow = lvRowList.getNode(i);
					// client id
					order.setMvClientId(lvRow.getChildNode(TagName.CLIENTID).getValue());
					// buy or sell
					order.setMvBS(lvRow.getChildNode(TagName.BS).getValue());
					// stock code
					order.setMvStockCode(lvRow.getChildNode(TagName.INSTRUMENTID).getValue());
					// market id
					order.setMvMarketId(lvRow.getChildNode(TagName.MARKETID).getValue());
					// quantity
					order.setMvQty(lvRow.getChildNode(TagName.QUANTITY).getValue());
					// order type
					order.setMvOrderType(lvRow.getChildNode(TagName.ORDERTYPE).getValue());
					// price
					order.setMvPrice(lvRow.getChildNode(TagName.PRICE).getValue());
					// stop price
					order.setMvStopPrice(lvRow.getChildNode(TagName.STOPPRICE).getValue());
					// goodtilldate
					order.setMvGoodTillDate(lvRow.getChildNode(TagName.GOODTILLDATE).getValue());
					// bank id
					order.setMvBankId(lvRow.getChildNode(TagName.BANKID).getValue());
					// bankACID
					order.setMvBankACID(lvRow.getChildNode(TagName.BANKACID).getValue());
					// ref id
					order.setMvRefId(lvRow.getChildNode(TagName.REFID).getValue());
					
					// add to list
					result.add(order);
				}
			}
			
		} catch(Exception ex) {
			Log.print("HKSMultiOrderTxn.getMultiOrderList() : ERROR: " + ex.toString(), Log.ERROR_LOG);
			
		} finally {
			Log.print("HKSMultiOrderTxn.getMultiOrderList() : END", Log.ACCESS_LOG);
		}
		return result;
	}
	
	/**
	 * @return the mvBS
	 */
	public String getMvBS() {
		return mvBS;
	}
	/**
	 * @param mvBS the mvBS to set
	 */
	public void setMvBS(String mvBS) {
		this.mvBS = mvBS;
	}
	/**
	 * @return the mvStockCode
	 */
	public String getMvStockCode() {
		return mvStockCode;
	}
	/**
	 * @param mvStockCode the mvStockCode to set
	 */
	public void setMvStockCode(String mvStockCode) {
		this.mvStockCode = mvStockCode;
	}
	/**
	 * @return the mvQuantity
	 */
	public String getMvQuantity() {
		return mvQuantity;
	}
	/**
	 * @param mvQuantity the mvQuantity to set
	 */
	public void setMvQuantity(String mvQuantity) {
		this.mvQuantity = mvQuantity;
	}
	/**
	 * @return the mvOrderTypeValue
	 */
	public String getMvOrderTypeValue() {
		return mvOrderTypeValue;
	}
	/**
	 * @param mvOrderTypeValue the mvOrderTypeValue to set
	 */
	public void setMvOrderTypeValue(String mvOrderTypeValue) {
		this.mvOrderTypeValue = mvOrderTypeValue;
	}
	/**
	 * @return the mvPrice
	 */
	public String getMvPrice() {
		return mvPrice;
	}
	/**
	 * @param mvPrice the mvPrice to set
	 */
	public void setMvPrice(String mvPrice) {
		this.mvPrice = mvPrice;
	}
	/**
	 * @return the mvStopPrice
	 */
	public String getMvStopPrice() {
		return mvStopPrice;
	}
	/**
	 * @param mvStopPrice the mvStopPrice to set
	 */
	public void setMvStopPrice(String mvStopPrice) {
		this.mvStopPrice = mvStopPrice;
	}
	/**
	 * @return the mvGoodTillDate
	 */
	public String getMvGoodTillDate() {
		return mvGoodTillDate;
	}
	/**
	 * @param mvGoodTillDate the mvGoodTillDate to set
	 */
	public void setMvGoodTillDate(String mvGoodTillDate) {
		this.mvGoodTillDate = mvGoodTillDate;
	}
	/**
	 * @return the mvGrossAmt
	 */
	public String getMvGrossAmt() {
		return mvGrossAmt;
	}
	/**
	 * @param mvGrossAmt the mvGrossAmt to set
	 */
	public void setMvGrossAmt(String mvGrossAmt) {
		this.mvGrossAmt = mvGrossAmt;
	}
	/**
	 * @return the mvMarketID
	 */
	public String getMvMarketID() {
		return mvMarketID;
	}
	/**
	 * @param mvMarketID the mvMarketID to set
	 */
	public void setMvMarketID(String mvMarketID) {
		this.mvMarketID = mvMarketID;
	}
	/**
	 * @return the mvClientID
	 */
	public String getMvClientID() {
		return mvClientID;
	}
	/**
	 * @param mvClientID the mvClientID to set
	 */
	public void setMvClientID(String mvClientID) {
		this.mvClientID = mvClientID;
	}
	/**
	 * @return the mvBankID
	 */
	public String getMvBankID() {
		return mvBankID;
	}
	/**
	 * @param mvBankID the mvBankID to set
	 */
	public void setMvBankID(String mvBankID) {
		this.mvBankID = mvBankID;
	}
	/**
	 * @return the mvBankACID
	 */
	public String getMvBankACID() {
		return mvBankACID;
	}
	/**
	 * @param mvBankACID the mvBankACID to set
	 */
	public void setMvBankACID(String mvBankACID) {
		this.mvBankACID = mvBankACID;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getRefId() {
		return refId;
	}

	public void setActivationDate(String activationDate) {
		this.activationDate = activationDate;
	}

	public String getActivationDate() {
		return activationDate;
	}

	public void setMvResult(String mvResult) {
		this.mvResult = mvResult;
	}

	public String getMvResult() {
		return mvResult;
	}

	public void setMvTotalRecord(String mvTotalRecord) {
		this.mvTotalRecord = mvTotalRecord;
	}

	public String getMvTotalRecord() {
		return mvTotalRecord;
	}

	public void setMvLatestId(String mvLatestId) {
		this.mvLatestId = mvLatestId;
	}

	public String getMvLatestId() {
		return mvLatestId;
	}

	public void setMvSelIdList(String mvSelIdList) {
		this.mvSelIdList = mvSelIdList;
	}

	public String getMvSelIdList() {
		return mvSelIdList;
	}
}
