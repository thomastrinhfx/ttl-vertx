package com.ttl.old.itrade.hks.bean.plugin;

public class HKSMarginExtensionBean {

	private String name;
	private String contract;
	private String activedate;
	private String expiredate;
	private String cash;
	private String loan;
	private String debitint;
	private String newexpire;
	private String status;
	private String serial;
	private int month;
	private int daysubtract;
	private int daysum;

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public int getDaysubtract() {
		return daysubtract;
	}

	public void setDaysubtract(int daysubtract) {
		this.daysubtract = daysubtract;
	}

	public int getDaysum() {
		return daysum;
	}

	public void setDaysum(int daysum) {
		this.daysum = daysum;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	private String currentdate;
	
	public String getCurrentdate() {
		return currentdate;
	}

	public void setCurrentdate(String currentdate) {
		this.currentdate = currentdate;
	}

	public String getNewexpire() {
		return newexpire;
	}

	public void setNewexpire(String newexpire) {
		this.newexpire = newexpire;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContract() {
		return contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}

	public String getActivedate() {
		return activedate;
	}

	public void setActivedate(String activedate) {
		this.activedate = activedate;
	}

	public String getExpiredate() {
		return expiredate;
	}

	public void setExpiredate(String expiredate) {
		this.expiredate = expiredate;
	}

	public String getCash() {
		return cash;
	}

	public void setCash(String cash) {
		this.cash = cash;
	}

	public String getLoan() {
		return loan;
	}

	public void setLoan(String loan) {
		this.loan = loan;
	}

	public String getDebitint() {
		return debitint;
	}

	public void setDebitint(String debitint) {
		this.debitint = debitint;
	}

}
