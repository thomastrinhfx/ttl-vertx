package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.ttl.old.itrade.util.Presentation;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * This class processes the script option submit
 * @author not attributable
 *
 */
public class HKSScripOptionSubmitTxn
{
    private String mvEntitlementId;
    private String mvClientId;
    private String mvTradingAccSeq;
    private String mvRemainingAmount;
    private String mvIsCalRemainingAmount;
    private String mvScripOptionQty;
    private String mvStatus;
    private String mvStatusDescription;
    private String mvIsConfirm;
    private String mvIsCheckPassword;
    private String mvPassword;
    private String mvDividendAmount;
    private String mvMaxDividendAmount;
    private String mvConversionPrice;
    private String mvCurrencyID;
    private String mvRegisteredQty;
    private String mvBookCloseQty;
    private String mvBookCloseDate;
    private String mvDividendDeliver;
    private String mvDividendRatioPer;
    private String mvSettleCurrencyID;
    private int mvReturnCode;
    private String mvErrorCode;
    private String mvErrorMessage;
    private String mvLastModifiedTime;

    TPErrorHandling tpError;
    Presentation presentation = null;

    /**
     * Default constructor for HKSScripOptionSubmitTxn class
     */
    public HKSScripOptionSubmitTxn(Presentation pPresentation)
    {
        tpError = new TPErrorHandling();
        presentation = pPresentation;
    }

    /**
     * Default constructor for HKSScripOptionSubmitTxn class
     * @param pPresentation the Presentation class
     * @param pClientId the Client ID
     */
    public HKSScripOptionSubmitTxn(Presentation pPresentation, String pClientId)
    {
        this(pPresentation);
        setClientId(pClientId);
    }
    /**
     * This method process the script option submit
     * @param pPresentation the Presentation class
     */
    public void process(Presentation pPresentation)
    {
        try
        {
            Hashtable		lvTxnMap = new Hashtable();

            IMsgXMLNode		lvRetNode;
            //TPManager		ivTPManager = ITradeServlet.getTPManager();
            TPManager ivTPManager = ITradeServlet.getTPManager(RequestName.HKSScripOptionSubmitRequest);
            TPBaseRequest       lvScripOptionSubmitRequest = ivTPManager.getRequest(RequestName.HKSScripOptionSubmitRequest);

            lvTxnMap.put(TagName.CLIENTID, getClientId());
            lvTxnMap.put(TagName.ENTITLEMENTID, getEntitlementId());
            lvTxnMap.put("SCRIPOPTIONQTY", getScripOptionQty());
            lvTxnMap.put("PASSWORD", getPassword());
            lvTxnMap.put("ISCONFIRM", getIsConfirm());
            lvTxnMap.put("ISCHECKPASSWORD", getIsCheckPassword());
            lvTxnMap.put("ISCALREMAININGAMOUNT", getIsCalRemainingAmount());
            lvTxnMap.put("TRADINGACCSEQ", getTradingAccSeq());
            lvTxnMap.put("MAXDIVIDENDAMOUNT", getMaxDividendAmount());
            lvTxnMap.put("DIVIDENDAMOUNT", getDividendAmount());
            lvTxnMap.put("CONVERSIONPRICE", getConversionPrice());
            lvTxnMap.put(TagName.CURRENCYID, getCurrencyID());
            lvTxnMap.put(TagName.REGISTEREDQTY, getRegisteredQty());
            lvTxnMap.put(TagName.BOOKCLOSEQTY, getBookCloseQty());
            lvTxnMap.put(TagName.BOOKCLOSEDATE, getBookCloseDate());
            lvTxnMap.put("DIVIDENDDELIVER", getDividendDeliver());
            lvTxnMap.put("DIVIDENDRATIOPER", getDividendRatioPer());
            lvTxnMap.put(TagName.SETTLECURRENCYID, getSettleCurrencyID());
            lvTxnMap.put(TagName.STATUS, getStatus());
            lvTxnMap.put("STATUSDESCRIPTION", getStatusDescription());

            lvRetNode = lvScripOptionSubmitRequest.send(lvTxnMap);

            setReturnCode(tpError.checkError(lvRetNode));
            if (mvReturnCode != TPErrorHandling.TP_NORMAL)
            {
                setErrorMessage(tpError.getErrDesc());
                if (mvReturnCode == TPErrorHandling.TP_APP_ERR)
                {
                    setErrorCode(tpError.getErrCode());
                }
            }
            else
            {
                if(lvRetNode.getChildNode(TagName.LOOP).getChildNode(TagName.LOOP_ELEMENT).getChildNode(TagName.LASTMODIFIEDTIME)!=null)
                {
                        setLastModifiedTime(lvRetNode.getChildNode(TagName.LOOP).getChildNode(TagName.LOOP_ELEMENT).getChildNode(TagName.LASTMODIFIEDTIME).getValue());
                }

                if (lvRetNode.getChildNode(TagName.LOOP).getChildNode(TagName.LOOP_ELEMENT).getChildNode("REMAININGAMOUNT")==null)
                    return;




                setRemainingAmount(lvRetNode.getChildNode(TagName.LOOP).getChildNode(TagName.LOOP_ELEMENT).getChildNode("REMAININGAMOUNT").getValue());

            }
        }
        catch (Exception e)
        {
            Log.println( e , Log.ERROR_LOG);
        }
    }
    /**
     * Get method for entitlement id
     * @return entitlement id
     */
    public String getEntitlementId()
    {
        return mvEntitlementId;
    }
    /**
     * Set method for entitlement id
     * @param pEntitlementId the entitlement id
     */
    public void setEntitlementId(String pEntitlementId)
    {
        mvEntitlementId = pEntitlementId;
    }
    /**
     * Get method for script option quantity
     * @return script option quantity
     */
    public String getScripOptionQty()
    {
            return mvScripOptionQty;
    }
    /**
     * Set method for script option quantity
     * @param pScripOptionQty the script option quantity
     */
    public void setScripOptionQty(String pScripOptionQty)
    {
            mvScripOptionQty = pScripOptionQty;
    }
    /**
     * Get method for is check client password
     * @return is check client password
     */
    public String getIsCheckPassword()
    {
            return mvIsCheckPassword;
    }
    /**
     * Set method for is check client password
     * @param pIsCheckPassword the is check client password
     */
    public void setIsCheckPassword(String pIsCheckPassword)
    {
            mvIsCheckPassword = pIsCheckPassword;
    }
    /**
     * Get method for is confirm
     * @return confirm
     */
    public String getIsConfirm()
    {
            return mvIsConfirm;
    }
    /**
     * Set method for is confirm
     * @param pIsConfirm the confirm
     */
    public void setIsConfirm(String pIsConfirm)
    {
            mvIsConfirm = pIsConfirm;
    }
    /**
     * Get method for is cancel remaining amount
     * @return cancel remaining amount
     */
    public String getIsCalRemainingAmount()
    {
            return mvIsCalRemainingAmount;
    }

    /**
     * Set method for is cancel remaining amount
     * @param pIsCalRemainingAmount the is cancel remaining amount
     */
    public void setIsCalRemainingAmount(String pIsCalRemainingAmount)
    {
            mvIsCalRemainingAmount = pIsCalRemainingAmount;
    }
    /**
     * Get method for remaining amount
     * @return remaining amount
     */
    public String getRemainingAmount()
    {
        return mvRemainingAmount;
    }

    /**
     * Set method for remaining amount
     * @param pRemainingAmount the remaining amount
     */
    public void setRemainingAmount(String pRemainingAmount)
    {
        mvRemainingAmount = pRemainingAmount;
    }
    /**
     * Get method for last modified time
     * @return last modified time
     */
    public String getLastModifiedTime()
    {
            return mvLastModifiedTime;
    }
    /**
     * Set method for last modified time
     * @param pLastModifiedTime the last modified time
     */
    public void setLastModifiedTime(String pLastModifiedTime)
    {
            mvLastModifiedTime = pLastModifiedTime;
    }
    /**
     * Get method for client id
     * @return client id
     */
    public String getClientId()
    {
        return mvClientId;
    }
    /**
     * Set method for client id
     * @param pClientId the client id
     */
    public void setClientId(String pClientId)
    {
        mvClientId = pClientId;
    }
    /**
     * Get method for trading account sequence
     * @return trading account sequence
     */
    public String getTradingAccSeq()
    {
            return mvTradingAccSeq;
    }
    /**
     * Set method for trading account sequence
     * @param pTradingAccSeq the trading account sequence
     */
    public void setTradingAccSeq(String pTradingAccSeq)
    {
            mvTradingAccSeq = pTradingAccSeq;
    }
    /**
     * Get method for client password
     * @return client password
     */
    public String getPassword()
    {
            return mvPassword;
    }
    /**
     * Set method for client password
     * @param pPassword the client password
     */
    public void setPassword(String pPassword)
    {
            mvPassword = pPassword;
    }
    /**
     * Get method for conversion price
     * @return conversion price
     */
    public String getConversionPrice()
    {
            return mvConversionPrice;
    }
    /**
     * Set method for conversion price
     * @param pConversionPrice the conversion price
     */
    public void setConversionPrice(String pConversionPrice)
    {
            mvConversionPrice = pConversionPrice;
    }
    /**
     * Get method for dividend amount 
     * @return dividend amount 
     */
    public String getDividendAmount()
    {
            return mvDividendAmount;
    }
    /**
     * Set method for dividend amount 
     * @param pDividendAmount the dividend amount 
     */
    public void setDividendAmount(String pDividendAmount)
    {
            mvDividendAmount = pDividendAmount;
    }
    /**
     * Get method for currency id
     * @return currency id
     */
    public String getCurrencyID()
    {
            return mvCurrencyID;
    }
    /**
     * Set method for currency id
     * @param pCurrencyID the currency id
     */
    public void setCurrencyID(String pCurrencyID)
    {
            mvCurrencyID = pCurrencyID;
    }
    /**
     * Get method for book close quantity
     * @return book close quantity
     */
    public String getBookCloseQty()
    {
            return mvBookCloseQty;
    }
    /**
     * Set method for book close quantity
     * @param pBookCloseQty the book close quantity
     */
    public void setBookCloseQty(String pBookCloseQty)
    {
            mvBookCloseQty = pBookCloseQty;
    }
    /**
     * Get method for registered quantity
     * @return registered quantity
     */
    public String getRegisteredQty()
    {
            return mvRegisteredQty;
    }
    /**
     * Set method for registered quantity
     * @param pRegisteredQty the registered quantity
     */
    public void setRegisteredQty(String pRegisteredQty)
    {
            mvRegisteredQty = pRegisteredQty;
    }
    /**
     * Get method for book close date
     * @return book close date
     */
    public String getBookCloseDate()
    {
            return mvBookCloseDate;
    }
    /**
     * Set method for book close date
     * @param pBookCloseDate the book close date
     */
    public void setBookCloseDate(String pBookCloseDate)
    {
            mvBookCloseDate = pBookCloseDate;
    }
    /**
     * Get method for max dividend amount
     * @return max dividend amount
     */
    public String getMaxDividendAmount()
    {
            return mvMaxDividendAmount;
    }
    /**
     * Set method for max dividend amount
     * @param pMaxDividendAmount the max dividend amount
     */
    public void setMaxDividendAmount(String pMaxDividendAmount)
    {
            mvMaxDividendAmount = pMaxDividendAmount;
    }
    /**
     * Get method for dividend deliver
     * @return dividend deliver
     */
    public String getDividendDeliver()
    {
            return mvDividendDeliver;
    }
    /**
     * Set method for dividend deliver
     * @param pDividendDeliver the dividend deliver
     */
    public void setDividendDeliver(String pDividendDeliver)
    {
            mvDividendDeliver = pDividendDeliver;
    }
    /**
     * Get method for dividend ratio per
     * @return dividend ratio per
     */
    public String getDividendRatioPer()
    {
            return mvDividendRatioPer;
    }
    /**
     * Set method for dividend ratio per
     * @aram pDividendRatioPer the dividend ratio per
     */
    public void setDividendRatioPer(String pDividendRatioPer)
    {
            mvDividendRatioPer = pDividendRatioPer;
    }
    /**
     * Get method for settle currency id
     * @return settle currency id
     */
    public String getSettleCurrencyID()
    {
            return mvSettleCurrencyID;
    }
    /**
     * Set method for settle currency id
     * @param pSettleCurrencyID the settle currency id
     */
    public void setSettleCurrencyID(String pSettleCurrencyID)
    {
            mvSettleCurrencyID = pSettleCurrencyID;
    }
    /**
     * Get method for status
     * @return status
     */
    public String getStatus()
    {
            return mvStatus;
    }
    /**
     * Set method for status
     * @param pStatus the status
     */
    public void setStatus(String pStatus)
    {
            mvStatus = pStatus;
    }
    /**
     * Get method for status description
     * @return status description
     */
    public String getStatusDescription()
    {
            return mvStatusDescription;
    }
    /**
     * Set method for status description
     * @param pStatusDescription the status description
     */
    public void setStatusDescription(String pStatusDescription)
    {
            mvStatusDescription = pStatusDescription;
    }

    /**
     * Get method for return system code
     * @return return system code
     */
    public int getReturnCode()
    {
        return mvReturnCode;
    }
    /**
     * Set method for return system code
     * @param pReturnCode the return system code
     */
    public void setReturnCode(int pReturnCode)
    {
        mvReturnCode = pReturnCode;
    }
    /**
     * Get method for error system code
     * @return error system code
     */
    public String getErrorCode()
    {
        return mvErrorCode;
    }
    /**
     * Set method for error system code
     * @param pErrorCode the error system code
     */
    public void setErrorCode(String pErrorCode)
    {
        mvErrorCode = pErrorCode;
    }
    /**
     * Get method for error system message
     * @return error system message
     */
    public String getErrorMessage()
    {
        return mvErrorMessage;
    }
    /**
     * Set method for error system message
     * @param pErrorMessage the error system message
     */
    public void setErrorMessage(String pErrorMessage)
    {
        mvErrorMessage = pErrorMessage;
    }
    /**
     * This method for localized error system message
     * @param pErrorCode the error system code
     * @param pDefaultMesg the default message
     * @return localized error system message
     */
   public String getLocalizedErrorMessage(String pErrorCode, String pDefaultMesg)
   {
      String lvRetMessage;

      try
      {
         if (pErrorCode.equals("0") || pErrorCode.equals("999"))
         {
            lvRetMessage = pDefaultMesg;
         }
         else
         {
        	 lvRetMessage = (String) presentation.getErrorCodeBundle().getString(pErrorCode);
         }
      }
      catch (Exception ex)
      {
         Log.println("Presentation._showPage: missing error mapping for " + pErrorCode + ", Default = " + pDefaultMesg, Log.ERROR_LOG);

         lvRetMessage = pDefaultMesg;
      }
      return lvRetMessage;
   }
}
