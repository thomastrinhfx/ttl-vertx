package com.ttl.old.itrade.hks.txn;

import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.tag.ITagXsfTagName;
import com.systekit.winvest.hks.config.mapping.TagName;
import com.ttl.old.itrade.tp.TPErrorHandling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

public class HKSAddOrRemoveStockTxn extends BaseTxn {

    private String mvClientID;
    private String mvStockID;
    private String mvMarketID;
    private String mvOperation;
    private String mvCategory;

    private String mvReturnErrorCode;
    private String mvRetunErrorMsg;
    private String mvInstrumentList;
    private String mvMarketList;

    private String mvTargetPrc;
    private String mvStopProfitPrc;
    private String mvStopLostPrc;
    private static final String WATCHLIST_OPERATION = "1";
    private static final String WARNINGLIST_OPERATION = "2";

    private List mvWarningList = new ArrayList();

    public HKSAddOrRemoveStockTxn(String pClientID, String pStockID, String pMarketID, String pOperation, String pCategory) {
        this.mvClientID = pClientID;
        this.mvStockID = pStockID;
        this.mvMarketID = pMarketID;
        this.mvOperation = pOperation;
        this.mvCategory = pCategory;
    }

    public HKSAddOrRemoveStockTxn(String pClientID, String pStockID, String pMarketID, String pOperation, String pCategory, String pTargetPrc, String pStopProfit, String pStopLost) {
        this.mvClientID = pClientID;
        this.mvStockID = pStockID;
        this.mvMarketID = pMarketID;
        this.mvOperation = pOperation;
        this.mvCategory = pCategory;
        this.mvTargetPrc = pTargetPrc;
        this.mvStopProfitPrc = pStopProfit;
        this.mvStopLostPrc = pStopLost;
    }

    @SuppressWarnings("unchecked")
    public void process() {
        try {
//			Log.println("[ HKSAddOrRemoveStockTxn.process() start ["
//					+ getMvClientID() + "]", Log.ACCESS_LOG);
            Hashtable lvTxnMap = new Hashtable();
            lvTxnMap.put(TagName.CLIENTID, getMvClientID());
            lvTxnMap.put(TagName.INSTRUMENTID, getMvStockID());
            lvTxnMap.put(TagName.MARKETID, getMvMarketID());
            lvTxnMap.put(TagName.OPERATION, getMvOperation());
            lvTxnMap.put("CATEGORY", getMvCategory());
            if (mvCategory.equals(WARNINGLIST_OPERATION) && mvOperation.equals("MODIFY")) {
                lvTxnMap.put("TARGETBUYPRICE", mvTargetPrc == null ? "null" : mvTargetPrc);
                lvTxnMap.put("STOPLOSTPRICE", mvStopLostPrc == null ? "null" : mvStopLostPrc);
                lvTxnMap.put("STOPPROFITPRICE", mvStopProfitPrc == null ? "null" : mvStopProfitPrc);
            }
            if (TPErrorHandling.TP_NORMAL == super.process(
                    "HKSAddRemoveStockWatchListRequest", lvTxnMap)) {
                setMvReturnErrorCode(mvReturnNode.getChildNode("ERRORCODE")
                        .getValue());
                setMvRetunErrorMsg(mvReturnNode.getChildNode("ERRORMSG")
                        .getValue());
                // If operation with watch list
                if (mvCategory.equals(WATCHLIST_OPERATION)) {
                    setMvInstrumentList(mvReturnNode.getChildNode("INSTRUMENTLIST")
                            .getValue());
                    setMvMarketList(mvReturnNode.getChildNode("MARKETLIST")
                            .getValue());
                    setMvTargetPriceList(mvReturnNode.getChildNode("TARGETPRICE")
                            .getValue());
                }
                // If query warning list
                if (mvCategory.equals(WARNINGLIST_OPERATION) && mvOperation.equals("QUERY")) {
                    //mvReturnNode.getChildNode(arg0)

                    IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList(ITagXsfTagName.CHILD_ROW);
                    for (int i = 0; i < lvRowList.size(); i++) {
                        IMsgXMLNode lvNode = lvRowList.getNode(i);
                        HashMap lvModel = new HashMap();
                        lvModel.put(TagName.INSTRUMENTID, lvNode.getChildNode(TagName.INSTRUMENTID).getValue());
                        lvModel.put(TagName.MARKETID, lvNode.getChildNode(TagName.MARKETID).getValue());

                        lvModel.put("TARGETBUYPRICE", lvNode.getChildNode("TARGETBUYPRICE").getValue());
                        lvModel.put("STOPLOSTPRICE", lvNode.getChildNode("STOPLOSTPRICE").getValue());
                        lvModel.put("STOPPROFITPRICE", lvNode.getChildNode("STOPPROFITPRICE").getValue());
                        getMvWarningList().add(lvModel);
                    }
                }

            } else {
                //Log.print("Unknown Error when exec HKSAddOrRemoveStockTxn.process", Log.ALERT_LOG);
            }
            //Log.println("[ HKSAddOrRemoveStockTxn.process() ends ["
            //		+ getMvClientID() + "]", Log.ACCESS_LOG);
        } catch (Exception e) {
            //Log.print(e, Log.ERROR_LOG);
        }

    }

    /**
     * @param mvClientID the mvClientID to set
     */
    public void setMvClientID(String mvClientID) {
        this.mvClientID = mvClientID;
    }

    /**
     * @return the mvClientID
     */
    public String getMvClientID() {
        return mvClientID;
    }

    /**
     * @param mvStockID the mvStockID to set
     */
    public void setMvStockID(String mvStockID) {
        this.mvStockID = mvStockID;
    }

    /**
     * @return the mvStockID
     */
    public String getMvStockID() {
        return mvStockID;
    }

    /**
     * @param mvMarketID the mvMarketID to set
     */
    public void setMvMarketID(String mvMarketID) {
        this.mvMarketID = mvMarketID;
    }

    /**
     * @return the mvMarketID
     */
    public String getMvMarketID() {
        return mvMarketID;
    }

    /**
     * @param mvOperation the mvOperation to set
     */
    public void setMvOperation(String mvOperation) {
        this.mvOperation = mvOperation;
    }

    /**
     * @return the mvOperation
     */
    public String getMvOperation() {
        return mvOperation;
    }

    /**
     * @param mvMarketList the mvMarketList to set
     */
    public void setMvMarketList(String mvMarketList) {
        this.mvMarketList = mvMarketList;
    }

    /**
     * @return the mvMarketList
     */
    public String getMvMarketList() {
        return mvMarketList;
    }

    /**
     * @param mvInstrumentList the mvInstrumentList to set
     */
    public void setMvInstrumentList(String mvInstrumentList) {
        this.mvInstrumentList = mvInstrumentList;
    }

    /**
     * @return the mvInstrumentList
     */
    public String getMvInstrumentList() {
        return mvInstrumentList;
    }

    /**
     * @param mvRetunErrorMsg the mvRetunErrorMsg to set
     */
    public void setMvRetunErrorMsg(String mvRetunErrorMsg) {
        this.mvRetunErrorMsg = mvRetunErrorMsg;
    }

    /**
     * @return the mvRetunErrorMsg
     */
    public String getMvRetunErrorMsg() {
        return mvRetunErrorMsg;
    }

    /**
     * @param mvReturnErrorCode the mvReturnErrorCode to set
     */
    public void setMvReturnErrorCode(String mvReturnErrorCode) {
        this.mvReturnErrorCode = mvReturnErrorCode;
    }

    /**
     * @return the mvReturnErrorCode
     */
    public String getMvReturnErrorCode() {
        return mvReturnErrorCode;
    }

    /**
     * @param mvCategory the mvCategory to set
     */
    public void setMvCategory(String mvCategory) {
        this.mvCategory = mvCategory;
    }

    /**
     * @return the mvCategory
     */
    public String getMvCategory() {
        return mvCategory;
    }

    /**
     * @param mvWarningList the mvWarningList to set
     */
    public void setMvWarningList(List mvWarningList) {
        this.mvWarningList = mvWarningList;
    }

    /**
     * @return the mvWarningList
     */
    public List getMvWarningList() {
        return mvWarningList;
    }

    public void setMvTargetPriceList(String mvTargetPriceList) {
        this.mvTargetPriceList = mvTargetPriceList;
    }

    public String getMvTargetPriceList() {
        return mvTargetPriceList;
    }

    private String mvTargetPriceList;
}
