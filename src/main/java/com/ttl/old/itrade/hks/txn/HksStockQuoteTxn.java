package com.ttl.old.itrade.hks.txn;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.MessageFormat;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

import com.ttl.old.itrade.hks.util.HKSInstrument;
import com.ttl.old.itrade.hks.util.URLUTF8Encoder;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.ErrorCode;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.err.errParsingException;
import com.systekit.winvest.hks.util.Utils;

/**
 * This class process the stock quote
 * @author not attributable
 *
 */
public class HksStockQuoteTxn extends BaseTxn
{
	private String mvMarketId;
	private String mvInstrumentId;
	private String mvLanguage;
	private String mvMemberId;
	private String mvSymbol;
	private String mvKeywrod;
	private String mvSwitch;
	private String mvCategory;
	private String mvMax;
	private static Properties _pIni;
	private String mvResult = "";
	private String mvExternalURL;
	private ErrorCode mvErrorCode = null;
	private int mvStockReturnCode = 0;
	private URLUTF8Encoder UrlEncoder;
    protected Vector svUsingStockCodeFormatMarket;
	//BEGIN Task #:- al00001 albert lai 20080828
	private static final int PASS = 0;
	private static final int FAIL = 1;
	//END Task #:- al00001 albert lai 20080828
	/**
	 * Constructor for the stock quote TXN
	 * @param pMarketId  the For Hong Kong Stock, use HKEX
	 * @param pInstrumentId the Instrument ID
	 */
	//BEGIN Task #:- al00001 albert lai 20080828
	static public void setProperties(Properties p){
    }
	//END Task #:- al00001 albert lai 20080828
	/*	public HksStockQuoteTxn(String pMarketId, String pInstrumentId)
		{
			//BEGIN Task al00001 albert lai 20080828
			//Make user friendly
				int lvAddZero = 5 - pInstrumentId.length();
				String lvTempInstrumentId = "";
				for(int lvI = 0; lvI < lvAddZero; lvI++){
					lvTempInstrumentId += "0";
				}
				pInstrumentId = lvTempInstrumentId + pInstrumentId;
			if(pMarketId == null) this.mvInstrumentId = "HKEX";
			//End user friendly
			//END Task al00001 albert lai 20080828
			this.mvMarketId = pMarketId;
			this.mvInstrumentId = pInstrumentId;
		}*/

		// BEGIN - TASK#: CL00026 - Charlie Liu 20081020
		
	/**
	 * Constructor for the stock quote TXN
	 * @param pLanguage the user locale
	 * @param pInstrumentId the instrument id
	 */
	public HksStockQuoteTxn(String pLanguage, String pInstrumentId)
	{
			int lvAddZero = 5 - pInstrumentId.length();
			String lvTempInstrumentId = "";
			for(int lvI = 0; lvI < lvAddZero; lvI++){
				lvTempInstrumentId += "0";
			}
		pInstrumentId = lvTempInstrumentId + pInstrumentId;
		this.mvLanguage = pLanguage;
		this.mvInstrumentId = pInstrumentId;
		this.mvSwitch = "1";
	}
	/**
	 * Constructor for the stock quote TXN
	 * @param pLanguage the user locale
	 * @param pSymbol the symbol
	 * @param pAastockIndex the stork index
	 */
	public HksStockQuoteTxn(String pLanguage, String pSymbol, boolean pAastockIndex)
	{
		this.mvLanguage = pLanguage;
		this.mvSymbol = pSymbol;
		this.mvSwitch = "2";
	}
	/**
	 * Constructor for the stock quote TXN
	 * @param pLanguage the user locale
	 * @param pMarketId the market id
	 * @param pCategory the category
	 * @param pMax the max
	 */
	public HksStockQuoteTxn(String pLanguage, String pMarketId, String pCategory, String pMax)
	{
		this.mvMarketId = pMarketId;
		this.mvLanguage = pLanguage;
		this.mvCategory = pCategory;
		this.mvMax = pMax;
		this.mvSwitch = "3";
	}
	/**
	 * Constructor for the stock quote TXN
	 * @param pLanguage the user locale
	 * @param pInstrumentId the instrument id
	 * @param pMemberId the member id
	 */
	public HksStockQuoteTxn(String pLanguage, String pInstrumentId, String pMemberId)
	{
			int lvAddZero = 5 - pInstrumentId.length();
			String lvTempInstrumentId = "";
			for(int lvI = 0; lvI < lvAddZero; lvI++){
				lvTempInstrumentId += "0";
			}
			pInstrumentId = lvTempInstrumentId + pInstrumentId;

		this.mvLanguage = pLanguage;
		this.mvInstrumentId = pInstrumentId;
		this.mvMemberId = pMemberId;
		this.mvSwitch = "4";
	}
	/**
	 * Constructor for the stock quote TXN
	 * @param pKeyword the keyword
	 */
	public HksStockQuoteTxn(String pKeyword)
	{
		pKeyword = UrlEncoder.encode(pKeyword);
		this.mvKeywrod = pKeyword;
		this.mvSwitch = "5";
	}
	//END - TASK# CL00026
	
	/**
     * process the stock quote
	 * @throws IOException
	 * @throws errParsingException
     *
     */
    public void process()
    {
    	//BEGIN Task al00001 albert lai 20080828
    	BufferedReader lvIn;
    	try{
    		//From Walter
    		//BEGIN TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
    		String lvUsingStockCodeFormatMarket = Utils.isNullStr(IMain.getProperty("UsingStockCodeFormatMarket")) ? "" : IMain.getProperty("UsingStockCodeFormatMarket") ;
    		//END TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
            if(!Utils.isNullStr(lvUsingStockCodeFormatMarket))
            {
                    svUsingStockCodeFormatMarket = new Vector();
                    StringTokenizer lvTokenList = new StringTokenizer(lvUsingStockCodeFormatMarket, ",");
                    while (lvTokenList.hasMoreTokens())
                    {
                            String lvMarketID = lvTokenList.nextToken();
                            if (lvMarketID != null)
                            {
                                    svUsingStockCodeFormatMarket.add(lvMarketID.trim());
                            }
                    }
            }

            //End Walter
            
        // BEGIN - TASK#: CL00026 - Charlie Liu 20081013 

    	try{
    		if (!Utils.isNullStr(mvSwitch)){
            if(mvSwitch.equalsIgnoreCase("1")){
        		  checkInstrument();
        		//BEGIN TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
        		  MessageFormat lvMessageFormat = new MessageFormat(Utils.isNullStr( IMain.getProperty("aastock.webserives.url")) ? "http://product1.aastocks.com/xml/datafeed/getquotelevel1.ashx?broker=BANKCOMM&real=0&language={0}&encoding=utf8&format=1&symbol={1}" : IMain.getProperty("aastock.webserives.url"));
            		String[] lvMessageFormatArg = {mvLanguage, mvInstrumentId};
    	    	    mvExternalURL = lvMessageFormat.format(lvMessageFormatArg);
            }
            else if (mvSwitch.equalsIgnoreCase("2")){
            		//MessageFormat lvMessageFormat = new MessageFormat(Utils.isNullStr( _pIni.getProperty("aastock.webserives.url.index")) ? "http://product1.aastocks.com/xml/datafeed/getindex.ashx?broker=BANKCOMM&real=0&language={0}&encoding=utf8&symbol={1}&format=1" : _pIni.getProperty("aastock.webserives.url.index"));
            		MessageFormat lvMessageFormat = new MessageFormat(Utils.isNullStr( IMain.getProperty("aastock.webserives.url.index")) ? "http://product1.aastocks.com/xml/datafeed/getindex.ashx?broker=BANKCOMM&real=0&language={0}&encoding=utf8&symbol={1}&format=1" : IMain.getProperty("aastock.webserives.url.index"));
            		String[] lvMessageFormatArg = {mvLanguage, mvSymbol};
    	    	    mvExternalURL = lvMessageFormat.format(lvMessageFormatArg);
            }
            else if (mvSwitch.equalsIgnoreCase("3")){
        		//MessageFormat lvMessageFormat = new MessageFormat(Utils.isNullStr( _pIni.getProperty("aastock.webserives.url.delayedtoprank")) ? "http://product1.aastocks.com/xml/datafeed/gettoprank.ashx?broker=BANKCOMM&language={0}&encoding=utf8&marketid={1}&category={2}&max={3}&format=1&real=0" : _pIni.getProperty("aastock.webserives.url.delayedtoprank"));
            	MessageFormat lvMessageFormat = new MessageFormat(Utils.isNullStr( IMain.getProperty("aastock.webserives.url.delayedtoprank")) ? "http://product1.aastocks.com/xml/datafeed/gettoprank.ashx?broker=BANKCOMM&language={0}&encoding=utf8&marketid={1}&category={2}&max={3}&format=1&real=0" : IMain.getProperty("aastock.webserives.url.delayedtoprank"));
        		String[] lvMessageFormatArg = {mvLanguage, mvMarketId, mvCategory, mvMax};
	    	    mvExternalURL = lvMessageFormat.format(lvMessageFormatArg);
            }
            else if (mvSwitch.equalsIgnoreCase("4")){
        		//MessageFormat lvMessageFormat = new MessageFormat(Utils.isNullStr( _pIni.getProperty("aastock.webserives.url.realtime")) ? "http://product1.aastocks.com/snapshot/BCOB/getquote.ashx?language={0}&encoding=utf8&format=1&symbol={1}&mbrid={2}" : _pIni.getProperty("aastock.webserives.url.realtime"));
            	MessageFormat lvMessageFormat = new MessageFormat(Utils.isNullStr( IMain.getProperty("aastock.webserives.url.realtime")) ? "http://product1.aastocks.com/snapshot/BCOB/getquote.ashx?language={0}&encoding=utf8&format=1&symbol={1}&mbrid={2}" : IMain.getProperty("aastock.webserives.url.realtime"));
            	String[] lvMessageFormatArg = {mvLanguage, mvInstrumentId, mvMemberId};
	    	    mvExternalURL = lvMessageFormat.format(lvMessageFormatArg);
            }
            else if (mvSwitch.equalsIgnoreCase("5"))
            {
            	//MessageFormat lvMessageFormat = new MessageFormat(Utils.isNullStr( _pIni.getProperty("aastock.webserives.url.stockSearch")) ? "http://product1.aastocks.com/xml/datafeed/getstocksearch.ashx?broker=BANKCOMM&encoding=utf8&keyword={0}" : _pIni.getProperty("aastock.webserives.url.stockSearch"));
            	MessageFormat lvMessageFormat = new MessageFormat(Utils.isNullStr( IMain.getProperty("aastock.webserives.url.stockSearch")) ? "http://product1.aastocks.com/xml/datafeed/getstocksearch.ashx?broker=BANKCOMM&encoding=utf8&keyword={0}" : IMain.getProperty("aastock.webserives.url.stockSearch"));
        		String[] lvMessageFormatArg = {mvKeywrod};
	    	    mvExternalURL = lvMessageFormat.format(lvMessageFormatArg);
            }
    	  }
    		else{
    			checkInstrument();
    			// From Walter
    	        //mvExternalURL =	_pIni.getProperty("aastock.webserives.url");
    			mvExternalURL =	IMain.getProperty("aastock.webserives.url");
    	      //END TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
    	        mvExternalURL += this.mvInstrumentId;

    		}
    	}
    	catch(NullPointerException npe)
        {
                npe.printStackTrace();
                Log.println("[HksStockQuoteTxn.process(): Source is null]", Log.ACCESS_LOG);
        }catch(Exception ex)
        {
                ex.printStackTrace();
                Log.println("[HksStockQuoteTxn.process(): Source Error ]", Log.ACCESS_LOG);
        }      
    	//END - TASK# CL00026

    	//For Debug
    	//mvExternalURL = "http://www.aastocks.com/pkages/broker/xml2/getxmlquote.asp?broker=bcom&module=1&cway=2&catg=2&symbol=" + this.mvInstrumentId;

    	//get URL through proxy
    	/*System.getProperties().put("proxySet", "true");
    	System.getProperties().put("https.proxyHost", "10.0.3.252");
    	System.getProperties().put("https.proxyPort", "8080");*/
        this.setStockReturnCode(PASS);
/*    	URL lvUrl = new URL(mvExternalURL);
    	InputStreamReader reader = new InputStreamReader((InputStream)lvUrl.openConnection().getContent());
    	lvIn = new BufferedReader(reader);
        String lvTempLine;
        if (!mvSwitch.equalsIgnoreCase("0"))
        	lvIn.readLine();
        while ((lvTempLine = lvIn.readLine()) != null) {
        	String lvRealLine ="";
        	for(int lvI = 0; lvI< lvTempLine.length(); lvI++){
        		int lvJ = (int)lvTempLine.charAt(lvI);
        		if( lvJ >= 32){
        			lvRealLine += lvTempLine.substring(lvI,lvI+1);
        		}
        	}
        	mvResult += lvRealLine;
        	}*/
        URL lvURL = new URL(mvExternalURL);
        Log.println("AAstock URL: " + mvExternalURL.toString(), Log.ACCESS_LOG);
        BufferedInputStream lvInputStream = new BufferedInputStream(lvURL.openStream());
        ByteArrayOutputStream lvBOS = new ByteArrayOutputStream();
        byte[] lvBytes = new byte[10*1024];
        int lvRead = 0;
        while((lvRead = lvInputStream.read(lvBytes)) > 0)
        {
        lvBOS.write(lvBytes, 0 , lvRead);
        }
        lvBOS.flush();
        mvResult = new String(lvBOS.toByteArray(), "UTF8"); 
        mvResult = mvResult.substring(mvResult.indexOf("?>") + 2);
        //For test only
        /*if(mvResult.equals("<?xml version=\"1.0\" encoding=\"big5\"?><quote></quote>")){
    		mvErrorCode = new ErrorCode(new String[0], HKSErrorCode.HKSItradeStockCodeInvalidErrorCode, "Stock Code is invalid.");
			throw new Throwable();
        }
        */
        //lvIn.close();

    	}
    	catch(UnknownHostException e)
    	{
    		Log.println(e, Log.ERROR_LOG);
    		setErrorCode(new ErrorCode(new String[0], "0100", "Unknown AAstock URL"));
    		setStockReturnCode(FAIL);
    	}
    	catch(Throwable e){
    		Log.println(e, Log.ERROR_LOG);
    		if(mvErrorCode != null){
    			setErrorCode(new ErrorCode(new String[0], mvErrorCode.getErrorID(), mvErrorCode.getDefaultErrorMessage()));
    			Log.println(e, Log.ERROR_LOG);
    		}
    		else
    		setErrorCode(new ErrorCode(new String[0], "0100", "System Error, Please contact support."));
    		setStockReturnCode(FAIL);
    	}
    	//END Task #:- al00001 albert lai 20080828
    }

    /**
     * Get the result in XML format
     * @return An XML string
     */
    public String getResult()
    {
    	return mvResult;
    }
    /**
     * Get method for Aa stock URL
     * @return Aa stock URL
     */
    public String getAastockURL (){
    	return mvExternalURL;
    }
    //BEGIN Task #:- al00001 albert lai 20080828
    /**
     * Get method for stock return system code
     * @return stock return system code
     */
    public int getStockReturnCode(){
    	return mvStockReturnCode;
    }
    /**
     * Set method for stock return system code
     * @param pStockReturnCode the stock return system code
     */
    public void setStockReturnCode(int pStockReturnCode){
    	mvStockReturnCode = pStockReturnCode;
    }
    
/*    //check language
    private void checkLanguage() throws Throwable{

        if (Utils.isNullStr(mvLanguage))
        {
       	 mvErrorCode = new ErrorCode(new String[0], "0100", "No language typed in.");
       	 throw new Throwable();
        }
        else if (mvLanguage.length()== 3)
        	if (mvLanguage.equalsIgnoreCase("eng") || mvLanguage.equalsIgnoreCase("chi") || mvLanguage.equalsIgnoreCase("chn"))
        		return;
        	else{
        		mvErrorCode = new ErrorCode(new String[0], "0100", "language incorrect.");
        		throw new Throwable();
        	}
        else{
         mvErrorCode = new ErrorCode(new String[0], "0100", "language length is invalid.");
      	 throw new Throwable();
        }
    }*/
    
    //check instrument
    /**
     * This method for Verify the existence of the stock numbers
     * @throws Throwable
     */
    private void checkInstrument() throws Throwable{
		mvMarketId = Utils.isNullStr(mvMarketId) ? "HKEX" : mvMarketId;
		  
        HKSInstrument lvInstrument = getInstrument(mvMarketId, mvInstrumentId);

        if (lvInstrument == null)
        {
       	 // stock code invalid error
        //Begin Task #RC00190 - Rice Cheng 20090114
       	 //mvErrorCode = new ErrorCode(new String[0], "HKSRISK0002", "Instrument does not belong to Exchange.");
        	mvErrorCode = new ErrorCode(new String[0], "ITWSERR0012", "Instrument does not belong to Exchange.");
       	 //End Task #RC00190 - Rice Cheng 20090114
       	 throw new Throwable();
       	 //lvEnterOrderModel.put("ERROR_TYPE", "StockCode");
        }
    }
    
    /*//check Symbol
    private void checkSymbol() throws Throwable{

        if (Utils.isNullStr(mvSymbol))
        {
       	 mvErrorCode = new ErrorCode(new String[0], "0100", "No Symbol typed in.");
       	 throw new Throwable();
        }
        else if (mvSymbol.length()== 3 || mvSymbol.length()== 4)
        	if (mvSymbol.equals("HSI") || mvSymbol.equals("HSCEI") || mvSymbol.equals("HSCCI"))
        		return;
        	else{
        		mvErrorCode = new ErrorCode(new String[0], "0100", "Symbol incorrect.");
        		throw new Throwable();
        	}
        else{
         mvErrorCode = new ErrorCode(new String[0], "0100", "Symbol length is invalid.");
      	 throw new Throwable();
        }
    }
    
    //check MarketID
    private void checkMarketID() throws Throwable{

        if (Utils.isNullStr(mvMarketId))
        {
       	 mvErrorCode = new ErrorCode(new String[0], "0100", "No MarketId typed in.");
       	 throw new Throwable();
        }
        else if (mvMarketId.length()== 1)
        	if (mvMarketId.equals("1") || mvMarketId.equals("2") || mvMarketId.equals("3"))
        		return;
        	else{
        		mvErrorCode = new ErrorCode(new String[0], "0100", "MarketId incorrect.");
        		throw new Throwable();
        	}
        else{
         mvErrorCode = new ErrorCode(new String[0], "0100", "MarketId length is invalid.");
      	 throw new Throwable();
        }
    }*/
    
    //From EnterOrder action
    /**
     * The method for store the instrument
     */
    protected HKSInstrument getInstrument(String pMarketID, String pStockCode)
    {
        HKSInstrument lvHKSInstrument = new HKSInstrument();
                  if(!Utils.isNullStr(pMarketID))
                  {
                          Log.println("[ DefaultAction MarketID : " + pMarketID + "]", Log.DEBUG_LOG);
                          if (svUsingStockCodeFormatMarket != null && svUsingStockCodeFormatMarket.contains( (String) pMarketID))
                          {
                                  lvHKSInstrument = (HKSInstrument) TPManager.mvInstrumentVector.get(pMarketID.concat("|").concat(Utils.getFormattedInstrumentID(pStockCode)));
                          }
                          else
                          {
                                  lvHKSInstrument = (HKSInstrument) TPManager.mvInstrumentVector.get(pMarketID.concat("|").concat(pStockCode.toUpperCase()));
                          }
                  }
        return lvHKSInstrument;
    }

	 /*protected String encode(String pURLEncoder)
	 // changed Mar 25, 2002: added if on 122 and else block on 129 to exclude Unicode range
	 {
	 String state   = "none";
	 int len     = pURLEncoder.length();
	 int backlen = len;
	 int i       = 0;
	 int lastpercent = 0;
	 String newStr  = "";
	 String frag    = "";
	 String encval  = "";
	 String original = pURLEncoder;
	
	 if (state == "none") // needs to be converted to normal chars
	    {
	      while (backlen > 0)
	            {
	              lastpercent = pURLEncoder.lastIndexOf("%");
	              if (lastpercent != -1) // we found a % char. Need to handle
	                 {
	                   // everything *after* the %
	                   frag = pURLEncoder.substring(lastpercent+1,pURLEncoder.length());
	                   // re-assign val to everything *before* the %
	                   pURLEncoder  = pURLEncoder.substring(0,lastpercent);
	                   if (frag.length() >= 2) // end contains unencoded
	                      {
	                      //  alert ("frag is greater than or equal to 2");
	                        encval = frag.substring(0,2);
	                        newStr = frag.substring(2,frag.length()) + newStr;
	                        //convert the char here. for now it just doesn't add it.
	                        if ("01234567890abcdefABCDEF".indexOf(encval.substring(0,1)) != -1 &&
	                            "01234567890abcdefABCDEF".indexOf(encval.substring(1,2)) != -1)
	                           {
	                            encval = new Character((char)Integer.parseInt(encval, 16)).toString(); // hex to base 10
	                            newStr = encval + newStr; // prepend the char in
	                           }
	                        // if so, convert. Else, ignore it.
	                      }
	                   // adjust length of the string to be examined
	                   backlen = lastpercent;
	                  // alert ("backlen at the end of the found % if is: " + backlen);
	                 }
	             else { newStr = pURLEncoder + newStr; backlen = 0; } // if there is no %, just leave the value as-is
	            } // end while
	    }         // end 'state=none' conversion

	     return newStr;
	 }
    */
    //End action
    //For Debug only
    
/*    public static void main(String[] args) {
    	HksStockQuoteTxn hks = new HksStockQuoteTxn("HKEX","00005");
    	hks.process();
    	System.out.println(hks.getResult());
    }*/
    
  //END Task #:- al00001 albert lai 20080828
}

