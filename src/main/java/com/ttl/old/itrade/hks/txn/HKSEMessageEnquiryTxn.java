package com.ttl.old.itrade.hks.txn;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.tag.ITagXsfTagName;
import com.systekit.winvest.hks.config.mapping.TagName;
/**
 * The HKSEMessageEnquiryTxn class definition for all methodsis
 * Used to check e-mail message
 * 
 * @author not attributable
 *
 */
public class HKSEMessageEnquiryTxn extends BaseTxn
{
	/**
	 * The method process Used to check e-mail message
	 * @param pClientID the Client ID
	 * @param pIsActive the Is Active
	 * @return the Return system e-mail Message information a collection
	 */
   public Vector process(String pClientID, String pIsActive)
   {
      Hashtable lvTxnMap = new Hashtable();
      lvTxnMap.put(TagName.CLIENTID, pClientID);
      lvTxnMap.put("ISACTIVE", pIsActive);

      Vector lvReturn = new Vector();
      if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSEMessageEnquiry, lvTxnMap))
      {
         IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList(ITagXsfTagName.ROW);
         for (int i = 0; i < lvRowList.size(); i++)
         {
            IMsgXMLNode lvNode = lvRowList.getNode(i);
            HashMap lvModel = new HashMap();
            lvModel.put(TagName.MESSAGEID, lvNode.getChildNode(TagName.MESSAGEID).getValue());
            lvModel.put("PLACEDATE", new Timestamp(Long.parseLong(lvNode.getChildNode("PLACEDATE").getValue())));
            lvModel.put(TagName.MESSAGE, lvNode.getChildNode(TagName.MESSAGE).getValue());
            lvReturn.add(lvModel);
         }
      }
      return lvReturn;
   }
}
