package com.ttl.old.itrade.hks.txn.plugin;

import java.util.Hashtable;

import javax.servlet.http.HttpSession;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLParser;
import com.systekit.common.msg.MsgManager;
import com.systekit.winvest.hks.config.mapping.TagName;
import com.systekit.winvest.hks.util.Utils;

/**
 * The HKSIPOGenConfirmTxn class definition for all method
 * IPO Gen Confirm
 * @author not attributable
 *
 */
public class HKSIPOGenConfirmTxn extends BaseTxn
{
    String mvEntitlementID = "";
    String mvClientID = "";
    String mvAppliedQty ="";
    String mvPhone = "";
    String mvEmail = "";
    // BEGIN RN00028 Ricky Ngan 20080816
    String mvMobile = "";
    String mvSMSRecieved = "";
    String mvSMSLang = "";
    String mvMarginFinancingType = "";
    String mvCreatorUserID = "";
    String mvFinancingPlan = "";
    // END RN00028
    // fixed charge
    String mvFlatfee = "";
    String mvFinanceFee = "";
    String mvDepositAmount = "";
    String mvLoanAmount = "";
    String mvLoanRate = "";
    String mvDepositRate = "";
    String mvLoanPercentage = "";
    // interest rate 
    String mvInterestRate = "";
    String mvInterestRateBasis = "";
    
    String mvTradingAccSeq = "";
    String mvAccountSeq = "";
    boolean mvVerifyPassword = true;
    String mvPassword = "";
    
    

    String mvSUBSCRIPTIONID, mvENTITLEMENTID, mvCONSIDERATION, mvAPPLIEDAMOUNT, mvQTY, mvHANDLINGFEE, mvINSTRUMENTID, mvINSTRUMENTSHORTNAME, mvINSTRUMENTCHINESESHORTNAME, mvINPUTTIME;

    int mvReturnCode = -1;
    String mvErrorMessage = "";
    String mvErrorCode = "";
    String mvIsSuccess = "";
    /**
     * This variable is used to the tp Error
     */
    TPErrorHandling tpError;
    
    private String mvCINO = new String();
    private String mvSSOUserID = new String();
    private String mvExternalPasswordVerificationEnable = new String();

    int mvLang = 0;

    /**
     * Default constructor for HKSIPOGenConfirmTxn class
     * @param pEntitlementID the entitlement id
     * @param pClientID the client id
     * @param pAccountSeq the account sequence
     * @param pTradingAccSeq the trading account sequence
     * @param pAppliedQty the application quantity
     * @param pTel the telephone
     * @param pEmail the email
     * @param pMobile the mobile
     * @param pSMSReceived the system message received
     * @param pSMSLang the system message language
     * @param pMaringFinancingType the margin financing type
     * @param pCreatorUserID the creator user id
     * @param pPassword the client password
     * @param pVerifyPassword the Verify client password
     */
    // BEGIN RN00028 Ricky Ngan 20080816
    public HKSIPOGenConfirmTxn(String pEntitlementID, String pClientID, String pAccountSeq, String pTradingAccSeq, String pAppliedQty, String pTel, String pEmail, String pMobile, String pSMSReceived, String pSMSLang, String pMaringFinancingType, String pCreatorUserID, String pPassword, String pVerifyPassword)
    {
        mvEntitlementID = pEntitlementID;
        mvClientID = pClientID;
        mvAccountSeq = pAccountSeq;
        mvTradingAccSeq = pTradingAccSeq;
        mvAppliedQty = pAppliedQty;
        mvPhone = pTel;
        mvEmail = pEmail;
        // BEGIN RN00028 Ricky Ngan 20080816
        mvMobile = pMobile;
        mvSMSRecieved = pSMSReceived;
        mvSMSLang = pSMSLang;
        mvMarginFinancingType = pMaringFinancingType;
        mvCreatorUserID = pCreatorUserID;
        // END RN00028
        mvVerifyPassword = pVerifyPassword.equals("Y");
        mvPassword = pPassword;

        tpError = new TPErrorHandling();
    }
 // END RN00028
   /**
    * Default constructor for HKSIPOGenConfirmTxn class
    * @param pEntitlementID the entitlement id
    * @param pClientID the client id
    * @param pAccountSeq the account sequence
    * @param pTradingAccSeq the trading account sequence
    * @param pAppliedQty the application quantity
    * @param pTel the telephone
    * @param pEmail the email
    * @param pMobile the mobile
    * @param pSMSReceived the system message received
    * @param pSMSLang the system message language
    * @param pMaringFinancingType the margin financing type
    * @param pCreatorUserID the creator user id
    * @param pPassword the client password
    * @param pVerifyPassword the verify client password
    * @param pExternalPasswordVerificationEnable the external client password verification enable
    */
    public HKSIPOGenConfirmTxn(String pEntitlementID, String pClientID, String pAccountSeq, String pTradingAccSeq, String pAppliedQty, String pTel, String pEmail, String pMobile, String pSMSReceived, String pSMSLang, String pMaringFinancingType, String pCreatorUserID, String pPassword, String pVerifyPassword, String pExternalPasswordVerificationEnable)
    {
        mvEntitlementID = pEntitlementID;
        mvClientID = pClientID;
        mvAccountSeq = pAccountSeq;
        mvTradingAccSeq = pTradingAccSeq;
        mvAppliedQty = pAppliedQty;
        mvPhone = pTel;
        mvEmail = pEmail;
        mvMobile = pMobile;
        mvSMSRecieved = pSMSReceived;
        mvSMSLang = pSMSLang;
        mvMarginFinancingType = pMaringFinancingType;
        mvCreatorUserID = pCreatorUserID;
        mvVerifyPassword = pVerifyPassword.equals("Y");
        mvPassword = pPassword;
        mvExternalPasswordVerificationEnable = pExternalPasswordVerificationEnable;
        tpError = new TPErrorHandling();
    }
    /**
     * This method for Fixed Charge Plan
     * @param pFlatFee the flat fee
     * @param pFinanceFee the finance fee
     * @param pLoanAmount the loan amount
     * @param pMarginPercentage the margin percentage
     */
    public void setFixedChargePlan(String pFlatFee, String pFinanceFee, String pLoanAmount, String pMarginPercentage){
    	mvFinancingPlan = "F";
    	mvFlatfee = pFlatFee;
        mvFinanceFee = pFinanceFee;
        mvLoanAmount = pLoanAmount;
        mvLoanRate = pMarginPercentage;
    }
    /**
     * This method for interest rate plan
     * @param pFinanceFee the finance fee
     * @param pInterestRate the interest rate
     * @param pInterestRateBasis the interest rate basis
     * @param pLoanAmount the loan amount
     * @param pInterestAmount the interest amount
     * @param pMarginPercentage the margin percentage
     */
    public void setInterestRatePlan(String pFinanceFee, String pInterestRate, String pInterestRateBasis, String pLoanAmount, String pInterestAmount, String pMarginPercentage){
    	mvFinancingPlan = "I";
    	mvFinanceFee = pFinanceFee;
    	mvInterestRate = pInterestRate;
    	mvInterestRateBasis = pInterestRateBasis;
    	mvLoanAmount = pLoanAmount; 
    	mvFlatfee = pInterestAmount;
    	mvLoanRate = pMarginPercentage;
    }
    
    /**
     * This method process IPO Gen Confirm
     */
    public void process()
    {
        try
        {
            Hashtable lvTxnMap = new Hashtable();
            IMsgXMLNode lvRetNode;
            TPManager ivTPManager;
            TPBaseRequest lvConfirmDataRequest;

                if(getLang()==1)
                {
               ivTPManager = ITradeServlet.getTPManager(RequestName.HKSIPOConfirmDataRequest_TW);
                 lvConfirmDataRequest = ivTPManager.getRequest(RequestName.HKSIPOConfirmDataRequest_TW);
                }else
                {
                    ivTPManager = ITradeServlet.getTPManager(RequestName.HKSIPOConfirmDataRequest);
                 lvConfirmDataRequest = ivTPManager.getRequest(RequestName.HKSIPOConfirmDataRequest);

                }
            lvTxnMap.put("CLIENTID", getMvClientID());
            lvTxnMap.put(TagName.ACCOUNTSEQ, getAccountSeq());
            lvTxnMap.put(TagName.TRADINGACCSEQ, getTradingAccSeq());
            lvTxnMap.put("ENTITLEMENTID", getMvEntitlementID());
            lvTxnMap.put("APPLIEDQTY", getMvAppliedQty());
            lvTxnMap.put("CONTACTNUMBER", mvPhone);
            lvTxnMap.put("CONTACTEMAIL", mvEmail);
            // BEGIN RN00028 Ricky Ngan 20080816
            lvTxnMap.put("CONTACTMOBILE", mvMobile);
            lvTxnMap.put("CONTACTSMSRECEIEVED", mvSMSRecieved);
            lvTxnMap.put("CONTACTSMSLANGUAGE", mvSMSLang);
            lvTxnMap.put("MARGINFINANCINGTYPE", mvMarginFinancingType);
            lvTxnMap.put("CREATORUSERID", mvCreatorUserID);
            // END RN00028
            lvTxnMap.put("FINANCINGPLAN", mvFinancingPlan);
            if("F".equals(mvFinancingPlan)){
            	lvTxnMap.put("LOANRATE", mvLoanRate);
            	lvTxnMap.put("LOANAMT", mvLoanAmount);
            	lvTxnMap.put("FLATFEE", mvFlatfee);
            	lvTxnMap.put("FINANCEFEE", mvFinanceFee);
            }else if("I".equals(mvFinancingPlan)){
            	lvTxnMap.put("LOANRATE", mvLoanRate);
            	lvTxnMap.put("LOANAMT", mvLoanAmount);
            	lvTxnMap.put("INTERESTRATEBASIS", mvInterestRateBasis);
            	lvTxnMap.put("INTERESTRATE", mvInterestRate);
            	lvTxnMap.put("INTERESTAMOUNT", mvFlatfee);
            	lvTxnMap.put("FINANCEFEE", mvFinanceFee);
            }
            
            if (isMvVerifyPassword())
            {
            	lvTxnMap.put("SSOUSERID", "");
                lvTxnMap.put("VRUPASSWORDVERIFICATION", "N");
                
                lvTxnMap.put("PASSWORDVERIFICATION", "Y");
                lvTxnMap.put("PASSWORD", getMvPassword());
            }
            else
            {
            	lvTxnMap.put("SSOUSERID", "");
                lvTxnMap.put("VRUPASSWORDVERIFICATION", "N");
                
                lvTxnMap.put("PASSWORDVERIFICATION", "N");
                lvTxnMap.put("PASSWORD", "");
            }

            lvRetNode = lvConfirmDataRequest.send(lvTxnMap);
            mvReturnCode = tpError.checkError(lvRetNode);
            if (mvReturnCode != tpError.TP_NORMAL)
            {
                mvErrorMessage = tpError.getErrDesc();
                if (mvReturnCode == tpError.TP_APP_ERR)
                {
                    mvErrorCode = tpError.getErrCode();
                }
                mvIsSuccess = "N";
                return;
            }
            else
            {
                mvIsSuccess = "Y";
                mvSUBSCRIPTIONID = lvRetNode.getChildNode("SUBSCRIPTIONID").getValue();
                mvENTITLEMENTID = lvRetNode.getChildNode("ENTITLEMENTID").getValue();
                mvCONSIDERATION = lvRetNode.getChildNode("CONSIDERATION").getValue();
                mvAPPLIEDAMOUNT = lvRetNode.getChildNode("APPLIEDAMOUNT").getValue();
                mvINPUTTIME = lvRetNode.getChildNode("LASTMODIFIEDTIME").getValue();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    /**
     * This method process the IPO Gen Confirm
     * @param pSession The HttpSession
     */
    public void process(HttpSession pSession)
    {
        try
        {
        	HttpSession lvSession = pSession;
            Hashtable lvTxnMap = new Hashtable();
            IMsgXMLNode lvRetNode;
           TPManager ivTPManager;
            TPBaseRequest lvConfirmDataRequest;

                if(getLang()==1)
                {
               ivTPManager = ITradeServlet.getTPManager(RequestName.HKSIPOConfirmDataRequest_TW);
                 lvConfirmDataRequest = ivTPManager.getRequest(RequestName.HKSIPOConfirmDataRequest_TW);
                }else
                {
                    ivTPManager = ITradeServlet.getTPManager(RequestName.HKSIPOConfirmDataRequest);
                 lvConfirmDataRequest = ivTPManager.getRequest(RequestName.HKSIPOConfirmDataRequest);

                }
            lvTxnMap.put("CLIENTID", getMvClientID());
            lvTxnMap.put(TagName.ACCOUNTSEQ, getAccountSeq());
            lvTxnMap.put(TagName.TRADINGACCSEQ, getTradingAccSeq());
            lvTxnMap.put("ENTITLEMENTID", getMvEntitlementID());
            lvTxnMap.put("APPLIEDQTY", getMvAppliedQty());
            lvTxnMap.put("CONTACTNUMBER", mvPhone);
            lvTxnMap.put("CONTACTEMAIL", mvEmail);
            lvTxnMap.put("CONTACTMOBILE", mvMobile);
            lvTxnMap.put("CONTACTSMSRECEIEVED", mvSMSRecieved);
            lvTxnMap.put("CONTACTSMSLANGUAGE", mvSMSLang);
            lvTxnMap.put("MARGINFINANCINGTYPE", mvMarginFinancingType);
            lvTxnMap.put("CREATORUSERID", mvCreatorUserID);
            lvTxnMap.put("FINANCINGPLAN", mvFinancingPlan);
            if("F".equals(mvFinancingPlan)){
            	lvTxnMap.put("LOANRATE", mvLoanRate);
            	lvTxnMap.put("LOANAMT", mvLoanAmount);
            	lvTxnMap.put("FLATFEE", mvFlatfee);
            	lvTxnMap.put("FINANCEFEE", mvFinanceFee);
            }else if("I".equals(mvFinancingPlan)){
            	lvTxnMap.put("LOANRATE", mvLoanRate);
            	lvTxnMap.put("LOANAMT", mvLoanAmount);
            	lvTxnMap.put("INTERESTRATEBASIS", mvInterestRateBasis);
            	lvTxnMap.put("INTERESTRATE", mvInterestRate);
            	lvTxnMap.put("INTERESTAMOUNT", mvFlatfee);
            	lvTxnMap.put("FINANCEFEE", mvFinanceFee);
            }
            
            if (isMvVerifyPassword())
            {
            	if(lvSession != null) {
            		try {
            			if(!Utils.isNullStr(lvSession.getAttribute("CINO"))) {
            				mvCINO = (String) lvSession.getAttribute("CINO");
            				if(!Utils.isNullStr(lvSession.getAttribute("LOGINID"))){
            					mvSSOUserID = (String) lvSession.getAttribute("LOGINID");
	            				if("Y".equals(getExternalPasswordVerificationEnable())) 
	            					lvTxnMap.put("SSOUSERID", mvSSOUserID);
	            					lvTxnMap.put("VRUPASSWORDVERIFICATION", "Y");
            				}
            				else {
            					lvTxnMap.put("SSOUSERID", "");
            					lvTxnMap.put("VRUPASSWORDVERIFICATION", "N");
            				}
            			}
            		} catch(NullPointerException npe) {
            			npe.printStackTrace();
            			Log.println("[ HKSIPOGenConfirmTxn.process() CINO, SSOUSER Null Pointer ]", Log.ERROR_LOG);
            			lvTxnMap.put("SSOUSERID", "");
            			lvTxnMap.put("VRUPASSWORDVERIFICATION", "N");

            		} catch(Exception ex) {
            			ex.printStackTrace();
            			Log.println("[ HKSIPOGenConfirmTxn.process() Exception ]", Log.ERROR_LOG);
            			lvTxnMap.put("SSOUSERID", "");
            			lvTxnMap.put("VRUPASSWORDVERIFICATION", "N");
            		}
            	}
                lvTxnMap.put("PASSWORDVERIFICATION", "Y");
                lvTxnMap.put("PASSWORD", getMvPassword());
            }
            else
            {
                lvTxnMap.put("PASSWORDVERIFICATION", "N");
                lvTxnMap.put("PASSWORD", "");
            }
            // TODO: simulate TP response XML
            lvRetNode = bulidXMLNode();
            //lvRetNode = lvConfirmDataRequest.send(lvTxnMap);
            
            mvReturnCode = tpError.checkError(lvRetNode);
            if (mvReturnCode != tpError.TP_NORMAL)
            {
                mvErrorMessage = tpError.getErrDesc();
                if (mvReturnCode == tpError.TP_APP_ERR)
                {
                    mvErrorCode = tpError.getErrCode();
                }
                mvIsSuccess = "N";
                return;
            }
            else
            {
                mvIsSuccess = "Y";
                mvSUBSCRIPTIONID = lvRetNode.getChildNode("SUBSCRIPTIONID").getValue();
                mvENTITLEMENTID = lvRetNode.getChildNode("ENTITLEMENTID").getValue();
                mvCONSIDERATION = lvRetNode.getChildNode("CONSIDERATION").getValue();
                mvAPPLIEDAMOUNT = lvRetNode.getChildNode("APPLIEDAMOUNT").getValue();
                mvINPUTTIME = lvRetNode.getChildNode("LASTMODIFIEDTIME").getValue();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    /**
     * Get method for application quantity
     * @return application quantity
     */
    public String getMvAppliedQty()
    {
        return mvAppliedQty;
    }
    /**
     * Get method for verify password
     * @return verify password
     */
    public boolean isMvVerifyPassword()
    {
        return mvVerifyPassword;
    }
    /**
     * Get method for system error message
     * @return system error message
     */
    public String getMvErrorMessage()
    {
        return mvErrorMessage;
    }
    /**
     * GEt method for entitlement id
     * @return entitlement id
     */
    public String getMvEntitlementID()
    {
        return mvEntitlementID;
    }
    /**
     * Get method for system error code
     * @return system error code
     */
    public String getMvErrorCode()
    {
        return mvErrorCode;
    }
    /**
     * Get method for return system code 
     * @return return system code 
     */
    public int getMvReturnCode()
    {
        return mvReturnCode;
    }
    /**
     * Get method for client id
     * @return client id
     */
    public String getMvClientID()
    {
        return mvClientID;
    }
    /**
     * Get method for account sequence
     * @return account sequence
     */
    public String getAccountSeq()
    {
        return mvAccountSeq;
    }
    /**
     * Get method for trading account sequence
     * @return trading account sequence
     */
    public String getTradingAccSeq()
    {
        return mvTradingAccSeq;
    }

    /**
     * Get method for is success
     * @return is success
     */
    public String getMvIsSuccess()
    {
        return mvIsSuccess;
    }
    /**
     * Get method for client password
     * @return client password
     */
    public String getMvPassword()
    {
        return mvPassword;
    }
    /**
     * Get method for application amount
     * @return application amount
     */
    public String getMvAPPLIEDAMOUNT()
    {
        return mvAPPLIEDAMOUNT;
    }
    /**
     * Get method for handling fee
     * @return handling fee
     */
    public String getMvHANDLINGFEE()
    {
        return mvHANDLINGFEE;
    }
    /**
     * Get method for input time
     * @return input time
     */
    public String getMvINPUTTIME()
    {
        return mvINPUTTIME;
    }
    /**
     * Get method for quantity
     * @return quantity
     */
    public String getMvQTY()
    {
        return mvQTY;
    }
    /**
     * Get method for subscription id
     * @return subscription id
     */
    public String getMvSUBSCRIPTIONID()
    {
        return mvSUBSCRIPTIONID;
    }
    /**
     * Get method for entitlement id
     * @return entitlement id
     */
    public String getMvENTITLEMENTID()
    {
        return mvENTITLEMENTID;
    }
    /**
     * Get method for consideration
     * @return consideration
     */
    public String getMvCONSIDERATION()
    {
        return mvCONSIDERATION;
    }
    /**
     * Get method for instrument chinese short name
     * @return instrument chinese short name
     */
    public String getMvINSTRUMENTCHINESESHORTNAME()
    {
        return mvINSTRUMENTCHINESESHORTNAME;
    }
    /**
     * Get method for instrument short name
     * @return
     */
    public String getMvINSTRUMENTSHORTNAME()
    {
        return mvINSTRUMENTSHORTNAME;
    }
    /**
     * Get method for instrument id
     * @return instrument id
     */
    public String getMvINSTRUMENTID()
    {
        return mvINSTRUMENTID;
    }
    /**
     * Get method for external client password verification enable
     * @return external client password verification enable
     */
	public String getExternalPasswordVerificationEnable() {
		return mvExternalPasswordVerificationEnable;
	}
	/**
	 * Set method for external client password verification enable
	 * @param pExternalPasswordVerificationEnable the external client password verification enable
	 */
	public void setExternalPasswordVerificationEnable(
			String pExternalPasswordVerificationEnable) {
		mvExternalPasswordVerificationEnable = pExternalPasswordVerificationEnable;
	}
	/**
	 * Get method for user locale
	 * @return user locale
	 */
    public int getLang()
    {
        return mvLang;
    }
    /**
     * Set method for user locale
     * @param pLang the user locale
     */
    public void setLang(int pLang)
    {
        mvLang = pLang;
    }
    
    
    /**
	 * This function return a request xml node for submit advance payment.
	 * @return xml node.
	 * @author Wind.Zhao
	 */
	private IMsgXMLNode bulidXMLNode(){
		IMsgXMLNode lvReturnXMLNode = null;
		IMsgXMLParser lvIMsgXMLParser = MsgManager.createParser();
		lvReturnXMLNode = lvIMsgXMLParser.createXMLNode("A_ER0001");
		lvReturnXMLNode.setAttribute("msgId", "ER0001");
		lvReturnXMLNode.setAttribute("issueTime", "20100301144705");
		lvReturnXMLNode.setAttribute("issueLoc", "888");
		lvReturnXMLNode.setAttribute("issueMetd", "01");
		lvReturnXMLNode.setAttribute("oprId", IMain.getProperty("AgentID"));
		lvReturnXMLNode.setAttribute("pwd", IMain.getProperty("AgentPassword"));
		lvReturnXMLNode.setAttribute("resvr", "0000000015");
		lvReturnXMLNode.setAttribute("language", "en");
		lvReturnXMLNode.setAttribute("country", "us");
		lvReturnXMLNode.setAttribute("retryForTimeout", "T");
		
		lvReturnXMLNode.addChildNode("C_ERROR_CODE").setValue("CORE10023");
		lvReturnXMLNode.addChildNode("C_ERROR_DESC").setValue("Invalid User ID");
		
		return lvReturnXMLNode;
	}

}
