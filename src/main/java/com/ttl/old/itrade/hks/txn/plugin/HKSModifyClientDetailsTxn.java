/**
 * 
 */
package com.ttl.old.itrade.hks.txn.plugin;


import java.util.Hashtable;
import com.ttl.old.itrade.hks.HKSTag;
import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.tp.TPErrorHandling;

/**
 * @author Khanh Nguyen
 * 
 */
public class HKSModifyClientDetailsTxn extends BaseTxn {

	private String mvClientID;
	private String mvEmail;
	private String mvTelephone;
	
	private String mvOldEmail;
	private String mvOldTelephone;
	
	private String mvAddress1;
	private String mvAddress2;
	private String mvAddress3;
	private String mvAddress4;
	private String mvAddress5;
	
	private String returnValue;

	public HKSModifyClientDetailsTxn(String pClientID, String pEmail,
			String pTelephone, String pAddress1, String pAddress2,
			String pAddress3, String pAddress4, String pAddress5) {
		super();
		mvClientID = pClientID;
		mvEmail = pEmail;
		mvTelephone = pTelephone;
		mvAddress1 = pAddress1;
		mvAddress2 = pAddress2;
		mvAddress3 = pAddress3;
		mvAddress4 = pAddress4;
		mvAddress5 = pAddress5;
	}
	
	public HKSModifyClientDetailsTxn(String pClientID, String pEmail,String pOldEmail,
			String pTelephone, String pOldTelephone, String pAddress1, String pAddress2,
			String pAddress3, String pAddress4, String pAddress5) {
		super();
		mvClientID = pClientID;
		mvEmail = pEmail;
		mvOldEmail = pOldEmail;		
		mvTelephone = pTelephone;
		mvOldTelephone = pOldTelephone;
		mvAddress1 = pAddress1;
		mvAddress2 = pAddress2;
		mvAddress3 = pAddress3;
		mvAddress4 = pAddress4;
		mvAddress5 = pAddress5;
	}
	

	@SuppressWarnings("unchecked")
	public void process()
	{
		try
		{
			Hashtable	lvTxnMap = new Hashtable();					
			lvTxnMap.put(HKSTag.CLIENTID, getMvClientID());		
			lvTxnMap.put(HKSTag.EMAIL, getMvEmail());	
			lvTxnMap.put("OLD" + HKSTag.EMAIL, getMvOldEmail());	
			lvTxnMap.put(HKSTag.PHONENUMBER, getMvTelephone());	
			lvTxnMap.put("OLD" + HKSTag.PHONENUMBER, getMvOldTelephone());	
			lvTxnMap.put(HKSTag.ADDRESS1, getMvAddress1());	
			lvTxnMap.put(HKSTag.ADDRESS2, getMvAddress2());	
			lvTxnMap.put(HKSTag.ADDRESS3, getMvAddress3());	
			lvTxnMap.put(HKSTag.ADDRESS4, getMvAddress4());	
			lvTxnMap.put(HKSTag.ADDRESS5, getMvAddress5());	
		
		    if (TPErrorHandling.TP_NORMAL == process("HKSModifyClientDetailsRequest", lvTxnMap)) {
		    	setReturnValue(mvReturnNode.getChildNode(HKSTag.RETURNCODE).getValue().trim());    	
		    }	
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * @param mvClientID
	 *            the mvClientID to set
	 */
	public void setMvClientID(String mvClientID) {
		this.mvClientID = mvClientID;
	}

	/**
	 * @return the mvClientID
	 */
	public String getMvClientID() {
		return mvClientID;
	}

	/**
	 * @param mvEmail
	 *            the mvEmail to set
	 */
	public void setMvEmail(String mvEmail) {
		this.mvEmail = mvEmail;
	}

	/**
	 * @return the mvEmail
	 */
	public String getMvEmail() {
		return mvEmail;
	}

	/**
	 * @param mvTelephone
	 *            the mvTelephone to set
	 */
	public void setMvTelephone(String mvTelephone) {
		this.mvTelephone = mvTelephone;
	}

	/**
	 * @return the mvTelephone
	 */
	public String getMvTelephone() {
		return mvTelephone;
	}

	/**
	 * @param mvAddress1
	 *            the mvAddress1 to set
	 */
	public void setMvAddress1(String mvAddress1) {
		this.mvAddress1 = mvAddress1;
	}

	/**
	 * @return the mvAddress1
	 */
	public String getMvAddress1() {
		return mvAddress1;
	}

	/**
	 * @param mvAddress2
	 *            the mvAddress2 to set
	 */
	public void setMvAddress2(String mvAddress2) {
		this.mvAddress2 = mvAddress2;
	}

	/**
	 * @return the mvAddress2
	 */
	public String getMvAddress2() {
		return mvAddress2;
	}

	/**
	 * @param mvAddress3
	 *            the mvAddress3 to set
	 */
	public void setMvAddress3(String mvAddress3) {
		this.mvAddress3 = mvAddress3;
	}

	/**
	 * @return the mvAddress3
	 */
	public String getMvAddress3() {
		return mvAddress3;
	}

	/**
	 * @param mvAddress4
	 *            the mvAddress4 to set
	 */
	public void setMvAddress4(String mvAddress4) {
		this.mvAddress4 = mvAddress4;
	}

	/**
	 * @return the mvAddress4
	 */
	public String getMvAddress4() {
		return mvAddress4;
	}

	/**
	 * @param mvAddress5
	 *            the mvAddress5 to set
	 */
	public void setMvAddress5(String mvAddress5) {
		this.mvAddress5 = mvAddress5;
	}

	/**
	 * @return the mvAddress5
	 */
	public String getMvAddress5() {
		return mvAddress5;
	}

	/**
	 * @param returnValue the returnValue to set
	 */
	public void setReturnValue(String returnValue) {
		this.returnValue = returnValue;
	}

	/**
	 * @return the returnValue
	 */
	public String getReturnValue() {
		return returnValue;
	}

	/**
	 * @param mvOldEmail the mvOldEmail to set
	 */
	public void setMvOldEmail(String mvOldEmail) {
		this.mvOldEmail = mvOldEmail;
	}

	/**
	 * @return the mvOldEmail
	 */
	public String getMvOldEmail() {
		return mvOldEmail;
	}

	/**
	 * @param mvOldTelephone the mvOldTelephone to set
	 */
	public void setMvOldTelephone(String mvOldTelephone) {
		this.mvOldTelephone = mvOldTelephone;
	}

	/**
	 * @return the mvOldTelephone
	 */
	public String getMvOldTelephone() {
		return mvOldTelephone;
	}

}
