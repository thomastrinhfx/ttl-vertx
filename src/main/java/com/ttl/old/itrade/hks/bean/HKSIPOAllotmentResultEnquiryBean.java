package com.ttl.old.itrade.hks.bean;

/**
 * The HKSIPOAllotmentResultEnquiryBean class define variables that to save values 
 * for action 
 * @author 
 *
 */
public class HKSIPOAllotmentResultEnquiryBean {
	private String mvStockID;
	private String mvStockName;
	private String mvAppliedQty;
	private String mvConfirmQty;
	private String mvUnAllottedQty;
	private String mvRefund;
	
	/**
     * This method returns the stock id.
     * @return the stock id.
     */
	public String getMvStockID() {
		return mvStockID;
	}
	
	/**
     * This method sets the stock id.
     * @param pStockID The stock id.
     */
	public void setMvStockID(String pStockID) {
		mvStockID = pStockID;
	}
	
	/**
     * This method returns the stock name.
     * @return the stock name.
     */
	public String getMvStockName() {
		return mvStockName;
	}
	
	/**
     * This method sets the stock name.
     * @param pStockName The stock name.
     */
	public void setMvStockName(String pStockName) {
		mvStockName = pStockName;
	}
	
	/**
     * This method returns the applied quantity.
     * @return the applied quantity.
     */
	public String getMvAppliedQty() {
		return mvAppliedQty;
	}
	
	/**
     * This method sets the applied quantity.
     * @param pAppliedQty The applied quantity.
     */
	public void setMvAppliedQty(String pAppliedQty) {
		mvAppliedQty = pAppliedQty;
	}
	
	/**
     * This method returns the confirm quantity.
     * @return the confirm quantity.
     */
	public String getMvConfirmQty() {
		return mvConfirmQty;
	}
	/**
     * This method sets the confirm quantity.
     * @param pConfirmQty The confirm quantity.
     */
	public void setMvConfirmQty(String pConfirmQty) {
		mvConfirmQty = pConfirmQty;
	}
	
	/**
     * This method returns the unallotted quantity.
     * @return the unallotted quantity.
     */
	public String getMvUnAllottedQty() {
		return mvUnAllottedQty;
	}
	
	/**
     * This method sets the unallotted quantity.
     * @param pUnAllottedQty The unallotted quantity.
     */
	public void setMvUnAllottedQty(String pUnAllottedQty) {
		mvUnAllottedQty = pUnAllottedQty;
	}
	
	/**
     * This method returns the refund.
     * @return the refund.
     */
	public String getMvRefund() {
		return mvRefund;
	}
	
	/**
     * This method sets the refund.
     * @param pRefund The refund.
     */
	public void setMvRefund(String pRefund) {
		mvRefund = pRefund;
	}
}