package com.ttl.old.itrade.hks.bean;

/**
 * The HKSGenModifyOrderBean class define variables that to save values 
 * for action 
 * @author Wind Zhao
 *
 */
public class HKSGenModifyOrderBean {
	private String mvIsPasswordSaved;
	private String mvPasswordConfirmation;
	private String mvSCodeEnableForOrdersModify;
	private String mvAllowOddLot;
	private String mvOrderId;
	private String mvOrderIdValue;
	private String mvOrderGroupId;
	private String mvOrderType;
	private String mvOrderTypeValue;
	private String mvAtauctionOrder;
	private String mvFilledQty;
	private String mvBaseNetAmtValue;
	private String mvAveragePrice;
	private String mvMarketId;
	private String mvQty;
	private String mvPrice;
	private String mvPriceValue;
	private String mvCancelQtyValue;
	private String mvStopPriceValue;
	private String mvNewPrice;
	private String mvNewQty;
	private String mvIsFromPreviousErrorPage;
	private String mvGoodTillDate;
	private String mvIsDisplayCurrencyWarning;
	//private String mvIsDisableModifyQty;
	private String mvStockId;
	private String mvStockName;
	private String mvCurrencyId;
	private String mvBuyOrSell;
	private String mvBSValue;
	private String mvMaxLotPerOrder;
	//private String mvIsAonDisable;
	private String mvTriggerDisable;
	//private String mvIsGoodTillDateDisable;
	//private String mvIsMultiMarketDisable;
	private String mvQuantityDescription;
	private String mvOrderTypeDescription;
	private String mvGoodTillDescription;
    private String mvLotSizeValue;
	private String mvStopOrderExpiryDate;
	private String mvValidityDate;
	private String mvActivationDate;
    private String mvSCRIP;
    private String mvAllOrNothing;
    private String mvRemark;
    private String mvContactPhone;
    private String mvGrossAmtValue;
    private String mvNetAmtValue;
    private String mvOriginalQuantity;
    private String mvOrigQtyValue;
    private String mvStopPrice;
    private String mvDisableModifyQtyHiddenField;
    private String mvDisableModifyQty;
    private String mvInstrumentName;
    private String mvAonDisable;
    private String mvGoodTillDateDisable;
    private String mvMultiMarketDisable;
    private String mvOrigPriceValue;
    private String mvStopTypeValue;
    private String mvGoodTillDateValue;
    private String floor;
    private String ceiling;
    private String mvStatus;
    
    //Begin Task #: TTL-GZ-ZZW-00011 Wind Zhao 20091118 for [ITradeR5 VN]Allow to modifycancel orders like ROSE's version
    
    /*
     * mvModifyOrderFor to control hidden or show some fields in 
     * modify order
     */
    private String mvModifyOrderFor;
    
    /*
     * mvSecurityCodeEnable is control hidden or show security Code field
     */
    private String mvSecurityCodeEnable;
    
    /**
     * This method returns the a symbol
     * to control security code show or hidden .
     * @return the symbol to control security code show or hidden.
     */
    public String getMvSecurityCodeEnable() {
		return mvSecurityCodeEnable;
	}
    
    /**
     * This method sets the a symbol
     * to control security code show or hidden .
     * @param pSecurityCodeEnable The symbol to control security code show or hidden.
     */
	public void setMvSecurityCodeEnable(String pSecurityCodeEnable) {
		mvSecurityCodeEnable = pSecurityCodeEnable;
	}
	
	/**
     * This method returns the symbol
     * to control show or hidden some fields.
     * @return the modify order for value.
     */
	public String getMvModifyOrderFor() {
    	return mvModifyOrderFor;
    }
	
	/**
     * This method sets the a symbol
     * to control show or hidden some fields.
     * @param pModifyOrderFor The modify order for value.
     */
    public void setMvModifyOrderFor(String pModifyOrderFor) {
    	mvModifyOrderFor = pModifyOrderFor;
    }
    //End Task #: TTL-GZ-ZZW-00011 Wind Zhao 20091118 for [ITradeR5 VN]Allow to modifycancel orders like ROSE's version
    
    /**
     * This method returns the order id.
     * @return the order id of the order.
     */
	public String getMvOrderIdValue() {
		return mvOrderIdValue;
	}
	
	/**
     * This method sets the order id.
     * @param pOrderIdValue The order id.
     */
	public void setMvOrderIdValue(String pOrderIdValue) {
		mvOrderIdValue = pOrderIdValue;
	}
	
	/**
     * This method returns the stop type of order.
     * @return the stop type of order.
     */
	public String getMvStopTypeValue() {
		return mvStopTypeValue;
	}
	
	/**
     * This method sets the stop type of order.
     * @param pStopTypeValue The stop type of order.
     */
	public void setMvStopTypeValue(String pStopTypeValue) {
		mvStopTypeValue = pStopTypeValue;
	}
	
	/**
     * This method returns the original price value.
     * @return the original price value.
     */
	public String getMvOrigPriceValue() {
		return mvOrigPriceValue;
	}
	
	/**
     * This method sets the original price value.
     * @param pOrigPriceValue The original price value.
     */
	public void setMvOrigPriceValue(String pOrigPriceValue) {
		mvOrigPriceValue = pOrigPriceValue;
	}
	
	/**
     * This method returns the multi market disable.
     * @return the multi market disable of the order.
     */
	public String getMvMultiMarketDisable() {
		return mvMultiMarketDisable;
	}
	
	/**
     * This method sets the multi market disable.
     * @param pMultiMarketDisable The multi market disable of the order.
     */
	public void setMvMultiMarketDisable(String pMultiMarketDisable) {
		mvMultiMarketDisable = pMultiMarketDisable;
	}
	
	/**
     * This method returns the good till date if disable.
     * @return the good till date if disable of the order.
     */
	public String getMvGoodTillDateDisable() {
		return mvGoodTillDateDisable;
	}
	
	/**
     * This method sets the good till date if disable.
     * @param pGoodTillDateDisable The good till date if disable of the order.
     */
	public void setMvGoodTillDateDisable(String pGoodTillDateDisable) {
		mvGoodTillDateDisable = pGoodTillDateDisable;
	}
	
	/**
     * This method returns the all or nothing if disable.
     * @return the all or nothing if disable of the order.
     */
	public String getMvAonDisable() {
		return mvAonDisable;
	}
	
	/**
     * This method sets the all or nothing if disable.
     * @param pAonDisable The all or nothing if disable of the order.
     */
	public void setMvAonDisable(String pAonDisable) {
		mvAonDisable = pAonDisable;
	}
	
	/**
     * This method returns the instrument name.
     * @return the instrument name of the order.
     */
	public String getMvInstrumentName() {
		return mvInstrumentName;
	}
	
	/**
     * This method sets the instrument name.
     * @param pInstrumentName The instrument name of the order.
     */
	public void setMvInstrumentName(String pInstrumentName) {
		mvInstrumentName = pInstrumentName;
	}
	
	/**
     * This method returns the modify quantity if disable
     * to control if show or hidden modify quantity field.
     * @return the modify quantity if disable.
     */
	public String getMvDisableModifyQty() {
		return mvDisableModifyQty;
	}
	
	/**
     * This method sets the modify quantity if disable
     * to control if show or hidden modify quantity field.
     * @param pDisableModifyQty The modify quantity if disable.
     */
	public void setMvDisableModifyQty(String pDisableModifyQty) {
		mvDisableModifyQty = pDisableModifyQty;
	}
	
	/**
     * This method returns the modify quantity hidden field if disable
     * to control if show or hidden modify quantity hidden field.
     * @return the modify quantity if disable.
     */
	public String getMvDisableModifyQtyHiddenField() {
		return mvDisableModifyQtyHiddenField;
	}
	
	/**
     * This method sets the modify quantity if disable
     * to control if show or hidden modify quantity hidden field.
     * @param pDisableModifyQtyHiddenField The modify quantity if disable.
     */
	public void setMvDisableModifyQtyHiddenField(
			String pDisableModifyQtyHiddenField) {
		mvDisableModifyQtyHiddenField = pDisableModifyQtyHiddenField;
	}
	
	/**
     * This method returns the stop price of order.
     * @return the stop price of order.
     */
	public String getMvStopPrice() {
		return mvStopPrice;
	}
	
	/**
     * This method sets the stop price of order.
     * @param pStopPrice The stop price of order.
     */
	public void setMvStopPrice(String pStopPrice) {
		mvStopPrice = pStopPrice;
	}
	
	/**
     * This method returns the original quantity value.
     * @return the original quantity value.
     */
	public String getMvOrigQtyValue() {
		return mvOrigQtyValue;
	}
	
	/**
     * This method sets the original quantity value of order.
     * @param pOrigQtyValue The original quantity value of order.
     */
	public void setMvOrigQtyValue(String pOrigQtyValue) {
		mvOrigQtyValue = pOrigQtyValue;
	}
	
	/**
     * This method returns the original quantity value.
     * @return the original quantity value is quantity subtract cancel quantity.
     */
	public String getMvOriginalQuantity() {
		return mvOriginalQuantity;
	}
	
	/**
     * This method sets the original quantity value of order.
     * @param pOriginalQuantity The original quantity value is quantity subtract cancel quantity.
     */
	public void setMvOriginalQuantity(String pOriginalQuantity) {
		mvOriginalQuantity = pOriginalQuantity;
	}
	
	/**
     * This method returns the net amount value.
     * @return the net amount value.
     */
	public String getMvNetAmtValue() {
		return mvNetAmtValue;
	}
	
	/**
     * This method sets the net amount value.
     * @param pNetAmtValue The net amount value.
     */
	public void setMvNetAmtValue(String pNetAmtValue) {
		mvNetAmtValue = pNetAmtValue;
	}
	
	/**
     * This method returns the gross amount value.
     * @return the gross amount value.
     */
	public String getMvGrossAmtValue() {
		return mvGrossAmtValue;
	}
	
	/**
     * This method sets the gross amount value.
     * @param pGrossAmtValue The gross amount value.
     */
	public void setMvGrossAmtValue(String pGrossAmtValue) {
		mvGrossAmtValue = pGrossAmtValue;
	}
	
	/**
     * This method returns the contact phone.
     * @return the contact phone.
     */
	public String getMvContactPhone() {
		return mvContactPhone;
	}
	
	/**
     * This method sets the contact phone.
     * @param pContactPhone The contact phone.
     */
	public void setMvContactPhone(String pContactPhone) {
		mvContactPhone = pContactPhone;
	}
	
	/**
     * This method returns the remark.
     * @return the remark.
     */
	public String getMvRemark() {
		return mvRemark;
	}
	
	/**
     * This method sets the remark.
     * @param pRemark The remark.
     */
	public void setMvRemark(String pRemark) {
		mvRemark = pRemark;
	}
	
	/**
     * This method returns the all or nothing.
     * @return the all or nothing of the order.
     */
	public String getMvAllOrNothing() {
		return mvAllOrNothing;
	}
	
	/**
     * This method sets the all or nothing.
     * @param pAllOrNothing The all or nothing of order.
     */
	public void setMvAllOrNothing(String pAllOrNothing) {
		mvAllOrNothing = pAllOrNothing;
	}
	
	/**
     * This method returns the scrip.
     * @return the scrip.
     *         [0] Y
     *         [1] N
     */
	public String getMvSCRIP() {
		return mvSCRIP;
	}
	
	/**
     * This method sets the scrip.
     * @param pSCRIP The scrip.
     */
	public void setMvSCRIP(String pSCRIP) {
		mvSCRIP = pSCRIP;
	}
	
	/**
     * This method returns the activation date.
     * @return the activation date.
     */
	public String getMvActivationDate() {
		return mvActivationDate;
	}
	
	/**
     * This method sets the activation date.
     * @param pActivationDate The activation date.
     */
	public void setMvActivationDate(String pActivationDate) {
		mvActivationDate = pActivationDate;
	}
	
	/**
     * This method returns the validity date.
     * @return the validity date.
     */
	public String getMvValidityDate() {
		return mvValidityDate;
	}
	
	/**
     * This method sets the validity date.
     * @param pValidityDate The validity date.
     */
	public void setMvValidityDate(String pValidityDate) {
		mvValidityDate =pValidityDate;
	}
	
	/**
     * This method returns the stop order expiry date.
     * @return the stop order expiry date.
     */
	public String getMvStopOrderExpiryDate() {
		return mvStopOrderExpiryDate;
	}
	
	/**
     * This method sets the stop order expiry date.
     * @param pStopOrderExpiryDate The stop order expiry date.
     */
	public void setMvStopOrderExpiryDate(String pStopOrderExpiryDate) {
		mvStopOrderExpiryDate = pStopOrderExpiryDate;
	}
	
	/**
     * This method returns the lot size value.
     * @return the lot size value.
     */
	public String getMvLotSizeValue() {
		return mvLotSizeValue;
	}
	
	/**
     * This method sets the lot size value.
     * @param pLotSizeValue The lot size value.
     */
	public void setMvLotSizeValue(String pLotSizeValue) {
		mvLotSizeValue = pLotSizeValue;
	}
	
	/**
     * This method returns the B(Buy) or S(Sell).
     * @return the B(Buy) or S(sell) of the order.
     */
	public String getMvBSValue() {
		return mvBSValue;
	}
	
	/**
     * This method sets the B(Buy) or S(Sell).
     * @param pBSValue The B(Buy) or S(Sell) of the order.
     */
	public void setMvBSValue(String pBSValue) {
		mvBSValue = pBSValue;
	}
	
	/**
     * This method returns the stock id.
     * @return the stock id of the order.
     */
	public String getMvStockId() {
		return mvStockId;
	}
	
	/**
     * This method sets the stock id.
     * @param pStockId The stock id of the order.
     */
	public void setMvStockId(String pStockId) {
		mvStockId = pStockId;
	}
	
	/**
     * This method returns the order type.
     * @return the order type of the order.
     */
	public String getMvOrderTypeValue() {
		return mvOrderTypeValue;
	}
	
	/**
     * This method sets the order type.
     * @param pOrderTypeValue The order type of the order
     */
	public void setMvOrderTypeValue(String pOrderTypeValue) {
		mvOrderTypeValue = pOrderTypeValue;
	}
	
	/**
     * This method returns the password if saved or not.
     * @return the password if saved or not.
     *         [0] Y is hidden the password field.
     *         [1] N is show the password field.
     */
	public String getMvIsPasswordSaved() {
		return mvIsPasswordSaved;
	}
	
	/**
     * This method sets the password if saved or not.
     * @param pIsPasswordSaved The password if saved or not.
     */
	public void setMvIsPasswordSaved(String pIsPasswordSaved) {
		mvIsPasswordSaved = pIsPasswordSaved;
	}
	
	/**
     * This method returns the password if confirm or not.
     * @return the password if confirm or not.
     *         [0] Y is confirm the password.
     *         [1] N is not confirm the password.
     */
	public String getMvPasswordConfirmation() {
		return mvPasswordConfirmation;
	}
	
	/**
     * This method sets the password if confirm or not.
     * @param pPasswordConfirmation The password if confirm or not.
     */
	public void setMvPasswordConfirmation(String pPasswordConfirmation) {
		mvPasswordConfirmation = pPasswordConfirmation;
	}
	
	/**
     * This method returns the security code if need enter or not.
     * @return  the security code if show or hidden for modify order.
     *         [0] true is need to enter security code.
     *         [1] false is not need to enter security code.
     */
	public String getMvSCodeEnableForOrdersModify() {
		return mvSCodeEnableForOrdersModify;
	}
	
	/**
     * This method sets the security code if need enter or not.
     * @param pSCodeEnableForOrdersModify The security code if need enter or not.
     */
	public void setMvSCodeEnableForOrdersModify(String pSCodeEnableForOrdersModify) {
		mvSCodeEnableForOrdersModify = pSCodeEnableForOrdersModify;
	}
	
	/**
     * This method returns the allow odd lot.
     * @return the allow odd lot.
     *         [0]true
     *         [1]false
     */
	public String getMvAllowOddLot() {
		return mvAllowOddLot;
	}
	
	/**
     * This method sets the allow odd lot.
     * @param pAllowOddLot The allow odd lot.
     */
	public void setMvAllowOddLot(String pAllowOddLot) {
		mvAllowOddLot = pAllowOddLot;
	}
	
	/**
     * This method returns the order id.
     * @return the order id of the order.
     */
	public String getMvOrderId() {
		return mvOrderId;
	}
	
	/**
     * This method sets the order id.
     * @param pOrderId The order id.
     */
	public void setMvOrderId(String pOrderId) {
		mvOrderId = pOrderId;
	}
	
	/**
     * This method returns the order group id.
     * @return the order group id of the order.
     */
	public String getMvOrderGroupId() {
		return mvOrderGroupId;
	}
	
	/**
     * This method sets the order group id.
     * @param pOrderGroupId The order group id of the order.
     */
	public void setMvOrderGroupId(String pOrderGroupId) {
		mvOrderGroupId = pOrderGroupId;
	}
	
	/**
     * This method returns the order type.
     * @return the order type of the order is formatted.
     */
	public String getMvOrderType() {
		return mvOrderType;
	}
	
	/**
     * This method sets the order type.
     * @param pOrderType The order type of the order is formatted.
     */
	public void setMvOrderType(String pOrderType) {
		mvOrderType = pOrderType;
	}
	
	/**
     * This method returns the transaction type if auction order.
     * @return the transaction type if auction order.
     *         [0]true is at auction order
     *         [1]false is not at auction order
     */
	public String getMvAtauctionOrder() {
		return mvAtauctionOrder;
	}
	
	/**
     * This method sets the transaction type if auction order.
     * @param pAtauctionOrder The transaction type if auction order.
     */
	public void setMvAtauctionOrder(String pAtauctionOrder) {
		mvAtauctionOrder = pAtauctionOrder;
	}
	
	/**
     * This method returns the filled quantity.
     * @return the filled quantity of the order.
     */
	public String getMvFilledQty() {
		return mvFilledQty;
	}
	
	/**
     * This method sets the filled quantity.
     * @param  pFilledQty The filled quantity of the order.
     */
	public void setMvFilledQty(String pFilledQty) {
		mvFilledQty = pFilledQty;
	}
	
	/**
     * This method returns the base net amount value.
     * @return the base net amount value of the order.
     */
	public String getMvBaseNetAmtValue() {
		return mvBaseNetAmtValue;
	}
	
	/**
     * This method sets the base net amount value.
     * @param  pBaseNetAmtValue The base net amount value of the order.
     */
	public void setMvBaseNetAmtValue(String pBaseNetAmtValue) {
		mvBaseNetAmtValue = pBaseNetAmtValue;
	}
	
	/**
     * This method returns the average price.
     * @return the average price of the order.
     */
	public String getMvAveragePrice() {
		return mvAveragePrice;
	}
	
	/**
     * This method sets the average price.
     * @param  pAveragePrice The average price of the order.
     */
	public void setMvAveragePrice(String pAveragePrice) {
		mvAveragePrice = pAveragePrice;
	}
	
	/**
     * This method returns the market id.
     * @return the market id of the order.
     */
	public String getMvMarketId() {
		return mvMarketId;
	}
	
	/**
     * This method sets the market id.
     * @param pMarketId The market id of the order.
     */
	public void setMvMarketId(String pMarketId) {
		mvMarketId = pMarketId;
	}
	
	/**
     * This method returns the quantity.
     * @return the quantity of order.
     */
	public String getMvQty() {
		return mvQty;
	}
	/**
     * This method sets the quantity.
     * @param pQty The quantity of order.
     */
	public void setMvQty(String pQty) {
		mvQty = pQty;
	}
	
	/**
     * This method returns the price.
     * @return the price of the order is formatted.
     */
	public String getMvPrice() {
		return mvPrice;
	}
	
	/**
     * This method sets the price.
     * @param pPrice The price of the order is formatted.
     */
	public void setMvPrice(String pPrice) {
		mvPrice = pPrice;
	}
	
	/**
     * This method returns the price.
     * @return the price of the order.
     */
	public String getMvPriceValue() {
		return mvPriceValue;
	}
	
	/**
     * This method sets the price.
     * @param pPriceValue The price of the order.
     */
	public void setMvPriceValue(String pPriceValue) {
		mvPriceValue = pPriceValue;
	}
	
	/**
     * This method returns the quantity of cancel.
     * @return the quantity of cancel.
     */
	public String getMvCancelQtyValue() {
		return mvCancelQtyValue;
	}
	
	/**
     * This method sets the quantity of cancel.
     * @param pCancelQtyValue The quantity of cancel.
     */
	public void setMvCancelQtyValue(String pCancelQtyValue) {
		mvCancelQtyValue = pCancelQtyValue;
	}
	
	/**
     * This method returns the stop price of order.
     * @return the stop price of order.
     */
	public String getMvStopPriceValue() {
		return mvStopPriceValue;
	}
	
	/**
     * This method sets the stop price of order.
     * @param pStopPriceValue The stop price of order.
     */
	public void setMvStopPriceValue(String pStopPriceValue) {
		mvStopPriceValue = pStopPriceValue;
	}
	/**
     * This method returns the new price of order.
     * @return the new price of order.
     */
	public String getMvNewPrice() {
		return mvNewPrice;
	}
	
	/**
     * This method sets the new price of order.
     * @param pNewPrice The new price of order.
     */
	public void setMvNewPrice(String pNewPrice) {
		mvNewPrice = pNewPrice;
	}
	
	/**
     * This method returns the new quantity of order.
     * @return the new quantity of order.
     */
	public String getMvNewQty() {
		return mvNewQty;
	}
	
	/**
     * This method sets the new quantity of order.
     * @param pNewQty The new quantity of order.
     */
	public void setMvNewQty(String pNewQty) {
		mvNewQty = pNewQty;
	}
	
	/**
     * This method returns the values is from previous error page.
     * @return the values is from previous error page.
     */
	public String getMvIsFromPreviousErrorPage() {
		return mvIsFromPreviousErrorPage;
	}
	
	/**
     * This method sets the values is from previous error page.
     * @param pIsFromPreviousErrorPage The values is from previous error page.
     */
	public void setMvIsFromPreviousErrorPage(String pIsFromPreviousErrorPage) {
		mvIsFromPreviousErrorPage = pIsFromPreviousErrorPage;
	}
	
	/**
     * This method returns the good till date.
     * @return the good till date of the order is formatted.
     */
	public String getMvGoodTillDate() {
		return mvGoodTillDate;
	}
	
	/**
     * This method sets the good till date.
     * @param pGoodTillDate The good till date of the order is formatted.
     */
	public void setMvGoodTillDate(String pGoodTillDate) {
		mvGoodTillDate = pGoodTillDate;
	}
	
	/**
     * This method returns the currency warning if display.
     * @return the currency warning if display.
     *         [0] Y is display.
     *         [1] N is not display.
     */
	public String getMvIsDisplayCurrencyWarning() {
		return mvIsDisplayCurrencyWarning;
	}
	
	/**
     * This method sets the currency warning if display.
     * @param pIsDisplayCurrencyWarning The the currency warning if display.
     */
	public void setMvIsDisplayCurrencyWarning(String pIsDisplayCurrencyWarning) {
		mvIsDisplayCurrencyWarning = pIsDisplayCurrencyWarning;
	}
	
	/*public String getMvIsDisableModifyQty() {
		return mvIsDisableModifyQty;
	}
	public void setMvIsDisableModifyQty(String pIsDisableModifyQty) {
		mvIsDisableModifyQty = pIsDisableModifyQty;
	}*/
	
	
	/**
     * This method returns the stock name.
     * @return the stock name of the order.
     */
	public String getMvStockName() {
		return mvStockName;
	}
	
	/**
     * This method sets the stock name.
     * @param pStockName The stock name of the order.
     */
	public void setMvStockName(String pStockName) {
		mvStockName = pStockName;
	}
	
	/**
     * This method returns the currency id.
     * @return the currency id of the order.
     */
	public String getMvCurrencyId() {
		return mvCurrencyId;
	}
	
	/**
     * This method sets the currency id.
     * @param pCurrencyId The currency id of the order.
     */
	public void setMvCurrencyId(String pCurrencyId) {
		mvCurrencyId = pCurrencyId;
	}
	
	/**
     * This method returns the buy or sell.
     * @return the buy or sell of the order.
     *         [0] Buy.
     *         [1] Sell
     */
	public String getMvBuyOrSell() {
		return mvBuyOrSell;
	}
	
	/**
     * This method sets the buy or sell.
     * @param pBuyOrSell The buy or sell of the order.
     */
	public void setMvBuyOrSell(String pBuyOrSell) {
		mvBuyOrSell = pBuyOrSell;
	}
	
	/**
     * This method returns the the max lot per order.
     * @return the max lot per order.
     */
	public String getMvMaxLotPerOrder() {
		return mvMaxLotPerOrder;
	}
	
	/**
     * This method sets the max lot per order.
     * @param pMaxLotPerOrder The max lot per order.
     */
	public void setMvMaxLotPerOrder(String pMaxLotPerOrder) {
		mvMaxLotPerOrder = pMaxLotPerOrder;
	}
	
	/*public String getMvIsAonDisable() {
		return mvIsAonDisable;
	}
	public void setMvIsAonDisable(String pIsAonDisable) {
		mvIsAonDisable = pIsAonDisable;
	}*/
	
	
	/**
     * This method returns the trigger field if disable.
     * @return the trigger field if disable.
     */
	public String getMvTriggerDisable() {
		return mvTriggerDisable;
	}
	
	/**
     * This method sets the trigger field if disable.
     * @param pTriggerDisable The trigger field if disable.
     */
	
	public void setMvTriggerDisable(String pTriggerDisable) {
		mvTriggerDisable = pTriggerDisable;
	}
	
	/*public String getMvIsGoodTillDateDisable() {
		return mvIsGoodTillDateDisable;
	}
	public void setMvIsGoodTillDateDisable(String pIsGoodTillDateDisable) {
		mvIsGoodTillDateDisable = pIsGoodTillDateDisable;
	}
	public String getMvIsMultiMarketDisable() {
		return mvIsMultiMarketDisable;
	}
	public void setMvIsMultiMarketDisable(String pIsMultiMarketDisable) {
		mvIsMultiMarketDisable = pIsMultiMarketDisable;
	}*/
	
	/**
     * This method returns the quantity description.
     * @return the quantity description of the order.
     */
	public String getMvQuantityDescription() {
		return mvQuantityDescription;
	}
	
	/**
     * This method sets the quantity description.
     * @param pQuantityDescription The quantity description of the order.
     */
	public void setMvQuantityDescription(String pQuantityDescription) {
		mvQuantityDescription = pQuantityDescription;
	}
	
	/**
     * This method returns the order type description.
     * @return the order type description of the order.
     */
	public String getMvOrderTypeDescription() {
		return mvOrderTypeDescription;
	}
	
	/**
     * This method sets the order type description.
     * @param pOrderTypeDescription The order type description of the order.
     */
	public void setMvOrderTypeDescription(String pOrderTypeDescription) {
		mvOrderTypeDescription = pOrderTypeDescription;
	}
	
	/**
     * This method returns the good till date description.
     * @return the good till date description of the order.
     */
	public String getMvGoodTillDescription() {
		return mvGoodTillDescription;
	}
	
	/**
     * This method sets the good till date description.
     * @param pGoodTillDescription The good till date description of the order.
     */
	public void setMvGoodTillDescription(String pGoodTillDescription) {
		mvGoodTillDescription = pGoodTillDescription;
	}
	
	/**
     * This method returns the good till date value.
     * @return the good till date value of the order is not formatted.
     */
	public String getMvGoodTillDateValue() {
		return mvGoodTillDateValue;
	}
	
	/**
     * This method sets the good till date value.
     * @param pGoodTillDateValue The good till date value of the order is not formatted.
     */
	public void setMvGoodTillDateValue(String pGoodTillDateValue) {
		mvGoodTillDateValue = pGoodTillDateValue;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getCeiling() {
		return ceiling;
	}

	public void setCeiling(String ceiling) {
		this.ceiling = ceiling;
	}

	public String getMvStatus() {
		return mvStatus;
	}

	public void setMvStatus(String mvStatus) {
		this.mvStatus = mvStatus;
	}
	
	
}
