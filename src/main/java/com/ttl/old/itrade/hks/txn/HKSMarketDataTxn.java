package com.ttl.old.itrade.hks.txn;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.hks.HKSTag;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.hks.util.HKSInstrument;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.tag.ITagXsfTagName;
import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * This class store the market date information on
 * 
 * @author not attributable
 * 
 */
public class HKSMarketDataTxn extends BaseTxn {

	private String mvClientID;
	private String mvMarketID;
	private String mvInstrumentList;
	private String mvLoopCounter;
	private String mvIsMarketIndex;
	private String mvNewQuery;

	/**
	 * Constructor for HKSMarketDataTxn class
	 * 
	 * @param pClientID
	 *            the client id
	 * @param pMarketID
	 *            the market id
	 * @param pInstrumentList
	 *            the instrument list
	 * @param pNewQuery
	 *            the new query
	 * @param pIsMarketIndex
	 *            the is market index
	 */
	public HKSMarketDataTxn(String pClientID, String pMarketID,
			String pInstrumentList, String pNewQuery, String pIsMarketIndex) {
		super();
		mvClientID = pClientID;
		mvMarketID = pMarketID;
		mvInstrumentList = pInstrumentList;
		mvIsMarketIndex = pIsMarketIndex;
		mvNewQuery = pNewQuery;
	}

	/**
	 * This method store the market date information on
	 * 
	 * @return Host access to market data collection
	 */
	@SuppressWarnings("unchecked")
	public Vector process() {
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put(TagName.CLIENTID, getClientID());
		lvTxnMap.put(TagName.MARKETID, getMarketID());
		lvTxnMap.put("INSTRUMENTLIST", getInstrumentList());
		lvTxnMap.put("ISMARKETINDEX", getIsMarketIndex());
		lvTxnMap.put("NEWQUERY", "N");

		Vector lvReturn = new Vector();
		Vector lvRows = new Vector();
		Vector lvFullList = new Vector();
		// Vector lvIndex = new Vector();

		StringBuffer lvCheckedList = new StringBuffer();
		if (TPErrorHandling.TP_NORMAL == process(
				RequestName.HKSMarketDataRequest, lvTxnMap)) {
			IMsgXMLNodeList lvInstrumentList = mvReturnNode
					.getNodeList(ITagXsfTagName.VALUE);
			for (int i = 0; i < lvInstrumentList.size(); i++) {
				IMsgXMLNode lvValue = lvInstrumentList.getNode(i);
				lvFullList.add(lvValue.getChildNode(TagName.ID).getValue());
			}
			lvReturn.add(lvFullList);

			IMsgXMLNodeList lvRowList = mvReturnNode
					.getNodeList(ITagXsfTagName.ROW);
			for (int i = 0; i < lvRowList.size(); i++) {
				IMsgXMLNode lvRow = lvRowList.getNode(i);
				HashMap lvModel = new HashMap();
				String lvMarketID = lvRow.getChildNode(
						TagName.MARKETID).getValue();
				lvModel.put(TagName.MARKETID, lvMarketID);
				String lvInstrumentID = lvRow.getChildNode(
						TagName.INSTRUMENTID).getValue();
				lvModel.put(TagName.INSTRUMENTID, lvInstrumentID);
				
				if (IMain.stockHistMap.containsKey(lvInstrumentID)) {
					long lastKey = IMain.stockHistMap.get(lvInstrumentID)
							.lastKey();
					String value = IMain.stockHistMap.get(lvInstrumentID)
							.get(lastKey);
					String[] lastObject = value.split("\\|");
					try {
						lvModel.put(TagName.NOMINALPRICE, lastObject[2]);
					} catch (Exception e) {
						Log.println(e, Log.ERROR_LOG);
					}
				} else {
					lvModel.put(TagName.NOMINALPRICE, lvRow.getChildNode(
						TagName.NOMINALPRICE).getValue());
				}
				// Begin Task#: KN00013 KEVIN NG 20080108
				 if(lvMarketID.equalsIgnoreCase(HKSTag.HOSE_EXCHANGE_CODE)){
					 
					if (IMain.stockHistMap.containsKey(lvInstrumentID)) {
						long lastKey = IMain.stockHistMap.get(lvInstrumentID)
								.lastKey();
						String value = IMain.stockHistMap.get(lvInstrumentID)
								.get(lastKey);
						String[] lastObject = value.split("\\|");
						try {
							lvModel.put(TagName.REFERENCEPRICE, lastObject[6]);
						} catch (Exception e) {
							Log.println(e, Log.ERROR_LOG);
						}
					} else {
						HKSInstrument lvHKSInstrument = (HKSInstrument) TPManager.mvInstrumentVector
								.get(lvMarketID.concat("|").concat(
										lvInstrumentID.toUpperCase()));
						String refPrice = lvHKSInstrument.getMvClosingPrice();
						if (refPrice != null && !refPrice.isEmpty()) {
							lvModel.put(TagName.REFERENCEPRICE, refPrice);
						} else {
							lvModel.put(TagName.REFERENCEPRICE, "-");
						}
					}
				} else {
					lvModel.put(TagName.REFERENCEPRICE, lvRow.getChildNode(
							TagName.REFERENCEPRICE).getValue());
				}
				// End Task#: KN00013
				lvModel.put(TagName.HIGHPRICE, lvRow.getChildNode(
						TagName.HIGHPRICE).getValue());
				lvModel.put(TagName.LOWPRICE, lvRow.getChildNode(
						TagName.LOWPRICE).getValue());
				lvModel.put(TagName.OPENINGPRICE, lvRow.getChildNode(
						TagName.OPENINGPRICE).getValue());
				lvModel.put(TagName.CLOSINGPRICE, lvRow.getChildNode(
						TagName.CLOSINGPRICE).getValue());
				lvModel.put("CEILING_PRICE", lvRow
						.getChildNode("CEILING_PRICE").getValue());
				lvModel.put("FLOOR_PRICE", lvRow.getChildNode("FLOOR_PRICE")
						.getValue());
				if (IMain.stockHistMap.containsKey(lvInstrumentID)) {
					long lastKey = IMain.stockHistMap.get(lvInstrumentID)
							.lastKey();
					String value = IMain.stockHistMap.get(lvInstrumentID).get(
							lastKey);
					String[] lastObject = value.split("\\|");
					try {
						lvModel.put(TagName.VOLUME, lastObject[4]);
					} catch (Exception e) {
						Log.println(e, Log.ERROR_LOG);
					}
				} else {
					lvModel.put(TagName.VOLUME, lvRow.getChildNode(TagName.VOLUME)
							.getValue());
				}
				
				lvModel.put("BEST_BID_1_PRICE", lvRow.getChildNode(
						"BEST_BID_1_PRICE").getValue());
				lvModel.put("BEST_BID_2_PRICE", lvRow.getChildNode(
						"BEST_BID_2_PRICE").getValue());
				lvModel.put("BEST_BID_3_PRICE", lvRow.getChildNode(
						"BEST_BID_3_PRICE").getValue());
				lvModel.put("BEST_OFFER_1_PRICE", lvRow.getChildNode(
						"BEST_OFFER_1_PRICE").getValue());
				lvModel.put("BEST_OFFER_2_PRICE", lvRow.getChildNode(
						"BEST_OFFER_2_PRICE").getValue());
				lvModel.put("BEST_OFFER_3_PRICE", lvRow.getChildNode(
						"BEST_OFFER_3_PRICE").getValue());
				lvModel.put("BEST_BID_1_VOLUME", lvRow.getChildNode(
						"BEST_BID_1_VOLUME").getValue());
				lvModel.put("BEST_BID_2_VOLUME", lvRow.getChildNode(
						"BEST_BID_2_VOLUME").getValue());
				lvModel.put("BEST_BID_3_VOLUME", lvRow.getChildNode(
						"BEST_BID_3_VOLUME").getValue());
				lvModel.put("BEST_OFFER_1_VOLUME", lvRow.getChildNode(
						"BEST_OFFER_1_VOLUME").getValue());
				lvModel.put("BEST_OFFER_2_VOLUME", lvRow.getChildNode(
						"BEST_OFFER_2_VOLUME").getValue());
				lvModel.put("BEST_OFFER_3_VOLUME", lvRow.getChildNode(
						"BEST_OFFER_3_VOLUME").getValue());
				lvRows.add(lvModel);

				lvCheckedList.append(lvRow.getChildNode(TagName.INSTRUMENTID)
						.getValue().trim()
						+ ",");
			}

			HashMap lvIndex = null;
			IMsgXMLNodeList lvDetailList = mvReturnNode
					.getNodeList(TagName.DetailNode);
			// for (int i = 0; i < lvDetailList.size(); i++)
			// {
			
			if(lvDetailList.size() > 0)
			{
				IMsgXMLNode lvDetailNode = lvDetailList.getNode(0);
				lvIndex = new HashMap();

				lvIndex.put(TagName.MARKETID, lvDetailNode.getChildNode(
						TagName.MARKETID).getValue());
				lvIndex.put("Date", lvDetailNode.getChildNode("Date").getValue());
				lvIndex.put("Index_ID", lvDetailNode.getChildNode("Index_ID")
						.getValue());
				lvIndex.put("Total_Volume", lvDetailNode.getChildNode(
						"Total_Volume").getValue());
				lvIndex.put("Up_Volume", lvDetailNode.getChildNode("Up_Volume")
						.getValue());
				lvIndex.put("PCT_Index", lvDetailNode.getChildNode("PCT_Index")
						.getValue());
				lvIndex.put("Total_Trade", lvDetailNode.getChildNode("Total_Trade")
						.getValue());				
			}
				
		
			// }

			lvReturn.add(lvCheckedList.toString());
			lvReturn.add(lvRows);
			lvReturn.add(lvIndex);
		}
		return lvReturn;
	}

	/**
	 * Get method for client id
	 * 
	 * @return client id
	 */
	public String getClientID() {
		return mvClientID;
	}

	/**
	 * Set method for client id
	 * 
	 * @param pClientID
	 *            the client id
	 */
	public void setClientID(String pClientID) {
		mvClientID = pClientID;
	}

	/**
	 * Get method for market id
	 * 
	 * @return market id
	 */
	public String getMarketID() {
		return mvMarketID;
	}

	/**
	 * Set method for market id
	 * 
	 * @param pMarketID
	 *            market id
	 */
	public void setMarketID(String pMarketID) {
		mvMarketID = pMarketID;
	}

	/**
	 * Get method for instrument list
	 * 
	 * @return instrument list
	 */
	public String getInstrumentList() {
		return mvInstrumentList;
	}

	/**
	 * Set method for instrument list
	 * 
	 * @param pInstrumentList
	 *            the instrument list
	 */
	public void setInstrumentList(String pInstrumentList) {
		mvInstrumentList = pInstrumentList;
	}

	// Begin TASK#: KN00008 KEVIN NG 20080104
	/**
	 * Get method for in market index
	 * 
	 * @return in market index
	 */
	public String getIsMarketIndex() {
		return mvIsMarketIndex;
	}

	/**
	 * Set method for in market index
	 * 
	 * @param pIsMarketIndex
	 *            the in market index
	 */
	public void setIsMarketIndex(String pIsMarketIndex) {
		mvIsMarketIndex = pIsMarketIndex;
	}

	// END TASK#: KN00008
	/**
	 * Get method for loop counter
	 * 
	 * @return Loop counter counts the number of
	 */
	public int getLoopCounter() {
		int lvCnt;
		try {
			lvCnt = Integer.parseInt(mvLoopCounter);
		} catch (Exception e) {
			return -1;
		}
		return lvCnt;
	}

	/**
	 * Set method for loop counter
	 * 
	 * @param pLoopCounter
	 *            the loop counter
	 */
	public void setLoopCounter(String pLoopCounter) {
		mvLoopCounter = pLoopCounter;
	}

}
