package com.ttl.old.itrade.hks.txn;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

import java.util.Hashtable;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.ttl.old.itrade.util.Presentation;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * The HKSExerciseStatusEnquiryTxn class definition for all method
 * Obtain the query plan from the server-side state information
 * @author not attributable
 *
 */
public class HKSExerciseStatusEnquiryTxn
{
    private String mvClientId;
    private String mvTradingAccSeq;
    private String mvAccountSeq;
    private int mvReturnCode;
    private String mvErrorCode;
    private String mvErrorMessage;

    private HKSExerciseDetails[]   mvHKSExerciseDetails;


    public static final String LOOP = "LOOP";
    public static final String LOOP_ELEMENT = "LOOP_ELEMENT";

    /**
     * This variable is used to the tp Error
     */
    TPErrorHandling tpError;
    Presentation presentation = null;

    /**
     * Default constructor for HKSExerciseStatusEnquiryTxn class
     * @param pPresentation the presentation
     */
    public HKSExerciseStatusEnquiryTxn(Presentation pPresentation)
    {
        tpError = new TPErrorHandling();
        presentation = pPresentation;
    }
    ;

    /**
     * Default constructor for HKSExerciseStatusEnquiryTxn class
     * @param pPresentation the presentation
     * @param pClientId the client id 
     * @param pTradingAccSeq the trading account seq
     * @param pAccountSeq the account seq
     */
    public HKSExerciseStatusEnquiryTxn(Presentation pPresentation, String pClientId, String pTradingAccSeq, String pAccountSeq)
    {
        this(pPresentation);
        setClientId(pClientId);
        setTradingAccSeq(pTradingAccSeq);
        setAccountSeq(pAccountSeq);
    }
    /**
     * This method process Obtain the query plan from the server-side state information
     * @param pPresentation the Presentation class
     */
    public void process(Presentation pPresentation)
    {
       try
       {
           Hashtable		lvTxnMap = new Hashtable();

           IMsgXMLNode		lvRetNode;
           TPManager ivTPManager = ITradeServlet.getTPManager(RequestName.HKSExerciseStatusRequest);
           TPBaseRequest   lvExerciseicationStatusEnquiry = ivTPManager.getRequest(RequestName.HKSExerciseStatusRequest);

           lvTxnMap.put(TagName.CLIENTID, getClientId());
           lvTxnMap.put(TagName.TRADINGACCSEQ, getTradingAccSeq());
           lvTxnMap.put(TagName.ACCOUNTSEQ, getAccountSeq());

           lvRetNode = lvExerciseicationStatusEnquiry.send(lvTxnMap);

           setReturnCode(tpError.checkError(lvRetNode));
           if (mvReturnCode != TPErrorHandling.TP_NORMAL)
           {
               setErrorMessage(tpError.getErrDesc());
               if (mvReturnCode == TPErrorHandling.TP_APP_ERR)
               {
                   setErrorCode(tpError.getErrCode());
               }
           }
           else
           {
               IMsgXMLNodeList lvStatusNodeList = lvRetNode.getChildNode(LOOP).getNodeList(LOOP_ELEMENT);

               if (lvStatusNodeList == null || lvStatusNodeList.size() == 0)
               {
                   mvHKSExerciseDetails = new HKSExerciseDetails[0];
                   return;
               }

               int lvSize = lvStatusNodeList.size();

               mvHKSExerciseDetails = new HKSExerciseDetails[lvSize];

               for (int i = 0; i < lvSize; i++)
               {
                     mvHKSExerciseDetails[i] = new HKSExerciseDetails();

                     mvHKSExerciseDetails[i].setClientID(lvStatusNodeList.getNode(i).getChildNode(TagName.CLIENTID).getValue());
                     mvHKSExerciseDetails[i].setTradingAccSeq(lvStatusNodeList.getNode(i).getChildNode(TagName.TRADINGACCSEQ).getValue());
                     mvHKSExerciseDetails[i].setCurrencyID(lvStatusNodeList.getNode(i).getChildNode(TagName.CURRENCYID).getValue());
                     mvHKSExerciseDetails[i].setEntitlementID(lvStatusNodeList.getNode(i).getChildNode(TagName.ENTITLEMENTID).getValue());
                     mvHKSExerciseDetails[i].setSubscriptionID(lvStatusNodeList.getNode(i).getChildNode(TagName.SUBSCRIPTIONID).getValue());

                     if(lvStatusNodeList.getNode(i).getChildNode(TagName.TYPE).getValue().equals("O"))
                     {
                             mvHKSExerciseDetails[i].setBookCloseInstrumentID(lvStatusNodeList.getNode(i).getChildNode(TagName.BOOKCLOSEINSTRUMENTID).getValue());
                     }else{
                             mvHKSExerciseDetails[i].setBookCloseInstrumentID("");
                     }

                     mvHKSExerciseDetails[i].setMarketID(lvStatusNodeList.getNode(i).getChildNode(TagName.MARKETID).getValue());
                     mvHKSExerciseDetails[i].setProductID(lvStatusNodeList.getNode(i).getChildNode(TagName.PRODUCTID).getValue());
                     mvHKSExerciseDetails[i].setType(lvStatusNodeList.getNode(i).getChildNode(TagName.TYPE).getValue());
                     mvHKSExerciseDetails[i].setTypeDescription(lvStatusNodeList.getNode(i).getChildNode("TYPEDESCRIPTION").getValue());
                     mvHKSExerciseDetails[i].setExercisableQty(lvStatusNodeList.getNode(i).getChildNode(TagName.EXERCISABLEQTY).getValue());
                     mvHKSExerciseDetails[i].setExcessQty(lvStatusNodeList.getNode(i).getChildNode("EXCESSQTY").getValue());
                     mvHKSExerciseDetails[i].setExerciseQty(lvStatusNodeList.getNode(i).getChildNode("EXERCISEQTY").getValue());
                     mvHKSExerciseDetails[i].setStatus(lvStatusNodeList.getNode(i).getChildNode(TagName.STATUS).getValue());
                     mvHKSExerciseDetails[i].setNumberOfSharesHeld(lvStatusNodeList.getNode(i).getChildNode("NUMBEROFSHARESHELD").getValue());
                     mvHKSExerciseDetails[i].setNumOfSharesHeldBalanceDate(lvStatusNodeList.getNode(i).getChildNode("BALANCEDATE").getValue());
                     mvHKSExerciseDetails[i].setNameofRightsIssue(lvStatusNodeList.getNode(i).getChildNode("NAMEOFRIGHTSISSUE").getValue());
                     mvHKSExerciseDetails[i].setNumOfSharesAvailableOfSubscription(lvStatusNodeList.getNode(i).getChildNode("NUMOFSHARESAVAILABLEOFSUB").getValue());
                     mvHKSExerciseDetails[i].setIssuePrice(lvStatusNodeList.getNode(i).getChildNode(TagName.ISSUEPRICE).getValue());
                     mvHKSExerciseDetails[i].setNumOfSharesAvailableForAcquisition(lvStatusNodeList.getNode(i).getChildNode("NUMOFSHARESAVAILABLEFORACQ").getValue());
                     mvHKSExerciseDetails[i].setProposedAcquistionPrice(lvStatusNodeList.getNode(i).getChildNode("PROPOSEDACQUISTIONPRICE").getValue());
                     mvHKSExerciseDetails[i].setClosingDate(lvStatusNodeList.getNode(i).getChildNode("CLOSINGDATE").getValue());
                     mvHKSExerciseDetails[i].setStatusDescription(lvStatusNodeList.getNode(i).getChildNode("STATUSDESCRIPTION").getValue());
                     mvHKSExerciseDetails[i].setOffercurrencyID(lvStatusNodeList.getNode(i).getChildNode("OFFERCURRENCYID").getValue());
                     mvHKSExerciseDetails[i].setExerciseRatioDelivery(lvStatusNodeList.getNode(i).getChildNode("EXERCISERATIODELIVERY").getValue());
                     mvHKSExerciseDetails[i].setExerciseRatioPer(lvStatusNodeList.getNode(i).getChildNode("EXERCISERATIOPER").getValue());
                     mvHKSExerciseDetails[i].setExIssueInstrumentID(lvStatusNodeList.getNode(i).getChildNode("EXISSUEINSTRUMENTID").getValue());
                     mvHKSExerciseDetails[i].setAccountSeq(lvStatusNodeList.getNode(i).getChildNode(TagName.ACCOUNTSEQ).getValue());
                     mvHKSExerciseDetails[i].setLocationID(lvStatusNodeList.getNode(i).getChildNode(TagName.LOCATIONID).getValue());
                     //mvHKSExerciseDetails[i].setProvisionalCode(lvStatusNodeList.getNode(i).getChildNode("PROVISIONALCODE").getValue());
                     mvHKSExerciseDetails[i].setInstrumentID(lvStatusNodeList.getNode(i).getChildNode("INSTRUMENTID").getValue());
                     mvHKSExerciseDetails[i].setAllowExcessExerciseFlag(lvStatusNodeList.getNode(i).getChildNode("ALLOWEXCESSEXERCISEFLAG").getValue());
               }
           }
       }
       catch (Exception e)
       {
           Log.println( e , Log.ERROR_LOG);
       }
    }

  /**
   * Get method for Client ID
   * @return the Client ID
   */
    public String getClientId()
    {
        return mvClientId;
    }

    /**
     * Set method for Client ID
     * @param pClientId the Client ID
     */
    public void setClientId(String pClientId)
    {
        mvClientId = pClientId;
    }
    /**
     * Get method for trading account sequence
     * @return the trading account sequence
     */
    public String getTradingAccSeq()
    {
        return mvTradingAccSeq;
    }

   /**
    * Set method for trading account sequence
    * @param pTradingAccSeq the trading account sequence
    */
    public void setTradingAccSeq(String pTradingAccSeq)
    {
        mvTradingAccSeq = pTradingAccSeq;
    }
    /**
     * Get method for account sequence
     * @return account sequence
     */
    public String getAccountSeq()
    {
        return mvAccountSeq;
    }

    /**
     * Set method for account sequence
     * @param pAccountSeq the account sequence
     */
    public void setAccountSeq(String pAccountSeq)
    {
        mvAccountSeq = pAccountSeq;
    }




   /**
    * Get method for IPO Application Status Details
    * @return Array of IPO Application Status Details
    */
    public HKSExerciseDetails[] getExerciseDetails()
    {
        return mvHKSExerciseDetails;
    }

    /**
     * Set method for IPO Application Status Details
     * @param pHKSExerciseDetails the Array of IPO Application Status Details
     */
    public void setExerciseDetails(HKSExerciseDetails[] pHKSExerciseDetails)
    {
        mvHKSExerciseDetails = pHKSExerciseDetails;
    }

  /**
   * Get method for System Return Code
   * @return System Return Code
   */
    public int getReturnCode()
    {
        return mvReturnCode;
    }

    /**
     * Set method for Systme Return Code
     * @param pReturnCode the Systme Return Code
     */
    public void setReturnCode(int pReturnCode)
    {
        mvReturnCode = pReturnCode;
    }

  /**
   * Get method for System Error Code
   * @return the System Error Code
   */
    public String getErrorCode()
    {
        return mvErrorCode;
    }

   /**
    * Set method for System Error Code
    * @param pErrorCode the System Error Code
    */
    public void setErrorCode(String pErrorCode)
    {
        mvErrorCode = pErrorCode;
    }

    /**
     * Get method for System Error Message
     * @return System Error Message
     */
    public String getErrorMessage()
    {
        return mvErrorMessage;
    }

   /**
    * Set method for System Error Message
    * @param pErrorMessage the System Error Message
    */
    public void setErrorMessage(String pErrorMessage)
    {
        mvErrorMessage = pErrorMessage;
    }
    /**
     * This method for localized system erroe messsage
     * @param pErrorCode The system error code
     * @param pDefaultMesg the defalut message
     * @return system erroe messsage
     */
   public String getLocalizedErrorMessage(String pErrorCode, String pDefaultMesg)
   {
      String lvRetMessage;

      try
      {
         if (pErrorCode.equals("0") || pErrorCode.equals("999"))
         {
            lvRetMessage = pDefaultMesg;
         }
         else
         {
            lvRetMessage = (String)presentation.getErrorCodeBundle().getString(pErrorCode);
         }
      }
      catch (Exception ex)
      {
         Log.println("Presentation._showPage: missing error mapping for " + pErrorCode + ", Default = " + pDefaultMesg, Log.ERROR_LOG);

         lvRetMessage = pDefaultMesg;
      }

      return lvRetMessage;
   }
}
