package com.ttl.old.itrade.hks.bean;

/**
 * The HKSGenCancelOrderBean class define variables that to save values 
 * for action 
 * @author Wind Zhao
 *
 */
public class HKSGenCancelOrderBean {
	private String mvIsPasswordSaved;
	private String mvPasswordConfirmation;
	private String mvSCodeEnableForOrdersCancel;
	private String mvSavePasswordLabel;
	private String mvOrderId;
	private String mvOrderIdValue;
	private String mvOrderGroupId;
	private String mvPrice;
	private String mvPriceValue; 
	private String mvQuantity;
	private String mvQuantityValue;
	private String mvCancelledQty;
	private String mvFilledQty;
	private String mvOSQty;
	private String mvOSQtyVaule;
	private String mvOrderType;
	private String mvOrderTypeValue;
	private String mvMarketId;
	private String mvCurrencyId;
	private String mvNetAmt;
	private String mvInstrumentName;
	private String mvStockId;
	private String mvAccountNum;
	private String mvBuyOrSell;
	private String mvBSValue;
	private String mvDateTime;
	private String mvStatus;
	private String mvTime_Date;
	private String mvGoodTillDate;
	private String mvGoodTillDateDisable;
	private String mvAONDisable;
	private String mvTriggerDisable;
	private String mvMultiMarketDisable;
	private String mvQuantityDescription;
	private String mvOrderTypeDescription;
	private String mvGoodtillDescription;
	private String mvGrossAmt;
	private String mvFeesCommission;
	private String mvTriggerPrice;
	private String mvTotalCost;
	private String mvStopTypeValue;
	private String mvStopPriceValue;
	private String mvAllorNothing;
	private String mvInputTime;
    //Begin Task #: TTL-GZ-ZZW-00011 Wind Zhao 20091118 for ITradeR5 vntrade svn
    
	/*
     * mvCancelOrderFor to control hidden or show some fields in 
     * cancel  order
     */
    private String mvCancelOrderFor;
    
    /*
     * mvSecurityCodeEnable is control hidden or show security Code field
     */
    private String mvSecurityCodeEnable;
    
    /**
     * This method returns the a symbol
     * to control security code show or hidden .
     * @return the symbol to control security code show or hidden.
     */
    public String getMvSecurityCodeEnable() {
		return mvSecurityCodeEnable;
	}
   
    /**
     * This method sets the a symbol
     * to control security code show or hidden .
     * @param pSecurityCodeEnable The symbol to control security code show or hidden.
     */
	public void setMvSecurityCodeEnable(String pSecurityCodeEnable) {
		mvSecurityCodeEnable = pSecurityCodeEnable;
	}
	
	/**
     * This method returns the symbol
     * to control show or hidden some fields.
     * @return the cancel order for value.
     */
	public String getMvCancelOrderFor() {
    	return mvCancelOrderFor;
    }
	
	/**
     * This method sets the a symbol
     * to control show or hidden some fields.
     * @param pCancelOrderFor The cancel order for value.
     */
    public void setMvCancelOrderFor(String pCancelOrderFor) {
    	mvCancelOrderFor = pCancelOrderFor;
    }
    //End Task #: TTL-GZ-ZZW-00011 Wind Zhao 20091118 for ITradeR5 vntrade svn
    
    /**
     * This method returns the order id.
     * @return the order id of the order.
     */
	public String getMvOrderIdValue() {
		return mvOrderIdValue;
	}
	
	/**
     * This method sets the order id.
     * @param pOrderIdValue The order id.
     */
	public void setMvOrderIdValue(String pOrderIdValue) {
		mvOrderIdValue = pOrderIdValue;
	}
	
	/**
     * This method returns the all or nothing.
     * @return the all or nothing of the order.
     */
	public String getMvAllorNothing() {
		return mvAllorNothing;
	}
	
	/**
     * This method sets the all or nothing.
     * @param pAllorNothing The all or nothing of order.
     */
	public void setMvAllorNothing(String pAllorNothing) {
		mvAllorNothing = pAllorNothing;
	}
	
	/**
     * This method returns the stop price of order.
     * @return the stop price of order.
     */
	public String getMvStopPriceValue() {
		return mvStopPriceValue;
	}
	
	/**
     * This method sets the stop price of order.
     * @param pStopPriceValue The stop price of order.
     */
	public void setMvStopPriceValue(String pStopPriceValue) {
		mvStopPriceValue = pStopPriceValue;
	}
	
	/**
     * This method returns the stop type of order.
     * @return the stop type of order.
     */
	public String getMvStopTypeValue() {
		return mvStopTypeValue;
	}
	
	/**
     * This method sets the stop type of order.
     * @param pStopTypeValue The stop type of order.
     */
	public void setMvStopTypeValue(String pStopTypeValue) {
		mvStopTypeValue = pStopTypeValue;
	}
	
	/**
     * This method returns the password if saved or not.
     * @return the password if saved or not.
     *         [0] Y is hidden the password field.
     *         [1] N is show the password field.
     */
	public String getMvIsPasswordSaved() {
		return mvIsPasswordSaved;
	}
	
	/**
     * This method sets the password if saved or not.
     * @param pIsPasswordSaved The password if saved or not.
     */
	public void setMvIsPasswordSaved(String pIsPasswordSaved) {
		mvIsPasswordSaved = pIsPasswordSaved;
	}
	
	/**
     * This method returns the password if confirm or not.
     * @return the password if confirm or not.
     *         [0] Y is confirm the password.
     *         [1] N is not confirm the password.
     */
	public String getMvPasswordConfirmation() {
		return mvPasswordConfirmation;
	}
	
	/**
     * This method sets the password if confirm or not.
     * @param pPasswordConfirmation The password if confirm or not.
     */
	public void setMvPasswordConfirmation(String pPasswordConfirmation) {
		mvPasswordConfirmation = pPasswordConfirmation;
	}
	
	/**
     * This method returns the security code if need enter or not.
     * @return the security code if show or hidden for cancel order.
     *         [0] true is need to enter security code.
     *         [1] false is not need to enter security code.
     */
	public String getMvSCodeEnableForOrdersCancel() {
		return mvSCodeEnableForOrdersCancel;
	}
	
	/**
     * This method sets the security code if need enter or not.
     * @param pSCodeEnableForOrdersCancel The security code if need enter or not.
     */
	public void setMvSCodeEnableForOrdersCancel(String pSCodeEnableForOrdersCancel) {
		mvSCodeEnableForOrdersCancel = pSCodeEnableForOrdersCancel;
	}
	
	/**
     * This method returns the save password label.
     * @return the save password label.
     */
	public String getMvSavePasswordLabel() {
		return mvSavePasswordLabel;
	}
	
	/**
     * This method sets the save password label.
     * @param pSavePasswordLabel The save password label.
     */
	public void setMvSavePasswordLabel(String pSavePasswordLabel) {
		mvSavePasswordLabel = pSavePasswordLabel;
	}
	
	/**
     * This method returns the order id.
     * @return the order id of the order.
     */
	public String getMvOrderId() {
		return mvOrderId;
	}
	
	/**
     * This method sets the order id.
     * @param pOrderId The order id.
     */
	public void setMvOrderId(String pOrderId) {
		mvOrderId = pOrderId;
	}
	
	/**
     * This method returns the order group id.
     * @return the order group id of the order.
     */
	public String getMvOrderGroupId() {
		return mvOrderGroupId;
	}
	
	/**
     * This method sets the order group id.
     * @param pOrderGroupId The order group id of the order.
     */
	public void setMvOrderGroupId(String pOrderGroupId) {
		mvOrderGroupId = pOrderGroupId;
	}
	
	/**
     * This method returns the price.
     * @return the price of the order is formatted.
     */
	public String getMvPrice() {
		return mvPrice;
	}
	
	/**
     * This method sets the price.
     * @param pPrice The price of the order is formatted.
     */
	public void setMvPrice(String pPrice) {
		mvPrice = pPrice;
	}
	
	/**
     * This method returns the price.
     * @return the price of the order.
     */
	public String getMvPriceValue() {
		return mvPriceValue;
	}
	
	/**
     * This method sets the price.
     * @param pPriceValue The price of the order.
     */
	public void setMvPriceValue(String pPriceValue) {
		mvPriceValue = pPriceValue;
	}
	
	/**
     * This method returns the quantity.
     * @return the quantity of the order is formatted.
     */
	public String getMvQuantity() {
		return mvQuantity;
	}
	
	/**
     * This method sets the quantity.
     * @param pQuantity The quantity of the order is formatted.
     */
	public void setMvQuantity(String pQuantity) {
		mvQuantity = pQuantity;
	}
	
	/**
     * This method returns the quantity.
     * @return the quantity of the order.
     */
	public String getMvQuantityValue() {
		return mvQuantityValue;
	}
	
	/**
     * This method sets the quantity.
     * @param pQuantityValue The quantity of the order.
     */
	public void setMvQuantityValue(String pQuantityValue) {
		mvQuantityValue = pQuantityValue;
	}
	
	/**
     * This method returns the cancel quantity.
     * @return the cancel quantity of the order.
     */
	public String getMvCancelledQty() {
		return mvCancelledQty;
	}
	
	/**
     * This method sets the cancel quantity.
     * @param pCancelledQty The cancel quantity of the order.
     */
	public void setMvCancelledQty(String pCancelledQty) {
		mvCancelledQty = pCancelledQty;
	}
	
	/**
     * This method returns the filled quantity.
     * @return the filled quantity of the order.
     */
	public String getMvFilledQty() {
		return mvFilledQty;
	}
	
	/**
     * This method sets the filled quantity.
     * @param  pFilledQty The filled quantity of the order.
     */
	public void setMvFilledQty(String pFilledQty) {
		mvFilledQty = pFilledQty;
	}
	
	/**
     * This method returns the out standing quantity.
     * @return the out standing quantity of the order.
     */
	public String getMvOSQty() {
		return mvOSQty;
	}
	
	/**
     * This method sets the out standing quantity.
     * @param pOSQty The out standing quantity of the order is formatted.
     */
	public void setMvOSQty(String pOSQty) {
		mvOSQty = pOSQty;
	}
	
	/**
     * This method returns the out standing quantity.
     * @return the out standing quantity of the order.
     */
	public String getMvOSQtyVaule() {
		return mvOSQtyVaule;
	}
	
	/**
     * This method sets the out standing quantity.
     * @param pOSQtyVaule The out standing quantity of the order.
     */
	public void setMvOSQtyVaule(String pOSQtyVaule) {
		mvOSQtyVaule = pOSQtyVaule;
	}
	
	/**
     * This method returns the order type.
     * @return the order type of the order is formatted.
     */
	public String getMvOrderType() {
		return mvOrderType;
	}
	
	/**
     * This method sets the order type.
     * @param pOrderType The order type of the order is formatted.
     */
	public void setMvOrderType(String pOrderType) {
		mvOrderType = pOrderType;
	}
	
	/**
     * This method returns the order type.
     * @return the order type of the order.
     */
	public String getMvOrderTypeValue() {
		return mvOrderTypeValue;
	}
	
	/**
     * This method sets the order type.
     * @param pOrderTypeValue The order type of the order
     */
	public void setMvOrderTypeValue(String pOrderTypeValue) {
		mvOrderTypeValue = pOrderTypeValue;
	}
	
	/**
     * This method returns the market id.
     * @return the market id of the order.
     */
	public String getMvMarketId() {
		return mvMarketId;
	}
	
	/**
     * This method sets the market id.
     * @param pMarketId The market id of the order.
     */
	public void setMvMarketId(String pMarketId) {
		mvMarketId = pMarketId;
	}
	
	/**
     * This method returns the currency id.
     * @return the currency id of the order.
     */
	public String getMvCurrencyId() {
		return mvCurrencyId;
	}
	
	/**
     * This method sets the currency id.
     * @param pCurrencyId The currency id of the order.
     */
	public void setMvCurrencyId(String pCurrencyId) {
		mvCurrencyId = pCurrencyId;
	}
	
	/**
     * This method returns the net amount of order.
     * @return the net amount of order is formatted.
     */
	public String getMvNetAmt() {
		return mvNetAmt;
	}
	
	/**
     * This method sets the net amount of order.
     * @param pNetAmt The net amount of order is formatted.
     */
	public void setMvNetAmt(String pNetAmt) {
		mvNetAmt = pNetAmt;
	}
	
	/**
     * This method returns the instrument name.
     * @return the instrument name of the order.
     */
	public String getMvInstrumentName() {
		return mvInstrumentName;
	}
	
	/**
     * This method sets the instrument name.
     * @param pInstrumentName The instrument name of the order.
     */
	public void setMvInstrumentName(String pInstrumentName) {
		mvInstrumentName = pInstrumentName;
	}
	
	/**
     * This method returns the stock id.
     * @return the stock id of the order.
     */
	public String getMvStockId() {
		return mvStockId;
	}
	
	/**
     * This method sets the stock id.
     * @param pStockId The stock id of the order.
     */
	public void setMvStockId(String pStockId) {
		mvStockId = pStockId;
	}
	
	/**
     * This method returns the account number.
     * @return the account number of the client.
     */
	public String getMvAccountNum() {
		return mvAccountNum;
	}
	
	/**
     * This method sets the account number.
     * @param pAccountNum The account number of client.
     */
	public void setMvAccountNum(String pAccountNum) {
		mvAccountNum = pAccountNum;
	}
	
	/**
     * This method returns the buy or sell.
     * @return the buy or sell of the order.
     *         [0] Buy.
     *         [1] Sell
     */
	public String getMvBuyOrSell() {
		return mvBuyOrSell;
	}
	
	/**
     * This method sets the buy or sell.
     * @param pBuyOrSell The buy or sell of the order.
     */
	public void setMvBuyOrSell(String pBuyOrSell) {
		mvBuyOrSell = pBuyOrSell;
	}
	
	/**
     * This method returns the B(Buy) or S(Sell).
     * @return the B(Buy) or S(sell) of the order.
     */
	public String getMvBSValue() {
		return mvBSValue;
	}
	
	/**
     * This method sets the B(Buy) or S(Sell).
     * @param pBSValue The B(Buy) or S(Sell) of the order.
     */
	public void setMvBSValue(String pBSValue) {
		mvBSValue = pBSValue;
	}
	
	/**
     * This method returns the date time.
     * @return the date time of the order.
     */
	public String getMvDateTime() {
		return mvDateTime;
	}
	
	/**
     * This method sets the date time.
     * @param pDateTime The date time of the order.
     */
	public void setMvDateTime(String pDateTime) {
		mvDateTime = pDateTime;
	}
	
	/**
     * This method returns the status.
     * @return the status of the order.
     */
	public String getMvStatus() {
		return mvStatus;
	}
	/**
     * This method sets the status.
     * @param pStatus The status of the order.
     */
	public void setMvStatus(String pStatus) {
		mvStatus = pStatus;
	}
	
	/**
     * This method returns the time date.
     * @return the time date of the order.
     */
	public String getMvTime_Date() {
		return mvTime_Date;
	}
	
	/**
     * This method sets the time date.
     * @param pDateTime The time date of the order.
     */
	public void setMvTime_Date(String pTime_Date) {
		mvTime_Date = pTime_Date;
	}
	
	/**
     * This method returns the good till date.
     * @return the good till date of the order is formatted.
     */
	public String getMvGoodTillDate() {
		return mvGoodTillDate;
	}
	
	/**
     * This method sets the good till date.
     * @param pGoodTillDate The good till date of the order is formatted.
     */
	public void setMvGoodTillDate(String pGoodTillDate) {
		mvGoodTillDate = pGoodTillDate;
	}
	
	/**
     * This method returns the good till date if disable.
     * @return the good till date if disable of the order.
     */
	public String getMvGoodTillDateDisable() {
		return mvGoodTillDateDisable;
	}
	
	/**
     * This method sets the good till date if disable.
     * @param pGoodTillDateDisable The good till date if disable of the order.
     */
	public void setMvGoodTillDateDisable(String pGoodTillDateDisable) {
		mvGoodTillDateDisable = pGoodTillDateDisable;
	}
	
	/**
     * This method returns the all or nothing if disable.
     * @return the all or nothing if disable of the order.
     */
	public String getMvAONDisable() {
		return mvAONDisable;
	}
	
	/**
     * This method sets the all or nothing if disable.
     * @param pAONDisable The all or nothing if disable of the order.
     */
	public void setMvAONDisable(String pAONDisable) {
		mvAONDisable = pAONDisable;
	}
	
	/**
     * This method returns the trigger order if disable.
     * @return the trigger order if disable of the order.
     */
	public String getMvTriggerDisable() {
		return mvTriggerDisable;
	}
	
	/**
     * This method sets the trigger order if disable.
     * @param pTriggerDisable The trigger order if disable of the order.
     */
	public void setMvTriggerDisable(String pTriggerDisable) {
		mvTriggerDisable = pTriggerDisable;
	}
	
	/**
     * This method returns the multi market disable.
     * @return the multi market disable of the order.
     */
	public String getMvMultiMarketDisable() {
		return mvMultiMarketDisable;
	}
	
	/**
     * This method sets the multi market disable.
     * @param pMultiMarketDisable The multi market disable of the order.
     */
	public void setMvMultiMarketDisable(String pMultiMarketDisable) {
		mvMultiMarketDisable = pMultiMarketDisable;
	}
	
	/**
     * This method returns the quantity description.
     * @return the quantity description of the order.
     */
	public String getMvQuantityDescription() {
		return mvQuantityDescription;
	}
	
	/**
     * This method sets the quantity description.
     * @param pQuantityDescription The quantity description of the order.
     */
	public void setMvQuantityDescription(String pQuantityDescription) {
		mvQuantityDescription = pQuantityDescription;
	}
	
	/**
     * This method returns the order type description.
     * @return the order type description of the order.
     */
	public String getMvOrderTypeDescription() {
		return mvOrderTypeDescription;
	}
	
	/**
     * This method sets the order type description.
     * @param pOrderTypeDescription The order type description of the order.
     */
	public void setMvOrderTypeDescription(String pOrderTypeDescription) {
		mvOrderTypeDescription = pOrderTypeDescription;
	}
	
	/**
     * This method returns the good till date description.
     * @return the good till date description of the order.
     */
	public String getMvGoodtillDescription() {
		return mvGoodtillDescription;
	}
	
	/**
     * This method sets the good till date description.
     * @param pGoodTillDescription The good till date description of the order.
     */
	public void setMvGoodtillDescription(String pGoodtillDescription) {
		mvGoodtillDescription = pGoodtillDescription;
	}
	
	/**
     * This method returns the gross amount of order.
     * @return the gross amount of order is formatted.
     */
	public String getMvGrossAmt() {
		return mvGrossAmt;
	}
	
	/**
     * This method sets the gross amount of order.
     * @param pGrossAmt The gross amount of order is formatted.
     */
	public void setMvGrossAmt(String pGrossAmt) {
		mvGrossAmt = pGrossAmt;
	}
	
	/**
     * This method returns the fees commission of order.
     * @return the fees commission of order is formatted.
     */
	public String getMvFeesCommission() {
		return mvFeesCommission;
	}
	
	/**
     * This method sets the fees commission of order.
     * @param pFeesCommission The fees commission of order is formatted.
     */
	public void setMvFeesCommission(String pFeesCommission) {
		mvFeesCommission = pFeesCommission;
	}
	
	/**
     * This method returns the trigger price of order.
     * @return the trigger price of order is formatted.
     */
	public String getMvTriggerPrice() {
		return mvTriggerPrice;
	}
	
	/**
     * This method sets the trigger price of order.
     * @param pTriggerPrice The trigger price of order is formatted.
     */
	public void setMvTriggerPrice(String pTriggerPrice) {
		mvTriggerPrice = pTriggerPrice;
	}
	
	/**
     * This method returns the total cost of order.
     * @return the total cost of order is formatted.
     */
	public String getMvTotalCost() {
		return mvTotalCost;
	}
	
	/**
     * This method sets the total cost of order.
     * @param pTotalCost The total cost of order is formatted.
     */
	public void setMvTotalCost(String pTotalCost) {
		mvTotalCost = pTotalCost;
	}

	/**
	 * @param mvInputTime the mvInputTime to set
	 */
	public void setMvInputTime(String mvInputTime) {
		this.mvInputTime = mvInputTime;
	}

	/**
	 * @return the mvInputTime
	 */
	public String getMvInputTime() {
		return mvInputTime;
	}
}