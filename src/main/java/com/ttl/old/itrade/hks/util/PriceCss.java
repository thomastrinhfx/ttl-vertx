/**
 * 
 */
package com.ttl.old.itrade.hks.util;

import java.math.BigDecimal;

/**
 * @since Apr 2, 2014
 * @author duonghuynh
 * 
 */
public class PriceCss {
	private Double referencePrice;
	private Double ceilingPrice;
	private Double floorPrice;

	public PriceCss(Double referencePrice, Double ceilingPrice, Double floorPrice) {
		this.referencePrice = referencePrice;
		this.ceilingPrice = ceilingPrice;
		this.floorPrice = floorPrice;
	}

	public Double getReferencePrice() {
		return referencePrice;
	}

	public void setReferencePrice(Double referencePrice) {
		this.referencePrice = referencePrice;
	}

	public Double getCeilingPrice() {
		return ceilingPrice;
	}

	public void setCeilingPrice(Double ceilingPrice) {
		this.ceilingPrice = ceilingPrice;
	}

	public Double getFloorPrice() {
		return floorPrice;
	}

	public void setFloorPrice(Double floorPrice) {
		this.floorPrice = floorPrice;
	}

	public int getCode(Double thisPrice) {
		if (thisPrice.compareTo(referencePrice) == 0) {
			return 0;
		} else if (thisPrice.compareTo(ceilingPrice) == 0) {
			return 1;
		} else if (thisPrice.compareTo(floorPrice) == 0) {
			return 2;
		} else if (thisPrice.compareTo(referencePrice) > 0) {
			return 3;
		} else {
			return 4;
		}
	}

	public int getCode(BigDecimal thisPrice) {
		return getCode(thisPrice.doubleValue());
	}

	public int getCode(String thisPrice) {
		if(thisPrice == null){
			return 0;
		}
		try {
			Double price = Double.parseDouble(thisPrice);
			return getCode(price);
		} catch (NumberFormatException e) {
			return 5; // not a number, for ex: ATC, ATO
		}		
	}
}
