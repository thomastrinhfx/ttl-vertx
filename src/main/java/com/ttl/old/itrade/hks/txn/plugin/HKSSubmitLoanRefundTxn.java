package com.ttl.old.itrade.hks.txn.plugin;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.hks.util.AgentUtils;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;
import com.systekit.winvest.hks.config.mapping.TagName;

public class HKSSubmitLoanRefundTxn extends BaseTxn {
	
	private String mvClientID;
	private String mvAmount;
	private String mvRemark;
	private String mvPassword;
	private String mvSecurityCode;
	private String mvResult;
	private String mvReturnCode;
	private String mvReturnMsg;
	private String mvName;
	private String mvLoanCur;
	private String mvLoanPay;
	private String mvLoanRem;
	
	public String getMvName() {
		return mvName;
	}
	public void setMvName(String mvName) {
		this.mvName = mvName;
	}
	public String getMvLoanCur() {
		return mvLoanCur;
	}
	public void setMvLoanCur(String mvLoanCur) {
		this.mvLoanCur = mvLoanCur;
	}
	public String getMvLoanPay() {
		return mvLoanPay;
	}
	public void setMvLoanPay(String mvLoanPay) {
		this.mvLoanPay = mvLoanPay;
	}
	public String getMvLoanRem() {
		return mvLoanRem;
	}
	public void setMvLoanRem(String mvLoanRem) {
		this.mvLoanRem = mvLoanRem;
	}
	public String getMvClientID() {
		return mvClientID;
	}
	public void setMvClientID(String mvClientID) {
		this.mvClientID = mvClientID;
	}
	public String getMvRemark() {
		return mvRemark;
	}
	public void setMvRemark(String mvRemark) {
		this.mvRemark = mvRemark;
	}
	public String getMvAmount() {
		return mvAmount;
	}
	public void setMvAmount(String mvAmount) {
		this.mvAmount = mvAmount;
	}
	public String getMvPassword() {
		return mvPassword;
	}
	public void setMvPassword(String mvPassword) {
		this.mvPassword = mvPassword;
	}
	public String getMvSecurityCode() {
		return mvSecurityCode;
	}
	public void setMvSecurityCode(String mvSecurityCode) {
		this.mvSecurityCode = mvSecurityCode;
	}
	public String getMvResult() {
		return mvResult;
	}
	public void setMvResult(String mvResult) {
		this.mvResult = mvResult;
	}
	public String getMvReturnCode() {
		return mvReturnCode;
	}
	public void setMvReturnCode(String mvReturnCode) {
		this.mvReturnCode = mvReturnCode;
	}
	public String getMvReturnMsg() {
		return mvReturnMsg;
	}
	public void setMvReturnMsg(String mvReturnMsg) {
		this.mvReturnMsg = mvReturnMsg;
	}
	
	public HKSSubmitLoanRefundTxn(String pClientID, String pAmount, String pRemark){
		mvClientID = pClientID;
		mvAmount = pAmount;
		mvRemark = pRemark;
	}
	
	public Map<String, String> submitLoanRefundCreation() {
		Map<String, String> result = new HashMap<String, String>();
		Hashtable lvTxnMap = new Hashtable();
		try {
			Log.println("HKSSubmitLoanRefundTxn.submitLoanRefundCreation START: Client ID = " + mvClientID , Log.ACCESS_LOG);
			
			 //END HIEU LE
			lvTxnMap.put(TagName.CLIENTID, mvClientID);
			lvTxnMap.put(TagName.AMOUNT, mvAmount);
			lvTxnMap.put(TagName.REMARK, mvRemark);
			lvTxnMap.put("FULLNAME", mvName);
			lvTxnMap.put("LOANPAY", mvLoanPay);
			lvTxnMap.put("LOANREMAIN", mvLoanRem);
			lvTxnMap.put("LOANCUR", mvLoanCur);
			lvTxnMap.put("OPRID", AgentUtils.getAgentID(this.getMvClientID()));
			lvTxnMap.put("EMAILLIST",AgentUtils.getOperatorEmails(this.getMvClientID(),AgentUtils.FUNCTION_TYPE_LOAN_REFUND.toUpperCase()));
			// BEGIN: Bao Tran 20140113 issue #VNPJT-1173
			lvTxnMap.put("ISAPPROVAL", IMain.getProperty("IsApprovalLoanRefund"));
			// END: Bao Tran 20140113 issue #VNPJT-1173
			
			
			// Send TP request
			if(TPErrorHandling.TP_NORMAL==process("SubmitLoanRefundCreation",lvTxnMap)){
				setMvResult(mvReturnNode.getChildNode(TagName.RESULT).getValue());
		    	setMvReturnCode(mvReturnNode.getChildNode("RETURNCODE").getValue());
		    	setMvReturnMsg(mvReturnNode.getChildNode(TagName.RETURNMESSAGE).getValue());
			}
		} catch (Exception e) {
			Log.println("HKSSubmitLoanRefundTxn.submitLoanRefundCreation Error: " + e.getMessage(), Log.ERROR_LOG);
		} finally {
			Log.println("HKSSubmitLoanRefundTxn.submitLoanRefundCreation END: Client ID = " + mvClientID, Log.ACCESS_LOG);
			lvTxnMap.clear();
		}
		
		
		return result;
	}
}
