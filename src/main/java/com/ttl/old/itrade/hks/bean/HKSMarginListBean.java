package com.ttl.old.itrade.hks.bean;

/**
 * The HKSTransactionHistoryBean class define variables that to save values for
 * action
 * 
 * @author
 * 
 */
public class HKSMarginListBean {
	
	private String mvInstrumentID;
	private String mvMarketID;
	private String mvInstrumentName;
	private String mvLendPercent;
	private String mvRowNum;
	
	
	public String getMvRowNum() {
		return mvRowNum;
	}

	public void setMvRowNum(String mvRowNum) {
		this.mvRowNum = mvRowNum;
	}

	public String getMvInstrumentID() {
		return mvInstrumentID;
	}
	
	public void setMvInstrumentID(String mvInstrumentID) {
		this.mvInstrumentID = mvInstrumentID;
	}
	
	public String getMvMarketID() {
		return mvMarketID;
	}
	
	public void setMvMarketID(String mvMarketID) {
		this.mvMarketID = mvMarketID;
	}
	
	public String getMvInstrumentName() {
		return mvInstrumentName;
	}
	
	public void setMvInstrumentName(String mvInstrumentName) {
		this.mvInstrumentName = mvInstrumentName;
	}

	public String getMvLendPercent() {
		return mvLendPercent;
	}

	public void setMvLendPercent(String mvLendPercent) {
		this.mvLendPercent = mvLendPercent;
	}
	
	
}
