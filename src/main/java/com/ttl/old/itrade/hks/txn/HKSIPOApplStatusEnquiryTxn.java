package com.ttl.old.itrade.hks.txn;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

import java.util.Hashtable;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.hks.util.HKSInstrument;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.ttl.old.itrade.util.Presentation;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;

/**
 * The HKSIPOApplStatusEnquiryTxn class definition for all method
 * From the host application to query the progress of IPO 
 * @author not attributable
 *
 */
public class HKSIPOApplStatusEnquiryTxn
{
    private String mvClientId;
    // BEGIN RN00028 Ricky Ngan 20080816
    private String mvMarginFinancingType;
    private String mvAllotmentQuery;
    // END RN00028
    private int mvReturnCode;
    private String mvErrorCode;
    private String mvErrorMessage;

    private HKSIPOApplStatusDetails[]   mvHKSIPOApplStatusDetails;
    //Variable is used to identify
    public static final String CLIENTID = "CLIENTID";
    public static final String INSTRUMENTID = "INSTRUMENTID";
    public static final String SUBSCRIPTIONID = "SUBSCRIPTIONID";
    public static final String ENTITLEMENTID = "ENTITLEMENTID";
    public static final String MINOFFERPRICE = "MINOFFERPRICE";
    public static final String MAXOFFERPRICE = "MAXOFFERPRICE";
    public static final String APPLIEDQTY = "APPLIEDQTY";
    public static final String INPUTDATE = "INPUTDATE";
    public static final String STATUS = "STATUS";
    public static final String REVERSESTATUS = "REVERSESTATUS";
    // BEGIN RN00028 Ricky Ngan 20080816
    public static final String CONFIRMEDQTY = "CONFIRMEDQTY";
    public static final String UNALLOTTEDQTY = "UNALLOTTEDQTY";
    public static final String REFUNDAMOUNT = "REFUNDAMOUNT";
    // END RN00028  
    //BEGIN TASK : CL00027 Charlie.liu 20081016
    public static final String CONFIRMEDPRICE = "CONFIRMEDPRICE";
    public static final String INSTRUMENTSHORTNAME = "INSTRUMENTSHORTNAME";
    public static final String INSTRUMENTCHINESESHORTNAME = "INSTRUMENTCHINESESHORTNAME";
    public static String mvMarketID = "HKEX";
    //END CL00027
    //BEGIN RN00040 Ricky Ngan 20081107	
    public static final String ALLOTMENT_RESULT_ENABLE = "ALLOTMENT_RESULT_ENABLE";
    //END RN00040
    
    //Begin Task #RC00194 - Rice Cheng 20090115
    public static final String MARGINFINANCINGTYPE = "MARGINFINANCINGTYPE";
    //End Task #RC00194 - Rice Cheng 20090115
    
    public static final String LOOP = "LOOP";
    public static final String LOOP_ELEMENT = "LOOP_ELEMENT";

    /**
     * This variable is used to the tp Error
     */
    TPErrorHandling tpError;
    Presentation presentation = null;

    /**
     * Default constructor for HKSIPOApplStatusEnquiryTxn class
     * @param Presentation the Presentation class
     */
    public HKSIPOApplStatusEnquiryTxn(Presentation pPresentation)
    {
        tpError = new TPErrorHandling();
        presentation = pPresentation;
    }
    ;

   /**
    * Default constructor for HKSIPOApplStatusEnquiryTxn class
    * @param pPresentation the Presentation class
    * @param pClientId the client id
    * @param pMarginFinancingType the margin financing type
    * @param pAllotmentQuery the allotment query
    */
    // BEGIN RN00028 Ricky Ngan 20080816
    public HKSIPOApplStatusEnquiryTxn(Presentation pPresentation, String pClientId, String pMarginFinancingType, String pAllotmentQuery)
    {
        this(pPresentation);
        setClientId(pClientId);
        setAllotmentQuery(pAllotmentQuery);
        setMarginFinancingType(pMarginFinancingType);
    }
    // END RN00028
    
	//Begin Task: CL00070 Charlie Lau 20090204
    /**
     * Set method for market id
     * @param pMarketID the market id
     */
	public void setMarketID(String pMarketID){
		mvMarketID = pMarketID;
	}
	// End Task: CL00070
	/**
	 * This method process From the host application to query the progress of IPO 
	 * @param pPresentation the Presentation class
	 */
    public void process(Presentation pPresentation)
    {
       try
       {
           Hashtable		lvTxnMap = new Hashtable();

           IMsgXMLNode		lvRetNode;
           TPManager ivTPManager = ITradeServlet.getTPManager(RequestName.HKSIPOApplStatusRequest);
           TPBaseRequest lvIPOApplicationStatusEnquiry = ivTPManager.getRequest(RequestName.HKSIPOApplStatusRequest);

           lvTxnMap.put(CLIENTID, getClientId());
           // BEGIN RN00028 Ricky Ngan 20080816
           lvTxnMap.put("MARGINFINANCINGTYPE", getMarginFinancingType());
           lvTxnMap.put("ALLOTMENTQUERY", getAllotmentQuery());
           // END RN00028
           lvRetNode = lvIPOApplicationStatusEnquiry.send(lvTxnMap);

           setReturnCode(tpError.checkError(lvRetNode));
           if (mvReturnCode != TPErrorHandling.TP_NORMAL)
           {
        	   if (lvRetNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase("TPBASEREQERROR"))
          	 {
          		 mvErrorCode = "0100";
          		 mvErrorMessage = "No connection with TP";
          	 }
          	 else{
               setErrorMessage(tpError.getErrDesc());
               if (mvReturnCode == TPErrorHandling.TP_APP_ERR)
               {
                   setErrorCode(tpError.getErrCode());
               }
          	 }
           }
           else
           {
               IMsgXMLNodeList lvStatusNodeList = lvRetNode.getChildNode(LOOP).getNodeList(LOOP_ELEMENT);

               if (lvStatusNodeList == null || lvStatusNodeList.size() == 0)
               {
                   mvHKSIPOApplStatusDetails = new HKSIPOApplStatusDetails[0];
                   return;
               }

               int lvSize = lvStatusNodeList.size();

               mvHKSIPOApplStatusDetails = new HKSIPOApplStatusDetails[lvSize];

               for (int i = 0; i < lvSize; i++)
               {
                     mvHKSIPOApplStatusDetails[i] = new HKSIPOApplStatusDetails();

                     mvHKSIPOApplStatusDetails[i].setInstrumentID(lvStatusNodeList.getNode(i).getChildNode(INSTRUMENTID).getValue());
                     mvHKSIPOApplStatusDetails[i].setSubscriptionID(lvStatusNodeList.getNode(i).getChildNode(SUBSCRIPTIONID).getValue());
                     mvHKSIPOApplStatusDetails[i].setEntitlementID(lvStatusNodeList.getNode(i).getChildNode(ENTITLEMENTID).getValue());
                     mvHKSIPOApplStatusDetails[i].setClientID(lvStatusNodeList.getNode(i).getChildNode(CLIENTID).getValue());
                     mvHKSIPOApplStatusDetails[i].setAppliedQty(lvStatusNodeList.getNode(i).getChildNode(APPLIEDQTY).getValue());
                     mvHKSIPOApplStatusDetails[i].setMinOfferPrice(lvStatusNodeList.getNode(i).getChildNode(MINOFFERPRICE).getValue());
                     mvHKSIPOApplStatusDetails[i].setMaxOfferPrice(lvStatusNodeList.getNode(i).getChildNode(MAXOFFERPRICE).getValue());
                     mvHKSIPOApplStatusDetails[i].setInputDate(lvStatusNodeList.getNode(i).getChildNode(INPUTDATE).getValue());
                     mvHKSIPOApplStatusDetails[i].setStatus(lvStatusNodeList.getNode(i).getChildNode(STATUS).getValue());
//                     mvHKSIPOApplStatusDetails[i].setReverseStatus(lvStatusNodeList.getNode(i).getChildNode(REVERSESTATUS).getValue());
                     // BEGIN RN00028 Ricky Ngan 20080816
                     mvHKSIPOApplStatusDetails[i].setConfirmedQty(lvStatusNodeList.getNode(i).getChildNode(CONFIRMEDQTY).getValue());
                     mvHKSIPOApplStatusDetails[i].setUnallottedQty(lvStatusNodeList.getNode(i).getChildNode(UNALLOTTEDQTY).getValue());
                     mvHKSIPOApplStatusDetails[i].setRefundAmount(lvStatusNodeList.getNode(i).getChildNode(REFUNDAMOUNT).getValue());
                     // END RN00028
                     //BEGIN TASK : CL00027 Charlie.liu 20081016
                     mvHKSIPOApplStatusDetails[i].setConfirmedPrice(lvStatusNodeList.getNode(i).getChildNode(CONFIRMEDPRICE).getValue());
                     mvHKSIPOApplStatusDetails[i].setInstrumentShortName(lvStatusNodeList.getNode(i).getChildNode(INSTRUMENTSHORTNAME).getValue());
                     mvHKSIPOApplStatusDetails[i].setInstrumentChineseShortName(lvStatusNodeList.getNode(i).getChildNode(INSTRUMENTCHINESESHORTNAME).getValue());
                     //END CL00027
                     // BEGIN RN00040 Ricky Ngan 20081107
                     mvHKSIPOApplStatusDetails[i].setAllotmentResultEnable(lvStatusNodeList.getNode(i).getChildNode(ALLOTMENT_RESULT_ENABLE).getValue());
                     // END RN00040
                     
                     //Begin Task #RC00194 - Rice Cheng 20090115
                     mvHKSIPOApplStatusDetails[i].setMarginFinancingType(lvStatusNodeList.getNode(i).getChildNode(MARGINFINANCINGTYPE).getValue());
                     //End Task #RC00194 - Rice Cheng 20090115
                     
                    //Begin Task: CL00070 Charlie Lau 20090204
                    HKSInstrument lvInstrumentNameMap = new HKSInstrument();

 					lvInstrumentNameMap.setMarketID(mvMarketID);
 					lvInstrumentNameMap.setInstrumentID(lvStatusNodeList.getNode(i).getChildNode(INSTRUMENTID).getValue());
 					lvInstrumentNameMap.setShortName(lvStatusNodeList.getNode(i).getChildNode(INSTRUMENTSHORTNAME).getValue());
 					lvInstrumentNameMap.setChineseShortName(lvStatusNodeList.getNode(i).getChildNode(INSTRUMENTCHINESESHORTNAME).getValue());
 					
 					TPManager.mvInstrumentNameVector.remove(mvMarketID +"|"+ lvStatusNodeList.getNode(i).getChildNode(INSTRUMENTID).getValue().toUpperCase());
                    TPManager.mvInstrumentNameVector.put(mvMarketID +"|"+ lvStatusNodeList.getNode(i).getChildNode(INSTRUMENTID).getValue().toUpperCase(), lvInstrumentNameMap);
                     // End Task: CL00070
               }
           }
       }
       catch (Exception e)
       {
           Log.println( e , Log.ERROR_LOG);
       }
    }

    /**
     * Get method for Client ID
     * @return Client ID
     */
    public String getClientId()
    {
        return mvClientId;
    }
    /**
     * Set method for Client ID
     * @param pClientId the Client ID
     */
    public void setClientId(String pClientId)
    {
        mvClientId = pClientId;
    }
    // BEGIN RN00028 Ricky Ngan 20080816
    /**
     * Get method for margin financing type
     * @return margin financing type
     */
    public String getMarginFinancingType(){
    	return mvMarginFinancingType;
    }
    /**
     * Set method for margin financing type
     * @param pMarginFinancingType the margin financing type
     */
    public void setMarginFinancingType(String pMarginFinancingType){
    	mvMarginFinancingType = pMarginFinancingType;
    }
    /**
     * Get method for allotment query 
     * @return allotment query 
     */
    public String getAllotmentQuery(){
    	return mvAllotmentQuery;
    }
    /**
     * Set method for allotment query
     * @param pAllotmentQuery the allotment query 
     */
    public void setAllotmentQuery(String pAllotmentQuery){
    	mvAllotmentQuery = pAllotmentQuery;
    }
    // END RN00028

    /**
     * Get method for IPO Application Status Details
     * @return Array of IPO Application Status Details
     */
    public HKSIPOApplStatusDetails[] getIPOApplStatusDetails()
    {
        return mvHKSIPOApplStatusDetails;
    }

    /**
     * Set method for IPO Application Status Details
     * @param pHKSIPOApplStatusDetails the Array of IPO Application Status Details
     */
    public void setIPOApplStatusDetails(HKSIPOApplStatusDetails[] pHKSIPOApplStatusDetails)
    {
        mvHKSIPOApplStatusDetails = pHKSIPOApplStatusDetails;
    }

    /**
     * Get method for Return Code
     * @return Return Code
     */
    public int getReturnCode()
    {
        return mvReturnCode;
    }

    /**
     * Set method for Return Code
     * @param pReturnCode the system Return Code
     */
    public void setReturnCode(int pReturnCode)
    {
        mvReturnCode = pReturnCode;
    }

    /**
     * Get method for Error Code
     * @return Error Code
     */
    public String getErrorCode()
    {
        return mvErrorCode;
    }

    /**
     * Set method for Error Code
     * @param pErrorCode the system Error Code
     */
    public void setErrorCode(String pErrorCode)
    {
        mvErrorCode = pErrorCode;
    }

    /**
     * Get method for Error Message
     * @return Error Message
     */
    public String getErrorMessage()
    {
        return mvErrorMessage;
    }

    /**
     * Set method for Error Message
     * @param pErrorMessage the system Error Message
     */
    public void setErrorMessage(String pErrorMessage)
    {
        mvErrorMessage = pErrorMessage;
    }
    /**
     * This method for localized system error message
     * @param pErrorCode the system error code
     * @param pDefaultMesg the default message
     * @return localized system error message
     */
   public String getLocalizedErrorMessage(String pErrorCode, String pDefaultMesg)
   {
      String lvRetMessage;

      try
      {
         if (pErrorCode.equals("0") || pErrorCode.equals("999"))
         {
            lvRetMessage = pDefaultMesg;
         }
         else
         {
            lvRetMessage = (String)presentation.getErrorCodeBundle().getString(pErrorCode);
         }
      }
      catch (Exception ex)
      {
         Log.println("Presentation._showPage: missing error mapping for " + pErrorCode + ", Default = " + pDefaultMesg, Log.ERROR_LOG);

         lvRetMessage = pDefaultMesg;
      }

      return lvRetMessage;
   }
}
