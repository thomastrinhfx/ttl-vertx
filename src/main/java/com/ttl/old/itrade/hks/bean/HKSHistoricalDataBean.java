package com.ttl.old.itrade.hks.bean;

import java.math.BigDecimal;

public class HKSHistoricalDataBean implements Comparable<HKSHistoricalDataBean> {
	private String mvTime;
	
	private Long mvRawTime;
	/**
	 * @return the mvTime
	 */
	public String getMvTime() {
		return mvTime;
	}
	/**
	 * @param mvTime the mvTime to set
	 */
	public void setMvTime(String mvTime) {
		this.mvTime = mvTime;
	}
	
	/**
	 * @return the mvMatchPrice
	 */
	public String getMvMatchPrice() {
		return mvMatchPrice;
	}
	/**
	 * @param mvMatchPrice the mvMatchPrice to set
	 */
	public void setMvMatchPrice(String mvMatchPrice) {
		this.mvMatchPrice = mvMatchPrice;
	}
	/**
	 * @return the mvMatchQty
	 */
	public BigDecimal getMvMatchQty() {
		return mvMatchQty;
	}
	/**
	 * @param mvMatchQty the mvMatchQty to set
	 */
	public void setMvMatchQty(BigDecimal mvMatchQty) {
		this.mvMatchQty = mvMatchQty;
	}

	/**
	 * @param mvRawTime the mvRawTime to set
	 */
	public void setMvRawTime(Long mvRawTime) {
		this.mvRawTime = mvRawTime;
	}
	/**
	 * @return the mvRawTime
	 */
	public Long getMvRawTime() {
		return mvRawTime;
	}

	private String mvMatchPrice;
	private BigDecimal mvMatchQty;
	
	private String mvTotalQty;
	
	public int compareTo(HKSHistoricalDataBean arg0) {
		return arg0.mvRawTime.compareTo(this.getMvRawTime());
	}
	/**
	 * @param mvTotalQty the mvTotalQty to set
	 */
	public void setMvTotalQty(String mvTotalQty) {
		this.mvTotalQty = mvTotalQty;
	}
	/**
	 * @return the mvTotalQty
	 */
	public String getMvTotalQty() {
		return mvTotalQty;
	}
}
