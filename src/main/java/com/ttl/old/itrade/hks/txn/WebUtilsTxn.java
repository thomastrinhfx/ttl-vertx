package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;

import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;
import com.systekit.winvest.hks.config.mapping.TagName;

public class WebUtilsTxn extends BaseTxn {
	
	
	private String GetBranchIDRequest = "GetBranchIDByClient";
	
	public String getBranchID(String clientId) {
		String branchID = "0";
		try {
			Hashtable<String, String> lvTxnMap = new Hashtable<String, String>();			
			lvTxnMap.put(TagName.CLIENTID, clientId);		
			int strReturn = process(GetBranchIDRequest, lvTxnMap);
			if (TPErrorHandling.TP_NORMAL == strReturn) {
		        branchID = mvReturnNode.getChildNode("BRANCHID").getValue().trim();
			}
		} catch (Exception e) {
			Log.print(e, Log.ERROR_LOG);
		}
		return branchID;
	}

}
