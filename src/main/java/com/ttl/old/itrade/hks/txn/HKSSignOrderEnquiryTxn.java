package com.ttl.old.itrade.hks.txn;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.tag.ITagXsfTagName;
import com.systekit.winvest.hks.config.mapping.TagName;
import com.systekit.winvest.hks.server.model.order.OrderSModel;

/**
 * The HKSSignOrderEnquiryTxn class definition for all method
 * stores the sign order enquiry details
 * @author not attributable
 *
 */

public class HKSSignOrderEnquiryTxn extends BaseTxn {
	   private String mvClientID;
	   private String mvInstrumentID;
	   private String mvMarketID;
	   private String mvBS;
	   private String mvFromTime;
	   private String mvToTime;
	   private String mvOrderType;
	   private int mvStartRecord;
	   private int mvEndRecord; 
	   
	   private int mvTotalOrders;
	   
	   private String mvSorting;
	   private String mvStatus;
	   private String mvStatusFilter;
	   private String mvLoopCounter;
	   private boolean mvIsExportData;
	   private Vector<OrderSModel> lvModelList;
	 

	   public HKSSignOrderEnquiryTxn(String pClientID, String pMarketID, String pInstrumentID, String pOrderType, String pBS, String pFromTime, String pToTime
			   , int mvStartRecord, int mvEndRecord
			   , boolean mvIsExportData, String pSorting, String pStatusFilter)
	   {
	      super();
	      mvClientID = pClientID;
	      mvMarketID = pMarketID;
	      mvOrderType = pOrderType;
	      mvInstrumentID = pInstrumentID;
	      mvBS = pBS;
	      mvFromTime = pFromTime;
	      mvToTime = pToTime;
	      this.mvStartRecord	= mvStartRecord;
	      this.mvEndRecord   	= mvEndRecord;
	      mvSorting = pSorting;
	      mvStatusFilter = pStatusFilter;
	      this.mvIsExportData=mvIsExportData;
	   }
	   /**
	    * The method process stores the order History enquiry details
	    * @return Order History Collection 
	    */
	   public void process() {
	      Hashtable<String,String> lvTxnMap = new Hashtable<String,String>();
	      lvTxnMap.put(TagName.CLIENTID, mvClientID);
	      if(null == mvInstrumentID || mvInstrumentID.isEmpty() ){
	    	  mvInstrumentID="ALL";
	      }
	      if(null == mvMarketID || mvMarketID.isEmpty() ){
	    	  mvMarketID="ALL";
	      }
	      if(null == mvOrderType || mvOrderType.isEmpty() ){
	    	  mvOrderType="ALL";
	      }
	      lvTxnMap.put(TagName.STOCKID, mvInstrumentID);
	      lvTxnMap.put(TagName.MARKETID, mvMarketID);
	      lvTxnMap.put(TagName.ORDERTYPE, mvOrderType);
	      lvTxnMap.put(TagName.BS, mvBS);
	      lvTxnMap.put(TagName.FROMTIME, mvFromTime);
	      lvTxnMap.put(TagName.TOTIME, mvToTime);
	      lvTxnMap.put(TagName.SORTING, mvSorting);
	      lvTxnMap.put("STATUSFILTER", mvStatusFilter);      
	      lvTxnMap.put("STARTRECORD", String.valueOf(mvStartRecord));
	      lvTxnMap.put("ENDRECORD", String.valueOf(mvEndRecord));
	      
		  
	      if (mvIsExportData) {
	    	  lvTxnMap.put("EXPORTDATA", "Y");
	      }else{
	    	  lvTxnMap.put("EXPORTDATA", "N");
	      }
	      
	  	  lvModelList = new Vector<OrderSModel>();
	  	  int status	= process(RequestName.SignOrderEnquiry, lvTxnMap);
	  	 
	      if (TPErrorHandling.TP_NORMAL == status)
	      {
	          IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList(ITagXsfTagName.CHILD_ROW);
	          mvTotalOrders = Integer.parseInt( mvReturnNode.getChildNode("TOTAL_RECORD").getValue());
	          if(mvTotalOrders > 0){
	        	  for (int i = 0; i < lvRowList.size(); i++)
	              {
	               	 IMsgXMLNode lvRow=lvRowList.getNode(i);                                       
	                  OrderSModel lvModel =parserBean(lvRow);    
	                  if(null != lvModel){
	                 	 lvModelList.add(lvModel);	
	                  } 
	              }             
	          }
	        
	      }      
	     //return mvHKSOrderEnqDetails;
	   }
	   
	   private static OrderSModel parserBean(IMsgXMLNode lvRow){
		   try {
			   OrderSModel lvModel = new OrderSModel();
			   
	           /*lvModel.setOrderGroupID(parseValue(lvRow, TagName.ORDERGROUPID));
	           lvModel.setOrderID(parseValue(lvRow, TagName.ORDERID));*/
			
				//lvModel.setInputTime(Timestamp.valueOf(parseValue(lvRow, TagName.INPUTTIME)));
				//lvModel.setSettleDate(Date.valueOf(parseValue(lvRow, TagName.HISTORYDATE)));
				
				lvModel.setTradeTime(Timestamp.valueOf(parseValue(lvRow, TagName.HISTORYDATE)));
				
				lvModel.setOrderType(parseValue(lvRow, TagName.ORDERTYPE).trim());
				lvModel.setStockID(parseValue(lvRow, TagName.STOCKID).trim());

				lvModel.setMarketID(parseValue(lvRow, TagName.MARKETID));

				lvModel.setBS(parseValue(lvRow, TagName.BS).trim());

				lvModel.setPrice(new BigDecimal(parseValue(lvRow, TagName.PRICE)));

				lvModel.setQty(Long.parseLong(parseValue(lvRow, TagName.QTY)));
				lvModel.setShortName(parseValue(lvRow, TagName.SHORTNAME).trim());
				lvModel.setFilledQty(Long.parseLong(parseValue(lvRow, TagName.FILLEDQTY)));
				lvModel.setCancelledQty(Long.parseLong(parseValue(lvRow, "CANCELLEDQTY")));
				lvModel.setOrderID(parseValue(lvRow, TagName.ORDERID).trim());
				lvModel.setStatus(parseValue(lvRow, TagName.STATUS).trim());
				//lvModel.setRejectReason(parseValue(lvRow, TagName.REJECTREASON));
				return lvModel;
		   } catch (Exception e) {}
		   
		   return null;
	   }

	   /**
		 * Get method for loop counter
		 * @return Loop counter counts the number of
		 */
	   public int getLoopCounter()
	{
	    int lvCnt;
	    try
	    {
	        lvCnt = Integer.parseInt(mvLoopCounter);
	    }
	    catch (Exception e)
	    {
	        return -1;
	    }
	    return lvCnt;
	}
	   
	   private static String parseValue(IMsgXMLNode lvRow, String keyName){
			String str	= "";
			try {			
				if(null != lvRow.getChildNode(keyName)){
					str  = lvRow.getChildNode(keyName).getValue();				
				}
				if(null == str)
					return "";
				
			} catch (Exception e) {}		
			return str;
		}

	/**
	 * Set method for Loop Counter
	 * @param Loop Counter
	 */
	public void setLoopCounter(String pLoopCounter)
	{
	        mvLoopCounter = pLoopCounter;
	}

	/**
	 * @return the mvTotalOrders
	 */
	public int getMvTotalOrders() {
		return mvTotalOrders;
	}
	/**
	 * @return the lvModelList
	 */
	public Vector<OrderSModel> getLvModelList() {
		return lvModelList;
	}




}
