/**
 * Constant class
 */
package com.ttl.old.itrade.hks.util;
public final class Constants{
	
	public static final int REPORT_PORFOLIO_ENQUIRY = 0;
	public static final int REPORT_ORDER_ENQUIRY = 1;
	public static final int REPORT_TRANSACTION_HISTORY = 2;
	public static final int REPORT_ORDER_HISTORY = 3;
	public static final int REPORT_CASH_TRANSACTION_HISTORY = 4;
	public static final int REPORT_AVAIABLE_MARGIN_LIST = 5;
	public static final int REPORT_MARGIN_LOAN = 6;
	
	// 
	public static final int REPORT_SIGN_ORDER = 7;
	//
	public static final String YYYYMMDD_WITH_MINUS_SIGN = "yyyy-MM-dd";
	public static final String YYYYMMDD = "yyyyMMdd";
	public static final String DAILY_DATA_HEADER = "Daily-Data-Header";
	public static final String HO_FLOOR_CODE = "HO";
	public static final String HA_FLOOR_CODE = "HA";
	public static final String OTC_FLOOR_CODE = "OTC";
	public static final String UPCOM_FLOOR_CODE = "UPCOM";
	public static final int HO_FLOOR_CODE_ID = 1;
	public static final int HA_FLOOR_CODE_ID = 2;
	public static final int UPCOM_FLOOR_CODE_ID = 3;
	
	
	//Transfer type
	public static final int Transfer_External = 1;
	public static final int Transfer_Internal = 2;
	
	//watch list
	public static final String HA_ATC = "ATC";
}
