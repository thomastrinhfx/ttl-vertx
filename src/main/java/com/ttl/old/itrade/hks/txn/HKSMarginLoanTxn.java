package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.bean.HKSMarginLoanBean;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * Refer to MarginLoanReportSDAO, HKSWBOML001
 * 
 * @author duonghuynh
 * @see com.systekit.winvest.hks.server.bo.dao.web.MarginLoanReportSDAO
 */
public class HKSMarginLoanTxn {
	/**
	 * This variable is used to the tp Error
	 */
	TPErrorHandling tpError;

	private int returnCode;
	private String errorCode;
	private String errorMessage;

	private int startRecord;
	private int endRecord;

	private String clientId;
	private String fromDate;
	private String toDate;
	private String language;
	private boolean isExportData = false;

	// output
	private String totalRecord;
	private String balBf;
	private String balCf;
	private String sumOutAmt;
	private String sumInAmt;
	private HKSMarginLoanBean[] marginLoanList = null;

	/**
	 * Default constructor for HKSMarginListTxn class
	 */
	public HKSMarginLoanTxn() {
		tpError = new TPErrorHandling();
	}

	/**
	 * 
	 * @param pClientId
	 * @param pFromDate
	 * @param pToDate
	 */
	public HKSMarginLoanTxn(String pClientId, String pFromDate, String pToDate) {
		setClientId(pClientId);
		setFromDate(pFromDate);
		setToDate(pToDate);
		tpError = new TPErrorHandling();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void getMarginLoan() {

		Hashtable lvTxnMap = new Hashtable();

		IMsgXMLNode lvRetNode;
		TPManager ivTPManager = ITradeServlet.getTPManager("HKSMarginLoan");
		TPBaseRequest lvTransactionHistoryRequest = ivTPManager.getRequest("HKSMarginLoan");

		lvTxnMap.put("CLIENTID", getClientId());
		lvTxnMap.put("FROMDATE", getFromDate());
		lvTxnMap.put("TODATE", getToDate());
		if (!isExportData) {
			lvTxnMap.put("STARTRECORD", String.valueOf(getStartRecord()));
			lvTxnMap.put("ENDRECORD", String.valueOf(getEndRecord()));
		}
		lvTxnMap.put(TagName.LANGUAGE, getLanguage());
		lvTxnMap.put("ISEXPORTDATA", String.valueOf(isExportData));

		lvRetNode = lvTransactionHistoryRequest.send(lvTxnMap);

		setReturnCode(tpError.checkError(lvRetNode));
		if (returnCode != TPErrorHandling.TP_NORMAL) {
			setErrorMessage(tpError.getErrDesc());
			if (returnCode == TPErrorHandling.TP_APP_ERR) {
				setErrorCode(tpError.getErrCode());
			}
		} else {
			if (lvRetNode.getChildNode("SUMOUTAMT") != null) {
				setSumOutAmt(lvRetNode.getChildNode("SUMOUTAMT").getValue());
			}
			
			if (lvRetNode.getChildNode("SUMINAMT") != null) {
				setSumInAmt(lvRetNode.getChildNode("SUMINAMT").getValue());
			}
			
			if (lvRetNode.getChildNode("TOTALRECORD") != null) {
				setTotalRecord(lvRetNode.getChildNode("TOTALRECORD").getValue());
			}

			IMsgXMLNodeList lvRowList = lvRetNode.getNodeList("Row");
			marginLoanList = new HKSMarginLoanBean[lvRowList.size()];
			for (int i = 0; i < lvRowList.size(); i++) {
				marginLoanList[i] = new HKSMarginLoanBean();
				IMsgXMLNode lvRow = lvRowList.getNode(i);
				marginLoanList[i].setRowNum(lvRow.getChildNode("ROWNUM").getValue());
				marginLoanList[i].setBalance(lvRow.getChildNode("BAL").getValue());
				marginLoanList[i].setDesc(lvRow.getChildNode("DESCRIPTION").getValue());
				marginLoanList[i].setIn(lvRow.getChildNode("TODAYIN").getValue());
				marginLoanList[i].setOut(lvRow.getChildNode("TODAYOUT").getValue());
				marginLoanList[i].setSellAmount(lvRow.getChildNode("SELLAMOUNT").getValue());
				marginLoanList[i].setTranDate(lvRow.getChildNode("TRANDATE").getValue());
				marginLoanList[i].setMarginCallF(lvRow.getChildNode("MARGINCALLF").getValue());
				if (i == 0) { // get one time only
					setBalCf(lvRow.getChildNode("BALCF").getValue());
					setBalBf(lvRow.getChildNode("BALBF").getValue());
				}
			}
		}

	}

	/**
	 * @return the tpError
	 */
	public TPErrorHandling getTpError() {
		return tpError;
	}

	/**
	 * @param tpError the tpError to set
	 */
	public void setTpError(TPErrorHandling tpError) {
		this.tpError = tpError;
	}

	/**
	 * @return the returnCode
	 */
	public int getReturnCode() {
		return returnCode;
	}

	/**
	 * @param returnCode the returnCode to set
	 */
	public void setReturnCode(int returnCode) {
		this.returnCode = returnCode;
	}

	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the startRecord
	 */
	public int getStartRecord() {
		return startRecord;
	}

	/**
	 * @param startRecord the startRecord to set
	 */
	public void setStartRecord(int startRecord) {
		this.startRecord = startRecord;
	}

	/**
	 * @return the endRecord
	 */
	public int getEndRecord() {
		return endRecord;
	}

	/**
	 * @param endRecord the endRecord to set
	 */
	public void setEndRecord(int endRecord) {
		this.endRecord = endRecord;
	}

	/**
	 * @return the totalRecord
	 */
	public String getTotalRecord() {
		return totalRecord;
	}

	/**
	 * @param totalRecord the totalRecord to set
	 */
	private void setTotalRecord(String totalRecord) {
		this.totalRecord = totalRecord;
	}

	/**
	 * @return the clientId
	 */
	public String getClientId() {
		return clientId;
	}

	/**
	 * @param clientId the clientId to set
	 */
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the isExportData
	 */
	public boolean isExportData() {
		return isExportData;
	}

	/**
	 * @param isExportData the isExportData to set
	 */
	public void setExportData(boolean isExportData) {
		this.isExportData = isExportData;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the marginLoadList
	 */
	public HKSMarginLoanBean[] getMarginLoanList() {
		return marginLoanList;
	}

	/**
	 * @return the balBf
	 */
	public String getBalBf() {
		return balBf;
	}

	/**
	 * @return the balCf
	 */
	public String getBalCf() {
		return balCf;
	}

	/**
	 * @param balBf the balBf to set
	 */
	private void setBalBf(String balBf) {
		this.balBf = balBf;
	}

	/**
	 * @param balCf the balCf to set
	 */
	private void setBalCf(String balCf) {
		this.balCf = balCf;
	}

	/**
	 * @return the sumOutAmt
	 */
	public String getSumOutAmt() {
		return sumOutAmt;
	}

	/**
	 * @param sumOutAmt the sumOutAmt to set
	 */
	public void setSumOutAmt(String sumOutAmt) {
		this.sumOutAmt = sumOutAmt;
	}

	/**
	 * @return the sumInAmt
	 */
	public String getSumInAmt() {
		return sumInAmt;
	}

	/**
	 * @param sumInAmt the sumInAmt to set
	 */
	public void setSumInAmt(String sumInAmt) {
		this.sumInAmt = sumInAmt;
	}

}
