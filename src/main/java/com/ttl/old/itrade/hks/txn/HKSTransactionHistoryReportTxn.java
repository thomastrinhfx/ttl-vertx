package com.ttl.old.itrade.hks.txn;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.util.TextFormatter;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.ttl.old.itrade.util.Presentation;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * This class store the transaction history
 * 
 * @author not attributable
 * 
 */
public class HKSTransactionHistoryReportTxn {

	private String mvClientId;
	private String mvFromDate;
	private String mvToDate;
	private String mvStatus;

	private int mvReturnCode;
	private String mvErrorCode;
	private String mvErrorMessage;

	private int mvStartRecord;
	private int mvEndRecord;
	private String mvLanguage;
	private String mvSumDebit;
	private String mvSumCredit;
	private String mvBalBF;
	private String mvBalCF;

	private String mvTotalRecord;
	private boolean mvIsExportData;

	@SuppressWarnings("rawtypes")
	private List mvHKSCashTransactionReportDetails;
	private HKSTxnHistoryDetails[] mvHKSTxnHistoryDetails;

	public static final String CLIENTID = "CLIENTID";
	public static final String FROMDATE = "FROMDATE";
	public static final String TODATE = "TODATE";

	TPErrorHandling mvTpError;
	SimpleDateFormat mvDateFormatter = new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * Constructor for HKSTransactionHistoryTxn class
	 * 
	 * @param pClientId the client id
	 * @param pFromDate the form date
	 * @param pToDate the to date
	 * @param pTxnType the txntype
	 */
	public HKSTransactionHistoryReportTxn(String pClientId, String pFromDate, String pToDate) {
		setClientId(pClientId);
		setFromDate(pFromDate);
		setToDate(pToDate);
		mvTpError = new TPErrorHandling();
	}

	/*** added by May on 18-12-2003 ***/
	/**
	 * The method for time type conversion
	 * 
	 * @param longTime the long time
	 */
	public static String shortTime(String longTime) {
		SimpleDateFormat origFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		SimpleDateFormat newFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date longDate = new Date();
		try {
			longDate = origFormatter.parse(longTime);
		} catch (ParseException ex) {
			Log.println("[ TransactionHistory.shortTime(): ParseException, longTime=" + longTime + " ]", Log.ERROR_LOG);
		}

		return newFormatter.format(longDate);
		/*
		 * int dot = longTime.lastIndexOf("."); if (dot > 0) return longTime.substring(0,dot); else return longTime;
		 */
	}

	/*******************************************/

	// comment by May on 17-12-2003
	// public void process()
	/**
	 * This method store the transaction history
	 * 
	 * @param presentation the Presentation class
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void getCashTransactions(Presentation presentation) {
		try {
			Hashtable lvTxnMap = new Hashtable();

			IMsgXMLNode lvRetNode;
			TPManager ivTPManager = ITradeServlet.getTPManager("HKSCashTransactionHistoryReportRequest");
			TPBaseRequest lvTransactionHistoryRequest = ivTPManager.getRequest("HKSCashTransactionHistoryReportRequest");

			lvTxnMap.put(CLIENTID, getClientId());
			lvTxnMap.put(FROMDATE, getFromDate());
			lvTxnMap.put(TODATE, getToDate());
			if (!mvIsExportData) {
				lvTxnMap.put("STARTRECORD", String.valueOf(getMvStartRecord()));
				lvTxnMap.put("ENDRECORD", String.valueOf(getMvEndRecord()));
			}
			lvTxnMap.put(TagName.LANGUAGE, getMvLanguage());
			lvTxnMap.put("TYPE", "CASH");
			lvTxnMap.put("ISEXPORTDATA", String.valueOf(mvIsExportData));
			lvRetNode = lvTransactionHistoryRequest.send(lvTxnMap);

			setReturnCode(mvTpError.checkError(lvRetNode));
			if (mvReturnCode != TPErrorHandling.TP_NORMAL) {
				setErrorMessage(mvTpError.getErrDesc());
				if (mvReturnCode == TPErrorHandling.TP_APP_ERR) {
					setErrorCode(mvTpError.getErrCode());
				}
			} else {
				setMvHKSCashTransactionReportDetails(new ArrayList<HashMap<String, String>>());
				setMvTotalRecord(lvRetNode.getChildNode("TOTALRECORD").getValue());
				setMvSumCredit(lvRetNode.getChildNode("SUMCREDIT").getValue());
				setMvSumDebit(lvRetNode.getChildNode("SUMDEBIT").getValue());
				setMvBalBF(lvRetNode.getChildNode("BALBF").getValue());
				setMvBalCF(lvRetNode.getChildNode("BALCF").getValue());

				IMsgXMLNodeList lvRowList = lvRetNode.getNodeList("Row");
				for (int i = 0; i < lvRowList.size(); i++) {
					HashMap<String, String> returnModel = new HashMap<String, String>();
					IMsgXMLNode lvRow = lvRowList.getNode(i);

					String cashSettledDate = TextFormatter.getFormatedDate(lvRow.getChildNode("TRANDATE").getValue(),
							IMain.getProperty("dateFormat"), "yyyy-MM-dd");
					returnModel.put("ROWNUM", lvRow.getChildNode("ROWNUM").getValue());
					returnModel.put("TRANDATE", cashSettledDate);
					returnModel.put("REMARKS", lvRow.getChildNode("REMARKS").getValue());
					returnModel.put("DEBITAMT", lvRow.getChildNode("DEBITAMT").getValue());
					returnModel.put("CREDITAMT", lvRow.getChildNode("CREDITAMT").getValue());
					returnModel.put("TOTALAMT", lvRow.getChildNode("TOTALAMT").getValue());

					getMvHKSCashTransactionReportDetails().add(returnModel);
				}
			}
		} catch (Exception e) {
			Log.println(e.getMessage(), Log.ERROR_LOG);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void getStockTransactions(Presentation presentation) {
		try {
			Hashtable lvTxnMap = new Hashtable();

			IMsgXMLNode lvRetNode;
			TPManager ivTPManager = ITradeServlet.getTPManager("HKSCashTransactionHistoryReportRequest");
			TPBaseRequest lvTransactionHistoryRequest = ivTPManager.getRequest("HKSCashTransactionHistoryReportRequest");

			lvTxnMap.put(CLIENTID, getClientId());
			lvTxnMap.put(FROMDATE, getFromDate());
			lvTxnMap.put(TODATE, getToDate());
			if (!mvIsExportData) {
				lvTxnMap.put("STARTRECORD", String.valueOf(getMvStartRecord()));
				lvTxnMap.put("ENDRECORD", String.valueOf(getMvEndRecord()));
			}
			lvTxnMap.put(TagName.LANGUAGE, getMvLanguage());
			lvTxnMap.put("TYPE", "STOCK");
			lvTxnMap.put("ISEXPORTDATA", String.valueOf(mvIsExportData));

			lvRetNode = lvTransactionHistoryRequest.send(lvTxnMap);

			setReturnCode(mvTpError.checkError(lvRetNode));
			if (mvReturnCode != TPErrorHandling.TP_NORMAL) {
				setErrorMessage(mvTpError.getErrDesc());
				if (mvReturnCode == TPErrorHandling.TP_APP_ERR) {
					setErrorCode(mvTpError.getErrCode());
				}
			} else {

				IMsgXMLNodeList lvRowList = lvRetNode.getNodeList("Row");
				if (!mvIsExportData) {
					if (lvRetNode.getChildNode("TOTALRECORD") != null) {
						setMvTotalRecord(lvRetNode.getChildNode("TOTALRECORD").getValue());
					}
				}
				mvHKSTxnHistoryDetails = new HKSTxnHistoryDetails[lvRowList.size()];
				for (int i = 0; i < lvRowList.size(); i++) {
					mvHKSTxnHistoryDetails[i] = new HKSTxnHistoryDetails();

					IMsgXMLNode lvRow = lvRowList.getNode(i);
					mvHKSTxnHistoryDetails[i].setRefId(lvRow.getChildNode("ROW_NUM").getValue());
					mvHKSTxnHistoryDetails[i].setTradeDate(lvRow.getChildNode("TRANDATE").getValue());
					mvHKSTxnHistoryDetails[i].setQty(lvRow.getChildNode("TSETTLED").getValue());
					mvHKSTxnHistoryDetails[i].setAvgPrice(lvRow.getChildNode("AVGPRICE").getValue());
					mvHKSTxnHistoryDetails[i].setAmt(lvRow.getChildNode("SETTLEAMOUNT").getValue());
					mvHKSTxnHistoryDetails[i].setStockId(lvRow.getChildNode("INSTRUMENTID").getValue());
					mvHKSTxnHistoryDetails[i].setTxnType(lvRow.getChildNode("TRANSACTIONTYPE").getValue() == null ? "" : lvRow
							.getChildNode("TRANSACTIONTYPE").getValue().trim());
					mvHKSTxnHistoryDetails[i].setMvTotalFee(lvRow.getChildNode("TOTALFEE").getValue());
					mvHKSTxnHistoryDetails[i].setDesc(lvRow.getChildNode("DESCRIPTION").getValue());

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get method for Client ID
	 * 
	 * @return Client ID
	 */
	public String getClientId() {
		return mvClientId;
	}

	/**
	 * Set method for Client ID
	 * 
	 * @param pClientId the Client ID
	 */
	public void setClientId(String pClientId) {
		mvClientId = pClientId;
	}

	/**
	 * Get method for From Date
	 * 
	 * @return From Date
	 */
	public String getFromDate() {
		return mvFromDate;
	}

	/**
	 * Set method for From Date
	 * 
	 * @param pFromDate the From Date
	 */
	public void setFromDate(String pFromDate) {
		mvFromDate = pFromDate;
	}

	/**
	 * Get method for To Date
	 * 
	 * @return To Date
	 */
	public String getToDate() {
		return mvToDate;
	}

	/**
	 * Set method for To Date
	 * 
	 * @param pToDate the To Date
	 */
	public void setToDate(String pToDate) {
		mvToDate = pToDate;
	}

	/**
	 * Get method for Status
	 * 
	 * @return Status
	 */
	public String getStatus() {
		return mvStatus;
	}

	/**
	 * Set method for Status
	 * 
	 * @param pStatus the Status
	 */
	public void setStatus(String pStatus) {
		mvStatus = pStatus;
	}

	/**
	 * Get method for Return Code
	 * 
	 * @return Return Code
	 */
	public int getReturnCode() {
		return mvReturnCode;
	}

	/**
	 * Set method for Return Code
	 * 
	 * @param pReturnCode the Return Code
	 */
	public void setReturnCode(int pReturnCode) {
		mvReturnCode = pReturnCode;
	}

	/**
	 * Get method for Error Code
	 * 
	 * @return Error Code
	 */
	public String getErrorCode() {
		return mvErrorCode;
	}

	/**
	 * Set method for Error Code
	 * 
	 * @param pErrorCode the Error Code
	 */
	public void setErrorCode(String pErrorCode) {
		mvErrorCode = pErrorCode;
	}

	/**
	 * Get method for Error Message
	 * 
	 * @return Error Message
	 */
	public String getErrorMessage() {
		return mvErrorMessage;
	}

	/**
	 * Set method for Error Message
	 * 
	 * @param pErrorMessage the Error Message
	 */
	public void setErrorMessage(String pErrorMessage) {
		mvErrorMessage = pErrorMessage;
	}

	/**
	 * @param mvHKSCashTransactionReportDetails the mvHKSCashTransactionReportDetails to set
	 */
	@SuppressWarnings("rawtypes")
	public void setMvHKSCashTransactionReportDetails(List mvHKSCashTransactionReportDetails) {
		this.mvHKSCashTransactionReportDetails = mvHKSCashTransactionReportDetails;
	}

	/**
	 * @return the mvHKSCashTransactionReportDetails
	 */
	@SuppressWarnings("rawtypes")
	public List getMvHKSCashTransactionReportDetails() {
		return mvHKSCashTransactionReportDetails;
	}

	/**
	 * @return the mvHKSTxnHistoryDetails
	 */
	public HKSTxnHistoryDetails[] getMvHKSTxnHistoryDetails() {
		return mvHKSTxnHistoryDetails;
	}

	/**
	 * @param mvHKSTxnHistoryDetails the mvHKSTxnHistoryDetails to set
	 */
	public void setMvHKSTxnHistoryDetails(HKSTxnHistoryDetails[] mvHKSTxnHistoryDetails) {
		this.mvHKSTxnHistoryDetails = mvHKSTxnHistoryDetails;
	}

	public void setMvStartRecord(int mvStartRecord) {
		this.mvStartRecord = mvStartRecord;
	}

	public int getMvStartRecord() {
		return mvStartRecord;
	}

	public void setMvEndRecord(int mvEndRecord) {
		this.mvEndRecord = mvEndRecord;
	}

	public int getMvEndRecord() {
		return mvEndRecord;
	}

	public void setMvLanguage(String mvLanguage) {
		this.mvLanguage = mvLanguage;
	}

	public String getMvLanguage() {
		return mvLanguage;
	}

	public void setMvTotalRecord(String mvTotalRecord) {
		this.mvTotalRecord = mvTotalRecord;
	}

	public String getMvTotalRecord() {
		return mvTotalRecord;
	}

	public void setMvIsExportData(boolean mvIsExportData) {
		this.mvIsExportData = mvIsExportData;
	}

	public boolean isMvIsExportData() {
		return mvIsExportData;
	}

	public String getMvSumDebit() {
		return mvSumDebit;
	}

	public void setMvSumDebit(String mvSumDebit) {
		this.mvSumDebit = mvSumDebit;
	}

	public String getMvSumCredit() {
		return mvSumCredit;
	}

	public void setMvSumCredit(String mvSumCredit) {
		this.mvSumCredit = mvSumCredit;
	}

	public String getMvBalBF() {
		return mvBalBF;
	}

	public void setMvBalBF(String mvBalBF) {
		this.mvBalBF = mvBalBF;
	}

	public String getMvBalCF() {
		return mvBalCF;
	}

	public void setMvBalCF(String mvBalCF) {
		this.mvBalCF = mvBalCF;
	}

}
