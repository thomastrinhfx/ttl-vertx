package com.ttl.old.itrade.hks.util;

import com.ttl.old.itrade.util.DefaultErrorMsgConverter;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class HKSOtherErrorMsgConverter extends DefaultErrorMsgConverter
{
    public static final int		HKSEF = 20000;
    public static final int		CORE = 4000000;

    public HKSOtherErrorMsgConverter() {}

    public static int getErrorCode(String s)
    {
        int errorCode = -1;

        // if the msg start with "HKSEF"
        if (s.startsWith("HKSEF"))
        {
            errorCode = HKSEF + Integer.parseInt(s.substring(5));
        }
        else if (s.equals("CORE10155"))
        {
            errorCode = HKSEF + 34;
        }
        else if (s.startsWith("CORE"))
        {
            errorCode = CORE + Integer.parseInt(s.substring(4));
        }

        return errorCode;
    }
}
