package com.ttl.old.itrade.hks.bean;

/**
 * The HKSTransactionHistoryBean class define variables that to save values 
 * for action 
 * @author
 *
 */
public class HKSSearchCashTransactionBean {
	private String transDate;
	private String refNo;
	private String transactionType;
	private String increaseValue;
	private String descreaseValue;
	private String note;
	
	/**
	 * @return the transDate
	 */
	public String getTransDate() {
		return transDate;
	}
	/**
	 * @param transDate the transDate to set
	 */
	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}
	/**
	 * @return the refNo
	 */
	public String getRefNo() {
		return refNo;
	}
	/**
	 * @param refNo the refNo to set
	 */
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	/**
	 * @return the transactionType
	 */
	public String getTransactionType() {
		return transactionType;
	}
	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	/**
	 * @return the increaseValue
	 */
	public String getIncreaseValue() {
		return increaseValue;
	}
	/**
	 * @param increaseValue the increaseValue to set
	 */
	public void setIncreaseValue(String increaseValue) {
		this.increaseValue = increaseValue;
	}
	/**
	 * @return the descreaseValue
	 */
	public String getDescreaseValue() {
		return descreaseValue;
	}
	/**
	 * @param descreaseValue the descreaseValue to set
	 */
	public void setDescreaseValue(String descreaseValue) {
		this.descreaseValue = descreaseValue;
	}
	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}
	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}		
}
