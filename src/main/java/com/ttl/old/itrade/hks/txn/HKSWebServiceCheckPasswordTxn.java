package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;

/**
 * This class process the web service check client password
 * @author not attributable
 *
 */
public class HKSWebServiceCheckPasswordTxn extends BaseTxn{
	
	private String mvSTNO="";
	private String mvPassword="";
	
	private String mvResult="";
	
	TPErrorHandling tpError;
	
	private int mvReturnCode;
	private String mvErrorCode;
	private String mvErrorMessage;
	
	public HKSWebServiceCheckPasswordTxn(){
		tpError = new TPErrorHandling();
	}
	/**
	 * Default constructor for HKSWebServiceCheckPasswordTxn class
	 * @param pSTNO the Security trading id
	 * @param pPassword the client password
	 */
	public HKSWebServiceCheckPasswordTxn(String pSTNO, String pPassword){
		tpError = new TPErrorHandling();
		setSTNO(pSTNO);
		setPassword(pPassword);
	}
	/**
	 * This method process the web service check client password
	 */
	public void process()
	{
		try
		{
			Log.println("[ HKSWebServiceCheckPasswordTxn.process(): ClientID "+ getSTNO()+" starts " , Log.ACCESS_LOG);
			Hashtable lvTxnMap = new Hashtable();
			IMsgXMLNode lvRetNode;
			TPManager ivTPManager = ITradeServlet.getTPManager(RequestName.HKSWSCheckPasswordRequest);
			TPBaseRequest lvHKSWebServiceRequest = ivTPManager.getRequest(RequestName.HKSWSCheckPasswordRequest);

			lvTxnMap.put("STNO", this.getSTNO());
			lvTxnMap.put("PASSWORD", this.getPassword());
			
			lvRetNode = lvHKSWebServiceRequest.send(lvTxnMap);
			
			setReturnCode(tpError.checkError(lvRetNode));
			
			if (mvReturnCode != tpError.TP_NORMAL)
			{
				setErrorMessage(tpError.getErrDesc());
				if (mvReturnCode == tpError.TP_APP_ERR)
				{
					setErrorCode(tpError.getErrCode());
				}
			}
			else
			{
				mvResult = lvRetNode.getChildNode("CHECKPASSWORD").getValue();
			}
		}catch (Exception e)
		{	
			Log.println("HKSWebServiceCheckPasswordTxn ClientID "+ getSTNO() +" Error " + e.toString(), Log.ERROR_LOG);
		}
	}
	/**
	 * Get method for result
	 * @return result
	 */
	public String getResult() {
		return mvResult;
	}
	/**
	 * Set method for result
	 * @param pReturnCode the result
	 */
	public void setReturnCode(int pReturnCode)
	{
	    mvReturnCode = pReturnCode;
	}
	/**
	 * Set method for error system code
	 * @param pErrorCode error system code
	 */
	public void setErrorCode(String pErrorCode)
    {
        mvErrorCode = pErrorCode;
    }
	/**
	 * Set method for error system message
	 * @param pErrorMessage the error system message
	 */
	public void setErrorMessage(String pErrorMessage)
    {
        mvErrorMessage = pErrorMessage;
    }
	/**
	 * Get method for security trading id
	 * @return security trading id
	 */
	public String getSTNO() {
		return mvSTNO;
	}
	/**
	 * Set method for security trading id
	 * @param pSTNO the security trading id
	 */
	public void setSTNO(String pSTNO) {
		this.mvSTNO = pSTNO;
	}
	/**
	 * Get method for client password
	 * @return client password
	 */
	public String getPassword() {
		return mvPassword;
	}
	/**
	 * Set method for client password
	 * @param pPassword the client password
	 */
	public void setPassword(String pPassword) {
		this.mvPassword = pPassword;
	}

}
