
package com.ttl.old.itrade.hks.bean.plugin;

/**
 * The HKSAdvancePaymentChildBean class define variables that to save values for action 
 * @author Wind.Zhao
 *
 */

//Begin Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100122 [iTrade R5] Make Advance Payment a Plugin Module
public class HKSAdvancePaymentChildBean {
	private String mvOrderID;
	private String mvContractID;
	private String mvAmount;
	private String mvFormatedAmount;
	private String mvQuantity;
	private String mvStockID;
	private String mvSettleDay;
	private String mvMarketId;
	private String mvPrice;
	private String tradeDate;
	private String tranDate;
	private String tradingFee;
	private String cashSettleDay;
	private String mvAvailableAmount;
	
	/**
	 * Returns the order id
	 * @return the order id
	 */
	public String getMvOrderID() {
		return mvOrderID;
	}
	
	/**
	 * Sets the order id
	 * @param pOrderID the order id.
	 */
	public void setMvOrderID(String pOrderID) {
		mvOrderID = pOrderID;
	}
	
	/**
	 * Returns the contract id.
	 * @return the contract id.
	 */
	public String getMvContractID() {
		return mvContractID;
	}
	
	/**
	 * Sets the contract id.
	 * @param pContractID the contract id.
	 */
	public void setMvContractID(String pContractID) {
		mvContractID = pContractID;
	}
	
	/**
	 * Returns the amount.
	 * @return the amount.
	 */
	public String getMvAmount() {
		return mvAmount;
	}
	
	/**
	 * Sets the amount.
	 * @param pAmount the amount.
	 */
	public void setMvAmount(String pAmount) {
		mvAmount = pAmount;
	}
	
	/**
	 * Returns the formated amount.
	 * @return the formated amount
	 */
	public String getMvFormatedAmount() {
		return mvFormatedAmount;
	}
	/**
	 * Sets the formated amount
	 * @param pFormatedAmount The formated amount.
	 */
	public void setMvFormatedAmount(String pFormatedAmount) {
		mvFormatedAmount = pFormatedAmount;
	}
	/**
	 * Returns the quantity.
	 * @return the quantity.
	 */
	public String getMvQuantity() {
		return mvQuantity;
	}
	
	/**
	 * Sets the quantity.
	 * @param pQuantity the quantity.
	 */
	public void setMvQuantity(String pQuantity) {
		mvQuantity = pQuantity;
	}
	
	/**
	 * Returns the stock id.
	 * @return the stock id.
	 */
	public String getMvStockID() {
		return mvStockID;
	}

	/**
	 * Sets the stock id 
	 * @param pStockID the stock id.
	 */
	public void setMvStockID(String pStockID) {
		this.mvStockID = pStockID;
	}

	public void setMvSettleDay(String mvSettleDay) {
		this.mvSettleDay = mvSettleDay;
	}

	public String getMvSettleDay() {
		return mvSettleDay;
	}

	public void setMvMarketId(String mvMarketId) {
		this.mvMarketId = mvMarketId;
	}

	public String getMvMarketId() {
		return mvMarketId;
	}

	public void setMvPrice(String mvPrice) {
		this.mvPrice = mvPrice;
	}

	public String getMvPrice() {
		return mvPrice;
	}

	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}

	public String getTradeDate() {
		return tradeDate;
	}

	public void setTranDate(String tranDate) {
		this.tranDate = tranDate;
	}

	public String getTranDate() {
		return tranDate;
	}

	public void setTradingFee(String tradingFee) {
		this.tradingFee = tradingFee;
	}

	public String getTradingFee() {
		return tradingFee;
	}

	public void setCashSettleDay(String cashSettleDay) {
		this.cashSettleDay = cashSettleDay;
	}

	public String getCashSettleDay() {
		return cashSettleDay;
	}

	public String getMvAvailableAmount() {
		return mvAvailableAmount;
	}

	public void setMvAvailableAmount(String mvAvailableAmount) {
		this.mvAvailableAmount = mvAvailableAmount;
	}	
	
}
//End Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100122 [iTrade R5] Make Advance Payment a Plugin Module