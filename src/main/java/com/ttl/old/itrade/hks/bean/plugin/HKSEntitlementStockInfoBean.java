package com.ttl.old.itrade.hks.bean.plugin;

/**
 * The HKSAdvancePaymentInfoBean class define variables that to save values for action 
 * @author Wind.Zhao
 *
 */

//Begin Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100127 [iTrade R5] Make Advance Payment a Plugin Module
public class HKSEntitlementStockInfoBean {
	private String stockId;
	private String stockName;
	private String marketId;
	private String entitlementId;
	private String subscriptionId;
	private String startDate;
	private String endDate;
	private String price;
	private String maxQtyCanBuy;
	private String registerQty;
	private String tradeStockCode;
	private String perRightRate;
	private String perStockRate;
	private String bookCloseDate;
	private String locationId;
	private String totalStock;
	private String status;
	private String actionRate;
	private String rightRate;
	private String transenddate;
	/**
	 * @return the stockId
	 */
	public String getStockId() {
		return stockId;
	}
	/**
	 * @param stockId the stockId to set
	 */
	public void setStockId(String stockId) {
		this.stockId = stockId;
	}
	/**
	 * @return the stockName
	 */
	public String getStockName() {
		return stockName;
	}
	/**
	 * @param stockName the stockName to set
	 */
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	/**
	 * @return the marketId
	 */
	public String getMarketId() {
		return marketId;
	}
	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(String marketId) {
		this.marketId = marketId;
	}
	/**
	 * @return the entitlementId
	 */
	public String getEntitlementId() {
		return entitlementId;
	}
	/**
	 * @param entitlementId the entitlementId to set
	 */
	public void setEntitlementId(String entitlementId) {
		this.entitlementId = entitlementId;
	}
	/**
	 * @return the subscriptionId
	 */
	public String getSubscriptionId() {
		return subscriptionId;
	}
	/**
	 * @param subscriptionId the subscriptionId to set
	 */
	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}
	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}
	/**
	 * @return the maxQtyCanBuy
	 */
	public String getMaxQtyCanBuy() {
		return maxQtyCanBuy;
	}
	/**
	 * @param maxQtyCanBuy the maxQtyCanBuy to set
	 */
	public void setMaxQtyCanBuy(String maxQtyCanBuy) {
		this.maxQtyCanBuy = maxQtyCanBuy;
	}
	/**
	 * @return the registerQty
	 */
	public String getRegisterQty() {
		return registerQty;
	}
	/**
	 * @param registerQty the registerQty to set
	 */
	public void setRegisterQty(String registerQty) {
		this.registerQty = registerQty;
	}
	/**
	 * @return the tradeStockCode
	 */
	public String getTradeStockCode() {
		return tradeStockCode;
	}
	/**
	 * @param tradeStockCode the tradeStockCode to set
	 */
	public void setTradeStockCode(String tradeStockCode) {
		this.tradeStockCode = tradeStockCode;
	}
	/**
	 * @return the perRightRate
	 */
	public String getPerRightRate() {
		return perRightRate;
	}
	/**
	 * @param perRightRate the perRightRate to set
	 */
	public void setPerRightRate(String perRightRate) {
		this.perRightRate = perRightRate;
	}
	/**
	 * @return the perStockRate
	 */
	public String getPerStockRate() {
		return perStockRate;
	}
	/**
	 * @param perStockRate the perStockRate to set
	 */
	public void setPerStockRate(String perStockRate) {
		this.perStockRate = perStockRate;
	}
	/**
	 * @param bookCloseDate the bookCloseDate to set
	 */
	public void setBookCloseDate(String bookCloseDate) {
		this.bookCloseDate = bookCloseDate;
	}
	/**
	 * @return the bookCloseDate
	 */
	public String getBookCloseDate() {
		return bookCloseDate;
	}
	/**
	 * @param localtionId the localtionId to set
	 */
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	/**
	 * @return the localtionId
	 */
	public String getLocationId() {
		return locationId;
	}
	/**
	 * @param totalStock the totalStock to set
	 */
	public void setTotalStock(String totalStock) {
		this.totalStock = totalStock;
	}
	/**
	 * @return the totalStock
	 */
	public String getTotalStock() {
		return totalStock;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	
	public String getActionRate() {
		return actionRate;
	}
	
	public void setActionRate(String actionRate) {
		this.actionRate = actionRate;
	}
	
	public String getRightRate() {
		return rightRate;
	}
	
	public void setRightRate(String rightRate) {
		this.rightRate = rightRate;
	}
	
	public String getTransenddate() {
		return transenddate;
	}
	
	public void setTransenddate(String transenddate) {
		this.transenddate = transenddate;
	}
	
	
}
//End Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100127 [iTrade R5] Make Advance Payment a Plugin Module
