package com.ttl.old.itrade.hks.txn;

/**
 * <p>Title:  Order Enquiry Transaction </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.ttl.old.itrade.util.Presentation;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;

/**
 * This class store the transaction history
 * @author not attributable
 *
 */
public class HKSSearchCashTransactionTxn
{

	private String						mvClientId;
	private String						mvFromDate;
	private String						mvToDate;
	private String						mvStatus;

	private String						mvLoopCounter;
	private int							mvReturnCode;
	private String						mvErrorCode;
	private String						mvErrorMessage;

//	private HKSTxnHistoryOrderDetails[]	mvHKSTxnHistoryOrderDetails;
//	private HKSTxnHistoryDWDetails[]	mvHKSTxnHistoryDWDetails;
	private HKSTxnHistoryDetails[]	mvHKSTxnHistoryDetails;

	public static final String			LOOPCOUNTER = "LoopCounter";
	public static final String			LOOPROWS = "LoopRows";
	public static final String			FIELDNAMES = "FieldNames";
	public static final String			VALUES = "Values";
//	public static final String			CLIENTID = "CLIENTID";
//	public static final String			FROMDATE = "FROMDATE";
//	public static final String			TODATE = "TODATE";
	public static final String			ORDER = "ORDER";
	public static final String			DW = "DW";

	// added by May on 18-12-2003
//	public static final String			SHORTTIME = "SHORTTIME";

	public static final String			CLIENTID = "CLIENTID";
	public static final String			TRADEDATE = "TRADEDATE";
	public static final String			TYPE = "TYPE";
	public static final String			DESC = "DESC";
	public static final String			NAME = "NAME";
	public static final String			CSHORTNAME = "CSHORTNAME";
        public static final String                      MARKETID = "MARKETID";
	public static final String			STOCKID = "STOCKID";
	public static final String			QTY = "QTY";
	public static final String			AVGPRICE = "AVGPRICE";
	public static final String			AMT = "AMT";
	public static final String			FROMDATE = "FROMDATE";
	public static final String			TODATE = "TODATE";

	TPErrorHandling mvTpError;
	SimpleDateFormat					mvDateFormatter = new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * Default constructor for HKSTransactionHistoryTxn class
	 */
	public HKSSearchCashTransactionTxn()
	{
		mvTpError = new TPErrorHandling();
	}
	;


	/**
	 * Default constructor for HKSTransactionHistoryTxn class
	 * @param pClientId the Client ID
	 */
	public HKSSearchCashTransactionTxn(String pClientId)
	{
		setClientId(pClientId);
		mvTpError = new TPErrorHandling();
	}
	;


	/**
	 * Constructor for HKSTransactionHistoryTxn class
	 * @param pClientId the client id
	 * @param pFromDate the form date
	 * @param pToDate the to date
	 * @param pTxnType the txntype
	 */
    public HKSSearchCashTransactionTxn(String pClientId, String pFromDate, String pToDate)
    {
        setClientId(pClientId);
        setFromDate(pFromDate);
        setToDate(pToDate);
        mvTpError = new TPErrorHandling();
    }


/***	added by May on 18-12-2003	***/
    /**
     * The method for time type conversion
     * @param longTime the long time
     */
	public static String shortTime(String longTime)
	{
		SimpleDateFormat origFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		SimpleDateFormat newFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date longDate = new Date();
		try
		{
			longDate = origFormatter.parse(longTime);
		}
		catch (ParseException ex)
		{
			Log.println("[ TransactionHistory.shortTime(): ParseException, longTime=" + longTime + " ]", Log.ERROR_LOG);
		}

		return newFormatter.format(longDate);

	}
/*******************************************/

//	comment by May on 17-12-2003
//	public void process()
	/**
	 * This method store the transaction history
	 * @param presentation the Presentation class
	 */
	public void process(Presentation presentation)
	{
		try
		{
			Hashtable		lvTxnMap = new Hashtable();

			IMsgXMLNode		lvRetNode;
			//TPManager		ivTPManager = ITradeServlet.getTPManager();
			TPManager ivTPManager = ITradeServlet.getTPManager(RequestName.HKSTransactionHistoryRequest);
			TPBaseRequest   lvTransactionHistoryRequest = ivTPManager.getRequest(RequestName.HKSTransactionHistoryRequest);

			lvTxnMap.put(CLIENTID, getClientId());
			lvTxnMap.put(FROMDATE, getFromDate());
			lvTxnMap.put(TODATE, getToDate());

			lvRetNode = lvTransactionHistoryRequest.send(lvTxnMap);

			setReturnCode(mvTpError.checkError(lvRetNode));
			if (mvReturnCode != mvTpError.TP_NORMAL)
			{
				setErrorMessage(mvTpError.getErrDesc());
				if (mvReturnCode == mvTpError.TP_APP_ERR)
				{
					setErrorCode(mvTpError.getErrCode());
				}
			}
			else
			{

				IMsgXMLNodeList lvRowList = lvRetNode.getNodeList("Row");

                mvHKSTxnHistoryDetails = new HKSTxnHistoryDetails[lvRowList.size()];
                for (int i = 0; i < lvRowList.size(); i++)
                {
                    mvHKSTxnHistoryDetails[i] = new HKSTxnHistoryDetails();

                    IMsgXMLNode lvRow = lvRowList.getNode(i);
                    mvHKSTxnHistoryDetails[i].setClientId(lvRow.getChildNode("CLIENTID").getValue());
                    mvHKSTxnHistoryDetails[i].setTradeDate(lvRow.getChildNode("TRADEDATE").getValue());
                    mvHKSTxnHistoryDetails[i].setType(lvRow.getChildNode("TYPE").getValue() == null ? "" : lvRow.getChildNode("TYPE").getValue().trim());
                    mvHKSTxnHistoryDetails[i].setDesc(lvRow.getChildNode("DESCRIPTION").getValue());
                    mvHKSTxnHistoryDetails[i].setQty(lvRow.getChildNode("QTY").getValue());
                    mvHKSTxnHistoryDetails[i].setAvgPrice(lvRow.getChildNode("AVGPRICE").getValue());
                    mvHKSTxnHistoryDetails[i].setAmt(lvRow.getChildNode("AMOUNT").getValue());
                    mvHKSTxnHistoryDetails[i].setMarketId(lvRow.getChildNode("MARKETID").getValue());
                    mvHKSTxnHistoryDetails[i].setStockId(lvRow.getChildNode("STOCKID").getValue());
                    mvHKSTxnHistoryDetails[i].setName(lvRow.getChildNode("INSTRUMENTNAME").getValue());
                    mvHKSTxnHistoryDetails[i].setCShortName(lvRow.getChildNode("INSTRUMENTCHINESESHORTNAME").getValue());
                    // BEGIN TASK: CL00049 Charlie Lau 20081230
                    mvHKSTxnHistoryDetails[i].setRefId(lvRow.getChildNode("REFID").getValue());
                    mvHKSTxnHistoryDetails[i].setSettleDate(lvRow.getChildNode("SETTLEDATE").getValue());
                    mvHKSTxnHistoryDetails[i].setTxnType(lvRow.getChildNode("TXNTYPEID").getValue() == null ? "" : lvRow.getChildNode("TXNTYPEID").getValue().trim());
                    // END TASK: CL00062
                }

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	/**
	 * Get method for Client ID
	 * @return Client ID
	 */
	public String getClientId()
	{
		return mvClientId;
	}

	/**
	 * Set method for Client ID
	 * @param pClientId the Client ID
	 */
	public void setClientId(String pClientId)
	{
		mvClientId = pClientId;
	}


	/**
	 * Get method for From Date
	 * @return From Date
	 */
	public String getFromDate()
	{
		return mvFromDate;
	}

	/**
	 * Set method for From Date
	 * @param pFromDate the From Date
	 */
	public void setFromDate(String pFromDate)
	{
		mvFromDate = pFromDate;
	}


	/**
	 * Get method for To Date
	 * @return To Date
	 */
	public String getToDate()
	{
		return mvToDate;
	}

	/**
	 * Set method for To Date
	 * @param pToDate the To Date
	 */
	public void setToDate(String pToDate)
	{
		mvToDate = pToDate;
	}


	/**
	 * Get method for Status
	 * @return Status
	 */
	public String getStatus()
	{
		return mvStatus;
	}

	/**
	 * Set method for Status
	 * @param pStatus the Status
	 */
	public void setStatus(String pStatus)
	{
		mvStatus = pStatus;
	}

	/**
	 * Set method for Loop Counter
	 * @param pLoopCounter the Loop Counter
	 */
	public void setLoopCounter(String pLoopCounter)
	{
		mvLoopCounter = pLoopCounter;
	}

	/**
	 * Set method for Details for Transaction History
	 * @param pHKSTxnHistoryDetails the Array of Details for Transaction History
	 */
	public void setTxnHistoryDetails(HKSTxnHistoryDetails[] pHKSTxnHistoryDetails)
	{
		mvHKSTxnHistoryDetails = pHKSTxnHistoryDetails;
	}

	/**
	 * Get method for Details for Transaction History
	 * @return Array of Details for Transaction History
	 */
	public HKSTxnHistoryDetails[] getTxnHistoryDetails()
	{
		return mvHKSTxnHistoryDetails;
	}

	/**
	 * Get method for Return Code
	 * @return Return Code
	 */
	public int getReturnCode()
	{
		return mvReturnCode;
	}

	/**
	 * Set method for Return Code
	 * @param pReturnCode the Return Code
	 */
	public void setReturnCode(int pReturnCode)
	{
		mvReturnCode = pReturnCode;
	}


	/**
	 * Get method for Error Code
	 * @return Error Code
	 */
	public String getErrorCode()
	{
		return mvErrorCode;
	}

	/**
	 * Set method for Error Code
	 * @param pErrorCode the Error Code
	 */
	public void setErrorCode(String pErrorCode)
	{
		mvErrorCode = pErrorCode;
	}

	/**
	 * Get method for Error Message
	 * @return Error Message
	 */
	public String getErrorMessage()
	{
		return mvErrorMessage;
	}

	/**
	 * Set method for Error Message
	 * @param pErrorMessage the Error Message
	 */
	public void setErrorMessage(String pErrorMessage)
	{
		mvErrorMessage = pErrorMessage;
	}
}
