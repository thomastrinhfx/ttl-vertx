package com.ttl.old.itrade.hks.txn.plugin;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.hks.util.AgentUtils;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;
import com.systekit.winvest.hks.config.mapping.TagName;

public class HKSSubmitMarginExtensionTxn extends BaseTxn {
	
	private String mvClientID;
	private String mvClientName;
	private String mvExpireDate;
	private String mvPassword;
	private String mvSecurityCode;
	private String mvResult;
	private String mvReturnCode;
	private String mvReturnMsg;
	
	public String getMvClientID() {
		return mvClientID;
	}
	public void setMvClientID(String mvClientID) {
		this.mvClientID = mvClientID;
	}
	
	public String getMvClientName() {
		return mvClientName;
	}
	public void setMvClientName(String mvClientName) {
		this.mvClientName = mvClientName;
	}
	public String getMvExpireDate() {
		return mvExpireDate;
	}
	public void setMvExpireDate(String mvExpireDate) {
		this.mvExpireDate = mvExpireDate;
	}
	public String getMvPassword() {
		return mvPassword;
	}
	public void setMvPassword(String mvPassword) {
		this.mvPassword = mvPassword;
	}
	public String getMvSecurityCode() {
		return mvSecurityCode;
	}
	public void setMvSecurityCode(String mvSecurityCode) {
		this.mvSecurityCode = mvSecurityCode;
	}
	public String getMvResult() {
		return mvResult;
	}
	public void setMvResult(String mvResult) {
		this.mvResult = mvResult;
	}
	public String getMvReturnCode() {
		return mvReturnCode;
	}
	public void setMvReturnCode(String mvReturnCode) {
		this.mvReturnCode = mvReturnCode;
	}
	public String getMvReturnMsg() {
		return mvReturnMsg;
	}
	public void setMvReturnMsg(String mvReturnMsg) {
		this.mvReturnMsg = mvReturnMsg;
	}
	
	public HKSSubmitMarginExtensionTxn(String pClientID, String pExpireDate, String pClientName){
		mvClientID = pClientID;
		mvExpireDate = pExpireDate;
		mvClientName = pClientName;
	}
	
	/**
	 * duonghuynh, 2013-08-14: add email feature
	 * @return
	 */
	public Map<String, String> submitMarginExtensionCreation() {
		Map<String, String> result = new HashMap<String, String>();
		Hashtable lvTxnMap = new Hashtable();
		try {
			Log.println("HKSSubmitMarginExtensionTxn.submitMarginExtensionCreation START: Client ID = " + mvClientID , Log.ACCESS_LOG);
			
			lvTxnMap.put(TagName.CLIENTID, mvClientID);
			lvTxnMap.put(TagName.FULLNAME, mvClientName);
			lvTxnMap.put(TagName.EXPIRYDATE, mvExpireDate);
			lvTxnMap.put("OPRID", AgentUtils.getAgentID(this.getMvClientID()));
			lvTxnMap.put("EMAILLIST", AgentUtils.getOperatorEmails(this.getMvClientID(), AgentUtils.FUNCTION_TYPE_MARGIN_EXTENSION.toUpperCase()));
			// Send TP request
			if(TPErrorHandling.TP_NORMAL==process("SubmitMarginExtensionCreation",lvTxnMap)){
				setMvResult(mvReturnNode.getChildNode(TagName.RESULT).getValue());
		    	setMvReturnCode(mvReturnNode.getChildNode("RETURNCODE").getValue());
		    	setMvReturnMsg(mvReturnNode.getChildNode(TagName.RETURNMESSAGE).getValue());
			}
		} catch (Exception e) {
			Log.println("HKSSubmitMarginExtensionTxn.submitMarginExtensionCreation Error: " + e.getMessage(), Log.ERROR_LOG);
		} finally {
			Log.println("HKSSubmitMarginExtensionTxn.submitMarginExtensionCreation END: Client ID = " + mvClientID, Log.ACCESS_LOG);
			lvTxnMap.clear();
		}
		
		
		return result;
	}
}
