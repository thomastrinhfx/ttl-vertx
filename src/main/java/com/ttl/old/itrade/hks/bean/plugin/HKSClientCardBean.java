package com.ttl.old.itrade.hks.bean.plugin;

public class HKSClientCardBean {
	
	private String  mvErrorCode	="CARD001";
	private String  mvErrorMsg	="NONEXISTING DATA";
	private String  mvSerialnumber="22333";	  	 
	private String  mvWordMatrixKey01="4,F";
	private String  mvWordMatrixKey02="3,G";
	private int 	attempt		=0;
	private int 	attemptLimit=0;
	private String 	isLock		="0";
	
	// CARD001:NONEXISTING DATA,CARD006:New Key,CARD005:correct,CARD004:incorrect,CARD007:Input valid data,CARD002:ISLOCK
	 
	public HKSClientCardBean(){
		
	}
	
	public HKSClientCardBean(String mvSerialnumber, String mvWordMatrixKey01,
			String mvWordMatrixKey02) {
		super();
		this.mvSerialnumber = mvSerialnumber;
		this.mvWordMatrixKey01 = mvWordMatrixKey01;
		this.mvWordMatrixKey02 = mvWordMatrixKey02;
	}


	public String getMvErrorCode() {
		return mvErrorCode;
	}


	public void setMvErrorCode(String mvErrorCode) {
		this.mvErrorCode = mvErrorCode;
	}


	public String getMvErrorMsg() {
		return mvErrorMsg;
	}


	public void setMvErrorMsg(String mvErrorMsg) {
		this.mvErrorMsg = mvErrorMsg;
	}


	public String getMvSerialnumber() {
		return mvSerialnumber;
	}


	public void setMvSerialnumber(String mvSerialnumber) {
		this.mvSerialnumber = mvSerialnumber;
	}


	public String getMvWordMatrixKey01() {
		return mvWordMatrixKey01;
	}


	public void setMvWordMatrixKey01(String mvWordMatrixKey01) {
		this.mvWordMatrixKey01 = mvWordMatrixKey01;
	}


	public String getMvWordMatrixKey02() {
		return mvWordMatrixKey02;
	}


	public void setMvWordMatrixKey02(String mvWordMatrixKey02) {
		this.mvWordMatrixKey02 = mvWordMatrixKey02;
	}

	public int getAttempt() {
		return attempt;
	}

	public void setAttempt(int attempt) {
		this.attempt = attempt;
	}

	public int getAttemptLimit() {
		return attemptLimit;
	}

	public void setAttemptLimit(int attemptLimit) {
		this.attemptLimit = attemptLimit;
	}

	public String getIsLock() {
		return isLock;
	}

	public void setIsLock(String isLock) {
		this.isLock = isLock;
	}	
	 
}
