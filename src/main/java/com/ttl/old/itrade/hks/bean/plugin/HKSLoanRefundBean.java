package com.ttl.old.itrade.hks.bean.plugin;

public class HKSLoanRefundBean {
	
	private String tranID;
	private String tradeDate;
	private String refundAmt;
	private String type;
	private String status;
	private String remark;
	private String lastupdate;
	
	/**
	 * @return the tranID
	 */
	public String getTranID() {
		return tranID;
	}
	
	/**
	 * @param tranID the tranID to set
	 */
	public void setTranID(String tranID) {
		this.tranID = tranID;
	}
	
	/**
	 * @return the tradeDate
	 */
	public String getTradeDate() {
		return tradeDate;
	}
	
	/**
	 * @param tradeDate the tradeDate to set
	 */
	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}
	
	/**
	 * @return the refundAmt
	 */
	public String getRefundAmt() {
		return refundAmt;
	}
	
	/**
	 * @param refundAmt the refundAmt to set
	 */
	public void setRefundAmt(String refundAmt) {
		this.refundAmt = refundAmt;
	}
	
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}
	
	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	/**
	 * @return the lastupdate
	 */
	public String getLastupdate() {
		return lastupdate;
	}
	
	/**
	 * @param lastupdate the lastupdate to set
	 */
	public void setLastupdate(String lastupdate) {
		this.lastupdate = lastupdate;
	}
	
	
}
