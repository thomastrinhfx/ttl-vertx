//BEGIN TASK: TTL-GZ-JJ-00001 JiangJi 20091206 [iTradeR5 VN] FO:Pop up stock info

package com.ttl.old.itrade.hks.bean;

/**
 * The HKSStockInfoBean class define variables that to save values 
 * for action 
 * @author
 *
 */
public class HKSStockInfoBean {
	private String mvdLedgerBalance;
	private String mvUsableBalance;
	private String mvTodayConfirmSell;
	private String mvTodayConfirmBuy;
	private String mvBuyingPowerd;
	private String mvUsable;
	private String mvCurrentRoom;
	private String mvStatusDscription;
	
	//BEGIN TASK: TTL-GZ-JJ-00008 JiangJi 20100108 [iTrade R5]Ceiling and Floor in Popup
	private String mvCeiling;
	private String mvFloor;
	//END TASK: TTL-GZ-JJ-00008 JiangJi 20100108 [iTrade R5]Ceiling and Floor in Popup
	//BEGIN TASK: TTL-GZ-JJ-00016 JiangJi 20100310 [iTrade R5] Stock popup new layout
	private String mvStockName;
	private String mvDayOpen;
	private String mvDayClose;
	private String mvReferencePrice;
	//END TASK: TTL-GZ-JJ-00016 JiangJi 20100310 [iTrade R5] Stock popup new layout
	
	//BEGIN TASK: TTL-VN VanTran 20100426 [iTrade R5] Stock popup new layout
	private String mvBidPrice1;
	private String mvBidPrice2;
	private String mvBidPrice3;
	private String mvBidVol1;
	private String mvBidVol2;
	private String mvBidVol3;
	private String mvOfferPrice1;
	private String mvOfferPrice2;
	private String mvOfferPrice3;
	
	
	private String mvHigh;
	private String mvLow;
	private String mvNomial;
	
	
	private String mvTemporaryFee;
	//BEGIN HIEU LE: Add margin percentage field
	private String mvMarginPercentage;
	//END HIEU LE
	private boolean isUpdate;
	
	private String mvCSettledCashBal;
	private String mvLedgerCashBal;
	private String mvManualReserve;
	
	private String mvSpreadTableCode;
	
	public String getMvBidPrice1() {
		return mvBidPrice1;
	}

	public void setMvBidPrice1(String mvBidPrice1) {
		this.mvBidPrice1 = mvBidPrice1;
	}

	public String getMvBidPrice2() {
		return mvBidPrice2;
	}

	public void setMvBidPrice2(String mvBidPrice2) {
		this.mvBidPrice2 = mvBidPrice2;
	}

	public String getMvBidPrice3() {
		return mvBidPrice3;
	}

	public void setMvBidPrice3(String mvBidPrice3) {
		this.mvBidPrice3 = mvBidPrice3;
	}

	public String getMvOfferPrice1() {
		return mvOfferPrice1;
	}

	public void setMvOfferPrice1(String mvOfferPrice1) {
		this.mvOfferPrice1 = mvOfferPrice1;
	}

	public String getMvOfferPrice2() {
		return mvOfferPrice2;
	}

	public void setMvOfferPrice2(String mvOfferPrice2) {
		this.mvOfferPrice2 = mvOfferPrice2;
	}

	public String getMvOfferPrice3() {
		return mvOfferPrice3;
	}

	public void setMvOfferPrice3(String mvOfferPrice3) {
		this.mvOfferPrice3 = mvOfferPrice3;
	}

	public String getMvBidVol1() {
		return mvBidVol1;
	}

	public void setMvBidVol1(String mvBidVol1) {
		this.mvBidVol1 = mvBidVol1;
	}

	public String getMvBidVol2() {
		return mvBidVol2;
	}

	public void setMvBidVol2(String mvBidVol2) {
		this.mvBidVol2 = mvBidVol2;
	}

	public String getMvBidVol3() {
		return mvBidVol3;
	}

	public void setMvBidVol3(String mvBidVol3) {
		this.mvBidVol3 = mvBidVol3;
	}

	public String getMvOfferVol1() {
		return mvOfferVol1;
	}

	public void setMvOfferVol1(String mvOfferVol1) {
		this.mvOfferVol1 = mvOfferVol1;
	}

	public String getMvOfferVol2() {
		return mvOfferVol2;
	}

	public void setMvOfferVol2(String mvOfferVol2) {
		this.mvOfferVol2 = mvOfferVol2;
	}

	public String getMvOfferVol3() {
		return mvOfferVol3;
	}

	public void setMvOfferVol3(String mvOfferVol3) {
		this.mvOfferVol3 = mvOfferVol3;
	}
	private String mvOfferVol1;
	private String mvOfferVol2;
	private String mvOfferVol3;
	//END TASK: TTL-VN VanTran 20100426 [iTrade R5] Stock popup new layout
	
	/**
	 * This method returns the buying power.
	 * @return the buying power is not formatted.
     */
	public String getMvBuyingPowerd() {
		return mvBuyingPowerd;
	}
	
	/**
     * This method sets the buying power.
     * @param pBuyingPowerd The buying power.
     */
	public void setMvBuyingPowerd(String pBuyingPowerd) {
		this.mvBuyingPowerd = pBuyingPowerd;
	}
	
	/**
	 * This method returns the usable.
	 * @return the usable is not formatted.
     */
	public String getMvUsable() {
		return mvUsable;
	}
	
	/**
     * This method sets the usable.
     * @param pUsable The usable.
     */
	public void setMvUsable(String pUsable) {
		this.mvUsable = pUsable;
	}
	
	/**
	 * This method returns the ledger balance.
	 * @return the dLedger balance is not formatted.
     */
	public String getMvdLedgerBalance() {
		return mvdLedgerBalance;
	}
	
	/**
     * This method sets the ledger balance.
     * @param pdLedgerBalance The ledger balance.
     */
	public void setMvdLedgerBalance(String pdLedgerBalance) {
		this.mvdLedgerBalance = pdLedgerBalance;
	}
	
	/**
	 * This method returns the today confirm sell.
	 * @return the today confirm sell is not formatted.
     */
	public String getMvTodayConfirmSell() {
		return mvTodayConfirmSell;
	}
	
	/**
     * This method sets the today confirm sell.
     * @param pTodayConfirmSell The today confirm sell.
     */
	public void setMvTodayConfirmSell(String pTodayConfirmSell) {
		this.mvTodayConfirmSell = pTodayConfirmSell;
	}
	
	/**
	 * This method returns the today confirm buy.
	 * @return the today confirm buy is not formatted.
     */
	public String getMvTodayConfirmBuy() {
		return mvTodayConfirmBuy;
	}
	
	/**
     * This method sets the today confirm buy.
     * @param pTodayConfirmBuy The today confirm buy.
     */
	public void setMvTodayConfirmBuy(String pTodayConfirmBuy) {
		this.mvTodayConfirmBuy = pTodayConfirmBuy;
	}
	
	/**
	 * This method returns the usable balance.
	 * @return the usable balance is not formatted.
     */
	public String getMvUsableBalance() {
		return mvUsableBalance;
	}
	
	/**
     * This method sets the usable balance.
     * @param pUsableBalance The usable balance.
     */
	public void setMvUsableBalance(String pUsableBalance) {
		this.mvUsableBalance = pUsableBalance;
	}
	
	/**
	 * This method returns the current room.
	 * @return the current room is not formatted.
     */
	public String getMvCurrentRoom() {
		return mvCurrentRoom;
	}
	
	/**
     * This method sets the current room.
     * @param pCurrentRoom The current room.
     */
	public void setMvCurrentRoom(String pCurrentRoom) {
		this.mvCurrentRoom = pCurrentRoom;
	}
	
	/**
	 * This method returns the status description.
	 * @return the status description is not formatted.
     */
	public String getMvStatusDscription() {
		return mvStatusDscription;
	}
	
	/**
     * This method sets the status description.
     * @param pStatusDscription The status description.
     */
	public void setMvStatusDscription(String pStatusDscription) {
		this.mvStatusDscription = pStatusDscription;
	}
	
	//BEGIN TASK: TTL-GZ-JJ-00008 JiangJi 20100108 [iTrade R5]Ceiling and Floor in Popup
	
	/**
	 * This method returns the ceiling price.
	 * @return the ceiling price.
     */
	public String getMvCeiling() {
		return mvCeiling;
	}
	
	/**
     * This method sets the the ceiling price.
     * @param pCeiling The ceiling price.
     */
	public void setMvCeiling(String pCeiling) {
		this.mvCeiling = pCeiling;
	}
	
	/**
	 * This method returns the floor price.
	 * @return the ceiling price.
     */
	public String getMvFloor() {
		return mvFloor;
	}
	
	/**
     * This method sets the the floor price.
     * @param pFloor The floor price.
     */
	public void setMvFloor(String pFloor) {
		this.mvFloor = pFloor;
	}
	//END TASK: TTL-GZ-JJ-00008 JiangJi 20100108 [iTrade R5]Ceiling and Floor in Popup
	//BEGIN TASK: TTL-GZ-JJ-00016 JiangJi 20100310 [iTrade R5] Stock popup new layout
	/**
	 * Get the stock name
	 * @return The stock name
	 */
	public String getMvStockName() {
		return mvStockName;
	}
	/**
	 * Sets the stock name
	 * @param pStockName the stock name
	 */
	public void setMvStockName(String pStockName) {
		this.mvStockName = pStockName;
	}
	/**
	 * Get the day open price
	 * @return The day open price
	 */
	public String getMvDayOpen() {
		return mvDayOpen;
	}
	/**
	 * Sets the day open price
	 * @param pDayOpen the day open price
	 */
	public void setMvDayOpen(String pDayOpen) {
		this.mvDayOpen = pDayOpen;
	}
	/**
	 * Get the reference price
	 * @return The reference price
	 */
	public String getMvReferencePrice() {
		return mvReferencePrice;
	}
	/**
	 * Sets the reference price
	 * @param pReferencePrice the reference price
	 */
	public void setMvReferencePrice(String pReferencePrice) {
		this.mvReferencePrice = pReferencePrice;
	}
	//END TASK: TTL-GZ-JJ-00016 JiangJi 20100310 [iTrade R5] Stock popup new layout

	public void setMvHigh(String mvHigh) {
		this.mvHigh = mvHigh;
	}

	public String getMvHigh() {
		return mvHigh;
	}

	public void setMvLow(String mvLow) {
		this.mvLow = mvLow;
	}

	public String getMvLow() {
		return mvLow;
	}

	public void setMvNomial(String mvNomial) {
		this.mvNomial = mvNomial;
	}

	public String getMvNomial() {
		return mvNomial;
	}

	/**
	 * @param mvDayClose the mvDayClose to set
	 */
	public void setMvDayClose(String mvDayClose) {
		this.mvDayClose = mvDayClose;
	}

	/**
	 * @return the mvDayClose
	 */
	public String getMvDayClose() {
		return mvDayClose;
	}

	/**
	 * @param mvTemporaryFee the mvTemporaryFee to set
	 */
	public void setMvTemporaryFee(String mvTemporaryFee) {
		this.mvTemporaryFee = mvTemporaryFee;
	}

	/**
	 * @return the mvTemporaryFee
	 */
	public String getMvTemporaryFee() {
		return mvTemporaryFee;
	}

	public boolean isUpdate() {
		return isUpdate;
	}

	public void setUpdate(boolean isUpdate) {
		this.isUpdate = isUpdate;
	}

	/**
	 * @return the mvCSettledCashBal
	 */
	public String getMvCSettledCashBal() {
		return mvCSettledCashBal;
	}

	/**
	 * @param mvCSettledCashBal the mvCSettledCashBal to set
	 */
	public void setMvCSettledCashBal(String mvCSettledCashBal) {
		this.mvCSettledCashBal = mvCSettledCashBal;
	}

	/**
	 * @return the mvLedgerCashBal
	 */
	public String getMvLedgerCashBal() {
		return mvLedgerCashBal;
	}

	/**
	 * @param mvLedgerCashBal the mvLedgerCashBal to set
	 */
	public void setMvLedgerCashBal(String mvLedgerCashBal) {
		this.mvLedgerCashBal = mvLedgerCashBal;
	}
	
	public String getMvMarginPercentage() {
		return mvMarginPercentage;
	}

	public void setMvMarginPercentage(String mvMarginPercentage) {
		this.mvMarginPercentage = mvMarginPercentage;
	}

	public String getMvManualReserve() {
		return mvManualReserve;
	}

	public void setMvManualReserve(String mvManualReserve) {
		this.mvManualReserve = mvManualReserve;
	}

	public String getSpreadTableCode() {
		return mvSpreadTableCode;
	}

	public void setSpreadTableCode(String spreadTableCode) {
		this.mvSpreadTableCode = spreadTableCode;
	}
}
//END TASK: TTL-GZ-JJ-00001 JiangJi 20091206 [iTradeR5 VN] FO:Pop up stock info