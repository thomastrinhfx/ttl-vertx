package com.ttl.old.itrade.hks.bean;

import java.util.Date;

/**
 * The HKSMarketDataBean class define variables that to save values 
 * for action 
 * @author Wind Zhao
 *
 */

public class HKSMarketDataBean {
	
	private String mvSymbol;
	private int mvNomCmToRefPri;
	private String mvCeilingPrice;
	private String mvFloorPrice;
	private String mvReferencePrice;
	private String mvBestBid1Price;
	private String mvBestBid1Volume;
	private String mvBestBid2Price;
	private String mvBestBid2Volume;
	private String mvBestBid3Price;
	private String mvBestBid3Volume;
	
	private int mvBestBid1Css;
	private int mvBestBid2Css;
	private int mvBestBid3Css;
	private String mvNominalPrice;
	private int mvNoalPriCss;
	private String mvTotalVol;
	private int mvTalVolCss;
	private String mvNoalPriSubRefPri;
	private String mvNoalPriPerRate;
	private int mvNoalPriSubRefPriCss;
	private int mvNoalPriPerRateCss;
	private String mvBestOffer1Price;
	private String mvBestOffer1Volume;
	private String mvBestOffer2Price;
	private String mvBestOffer2Volume;
	private String mvBestOffer3Price;
	private String mvBestOffer3Volume;
	private int mvBestOffer1Css;
	private String mvClosePrice;
	private String mvMarketID;
	private int mvBestOffer2Css;
	private int mvBestOffer3Css;
	private String mvOpenPrice;
	private int mvOpenPriCss;
	private String mvHighPrice;
	private int mvHighPriCss;
	private String mvLowPrice;
	private int mvLowPriCss;
	private String mvStockName;
	
	private String mvTargetBuyPrc;
	private String mvStopProfitPrice;
	private String mvStopLostPrice;
	
	private Date lstUpdate;
	
	private String avgPrice;
	
	private int avgPriceCss = 0;
	
	public HKSMarketDataBean()
	{
		mvBestBid1Css = 0;
		mvBestBid1Price = "";
		mvBestBid1Volume = "";
		mvBestBid2Css = 0;
		mvBestBid2Price = "";
		mvBestBid2Volume = "";
		mvBestBid3Css = 0;
		mvBestBid3Price = "";
		mvBestBid3Volume = "";
		mvBestOffer1Css = 0;
		mvBestOffer1Price = "";
		mvBestOffer1Volume = "";
		mvBestOffer2Css = 0;
		mvBestOffer2Price = "";
		mvBestOffer2Volume = "";
		mvBestOffer3Css = 0;
		mvBestOffer3Price = "";
		mvBestOffer3Volume = "";
		mvBuyForeignQty = "";
		mvCeilingPrice = "";
		mvClosePrice = "";
		mvCurrentRoom = "";
		mvFloorPrice = "";
		mvHighPrice = "";
		mvHighPriCss = 0;
		mvLowPrice = "";
		mvLowPriCss = 0;
		mvMarketID = "";
		mvMatchPrice = "";
		mvMatchQty = "";
		mvMatchQtyCss = 0;
		mvNoalPriCss = 0;
		mvNoalPriSubRefPri = "";
		mvNoalPriSubRefPriCss = 0;
		mvNomCmToRefPri = 0;
		mvNominalPrice = "";
		mvOpenPrice = "";
		mvOpenPriCss = 0;
		mvReferencePrice = "";
		mvSellForeignQty = "";
		mvStockName = "";
		mvStopLostPrice = "";
		mvStopProfitPrice = "";
		mvSymbol = "";
		mvTalVolCss = 0;
		mvTargetBuyPrc = "";
		mvTotalTradingQty = "";
		mvTotalTradingValue = "";
		mvTotalVol = "";
		mvNoalPriPerRate = "";
		mvNoalPriPerRateCss = 0;
		lstUpdate = null;
	}
	
	/**
	 * @return the mvMatchPrice
	 */
	public String getMvMatchPrice() {
		return formatIfZero(mvMatchPrice);
	}

	/**
	 * @param mvMatchPrice the mvMatchPrice to set
	 */
	public void setMvMatchPrice(String mvMatchPrice) {
		this.mvMatchPrice = mvMatchPrice;
	}

	/**
	 * @return the mvMatchQty
	 */
	public String getMvMatchQty() {
		return mvMatchQty;
	}

	/**
	 * @param mvMatchQty the mvMatchQty to set
	 */
	public void setMvMatchQty(String mvMatchQty) {
		this.mvMatchQty = mvMatchQty;
	}

	/**
	 * @return the mvTotalTradingQty
	 */
	public String getMvTotalTradingQty() {
		return formatIfZero(mvTotalTradingQty);
	}

	/**
	 * @param mvTotalTradingQty the mvTotalTradingQty to set
	 */
	public void setMvTotalTradingQty(String mvTotalTradingQty) {
		this.mvTotalTradingQty = mvTotalTradingQty;
	}

	/**
	 * @return the mvTotalTradingValue
	 */
	public String getMvTotalTradingValue() {
		return mvTotalTradingValue;
	}

	/**
	 * @param mvTotalTradingValue the mvTotalTradingValue to set
	 */
	public void setMvTotalTradingValue(String mvTotalTradingValue) {
		this.mvTotalTradingValue = mvTotalTradingValue;
	}

	/**
	 * @return the mvBuyForeignQty
	 */
	public String getMvBuyForeignQty() {
		return mvBuyForeignQty;
	}

	/**
	 * @param mvBuyForeignQty the mvBuyForeignQty to set
	 */
	public void setMvBuyForeignQty(String mvBuyForeignQty) {
		this.mvBuyForeignQty = mvBuyForeignQty;
	}

	/**
	 * @return the mvCurrentRoom
	 */
	public String getMvCurrentRoom() {
		return mvCurrentRoom;
	}

	/**
	 * @param mvCurrentRoom the mvCurrentRoom to set
	 */
	public void setMvCurrentRoom(String mvCurrentRoom) {
		this.mvCurrentRoom = mvCurrentRoom;
	}

	private String mvMatchPrice;
	private int 	mvMatchPriceCss;
	private String mvMatchQty;
	private int 	mvMatchQtyCss;
	private String mvTotalTradingQty;
	private String mvTotalTradingValue;
	private String mvBuyForeignQty;
	private String mvCurrentRoom;
	
	private String mvSellForeignQty;
	
	/**
     * This method returns the market id.
     * @return the market id.
     */
	public String getMvMarketID() {
		return mvMarketID;
	}
	
	/**
     * This method sets the market id.
     * @param pMarketID The market id.
     */
	public void setMvMarketID(String pMarketID) {
		mvMarketID = pMarketID;
	}
	
	/**
     * This method returns the close price.
     * @return the close price is formatted.
     */
	public String getMvClosePrice() {
		return mvClosePrice;
	}
	
	/**
     * This method sets the close price.
     * @param pClosePrice The close price is formatted.
     */
	public void setMvClosePrice(String pClosePrice) {
		mvClosePrice = pClosePrice;
	}
	
	/**
     * This method returns the low price.
     * @return the low price is formatted.
     */
	public String getMvLowPrice() {
		return formatIfZero(mvLowPrice);
	}
	
	/**
     * This method sets the low price.
     * @param pLowPrice The low price is formatted.
     */
	public void setMvLowPrice(String pLowPrice) {
		mvLowPrice = pLowPrice;
	}
	
	/**
     * This method returns the low price css.
     * @return the low price css.
     */
	public int getMvLowPriCss() {
		return computeCssDefault(getMvLowPrice(), mvLowPriCss);
	}
	
	/**
     * This method sets the low price css.
     * @param pLowPriCss The low price css.
     */
	public void setMvLowPriCss(int pLowPriCss) {
		mvLowPriCss = pLowPriCss;
	}
	
	/**
     * This method returns the high price.
     * @return the high price is formatted.
     */
	public String getMvHighPrice() {
		return formatIfZero(mvHighPrice);
	}
	
	/**
     * This method sets the high price.
     * @param pHighPrice The high price is formatted.
     */
	public void setMvHighPrice(String pHighPrice) {
		mvHighPrice = pHighPrice;
	}
	
	/**
     * This method returns the high price css.
     * @return the high price css.
     */
	public int getMvHighPriCss() {
		return computeCssDefault(getMvHighPrice(), mvHighPriCss);
	}
	
	/**
     * This method sets the high price css.
     * @param pHighPriCss The high price css.
     */
	public void setMvHighPriCss(int pHighPriCss) {
		mvHighPriCss = pHighPriCss;
	}
	
	/**
     * This method returns the open price.
     * @return the open price is formatted.
     */
	public String getMvOpenPrice() {
		return formatIfZero(mvOpenPrice);
	}
	
	/**
     * This method sets the open price.
     * @param pOpenPrice The open price is formatted.
     */
	public void setMvOpenPrice(String pOpenPrice) {
		mvOpenPrice = pOpenPrice;
	}
	
	/**
     * This method returns the open price css.
     * @return the open price css.
     */
	public int getMvOpenPriCss() {
		return computeCssDefault(getMvOpenPrice(), mvOpenPriCss);
	}
	
	/**
     * This method sets the open price css.
     * @param pOpenPriCss The open price css.
     */
	public void setMvOpenPriCss(int pOpenPriCss) {
		mvOpenPriCss = pOpenPriCss;
	}
	
	/**
     * This method returns the best offer1 css.
     * @return the best offer1 css.
     */
	public int getMvBestOffer1Css() {
		return computeCssDefault(getMvBestOffer1Price(), mvBestOffer1Css);
	}
	
	/**
     * This method sets the best offer1 css.
     * @param pBestOffer1Css The best offer1 css.
     */
	public void setMvBestOffer1Css(int pBestOffer1Css) {
		mvBestOffer1Css = pBestOffer1Css;
	}
	
	/**
     * This method returns the best offer2 css.
     * @return the best offer2 css.
     */
	public int getMvBestOffer2Css() {
		return computeCssDefault(getMvBestOffer2Price(), mvBestOffer2Css);
	}
	
	/**
     * This method sets the best offer2 css.
     * @param pBestOffer2Css The best offer2 css.
     */
	public void setMvBestOffer2Css(int pBestOffer2Css) {
		mvBestOffer2Css = pBestOffer2Css;
	}
	
	/**
     * This method returns the best offer3 css.
     * @return the best offer3 css.
     */
	public int getMvBestOffer3Css() {
		return computeCssDefault(getMvBestOffer3Price(), mvBestOffer3Css);
	}
	
	/**
     * This method sets the best offer3 css.
     * @param pBestOffer3Css The best offer3 css.
     */
	public void setMvBestOffer3Css(int pBestOffer3Css) {
		mvBestOffer3Css = pBestOffer3Css;
	}
	
	/**
     * This method returns the best offer1 price.
     * @return the best offer1 price is formatted.
     */
	public String getMvBestOffer1Price() {
		return formatIfZero(mvBestOffer1Price);
	}
	
	/**
     * This method sets the best offer1 price.
     * @param pBestOffer1Price The best offer1 price is formatted.
     */
	public void setMvBestOffer1Price(String pBestOffer1Price) {
		mvBestOffer1Price = pBestOffer1Price;
	}
	
	/**
     * This method returns the best offer1 volume.
     * @return the best offer1 volume is formatted.
     */
	public String getMvBestOffer1Volume() {
		return formatIfZero(mvBestOffer1Volume);
	}
	
	/**
     * This method sets the best offer1 volume.
     * @param pBestOffer1Volume The best offer1 volume is formatted.
     */
	public void setMvBestOffer1Volume(String pBestOffer1Volume) {
		mvBestOffer1Volume = pBestOffer1Volume;
	}
	
	/**
     * This method returns the best offer2 price.
     * @return the best offer2 price is formatted.
     */
	public String getMvBestOffer2Price() {
		return formatIfZero(mvBestOffer2Price);
	}
	
	/**
     * This method sets the best offer2 price.
     * @param pBestOffer2Price The best offer2 price is formatted.
     */
	public void setMvBestOffer2Price(String pBestOffer2Price) {
		mvBestOffer2Price = pBestOffer2Price;
	}
	
	/**
     * This method returns the best offer2 volume.
     * @return the best offer2 volume is formatted.
     */
	public String getMvBestOffer2Volume() {
		return formatIfZero(mvBestOffer2Volume);
	}
	
	/**
     * This method sets the best offer2 volume.
     * @param pBestOffer2Volume The best offer2 volume is formatted.
     */
	public void setMvBestOffer2Volume(String pBestOffer2Volume) {
		mvBestOffer2Volume = pBestOffer2Volume;
	}
	
	/**
     * This method returns the best offer3 price.
     * @return the best offer3 price is formatted.
     */
	public String getMvBestOffer3Price() {
		return formatIfZero(mvBestOffer3Price);
	}
	
	/**
     * This method sets the best offer3 price.
     * @param pBestOffer3Price The best offer3 price is formatted.
     */
	public void setMvBestOffer3Price(String pBestOffer3Price) {
		mvBestOffer3Price = pBestOffer3Price;
	}
	
	/**
     * This method returns the best offer3 volume.
     * @return the best offer3 volume is formatted.
     */
	public String getMvBestOffer3Volume() {
		return formatIfZero(mvBestOffer3Volume);
	}
	
	/**
     * This method sets the best offer3 volume.
     * @param pBestOffer3Volume The best offer3 volume is formatted.
     */
	public void setMvBestOffer3Volume(String pBestOffer3Volume) {
		mvBestOffer3Volume = pBestOffer3Volume;
	}
	
	/**
     * This method returns the nominal price sub ref price css.
     * @return the nominal price sub ref price css.
     */
	public int getMvNoalPriSubRefPriCss() {
		return mvNoalPriSubRefPriCss;
	}
	
	/**
     * This method sets the nominal price sub ref price css.
     * @param pNoalPriSubRefPriCss The nominal price sub ref price css.
     */
	public void setMvNoalPriSubRefPriCss(int pNoalPriSubRefPriCss) {
		mvNoalPriSubRefPriCss = pNoalPriSubRefPriCss;
	}
	
	/**
     * This method returns the nominal price sub ref price.
     * @return the nominal price sub ref price is formatted.
     */
	public String getMvNoalPriSubRefPri() {
		return mvNoalPriSubRefPri;
	}
	
	/**
     * This method sets the nominal price sub ref price.
     * @param pNoalPriSubRefPri The nominal price sub ref price is formatted.
     */
	public void setMvNoalPriSubRefPri(String pNoalPriSubRefPri) {
		mvNoalPriSubRefPri = pNoalPriSubRefPri;
	}
	
	public String getMvNoalPriPerRate() {
		return mvNoalPriPerRate;
	}

	public void setMvNoalPriPerRate(String pNoalPriPerRate) {
		mvNoalPriPerRate = pNoalPriPerRate;
	}

	public int getMvNoalPriPerRateCss() {
		return mvNoalPriPerRateCss;
	}

	public void setMvNoalPriPerRateCss(int pNoalPriPerRateCss) {
		mvNoalPriPerRateCss = pNoalPriPerRateCss;
	}

	/**
     * This method returns the total volume.
     * @return the total volume is formatted.
     */
	public String getMvTotalVol() {
		return mvTotalVol;
	}
	
	/**
     * This method sets the total volume.
     * @param pTotalVol The total volume is formatted.
     */
	public void setMvTotalVol(String pTotalVol) {
		mvTotalVol = pTotalVol;
	}
	
	/**
     * This method returns the total volume css.
     * @return the total volume css.
     */
	public int getMvTalVolCss() {
		return computeCssDefault(getMvTotalVol(), mvTalVolCss);
	}
	
	/**
     * This method sets the total volume css.
     * @param pTalVolCss The total volume css.
     */
	public void setMvTalVolCss(int pTalVolCss) {
		mvTalVolCss = pTalVolCss;
	}
	
	/**
     * This method returns the nominal price.
     * @return the nominal price is formatted.
     */
	public String getMvNominalPrice() {
		return formatIfZero(mvNominalPrice);
	}
	
	/**
     * This method sets the nominal price.
     * @param pNominalPrice The nominal price is formatted.
     */
	public void setMvNominalPrice(String pNominalPrice) {
		mvNominalPrice = pNominalPrice;
	}
	
	/**
     * This method returns the best bid1 css.
     * @return the best bid1 css.
     */
	public int getMvBestBid1Css() {
		return computeCssDefault(getMvBestBid1Price(), mvBestBid1Css);
	}
	
	/**
     * This method sets the best bid1 css.
     * @param pBestBid1Css The best bid1 css.
     */
	public void setMvBestBid1Css(int pBestBid1Css) {
		mvBestBid1Css = pBestBid1Css;
	}
	
	/**
     * This method returns the best bid2 css.
     * @return the best bid2 css.
     */
	public int getMvBestBid2Css() {
		return computeCssDefault(getMvBestBid2Price(), mvBestBid2Css);
	}
	
	/**
     * This method sets the best bid2 css.
     * @param pBestBid2Css The best bid2 css.
     */
	public void setMvBestBid2Css(int pBestBid2Css) {
		mvBestBid2Css = pBestBid2Css;
	}
	
	/**
     * This method returns the best bid3 css.
     * @return the best bid3 css.
     */
	public int getMvBestBid3Css() {
		return computeCssDefault(getMvBestBid3Price(), mvBestBid3Css);
	}
	
	/**
     * This method sets the best bid3 css.
     * @param pBestBid3Css The best bid3 css.
     */
	public void setMvBestBid3Css(int pBestBid3Css) {
		mvBestBid3Css = pBestBid3Css;
	}
	
	/**
     * This method returns the nominal price css.
     * @return the nominal price css.
     */
	public int getMvNoalPriCss() {
		return computeCssDefault(getMvNominalPrice(), mvNoalPriCss);
	}
	
	/**
     * This method sets the nominal price css.
     * @param pNoalPriCss The nominal price css.
     */
	public void setMvNoalPriCss(int pNoalPriCss) {
		mvNoalPriCss = pNoalPriCss;
	}
	
	/**
     * This method returns the best bid1 price.
     * @return the best bid1 price is formatted.
     */
	public String getMvBestBid1Price() {
		return formatIfZero(mvBestBid1Price);
	}
	
	/**
     * This method sets the best bid1 price.
     * @param pBestBid1Price The best bid1 price is formatted.
     */
	public void setMvBestBid1Price(String pBestBid1Price) {
		mvBestBid1Price = pBestBid1Price;
	}
	
	/**
     * This method returns the best bid1 volume.
     * @return the best bid1 volume is formatted.
     */
	public String getMvBestBid1Volume() {
		return formatIfZero(mvBestBid1Volume);
	}
	
	/**
     * This method sets the best bid1 volume.
     * @param pBestBid1Volume The best bid1 volume is formatted.
     */
	public void setMvBestBid1Volume(String pBestBid1Volume) {
		mvBestBid1Volume = pBestBid1Volume;
	}
	
	/**
     * This method returns the best bid2 price.
     * @return the best bid2 price is formatted.
     */
	public String getMvBestBid2Price() {
		return formatIfZero(mvBestBid2Price);
	}
	
	/**
     * This method sets the best bid2 price.
     * @param pBestBid2Price The best bid2 price is formatted.
     */
	public void setMvBestBid2Price(String pBestBid2Price) {
		mvBestBid2Price = pBestBid2Price;
	}
	
	/**
     * This method returns the best bid2 volume.
     * @return the best bid2 volume is formatted.
     */
	public String getMvBestBid2Volume() {
		return formatIfZero(mvBestBid2Volume);
	}
	
	/**
     * This method sets the best bid2 volume.
     * @param pBestBid2Volume The best bid2 volume is formatted.
     */
	public void setMvBestBid2Volume(String pBestBid2Volume) {
		mvBestBid2Volume = pBestBid2Volume;
	}
	
	/**
     * This method returns the best bid3 price.
     * @return the best bid3 price is formatted.
     */
	public String getMvBestBid3Price() {
		return formatIfZero(mvBestBid3Price);
	}
	
	/**
     * This method sets the best bid3 price.
     * @param pBestBid3Price The best bid3 price is formatted.
     */
	public void setMvBestBid3Price(String pBestBid3Price) {
		mvBestBid3Price = pBestBid3Price;
	}
	
	/**
     * This method returns the best bid3 volume.
     * @return the best bid3 volume is formatted.
     */
	public String getMvBestBid3Volume() {
		return formatIfZero(mvBestBid3Volume);
	}
	
	/**
     * This method sets the best bid3 volume.
     * @param pBestBid3Volume The best bid3 volume is formatted.
     */
	public void setMvBestBid3Volume(String pBestBid3Volume) {
		mvBestBid3Volume = pBestBid3Volume;
	}
	
	/**
     * This method returns the reference price.
     * @return the reference price.
     */
	public String getMvReferencePrice() {
		return formatIfZero(mvReferencePrice);
	}
	
	/**
     * This method sets the reference price.
     * @param pReferencePrice The reference price.
     */
	public void setMvReferencePrice(String pReferencePrice) {
		mvReferencePrice = pReferencePrice;
	}
	
	/**
     * This method returns the floor price.
     * @return the floor price is formatted.
     */
	public String getMvFloorPrice() {
		return mvFloorPrice;
	}
	
	/**
     * This method sets the floor price.
     * @param pFloorPrice The floor price is formatted.
     */
	public void setMvFloorPrice(String pFloorPrice) {
		mvFloorPrice = pFloorPrice;
	}
	
	/**
     * This method returns the ceiling price.
     * @return the ceiling price is formatted.
     */
	public String getMvCeilingPrice() {
		return mvCeilingPrice;
	}
	
	/**
     * This method sets the ceiling price.
     * @param pFloorPrice The ceiling price is formatted.
     */
	public void setMvCeilingPrice(String pCeilingPrice) {
		mvCeilingPrice = pCeilingPrice;
	}
	
	/**
     * This method returns the nominal price compare to reference price.
     * @return the nominal price compare to reference price.
     */
	public int getMvNomCmToRefPri() {
		return mvNomCmToRefPri;
	}
	
	/**
     * This method sets the nominal price compare to reference price.
     * @param pNomCmToRefPri The nominal price compare to reference price.
     */
	public void setMvNomCmToRefPri(int pNomCmToRefPri) {
		mvNomCmToRefPri = pNomCmToRefPri;
	}
	
	/**
     * This method returns the symbol.
     * @return the symbol.
     */
	public String getMvSymbol() {
		return mvSymbol;
	}
	
	/**
     * This method sets the symbol.
     * @param pvSymbol The symbol.
     */
	public void setMvSymbol(String pvSymbol) {
		mvSymbol = pvSymbol;
	}

	/**
	 * @param mvStockName the mvStockName to set
	 */
	public void setMvStockName(String mvStockName) {
		this.mvStockName = mvStockName;
	}

	/**
	 * @return the mvStockName
	 */
	public String getMvStockName() {
		return mvStockName;
	}

	/**
	 * @param mvSellForeignQty the mvSellForeignQty to set
	 */
	public void setMvSellForeignQty(String mvSellForeignQty) {
		this.mvSellForeignQty = mvSellForeignQty;
	}

	/**
	 * @return the mvSellForeignQty
	 */
	public String getMvSellForeignQty() {
		return mvSellForeignQty;
	}

	/**
	 * @param mvTargetBuyPrc the mvTargetBuyPrc to set
	 */
	public void setMvTargetBuyPrc(String mvTargetBuyPrc) {
		this.mvTargetBuyPrc = mvTargetBuyPrc;
	}

	/**
	 * @return the mvTargetBuyPrc
	 */
	public String getMvTargetBuyPrc() {
		return mvTargetBuyPrc;
	}

	/**
	 * @param mvStopProfitPrice the mvStopProfitPrice to set
	 */
	public void setMvStopProfitPrice(String mvStopProfitPrice) {
		this.mvStopProfitPrice = mvStopProfitPrice;
	}

	/**
	 * @return the mvStopProfitPrice
	 */
	public String getMvStopProfitPrice() {
		return mvStopProfitPrice;
	}

	/**
	 * @param mvStopLostPrice the mvStopLostPrice to set
	 */
	public void setMvStopLostPrice(String mvStopLostPrice) {
		this.mvStopLostPrice = mvStopLostPrice;
	}

	/**
	 * @return the mvStopLostPrice
	 */
	public String getMvStopLostPrice() {
		return mvStopLostPrice;
	}

	public Date getLstUpdate() {
		return lstUpdate;
	}

	public void setLstUpdate(Date lstUpdate) {
		this.lstUpdate = lstUpdate;
	}

	public int getMvMatchPriceCss() {
		return computeCssDefault(getMvMatchPrice(), mvMatchPriceCss);
	}

	public void setMvMatchPriceCss(int mvMatchPriceCss) {
		this.mvMatchPriceCss = mvMatchPriceCss;
	}

	public int getMvMatchQtyCss() {
		return computeCssDefault(getMvMatchQty(), mvMatchQtyCss);
	}

	public void setMvMatchQtyCss(int mvMatchQtyCss) {
		this.mvMatchQtyCss = mvMatchQtyCss;
	}

	public String getAvgPrice() {
		return avgPrice;
	}

	public void setAvgPrice(String avgPrice) {
		this.avgPrice = avgPrice;
	}

	public int getAvgPriceCss() {
		return computeCssDefault(getAvgPrice(), avgPriceCss);
	}

	public void setAvgPriceCss(int avgPriceCss) {
		this.avgPriceCss = avgPriceCss;
	}

	private static String formatIfZero(String value){
		if("0".equals(value) || "0.0".equals(value) ||  "0.000".equals(value)){
			return "-";
		}else {
			return value;
		}
	}
	
	private static int computeCssDefault(String value, int origin){
		if("-".equals(value)){
			// default is refColor
			return 0;
		}else {
			return origin;
		}
	}
	
}
