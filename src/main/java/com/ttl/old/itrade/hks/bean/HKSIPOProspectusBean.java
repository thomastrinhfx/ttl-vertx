package com.ttl.old.itrade.hks.bean;

/**
 * The HKSGenMarginInfoBean class define variables that to save values 
 * for action 
 * @author
 *
 */
public class HKSIPOProspectusBean {
	private String mvEntitlementID;
	private String mvProspectusLink;

	/**
     * This method returns the entitlement id.
     * @return the entitlement id.
     */
	public String getMvEntitlementID() {
		return mvEntitlementID;
	}

	/**
     * This method sets the entitlement id.
     * @param pEntitlementID The entitlement id.
     */
	public void setMvEntitlementID(String pEntitlementID) {
		mvEntitlementID = pEntitlementID;
	}

	/**
     * This method returns the prospectus link.
     * @return the prospectus link.
     */
	public String getMvProspectusLink() {
		return mvProspectusLink;
	}

	/**
     * This method sets the prospectus link.
     * @param pProspectusLink The prospectus link.
     */
	public void setMvProspectusLink(String pProspectusLink) {
		mvProspectusLink = pProspectusLink;
	}

}
