package com.ttl.old.itrade.hks.bean;

/**
 * The HKSConfirmFundTransferBean class define variables that to save values 
 * for action 
 * @author Wind zhao
 *
 */
public class HKSConfirmFundTransferBean {
	private String mvAccountNumber;
	private String mvClientId;
	private String mvDateTime;
	private String mvAmount;
	private String mvAmountDisplay;
	private String mvTransferId;
	private String mvErrorMsg;
	private String mvPasswordConfirmation;
	private String mvOrderId;
	private String mvType;
	private String mvFundTransferTimeout;
	
	/**
     * This method returns the fund transfer if time out.
     * @return the fund transfer if time out.
     */
	public String getMvFundTransferTimeout() {
		return mvFundTransferTimeout;
	}
	/**
     * This method sets the fund transfer if time out.
     * @param pFundTransferTimeout The fund transfer if time out.
     */
	public void setMvFundTransferTimeout(String pFundTransferTimeout) {
		mvFundTransferTimeout = pFundTransferTimeout;
	}
	/**
     * This method returns the fund transfer type.
     * @return the fund transfer type.
     *         [0] W is Withdrawal
     *         [1] D is Deposit
     */
	public String getMvType() {
		return mvType;
	}
	/**
     * This method sets the fund transfer type.
     * @param pType The fund transfer type.
     */
	public void setMvType(String pType) {
		mvType = pType;
	}
	/**
     * This method returns the fund transfer order id.
     * @return the fund transfer order id.
     */
	public String getMvOrderId() {
		return mvOrderId;
	}
	/**
     * This method sets the fund transfer order id.
     * @param pOrderId The fund transfer order id.
     */
	public void setMvOrderId(String pOrderId) {
		mvOrderId = pOrderId;
	}
	/**
     * This method returns the transaction account number.
     * @return the transaction account number.
     */
	public String getMvAccountNumber() {
		return mvAccountNumber;
	}
	/**
     * This method sets the transaction account number.
     * @param pAccountNumber The transaction account number.
     */
	public void setMvAccountNumber(String pAccountNumber) {
		mvAccountNumber = pAccountNumber;
	}
	/**
     * This method returns the trader's client id.
     * @return the trader's client id.
     */
	public String getMvClientId() {
		return mvClientId;
	}
	/**
     * This method sets the trader's client id.
     * @param pClientId The trader's client id.
     */
	public void setMvClientId(String pClientId) {
		mvClientId = pClientId;
	}
	/**
     * This method returns the transaction date.
     * @return the transaction date.
     */
	public String getMvDateTime() {
		return mvDateTime;
	}
	/**
     * This method sets the transaction date.
     * @param pDateTime The transaction date.
     */
	public void setMvDateTime(String pDateTime) {
		mvDateTime = pDateTime;
	}
	/**
     * This method returns the transaction amount.
     * @return the transaction amount.
     */
	public String getMvAmount() {
		return mvAmount;
	}
	/**
     * This method sets the transaction amount.
     * @param pAmount The transaction amount.
     */
	public void setMvAmount(String pAmount) {
		mvAmount = pAmount;
	}
	/**
     * This method returns the transaction amount.
     * @return the transaction amount is formatted.
     */
	public String getMvAmountDisplay() {
		return mvAmountDisplay;
	}
	/**
     * This method sets the transaction amount.
     * @param pAmount The transaction amount is formatted.
     */
	public void setMvAmountDisplay(String pAmountDisplay) {
		mvAmountDisplay = pAmountDisplay;
	}
	/**
     * This method returns the fund transfer id.
     * @return the fund transfer id.
     */
	public String getMvTransferId() {
		return mvTransferId;
	}
	/**
     * This method sets the fund transfer id.
     * @param pTransferId The fund transfer id.
     */
	public void setMvTransferId(String pTransferId) {
		mvTransferId = pTransferId;
	}
	/**
     * This method returns the fund transfer error message.
     * @return the fund transfer error message.
     */
	public String getMvErrorMsg() {
		return mvErrorMsg;
	}
	/**
     * This method sets the fund transfer error message.
     * @param pErrorMsg The fund transfer error message.
     */
	public void setMvErrorMsg(String pErrorMsg) {
		mvErrorMsg = pErrorMsg;
	}
	/**
     * This method returns the fund transfer if confirm password.
     * @return the fund transfer if confirm password.
     *         [0] N
     *         [1] Y
     */
	public String getMvPasswordConfirmation() {
		return mvPasswordConfirmation;
	}
	/**
     * This method sets the fund transfer if confirm password.
     * @param pPasswordConfirmation The fund transfer if confirm password.
     */
	public void setMvPasswordConfirmation(String pPasswordConfirmation) {
		mvPasswordConfirmation = pPasswordConfirmation;
	}
	
}
