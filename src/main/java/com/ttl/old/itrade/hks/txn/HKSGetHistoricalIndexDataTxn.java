package com.ttl.old.itrade.hks.txn;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.tag.ITagXsfTagName;
import com.systekit.winvest.hks.config.mapping.TagName;

public class HKSGetHistoricalIndexDataTxn extends BaseTxn {

	private static String fileName = "C:\\MarketIndexHistorical_%s.txt";
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

	private List<Double> mvIndexArray;
	private List<Double> mvTradeQtyArray;
	private List<Date> mvDateArray;
	
	private Hashtable lvTxnMap;
	

	public List<Double> getMvIndexArray() {
		return mvIndexArray;
	}

	public void setMvIndexArray(List<Double> mvIndexArray) {
		this.mvIndexArray = mvIndexArray;
	}

	public List<Double> getMvTradeQtyArray() {
		return mvTradeQtyArray;
	}

	public void setMvTradeQtyArray(List<Double> mvTradeQtyArray) {
		this.mvTradeQtyArray = mvTradeQtyArray;
	}

	public List<Date> getMvDateArray() {
		return mvDateArray;
	}

	public void setMvDateArray(List<Date> mvDateArray) {
		this.mvDateArray = mvDateArray;
	}

	/**
	 * 
	 * @param exchangeCode
	 * @param fromDate
	 *            in yyyy-mm-dd format
	 * @param toDate
	 *            in yyyy-mm-dd format
	 * @return
	 */
	public void queryIndexHistoricalData(int exchangeCode, String fromDate,
			String toDate) {

		Connection conn = null;
		CallableStatement callableStatement = null;

		try {
			Class.forName("net.sourceforge.jtds.jdbc.Driver");
			conn = DriverManager.getConnection(IMain
					.getProperty("chartFeeder.db.conn"), IMain
					.getProperty("chartFeeder.db.uid"), IMain
					.getProperty("chartFeeder.db.pwd"));
			// 1, 'STB', '2010-08-01', '2010-08-27'

			callableStatement = conn
					.prepareCall("{ call SP_ITRADE_GET_DataIndexChart(?, ?, ?) }");
			callableStatement.setInt(1, exchangeCode);
			callableStatement.setDate(2, Date.valueOf(fromDate));
			callableStatement.setDate(3, Date.valueOf(toDate));

			callableStatement.execute();
			ResultSet rs = callableStatement.getResultSet();

			mvDateArray = new ArrayList<Date>();
			mvIndexArray = new ArrayList<Double>();
			mvTradeQtyArray = new ArrayList<Double>();
			BigDecimal milionDecimal = new BigDecimal(1000000);
			while (rs.next()) {
				mvDateArray.add(rs.getDate(1));
				mvIndexArray.add(rs.getBigDecimal(2).setScale(2).doubleValue());
				mvTradeQtyArray.add(rs.getBigDecimal(3).divide(milionDecimal)
						.setScale(2,RoundingMode.DOWN).doubleValue());
			}
			
			//writeHistoricalData2File(exchangeCode);

		} catch (SQLException e) {
			Log.print(e, Log.ERROR_LOG);
		} catch (Exception e) {
			Log.print(e, Log.ERROR_LOG);
		} finally {
			if (conn != null) {
				try {
					// close connection
					if (!conn.isClosed()) {
						callableStatement.close();
						conn.close();
					}
				} catch (SQLException e) {
					Log.print(e, Log.ERROR_LOG);
				}
			}
		}
	}
	
	
	public void queryIndexHistoricalDataFromFO(String pMarketID, String pFromDate,
			String pToDate) {
		
		lvTxnMap = new Hashtable();
	    lvTxnMap.put("MARKETID", pMarketID);
	    lvTxnMap.put("FROMDATE", pFromDate);
	    lvTxnMap.put("TODATE", pToDate);   	  
	    
    	if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSMarketIndexHistoryDataRequest, lvTxnMap))
    	{
            IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList(ITagXsfTagName.CHILD_ROW);
                        
            mvDateArray = new ArrayList<Date>();
    		mvIndexArray = new ArrayList<Double>();
    		mvTradeQtyArray = new ArrayList<Double>();
    		
        	BigDecimal milionDecimal = new BigDecimal(1000000);
        	
        	SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
        	
            for(int i = 0; i < lvRowList.size() ; i++)
            { 
            	IMsgXMLNode lvRow = lvRowList.getNode(i);           	
				try {					
					mvDateArray.add(new Date(formatDate.parse(lvRow.getChildNode("HISTORYDATE").getValue()).getTime()));				
					mvIndexArray.add(Double.parseDouble(lvRow.getChildNode(TagName.MARKETINDEX).getValue()));			
					mvTradeQtyArray.add((new BigDecimal(lvRow.getChildNode("MARKETTOTALQTY").getValue()).divide(milionDecimal)
							.setScale(2,RoundingMode.DOWN).doubleValue()));
					
				} catch (Exception e) {
					Log.println(e, Log.ERROR_LOG);
				}
            }
    	}		
	}
	
	
	public void readDataFromFile (int marketID){
		InputStream fstream = null;
		DataInputStream in = null;
		BufferedReader br = null;
		try{
			String fullName ="";
			switch (marketID) {
			case 1:
				fullName = String.format(fileName, "HO");
				break;
			case 2:
				fullName = String.format(fileName, "HA");
				break;
			case 3:
				fullName = String.format(fileName, "UPCOM");
				break;
			default:
				fullName = String.format(fileName, "HO");
				break;
			}
			
			File f = new File(fullName);
		    if (!f.exists()) {
		    	f.createNewFile();
		    }
		    
			fstream = new FileInputStream(fullName);
			
			// Get the object of DataInputStream
			in = new DataInputStream(fstream);
			br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			// Read File Line By Line
			String []tmp;
			mvDateArray = new ArrayList<Date>();
			mvIndexArray = new ArrayList<Double>();
			mvTradeQtyArray = new ArrayList<Double>();
			while ((strLine = br.readLine()) != null) {
				//System.out.println("strLine:"+strLine);
				//Log.println(strLine, Log.ERROR_LOG);
				tmp = strLine.split("\\|");
				mvDateArray.add( new Date(sdf.parse(tmp[0]).getTime()));
				mvIndexArray.add(Double.valueOf(tmp[1]));
				mvTradeQtyArray.add(Double.valueOf(tmp[2]));
			}
		} catch (IOException e) {
			Log.println(e, Log.ERROR_LOG);
		}catch (Exception e) {
			Log.println(e, Log.ERROR_LOG);
		}
	}
	
	public void writeHistoricalData2File(int marketID) {
		String fullName ="";
		switch (marketID) {
		case 1:
			fullName = String.format(fileName, "HO");
			break;
		case 2:
			fullName = String.format(fileName, "HA");
			break;
		case 3:
			fullName = String.format(fileName, "UPCOM");
			break;
		default:
			fullName = String.format(fileName, "HO");
			break;
		}
		File f = new File(fullName);
		try {
			FileWriter fstream = null;
			BufferedWriter out = null;

			if (!f.exists()) {
				f.createNewFile();
			}

			StringBuffer temp = new StringBuffer();
			for (int index = 0; index < mvDateArray.size(); index++) {
				temp.append(sdf.format(mvDateArray.get(index))).append("|").append(
						mvIndexArray.get(index)).append("|").append(
						mvTradeQtyArray.get(index)).append(
						System.getProperty("line.separator"));
			}
			fstream = new FileWriter(fullName, false);
			out = new BufferedWriter(fstream);
			out.write(temp.toString());
			out.flush();
		} catch (IOException e) {
			Log.println(e, Log.ERROR_LOG);
		}
	}
}
