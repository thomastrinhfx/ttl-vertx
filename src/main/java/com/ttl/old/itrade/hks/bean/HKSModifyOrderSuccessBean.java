package com.ttl.old.itrade.hks.bean;
/**
 * The HKSModifyOrderSuccessBean class define variables that to save values 
 * for action 
 * @author Wind Zhao
 *
 */
public class HKSModifyOrderSuccessBean {
	private String mvErrorMessageBox;
	private String mvTextBoxClose;
	private String mvReasonMessage;
	private String mvBuyOrderTitleColor;
	private String mvBuyOrderBackgroundColor;
	private String mvSellOrderTitleColor;
	private String mvSellOrderBackgroundColor;
	private String mvBS;
	private String mvMarketID;
	private String mvBuyOrSell;
	private String mvOrderID;
	private String mvInstrumentID;
	private String mvInstrumentName;
	private String mvPrice;
	private String mvQuantity;
	private String mvOrderType;
	private String mvGoodtillDate;
	private String mvGoodTillDateDisable;
	private String mvNetAmt;
	private String mvQuantityDescription;
	private String mvOrdertypeDescription;
	private String mvGoodtillDescription;
	private String mvCurrencyID;
	private String mvMultiMarketDisable;
	//Begin Task #: TTL-GZ-ZZW-00011 Wind Zhao 20091118 for [ITradeR5 VN]Allow to modifycancel orders like ROSE's version
    
	/*
     * mvModifyFor to control hidden or show some fields in 
     * modify order
     */
    private String mvModifyOrderFor;
    
    /**
     * This method returns the modify order value
     * to control show or hidden some fields.
     * @return the modify order for value.
     */
    
    public String getMvModifyOrderFor() {
    	return mvModifyOrderFor;
    }
    
    /**
     * This method sets the modify order for
     * * to control show or hidden some fields.
     * @param pModifyOrderFor The modify order for value.
     */
    public void setMvModifyOrderFor(String pModifyOrderFor) {
    	mvModifyOrderFor = pModifyOrderFor;
    }
    
    //End Task #: TTL-GZ-ZZW-00011 Wind Zhao 20091118 for [ITradeR5 VN]Allow to modifycancel orders like ROSE's version
    
    /**
     * This method returns the error message box.
     * @return the error message box.
     */
	public String getMvErrorMessageBox() {
		return mvErrorMessageBox;
	}
	
	/**
     * This method sets the error message box
     * @param pErrorMessageBox The error message box.
     */
	public void setMvErrorMessageBox(String pErrorMessageBox) {
		mvErrorMessageBox = pErrorMessageBox;
	}
	
	/**
     * This method returns the text box close.
     * @return the text box close.
     */
	public String getMvTextBoxClose() {
		return mvTextBoxClose;
	}
	
	/**
     * This method sets the text box close.
     * @param pTextBoxClose The text box close.
     */
	public void setMvTextBoxClose(String pTextBoxClose) {
		mvTextBoxClose = pTextBoxClose;
	}
	
	/**
     * This method returns the reason message.
     * @return the reason message.
     */
	public String getMvReasonMessage() {
		return mvReasonMessage;
	}
	
	/**
     * This method sets the reason message.
     * @param pReasonMessage The reason message.
     */
	public void setMvReasonMessage(String pReasonMessage) {
		mvReasonMessage = pReasonMessage;
	}
	
	/**
     * This method returns the buy order title color.
     * @return the buy order title color.
     */
	public String getMvBuyOrderTitleColor() {
		return mvBuyOrderTitleColor;
	}
	
	/**
     * This method sets the buy order title color.
     * @param pBuyOrderTitleColor The buy order title color.
     */
	public void setMvBuyOrderTitleColor(String pBuyOrderTitleColor) {
		mvBuyOrderTitleColor = pBuyOrderTitleColor;
	}
	
	/**
     * This method returns the buy order back ground color.
     * @return the buy order back ground color.
     */
	public String getMvBuyOrderBackgroundColor() {
		return mvBuyOrderBackgroundColor;
	}
	
	/**
     * This method sets the buy order back ground color.
     * @param pBuyOrderBackgroundColor The buy order back ground color.
     */
	public void setMvBuyOrderBackgroundColor(String pBuyOrderBackgroundColor) {
		mvBuyOrderBackgroundColor = pBuyOrderBackgroundColor;
	}
	
	/**
     * This method returns the sell order title color.
     * @return the sell order title color.
     */
	public String getMvSellOrderTitleColor() {
		return mvSellOrderTitleColor;
	}
	
	/**
     * This method sets the sell order title color.
     * @param pSellOrderTitleColor The sell order title color.
     */
	public void setMvSellOrderTitleColor(String pSellOrderTitleColor) {
		mvSellOrderTitleColor = pSellOrderTitleColor;
	}
	
	/**
     * This method returns the sell order back ground color.
     * @return the sell order back ground color.
     */
	public String getMvSellOrderBackgroundColor() {
		return mvSellOrderBackgroundColor;
	}
	
	/**
     * This method sets the sell order back ground color.
     * @param pSellOrderBackgroundColor The sell order back ground color.
     */
	public void setMvSellOrderBackgroundColor(String pSellOrderBackgroundColor) {
		mvSellOrderBackgroundColor = pSellOrderBackgroundColor;
	}
	
	/**
     * This method returns the buy sell.
     * @return the buy sell.
     */
	public String getMvBS() {
		return mvBS;
	}
	
	/**
     * This method sets the buy sell.
     * @param pBS The buy sell.
     */
	public void setMvBS(String pBS) {
		mvBS = pBS;
	}
	
	/**
     * This method returns the market id.
     * @return the market id.
     */
	public String getMvMarketID() {
		return mvMarketID;
	}
	
	/**
     * This method sets the market id.
     * @param pMarketID The market id.
     */
	public void setMvMarketID(String pMarketID) {
		mvMarketID = pMarketID;
	}
	
	/**
     * This method returns the buy or sell.
     * @return the buy or sell.
     */
	public String getMvBuyOrSell() {
		return mvBuyOrSell;
	}
	
	/**
     * This method sets the buy or sell.
     * @param pBuyOrSell The buy or sell.
     */
	public void setMvBuyOrSell(String pBuyOrSell) {
		mvBuyOrSell = pBuyOrSell;
	}
	
	/**
     * This method returns the order id.
     * @return the order id.
     */
	public String getMvOrderID() {
		return mvOrderID;
	}
	
	/**
     * This method sets the order id.
     * @param pOrderID The order id.
     */
	public void setMvOrderID(String pOrderID) {
		mvOrderID = pOrderID;
	}
	
	/**
     * This method returns the instrument id.
     * @return the instrument id.
     */
	public String getMvInstrumentID() {
		return mvInstrumentID;
	}
	
	/**
     * This method sets the instrument id.
     * @param pInstrumentID The instrument id.
     */
	public void setMvInstrumentID(String pInstrumentID) {
		mvInstrumentID = pInstrumentID;
	}
	
	/**
     * This method returns the instrument name.
     * @return the instrument name.
     */
	public String getMvInstrumentName() {
		return mvInstrumentName;
	}
	
	/**
     * This method sets the instrument name.
     * @param pInstrumentName The instrument name.
     */
	public void setMvInstrumentName(String pInstrumentName) {
		mvInstrumentName = pInstrumentName;
	}
	
	/**
     * This method returns the price.
     * @return the price is formatted.
     */
	public String getMvPrice() {
		return mvPrice;
	}
	
	/**
     * This method sets the price.
     * @param pPrice The price is formatted.
     */
	public void setMvPrice(String pPrice) {
		mvPrice = pPrice;
	}
	
	/**
     * This method returns the quantity.
     * @return the quantity is formatted.
     */
	public String getMvQuantity() {
		return mvQuantity;
	}
	
	/**
     * This method sets the quantity.
     * @param pQuantity The quantity is formatted.
     */
	public void setMvQuantity(String pQuantity) {
		mvQuantity = pQuantity;
	}
	
	/**
     * This method returns the order type.
     * @return the order type.
     */
	public String getMvOrderType() {
		return mvOrderType;
	}
	
	/**
     * This method sets the order type.
     * @param pOrderType The order type.
     */
	public void setMvOrderType(String pOrderType) {
		mvOrderType = pOrderType;
	}
	
	/**
     * This method returns the good till date.
     * @return the good till date is formatted.
     */
	public String getMvGoodtillDate() {
		return mvGoodtillDate;
	}
	
	/**
     * This method sets the good till date.
     * @param pGoodtillDate The good till date is formatted.
     */
	public void setMvGoodtillDate(String pGoodtillDate) {
		mvGoodtillDate = pGoodtillDate;
	}
	
	/**
     * This method returns the good till date disable.
     * @return the good till date disable.
     */
	public String getMvGoodTillDateDisable() {
		return mvGoodTillDateDisable;
	}
	
	/**
     * This method sets the good till date disable.
     * @param pGoodTillDateDisable The good till date disable.
     */
	public void setMvGoodTillDateDisable(String pGoodTillDateDisable) {
		mvGoodTillDateDisable = pGoodTillDateDisable;
	}
	
	/**
     * This method returns the net amount.
     * @return the net amount.
     */
	public String getMvNetAmt() {
		return mvNetAmt;
	}
	
	/**
     * This method sets the net amount.
     * @param pNetAmt The net amount.
     */
	public void setMvNetAmt(String pNetAmt) {
		mvNetAmt = pNetAmt;
	}
	
	/**
     * This method returns the quantity description.
     * @return the quantity description.
     */
	public String getMvQuantityDescription() {
		return mvQuantityDescription;
	}
	
	/**
     * This method sets the quantity description.
     * @param pQuantityDescription The quantity description.
     */
	public void setMvQuantityDescription(String pQuantityDescription) {
		mvQuantityDescription = pQuantityDescription;
	}
	
	/**
     * This method returns the order type description.
     * @return the order type description.
     */
	public String getMvOrdertypeDescription() {
		return mvOrdertypeDescription;
	}
	
	/**
     * This method sets the order type description.
     * @param pOrderTypeDescription The order type description.
     */
	public void setMvOrdertypeDescription(String pOrderTypeDescription) {
		mvOrdertypeDescription = pOrderTypeDescription;
	}
	
	/**
     * This method returns the good till date description.
     * @return the good till date description.
     */
	public String getMvGoodtillDescription() {
		return mvGoodtillDescription;
	}
	
	/**
     * This method sets the good till date description.
     * @param pGoodtillDescription The good till date description.
     */
	public void setMvGoodtillDescription(String pGoodtillDescription) {
		mvGoodtillDescription = pGoodtillDescription;
	}
	
	/**
     * This method returns the currency id.
     * @return the currency id.
     */
	public String getMvCurrencyID() {
		return mvCurrencyID;
	}
	
	/**
     * This method sets the currency id.
     * @param pCurrencyID The currency id.
     */
	public void setMvCurrencyID(String pCurrencyID) {
		mvCurrencyID = pCurrencyID;
	}
	
	/**
     * This method returns the multi market disable.
     * @return the multi market disable.
     */
	public String getMvMultiMarketDisable() {
		return mvMultiMarketDisable;
	}
	
	/**
     * This method sets the multi market disable.
     * @param pMultiMarketDisable The multi market disable.
     */
	public void setMvMultiMarketDisable(String pMultiMarketDisable) {
		mvMultiMarketDisable = pMultiMarketDisable;
	}
	
}