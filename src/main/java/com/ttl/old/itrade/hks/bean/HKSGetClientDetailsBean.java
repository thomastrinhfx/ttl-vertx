package com.ttl.old.itrade.hks.bean;

/**
 * The HKSGetClientDetailsBean class define variables that to save values 
 * for action 
 * @author 
 *
 */
public class HKSGetClientDetailsBean {
	private String pClientID;
	private String pClientFullName;
	private String pIDNumber;
	
	private String pFirstTimeChangePassword;

	private String pErrorMsg;
	private String pData;
	
	 /**
     * This method returns the information of change password or not form action.
     * @return the information of change password or not.
     * @type String.
     */
	public String getPData() {
		return pData;
	}
	
	/**
     * This method sets the information of change password or not form action.
     * @param pData The information of change password or not.
     */
	public void setPData(String pData) {
		this.pData = pData;
	}
	
	/**
     * This method returns the client id.
     * @return the client id.
     */
	public String getPClientID() {
		return pClientID;
	}
	
	/**
     * This method sets client id.
     * @param pClientID The client id.
     */
	public void setPClientID(String pClientID) {
		this.pClientID = pClientID;
	}
	
	/**
     * This method returns the client name.
     * @return the client name.
     */
	public String getPClientName() {
		return pClientFullName;
	}
	
	/**
     * This method sets client name.
     * @param pClientName The client name.
     */
	public void setPClientName(String pClientName) {
		this.pClientFullName = pClientName;
	}
	
	
	/**
     * This method returns the if first time change password.
     * @return the if first time change password.
     */
	public String getPFirstTimeChangePassword() {
		return pFirstTimeChangePassword;
	}
	
	/**
     * This method sets the if first time change password.
     * @param pFirstTimeChangePassword The if first time change password.
     */
	public void setPFirstTimeChangePassword(String pFirstTimeChangePassword) {
		this.pFirstTimeChangePassword = pFirstTimeChangePassword;
	}
	
	
	/**
     * This method returns the error message when change password.
     * @return the error message.
     */
	public String getPErrorMsg() {
		return pErrorMsg;
	}
	
	/**
     * This method sets the error message when change password
     * @param pErrorMsg The error message.
     */
	public void setPErrorMsg(String pErrorMsg) {
		this.pErrorMsg = pErrorMsg;
	}

	public void setPIDNumber(String pIDNumber) {
		this.pIDNumber = pIDNumber;
	}

	public String getPIDNumber() {
		return pIDNumber;
	}
	
}