package com.ttl.old.itrade.hks.txn;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.tag.ITagXsfTagName;
import com.systekit.winvest.hks.config.mapping.TagName;
/**
 * This class processes the research reports enquiry
 * @author not attributable
 *
 */
public class HKSResearchReportsEnquiryTxn extends BaseTxn
{
	/**
	 * This method process the research reports enquiry
	 * @param pClientID the clietn id
	 * @return research reports enquiry result set
	 */
   public Vector process(String pClientID)
   {
      Hashtable lvTxnMap = new Hashtable();
      lvTxnMap.put(TagName.CLIENTID, pClientID);

      Vector lvReturn = new Vector();
      if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSResearchReportsEnquiry, lvTxnMap))
      {
         IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList(ITagXsfTagName.ROW);
         for (int i = 0; i < lvRowList.size(); i++)
         {
            IMsgXMLNode lvNode = lvRowList.getNode(i);
            HashMap lvModel = new HashMap();
            lvModel.put(TagName.MESSAGEID, lvNode.getChildNode(TagName.MESSAGEID).getValue());
            lvModel.put("PLACEDATE", new Timestamp(Long.parseLong(lvNode.getChildNode("PLACEDATE").getValue())));
            lvModel.put(TagName.MESSAGE, lvNode.getChildNode(TagName.MESSAGE).getValue());
            lvReturn.add(lvModel);
         }
      }
      return lvReturn;
   }
}
