package com.ttl.old.itrade.hks.bean;

/**
 * The HKSGenMarginPlanBean class define variables that to save values 
 * for action 
 * @author
 *
 */
public class HKSGenMarginPlanBean {
	private String mvEntitlementId;
	private String mvNumOfPlan;
	private String mvApplyShare;
	private String mvFlatfee;
	private String mvFinanceFee;
	private String mvDepositamt;
	private String mvLoanAmount;
	private String mvLoanRate;
	private String mvDepositRate;
	private String mvNumofRate;
	private String mvLoanAmountLessThan;
	private String mvInterestRate;
	private String mvLendingPercentage;
	
	/**
     * This method returns the entitlement id.
     * @return the entitlement id.
     */
	public String getMvEntitlementId() {
		return mvEntitlementId;
	}
	
	/**
     * This method sets the entitlement id.
     * @param pEntitlementId The entitlement id.
     */
	public void setMvEntitlementId(String pEntitlementId) {
		mvEntitlementId = pEntitlementId;
	}
	
	/**
     * This method returns the number of plan.
     * @return the number of plan.
     */
	public String getMvNumOfPlan() {
		return mvNumOfPlan;
	}
	
	/**
     * This method sets the number of plan.
     * @param pNumOfPlan The number of plan.
     */
	public void setMvNumOfPlan(String pNumOfPlan) {
		mvNumOfPlan = pNumOfPlan;
	}
	
	/**
     * This method returns the apply share.
     * @return the apply share.
     */
	public String getMvApplyShare() {
		return mvApplyShare;
	}
	
	/**
     * This method sets the apply share.
     * @param pApplyShare The apply share.
     */
	public void setMvApplyShare(String pApplyShare) {
		mvApplyShare = pApplyShare;
	}
	
	/**
     * This method returns the flat fee.
     * @return the flat fee.
     */
	public String getMvFlatfee() {
		return mvFlatfee;
	}
	
	/**
     * This method sets the flat fee.
     * @param pFlatfee The flat fee.
     */
	public void setMvFlatfee(String pFlatfee) {
		mvFlatfee = pFlatfee;
	}
	
	/**
     * This method returns the finance fee.
     * @return the finance fee.
     */
	public String getMvFinanceFee() {
		return mvFinanceFee;
	}
	
	/**
     * This method sets the finance fee.
     * @param pFinanceFee The finance fee.
     */
	public void setMvFinanceFee(String pFinanceFee) {
		mvFinanceFee = pFinanceFee;
	}
	
	/**
     * This method returns the deposit amount.
     * @return the deposit amount.
     */
	public String getMvDepositamt() {
		return mvDepositamt;
	}
	
	/**
     * This method sets the deposit amount.
     * @param pDepositamt The deposit amount.
     */
	public void setMvDepositamt(String pDepositamt) {
		mvDepositamt = pDepositamt;
	}
	
	/**
     * This method returns the loan amount.
     * @return the loan amount.
     */
	public String getMvLoanAmount() {
		return mvLoanAmount;
	}
	
	/**
     * This method sets the loan amount.
     * @param pLoanAmount The loan amount.
     */
	public void setMvLoanAmount(String pLoanAmount) {
		mvLoanAmount = pLoanAmount;
	}
	
	/**
     * This method returns the loan rate.
     * @return the loan rate.
     */
	public String getMvLoanRate() {
		return mvLoanRate;
	}
	
	/**
     * This method sets the loan rate.
     * @param pLoanRate The loan rate.
     */
	public void setMvLoanRate(String pLoanRate) {
		mvLoanRate = pLoanRate;
	}
	
	/**
     * This method returns the deposit rate.
     * @return the deposit rate.
     */
	public String getMvDepositRate() {
		return mvDepositRate;
	}
	
	/**
     * This method sets the deposit rate.
     * @param pDepositRate The deposit rate.
     */
	public void setMvDepositRate(String pDepositRate) {
		mvDepositRate = pDepositRate;
	}
	
	/**
     * This method returns the number of rate.
     * @return the number of rate.
     */
	public String getMvNumofRate() {
		return mvNumofRate;
	}
	
	/**
     * This method sets the number of rate.
     * @param pNumofRate The number of rate.
     */
	public void setMvNumofRate(String pNumofRate) {
		mvNumofRate = pNumofRate;
	}
	
	/**
     * This method returns the loan amount less than.
     * @return the loan amount less than.
     */
	public String getMvLoanAmountLessThan() {
		return mvLoanAmountLessThan;
	}
	
	/**
     * This method sets the loan amount less than.
     * @param pLoanAmountLessThan The loan amount less than.
     */
	public void setMvLoanAmountLessThan(String pLoanAmountLessThan) {
		mvLoanAmountLessThan = pLoanAmountLessThan;
	}
	
	/**
     * This method returns the interest rate.
     * @return the interest rate.
     */
	public String getMvInterestRate() {
		return mvInterestRate;
	}
	
	/**
     * This method sets the interest rate.
     * @param pInterestRate The interest rate.
     */
	public void setMvInterestRate(String pInterestRate) {
		mvInterestRate = pInterestRate;
	}
	
	/**
     * This method returns the lending percentage.
     * @return the lending percentage.
     */
	public String getMvLendingPercentage() {
		return mvLendingPercentage;
	}
	
	/**
     * This method sets the lending percentage.
     * @param pLendingPercentage The lending percentage.
     */
	public void setMvLendingPercentage(String pLendingPercentage) {
		mvLendingPercentage = pLendingPercentage;
	}
}