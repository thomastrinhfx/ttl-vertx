package com.ttl.old.itrade.hks.txn;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.tag.ITagXsfTagName;

/**
 * The HKSIPOFinancingFixedChargeTxn class definition for all method
 * Host of new shares to obtain loans from the fixed charge
 * @author not attributable
 *
 */
public class HKSIPOFinancingFixedChargeTxn extends BaseTxn {
	/**
	 * This variable is used to the tp Error
	 */
	private TPErrorHandling tpError;
	
	private int mvReturnCode;
    private String mvErrorCode;
    private String mvErrorMessage;
    
    private String mvEntitlementID;
    private Vector mvResultApplyShare = new Vector();
    private Vector mvResultIPORate = new Vector();
	private Vector mvResultFlatFee = new Vector();
	private Vector mvResultDepositAmt = new Vector();
    /**
     * Default constructor for HKSIPOFinancingFixedChargeTxn class
     */
	public HKSIPOFinancingFixedChargeTxn(){
		tpError = new TPErrorHandling();
	}
	/**
	 * Default constructor for HKSIPOFinancingFixedChargeTxn class
	 * @param pEntitlementID the entitlement id
	 */
	public HKSIPOFinancingFixedChargeTxn(String pEntitlementID){
		tpError = new TPErrorHandling();
		setEntitlementID(pEntitlementID);
	} 
	/**
	 * This method process Host of new shares to obtain loans from the fixed charge
	 * @param pEntitlementID
	 * @return IPO loans to fixed-fee collection
	 */
	public Vector process(String pEntitlementID){
		Vector lvReturn = new Vector();
		try{
			Log.println("HKSMarginFixedChargeTxn starts", Log.ACCESS_LOG);
			
			Hashtable lvTxnMap = new Hashtable();
			
			lvTxnMap.put("ENTITLEMENTID", pEntitlementID);
			
			if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSMarginFixedChargeRequest, lvTxnMap)){
				IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList(ITagXsfTagName.ROW);
				
				for(int i=0; i< lvRowList.size(); i++){
					
					IMsgXMLNode lvNode = lvRowList.getNode(i);
					HashMap lvModel = new HashMap();
					lvModel.put("APPLYSHARE", lvNode.getChildNode("APPLYSHARE").getValue());
					lvModel.put("FLATFEE", lvNode.getChildNode("FLATFEE").getValue());
					lvModel.put("FINANCEFEE", lvNode.getChildNode("FINANCEFEE").getValue());
					lvModel.put("DEPOSITAMT",lvNode.getChildNode("DEPOSITAMT").getValue());
					lvModel.put("LOANAMT",lvNode.getChildNode("LOANAMT").getValue());
					lvModel.put("LOANRATE",lvNode.getChildNode("LOANRATE").getValue());
					lvModel.put("DEPOSITRATE",lvNode.getChildNode("DEPOSITRATE").getValue());
					lvReturn.add(lvModel);
				}
			}
		}catch(Exception e)
		{	
			Log.println("HKSMarginFixedChargeTxn error" + e.toString(), Log.ERROR_LOG);
		}
		return lvReturn;
	}
	/**
	 * Get method for entitlement id
	 * @return  entitlement id
	 */
	public String getEntitlementID(){
		return mvEntitlementID;
	}
	/**
	 * Set method for entitlement id
	 * @param pEntitlementID the entitlement id
	 */
	public void setEntitlementID(String pEntitlementID){
		this.mvEntitlementID=pEntitlementID;
	}
	/**
	 * Set method for return system code 
	 * @param pReturnCode the return system code 
	 */
    public void setReturnCode(int pReturnCode)
    {
        mvReturnCode = pReturnCode;
    }
    /**
     * Get method for system error message
     * @param pErrorCode the system error message
     */
    public void setErrorCode(String pErrorCode)
    {
        mvErrorCode = pErrorCode;
    }
    /**
     * Set method for system error message
     * @param pErrorMessage the system error message
     */
    public void setErrorMessage(String pErrorMessage)
    {
        mvErrorMessage = pErrorMessage;
    }
}
