package com.ttl.old.itrade.hks.bean;

import java.util.Calendar;

import org.apache.commons.validator.GenericValidator;

public class HKSMobileOrderInfo {
	private String stockCode;
	private int quantity;
	private String price;
	private Integer orderSide;
	private int currentStatus;
	private String orderTypeValue;
	private String stockName;
	private String orderSideName;
	private String marketID;
	private String marketName;
	private int orderSource;
	private Calendar orderTime;
	private String orderValue;
	private String orderFee;
	
	private String displayBS;
	private String orderTypeDisp;
	private String orderTimeDisp;
	private String quantityDisp;
	private String filledQuantity;
	private String status;
	private String cancelIcon;
	
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public Integer getOrderSide() {
		return orderSide;
	}
	public void setOrderSide(Integer orderSide) {
		this.orderSide = orderSide;
	}
	public int getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(int currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getStockName() {
		return stockName;
	}
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	public String getOrderSideName() {
		return orderSideName;
	}
	public void setOrderSideName(String orderSideName) {
		this.orderSideName = orderSideName;
	}
	public String getMarketID() {
		return marketID;
	}
	public void setMarketID(String marketID) {
		this.marketID = marketID;
	}
	public String getMarketName() {
		return marketName;
	}
	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}
	public int getOrderSource() {
		return orderSource;
	}
	public void setOrderSource(int orderSource) {
		this.orderSource = orderSource;
	}
	public Calendar getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(Calendar orderTime) {
		this.orderTime = orderTime;
	}
	public String getOrderValue() {
		/*if (GenericValidator.isDouble(price)) {
			orderValue = (double)(Double.parseDouble(price) * quantity);
		}*/
		return orderValue;
	}
	public String getOrderTypeValue() {
		return orderTypeValue;
	}
	public void setOrderTypeValue(String orderTypeValue) {
		this.orderTypeValue = orderTypeValue;
	}
	public String getDisplayBS() {
		return displayBS;
	}
	public void setDisplayBS(String displayBS) {
		this.displayBS = displayBS;
	}
	public String getOrderTypeDisp() {
		return orderTypeDisp;
	}
	public void setOrderTypeDisp(String orderTypeDisp) {
		this.orderTypeDisp = orderTypeDisp;
	}
	public String getOrderTimeDisp() {
		return orderTimeDisp;
	}
	public void setOrderTimeDisp(String orderTimeDisp) {
		this.orderTimeDisp = orderTimeDisp;
	}
	public void setOrderValue(String orderValue) {
		this.orderValue = orderValue;
	}
	public String getQuantityDisp() {
		return quantityDisp;
	}
	public void setQuantityDisp(String quantityDisp) {
		this.quantityDisp = quantityDisp;
	}
	public String getFilledQuantity() {
		return filledQuantity;
	}
	public void setFilledQuantity(String filledQuantity) {
		this.filledQuantity = filledQuantity;
	}
	public String getOrderFee() {
		return orderFee;
	}
	public void setOrderFee(String orderFee) {
		this.orderFee = orderFee;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCancelIcon() {
		return cancelIcon;
	}
	public void setCancelIcon(String cancelIcon) {
		this.cancelIcon = cancelIcon;
	}
	
}
