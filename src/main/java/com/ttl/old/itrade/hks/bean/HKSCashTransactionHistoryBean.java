package com.ttl.old.itrade.hks.bean;

/**
 * The HKSTransactionHistoryBean class define variables that to save values for
 * action
 * 
 * @author
 * 
 */
public class HKSCashTransactionHistoryBean {
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the totalLendingAmt
	 */
	public String getTotalLendingAmt() {
		return totalLendingAmt;
	}

	/**
	 * @param totalLendingAmt
	 *            the totalLendingAmt to set
	 */
	public void setTotalLendingAmt(String totalLendingAmt) {
		this.totalLendingAmt = totalLendingAmt;
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action
	 *            the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	private String status;
	private String totalLendingAmt;
	private String action;
	private String creationTime;
	private String interestAccured;
	private String lastApprovaltime;
	private String bankid;
	private String fee;
	private String feePercent;
	private String transType;
	private String transferChannel;
	private String remark;
	private String waiveAllFlag;
	private String createTime;

	// Fund Transfer Detail
	private String tranID;
	
	private String trandate;
	private String receiveClientID;
	private String receiveBankID;
	private String receiveBankBranch;
	private String receiveBankAcID;
	
	private String ownerName;
	private String bankName;
	private String bankBranch;
	private String inputChannel;
	/**
	 * @return the tranID
	 */
	public String getTranID() {
		return tranID;
	}

	/**
	 * @param tranID the tranID to set
	 */
	public void setTranID(String tranID) {
		this.tranID = tranID;
	}

	/**
	 * @return the trandate
	 */
	public String getTrandate() {
		return trandate;
	}

	/**
	 * @param trandate the trandate to set
	 */
	public void setTrandate(String trandate) {
		this.trandate = trandate;
	}

	/**
	 * @return the receiveClientID
	 */
	public String getReceiveClientID() {
		return receiveClientID;
	}

	/**
	 * @param receiveClientID the receiveClientID to set
	 */
	public void setReceiveClientID(String receiveClientID) {
		this.receiveClientID = receiveClientID;
	}

	/**
	 * @return the receiveBankID
	 */
	public String getReceiveBankID() {
		return receiveBankID;
	}

	/**
	 * @param receiveBankID the receiveBankID to set
	 */
	public void setReceiveBankID(String receiveBankID) {
		this.receiveBankID = receiveBankID;
	}

	/**
	 * @return the receiveBankAcID
	 */
	public String getReceiveBankAcID() {
		return receiveBankAcID;
	}

	public String getReceiveBankBranch() {
		return receiveBankBranch;
	}

	public void setReceiveBankBranch(String receiveBankBranch) {
		this.receiveBankBranch = receiveBankBranch;
	}

	/**
	 * @param receiveBankAcID the receiveBankAcID to set
	 */
	public void setReceiveBankAcID(String receiveBankAcID) {
		this.receiveBankAcID = receiveBankAcID;
	}

	// STATUS,CREATIONTIME,TOTALLENDINGAMT,INTERESTACCURED,LASTAPPROVALTIME
	/**
	 * @return the creationTime
	 */
	public String getCreationTime() {
		return creationTime;
	}

	/**
	 * @param creationTime
	 *            the creationTime to set
	 */
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	/**
	 * @return the interestAccured
	 */
	public String getInterestAccured() {
		return interestAccured;
	}

	/**
	 * @param interestAccured
	 *            the interestAccured to set
	 */
	public void setInterestAccured(String interestAccured) {
		this.interestAccured = interestAccured;
	}

	/**
	 * @return the lastApprovaltime
	 */
	public String getLastApprovaltime() {
		return lastApprovaltime;
	}

	/**
	 * @param lastApprovaltime
	 *            the lastApprovaltime to set
	 */
	public void setLastApprovaltime(String lastApprovaltime) {
		this.lastApprovaltime = lastApprovaltime;
	}

	public void setBankid(String bankid) {
		this.bankid = bankid;
	}

	public String getBankid() {
		return bankid;
	}

	/**
	 * @param fee the fee to set
	 */
	public void setFee(String fee) {
		this.fee = fee;
	}

	/**
	 * @return the fee
	 */
	public String getFee() {
		return fee;
	}

	/**
	 * @param feePercent the feePercent to set
	 */
	public void setFeePercent(String feePercent) {
		this.feePercent = feePercent;
	}

	/**
	 * @return the feePercent
	 */
	public String getFeePercent() {
		return feePercent;
	}

	/**
	 * @param transType the transType to set
	 */
	public void setTransType(String transType) {
		this.transType = transType;
	}

	/**
	 * @return the transType
	 */
	public String getTransType() {
		return transType;
	}

	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @return the waiveAllFlag
	 */
	public String getWaiveAllFlag() {
		return waiveAllFlag;
	}

	/**
	 * @param waiveAllFlag the waiveAllFlag to set
	 */
	public void setWaiveAllFlag(String waiveAllFlag) {
		this.waiveAllFlag = waiveAllFlag;
	}

	/**
	 * @param createTime the createTime to set
	 */
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return the createTime
	 */
	public String getCreateTime() {
		return createTime;
	}

	/**
	 * @param ownerName the ownerName to set
	 */
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	/**
	 * @return the ownerName
	 */
	public String getOwnerName() {
		return ownerName;
	}

	/**
	 * @param bankName the bankName to set
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	/**
	 * @return the bankName
	 */
	public String getBankName() {
		return bankName;
	}

	public String getBankBranch() {
		return bankBranch;
	}

	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}

	/**
	 * @param inputChannel the inputChannel to set
	 */
	public void setInputChannel(String inputChannel) {
		this.inputChannel = inputChannel;
	}

	/**
	 * @return the inputChannel
	 */
	public String getInputChannel() {
		return inputChannel;
	}

	public String getTransferChannel() {
		return transferChannel;
	}

	public void setTransferChannel(String transferChannel) {
		this.transferChannel = transferChannel;
	}

}
