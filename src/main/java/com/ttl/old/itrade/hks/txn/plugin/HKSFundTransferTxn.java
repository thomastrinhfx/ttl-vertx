//BEGIN TASK #: - TTL-CN-XYL-00051 20100120 [iTrade R5] Fund Transfer for ORS
package com.ttl.old.itrade.hks.txn.plugin;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.ttl.old.itrade.hks.bean.HKSComboBoxBean;
import com.ttl.old.itrade.hks.bean.plugin.HKSBankInfoBean;
import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.hks.util.AgentUtils;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.winvest.hks.config.mapping.TagName;

public class HKSFundTransferTxn extends BaseTxn {

	private String mvClientID;
	private String mvClientName;
	private String mvTradingAccSeq;
	private String mvFundTransferFrom;
	private String mvTargetAccount;
	private String mvAccount;
	private String mvAmount;
	private String mvIsPasswordConfirm;
	private String mvIsSecurityCodeConfirm;
	private String mvPassword;
	private String mvSecurityCode;

	private String mvDestBankID;
	private String mvDestAccountName;
	private String mvBankId;
	private String mvDestClientID;
	private String mvTransferType;
	private String mvRemark;
	private String mvPersonCharged;
	private String mvTranID;
	private String mvStatus;
	private String mvWithdrawAmt;
	private String mvAvaiableAmt;
	private String inputBankName;
	private String inputBankBranch;

	private String mvResult;

	public String getMvWithdrawAmt() {
		return mvWithdrawAmt;
	}

	public void setMvWithdrawAmt(String mvWithdrawAmt) {
		this.mvWithdrawAmt = mvWithdrawAmt;
	}

	public String getMvAvaiableAmt() {
		return mvAvaiableAmt;
	}

	public void setMvAvaiableAmt(String mvAvaiableAmt) {
		this.mvAvaiableAmt = mvAvaiableAmt;
	}

	public String getMvClientName() {
		return mvClientName;
	}

	public void setMvClientName(String mvClientName) {
		this.mvClientName = mvClientName;
	}

	/**
	 * @return the mvResult
	 */
	public String getMvResult() {
		return mvResult;
	}

	/**
	 * @param mvResult the mvResult to set
	 */
	public void setMvResult(String mvResult) {
		this.mvResult = mvResult;
	}

	/**
	 * @return the mvReturnCode
	 */
	public String getMvReturnCode() {
		return mvReturnCode;
	}

	/**
	 * @param mvReturnCode the mvReturnCode to set
	 */
	public void setMvReturnCode(String mvReturnCode) {
		this.mvReturnCode = mvReturnCode;
	}

	/**
	 * @return the mvReturnMsg
	 */
	public String getMvReturnMsg() {
		return mvReturnMsg;
	}

	/**
	 * @param mvReturnMsg the mvReturnMsg to set
	 */
	public void setMvReturnMsg(String mvReturnMsg) {
		this.mvReturnMsg = mvReturnMsg;
	}

	private String mvReturnCode;
	private String mvReturnMsg;

	/**
	 * @return the mvDestBankID
	 */
	public String getMvDestBankID() {
		return mvDestBankID;
	}

	/**
	 * @param mvDestBankID the mvDestBankID to set
	 */
	public void setMvDestBankID(String mvDestBankID) {
		this.mvDestBankID = mvDestBankID;
	}

	public HKSFundTransferTxn(String pClientID) {
		super();
		this.setMvClientID(pClientID);
	}

	/**
	 * constructor for HKSFundTransferTxn
	 * 
	 * @param pClientID, pFundTransferFrom, pTargetAccount, pAccount, pAmount, pIsPasswordConfirm, pIsSecurityCodeConfirm, pPassword,
	 *            pSecurityCode
	 */
	public HKSFundTransferTxn(String pClientID, String pBankID, String pFundTransferFrom, String pDestBankId, String pDestClientId,
			String pAmount, String pIsPasswordConfirm, String pIsSecurityCodeConfirm, String pPassword, String pSecurityCode,
			String mvTransferType, String mvRemark, String mvPersonCharged) {
		super();
		this.setMvClientID(pClientID);
		this.setMvBankId(pBankID);
		this.setMvFundTransferFrom(pFundTransferFrom);
		this.setMvDestBankID(pDestBankId);
		this.setMvDestClientID(pDestClientId);
		this.setMvAmount(pAmount);
		this.setMvIsPasswordConfirm(pIsPasswordConfirm);
		this.setMvIsSecurityCodeConfirm(pIsSecurityCodeConfirm);
		this.setMvPassword(pPassword);
		this.setMvSecurityCode(pSecurityCode);
		this.setMvTransferType(mvTransferType);
		this.setMvRemark(mvRemark);
		this.mvPersonCharged = mvPersonCharged;
	}

	/**
	 * @return the mvBankId
	 */
	public String getMvBankId() {
		return mvBankId;
	}

	/**
	 * @param mvBankId the mvBankId to set
	 */
	public void setMvBankId(String mvBankId) {
		this.mvBankId = mvBankId;
	}

	/**
	 * @return the mvDestClientID
	 */
	public String getMvDestClientID() {
		return mvDestClientID;
	}

	/**
	 * @param mvDestClientID the mvDestClientID to set
	 */
	public void setMvDestClientID(String mvDestClientID) {
		this.mvDestClientID = mvDestClientID;
	}

	/**
	 * Get banks with balance list
	 * 
	 * @return
	 */
	public List<HKSBankInfoBean> getBankBalanceList() {
		return getBankBalanceList(false);
	}

	/**
	 * Get bank balance list
	 * 
	 * @param includeDefault if 'true' return the default settlement account
	 * @return
	 */
	public List<HKSBankInfoBean> getBankBalanceList(boolean includeDefault) {
		List<HKSBankInfoBean> result = new ArrayList<HKSBankInfoBean>();

		// Add the default settlement account as the first bank, for display only, no balance include
		if (includeDefault) {
			HKSBankInfoBean defaultBank = new HKSBankInfoBean();
			defaultBank.setMvBankID("");
			defaultBank.setMvBankACID("");
			defaultBank.setMvSettlementAccountDisplayName(IMain.getProperty("DefaultSetteleAcc"));
			result.add(defaultBank);
		}

		// Get list of banks with balance
		Hashtable<String, String> lvTxnMap = new Hashtable<String, String>();
		lvTxnMap.put("CLIENTID", mvClientID);
		int returnCode = process("HKSQueryBankBalanceList", lvTxnMap);
		if (returnCode == TPErrorHandling.TP_NORMAL) {
			IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList("ChildRow");
			for (int i = 0; i < lvRowList.size(); i++) {
				HKSBankInfoBean bean = new HKSBankInfoBean();

				IMsgXMLNode lvRow = lvRowList.getNode(i);

				// get bank id
				bean.setMvBankID(lvRow.getChildNode(TagName.BANKID).getValue());
				// get bank acid
				bean.setMvBankACID(lvRow.getChildNode(TagName.BANKACID).getValue());
				// get balance
				bean.setMvBalance(lvRow.getChildNode(TagName.BALANCE).getValue());

				bean.setMvSettlementAccountDisplayName(lvRow.getChildNode(TagName.BANKID).getValue() + " - "
						+ lvRow.getChildNode(TagName.BANKACID).getValue());

				result.add(bean);
			}
		} else {
			if (returnCode == TPErrorHandling.TP_APP_ERR) {
				Log.println("[ HKSGenEnterOrderAction.getSettlementAccountList(): Order for " + mvClientID
						+ " fail with application error:" + getErrorMessage() + " ]", Log.ERROR_LOG);
			} else if (returnCode == TPErrorHandling.TP_SYS_ERR) {
				Log.println("[ HKSGenEnterOrderAction.getSettlementAccountList(): Order for " + mvClientID + " fail with system error:"
						+ getErrorMessage() + " ]", Log.ERROR_LOG);
			}
		}

		return result;
	}

	/**
	 * To get the transfer target account list.
	 * 
	 * @return mvTransferTargetAccountList : A List for transfer target accounts.
	 */
	@SuppressWarnings("rawtypes")
	public List getTransferTargetAccountList() {

		List<HKSComboBoxBean> mvTransferTargetAccountList = null;

		// get bank acc list for the associated client
		HKSQueryBankInformationTxn mvQueryBankInfoTxn = new HKSQueryBankInformationTxn(mvClientID, mvTradingAccSeq);
		List lvBankInfoList = mvQueryBankInfoTxn.process();

		if (lvBankInfoList != null && lvBankInfoList.size() > 0) {

			mvTransferTargetAccountList = new ArrayList<HKSComboBoxBean>();
			HKSComboBoxBean lvTargetAccountORSBean = new HKSComboBoxBean();
			lvTargetAccountORSBean.setMvID("ORS");
			lvTargetAccountORSBean.setMvOptionDisplay("ORS");
			lvTargetAccountORSBean.setMvOptionValue("ORS");
			mvTransferTargetAccountList.add(lvTargetAccountORSBean);

			Map lvBankInfoMap = null;
			String bankID = "";
			for (int i = 0; i < lvBankInfoList.size(); i++) {
				lvTargetAccountORSBean = new HKSComboBoxBean();

				lvBankInfoMap = (Hashtable) lvBankInfoList.get(i);
				bankID = lvBankInfoMap.get(TagName.BANKID).toString();
				lvTargetAccountORSBean.setMvID(bankID);
				lvTargetAccountORSBean.setMvOptionDisplay(bankID);
				lvTargetAccountORSBean.setMvOptionValue(bankID);
				mvTransferTargetAccountList.add(lvTargetAccountORSBean);
			}
		} else {
			setErrorCode(mvQueryBankInfoTxn.getErrorCode());
		}
		return mvTransferTargetAccountList;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String getAvailableAmount() {
		String result = "";
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put("CLIENTID", mvClientID);
		/* lvTxnMap.put("BANKID", bankId); */

		if (TPErrorHandling.TP_NORMAL == process("QueryAvailableAmount", lvTxnMap)) {
			result = mvReturnNode.getChildNode("AVAILABLE_BALANCE").getValue();
		}
		return result;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Hashtable getReceiverACCInfomation() {
		Hashtable listReceiverInfo = new Hashtable();

		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put("CLIENTID", mvClientID);

		// add bank internal/ external fee id
		String exFee = IMain.getProperty("bankExternalFeeId");
		String inFee = IMain.getProperty("bankInternalFeeId");
		if (exFee != null && exFee.trim().length() > 0) {
			lvTxnMap.put("BANKEXTERNALFEEID", exFee.trim());
		}

		if (inFee != null && inFee.trim().length() > 0) {
			lvTxnMap.put("BANKINTERNALFEEID", inFee.trim());
		}

		String balance = "0";
		String chargeRate = "0";
		String[] receiverInfo = null;
		if (TPErrorHandling.TP_NORMAL == process("QueryAvailableAmount", lvTxnMap)) {
			balance = mvReturnNode.getChildNode("AVAILABLE_BALANCE").getValue();
			String total = mvReturnNode.getChildNode("COUNTER").getValue();
			chargeRate = mvReturnNode.getChildNode("CHARGE_RATE").getValue();
			if (total != null && !total.equals("")) {
				receiverInfo = new String[Integer.parseInt(total)];
				for (int i = 0; i < Integer.parseInt(total); i++) {
					receiverInfo[i] = mvReturnNode.getChildNode("NODE" + i).getValue();
				}
			}
		}

		// add to return value
		listReceiverInfo.put("AVAILABLE_BALANCE", balance);
		listReceiverInfo.put("RECEIVERS", receiverInfo);
		listReceiverInfo.put("CHARGE_RATE", chargeRate);

		return listReceiverInfo;
	}

	/**
	 * Calling TP with the parameter Hashtable to process fund transfer.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void doFundTransfer() {
		Log.println("[ HKSFundTransferTxn.process() starts [" + this.getMvClientID() + "]", Log.ACCESS_LOG);

		Hashtable lvParamMap = new Hashtable();
		lvParamMap.put("CLIENTID", this.getMvClientID());
		// HIEU LE:
		lvParamMap.put("OPRID", AgentUtils.getAgentID(this.getMvClientID()));
		lvParamMap.put("EMAILLIST", AgentUtils.getOperatorEmails(this.getMvClientID(), AgentUtils.FUNCTION_TYPE_TRANSFER.toUpperCase()));
		lvParamMap.put("FULLNAME", getMvClientName());
		lvParamMap.put("AVAIABLEAMT", getMvAvaiableAmt());
		lvParamMap.put("WITHDRAWAMT", getMvWithdrawAmt());
		// END HIEU LE
		lvParamMap.put("BANKID", this.getMvBankId());
		lvParamMap.put("DESTBANKID", this.getMvDestBankID());
		lvParamMap.put("DESTCLIENTID", this.getMvDestClientID());
		lvParamMap.put("DESTACCOUNTNAME", getMvDestAccountName());
		lvParamMap.put("INPUTBANKNAME", getInputBankName());
		lvParamMap.put("INPUTBANKBRANCH", getInputBankBranch());

		// set transfer type and personal in charged
		String personCharged = "N";
		if (mvTransferType.equals("1")) {
			mvTransferType = "E";
			// set personal in charged
			if (mvPersonCharged.equals("2")) {
				personCharged = "Y";
			}
		} else if (mvTransferType.equals("2")) {
			mvTransferType = "I";
		}

		lvParamMap.put("TRANSFERTYPE", this.getMvTransferType());
		lvParamMap.put(TagName.WAIVEALLFLAG, personCharged);

		// set remark is blank when no input remark
		if (this.getMvRemark() == null || this.getMvRemark().trim().length() == 0) {
			mvRemark = "";
		}
		lvParamMap.put("REMARK", this.getMvRemark().trim());

		lvParamMap.put("AMOUNT", this.getMvAmount());
		if (this.getMvPassword() != null && this.getMvPassword().length() > 0) {
			lvParamMap.put("PASSWORD", this.getMvPassword());
			lvParamMap.put(TagName.PASSWORDVERIFICATION, "Y");
		} else {
			lvParamMap.put(TagName.PASSWORDVERIFICATION, "N");
		}
		if (this.getMvIsSecurityCodeConfirm() != null && this.getMvIsSecurityCodeConfirm().equalsIgnoreCase("true")) {
			lvParamMap.put("SCODEVERIFICATION", "Y");
			// lvParamMap.put("SECURITYCODE", this.getMvSecurityCode());
			lvParamMap.put("SECURITYCODE", "");
			lvParamMap.put(TagName.PASSWORDVERIFICATION, "N");
		} else {
			lvParamMap.put("SCODEVERIFICATION", "N");
		}

		// add bank internal/ external fee id
		String exFee = IMain.getProperty("bankExternalFeeId");
		String inFee = IMain.getProperty("bankInternalFeeId");
		String sysBankFee = IMain.getProperty("bankSystemFeeId");
		String sysBank = IMain.getProperty("bankSystemId");

		if (exFee != null && exFee.trim().length() > 0) {
			lvParamMap.put("BANKEXTERNALFEEID", exFee.trim());
		}

		if (inFee != null && inFee.trim().length() > 0) {
			lvParamMap.put("BANKINTERNALFEEID", inFee.trim());
		}

		if (sysBankFee != null && sysBankFee.trim().length() > 0) {
			lvParamMap.put("BANKSYSTEMFEEID", sysBankFee.trim());
		}

		if (sysBank != null && sysBank.trim().length() > 0) {
			lvParamMap.put("BANKSYSTEMID", sysBank.trim());
		}

		Log.println("[ HKSFundTransferTxn.process() finished constructing lvParamMap [" + this.getMvClientID() + "]", Log.ACCESS_LOG);

		if (TPErrorHandling.TP_NORMAL == super.process("FundTransfer", lvParamMap)) {
			mvResult = mvReturnNode.getChildNode(TagName.RESULT).getValue();
			mvReturnCode = mvReturnNode.getChildNode("RETURNCODE").getValue();
			mvReturnMsg = mvReturnNode.getChildNode(TagName.RETURNMESSAGE).getValue();
		}
		Log.println("[ HKSFundTransferTxn.process() ends [" + this.getMvClientID() + "]", Log.ACCESS_LOG);
	}

	/**
	 * Cancel Fundtransfer function
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void cancelFundTransfer() {
		Log.print("HKSFundTransferTxn.cancelFundTransfer(): tranID :" + mvTranID + " status " + mvStatus + " clientId" + mvClientID
				+ " ----> START", Log.ACCESS_LOG);

		Hashtable lvParamMap = new Hashtable();
		lvParamMap.put("CLIENTID", this.getMvClientID());

		lvParamMap.put("TRANID", this.getMvTranID());
		lvParamMap.put("STATUS", this.getMvStatus());

		Log.println("[ HKSFundTransferTxn.cancelFundTransfer() finished constructing lvParamMap [" + this.getMvClientID() + "]",
				Log.ACCESS_LOG);

		if (TPErrorHandling.TP_NORMAL == super.process("CancelFundTransfer", lvParamMap)) {
			mvResult = mvReturnNode.getChildNode(TagName.RESULT).getValue();
			mvReturnCode = mvReturnNode.getChildNode("RETURNCODE").getValue();
			mvReturnMsg = mvReturnNode.getChildNode(TagName.RETURNMESSAGE).getValue();
		}
		Log.print("HKSFundTransferTxn.cancelFundTransfer(): tranID :" + mvTranID + " status " + mvStatus + " clientId" + mvClientID
				+ " <---- END", Log.ACCESS_LOG);
	}

	/**
	 * Getting mvClientID
	 * 
	 * @return mvClientID
	 */
	public String getMvClientID() {
		return mvClientID;
	}

	/**
	 * Setting mvClientID
	 * 
	 * @param mvClientID
	 */
	public void setMvClientID(String mvClientID) {
		this.mvClientID = mvClientID;
	}

	/**
	 * Getting mvFundTransferFrom
	 * 
	 * @return mvFundTransferFrom
	 */
	public String getMvFundTransferFrom() {
		return mvFundTransferFrom;
	}

	/**
	 * Setting mvFundTransferFrom
	 * 
	 * @param mvFundTransferFrom
	 */
	public void setMvFundTransferFrom(String mvFundTransferFrom) {
		this.mvFundTransferFrom = mvFundTransferFrom;
	}

	/**
	 * Getting mvTargetAccount
	 * 
	 * @return mvTargetAccount
	 */
	public String getMvTargetAccount() {
		return mvTargetAccount;
	}

	/**
	 * Setting mvTargetAccount
	 * 
	 * @param mvTargetAccount
	 */
	public void setMvTargetAccount(String mvTargetAccount) {
		this.mvTargetAccount = mvTargetAccount;
	}

	/**
	 * Getting mvAccount
	 * 
	 * @return
	 */
	public String getMvAccount() {
		return mvAccount;
	}

	/**
	 * Setting mvAccount
	 * 
	 * @param mvAccount
	 */
	public void setMvAccount(String mvAccount) {
		this.mvAccount = mvAccount;
	}

	/**
	 * Getting mvAmount
	 * 
	 * @return
	 */
	public String getMvAmount() {
		return mvAmount;
	}

	/**
	 * Setting mvAmount
	 * 
	 * @param mvAmount
	 */
	public void setMvAmount(String mvAmount) {
		this.mvAmount = mvAmount;
	}

	/**
	 * Getting mvIsPasswordConfirm
	 * 
	 * @return
	 */
	public String getMvIsPasswordConfirm() {
		return mvIsPasswordConfirm;
	}

	/**
	 * Setting mvIsPasswordConfirm
	 * 
	 * @param mvIsPasswordConfirm
	 */
	public void setMvIsPasswordConfirm(String mvIsPasswordConfirm) {
		this.mvIsPasswordConfirm = mvIsPasswordConfirm;
	}

	/**
	 * Getting mvIsSecurityCodeConfirm
	 * 
	 * @return
	 */
	public String getMvIsSecurityCodeConfirm() {
		return mvIsSecurityCodeConfirm;
	}

	/**
	 * Setting mvIsSecurityCodeConfirm
	 * 
	 * @param mvIsSecurityCodeConfirm
	 */
	public void setMvIsSecurityCodeConfirm(String mvIsSecurityCodeConfirm) {
		this.mvIsSecurityCodeConfirm = mvIsSecurityCodeConfirm;
	}

	/**
	 * Getting mvPassword
	 * 
	 * @return
	 */
	public String getMvPassword() {
		return mvPassword;
	}

	/**
	 * Setting mvPassword
	 * 
	 * @param mvPassword
	 */
	public void setMvPassword(String mvPassword) {
		this.mvPassword = mvPassword;
	}

	/**
	 * Getting mvSecurityCode
	 * 
	 * @return
	 */
	public String getMvSecurityCode() {
		return mvSecurityCode;
	}

	/**
	 * Setting mvSecurityCode
	 * 
	 * @param mvSecurityCode
	 */
	public void setMvSecurityCode(String mvSecurityCode) {
		this.mvSecurityCode = mvSecurityCode;
	}

	/**
	 * @param mvTransferType the mvTransferType to set
	 */
	public void setMvTransferType(String mvTransferType) {
		this.mvTransferType = mvTransferType;
	}

	/**
	 * @return the mvTransferType
	 */
	public String getMvTransferType() {
		return mvTransferType;
	}

	/**
	 * @param mvRemark the mvRemark to set
	 */
	public void setMvRemark(String mvRemark) {
		this.mvRemark = mvRemark;
	}

	/**
	 * @return the mvRemark
	 */
	public String getMvRemark() {
		return mvRemark;
	}

	/**
	 * @param mvPersonCharged the mvPersonCharged to set
	 */
	public void setMvPersonCharged(String mvPersonCharged) {
		this.mvPersonCharged = mvPersonCharged;
	}

	/**
	 * @return the mvPersonCharged
	 */
	public String getMvPersonCharged() {
		return mvPersonCharged;
	}

	/**
	 * @param mvTranID the mvTranID to set
	 */
	public void setMvTranID(String mvTranID) {
		this.mvTranID = mvTranID;
	}

	/**
	 * @return the mvTranID
	 */
	public String getMvTranID() {
		return mvTranID;
	}

	/**
	 * @param mvStatus the mvStatus to set
	 */
	public void setMvStatus(String mvStatus) {
		this.mvStatus = mvStatus;
	}

	/**
	 * @return the mvStatus
	 */
	public String getMvStatus() {
		return mvStatus;
	}

	public String getMvDestAccountName() {
		return mvDestAccountName;
	}

	public void setMvDestAccountName(String mvDestAccountName) {
		this.mvDestAccountName = mvDestAccountName;
	}

	public String getInputBankName() {
		return inputBankName;
	}

	public void setInputBankName(String inputBankName) {
		this.inputBankName = inputBankName;
	}

	public String getInputBankBranch() {
		return inputBankBranch;
	}

	public void setInputBankBranch(String inputBankBranch) {
		this.inputBankBranch = inputBankBranch;
	}

}
// END TASK #: - TTL-CN-XYL-00051 20100120 [iTrade R5] Fund Transfer for ORS
