package com.ttl.old.itrade.hks.bean;

/**
 * The HKSGenMarginDetailsBean class define variables that to save values 
 * for action 
 * @author 
 *
 */
public class HKSGenMarginDetailsBean {
	private String mvEntitlementId;
	private String mvInstrumentId;
	private String mvStockName;
	private String mvNumberofOfferShare;
	private String mvNumberofPubOfferShare;
	private String mvOfferPrice;
	private String mvIPOClosingDate;
	private String mvIPOOnlineClosingDate;
	private String mvListingDate;
	private String mvProspectusLink;
	private String mvWebsite;
	private String mvApplStatus;

	/**
     * This method returns the entitlement id of margin apply.
     * @return the entitlement id of margin apply.
     */
	public String getMvEntitlementId() {
		return mvEntitlementId;
	}
	
	/**
     * This method sets the entitlement id of margin apply.
     * @param pEntitlementId The entitlement id of margin apply.
     */
	public void setMvEntitlementId(String pEntitlementId) {
		mvEntitlementId = pEntitlementId;
	}
	
	/**
     * This method returns the id of instrument.
     * @return the id of instrument.
     */
	public String getMvInstrumentId() {
		return mvInstrumentId;
	}
	
	/**
     * This method sets the id of instrument.
     * @param pInstrumentId The id of instrument.
     */
	public void setMvInstrumentId(String pInstrumentId) {
		mvInstrumentId = pInstrumentId;
	}
	
	/**
     * This method returns the name of stock.
     * @return the name of stock.
     */
	public String getMvStockName() {
		return mvStockName;
	}
	
	/**
     * This method sets the name of stock.
     * @param pStockName The name of stock.
     */
	public void setMvStockName(String pStockName) {
		mvStockName = pStockName;
	}
	
	/**
     * This method returns the number of offer share.
     * @return the number of offer share.
     */
	public String getMvNumberofOfferShare() {
		return mvNumberofOfferShare;
	}
	
	/**
     * This method sets the number of offer share.
     * @param pNumberofOfferShare The number of offer share.
     */
	public void setMvNumberofOfferShare(String pNumberofOfferShare) {
		mvNumberofOfferShare = pNumberofOfferShare;
	}
	
	/**
     * This method returns the number of pub offer share.
     * @return the number of pub offer share.
     */
	public String getMvNumberofPubOfferShare() {
		return mvNumberofPubOfferShare;
	}
	
	/**
     * This method sets the number of pub offer share.
     * @param pNumberofPubOfferShare The number of pub offer share.
     */
	public void setMvNumberofPubOfferShare(String pNumberofPubOfferShare) {
		mvNumberofPubOfferShare = pNumberofPubOfferShare;
	}
	
	/**
     * This method returns the offer price.
     * @return the offer price is formatted.
     */
	public String getMvOfferPrice() {
		return mvOfferPrice;
	}
	
	/**
     * This method sets the offer price.
     * @param pOfferPrice The offer price is formatted.
     */
	public void setMvOfferPrice(String pOfferPrice) {
		mvOfferPrice = pOfferPrice;
	}
	
	/**
     * This method returns the ipo closing date.
     * @return the ipo closing date.
     */
	public String getMvIPOClosingDate() {
		return mvIPOClosingDate;
	}
	
	/**
     * This method sets the ipo closing date.
     * @param pIPOClosingDate The ipo closing date.
     */
	public void setMvIPOClosingDate(String pIPOClosingDate) {
		mvIPOClosingDate = pIPOClosingDate;
	}
	
	/**
     * This method returns the ipo online closing date.
     * @return the ipo online closing date.
     */
	public String getMvIPOOnlineClosingDate() {
		return mvIPOOnlineClosingDate;
	}
	
	/**
     * This method sets the ipo online closing date.
     * @param pIPOOnlineClosingDate The ipo online closing date.
     */
	public void setMvIPOOnlineClosingDate(String pIPOOnlineClosingDate) {
		mvIPOOnlineClosingDate = pIPOOnlineClosingDate;
	}
	
	/**
     * This method returns the listing date.
     * @return the listing date.
     */
	public String getMvListingDate() {
		return mvListingDate;
	}
	
	/**
     * This method sets the listing date.
     * @param pListingDate The listing date.
     */
	public void setMvListingDate(String pListingDate) {
		mvListingDate = pListingDate;
	}
	
	/**
     * This method returns the prospectus link.
     * @return the prospectus link.
     */
	public String getMvProspectusLink() {
		return mvProspectusLink;
	}
	
	/**
     * This method sets the prospectus link.
     * @param pProspectusLink The prospectus link.
     */
	public void setMvProspectusLink(String pProspectusLink) {
		mvProspectusLink = pProspectusLink;
	}
	
	/**
     * This method returns the web site.
     * @return the web site.
     */
	public String getMvWebsite() {
		return mvWebsite;
	}
	
	/**
     * This method sets the web site.
     * @param pWebsite The web site.
     */
	public void setMvWebsite(String pWebsite) {
		mvWebsite = pWebsite;
	}
	
	/**
     * This method returns the apply status.
     * @return the apply status.
     */
	public String getMvApplStatus() {
		return mvApplStatus;
	}
	
	/**
     * This method sets the apply status.
     * @param pApplStatus The apply status.
     */
	public void setMvApplStatus(String pApplStatus) {
		mvApplStatus = pApplStatus;
	}
}