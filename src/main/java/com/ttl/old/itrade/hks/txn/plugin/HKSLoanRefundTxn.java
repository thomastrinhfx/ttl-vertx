package com.ttl.old.itrade.hks.txn.plugin;

import java.util.Hashtable;

import com.ttl.old.itrade.hks.bean.plugin.HKSLoanRefundBean;
import com.ttl.old.itrade.hks.bean.plugin.HKSLoanRefundCreationInforBean;
import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.winvest.hks.config.mapping.TagName;

public class HKSLoanRefundTxn extends BaseTxn{

	private String mvClientID;
	private Boolean mvSuccess = false;
	
	private String mvStartDate;
	
	private String mvEndDate;
	
	private int mvStartRecord;
	private int mvEndRecord;
	private String mvTotalRecord;

	private HKSLoanRefundBean[] loanrefundList;
	
	private HKSLoanRefundBean[] loanrefundHistoryList;
	
	public HKSLoanRefundBean[] getLoanrefundHistoryList() {
		return loanrefundHistoryList;
	}


	public void setLoanrefundHistoryList(HKSLoanRefundBean[] loanrefundHistoryList) {
		this.loanrefundHistoryList = loanrefundHistoryList;
	}

	/**
	 * @param loanrefundList the loanrefundList to set
	 */
	public void setLoanRefundList(HKSLoanRefundBean[] loanrefundList) {
		this.loanrefundList = loanrefundList;
	}
	
	/**
	 * @return the loanrefundList
	 */
	public HKSLoanRefundBean[] getloanrefundList() {
		return loanrefundList;
	}
	
	public String getMvClientID() {
		return mvClientID;
	}

	public void setMvClientID(String mvClientID) {
		this.mvClientID = mvClientID;
	}
	
	public String getMvStartDate() {
		return mvStartDate;
	}



	public void setMvStartDate(String mvStartDate) {
		this.mvStartDate = mvStartDate;
	}



	public String getMvEndDate() {
		return mvEndDate;
	}



	public void setMvEndDate(String mvEndDate) {
		this.mvEndDate = mvEndDate;
	}



	public HKSLoanRefundTxn(String pClientID) {
		mvClientID = pClientID;
	}
	
	
	
	public int getMvStartRecord() {
		return mvStartRecord;
	}


	public void setMvStartRecord(int mvStartRecord) {
		this.mvStartRecord = mvStartRecord;
	}


	public int getMvEndRecord() {
		return mvEndRecord;
	}


	public void setMvEndRecord(int mvEndRecord) {
		this.mvEndRecord = mvEndRecord;
	}


	public String getMvTotalRecord() {
		return mvTotalRecord;
	}


	public void setMvTotalRecord(String mvTotalRecord) {
		this.mvTotalRecord = mvTotalRecord;
	}


	/**
	 * Function get loan refund history
	 */
	public void getLoanRefundHistory(){
		
		// Send TP request
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put(TagName.CLIENTID, mvClientID);
		lvTxnMap.put(TagName.FROMDATE, String.valueOf(mvStartDate));
		lvTxnMap.put(TagName.TODATE, String.valueOf(mvEndDate));
		lvTxnMap.put("STARTRECORD", String.valueOf(mvStartRecord));
		lvTxnMap.put("ENDRECORD", String.valueOf(mvEndRecord));
		
		if(TPErrorHandling.TP_NORMAL==process("HKSLoanHistoryEnquiry",lvTxnMap)){
			if (mvReturnNode.getChildNode("TOTALRECORD") != null) {
				setMvTotalRecord(mvReturnNode.getChildNode("TOTALRECORD")
						.getValue());
			}
			IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList("ChildRow");
			setLoanrefundHistoryList(new HKSLoanRefundBean[lvRowList.size()]);
			for (int i = 0; i < lvRowList.size(); i++) {
				getLoanrefundHistoryList()[i] = new HKSLoanRefundBean();
				IMsgXMLNode lvRow = lvRowList.getNode(i);
				
				getLoanrefundHistoryList()[i].setTranID(lvRow.getChildNode(TagName.TRANID).getValue());
				getLoanrefundHistoryList()[i].setTradeDate(lvRow.getChildNode(TagName.TRANDATE).getValue());
				getLoanrefundHistoryList()[i].setRefundAmt(lvRow.getChildNode(TagName.AMOUNT).getValue());
				getLoanrefundHistoryList()[i].setType(lvRow.getChildNode(TagName.TYPE).getValue());
				getLoanrefundHistoryList()[i].setStatus(lvRow.getChildNode(TagName.STATE).getValue());
				getLoanrefundHistoryList()[i].setRemark(lvRow.getChildNode(TagName.REMARK).getValue());
				getLoanrefundHistoryList()[i].setLastupdate(lvRow.getChildNode(TagName.LASTAPPROVALTIME).getValue());
				
			}			
		}
		
	}
	
	/**
	 * Function get loan refund data
	 */
	public void getLoanRefundData(){
		
		// Send TP request
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put(TagName.CLIENTID, mvClientID);
		lvTxnMap.put("STARTRECORD", String.valueOf(mvStartRecord));
		lvTxnMap.put("ENDRECORD", String.valueOf(mvEndRecord));
		
		if(TPErrorHandling.TP_NORMAL==process("HKSLoanEnquiry",lvTxnMap)){
			if (mvReturnNode.getChildNode("TOTALRECORD") != null) {
				setMvTotalRecord(mvReturnNode.getChildNode("TOTALRECORD")
						.getValue());
			}
			IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList("ChildRow");
			setLoanRefundList(new HKSLoanRefundBean[lvRowList.size()]);
			for (int i = 0; i < lvRowList.size(); i++) {
				getloanrefundList()[i] = new HKSLoanRefundBean();
				IMsgXMLNode lvRow = lvRowList.getNode(i);
				
				getloanrefundList()[i].setTranID(lvRow.getChildNode(TagName.TRANID).getValue());
				getloanrefundList()[i].setTradeDate(lvRow.getChildNode(TagName.TRANDATE).getValue());
				getloanrefundList()[i].setRefundAmt(lvRow.getChildNode(TagName.AMOUNT).getValue());
				getloanrefundList()[i].setType(lvRow.getChildNode(TagName.TYPE).getValue());
				getloanrefundList()[i].setStatus(lvRow.getChildNode(TagName.STATE).getValue());
				getloanrefundList()[i].setRemark(lvRow.getChildNode(TagName.REMARK).getValue());
				getloanrefundList()[i].setLastupdate(lvRow.getChildNode(TagName.LASTAPPROVALTIME).getValue());
				
			}			
		}
		
	}
	
	public HKSLoanRefundCreationInforBean getLocalLoanRefundCreation() {
		HKSLoanRefundCreationInforBean bean = new HKSLoanRefundCreationInforBean();
		try{
			
			Log.println("HKSLoanRefundTxn.getLocalLoanRefundCreation() START: Client ID = " + mvClientID , Log.ACCESS_LOG);
			Hashtable lvTxnMap = new Hashtable();
			lvTxnMap.put(TagName.CLIENTID, mvClientID);
			
			// Send TP request
			if(TPErrorHandling.TP_NORMAL==process("QueryLoanRefundCreationInfo",lvTxnMap)){
	
				bean.setAdvAvailable(mvReturnNode.getChildNode("AVAILABLEADVANCEMONEY").getValue());
				bean.setLoan(mvReturnNode.getChildNode("CUNREALIZEDLOAN").getValue().replace("-", ""));
			}
			
			if(TPErrorHandling.TP_NORMAL==process("QueryCashReleaseReserve",lvTxnMap)){
				bean.setCashrsv(mvReturnNode.getChildNode("RELEASABLE").getValue());
			}
			
		} catch(Exception e) {
			Log.println("HKSLoanRefundTxn.getLocalLoanRefundCreation() Exception: " + e.toString() + " for clientid: " + mvClientID , Log.ERROR_LOG);
		} finally {
			Log.println("HKSLoanRefundTxn.getLocalLoanRefundCreation() END: Client ID = " + mvClientID, Log.ACCESS_LOG);
		}
		
		return bean;
	}
}
