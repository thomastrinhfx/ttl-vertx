package com.ttl.old.itrade.hks.bean.plugin;

/**
 * The HKSAdvancePaymentInfoBean class define variables that to save values for action 
 * @author Wind.Zhao
 *
 */

//Begin Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100127 [iTrade R5] Make Advance Payment a Plugin Module
public class HKSAdvancePaymentInfoBean {
	private String mvIsPasswordConfirm;
	private String mvIsSecurityCodeConfirm;
	
	/**
	 * Returns the password if need to show.
	 * @return the password if need to show.
	 */
	public String getMvIsPasswordConfirm() {
		return mvIsPasswordConfirm;
	}
	
	/**
	 * Sets the password if need to show.
	 * @param pIsPasswordConfirm The the password if need to show.
	 */
	public void setMvIsPasswordConfirm(String pIsPasswordConfirm) {
		mvIsPasswordConfirm = pIsPasswordConfirm;
	}
	
	/**
	 * Returns the security code if need to show.
	 * @return the security code if need to show.
	 */
	public String getMvIsSecurityCodeConfirm() {
		return mvIsSecurityCodeConfirm;
	}
	
	/**
	 * Sets the security code if need to show.
	 * @param pIsSecurityCodeConfirm The security code if need to show.
	 */
	public void setMvIsSecurityCodeConfirm(String pIsSecurityCodeConfirm) {
		mvIsSecurityCodeConfirm = pIsSecurityCodeConfirm;
	}
}
//End Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100127 [iTrade R5] Make Advance Payment a Plugin Module
