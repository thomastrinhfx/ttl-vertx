package com.ttl.old.itrade.hks.bean;

/**
 * 
 * @author duonghuynh
 * 
 */
public class HKSClientContractBean {
	private String tranDate;
	private String instrumentSettleDate;
	private String cashSettleDate;
	private String clientId;
	private String bs;
	private String marketId;
	private String instrumentId;
	private String price;
	private String qty;
	private String contractAmount;
	private String bankId;
	private String bankAcId;

	public String getTranDate() {
		return tranDate;
	}

	public void setTranDate(String tranDate) {
		this.tranDate = tranDate;
	}

	public String getInstrumentSettleDate() {
		return instrumentSettleDate;
	}

	public void setInstrumentSettleDate(String instrumentSettleDate) {
		this.instrumentSettleDate = instrumentSettleDate;
	}

	public String getCashSettleDate() {
		return cashSettleDate;
	}

	public void setCashSettleDate(String cashSettleDate) {
		this.cashSettleDate = cashSettleDate;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getBs() {
		return bs;
	}

	public void setBs(String bs) {
		this.bs = bs;
	}

	public String getMarketId() {
		return marketId;
	}

	public void setMarketId(String marketId) {
		this.marketId = marketId;
	}

	public String getInstrumentId() {
		return instrumentId;
	}

	public void setInstrumentId(String instrumentId) {
		this.instrumentId = instrumentId;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}

	public String getContractAmount() {
		return contractAmount;
	}

	public void setContractAmount(String contractAmount) {
		this.contractAmount = contractAmount;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getBankAcId() {
		return bankAcId;
	}

	public void setBankAcId(String bankAcId) {
		this.bankAcId = bankAcId;
	}

}
