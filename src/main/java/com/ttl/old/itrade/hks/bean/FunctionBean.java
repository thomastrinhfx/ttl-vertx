package com.ttl.old.itrade.hks.bean;

/**
 * The FunctionBean class define variables that to save values for action 
 * @author 
 *
 */
public class FunctionBean {

	private String mvID;
	private String mvName;
	private String mvDisplayName;
	private boolean mvIsEnable;
	private String mvUrl;
	private String mvICON;
	private boolean mvIsShown;
	//BEGIN TASK: TTL-GZ-XYL-00045 XuYuLong 20091211 [iTrade R5] Plugin redesign and implement
	private String mvJSPath;
	private String mvLanguageSourceJSPath;
	
	//BEGIN TASK: TTLVN GiangTD 20110125 Add the function handler
	private String mvHandlerName;
	
	/**
	 * Returns the js path of internationalization according language.
	 * @return the js path of internationalization according language.
	 */
	public String getMvLanguageSourceJSPath() {
		return mvLanguageSourceJSPath;
	}
	
	/**
	 * Sets the js path of internationalization according language.
	 * @param pLanguageSourceJSPath The js path of internationalization to be set according language.
	 */
	public void setMvLanguageSourceJSPath(String pLanguageSourceJSPath) {
		this.mvLanguageSourceJSPath = pLanguageSourceJSPath;
	}
	
	/**
	 * Returns the js path.
	 * @return the js path.
	 */
	public String getMvJSPath() {
		return mvJSPath;
	}
	/**
	 * Sets the js path
	 * @param pJSPath The js path
	 */
	public void setMvJSPath(String pJSPath) {
		this.mvJSPath = pJSPath;
	}
	//END TASK: TTL-GZ-XYL-00045 XuYuLong 20091211 [iTrade R5] Plugin redesign and implement
	
	/**
	 * Returns the display name
	 * @return the display Name
	 */
	public String getMvDisplayName() {
		return mvDisplayName;
	}
	
	/**
	 * Sets the display name
	 * @param pDisplayName The name to be displayed
	 */
	public void setMvDisplayName(String pDisplayName) {
		this.mvDisplayName = pDisplayName;
	}
	
	/**
	 * Returns is shown or not 
	 * @return the shown is true or false
	 */
	public boolean isMvIsShown() {
		return mvIsShown;
	}
	
	/**
	 * Sets the is shown or not
	 * @param pIsShown The shown is true or false
	 */
	public void setMvIsShown(boolean pIsShown) {
		this.mvIsShown = pIsShown;
	}
	
	/**
	 * Returns the function ID
	 * @return the function ID
	 */
	public String getMvID() {
		return mvID;
	}
	
	/**
	 * Sets the function ID
	 * @param pID The function ID to be show
	 */
	public void setMvID(String pID) {
		this.mvID = pID;
	}
	
	/**
	 * Returns the function name
	 * @return the function name
	 */
	public String getMvName() {
		return mvName;
	}
	/**
	 * Sets the function name
	 * @param pName The function name to be show
	 */
	public void setMvName(String pName) {
		this.mvName = pName;
	}
	
	/**
	 * Returns the function if enable
	 * @return the function is enable with true or false
	 */
	public boolean isMvIsEnable() {
		return mvIsEnable;
	}
	
	/**
	 * Sets the function is enable or not
	 * @param pIsEnable The function if enable or not
	 */
	public void setMvIsEnable(boolean pIsEnable) {
		this.mvIsEnable = pIsEnable;
	}
	
	/**
	 * Returns the function url
	 * @return the function url will be use
	 */
	public String getMvUrl() {
		return mvUrl;
	}
	
	/**
	 * Sets the function url
	 * @param pUrl The function url
	 */
	public void setMvUrl(String pUrl) {
		this.mvUrl = pUrl;
	}
	
	/**
	 * Return the function icons
	 * @return the function icons will be show
	 */
	public String getMvICON() {
		return mvICON;
	}
	
	/**
	 * Sets the function icons
	 * @param pICON The function icons
	 */
	public void setMvICON(String pICON) {
		this.mvICON = pICON;
	}

	/**
	 * @param mvHandlerName the mvHandlerName to set
	 */
	public void setMvHandlerName(String mvHandlerName) {
		this.mvHandlerName = mvHandlerName;
	}

	/**
	 * @return the mvHandlerName
	 */
	public String getMvHandlerName() {
		return mvHandlerName;
	}
}
