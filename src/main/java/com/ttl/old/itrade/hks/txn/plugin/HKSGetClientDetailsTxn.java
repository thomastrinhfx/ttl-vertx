package com.ttl.old.itrade.hks.txn.plugin;

import java.util.ArrayList;
import java.util.Hashtable;

import com.ttl.old.itrade.hks.HKSTag;
import com.ttl.old.itrade.hks.bean.plugin.HKSAgentBean;
import com.ttl.old.itrade.hks.bean.plugin.HKSPersonnalProfileBean;
import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNodeList;

public class HKSGetClientDetailsTxn extends BaseTxn {

	
	private String	clientId;
	private String	language;
	
	private HKSPersonnalProfileBean mvPersonnalProfileBean;
	
	public HKSGetClientDetailsTxn(String pClientID, String pLanguage)
	{
		super();
		clientId = pClientID;
		language = pLanguage;
	}
	
	/**
	 * Get method for Client ID
	 * @return the Client ID
	 */
	public String getClientId()
	{
		return clientId;
	}

	/**
	 * Set method for Client ID
	 * @param pClientId the Client ID
	 */
	public void setClientId(String pClientId)
	{
		this.clientId = pClientId;
	}


	/**
	 * @param mvPersonnalProfileBean the mvPersonnalProfileBean to set
	 */
	public void setMvPersonnalProfileBean(HKSPersonnalProfileBean mvPersonnalProfileBean) {
		this.mvPersonnalProfileBean = mvPersonnalProfileBean;
	}


	/**
	 * @return the mvPersonnalProfileBean
	 */
	public HKSPersonnalProfileBean getMvPersonnalProfileBean() {
		return mvPersonnalProfileBean;
	}	
	
	@SuppressWarnings("unchecked")
	public void process()
	{
		try
		{
			Hashtable	lvTxnMap = new Hashtable();		
			lvTxnMap.put(HKSTag.CLIENTID, getClientId());
		
		    if (TPErrorHandling.TP_NORMAL == process("HKSGetClientDetailsRequest", lvTxnMap)) {
		    	
		    	mvPersonnalProfileBean = new HKSPersonnalProfileBean();
		    	mvPersonnalProfileBean.setMvName(mvReturnNode.getChildNode("NAME").getValue().trim());
		    	mvPersonnalProfileBean.setMvAccountNumber(mvReturnNode.getChildNode("ACCOUNTCODE").getValue().trim());
		    	
		    	String lvTemp = mvReturnNode.getChildNode("PHONENUMBER").getValue().trim();
		    	lvTemp =  lvTemp.equalsIgnoreCase("null")?"": lvTemp;		    	
		    	mvPersonnalProfileBean.setMvPhoneNumber(lvTemp);
		    	
		    	lvTemp = mvReturnNode.getChildNode("EMAIL").getValue().trim();
		    	lvTemp =  lvTemp.equalsIgnoreCase("null")?"": lvTemp;		    	
		    	mvPersonnalProfileBean.setMvEmail(lvTemp);
		    	
		    	lvTemp = mvReturnNode.getChildNode("IDNUMBER").getValue().trim();
		    	lvTemp =  lvTemp.equalsIgnoreCase("null")?"": lvTemp;		    	
		    	mvPersonnalProfileBean.setMvIDNumber(lvTemp);		    	
		    	
		    	lvTemp = mvReturnNode.getChildNode("ADDRESS").getValue().trim();
		    	lvTemp =  lvTemp.equalsIgnoreCase("null")?"": lvTemp;		    	
		    	mvPersonnalProfileBean.setMvAddress(lvTemp);
		    	
		    	String totalAgent = mvReturnNode.getChildNode("TOTALAGENT").getValue().trim();
		    	
		    	//IMsgXMLNodeList  agentNodeList =  mvReturnNode.getNodeList("AGENT");		    	
		    	HKSAgentBean agentBean = null;
		    	ArrayList agentList = new ArrayList();
		    	
		    	if(totalAgent != null && Integer.parseInt(totalAgent) > 0){
			    	for(int i = 0 ; i < Integer.parseInt(totalAgent); i++)
			    	{ 		    
			    		IMsgXMLNodeList  agentNodeList =  mvReturnNode.getNodeList("AGENT" + i );
			    		agentBean = new HKSAgentBean();
			    		agentBean.setAgentName(agentNodeList.getNode(0).getChildNode("AGENTNAME").getValue().trim());
			    		agentBean.setAgentPhone(agentNodeList.getNode(0).getChildNode("TELEPHONE").getValue().trim());
			    		agentBean.setAgentIDNumber(agentNodeList.getNode(0).getChildNode("PERSONALID").getValue().trim());
			    		
			    		String attorney = "";
			    		
			    		if("en_US".equals(language.trim())){
			    			attorney = agentNodeList.getNode(0).getChildNode("ATTORNEY").getValue().trim();
			    		} else {
			    			attorney = agentNodeList.getNode(0).getChildNode("ATTORNEY_VN").getValue().trim();
			    		}
			    		
			    		agentBean.setAgentAttorney(attorney);
			    		
			    		agentList.add(agentBean);
			    	}	    	
		    	}
		    	mvPersonnalProfileBean.setMvAgentList(agentList);	  	    		    	
		    }	
		}
		catch (Exception e)
		{
			e.printStackTrace();
		    Log.println("[ HKSGetClientDetailsTxn.process() ]" + e.toString(),  Log.ERROR_LOG);
		}	
	}	
}
