package com.ttl.old.itrade.hks.txn;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.tag.ITagXsfTagName;
/**
 * The HKSIPOFinancingLendingPercentageTxn class definition for all method
 * IPO percentage of loan financing
 * @author not attributable
 *
 */
public class HKSIPOFinancingLendingPercentageTxn extends BaseTxn{
	/**
	 * This variable is used to the tp Error
	 */
	private TPErrorHandling tpError;
	private int mvReturnCode;
    private String mvErrorCode;
    private String mvErrorMessage;
    
	private String mvEntitlementID;
	/**
	 * Default constructor for HKSIPOFinancingLendingPercentageTxn class
	 */
	public HKSIPOFinancingLendingPercentageTxn(){
		tpError = new TPErrorHandling();
	}
	/**
	 * Default constructor for HKSIPOFinancingLendingPercentageTxn class
	 * @param pEntitlementID the entitlement id
	 */
	public HKSIPOFinancingLendingPercentageTxn(String pEntitlementID){
		tpError = new TPErrorHandling();
		setEntitlementID(pEntitlementID);
	}
	/**
	 * This method process IPO percentage of loan financing
	 * @param pEntitlementID the entitlement id
	 * @return The percentage of IPOs to obtain a collection of loan financing
	 */
	public Vector process(String pEntitlementID){
		Vector lvReturn = new Vector();
		try{
			Log.println("HKSIPOFinancingLendingPercentageTxn starts", Log.ACCESS_LOG);
			
			Hashtable lvTxnMap = new Hashtable();
			
			lvTxnMap.put("ENTITLEMENTID", pEntitlementID);
			
			if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSMarginLendingPercentageRequest, lvTxnMap)){
				IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList(ITagXsfTagName.ROW);
				for(int i=0; i< lvRowList.size(); i++){
					IMsgXMLNode lvNode = lvRowList.getNode(i);
					HashMap lvModel = new HashMap();
					lvModel.put("LEADINGPERCENTAGE", lvNode.getChildNode("LEADINGPERCENTAGE").getValue());
					lvReturn.add(lvModel);
				}
			}
		}catch(Exception e)
		{	
			Log.println("HKSIPOFinancingLendingPercentageTxn error" + e.toString(), Log.ERROR_LOG);
		}
		return lvReturn;
	}
	/**
	 * Set method for entitlement id
	 * @param pEntitlementID the entitlement id
	 */
	public void setEntitlementID(String pEntitlementID){
		this.mvEntitlementID=pEntitlementID;
	}
	/**
	 * Set method for return system code
	 * @param pReturnCode the return system code
	 */
	public void setReturnCode(int pReturnCode)
    {
        mvReturnCode = pReturnCode;
    }
	/**
	 * Set method for system error code
	 * @param pErrorCode the system error code
	 */
    public void setErrorCode(String pErrorCode)
    {
        mvErrorCode = pErrorCode;
    }
    /**
     * Set method for system error message
     * @param pErrorMessage the system error message
     */
    public void setErrorMessage(String pErrorMessage)
    {
        mvErrorMessage = pErrorMessage;
    }
}
