package com.ttl.old.itrade.hks.util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import com.ttl.old.itrade.hks.HKSTag;
import com.ttl.old.itrade.hks.bean.HKSHistoricalDataBean;
import com.ttl.old.itrade.hks.bean.HKSMarketDataBean;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.systekit.winvest.hks.config.mapping.TagName;
import com.ttl.mds.model.BestPriceModel;
import com.ttl.mds.model.HistoricalDataModel;
import com.ttl.mds.model.StockInfoModel;

public class StockPriceInfoParser {

	/**
	 * 
	 */
	@SuppressWarnings("rawtypes")
	public static HKSMarketDataBean StockPriceInfoParsing(HashMap pStockInfoModel) {
		HKSMarketDataBean mvMarketDataBean = new HKSMarketDataBean();
		String lvMarketID = pStockInfoModel.get(TagName.MARKETID).toString();
		mvMarketDataBean.setMvSymbol(pStockInfoModel.get(TagName.INSTRUMENTID).toString());
		mvMarketDataBean.setMvMarketID(lvMarketID);

		Double lvReferencePrice = MarketDataUtils.parseDouble(pStockInfoModel.get(TagName.REFERENCEPRICE).toString());
		Double lvCeilingPrice = MarketDataUtils.parseDouble(pStockInfoModel.get("CEILING_PRICE").toString());
		Double lvFloorPrice = MarketDataUtils.parseDouble(pStockInfoModel.get("FLOOR_PRICE").toString());
		Double lvNominalPrice = MarketDataUtils.parseDouble(pStockInfoModel.get(TagName.NOMINALPRICE).toString());

		// For CSS code
		PriceCss priceCss = new PriceCss(lvReferencePrice, lvCeilingPrice, lvFloorPrice);

		// Set Values Accordingly
		mvMarketDataBean.setMvOpenPrice(MarketDataUtils.formatPrice(pStockInfoModel.get(TagName.OPENINGPRICE).toString()));
		mvMarketDataBean.setMvClosePrice(MarketDataUtils.formatPrice(pStockInfoModel.get(TagName.CLOSINGPRICE).toString()));
		mvMarketDataBean.setMvReferencePrice(MarketDataUtils.formatPrice(pStockInfoModel.get(TagName.REFERENCEPRICE).toString()));
		mvMarketDataBean.setMvCeilingPrice(MarketDataUtils.formatPrice(pStockInfoModel.get("CEILING_PRICE").toString()));
		mvMarketDataBean.setMvFloorPrice(MarketDataUtils.formatPrice(pStockInfoModel.get("FLOOR_PRICE").toString()));

		mvMarketDataBean.setMvNoalPriCss(priceCss.getCode(lvNominalPrice));
		mvMarketDataBean.setMvNominalPrice(MarketDataUtils.formatPrice(pStockInfoModel.get(TagName.NOMINALPRICE).toString()));

		// Bid 1st
		mvMarketDataBean.setMvBestBid1Css(priceCss.getCode(pStockInfoModel.get("BEST_BID_1_PRICE").toString()));
		mvMarketDataBean.setMvBestBid1Price(MarketDataUtils.formatPrice(pStockInfoModel.get("BEST_BID_1_PRICE").toString()));
		mvMarketDataBean.setMvBestBid1Volume(MarketDataUtils.formatVolume(pStockInfoModel.get("BEST_BID_1_VOLUME").toString()));
		// Bid 2nd
		mvMarketDataBean.setMvBestBid2Css(priceCss.getCode(pStockInfoModel.get("BEST_BID_2_PRICE").toString()));
		mvMarketDataBean.setMvBestBid2Price(MarketDataUtils.formatPrice(pStockInfoModel.get("BEST_BID_2_PRICE").toString()));
		mvMarketDataBean.setMvBestBid2Volume(MarketDataUtils.formatVolume(pStockInfoModel.get("BEST_BID_2_VOLUME").toString()));
		// Bid 3rd
		mvMarketDataBean.setMvBestBid3Css(priceCss.getCode(pStockInfoModel.get("BEST_BID_3_PRICE").toString()));
		mvMarketDataBean.setMvBestBid3Price(MarketDataUtils.formatPrice(pStockInfoModel.get("BEST_BID_3_PRICE").toString()));
		mvMarketDataBean.setMvBestBid3Volume(MarketDataUtils.formatVolume(pStockInfoModel.get("BEST_BID_3_VOLUME").toString()));
		// Offer 1st
		mvMarketDataBean.setMvBestOffer1Css(priceCss.getCode(pStockInfoModel.get("BEST_OFFER_1_PRICE").toString()));
		mvMarketDataBean.setMvBestOffer1Price(MarketDataUtils.formatPrice(pStockInfoModel.get("BEST_OFFER_1_PRICE").toString()));
		mvMarketDataBean.setMvBestOffer1Volume(MarketDataUtils.formatVolume(pStockInfoModel.get("BEST_OFFER_1_VOLUME").toString()));
		// Offer 2nd
		mvMarketDataBean.setMvBestOffer2Css(priceCss.getCode(pStockInfoModel.get("BEST_OFFER_2_PRICE").toString()));
		mvMarketDataBean.setMvBestOffer2Price(MarketDataUtils.formatPrice(pStockInfoModel.get("BEST_OFFER_2_PRICE").toString()));
		mvMarketDataBean.setMvBestOffer2Volume(MarketDataUtils.formatVolume(pStockInfoModel.get("BEST_OFFER_2_VOLUME").toString()));
		// Offer 3rd
		mvMarketDataBean.setMvBestOffer3Css(priceCss.getCode(pStockInfoModel.get("BEST_OFFER_3_PRICE").toString()));
		mvMarketDataBean.setMvBestOffer3Price(MarketDataUtils.formatPrice(pStockInfoModel.get("BEST_OFFER_3_PRICE").toString()));
		mvMarketDataBean.setMvBestOffer3Volume(MarketDataUtils.formatVolume(pStockInfoModel.get("BEST_OFFER_3_VOLUME").toString()));

		mvMarketDataBean.setMvHighPriCss(priceCss.getCode(pStockInfoModel.get(TagName.HIGHPRICE).toString()));
		mvMarketDataBean.setMvHighPrice(MarketDataUtils.formatPrice(pStockInfoModel.get(TagName.HIGHPRICE).toString()));

		mvMarketDataBean.setMvLowPriCss(priceCss.getCode(pStockInfoModel.get(TagName.LOWPRICE).toString()));
		mvMarketDataBean.setMvLowPrice(MarketDataUtils.formatPrice(pStockInfoModel.get(TagName.LOWPRICE).toString()));

		mvMarketDataBean.setMvTalVolCss(priceCss.getCode(lvNominalPrice));
		mvMarketDataBean.setMvTotalVol(MarketDataUtils.formatVolume(pStockInfoModel.get(TagName.VOLUME).toString()));

		mvMarketDataBean.setMvNomCmToRefPri(lvNominalPrice.compareTo(lvReferencePrice));

		setCompareNomRefPrice(mvMarketDataBean, lvReferencePrice, lvNominalPrice);

		Log.println("StockPriceInfoParsing:" + mvMarketDataBean.getMvSymbol(), Log.DEBUG_LOG);
		return mvMarketDataBean;
	}

	/**
	 * 
	 * @param pMarket
	 * @param pValue
	 * @param IsFormatValue
	 * @return
	 */
	public static String convertVolWithLotSize(String pMarket, String pValue, boolean IsFormatValue) {
		String resultValue = pValue;
		if (pValue.isEmpty() || pValue.equals("-")) {
			resultValue = "-";
		} else {
			if (IsFormatValue) {
				resultValue = (new DisplayNumber(resultValue)).toString();
			}
		}
		return resultValue;
	}

	/**
	 * 
	 * @param dateTime
	 * @param data
	 * @return
	 */
	public static HKSHistoricalDataBean parseHistorycalData(Long rawTime, String dateTime, String data) {
		HKSHistoricalDataBean hisBean = new HKSHistoricalDataBean();
		try {
			String[] lvTempMarketDataArray = StringSplitter.split(data, "\\|");
			hisBean.setMvTime(dateTime);
			hisBean.setMvRawTime(rawTime);
			hisBean.setMvMatchPrice(MarketDataUtils.formatPrice(lvTempMarketDataArray[13]));
			hisBean.setMvMatchQty(new BigDecimal(MarketDataUtils.parseDouble(lvTempMarketDataArray[14])).setScale(3, 4));
			hisBean.setMvTotalQty(MarketDataUtils.formatPrice(lvTempMarketDataArray[15]));
		} catch (Exception e) {
			Log.println("Exception in parseHistorycalData " + e, Log.DEBUG_LOG);
			Log.println("Print detail:" + data, Log.DEBUG_LOG);
		}

		return hisBean;
	}

	public static <K, V extends Comparable<V>> Map<K, V> sortByValues(final Map<K, V> map) {
		Comparator<K> valueComparator = new Comparator<K>() {
			public int compare(K k1, K k2) {
				int compare = map.get(k2).compareTo(map.get(k1));
				if (compare == 0)
					return 1;
				else
					return compare;
			}
		};
		Map<K, V> sortedByValues = new TreeMap<K, V>(valueComparator);
		sortedByValues.putAll(map);
		return sortedByValues;
	}

	public static HKSHistoricalDataBean parseVDSCHistorycalData(Long rawTime, String dateTime, String data) {
		HKSHistoricalDataBean hisBean = new HKSHistoricalDataBean();

		try {
			String[] lvTempMarketDataArray = StringSplitter.split(data, "\\|");
			hisBean.setMvTime(lvTempMarketDataArray[1]);
			hisBean.setMvRawTime(DateUtil.convertStringToDate("HH:mm:ss", lvTempMarketDataArray[1]).getTime());
			hisBean.setMvMatchPrice(MarketDataUtils.formatPrice(lvTempMarketDataArray[2]));
			hisBean.setMvMatchQty(new BigDecimal(MarketDataUtils.parseDouble(lvTempMarketDataArray[3])).setScale(3, 4));
			hisBean.setMvTotalQty(MarketDataUtils.formatVolume(lvTempMarketDataArray[4]));
		} catch (Exception e) {
			Log.println("Exception in parseHistorycalData " + e, Log.DEBUG_LOG);
			Log.println("Print detail:" + data, Log.DEBUG_LOG);
		}

		return hisBean;
	}

	public static HKSMarketDataBean HBBSStockPriceInfoParsing(String[] pMarketDataArray) {
		HKSMarketDataBean mvMarketDataBean = new HKSMarketDataBean();
		mvMarketDataBean.setMvMarketID(pMarketDataArray[0]);
		String lvMarketID = pMarketDataArray[0];
		String lvSymbol = pMarketDataArray[1].toUpperCase();
		mvMarketDataBean.setMvSymbol(lvSymbol);

		// Set Values Accordingly
		Double lvCeilingPrice = MarketDataUtils.parseDouble(pMarketDataArray[8]);
		Double lvFloorPrice = MarketDataUtils.parseDouble(pMarketDataArray[9]);
		Double lvReferencePrice = MarketDataUtils.parseDouble(pMarketDataArray[3]);
		Double lvNominalPrice = new Double(0);

		// Get lvNominalPrice from HistMap
		if (IMain.stockHistMap.containsKey(lvSymbol)) {
			long lastKey = IMain.stockHistMap.get(lvSymbol).lastKey();
			String value = IMain.stockHistMap.get(lvSymbol).get(lastKey);
			String[] lastObject = value.split("\\|");
			lvNominalPrice = MarketDataUtils.parseDouble(lastObject[2]);
		} else {
			// in case no history
			lvNominalPrice = MarketDataUtils.parseDouble(pMarketDataArray[13]); // same as match price
		}

		// Correct lvReferencePrice in case not exist in stockHistMap and market is HO
		if (!IMain.stockHistMap.containsKey(lvSymbol) && HKSTag.HOSE_EXCHANGE_CODE.equalsIgnoreCase(lvMarketID)) {
			HKSInstrument lvHKSInstrument = (HKSInstrument) TPManager.mvInstrumentVector.get(lvMarketID.concat("|").concat(lvSymbol));
			if (lvHKSInstrument != null) {
				lvReferencePrice = MarketDataUtils.parseDouble(lvHKSInstrument.getMvClosingPrice());
			}
		}
		// For CSS code
		PriceCss priceCss = new PriceCss(lvReferencePrice, lvCeilingPrice, lvFloorPrice);

		mvMarketDataBean.setMvReferencePrice(MarketDataUtils.formatPrice(lvReferencePrice.toString()));
		mvMarketDataBean.setMvNominalPrice(MarketDataUtils.formatPrice(lvNominalPrice.toString()));
		mvMarketDataBean.setMvNoalPriCss(priceCss.getCode(lvNominalPrice));

		mvMarketDataBean.setMvNomCmToRefPri(lvNominalPrice.compareTo(lvReferencePrice));

		// setMvNoalPriSubRefPri
		setCompareNomRefPrice(mvMarketDataBean, lvReferencePrice, lvNominalPrice);

		// low / high values
		mvMarketDataBean.setMvHighPrice(MarketDataUtils.formatPrice(pMarketDataArray[4]));
		mvMarketDataBean.setMvHighPriCss(priceCss.getCode(pMarketDataArray[4]));
		mvMarketDataBean.setMvLowPrice(MarketDataUtils.formatPrice(pMarketDataArray[5]));
		mvMarketDataBean.setMvLowPriCss(priceCss.getCode(pMarketDataArray[5]));

		mvMarketDataBean.setMvOpenPrice(MarketDataUtils.formatPrice(pMarketDataArray[6]));
		mvMarketDataBean.setMvOpenPriCss(priceCss.getCode(pMarketDataArray[6]));

		mvMarketDataBean.setMvClosePrice(MarketDataUtils.formatPrice(pMarketDataArray[7]));
		mvMarketDataBean.setMvCeilingPrice(MarketDataUtils.formatPrice(pMarketDataArray[8]));
		mvMarketDataBean.setMvFloorPrice(MarketDataUtils.formatPrice(pMarketDataArray[9]));

		// fix for totalVol, get totalTradingQty as replacement
		Double lvTotalVol = MarketDataUtils.parseDouble(pMarketDataArray[10]);
		if(lvTotalVol == 0){
			lvTotalVol = MarketDataUtils.parseDouble(pMarketDataArray[15]); // same as TotalTradingQty
		}
		mvMarketDataBean.setMvTotalVol(MarketDataUtils.formatPrice(lvTotalVol.toString()));
		mvMarketDataBean.setMvTalVolCss(priceCss.getCode(lvNominalPrice)); // have same color as nominal price

		mvMarketDataBean.setMvMatchPrice(MarketDataUtils.formatPrice(pMarketDataArray[13]));
		mvMarketDataBean.setMvMatchQty(MarketDataUtils.formatVolume(pMarketDataArray[14]));
		
		mvMarketDataBean.setMvTotalTradingQty(MarketDataUtils.formatVolume(pMarketDataArray[15]));
		mvMarketDataBean.setMvTotalTradingValue(MarketDataUtils.formatVolume(pMarketDataArray[16])); // money but format without fraction
		
		mvMarketDataBean.setMvBuyForeignQty(MarketDataUtils.formatVolume(pMarketDataArray[17]));
		mvMarketDataBean.setMvCurrentRoom(MarketDataUtils.formatVolume(pMarketDataArray[18]));
		mvMarketDataBean.setMvSellForeignQty(MarketDataUtils.formatVolume(pMarketDataArray[19]));

		mvMarketDataBean.setAvgPrice(MarketDataUtils.formatPrice(pMarketDataArray[21]));
		mvMarketDataBean.setAvgPriceCss(priceCss.getCode(pMarketDataArray[21]));
		
		// Change the way to get bid/ask price
		String bidAsk = IMain.mvBestPrice.get((lvMarketID + "|" + lvSymbol).toUpperCase());
		if (bidAsk != null) {
			String[] bidAskArray = bidAsk.split("\\|");
			String[] lvTempBid = bidAskArray[0].split(",");
			String[] lvTempOffer = bidAskArray[1].split(",");

			// Bid 1st
			if (lvTempBid.length >= 2) {
				mvMarketDataBean.setMvBestBid1Css(priceCss.getCode(lvTempBid[0]));
				mvMarketDataBean.setMvBestBid1Price(MarketDataUtils.formatPrice(lvTempBid[0]));
				mvMarketDataBean.setMvBestBid1Volume(MarketDataUtils.formatVolume(lvTempBid[1]));
				// Corrective for HA - ATC price
				if (lvMarketID.equals("HA") && MarketDataUtils.parseDouble(lvTempBid[1]) > 0
						&& MarketDataUtils.parseDouble(lvTempBid[0]) == 0) {
					mvMarketDataBean.setMvBestBid1Price("ATC");
					mvMarketDataBean.setMvBestBid1Css(priceCss.getCode("ATC"));					
				}
			}
			// Bid 2nd
			if (lvTempBid.length >= 4) {
				mvMarketDataBean.setMvBestBid2Css(priceCss.getCode(lvTempBid[2]));
				mvMarketDataBean.setMvBestBid2Price(MarketDataUtils.formatPrice(lvTempBid[2]));
				mvMarketDataBean.setMvBestBid2Volume(MarketDataUtils.formatVolume(lvTempBid[3]));
			}
			// Bid 3rd
			if (lvTempBid.length >= 6) {
				mvMarketDataBean.setMvBestBid3Css(priceCss.getCode(lvTempBid[4]));
				mvMarketDataBean.setMvBestBid3Price(MarketDataUtils.formatPrice(lvTempBid[4]));
				mvMarketDataBean.setMvBestBid3Volume(MarketDataUtils.formatVolume(lvTempBid[5]));
			}
			// Offer 1st
			if (lvTempOffer.length >= 2) {
				mvMarketDataBean.setMvBestOffer1Css(priceCss.getCode(lvTempOffer[0]));
				mvMarketDataBean.setMvBestOffer1Price(MarketDataUtils.formatPrice(lvTempOffer[0]));
				mvMarketDataBean.setMvBestOffer1Volume(MarketDataUtils.formatVolume(lvTempOffer[1]));
				// Corrective for HA - ATC price
				if (lvMarketID.equals("HA") && MarketDataUtils.parseDouble(lvTempOffer[1]) > 0
						&& MarketDataUtils.parseDouble(lvTempOffer[0]) == 0) {
					mvMarketDataBean.setMvBestOffer1Price("ATC");
					mvMarketDataBean.setMvBestOffer1Css(priceCss.getCode("ATC"));
				}
			}
			// Offer 2nd
			if (lvTempOffer.length >= 4) {
				mvMarketDataBean.setMvBestOffer2Css(priceCss.getCode(lvTempOffer[2]));
				mvMarketDataBean.setMvBestOffer2Price(MarketDataUtils.formatPrice(lvTempOffer[2]));
				mvMarketDataBean.setMvBestOffer2Volume(MarketDataUtils.formatVolume(lvTempOffer[3]));
			}
			// Offer 3rd
			if (lvTempOffer.length >= 6) {
				mvMarketDataBean.setMvBestOffer3Css(priceCss.getCode(lvTempOffer[4]));
				mvMarketDataBean.setMvBestOffer3Price(MarketDataUtils.formatPrice(lvTempOffer[4]));
				mvMarketDataBean.setMvBestOffer3Volume(MarketDataUtils.formatVolume(lvTempOffer[5]));
			}
		}

		return mvMarketDataBean;
	}

	/**
	 * @param mvMarketDataBean
	 * @param lvReferencePrice
	 * @param lvNominalPrice
	 */
	public static void setCompareNomRefPrice(HKSMarketDataBean mvMarketDataBean, Double lvReferencePrice, Double lvNominalPrice) {
		Double lvNoalPriSubRefPri = new Double(0);
		if (lvNominalPrice > 0 && lvReferencePrice > 0) {
			lvNoalPriSubRefPri = lvNominalPrice - lvReferencePrice;
		}
		if (lvNoalPriSubRefPri.doubleValue() != 0) {
			mvMarketDataBean.setMvNoalPriSubRefPri(new BigDecimal(lvNoalPriSubRefPri).setScale(1, BigDecimal.ROUND_HALF_UP).toString());
			mvMarketDataBean.setMvNoalPriPerRate(new BigDecimal(lvNoalPriSubRefPri * 100 / lvReferencePrice).setScale(1,
					BigDecimal.ROUND_HALF_UP).toString());
		} else {
			mvMarketDataBean.setMvNoalPriSubRefPri("-");
			mvMarketDataBean.setMvNoalPriPerRate("-");
		}
		// for CSS, do not use the common common function
		if (lvNominalPrice.compareTo(lvReferencePrice) == 0) {
			mvMarketDataBean.setMvNoalPriSubRefPriCss(1);
			mvMarketDataBean.setMvNoalPriPerRateCss(1);
		} else if (lvNominalPrice.compareTo(lvReferencePrice) > 0) {
			mvMarketDataBean.setMvNoalPriSubRefPriCss(2);
			mvMarketDataBean.setMvNoalPriPerRateCss(2);
		} else {
			mvMarketDataBean.setMvNoalPriSubRefPriCss(3);
			mvMarketDataBean.setMvNoalPriPerRateCss(3);
		}
	}

	public static HKSMarketDataBean StockInfoParsing(StockInfoModel stockInfo, HistoricalDataModel histData, BestPriceModel bestPrice) {
		HKSMarketDataBean mvMarketDataBean = new HKSMarketDataBean();

		String lvMarketID = stockInfo.getMarketID();
		String lvSymbol = stockInfo.getStockCode();
		mvMarketDataBean.setMvMarketID(lvMarketID);
		mvMarketDataBean.setMvSymbol(lvSymbol);

		// Set Values Accordingly
		Double lvOpeningPrice = new Double(0);
		HKSInstrument lvHKSInstrument = (HKSInstrument) TPManager.mvInstrumentVector.get(lvMarketID + "|" + lvSymbol.toUpperCase());
		if (lvHKSInstrument != null && lvHKSInstrument.getMvOpenPrice() != null) {
			lvOpeningPrice = MarketDataUtils.parseDouble(lvHKSInstrument.getMvOpenPrice());
		} else {
			lvOpeningPrice = MarketDataUtils.parseDouble(stockInfo.getOpenPrice());
		}

		Double lvNominalPrice = new Double(0);
		Double lvReferencePrice = new Double(0);

		if (histData != null && !histData.getMatchedPrice().isEmpty()) {
			lvNominalPrice = MarketDataUtils.parseDouble(histData.getMatchedPrice());
			lvReferencePrice = MarketDataUtils.parseDouble(stockInfo.getReferencePrice());
		} else {
			lvNominalPrice = MarketDataUtils.parseDouble(stockInfo.getMatchPrice());
			if (HKSTag.HOSE_EXCHANGE_CODE.equalsIgnoreCase(lvMarketID)) {
				if (lvHKSInstrument != null) {
					lvReferencePrice = MarketDataUtils.parseDouble(lvHKSInstrument.getMvClosingPrice());
				}
			} else {
				lvReferencePrice = MarketDataUtils.parseDouble(stockInfo.getReferencePrice());
			}
		}
		mvMarketDataBean.setMvReferencePrice(MarketDataUtils.formatPrice(lvReferencePrice.toString()));

		mvMarketDataBean.setMvMatchPrice(MarketDataUtils.formatPrice(stockInfo.getMatchPrice()));
		mvMarketDataBean.setMvMatchQty(MarketDataUtils.formatVolume(stockInfo.getMatchQty()));
		mvMarketDataBean.setMvTotalTradingQty(MarketDataUtils.formatVolume(stockInfo.getTotalMatchQty()));
		mvMarketDataBean.setMvTotalTradingValue(MarketDataUtils.formatVolume(stockInfo.getTotalMatchValue()));
		mvMarketDataBean.setMvBuyForeignQty(MarketDataUtils.formatVolume(stockInfo.getBuyForeignQty()));
		mvMarketDataBean.setMvCurrentRoom(MarketDataUtils.formatVolume(stockInfo.getCurrentRoom()));
		mvMarketDataBean.setMvSellForeignQty(MarketDataUtils.formatVolume(stockInfo.getSellForeignQty()));
		mvMarketDataBean.setMvClosePrice(MarketDataUtils.formatPrice(stockInfo.getClosePrice()));

		// For CSS code
		PriceCss priceCss = new PriceCss(lvReferencePrice, MarketDataUtils.parseDouble(stockInfo.getCeilingPrice()),
				MarketDataUtils.parseDouble(stockInfo.getFloorPrice()));

		mvMarketDataBean.setMvNomCmToRefPri(lvNominalPrice.compareTo(lvReferencePrice));
		mvMarketDataBean.setMvCeilingPrice(MarketDataUtils.formatPrice(stockInfo.getCeilingPrice()));
		mvMarketDataBean.setMvFloorPrice(MarketDataUtils.formatPrice(stockInfo.getFloorPrice()));

		if (bestPrice != null) {
			mvMarketDataBean.setMvBestBid1Price(MarketDataUtils.formatPrice(bestPrice.getBestBid1Price()));
			mvMarketDataBean.setMvBestBid1Volume(MarketDataUtils.formatVolume(bestPrice.getBestBid1Qty()));
			mvMarketDataBean.setMvBestBid1Css(priceCss.getCode(bestPrice.getBestBid1Price()));

			mvMarketDataBean.setMvBestBid2Price(MarketDataUtils.formatPrice(bestPrice.getBestBid2Price()));
			mvMarketDataBean.setMvBestBid2Volume(MarketDataUtils.formatVolume(bestPrice.getBestBid2Qty()));
			mvMarketDataBean.setMvBestBid2Css(priceCss.getCode(bestPrice.getBestBid2Price()));

			mvMarketDataBean.setMvBestBid3Price(MarketDataUtils.formatPrice(bestPrice.getBestBid3Price()));
			mvMarketDataBean.setMvBestBid3Volume(MarketDataUtils.formatVolume(bestPrice.getBestBid3Qty()));
			mvMarketDataBean.setMvBestBid3Css(priceCss.getCode(bestPrice.getBestBid3Price()));

			mvMarketDataBean.setMvBestOffer1Price(MarketDataUtils.formatPrice(bestPrice.getBestAsk1Price()));
			mvMarketDataBean.setMvBestOffer1Volume(MarketDataUtils.formatVolume(bestPrice.getBestAsk1Qty()));
			mvMarketDataBean.setMvBestOffer1Css(priceCss.getCode(bestPrice.getBestAsk1Price()));

			mvMarketDataBean.setMvBestOffer2Price(MarketDataUtils.formatPrice(bestPrice.getBestAsk2Price()));
			mvMarketDataBean.setMvBestOffer2Volume(MarketDataUtils.formatVolume(bestPrice.getBestAsk2Qty()));
			mvMarketDataBean.setMvBestOffer2Css(priceCss.getCode(bestPrice.getBestAsk2Price()));

			mvMarketDataBean.setMvBestOffer3Price(MarketDataUtils.formatPrice(bestPrice.getBestAsk3Price()));
			mvMarketDataBean.setMvBestOffer3Volume(MarketDataUtils.formatVolume(bestPrice.getBestAsk3Qty()));
			mvMarketDataBean.setMvBestOffer3Css(priceCss.getCode(bestPrice.getBestAsk3Price()));
		}

		mvMarketDataBean.setMvNominalPrice(MarketDataUtils.formatPrice(lvNominalPrice.toString()));
		mvMarketDataBean.setMvNoalPriCss(priceCss.getCode(lvNominalPrice));
		mvMarketDataBean.setMvMatchQtyCss(priceCss.getCode(lvNominalPrice));

		mvMarketDataBean.setMvTotalVol(MarketDataUtils.formatVolume(stockInfo.getTotalMatchQty()));
		mvMarketDataBean.setMvTalVolCss(priceCss.getCode(lvNominalPrice));

		// setMvNoalPriSubRefPri
		setCompareNomRefPrice(mvMarketDataBean, lvReferencePrice, lvNominalPrice);

		mvMarketDataBean.setMvOpenPrice(MarketDataUtils.formatPrice(lvOpeningPrice.toString()));
		mvMarketDataBean.setMvOpenPriCss(priceCss.getCode(lvOpeningPrice));

		// Only set high/low price when have date from matched deals VDSC Map
		if (histData != null) {
			mvMarketDataBean.setMvHighPrice(MarketDataUtils.formatPrice(stockInfo.getHighestPrice()));
			mvMarketDataBean.setMvHighPriCss(priceCss.getCode(stockInfo.getHighestPrice()));
			mvMarketDataBean.setMvLowPrice(MarketDataUtils.formatPrice(stockInfo.getLowestPrice()));
			mvMarketDataBean.setMvLowPriCss(priceCss.getCode(stockInfo.getLowestPrice()));
		}

		Date tradingDate = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			tradingDate = sdf.parse(stockInfo.getTradingDate());
		} catch (Exception ex) {
			tradingDate = new Date();
		}
		mvMarketDataBean.setLstUpdate(tradingDate);

		if (histData != null && !histData.getMatchedPrice().isEmpty() && !histData.getVol().isEmpty()) {
			mvMarketDataBean.setMvMatchQty(new DisplayNumber(histData.getVol().toString()).toString());
		}
		return mvMarketDataBean;
	}
}
