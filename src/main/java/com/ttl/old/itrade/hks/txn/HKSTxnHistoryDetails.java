package com.ttl.old.itrade.hks.txn;

/**
 * <p>Title: Details for Transaction History Enquiry</p>
 * <p>Description: This class stores the details for Transaction History Enquiry</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

import java.util.Hashtable;
/**
 * This class the HKSTransactionHistoryTxn class entity
 * @author not attributable
 *
 */
public class HKSTxnHistoryDetails
{

//	private String		mvOrderId;
//	private String		seriesId;
//	private String		product;
//	private String		contractMonth;
	private String      mvClientId;
	private String      mvTradeDate;
	private String      mvType;
//	private String      mvRelatedAC;
	private String      mvDesc;
	private String      mvName;
	private String      mvCShortName;
        private String      mvMarketId;
	private String		mvStockId;
	private String      mvQty;
	private String      mvAvgPrice;
	private String      mvAmt;
	// BEGIN TASK: CL00049 Charlie Lau 20081124
	private String      mvId;
	private String      mvSettleDate;
	private String      mvTotalFee;
	// END TASK: CL00049
//	private String		mvBs;
//	private String		mvPrice;
//	private String		mvQuantity;
//	private String		validity;
//	private String		mvValidityDate;
//	private String		ol;
//	private String		ao;
//	private String		mvStop;
//	private String		mvSPrice;
//	private String		remarks;
//	private String		mvCreateTime;

//	added by May on 18-12-2003
	private String		mvShortTime;

	private Hashtable   mvHMap;
	/**
	 * Get method for client id
	 * @return client id
	 */
	public String getClientId()
	{
		return mvClientId;
	}
	/**
	 * Set method for client id
	 * @param pClientId the client id
	 */
	public void setClientId(String pClientId)
	{
		mvClientId = pClientId;
	}
	/**
	 * Get method for trade date
	 * @return trade date
	 */
	public String getTradeDate()
	{
		return mvTradeDate;
	}
	/**
	 * Set method for trade date
	 * @param pTradeDate the trade date
	 */
	public void setTradeDate(String pTradeDate)
	{
		mvTradeDate = pTradeDate;
	}
	/**
	 * Get method for type
	 * @return type
	 */
	public String getType()
	{
		return mvType;
	}
	/**
	 * Set method for type
	 * @param pType the type
	 */
	public void setType(String pType)
	{
		mvType = pType;
	}
	/**
	 * Get method for desc
	 * @return desc
	 */
	public String getDesc()
	{
		return mvDesc;
	}
	/**
	 * Set method for desc
	 * @param pDesc the desc
	 */
	public void setDesc(String pDesc)
	{
		mvDesc = pDesc;
	}
	/**
	 * Get method for name
	 * @return name
	 */
	public String getName()
	{
		return mvName;
	}
	/**
	 * Set method for name
	 * @param pName thename
	 */
	public void setName(String pName)
	{
		mvName = pName;
	}
	/**
	 * Get method for client short name
	 * @return client short name
	 */
	public String getCShortName()
	{
		return mvCShortName;
	}
	/**
	 * Set method for client short name
	 * @param pCShortName the client short name
	 */
	public void setCShortName(String pCShortName)
	{
		mvCShortName = pCShortName;
	}
	/**
	 * Get method for market id
	 * @return market id
	 */
        public String getMarketId()
        {
                return mvMarketId;
        }
        /**
         * Set method for market id
         * @param pMarketId the market id
         */
        public void setMarketId(String pMarketId)
        {
                mvMarketId = pMarketId;
        }

    /**
     * Get method for stock id
     * @return stock id
     */
	public String getStockId()
	{
		return mvStockId;
	}
	/**
	 * Set method for stock id
	 * @param pStockId the stock id
	 */
	public void setStockId(String pStockId)
	{
		mvStockId = pStockId;
	}
	/**
	 * Get method for quantity
	 * @return quantity
	 */
	public String getQty()
	{
		return mvQty;
	}
	/**
	 * Set method for quantity
	 * @param pQty the quantity
	 */
	public void setQty(String pQty)
	{
		mvQty = pQty;
	}
	/**
	 * Get method for AVG price
	 * @return AVG price
	 */
	public String getAvgPrice()
	{
		return mvAvgPrice;
	}
	/**
	 * Set method for AVG price
	 * @param pAvgPrice the AVG price
	 */
	public void setAvgPrice(String pAvgPrice)
	{
		mvAvgPrice = pAvgPrice;
	}
	/**
	 * Get method for amount
	 * @return amount
	 */
	public String getAmt()
	{
		return mvAmt;
	}
	/**
	 * Set method for amount
	 * @param pAmt the amount
	 */
	public void setAmt(String pAmt)
	{
		mvAmt = pAmt;
	}
	
	// BEGIN TASK: CL00049 Charlie Lau 20081230
	/**
	 * Set method for RefId
	 * @param pId the RefId
	 */
	public void setRefId(String pId)
	{
		mvId = pId;
	}
	/**
	 * Get method for RefId
	 * @return RefId
	 */
	public String getRefId()
	{
		return mvId;
	}
	/**
	 * Set method for settle date
	 * @param pSettleDate the settle date
	 */
	public void setSettleDate(String pSettleDate)
	{
		mvSettleDate = pSettleDate;
	}
	/**
	 * Get method for settle date
	 * @return settle date
	 */
	public String getSettleDate()
	{
		return mvSettleDate;
	}
	/**
	 * Get method for Txn type
	 * @return Txn type
	 */
	public String getTxnType()
	{
		return mvType;
	}
	/**
	 * Set method for Txn type
	 * @param pTxnType the Txn type
	 */
	public void setTxnType(String pTxnType)
	{
		mvType = pTxnType;
	}
	// END TASK: CL00062

//	public String getProduct()
//	{
//		return product;
//	}
//
//	public void setProduct(String product)
//	{
//		this.product = product;
//	}
//
//
//	public String getContractMonth()
//	{
//		return contractMonth;
//	}
//
//	public void setContractMonth(String contractMonth)
//	{
//		this.contractMonth = contractMonth;
//	}
//
//	public String getValidity()
//	{
//		return validity;
//	}
//
//	public void setValidity(String validity)
//	{
//		this.validity = validity;
//	}
//
//	public String getAO()
//	{
//		return ao;
//	}
//
//	public void setAO(String ao)
//	{
//		this.ao = ao;
//	}
//
//	public String getRemarks()
//	{
//		return remarks;
//	}
//
//	public void setRemarks(String remarks)
//	{
//		this.remarks = remarks;
//	}
//
//	public String getCreateTime()
//	{
//		return mvCreateTime;
//	}
//
///***	added by May on 18-12-2003	***/
//	public String getShortTime()
//	{
//		return mvShortTime;
//	}
///******************************************/
//
//	public void setCreateTime(String pCreateTime)
//	{
//		mvCreateTime = pCreateTime;
//	}
//
///***	added by May on 18-12-2003	***/
//	public void setShortTime(String pShortTime)
//	{
//		mvShortTime = pShortTime;
//	}
///******************************************/
//
	/**
	 * Get method for Hashtable
	 * @return Hashtable
	 */
	public Hashtable getHashtable()
	{
		return mvHMap;
	}
	/**
	 * Set method for Hashtable
	 * @param pHMap the Hashtable
	 */
	public void setHashtable(Hashtable pHMap)
	{
		mvHMap = pHMap;
	}
	/**
	 * @param mvTotalFee the mvTotalFee to set
	 */
	public void setMvTotalFee(String mvTotalFee) {
		this.mvTotalFee = mvTotalFee;
	}
	/**
	 * @return the mvTotalFee
	 */
	public String getMvTotalFee() {
		return mvTotalFee;
	}
}
