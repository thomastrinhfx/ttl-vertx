package com.ttl.old.itrade.hks.bean;

/**
 * The HKSCancelOrderFailBean class define variables that to save values 
 * for action 
 * @author Wind Zhao
 *
 */
public class HKSCancelOrderFailBean {
	private String mvOrderId;
	private String mvDateTime;
	private String mvClientId;
	private String mvInstrumentId;
	private String mvInstrumentName;
	private String mvPrice;
	private String mvPriceValue;
	private String mvQuantityDescription;
	private String mvCurrencyId;
	private String mvOSQty;
	private String mvOSQtyVaule;
	private String mvFilledQty;
	private String mvMarketId;
	private String mvReasonMessage;
	private String mvMultiMarketDisable;
	private String mvBSValue;
	private String mvBuyOrSell;
	private String mvQuantityValue;
	private String mvGoodTillDateValue;
	private String mvOrderGroupIdValue;
	private String mvOrderIdValue;
	private String mvOrderTypeValue;
	//Begin Task #: TTL-GZ-ZZW-00011 Wind Zhao 20091118 for [ITradeR5 VN]Allow to modifycancel orders like ROSE's version
    
	/*
     * mvCancelOrderFor to control hidden or show some fields in 
     * cancel order
     */
    private String mvCancelOrderFor;
    
    /*
     * mvQuantity is show in cancel order
     */
    private String mvQuantity;
    
    /**
     * This method returns the quantity.
     * @return the quantity of the order.
     */
    public String getMvQuantity() {
		return mvQuantity;
	}
    
    /**
     * This method sets the quantity.
     * @param pQuantity The quantity of the order.
     */
	public void setMvQuantity(String pQuantity) {
		mvQuantity = pQuantity;
	}
	
	/**
     * This method returns the symbol
     * to control show or hidden some fields.
     * @return the cancel order for value.
     */
	public String getMvCancelOrderFor() {
    	return mvCancelOrderFor;
    }
	
	/**
     * This method sets the a symbol
     * to control show or hidden some fields.
     * @param pCancelOrderFor The symbol to control show or hidden some fields.
     */
    public void setMvCancelOrderFor(String pCancelOrderFor) {
    	mvCancelOrderFor = pCancelOrderFor;
    }
    //End Task #: TTL-GZ-ZZW-00011 Wind Zhao 20091118 for [ITradeR5 VN]Allow to modifycancel orders like ROSE's version
   
    /**
     * This method returns the order id.
     * @return the order id of the order.
     */
	public String getMvOrderId() {
		return mvOrderId;
	}
	
	/**
     * This method sets the order id.
     * @param pOrderId The order id.
     */
	public void setMvOrderId(String pOrderId) {
		mvOrderId = pOrderId;
	}
	
	/**
     * This method returns the date time.
     * @return the date time of the order.
     */
	public String getMvDateTime() {
		return mvDateTime;
	}
	
	/**
     * This method sets the date time.
     * @param pDateTime The date time of the order.
     */
	public void setMvDateTime(String pDateTime) {
		mvDateTime = pDateTime;
	}
	
	/**
     * This method returns the client id.
     * @return the client id.
     */
	public String getMvClientId() {
		return mvClientId;
	}
	
	/**
     * This method sets client id.
     * @param pClientId The client id.
     */
	public void setMvClientId(String pClientId) {
		mvClientId = pClientId;
	}
	
	/**
     * This method returns the instrument id.
     * @return the instrument id of the order.
     */
	public String getMvInstrumentId() {
		return mvInstrumentId;
	}
	
	/**
     * This method sets the instrument id.
     * @param pInstrumentId The instrument id of the order.
     */
	public void setMvInstrumentId(String pInstrumentId) {
		mvInstrumentId = pInstrumentId;
	}
	
	/**
     * This method returns the instrument name.
     * @return the instrument name of the order.
     */
	public String getMvInstrumentName() {
		return mvInstrumentName;
	}
	
	/**
     * This method sets the instrument name.
     * @param pInstrumentName The instrument name of the order.
     */
	public void setMvInstrumentName(String pInstrumentName) {
		mvInstrumentName = pInstrumentName;
	}
	
	/**
     * This method returns the price.
     * @return the price of the order is formatted.
     */
	public String getMvPrice() {
		return mvPrice;
	}
	
	/**
     * This method sets the price.
     * @param pPrice The price of the order is formatted.
     */
	public void setMvPrice(String pPrice) {
		mvPrice = pPrice;
	}
	
	/**
     * This method returns the price.
     * @return the price of the order.
     */
	public String getMvPriceValue() {
		return mvPriceValue;
	}
	
	/**
     * This method sets the price.
     * @param pPriceValue The price of the order.
     */
	public void setMvPriceValue(String pPriceValue) {
		mvPriceValue = pPriceValue;
	}
	
	/**
     * This method returns the quantity description.
     * @return the quantity description of the order.
     */
	public String getMvQuantityDescription() {
		return mvQuantityDescription;
	}
	
	/**
     * This method sets the quantity description.
     * @param pQuantityDescription The quantity description of the order.
     */
	public void setMvQuantityDescription(String pQuantityDescription) {
		mvQuantityDescription = pQuantityDescription;
	}
	
	/**
     * This method returns the currency id.
     * @return the currency id of the order.
     */
	public String getMvCurrencyId() {
		return mvCurrencyId;
	}
	
	/**
     * This method sets the currency id.
     * @param pCurrencyId The currency id of the order.
     */
	public void setMvCurrencyId(String pCurrencyId) {
		mvCurrencyId = pCurrencyId;
	}
	
	/**
     * This method returns the not clinch quantity.
     * @return the not clinch quantity of the order is formatted.
     */
	public String getMvOSQty() {
		return mvOSQty;
	}
	
	/**
     * This method sets the out standing quantity.
     * @param pOSQty The out standing quantity of the order is formatted.
     */
	public void setMvOSQty(String pOSQty) {
		mvOSQty = pOSQty;
	}
	
	/**
     * This method returns the out standing quantity value.
     * @return the out standing quantity value of the order is not formatted.
     */
	public String getMvOSQtyVaule() {
		return mvOSQtyVaule;
	}
	
	/**
     * This method sets the out standing quantity value.
     * @param pOSQtyVaule The out standing quantity value of the order is not formatted.
     */
	public void setMvOSQtyVaule(String pOSQtyVaule) {
		mvOSQtyVaule = pOSQtyVaule;
	}
	
	/**
     * This method returns the filled quantity.
     * @return the filled quantity of the order.
     */
	public String getMvFilledQty() {
		return mvFilledQty;
	}
	
	/**
     * This method sets the filled quantity.
     * @param  pFilledQty The filled quantity of the order.
     */
	public void setMvFilledQty(String pFilledQty) {
		mvFilledQty = pFilledQty;
	}
	
	/**
     * This method returns the market id.
     * @return the market id of the order.
     */
	public String getMvMarketId() {
		return mvMarketId;
	}
	
	/**
     * This method sets the market id.
     * @param pMarketId The market id of the order.
     */
	public void setMvMarketId(String pMarketId) {
		mvMarketId = pMarketId;
	}
	
	/**
     * This method returns the reason message.
     * @return the reason message of the order.
     */
	public String getMvReasonMessage() {
		return mvReasonMessage;
	}
	
	/**
     * This method sets the reason message.
     * @param pReasonMessage The reason message of the order.
     */
	public void setMvReasonMessage(String pReasonMessage) {
		mvReasonMessage = pReasonMessage;
	}
	
	/**
     * This method returns the multi market disable.
     * @return the multi market disable of the order.
     */
	public String getMvMultiMarketDisable() {
		return mvMultiMarketDisable;
	}
	
	/**
     * This method sets the multi market disable.
     * @param pMultiMarketDisable The multi market disable of the order.
     */
	public void setMvMultiMarketDisable(String pMultiMarketDisable) {
		mvMultiMarketDisable = pMultiMarketDisable;
	}
	
	/**
     * This method returns the B(Buy) or S(Sell).
     * @return the B(Buy) or S(sell) of the order.
     */
	public String getMvBSValue() {
		return mvBSValue;
	}
	
	/**
     * This method sets the B(Buy) or S(Sell).
     * @param pBSValue The B(Buy) or S(Sell) of the order.
     */
	public void setMvBSValue(String pBSValue) {
		mvBSValue = pBSValue;
	}
	
	/**
     * This method returns the buy or sell.
     * @return the buy or sell of the order.
     */
	public String getMvBuyOrSell() {
		return mvBuyOrSell;
	}
	
	/**
     * This method sets the buy or sell.
     * @param pBuyOrSell The buy or sell of the order.
     */
	public void setMvBuyOrSell(String pBuyOrSell) {
		mvBuyOrSell = pBuyOrSell;
	}
	
	/**
     * This method returns the quantity.
     * @return the quantity of the order is not formatted.
     */
	public String getMvQuantityValue() {
		return mvQuantityValue;
	}
	
	/**
     * This method sets the quantity.
     * @param pQuantityValue The quantity of the order is not formatted.
     */
	public void setMvQuantityValue(String pQuantityValue) {
		mvQuantityValue = pQuantityValue;
	}
	
	/**
     * This method returns the good till date.
     * @return the good till date of the order is not formatted.
     */
	public String getMvGoodTillDateValue() {
		return mvGoodTillDateValue;
	}
	
	/**
     * This method sets the good till date.
     * @param pGoodTillDateValue The good till date of the order is not formatted.
     */
	public void setMvGoodTillDateValue(String pGoodTillDateValue) {
		mvGoodTillDateValue = pGoodTillDateValue;
	}
	
	/**
     * This method returns the order group id.
     * @return the order group id of the order.
     */
	public String getMvOrderGroupIdValue() {
		return mvOrderGroupIdValue;
	}
	
	/**
     * This method sets the order group id.
     * @param pOrderGroupIdValue The order group id of the order.
     * @type String.
     */
	public void setMvOrderGroupIdValue(String pOrderGroupIdValue) {
		mvOrderGroupIdValue = pOrderGroupIdValue;
	}
	
	/**
     * This method returns the order id.
     * @return the order id of the order.
     */
	public String getMvOrderIdValue() {
		return mvOrderIdValue;
	}
	
	/**
     * This method sets the order id.
     * @param pOrderIdValue The order id of the order.
     */
	public void setMvOrderIdValue(String pOrderIdValue) {
		mvOrderIdValue = pOrderIdValue;
	}
	
	/**
     * This method returns the order type.
     * @return the order type of the order.
     */
	public String getMvOrderTypeValue() {
		return mvOrderTypeValue;
	}
	
	/**
     * This method sets the order type.
     * @param pOrderTypeValue The order type of the order.
     */
	public void setMvOrderTypeValue(String pOrderTypeValue) {
		mvOrderTypeValue = pOrderTypeValue;
	}
}