package com.ttl.old.itrade.hks.bean.plugin;

/**
 * The HKSBankInfoBean class define variables that to save values for action
 * 
 * 2013-11-04 DuongHN Add getMvSettlementAccountDisplayName and getMvSettlementAccountValue to replace HKSClientDetailsBean class
 * 
 * @author Wind.Zhao
 * 
 */

// Begin Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100122 [iTrade R5] Make Advance Payment a Plugin Module
public class HKSBankInfoBean {
	private String mvBankID;
	private String mvBankACID;
	private String mvBalance;
	private String mvSettlementAccountDisplayName;
	private boolean mvIsDefault;
	private String mvInterfaceSeq;

	/**
	 * Returns the bank id.
	 * 
	 * @return the bank id.
	 */
	public String getMvBankID() {
		return mvBankID;
	}

	/**
	 * Sets bank id.
	 * 
	 * @param pBankID the bank id.
	 */
	public void setMvBankID(String pBankID) {
		this.mvBankID = pBankID;
	}

	public void setMvBankACID(String mvBankACID) {
		this.mvBankACID = mvBankACID;
	}

	public String getMvBankACID() {
		return mvBankACID;
	}

	public void setMvBalance(String mvBalance) {
		this.mvBalance = mvBalance;
	}

	public String getMvBalance() {
		return mvBalance;
	}

	public String getMvSettlementAccountDisplayName() {
		return mvSettlementAccountDisplayName;
	}

	public void setMvSettlementAccountDisplayName(String mvSettlementAccountDisplayName) {
		this.mvSettlementAccountDisplayName = mvSettlementAccountDisplayName;
	}

	public String getMvSettlementAccountValue() {
		return mvBankACID;
	}

	/**
	 * @return the mvIsDefault
	 */
	public boolean isMvIsDefault() {
		return mvIsDefault;
	}

	/**
	 * @param mvIsDefault the mvIsDefault to set
	 */
	public void setMvIsDefault(boolean mvIsDefault) {
		this.mvIsDefault = mvIsDefault;
	}

	/**
	 * @return the mvInterfaceSeq
	 */
	public String getMvInterfaceSeq() {
		return mvInterfaceSeq;
	}

	/**
	 * @param mvInterfaceSeq the mvInterfaceSeq to set
	 */
	public void setMvInterfaceSeq(String mvInterfaceSeq) {
		this.mvInterfaceSeq = mvInterfaceSeq;
	}

	
}
// End Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100122 [iTrade R5] Make Advance Payment a Plugin Module