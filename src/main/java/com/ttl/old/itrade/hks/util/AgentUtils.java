package com.ttl.old.itrade.hks.util;

import java.util.HashMap;

import com.ttl.old.itrade.hks.txn.WebUtilsTxn;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.util.Log;

public class AgentUtils {
	public static final String FUNCTION_TYPE_TRANSFER = "transfer";
	public static final String FUNCTION_TYPE_ADVANCE = "advance";
	public static final String FUNCTION_TYPE_LOAN_REFUND = "loanRefund";
	public static final String FUNCTION_TYPE_INTEREST_POSTING = "interestPosting";
	public static final String FUNCTION_TYPE_MARGIN_EXTENSION = "marginExtension";
	public static HashMap<String, String> branchIDs = new HashMap<String, String>();	//key: ClientID; value: branchID
	public static HashMap<String, String> sAgents = null;
	
	public static void addBranchIDs(String clientID, String branchID){
		String tmp = branchIDs.get(clientID);
		if (tmp == null){
			branchIDs.put(clientID, branchID);
			Log.print("AgentUtils addBranchID: " + branchID, 1);
			Log.print("[HIEU LE] AgentUtils	addBranchIDs("+clientID+","+branchID+"): branchIDs= "+ branchIDs.toString(), 1);
		}
	}
	
	public static void initAgents(){
		{
			//init sAgents
			if (sAgents == null){
				sAgents = new HashMap<String, String>();
				
				//default agent
				String defaultAgent = IMain.getProperty("AgentID");
				String defaultEmail = IMain.getProperty("EMAIL");
				sAgents.put("AgentID", defaultAgent);		//default agent value
				sAgents.put("EMAIL", defaultEmail);			//default email value
				
				//more agents
				int iCount = 0;
				iCount = Integer.parseInt(IMain.getProperty("AgentCount"));
				if (iCount < 1) return;
				
				int i = 0;
				while (i < iCount){
					String keyAgent = "AgentID_" + i;
					String valueAgent = IMain.getProperty(keyAgent);
					String keyEmail = "EMAIL_" + i;
					String valueEmail = IMain.getProperty(keyEmail);
					String keyEmailTransfer = "EMAIL_" + i + "_" + FUNCTION_TYPE_TRANSFER.toUpperCase();
					String valueEmailTransfer = IMain.getProperty(keyEmailTransfer);
					String keyEmailAdvance = "EMAIL_" + i + "_" + FUNCTION_TYPE_ADVANCE.toUpperCase();
					String valueEmailAdvance = IMain.getProperty(keyEmailAdvance);
					String keyEmailLoanRefund = "EMAIL_" + i + "_" + FUNCTION_TYPE_LOAN_REFUND.toUpperCase();
					String valueEmailLoanRefund = IMain.getProperty(keyEmailLoanRefund);
					String keyEmailInterestPosting = "EMAIL_" + i + "_" + FUNCTION_TYPE_INTEREST_POSTING.toUpperCase();
					String valueEmailInterestPosting = IMain.getProperty(keyEmailInterestPosting);
					String keyEmailMarginExtension = "EMAIL_" + i + "_" + FUNCTION_TYPE_MARGIN_EXTENSION.toUpperCase();
					String valueEmailMarginExtension = IMain.getProperty(keyEmailMarginExtension);
					if (valueAgent != null && !"".equalsIgnoreCase(valueAgent)) {
						sAgents.put(keyAgent, valueAgent);
					} else {
						sAgents.put(keyAgent, defaultAgent);
					}
					if (valueEmail != null && !"".equalsIgnoreCase(valueEmail)){
						sAgents.put(keyEmail, valueEmail);
					} else {
						valueEmail = defaultEmail;
						sAgents.put(keyEmail, valueEmail);
					}
					//email for transfer
					if (valueEmailTransfer != null && !"".equalsIgnoreCase(valueEmailTransfer)){
						sAgents.put(keyEmailTransfer, valueEmailTransfer);
					} else {
						sAgents.put(keyEmailTransfer, valueEmail);
					}
					//email for Advance
					if (valueEmailAdvance != null && !"".equalsIgnoreCase(valueEmailAdvance)){
						sAgents.put(keyEmailAdvance, valueEmailAdvance);
					} else {
						sAgents.put(keyEmailAdvance, valueEmail);
					}
					//email for Loan refund
					if (valueEmailLoanRefund != null && !"".equalsIgnoreCase(valueEmailLoanRefund)){
						sAgents.put(keyEmailLoanRefund, valueEmailLoanRefund);
					} else {
						sAgents.put(keyEmailLoanRefund, valueEmail);
					}
					//email for Interest Posting
					if (valueEmailInterestPosting != null && !"".equalsIgnoreCase(valueEmailInterestPosting)){
						sAgents.put(keyEmailInterestPosting, valueEmailInterestPosting);
					} else {
						sAgents.put(keyEmailInterestPosting, valueEmail);
					}
					//email for Margin Extension
					if (valueEmailMarginExtension != null && !"".equalsIgnoreCase(valueEmailMarginExtension)){
						sAgents.put(keyEmailMarginExtension, valueEmailMarginExtension);
					} else {
						sAgents.put(keyEmailMarginExtension, valueEmail);
					}
					i++;
				}
				Log.print("[HIEU LE] first-time init: AgentUtils	initAgent: sAgents= "+ sAgents.toString(), 1);
			}
		}
	}
	
	public static String getBranchID(String clientId) {
		String branchID = "0";
		WebUtilsTxn wUtilsTxn = new WebUtilsTxn();
		branchID = wUtilsTxn.getBranchID(clientId);
		Log.print("[HIEU LE] AgentUtils	getBranchID("+clientId+"): branchID= "+ branchID, 1);
		return branchID;
	}
	
	public static String getAgentID(String clientID) {
		if (sAgents == null) initAgents();
		String branchID = branchIDs.get(clientID);
		String pOperatorID = sAgents.get("AgentID_" + branchID).toString().trim();
		Log.print("[HIEU LE] AgentUtils	getAgentID("+clientID+"): pOperatorID= "+ pOperatorID, 1);
		return pOperatorID;
	}
	
	public static String getOperatorEmails(String clientID, String functionType){
		if (sAgents == null) initAgents();
		String branchID = branchIDs.get(clientID);
		String emailDefault = sAgents.get("EMAIL_" + branchID).toString();
		String emailList = sAgents.get("EMAIL_" + branchID + "_" + functionType).toString();
		if (emailList == null || "".equalsIgnoreCase(emailList)) emailList = emailDefault;
		Log.print("[HIEU LE] AgentUtils	getOperatorEmails("+clientID+"," + functionType + "): emailList= "+ emailList, 1);
		return emailList;
	}
}
