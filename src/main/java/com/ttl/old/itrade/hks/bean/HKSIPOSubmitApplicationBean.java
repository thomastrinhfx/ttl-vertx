package com.ttl.old.itrade.hks.bean;

import java.math.BigDecimal;

/**
 * The HKSIPOSubmitApplicationBean class define variables that to save values 
 * for action 
 * @author
 *
 */
public class HKSIPOSubmitApplicationBean {

	private String mvFormattedStockCode;
    private String mvStockName;
    private String mvOfferPrice;
    private String mvFormattedQty;
    private String mvAmount;
    private String mvSubtotal;
	private String mvFormattedSubtotal;
    private String mvFormattedBankCharge;
    private String mvFormattedTotalAmt;
    private String mvTelephone;
    private String mvApplicationInputMethod;
    private String mvSMS;
    private String mvMobile;
    private String mvSMSLanguage;
    private String mvEMail;
    private String mvEntitlementID;
    private String mvStockCode;
    private String mvInputQty;
    private BigDecimal mvBankCharge;
    private BigDecimal mvTotalAmt;
    private String mvMaxOfferPrice;
    private String mvMinOfferPrice;
    private String mvFormattedMaxOfferPrice;
    private String mvErrorMsg;
    private String mvIsPasswordConfirmation;
    
    /**
     * This method returns the stock code.
     * @return the stock code is formatted.
     */
    public String getMvFormattedStockCode() {
		return mvFormattedStockCode;
	}
    
    /**
     * This method sets the stock code.
     * @param pFormattedStockCode The stock code is formatted.
     */
	public void setMvFormattedStockCode(String pFormattedStockCode) {
		mvFormattedStockCode = pFormattedStockCode;
	}
	
	/**
     * This method returns the stock name.
     * @return the stock name.
     */
	public String getMvStockName() {
		return mvStockName;
	}
	
	/**
     * This method sets the stock name.
     * @param pStockName The stock name.
     */
	public void setMvStockName(String pStockName) {
		mvStockName = pStockName;
	}
	
	/**
     * This method returns the offer price.
     * @return the offer price is formatted.
     */
	public String getMvOfferPrice() {
		return mvOfferPrice;
	}
	
	/**
     * This method sets the offer price.
     * @param pOfferPrice The offer price is formatted.
     */
	public void setMvOfferPrice(String pOfferPrice) {
		mvOfferPrice = pOfferPrice;
	}
	
	/**
     * This method returns the quantity.
     * @return the quantity is formatted.
     */
	public String getMvFormattedQty() {
		return mvFormattedQty;
	}
	
	/**
     * This method sets the quantity.
     * @param pFormattedQty The quantity is formatted.
     */
	public void setMvFormattedQty(String pFormattedQty) {
		mvFormattedQty = pFormattedQty;
	}
	
	/**
     * This method returns the amount.
     * @return the amount.
     */
	public String getMvAmount() {
		return mvAmount;
	}
	
	/**
     * This method sets the amount.
     * @param pAmount The amount.
     */
	public void setMvAmount(String pAmount) {
		mvAmount = pAmount;
	}
	
	/**
     * This method returns the sub total.
     * @return the sub total.
     */
	public String getMvSubtotal() {
		return mvSubtotal;
	}
	
	/**
     * This method sets the sub total.
     * @param pSubtotal The sub total.
     */
	public void setMvSubtotal(String pSubtotal) {
		mvSubtotal = pSubtotal;
	}
	
	/**
     * This method returns the sub total.
     * @return the sub total is formatted.
     */
	public String getMvFormattedSubtotal() {
		return mvFormattedSubtotal;
	}
	
	/**
     * This method sets the sub total.
     * @param pFormattedSubtotal The sub total is formatted.
     */
	public void setMvFormattedSubtotal(String pFormattedSubtotal) {
		mvFormattedSubtotal = pFormattedSubtotal;
	}
	
	/**
     * This method returns the bank charge.
     * @return the bank charge is formatted.
     */
	public String getMvFormattedBankCharge() {
		return mvFormattedBankCharge;
	}
	
	/**
     * This method sets the bank charge.
     * @param pFormattedBankCharge The bank charge is formatted.
     */
	public void setMvFormattedBankCharge(String pFormattedBankCharge) {
		mvFormattedBankCharge = pFormattedBankCharge;
	}
	
	/**
     * This method returns the total amount.
     * @return the total amount is formatted.
     */
	public String getMvFormattedTotalAmt() {
		return mvFormattedTotalAmt;
	}
	
	/**
     * This method sets the total amount.
     * @param pFormattedTotalAmt The total amount is formatted.
     */
	public void setMvFormattedTotalAmt(String pFormattedTotalAmt) {
		mvFormattedTotalAmt = pFormattedTotalAmt;
	}
	
	/**
     * This method returns the telephone.
     * @return the telephone.
     */
	public String getMvTelephone() {
		return mvTelephone;
	}
	
	/**
     * This method sets the telephone.
     * @param pTelephone The telephone.
     */
	public void setMvTelephone(String pTelephone) {
		mvTelephone = pTelephone;
	}
	
	/**
     * This method returns the application apply method.
     * @return the application apply method.
     */
	public String getMvApplicationInputMethod() {
		return mvApplicationInputMethod;
	}
	
	/**
     * This method sets the application apply method.
     * @param pApplicationInputMethod The application apply method.
     */
	public void setMvApplicationInputMethod(String pApplicationInputMethod) {
		mvApplicationInputMethod = pApplicationInputMethod;
	}
	
	/**
     * This method returns the SMS.
     * @return the SMS.
     */
	public String getMvSMS() {
		return mvSMS;
	}
	
	/**
     * This method sets the SMS.
     * @param pSMS The SMS.
     */
	public void setMvSMS(String pSMS) {
		mvSMS = pSMS;
	}
	
	/**
     * This method returns the mobile.
     * @return the mobile.
     */
	public String getMvMobile() {
		return mvMobile;
	}
	
	/**
     * This method sets the mobile.
     * @param pMobile The mobile.
     */
	public void setMvMobile(String pMobile) {
		mvMobile = pMobile;
	}
	
	/**
     * This method returns the SMS language.
     * @return the SMS language.
     */
	public String getMvSMSLanguage() {
		return mvSMSLanguage;
	}
	
	/**
     * This method sets the SMS language.
     * @param pSMSLanguage The SMS language.
     */
	public void setMvSMSLanguage(String pSMSLanguage) {
		mvSMSLanguage = pSMSLanguage;
	}
	
	/**
     * This method returns the email.
     * @return the email.
     */
	public String getMvEMail() {
		return mvEMail;
	}
	
	/**
     * This method sets the email.
     * @param pEmail The email.
     */
	public void setMvEMail(String pEMail) {
		mvEMail = pEMail;
	}
	
	/**
     * This method returns the entitlement id of margin apply.
     * @return the entitlement id of margin apply.
     */
	public String getMvEntitlementID() {
		return mvEntitlementID;
	}
	
	/**
     * This method sets the entitlement id of margin apply.
     * @param pEntitlementID The entitlement id of margin apply.
     */
	public void setMvEntitlementID(String pEntitlementID) {
		mvEntitlementID = pEntitlementID;
	}
	
	/**
     * This method returns the id of stock.
     * @return the id of stock.
     */
	public String getMvStockCode() {
		return mvStockCode;
	}
	
	/**
     * This method sets the id of stock.
     * @param pStockCode The id of stock.
     */
	public void setMvStockCode(String pStockCode) {
		mvStockCode = pStockCode;
	}
	
	/**
     * This method returns the input quantity.
     * @return the input quantity.
     */
	public String getMvInputQty() {
		return mvInputQty;
	}
	
	/**
     * This method sets the input quantity.
     * @param pInputQty The input quantity.
     */
	public void setMvInputQty(String pInputQty) {
		mvInputQty = pInputQty;
	}
	
	/**
     * This method returns the bank charge.
     * @return the bank charge.
     */
	public BigDecimal getMvBankCharge() {
		return mvBankCharge;
	}
	
	/**
     * This method sets the bank charge.
     * @param pBankCharge The bank charge.
     */
	public void setMvBankCharge(BigDecimal pBankCharge) {
		mvBankCharge = pBankCharge;
	}
	
	/**
     * This method returns the total amount.
     * @return the total amount.
     */
	public BigDecimal getMvTotalAmt() {
		return mvTotalAmt;
	}
	
	/**
     * This method sets the total amount.
     * @param pTotalAmt The total amount.
     */
	public void setMvTotalAmt(BigDecimal pTotalAmt) {
		mvTotalAmt = pTotalAmt;
	}
	
	/**
     * This method returns the max offer price.
     * @return the max offer price.
     */
	public String getMvMaxOfferPrice() {
		return mvMaxOfferPrice;
	}
	
	/**
     * This method sets the max offer price.
     * @param pMaxOfferPrice The max offer price.
     */
	public void setMvMaxOfferPrice(String pMaxOfferPrice) {
		mvMaxOfferPrice = pMaxOfferPrice;
	}
	
	/**
     * This method returns the min offer price.
     * @return the min offer price.
     */
	public String getMvMinOfferPrice() {
		return mvMinOfferPrice;
	}
	
	/**
     * This method sets the min offer price.
     * @param pMinOfferPrice The min offer price.
     */
	public void setMvMinOfferPrice(String pMinOfferPrice) {
		mvMinOfferPrice = pMinOfferPrice;
	}
	
	/**
     * This method returns the max offer price.
     * @return the max offer price is formatted.
     */
	public String getMvFormattedMaxOfferPrice() {
		return mvFormattedMaxOfferPrice;
	}
	
	/**
     * This method sets the max offer price.
     * @param pFormattedMaxOfferPrice The max offer price is formatted.
     */
	public void setMvFormattedMaxOfferPrice(String pFormattedMaxOfferPrice) {
		mvFormattedMaxOfferPrice = pFormattedMaxOfferPrice;
	}
	
	/**
     * This method returns the error message.
     * @return the error message.
     */
	public String getMvErrorMsg() {
		return mvErrorMsg;
	}
	
	/**
     * This method sets the error message
     * @param pErrorMsg The error message.
     */
	public void setMvErrorMsg(String pErrorMsg) {
		mvErrorMsg = pErrorMsg;
	}
	
	/**
     * This method returns the password confirmation is.
     * @return the error message.
     */
	public String getMvIsPasswordConfirmation() {
		return mvIsPasswordConfirmation;
	}
	
	/**
     * This method sets the error message
     * @param pIsPasswordConfirmation The error message.
     */
	public void setMvIsPasswordConfirmation(String pIsPasswordConfirmation) {
		mvIsPasswordConfirmation = pIsPasswordConfirmation;
	}
}
