package com.ttl.old.itrade.hks.txn.plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import com.ttl.old.itrade.hks.HKSTag;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.ErrorCode;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.tag.ITagXsfTagName;

/**
 * This HKSUIManagementTxn class defines methods that 
 * do save layout of each user
 * @author Khanh Nguyen
 * @since 20110512
 */
public class HKSUIManagementTxn extends BaseTxn
{
  
	/**
	 * 
	 */
	private static final long serialVersionUID = 4L;
	
	private static final String DEFAULT_EMPTY_WINDOWS = "{\"windows\":[]}";
	/**
	 * TODO success is not a standard naming
	 */
	private boolean success;
	private String mvGroupID;
	private String mvGroupName;
	private String mvGroupType;
	private String mvIsDefault;	
	private String mvSavedContent;
	private String mvClientID;
	private String mvAction;
	private List mvCfgList ;
	
	
	public HKSUIManagementTxn(String pClientId, String pAction )
    {    
    	super();
    	mvClientID = pClientId;
    	setMvAction(pAction);
    }   
	  
	
    public HKSUIManagementTxn(String pClientId, String pGroupID,  String pGroupName, String pGroupType, String pIsDefault, String pSavedContent, String pAction )
    {    
    	super();
    	mvClientID = pClientId;
    	mvGroupID = pGroupID;
    	mvGroupName = pGroupName ;
    	mvGroupType = pGroupType;
    	mvIsDefault = pIsDefault;
    	mvSavedContent = pSavedContent;
    	setMvAction(pAction);
    }   
   
    
	@SuppressWarnings("unchecked")
    public void process()
    {	
        Log.println("[ HKSUIManagementTxn._process() CLIENTID : " + getClientId()+ " ]", Log.ACCESS_LOG);
        try
        {
        	Hashtable lvTxnMap = new Hashtable();
            lvTxnMap.put(HKSTag.CLIENTID, getClientId());
            lvTxnMap.put(HKSTag.GROUPID, getMvGroupID()!= null ? getMvGroupID() : "");	
            lvTxnMap.put(HKSTag.GROUPNAME, getMvGroupName()!= null ? getMvGroupName() :  "");	
            lvTxnMap.put(HKSTag.GROUPTYPE, getMvGroupType() != null ?  getMvGroupType(): "U");	
            lvTxnMap.put(HKSTag.ISDEFAULT, getMvIsDefault()!= null ? getMvIsDefault() : "");	
            
            String savedContent = getMvSavedContent() != null && !"".equals(getMvSavedContent().trim())? getMvSavedContent() : DEFAULT_EMPTY_WINDOWS;
            lvTxnMap.put(HKSTag.SAVEDCONTENT, savedContent);	
            
            lvTxnMap.put("ACTION", getMvAction());	
            IMsgXMLNode lvErrorCodeNode;          
     
        	if (TPErrorHandling.TP_NORMAL != super.process( RequestName.HKSUIManagementRequest, lvTxnMap))
        	{
        		lvErrorCodeNode = mvReturnNode.getChildNode("C_ERROR_CODE");
    			if (lvErrorCodeNode == null || lvErrorCodeNode.getValue().equalsIgnoreCase("")) {
    				setErrorCode(new ErrorCode(new String[0], "SAVE_UI_ERROR", ""));
    			} else {
    				setErrorCode(new ErrorCode(new String[0], lvErrorCodeNode.getValue(), ""));
    			}
        	}
        	else
        	{      
        		mvCfgList = new ArrayList();
        		
        		if("QUERYLIST".equalsIgnoreCase(getMvAction()) ||  "QUERYDETAIL".equalsIgnoreCase(getMvAction()) || "QUERYDEFAULT".equalsIgnoreCase(getMvAction()) )
        		{
        			IMsgXMLNodeList lvCfgList = mvReturnNode.getNodeList(ITagXsfTagName.CHILD_ROW);
    				for (int i = 0; i < lvCfgList.size(); i++) {
    					IMsgXMLNode lvRow = lvCfgList.getNode(i);
    					
    					HashMap lvModel = new HashMap();    			            
    					lvModel.put(HKSTag.GROUPID, lvRow.getChildNode(HKSTag.GROUPID).getValue());
    					lvModel.put(HKSTag.GROUPNAME, lvRow.getChildNode(HKSTag.GROUPNAME).getValue());
    					lvModel.put(HKSTag.GROUPTYPE, lvRow.getChildNode(HKSTag.GROUPTYPE).getValue());    					
    					lvModel.put(HKSTag.ISDEFAULT, lvRow.getChildNode(HKSTag.ISDEFAULT).getValue());
    					if( "QUERYDETAIL".equalsIgnoreCase(getMvAction()) || "QUERYDEFAULT".equalsIgnoreCase(getMvAction()))
    						lvModel.put(HKSTag.SAVEDCONTENT, lvRow.getChildNode(HKSTag.SAVEDCONTENT).getValue());    					
    					mvCfgList.add(lvModel);
    				}       			
        		}       		
        	}        	
        }
        catch (Exception e) {
        	e.printStackTrace();
        	Log.println("[ HKSUIManagementTxn.process() ]" + e.toString(),  Log.ERROR_LOG);
		}        
    }

    /**
     * Get the client ID
     * @return Client ID
     */
    public String getClientId()
    {
        return mvClientID;
    }

    /**
     * Set the client ID
     * @param pClientId the client ID
     */
    public void setClientId(String pClientId)
    {
        this.mvClientID = pClientId;
    } 

	/**
	 * @param success the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}
	
	/**
	 * @param mvGroupName the mvGroupName to set
	 */
	public void setMvGroupName(String mvGroupName) {
		this.mvGroupName = mvGroupName;
	}
	
	/**
	 * @return the mvGroupName
	 */
	public String getMvGroupName() {
		return mvGroupName;
	}
	
	/**
	 * @param mvIsDefault the mvIsDefault to set
	 */
	public void setMvIsDefault(String mvIsDefault) {
		this.mvIsDefault = mvIsDefault;
	}
	
	/**
	 * @return the mvIsDefault
	 */
	public String getMvIsDefault() {
		return mvIsDefault;
	}
	
	/**
	 * @param mvGroupType the mvGroupType to set
	 */
	public void setMvGroupType(String mvGroupType) {
		this.mvGroupType = mvGroupType;
	}
	
	/**
	 * @return the mvGroupType
	 */
	public String getMvGroupType() {
		return mvGroupType;
	}
	
	/**
	 * @param mvSavedContent the mvSavedContent to set
	 */
	public void setMvSavedContent(String mvSavedContent) {
		this.mvSavedContent = mvSavedContent;
	}
	
	/**
	 * @return the mvSavedContent
	 */
	public String getMvSavedContent() {
		return mvSavedContent;
	}


	/**
	 * @param mvAction the mvAction to set
	 */
	public void setMvAction(String mvAction) {
		this.mvAction = mvAction;
	}


	/**
	 * @return the mvAction
	 */
	public String getMvAction() {
		return mvAction;
	}


	/**
	 * @param mvGroupID the mvGroupID to set
	 */
	public void setMvGroupID(String mvGroupID) {
		this.mvGroupID = mvGroupID;
	}


	/**
	 * @return the mvGroupID
	 */
	public String getMvGroupID() {
		return mvGroupID;
	}


	/**
	 * @param mvCfgList the mvCfgList1 to set
	 */
	public void setMvCfgList(List mvCfgList) {
		this.mvCfgList = mvCfgList;
	}


	/**
	 * @return the mvCfgList
	 */
	public List getMvCfgList() {
		return mvCfgList;
	}
}
