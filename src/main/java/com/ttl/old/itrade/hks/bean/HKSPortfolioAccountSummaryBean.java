package com.ttl.old.itrade.hks.bean;

/**
 * The HKSPortfolioEnquiry class define variables that to save values for action
 * 
 * @author
 * 
 */
public class HKSPortfolioAccountSummaryBean {

	private String mvClientID;
	private String mvToCurrencyID;
	private String mvCurrencyID;
	private int mvAccountSeq;
	private String mvCDueBuy;
	private String mvCDueSell;
	private String mvCSettled;
	private String mvCUnsettleBuy;
	private String mvCUnsettleSell;

	private String mvCTodayBuy;
	private String mvCTodaySell;
	private String mvCTodayConfirmShortsell;
	private String mvCTodayConfirmBuy;
	private String mvCTodayConfirmSell;
	private String mvCTodayIn;
	private String mvCTodayOut;
	private String mvCPendingDeposit;
	private String mvCPendingWithdrawal;
	private String mvCTodayUnsettleBuy;
	private String mvCTodayUnsettleSell;
	private String mvCT1UnsettleBuy;
	private String mvCT2UnsettleBuy;
	private String mvCT3UnsettleBuy;
	private String mvCT4UnsettleBuy;
	
	private String mvCT0UnsettleSell;

	private String mvCT1UnsettleSell;
	private String mvCT2UnsettleSell;
	private String mvCT3UnsettleSell;
	private String mvCT4UnsettleSell;
	private String mvCManualHold;
	private String mvCManualReserve;
	private String mvCCreditLine;
	private String mvCMortgagePendingAmt;
	private String mvCMortgageHoldAmt;
	private String mvCDailyOpenBal;
	private String mvCMonthlyOpenBal;
	private String mvAvailableBal;
	private String mvLedgerBal;
	private String mvSettledBalance;
	private String mvCashWithdrawable;
	private String mvMarginableBalf;
	private String mvMarginableBal;
	private String mvTradableBal;
	private String mvDrawableBal;
	private String mvAccruedInterest;

	private String mvTodaySettlement;
	private String mvHoldAmount;
	private String mvReserveAmount;
	private String mvInterest;
	private String mvDPWD;
	private String mvUsable;

	private String mvDebitAccruedInterest;
	private String mvCreditAccruedInterest;
	private String mvCNextDayDueSell;
	private String mvCNextDayDueBuy;
	private String mvCDailyMarginableBal;
	private String mvCInactiveBuy;
	private String mvCTodayHoldAmtExt;
	private String mvCShortSellAmt;
	private String mvCTodayOverrideAmt;
	private String mvBaseCcy;
	private String mvEvaluationRate;
	private String mvConversionRate;

	private String mvTDMortgageAmt;
	private String mvCManualReleaseAmt;
	private String mvCUnrealizedLoan;
	private String mvCChargeableAmt;	
	private String mvOutstandingLoan;	

	private String mvTotalOutAdvance;
	private String mvTotalHoldAmount;
	private String mvBuyHoldAmount;

	private String mvBuyingPowerd;
	private String mvPendingWithdraw;
	private String mvPendingSettled;
	private String mvMarginCall;
	
	private String mvDueBalance;
	private String mvTodayBS;
	private String mvMarketValue;
	private String mvMarginValue;
	private String mvCreditLimit;
	
	private String mvMarginPercentage;	
	private String mvExtraCreditd;
	private String mvAvailAdvanceMoney;
	//HIEU LE: new requirements from MAS
	//tab1
	private String totalAsset;
	private String equity;
	private String stockValue;
	private String profitLoss;
	private String PLPercent;
	//tab2
	private String cashBalance;
	private String pendingHold;
	private String pendingWithdrawal;
	private String soldT0;
	private String soldT1;
	private String soldT2;
	//tab3
	private String totalAssetMaintenance;
	private String equityMar;
	private String stockMaintenance;
	private String cashMaintenance;
	private String outStandingLoan;
	private String debtIncByPurchase;
	private String creditLimit;
	//tab4
	private String lendableValue;
	private String minMarginReq;
	private String curLiqMargin;
	private String cashDeposit;
	private String sellStkInMarPort;
	private String sellStkNotInMarPort;
	
	//END HIEU LE:
	
	public HKSPortfolioAccountSummaryBean() {

	}

	public String getClientID() {
		return mvClientID;
	}

	public String getToCurrencyID() {
		return mvToCurrencyID;
	}

	public String getCurrencyID() {
		return mvCurrencyID;
	}

	public int getAccountSeq() {
		return mvAccountSeq;
	}

	public String getCDueBuy() {
		return mvCDueBuy;
	}

	public String getCDueSell() {
		return mvCDueSell;
	}

	public String getCSettled() {
		return mvCSettled;
	}

	public String getCUnsettleBuy() {
		return mvCUnsettleBuy;
	}

	public String getCUnsettleSell() {
		return mvCUnsettleSell;
	}

	public String getCTodayBuy() {
		return mvCTodayBuy;
	}

	public String getCTodaySell() {
		return mvCTodaySell;
	}

	public String getCTodayConfirmShortsell() {
		return mvCTodayConfirmShortsell;
	}

	public String getCTodayConfirmBuy() {
		return mvCTodayConfirmBuy;
	}

	public String getCTodayConfirmSell() {
		return mvCTodayConfirmSell;
	}

	public String getCManualHold() {
		return mvCManualHold;
	}

	public String getCManualReserve() {
		return mvCManualReserve;
	}

	public String getCCreditLine() {
		return mvCCreditLine;
	}

	public String getCMortgagePendingAmt() {
		return mvCMortgagePendingAmt;
	}

	public String getCMortgageHoldAmt() {
		return mvCMortgageHoldAmt;
	}

	public String getCDailyOpenBal() {
		return mvCDailyOpenBal;
	}

	public String getCDailyMarginableBal() {
		return mvCDailyMarginableBal;
	}

	public String getCMonthlyOpenBal() {
		return mvCMonthlyOpenBal;
	}

	public String getAvailableBal() {
		return mvAvailableBal;
	}

	public String getLedgerBal() {
		return mvLedgerBal;
	}

	public String getMarginableBalf() {
		return mvMarginableBalf;
	}

	public String getMarginableBal() {
		return mvMarginableBal;
	}

	public String getTradableBal() {
		return mvTradableBal;
	}

	public String getDrawableBal() {
		return mvDrawableBal;
	}

	public String getAccruedInterest() {
		return mvAccruedInterest;
	}

	public String getDebitAccruedInterest() {
		return mvDebitAccruedInterest;
	}

	public String getCreditAccruedInterest() {
		return mvCreditAccruedInterest;
	}

	public String getCNextDayDueSell() {
		return mvCNextDayDueSell;
	}

	public String getCNextDayDueBuy() {
		return mvCNextDayDueBuy;
	}

	public String getCInactiveBuy() {
		return mvCInactiveBuy;
	}

	public String getCTodayHoldAmtExt() {
		return mvCTodayHoldAmtExt;
	}

	public String getCShortSellAmt() {
		return mvCShortSellAmt;
	}

	public String getCTodayOverrideAmt() {
		return mvCTodayOverrideAmt;
	}

	public String getBaseCcy() {
		return mvBaseCcy;
	}

	public String getEvaluationRate() {
		return mvEvaluationRate;
	}

	public String getConversionRate() {
		return mvConversionRate;
	}

	public String getTDMortgageAmt() {
		return mvTDMortgageAmt;
	}

	public String getCManualReleaseAmt() {
		return mvCManualReleaseAmt;
	}

	public void setClientID(String pClientID) {
		mvClientID = pClientID;
	}

	public void setToCurrencyID(String pToCurrencyID) {
		mvToCurrencyID = pToCurrencyID;
	}

	public void setCurrencyID(String pCurrencyID) {
		mvCurrencyID = pCurrencyID;
	}

	public void setBaseCcy(String pBaseCcy) {
		mvBaseCcy = pBaseCcy;
	}

	public void setAccountSeq(int pAccountSeq) {
		mvAccountSeq = pAccountSeq;
	}

	public void setCDueBuy(String pCDueBuy) {
		mvCDueBuy = pCDueBuy;
	}

	public void setCDueSell(String pCDueSell) {
		mvCDueSell = pCDueSell;
	}

	public void setCSettled(String pCSettled) {
		mvCSettled = pCSettled;
	}

	public void setCUnsettleBuy(String pCUnsettleBuy) {
		mvCUnsettleBuy = pCUnsettleBuy;
	}

	public void setCUnsettleSell(String pCUnsettleSell) {
		mvCUnsettleSell = pCUnsettleSell;
	}

	public void setCTodayBuy(String pCTodayBuy) {
		mvCTodayBuy = pCTodayBuy;
	}

	public void setCTodaySell(String pCTodaySell) {
		mvCTodaySell = pCTodaySell;
	}

	public void setCTodayConfirmShortsell(String pCTodayConfirmShortsell) {
		mvCTodayConfirmShortsell = pCTodayConfirmShortsell;
	}

	public void setCTodayConfirmBuy(String pCTodayConfirmBuy) {
		mvCTodayConfirmBuy = pCTodayConfirmBuy;
	}

	public void setCTodayConfirmSell(String pCTodayConfirmSell) {
		mvCTodayConfirmSell = pCTodayConfirmSell;
	}

	public void setCManualHold(String pCManualHold) {
		mvCManualHold = pCManualHold;
	}

	public void setCManualReserve(String pCManualReserve) {
		mvCManualReserve = pCManualReserve;
	}

	public void setCCreditLine(String pCCreditLine) {
		mvCCreditLine = pCCreditLine;
	}

	public void setCMortgagePendingAmt(String pCMortgagePendingAmt) {
		mvCMortgagePendingAmt = pCMortgagePendingAmt;
	}

	public void setCMortgageHoldAmt(String pCMortgageHoldAmt) {
		mvCMortgageHoldAmt = pCMortgageHoldAmt;
	}

	public void setCDailyOpenBal(String pCDailyOpenBal) {
		mvCDailyOpenBal = pCDailyOpenBal;
	}

	public void setCMonthlyOpenBal(String pCMonthlyOpenBal) {
		mvCMonthlyOpenBal = pCMonthlyOpenBal;
	}

	public void setAvailableBal(String pAvailableBal) {
		mvAvailableBal = pAvailableBal;
	}

	public void setLedgerBal(String pLedgerBal) {
		mvLedgerBal = pLedgerBal;
	}

	public void setEvaluationRate(String pEvaluationRate) {
		mvEvaluationRate = pEvaluationRate;
	}

	public void setConversionRate(String pConversionRate) {
		mvConversionRate = pConversionRate;
	}

	public void setMarginableBalf(String pMarginableBalf) {
		mvMarginableBalf = pMarginableBalf;
	}

	public void setMarginableBal(String pMarginableBal) {
		mvMarginableBal = pMarginableBal;
	}

	public void setTradableBal(String pTradableBal) {
		mvTradableBal = pTradableBal;
	}

	public void setDrawableBal(String pDrawableBal) {
		mvDrawableBal = pDrawableBal;
	}

	public void setAccruedInterest(String pAccruedInterest) {
		mvAccruedInterest = pAccruedInterest;
	}

	public void setDebitAccruedInterest(String pDebitAccruedInterest) {
		mvDebitAccruedInterest = pDebitAccruedInterest;
	}

	public void setCreditAccruedInterest(String pCreditAccruedInterest) {
		mvCreditAccruedInterest = pCreditAccruedInterest;
	}

	public void setCNextDayDueSell(String pCNextDayDueSell) {
		mvCNextDayDueSell = pCNextDayDueSell;
	}

	public void setCNextDayDueBuy(String pCNextDayDueBuy) {
		mvCNextDayDueBuy = pCNextDayDueBuy;
	}

	public void setCDailyMarginableBal(String pCDailyMarginableBal) {
		mvCDailyMarginableBal = pCDailyMarginableBal;
	}

	public void setCInactiveBuy(String pCInactiveBuy) {
		mvCInactiveBuy = pCInactiveBuy;
	}

	public void setCTodayHoldAmtExt(String pCTodayHoldAmtExt) {
		mvCTodayHoldAmtExt = pCTodayHoldAmtExt;
	}

	public void setCShortSellAmt(String pCShortSellAmt) {
		mvCShortSellAmt = pCShortSellAmt;
	}

	public void setCTodayOverrideAmt(String pCTodayOverrideAmt) {
		mvCTodayOverrideAmt = pCTodayOverrideAmt;
	}

	public void setTDMortgageAmt(String pTDMortgageAmt) {
		mvTDMortgageAmt = pTDMortgageAmt;
	}

	public void setCManualReleaseAmt(String pvManualReleaseAmt) {
		mvCManualReleaseAmt = pvManualReleaseAmt;
	}

	public String getCTodayUnsettleBuy() {
		return mvCTodayUnsettleBuy;
	}

	public void setCTodayUnsettleBuy(String pTodayUnsettleBuy) {
		mvCTodayUnsettleBuy = pTodayUnsettleBuy;
	}

	public String getCTodayUnsettleSell() {
		return mvCTodayUnsettleSell;
	}

	public void setCTodayUnsettleSell(String pTodayUnsettleSell) {
		mvCTodayUnsettleSell = pTodayUnsettleSell;
	}

	public String getCT1UnsettleBuy() {
		return mvCT1UnsettleBuy;
	}

	public void setCT1UnsettleBuy(String pUnsettleBuy) {
		mvCT1UnsettleBuy = pUnsettleBuy;
	}

	public String getCT2UnsettleBuy() {
		return mvCT2UnsettleBuy;
	}

	public void setCT2UnsettleBuy(String pUnsettleBuy) {
		mvCT2UnsettleBuy = pUnsettleBuy;
	}

	public String getCT3UnsettleBuy() {
		return mvCT3UnsettleBuy;
	}

	public void setCT3UnsettleBuy(String pUnsettleBuy) {
		mvCT3UnsettleBuy = pUnsettleBuy;
	}

	public String getCT4UnsettleBuy() {
		return mvCT4UnsettleBuy;
	}

	public void setCT4UnsettleBuy(String pUnsettleBuy) {
		mvCT4UnsettleBuy = pUnsettleBuy;
	}

	public String getCT1UnsettleSell() {
		return mvCT1UnsettleSell;
	}

	public void setCT1UnsettleSell(String pUnsettleSell) {
		mvCT1UnsettleSell = pUnsettleSell;
	}

	public String getCT2UnsettleSell() {
		return mvCT2UnsettleSell;
	}

	public void setCT2UnsettleSell(String pUnsettleSell) {
		mvCT2UnsettleSell = pUnsettleSell;
	}

	public String getCT3UnsettleSell() {
		return mvCT3UnsettleSell;
	}

	public void setCT3UnsettleSell(String pUnsettleSell) {
		mvCT3UnsettleSell = pUnsettleSell;
	}

	public String getCT4UnsettleSell() {
		return mvCT4UnsettleSell;
	}

	public void setCT4UnsettleSell(String pUnsettleSell) {
		mvCT4UnsettleSell = pUnsettleSell;
	}

	public String getCTodayIn() {
		return mvCTodayIn;
	}

	public void setCTodayIn(String pTodayIn) {
		mvCTodayIn = pTodayIn;
	}

	public String getCTodayOut() {
		return mvCTodayOut;
	}

	public void setCTodayOut(String pTodayOut) {
		mvCTodayOut = pTodayOut;
	}

	public void setCPendingDeposit(String pPendingDeposit) {
		mvCPendingDeposit = pPendingDeposit;
	}

	public String getCPendingWithdrawal() {
		return mvCPendingWithdrawal;
	}

	public void setCPendingWithdrawal(String pPendingWithdrawal) {
		mvCPendingWithdrawal = pPendingWithdrawal;
	}

	public String getCPendingDeposit() {
		return mvCPendingDeposit;
	}

	public String getCUnrealizedLoan() {
		return mvCUnrealizedLoan;
	}

	public void setCUnrealizedLoan(String pCUnrealizedLoan) {
		mvCUnrealizedLoan = pCUnrealizedLoan;
	}

	public String getCChargeableAmt() {
		return mvCChargeableAmt;
	}

	public void setCChargeableAmt(String pCChargeableAmt) {
		mvCChargeableAmt = pCChargeableAmt;
	}

	/**
	 * @return the mvSettledBalance
	 */
	public String getMvSettledBalance() {
		return mvSettledBalance;
	}

	/**
	 * @param mvSettledBalance
	 *            the mvSettledBalance to set
	 */
	public void setMvSettledBalance(String mvSettledBalance) {
		this.mvSettledBalance = mvSettledBalance;
	}

	/**
	 * @return the mvTodaySettlement
	 */
	public String getMvTodaySettlement() {
		return mvTodaySettlement;
	}

	/**
	 * @param mvTodaySettlement
	 *            the mvTodaySettlement to set
	 */
	public void setMvTodaySettlement(String mvTodaySettlement) {
		this.mvTodaySettlement = mvTodaySettlement;
	}

	/**
	 * @return the mvHoldAmount
	 */
	public String getMvHoldAmount() {
		return mvHoldAmount;
	}

	/**
	 * @param mvHoldAmount
	 *            the mvHoldAmount to set
	 */
	public void setMvHoldAmount(String mvHoldAmount) {
		this.mvHoldAmount = mvHoldAmount;
	}

	/**
	 * @return the mvReserveAmount
	 */
	public String getMvReserveAmount() {
		return mvReserveAmount;
	}

	/**
	 * @param mvReserveAmount
	 *            the mvReserveAmount to set
	 */
	public void setMvReserveAmount(String mvReserveAmount) {
		this.mvReserveAmount = mvReserveAmount;
	}

	/**
	 * @return the mvInterest
	 */
	public String getMvInterest() {
		return mvInterest;
	}

	/**
	 * @param mvInterest
	 *            the mvInterest to set
	 */
	public void setMvInterest(String mvInterest) {
		this.mvInterest = mvInterest;
	}

	/**
	 * @return the mvDPWD
	 */
	public String getMvDPWD() {
		return mvDPWD;
	}

	/**
	 * @param mvDPWD
	 *            the mvDPWD to set
	 */
	public void setMvDPWD(String mvDPWD) {
		this.mvDPWD = mvDPWD;
	}

	/**
	 * @return the mvUsable
	 */
	public String getMvUsable() {
		return mvUsable;
	}

	/**
	 * @param mvUsable
	 *            the mvUsable to set
	 */
	public void setMvUsable(String mvUsable) {
		this.mvUsable = mvUsable;
	}

	

	/**
	 * @return the mvOutstandingLoan
	 */
	public String getMvOutstandingLoan() {
		return mvOutstandingLoan;
	}

	/**
	 * @param mvOutstandingLoan
	 *            the mvOutstandingLoan to set
	 */
	public void setMvOutstandingLoan(String mvOutstandingLoan) {
		this.mvOutstandingLoan = mvOutstandingLoan;
	}

	
	/**
	 * @return the mvTotalOutAdvance
	 */
	public String getMvTotalOutAdvance() {
		return mvTotalOutAdvance;
	}

	/**
	 * @param mvTotalOutAdvance
	 *            the mvTotalOutAdvance to set
	 */
	public void setMvTotalOutAdvance(String mvTotalOutAdvance) {
		this.mvTotalOutAdvance = mvTotalOutAdvance;
	}

	/**
	 * @return the mvTotalHoldAmount
	 */
	public String getMvTotalHoldAmount() {
		return mvTotalHoldAmount;
	}

	/**
	 * @param mvTotalHoldAmount
	 *            the mvTotalHoldAmount to set
	 */
	public void setMvTotalHoldAmount(String mvTotalHoldAmount) {
		this.mvTotalHoldAmount = mvTotalHoldAmount;
	}

	/**
	 * @return the mvBuyHoldAmount
	 */
	public String getMvBuyHoldAmount() {
		return mvBuyHoldAmount;
	}

	/**
	 * @param mvBuyHoldAmount
	 *            the mvBuyHoldAmount to set
	 */
	public void setMvBuyHoldAmount(String mvBuyHoldAmount) {
		this.mvBuyHoldAmount = mvBuyHoldAmount;
	}

	/**
	 * @return the mvBuyingPowerd
	 */
	public String getMvBuyingPowerd() {
		return mvBuyingPowerd;
	}

	/**
	 * @param mvBuyingPowerd the mvBuyingPowerd to set
	 */
	public void setMvBuyingPowerd(String mvBuyingPowerd) {
		this.mvBuyingPowerd = mvBuyingPowerd;
	}

	/**
	 * @return the mvPendingWithdraw
	 */
	public String getMvPendingWithdraw() {
		return mvPendingWithdraw;
	}

	/**
	 * @param mvPendingWithdraw the mvPendingWithdraw to set
	 */
	public void setMvPendingWithdraw(String mvPendingWithdraw) {
		this.mvPendingWithdraw = mvPendingWithdraw;
	}

	/**
	 * @return the mvPendingSettled
	 */
	public String getMvPendingSettled() {
		return mvPendingSettled;
	}

	/**
	 * @param mvPendingSettled the mvPendingSettled to set
	 */
	public void setMvPendingSettled(String mvPendingSettled) {
		this.mvPendingSettled = mvPendingSettled;
	}

	/**
	 * @return the mvMarginCall
	 */
	public String getMvMarginCall() {
		return mvMarginCall;
	}

	/**
	 * @param mvMarginCall the mvMarginCall to set
	 */
	public void setMvMarginCall(String mvMarginCall) {
		this.mvMarginCall = mvMarginCall;
	}

	/**
	 * @return the mvDueBalance
	 */
	public String getMvDueBalance() {
		return mvDueBalance;
	}

	/**
	 * @param mvDueBalance the mvDueBalance to set
	 */
	public void setMvDueBalance(String mvDueBalance) {
		this.mvDueBalance = mvDueBalance;
	}

	/**
	 * @return the mvTodayBS
	 */
	public String getMvTodayBS() {
		return mvTodayBS;
	}

	/**
	 * @param mvTodayBS the mvTodayBS to set
	 */
	public void setMvTodayBS(String mvTodayBS) {
		this.mvTodayBS = mvTodayBS;
	}

	/**
	 * @return the mvMarketValue
	 */
	public String getMvMarketValue() {
		return mvMarketValue;
	}

	/**
	 * @param mvMarketValue the mvMarketValue to set
	 */
	public void setMvMarketValue(String mvMarketValue) {
		this.mvMarketValue = mvMarketValue;
	}

	/**
	 * @return the mvMarginValue
	 */
	public String getMvMarginValue() {
		return mvMarginValue;
	}

	/**
	 * @param mvMarginValue the mvMarginValue to set
	 */
	public void setMvMarginValue(String mvMarginValue) {
		this.mvMarginValue = mvMarginValue;
	}

	/**
	 * @return the mvCreditLimit
	 */
	public String getMvCreditLimit() {
		return mvCreditLimit;
	}

	/**
	 * @param mvCreditLimit the mvCreditLimit to set
	 */
	public void setMvCreditLimit(String mvCreditLimit) {
		this.mvCreditLimit = mvCreditLimit;
	}

	/**
	 * @return the mvMarginPercentage
	 */
	public String getMvMarginPercentage() {
		return mvMarginPercentage;
	}

	/**
	 * @param mvMarginPercentage the mvMarginPercentage to set
	 */
	public void setMvMarginPercentage(String mvMarginPercentage) {
		this.mvMarginPercentage = mvMarginPercentage;
	}	

	/**
	 * @return the mvExtraCreditd
	 */
	public String getMvExtraCreditd() {
		return mvExtraCreditd;
	}

	/**
	 * @param mvExtraCreditd the mvExtraCreditd to set
	 */
	public void setMvExtraCreditd(String mvExtraCreditd) {
		this.mvExtraCreditd = mvExtraCreditd;
	}

	/**
	 * @return the mvAvailAdvanceMoney
	 */
	public String getMvAvailAdvanceMoney() {
		return mvAvailAdvanceMoney;
	}

	/**
	 * @param mvAvailAdvanceMoney the mvAvailAdvanceMoney to set
	 */
	public void setMvAvailAdvanceMoney(String mvAvailAdvanceMoney) {
		this.mvAvailAdvanceMoney = mvAvailAdvanceMoney;
	}


	public String getMvCT0UnsettleSell() {
		return mvCT0UnsettleSell;
	}

	public void setMvCT0UnsettleSell(String mvCT0UnsettleSell) {
		this.mvCT0UnsettleSell = mvCT0UnsettleSell;
	}

	public String getMvCashWithdrawable() {
		return mvCashWithdrawable;
	}

	public void setMvCashWithdrawable(String mvCashWithdrawable) {
		this.mvCashWithdrawable = mvCashWithdrawable;
	}

	public String getTotalAsset() {
		return totalAsset;
	}

	public void setTotalAsset(String totalAsset) {
		this.totalAsset = totalAsset;
	}

	public String getEquity() {
		return equity;
	}

	public void setEquity(String equity) {
		this.equity = equity;
	}

	public String getStockValue() {
		return stockValue;
	}

	public void setStockValue(String stockValue) {
		this.stockValue = stockValue;
	}

	public String getProfitLoss() {
		return profitLoss;
	}

	public void setProfitLoss(String profitLoss) {
		this.profitLoss = profitLoss;
	}

	public String getPLPercent() {
		return PLPercent;
	}

	public void setPLPercent(String pLPercent) {
		PLPercent = pLPercent;
	}

	public String getCashBalance() {
		return cashBalance;
	}

	public void setCashBalance(String cashBalance) {
		this.cashBalance = cashBalance;
	}

	public String getPendingHold() {
		return pendingHold;
	}

	public void setPendingHold(String pendingHold) {
		this.pendingHold = pendingHold;
	}

	public String getPendingWithdrawal() {
		return pendingWithdrawal;
	}

	public void setPendingWithdrawal(String pendingWithdrawal) {
		this.pendingWithdrawal = pendingWithdrawal;
	}

	public String getSoldT0() {
		return soldT0;
	}

	public void setSoldT0(String soldT0) {
		this.soldT0 = soldT0;
	}

	public String getSoldT1() {
		return soldT1;
	}

	public void setSoldT1(String soldT1) {
		this.soldT1 = soldT1;
	}

	public String getSoldT2() {
		return soldT2;
	}

	public void setSoldT2(String soldT2) {
		this.soldT2 = soldT2;
	}

	public String getTotalAssetMaintenance() {
		return totalAssetMaintenance;
	}

	public void setTotalAssetMaintenance(String totalAssetMaintenance) {
		this.totalAssetMaintenance = totalAssetMaintenance;
	}

	public String getEquityMar() {
		return equityMar;
	}

	public void setEquityMar(String equityMar) {
		this.equityMar = equityMar;
	}

	public String getStockMaintenance() {
		return stockMaintenance;
	}

	public void setStockMaintenance(String stockMaintenance) {
		this.stockMaintenance = stockMaintenance;
	}

	public String getCashMaintenance() {
		return cashMaintenance;
	}

	public void setCashMaintenance(String cashMaintenance) {
		this.cashMaintenance = cashMaintenance;
	}

	public String getOutStandingLoan() {
		return outStandingLoan;
	}

	public void setOutStandingLoan(String outStandingLoan) {
		this.outStandingLoan = outStandingLoan;
	}

	public String getDebtIncByPurchase() {
		return debtIncByPurchase;
	}

	public void setDebtIncByPurchase(String debtIncByPurchase) {
		this.debtIncByPurchase = debtIncByPurchase;
	}

	public String getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(String creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getLendableValue() {
		return lendableValue;
	}

	public void setLendableValue(String lendableValue) {
		this.lendableValue = lendableValue;
	}

	public String getMinMarginReq() {
		return minMarginReq;
	}

	public void setMinMarginReq(String minMarginReq) {
		this.minMarginReq = minMarginReq;
	}

	public String getCurLiqMargin() {
		return curLiqMargin;
	}

	public void setCurLiqMargin(String curLiqMargin) {
		this.curLiqMargin = curLiqMargin;
	}

	public String getCashDeposit() {
		return cashDeposit;
	}

	public void setCashDeposit(String cashDeposit) {
		this.cashDeposit = cashDeposit;
	}

	public String getSellStkInMarPort() {
		return sellStkInMarPort;
	}

	public void setSellStkInMarPort(String sellStkInMarPort) {
		this.sellStkInMarPort = sellStkInMarPort;
	}

	public String getSellStkNotInMarPort() {
		return sellStkNotInMarPort;
	}

	public void setSellStkNotInMarPort(String sellStkNotInMarPort) {
		this.sellStkNotInMarPort = sellStkNotInMarPort;
	}
	
}
