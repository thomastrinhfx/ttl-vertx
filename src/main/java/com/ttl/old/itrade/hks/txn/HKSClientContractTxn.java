package com.ttl.old.itrade.hks.txn;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import com.ttl.old.itrade.hks.bean.HKSClientContractBean;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;

/**
 * Get client contract
 * 
 * @author duonghuynh
 */
public class HKSClientContractTxn extends BaseTxn {

	private String clientId;
	private String fromDate;
	private String toDate;
	private String bankId;
	private String bankAcId;
	private String unSettled;

	private List<HKSClientContractBean> clientContractList;

	/**
	 * 
	 * @param clientId
	 * @param fromDate
	 * @param toDate
	 * @param bankId
	 * @param bankAcId
	 * @param unSettled Y: get unsettled contracts. If "Y', fromData and toDate are invalid
	 */
	public HKSClientContractTxn(String clientId, String fromDate, String toDate, String bankId, String bankAcId, String unSettled) {
		setClientId(clientId);
		setFromDate(fromDate);
		setToDate(toDate);
		setUnSettled(unSettled);
		setBankId(bankId);
		setBankAcId(bankAcId);
	}

	/**
	 * @return the clientId
	 */
	public String getClientId() {
		return clientId;
	}

	/**
	 * @param clientId the clientId to set
	 */
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getBankAcId() {
		return bankAcId;
	}

	public void setBankAcId(String bankAcId) {
		this.bankAcId = bankAcId;
	}

	public String getUnSettled() {
		return unSettled;
	}

	public void setUnSettled(String unSettled) {
		this.unSettled = unSettled;
	}

	public List<HKSClientContractBean> getClientContractList() {
		return clientContractList;
	}

	public void setClientContractList(List<HKSClientContractBean> clientContractList) {
		this.clientContractList = clientContractList;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void process() {
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put("CLIENTID", getClientId());
		lvTxnMap.put("FROMDATE", getFromDate());
		lvTxnMap.put("TODATE", getToDate());
		lvTxnMap.put("BANKID", getBankId());
		lvTxnMap.put("BANKACID", getBankAcId());
		lvTxnMap.put("UNSETTLED", getUnSettled());

		if (TPErrorHandling.TP_NORMAL == process("HKSClientContract", lvTxnMap)) {
			clientContractList = new ArrayList<HKSClientContractBean>();
			IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList("Row");
			for (int i = 0; i < lvRowList.size(); i++) {
				IMsgXMLNode lvRow = lvRowList.getNode(i);
				HKSClientContractBean bean = new HKSClientContractBean();
				bean.setBankAcId(lvRow.getChildNode("BANKACID").getValue());
				bean.setBankId(lvRow.getChildNode("BANKID").getValue());
				bean.setBs(lvRow.getChildNode("BS").getValue());
				bean.setCashSettleDate(lvRow.getChildNode("CASHSETTLEDATE").getValue());
				bean.setClientId(lvRow.getChildNode("CLIENTID").getValue());
				bean.setContractAmount(lvRow.getChildNode("CONTRACTAMOUNT").getValue());
				bean.setInstrumentId(lvRow.getChildNode("INSTRUMENTID").getValue());
				bean.setInstrumentSettleDate(lvRow.getChildNode("INSTRUMENTSETTLEDATE").getValue());
				bean.setMarketId(lvRow.getChildNode("MARKETID").getValue());
				bean.setPrice(lvRow.getChildNode("PRICE").getValue());
				bean.setQty(lvRow.getChildNode("QTY").getValue());
				bean.setTranDate(lvRow.getChildNode("TRANDATE").getValue());
				clientContractList.add(bean);
			}
		}
	}
}
