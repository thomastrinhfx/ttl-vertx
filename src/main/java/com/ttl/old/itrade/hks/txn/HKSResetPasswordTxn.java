package com.ttl.old.itrade.hks.txn;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
import java.util.Hashtable;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.systekit.common.msg.IMsgXMLNode;

/**
 * This class processes the reset client password
 * @author not attributable
 *
 */
public class HKSResetPasswordTxn
{

        private String mvClientID;
        private String mvClientIDNumber;
        private String mvBirthday;
        private String mvBranchID;
        private String mvNewPassword;
        private int mvReturnCode;
        private String mvErrorCode;
        private String mvErrorMessage;
        private String mvIsAutoGenPassword;

        public static final String CLIENTID = "CLIENTID";
        public static final String IDNUMBER = "IDNUMBER";
        public static final String BIRTHDAY = "BIRTHDAY";
        public static final String BRANCHID = "BRANCHID";
        public static final String NEWPASSWORD = "NEWPASSWORD";
        public static final String ISAUTOGENPASSWORD = "ISAUTOGENPASSWORD";
        
	TPErrorHandling				tpError;

	/**
	 * Default constructor for HKSResetPasswordTxn class
	 */
	public HKSResetPasswordTxn()
	{
		tpError = new TPErrorHandling();
	}
	;


	/**
	 * Constructor for HKSResetPasswordTxn class
	 * @param pClientID the client id
	 * @param pClientIDNumber the client id number
	 * @param pBirthday the birthday
	 * @param pBranchID the branch id
	 * @param pNewPassword the new client password
	 * @param pIsAutoGenPassword the is auto gen client password
	 */
	public HKSResetPasswordTxn(String pClientID, String pClientIDNumber, String pBirthday, String pBranchID, String pNewPassword, String pIsAutoGenPassword)
	{
                setClientID(pClientID);
                setClientIDNumber(pClientIDNumber);
                setBirthday(pBirthday);
                setBranchID(pBranchID);
                setNewPassword(pNewPassword);
                setIsAutoGenPassword(pIsAutoGenPassword);
                tpError = new TPErrorHandling();
	}

	/**
	 * This method process the reset client password
	 */
	public void process()
	{
		try
		{
			Hashtable		lvTxnMap = new Hashtable();
			IMsgXMLNode		lvRetNode;
			IMsgXMLNode		lvErrorCodeNode;
                        TPManager ivTPManager = ITradeServlet.getTPManager(RequestName.HKSResetPasswordRequest);
                        TPBaseRequest  lvHKSResetPasswordRequest = ivTPManager.getRequest(RequestName.HKSResetPasswordRequest);

                        lvTxnMap.put(this.CLIENTID, getClientID());
                        lvTxnMap.put(this.IDNUMBER, getClientIDNumber());
                        lvTxnMap.put(this.BIRTHDAY, getBirthday());
                        lvTxnMap.put(this.BRANCHID, getBranchID());
                        lvTxnMap.put(this.NEWPASSWORD, getNewPassword());
                        lvTxnMap.put(this.ISAUTOGENPASSWORD, getIsAutoGenPassword());

			lvRetNode = lvHKSResetPasswordRequest.send(lvTxnMap);
			setReturnCode(tpError.checkError(lvRetNode));

                        if (mvReturnCode != tpError.TP_NORMAL)
                        {
                                setErrorMessage(tpError.getErrDesc());
                                if (mvReturnCode == tpError.TP_APP_ERR)
                                {
                                        setErrorCode(tpError.getErrCode());
                                }
                        }


		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Get method for client id
	 * @return client id
	 */
	public String getClientID()
	{
		return mvClientID;
	}
	/**
	 * Set method for client id
	 * @param pClientID the client id
	 */
        public void setClientID(String pClientID)
        {
                mvClientID = pClientID;
        }
        /**
         * Get method for client id number
         * @return client id number
         */
        public String getClientIDNumber()
        {
                return mvClientIDNumber;
        }
        /**
         * Set method for client id number
         * @param pClientIDNumber the client id number
         */
        public void setClientIDNumber(String pClientIDNumber)
        {
                mvClientIDNumber = pClientIDNumber;
        }
        /**
         * Get method for birthday
         * @return birthday
         */
        public String getBirthday()
        {
                return mvBirthday;
        }
        /**
         * Set method for birthday
         * @param pBirthday the birthday
         */
        public void setBirthday(String pBirthday)
        {
                mvBirthday = pBirthday;
        }
        /**
         * Get method for branch id
         * @return branch id
         */
        public String getBranchID()
        {
                return mvBranchID;
        }
        /**
         * Set method for branch id
         * @param pBranchID the branch id
         */
        public void setBranchID(String pBranchID)
        {
                mvBranchID = pBranchID;
        }
        /**
         * Get method for new client password
         * @return new client password
         */
        public String getNewPassword()
        {
                return mvNewPassword;
        }
        /**
         * Set method for new client password
         * @param pNewPassword the new client password
         */
        public void setNewPassword(String pNewPassword)
        {
                mvNewPassword = pNewPassword;
        }
        /**
         * Get method for is auto gen client password
         * @return is auto gen client password
         */
        public String getIsAutoGenPassword()
        {
                return mvIsAutoGenPassword;
        }
        /**
         * Set method for is auto gen client password
         * @param pIsAutoGenPassword the is auto gen client password
         */
        public void setIsAutoGenPassword(String pIsAutoGenPassword)
        {
                mvIsAutoGenPassword = pIsAutoGenPassword;
        }
        /**
         * Get method for return system code
         * @return  return system code
         */
        public int getReturnCode()
        {
                return mvReturnCode;
        }
        /**
         * Set method for return system code
         * @param pReturnCode the return system code
         */
        public void setReturnCode(int pReturnCode)
        {
                mvReturnCode = pReturnCode;
        }
        /**
         * Get method for error system code
         * @return error system code
         */
        public String getErrorCode()
        {
                return mvErrorCode;
        }
        /**
         * Set method for error system code
         * @param pErrorCode the error system code
         */
        public void setErrorCode(String pErrorCode)
        {
                mvErrorCode = pErrorCode;
        }
        /**
         * Get method for error system message
         * @return error system message
         */
        public String getErrorMessage()
        {
                return mvErrorMessage;
        }
        /**
         * Set method for error system message
         * @param pErrorMessage the error system message
         */
        public void setErrorMessage(String pErrorMessage)
        {
                mvErrorMessage = pErrorMessage;
        }

}
