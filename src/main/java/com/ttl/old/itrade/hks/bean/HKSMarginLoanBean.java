package com.ttl.old.itrade.hks.bean;

/**
 * 
 * @author
 * 
 */
public class HKSMarginLoanBean {

	private String rowNum;
	private String desc;
	private String tranDate;
	private String balance;
	private String out;
	private String in;
	private String marginCallF;
	private String sellAmount;

	public HKSMarginLoanBean() {
		rowNum = "";
		desc = "";
		tranDate = "";
		balance = "";
		out = "";
		in = "";
		marginCallF = "";
		sellAmount = "";
	}

	/**
	 * @return the rowNum
	 */
	public String getRowNum() {
		return rowNum;
	}

	/**
	 * @param rowNum the rowNum to set
	 */
	public void setRowNum(String rowNum) {
		this.rowNum = rowNum;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getTranDate() {
		return tranDate;
	}

	public void setTranDate(String tranDate) {
		this.tranDate = tranDate;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getOut() {
		return out;
	}

	public void setOut(String out) {
		this.out = out;
	}

	public String getIn() {
		return in;
	}

	public void setIn(String in) {
		this.in = in;
	}

	public String getMarginCallF() {
		return marginCallF;
	}

	public void setMarginCallF(String marginCallF) {
		this.marginCallF = marginCallF;
	}

	public String getSellAmount() {
		return sellAmount;
	}

	public void setSellAmount(String sellAmount) {
		this.sellAmount = sellAmount;
	}

}
