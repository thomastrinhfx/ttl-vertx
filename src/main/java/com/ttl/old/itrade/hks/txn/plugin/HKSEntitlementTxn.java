//BEGIN TASK #: - Giang Tran 20100827 [iTrade R5] EntitlementTxn
package com.ttl.old.itrade.hks.txn.plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import com.ttl.old.itrade.hks.bean.plugin.HKSActionRightBean;
import com.ttl.old.itrade.hks.bean.plugin.HKSEntitlementHistoryBean;
import com.ttl.old.itrade.hks.bean.plugin.HKSEntitlementStockInfoBean;
import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.hks.util.DisplayNumber;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.ErrorCode;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.winvest.hks.config.mapping.TagName;

public class HKSEntitlementTxn extends BaseTxn {
	
	private String mvClientID;
	private String mvStockCode;
	private String mvMarketId;
	private String mvEntitlementId;
	private String mvQuantity;
	private String mvLotSize;
	private String mvTradeDate;
	private Boolean mvSuccess = false;
	private String mvLocationId;
	private int mvStartRecord;
	private int mvEndRecord;
	private String mvTotalRecord;
	
	private String mvActionType;
	private String mvStartDate;
	private String mvEndDate;
	private String mvInterfaceSeq;
	
	private HKSEntitlementHistoryBean[] historyList;
	private HKSEntitlementStockInfoBean[] addtionIssueList;
	private HKSActionRightBean[] actionRightList;

	/**
	 * constructor for HKSEntitlementTxn
	 * @param pClientID
	 */
	public HKSEntitlementTxn(String pClientID) {
		super();
		this.setMvClientID(pClientID);
	}
	
	/**
	 * constructor for HKSEntitlementTxn
	 * @param pStockCode, pMarketId, pEntitlementId
	 */
	public HKSEntitlementTxn(String pStockCode, String pMarketId, String pEntitlementId, String pClientId, String pQuantity, String pLotSize, String pTradeDate) {
		super();
		this.setMvStockCode(pStockCode);
		this.setMvMarketId(pMarketId);
		this.setMvEntitlementId(pEntitlementId);
		this.setMvClientID(pClientId);
		this.setMvQuantity(pQuantity);
		this.setMvLotSize(pLotSize);
		this.setMvTradeDate(pTradeDate);
	}
	
	/**
	 * Get stock list which client have right to buy
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List getEntitlementStockList(){
		// Send TP request
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put(TagName.CLIENTID, mvClientID);
		
		ArrayList lvResultList = null;
		
		if(TPErrorHandling.TP_NORMAL==process("HKSGetEntitlementStockList",lvTxnMap)){
			lvResultList = new ArrayList();
			Hashtable lvStockMap = null;
			
			IMsgXMLNodeList lvStockList = mvReturnNode.getNodeList("ChildRow");
			if(lvStockList != null && lvStockList.size() > 0){
				for(int i=0; i<lvStockList.size(); i++){					
					lvStockMap = new Hashtable();
					IMsgXMLNode lvChildNode = lvStockList.getNode(i);
					
					lvStockMap.put(TagName.MARKETID, lvChildNode.getChildNode(TagName.MARKETID).getValue());
					lvStockMap.put(TagName.INSTRUMENTID, lvChildNode.getChildNode(TagName.INSTRUMENTID).getValue());
					lvStockMap.put(TagName.INSTRUMENTNAME, lvChildNode.getChildNode(TagName.INSTRUMENTNAME).getValue());
					lvStockMap.put(TagName.ENTITLEMENTID, lvChildNode.getChildNode(TagName.ENTITLEMENTID).getValue());
					lvStockMap.put(TagName.ENDDATE, lvChildNode.getChildNode(TagName.ENDDATE).getValue());
					lvStockMap.put(TagName.ISSUEINSTRUMENTID, lvChildNode.getChildNode(TagName.ISSUEINSTRUMENTID).getValue());
					lvStockMap.put(TagName.LOCATIONID, lvChildNode.getChildNode(TagName.LOCATIONID).getValue());
					
					lvResultList.add(lvStockMap);
				}
			}
		}
		
		return lvResultList;
	}
	
	
	/**
	 * Ge entitlement data of the stock which client selected
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public HashMap getEntitlementData(){
		// Send TP request
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put(TagName.CLIENTID, mvClientID);
		lvTxnMap.put(TagName.INSTRUMENTID, mvStockCode);
		lvTxnMap.put(TagName.MARKETID, mvMarketId);
		lvTxnMap.put(TagName.ENTITLEMENTID, mvEntitlementId);
		lvTxnMap.put(TagName.LOCATIONID, mvLocationId);
		
		HashMap result = null;		
		if(TPErrorHandling.TP_NORMAL==process("HKSGetEntitlementData",lvTxnMap)){
			result = new HashMap();
			result.put(TagName.STARTDATE, mvReturnNode.getChildNode(TagName.STARTDATE).getValue());
			result.put(TagName.ENDDATE, mvReturnNode.getChildNode(TagName.ENDDATE).getValue());
			result.put(TagName.CONFIRMEDPRICE, mvReturnNode.getChildNode(TagName.CONFIRMEDPRICE).getValue());
			result.put(TagName.INSTRUMENTID, mvReturnNode.getChildNode(TagName.INSTRUMENTID).getValue());
			result.put(TagName.ISSUEINSTRUMENTID, mvReturnNode.getChildNode(TagName.ISSUEINSTRUMENTID).getValue());
			result.put(TagName.EXERCISERATIOPER, mvReturnNode.getChildNode(TagName.EXERCISERATIOPER).getValue());
			result.put(TagName.EXERCISERATIODELIVERY, mvReturnNode.getChildNode(TagName.EXERCISERATIODELIVERY).getValue());
			result.put(TagName.EXERCISABLEQTY, mvReturnNode.getChildNode(TagName.EXERCISABLEQTY).getValue());
			result.put("REGISTEDQTY", mvReturnNode.getChildNode("REGISTEDQTY").getValue());
			result.put(TagName.ENTITLEMENTID, mvReturnNode.getChildNode(TagName.ENTITLEMENTID).getValue());
			result.put(TagName.MARKETID, mvReturnNode.getChildNode(TagName.MARKETID).getValue());
			result.put(TagName.BOOKCLOSEDATE, mvReturnNode.getChildNode(TagName.BOOKCLOSEDATE).getValue());
			result.put(TagName.LOCATIONID, mvReturnNode.getChildNode(TagName.LOCATIONID).getValue());
			
		}
		
		return result;		
	}
	
	/**
	 * Function to register to buy the sotck
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void doRegisterExercise(){
		// Send TP request
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put(TagName.CLIENTID, mvClientID);
		lvTxnMap.put(TagName.INSTRUMENTID, mvStockCode);
		lvTxnMap.put(TagName.MARKETID, mvMarketId);
		lvTxnMap.put(TagName.ENTITLEMENTID, mvEntitlementId);
		lvTxnMap.put(TagName.QUANTITY, mvQuantity);
		lvTxnMap.put(TagName.LOTSIZE, mvLotSize);
		lvTxnMap.put(TagName.TRADEDATE, mvTradeDate);
		lvTxnMap.put(TagName.LOCATIONID, mvLocationId);
		lvTxnMap.put(TagName.INTERFACESEQ, mvInterfaceSeq);
		
		if(TPErrorHandling.TP_NORMAL==process("HKSDoRegisterExercise",lvTxnMap)){
			String result = mvReturnNode.getChildNode(TagName.RESULT).getValue();
			String returnCode = mvReturnNode.getChildNode(TagName.RETURNCODE).getValue();
			String msg = mvReturnNode.getChildNode(TagName.RETURNMESSAGE).getValue();
			if("FAILED".equals(result) && result != null){
				setMvSuccess(false);
				setErrorCode(new ErrorCode(new String[0], returnCode, msg));
			} else {
				setMvSuccess(true);
			}
		}
	}
	
	/**
	 * Function get entitlement history data
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void getEntitlementHistoryData(){
		
		// Send TP request
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put(TagName.CLIENTID, mvClientID);
		lvTxnMap.put("STARTRECORD", String.valueOf(mvStartRecord));
		lvTxnMap.put("ENDRECORD", String.valueOf(mvEndRecord));
		lvTxnMap.put(TagName.FROMDATE, String.valueOf(mvStartDate));
		lvTxnMap.put(TagName.TODATE, String.valueOf(mvEndDate));
		lvTxnMap.put(TagName.INSTRUMENTID, String.valueOf(mvStockCode));
		
		
		if(TPErrorHandling.TP_NORMAL==process("HKSGetEntitlementHistory",lvTxnMap)){
			
			if (mvReturnNode.getChildNode("TOTALRECORD") != null) {
				setMvTotalRecord(mvReturnNode.getChildNode("TOTALRECORD")
						.getValue());
			}
			
			IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList("ChildRow");
			setHistoryList(new HKSEntitlementHistoryBean[lvRowList.size()]);
			for (int i = 0; i < lvRowList.size(); i++) {
				getHistoryList()[i] = new HKSEntitlementHistoryBean();
				IMsgXMLNode lvRow = lvRowList.getNode(i);
				
				// entitlement id
				getHistoryList()[i].setEntitlementId(lvRow.getChildNode(TagName.ENTITLEMENTID).getValue());
				
				// subscription id
				getHistoryList()[i].setSubscriptionId(lvRow.getChildNode(TagName.SUBSCRIPTIONID).getValue());
				
				// create time
				getHistoryList()[i].setCreateTime(lvRow.getChildNode(TagName.CREATIONTIME).getValue());
				
				// remark 
				getHistoryList()[i].setRemark(lvRow.getChildNode(TagName.REMARK).getValue());
				
				// status
				getHistoryList()[i].setStatus(lvRow.getChildNode(TagName.STATUS).getValue());
				
				// stock code
				getHistoryList()[i].setStockId(lvRow.getChildNode(TagName.INSTRUMENTID).getValue());
				
				// exercise qty
				getHistoryList()[i].setExerciseQty(lvRow.getChildNode(TagName.QUANTITY).getValue());
				
				// confirmed date
				getHistoryList()[i].setComfirmedDate(lvRow.getChildNode("CONFIRMEDDATE").getValue());
				
				// applied amt
				getHistoryList()[i].setAppliedAmt(lvRow.getChildNode(TagName.APPLIEDAMOUNT).getValue());
				
				// price
				getHistoryList()[i].setPrice(lvRow.getChildNode(TagName.CONFIRMEDPRICE).getValue());
				
				// issue stock
				getHistoryList()[i].setTradeStockCode(lvRow.getChildNode(TagName.ISSUEINSTRUMENTID).getValue());
				
				// result Qty
				getHistoryList()[i].setResultQty(lvRow.getChildNode("RESULTQUANTITY").getValue());
				// get location
				getHistoryList()[i].setLocationId(lvRow.getChildNode(TagName.LOCATIONID).getValue());
			}			
		}
		
	}
	
	
	/**
	 * Function get entitlement history data
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void getAdditionIssueShareInfo(){
		
		// Send TP request
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put(TagName.CLIENTID, mvClientID);
		
		if(TPErrorHandling.TP_NORMAL==process("HKSGetAdditionIssueSharesInfo",lvTxnMap)){
			
			IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList("ChildRow");
			addtionIssueList = new HKSEntitlementStockInfoBean[lvRowList.size()];
			for (int i = 0; i < lvRowList.size(); i++) {
				addtionIssueList[i] = new HKSEntitlementStockInfoBean();
				IMsgXMLNode lvRow = lvRowList.getNode(i);
				
				addtionIssueList[i].setStartDate(lvRow.getChildNode(TagName.STARTDATE).getValue());
				addtionIssueList[i].setEndDate(lvRow.getChildNode(TagName.ENDDATE).getValue());
				addtionIssueList[i].setPrice(lvRow.getChildNode(TagName.CONFIRMEDPRICE).getValue());
				addtionIssueList[i].setTransenddate(lvRow.getChildNode("TRANSFERENDDATE").getValue());
				addtionIssueList[i].setStockId(lvRow.getChildNode(TagName.INSTRUMENTID).getValue());
				addtionIssueList[i].setTradeStockCode(lvRow.getChildNode(TagName.ISSUEINSTRUMENTID).getValue());
				addtionIssueList[i].setPerRightRate(lvRow.getChildNode(TagName.EXERCISERATIOPER).getValue());
				addtionIssueList[i].setPerStockRate(lvRow.getChildNode(TagName.EXERCISERATIODELIVERY).getValue());
				addtionIssueList[i].setEntitlementId(lvRow.getChildNode(TagName.ENTITLEMENTID).getValue());
				addtionIssueList[i].setMarketId(lvRow.getChildNode(TagName.MARKETID).getValue());
				addtionIssueList[i].setBookCloseDate(lvRow.getChildNode(TagName.BOOKCLOSEDATE).getValue());
				addtionIssueList[i].setLocationId(lvRow.getChildNode(TagName.LOCATIONID).getValue());
				addtionIssueList[i].setRegisterQty(lvRow.getChildNode("REGISTEDQTY").getValue());
				addtionIssueList[i].setTotalStock(lvRow.getChildNode("BOOKCLOSEQTY").getValue());
				addtionIssueList[i].setMaxQtyCanBuy(lvRow.getChildNode(TagName.EXERCISABLEQTY).getValue());
				addtionIssueList[i].setStatus(lvRow.getChildNode(TagName.STATUS).getValue());
				addtionIssueList[i].setRightRate(new DisplayNumber(lvRow.getChildNode(TagName.ISSUERATIOPER).getValue()).toString()
						+ " : " + new DisplayNumber(lvRow.getChildNode(TagName.ISSUERATIODELIVERY).getValue()).toString());
			}			
		}
		
	}
	
	/**
	 * getAllRightList
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void getAllRightList() {		
		// Send TP request
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put(TagName.CLIENTID, mvClientID);
		lvTxnMap.put("STARTRECORD", String.valueOf(mvStartRecord));
		lvTxnMap.put("ENDRECORD", String.valueOf(mvEndRecord));
		lvTxnMap.put(TagName.FROMDATE, String.valueOf(mvStartDate));
		lvTxnMap.put(TagName.TODATE, String.valueOf(mvEndDate));
		lvTxnMap.put(TagName.ISSUETYPE, String.valueOf(mvActionType));
		lvTxnMap.put(TagName.INSTRUMENTID, String.valueOf(mvStockCode));
		
		
		if(TPErrorHandling.TP_NORMAL==process("HKSGetEntitlementActionRightList",lvTxnMap)){
			
			if (mvReturnNode.getChildNode("TOTALRECORD") != null) {
				setMvTotalRecord(mvReturnNode.getChildNode("TOTALRECORD")
						.getValue());
			}
			
			IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList("ChildRow");
			setActionRightList(new HKSActionRightBean[lvRowList.size()]);
			for (int i = 0; i < lvRowList.size(); i++) {
				getActionRightList()[i] = new HKSActionRightBean();
				IMsgXMLNode lvRow = lvRowList.getNode(i);
				
				getActionRightList()[i].setBookCloseDate(lvRow.getChildNode(TagName.BOOKCLOSEDATE).getValue());
				getActionRightList()[i].setIssueRatioDelivery(lvRow.getChildNode(TagName.ISSUERATIODELIVERY).getValue());
				getActionRightList()[i].setIssueRatioPer(lvRow.getChildNode(TagName.ISSUERATIOPER).getValue());
				getActionRightList()[i].setIssueType(lvRow.getChildNode(TagName.ISSUETYPE).getValue());
				getActionRightList()[i].setPaidDate(lvRow.getChildNode("PAIDDATE").getValue());
				getActionRightList()[i].setPayableDate(lvRow.getChildNode("PAYABLEDATE").getValue());
				getActionRightList()[i].setPrice(lvRow.getChildNode(TagName.CONFIRMEDPRICE).getValue());
				getActionRightList()[i].setRemark(lvRow.getChildNode(TagName.REMARK).getValue());
				getActionRightList()[i].setStartDate(lvRow.getChildNode(TagName.STARTDATE).getValue());
				getActionRightList()[i].setStatus(lvRow.getChildNode(TagName.STATUS).getValue());
				getActionRightList()[i].setStockId(lvRow.getChildNode(TagName.INSTRUMENTID).getValue());
				getActionRightList()[i].setTotalBonusRight(lvRow.getChildNode("TOTALBONOUSRIGHT").getValue());
				getActionRightList()[i].setTotalIssue(lvRow.getChildNode("TOTALISSUE").getValue());
				getActionRightList()[i].setTotalRemainAmt(lvRow.getChildNode("TOTALREMAINDERAMT").getValue());
				getActionRightList()[i].setTotalScript(lvRow.getChildNode("TOTALSCRIPT").getValue());
				getActionRightList()[i].setTypeDescription(lvRow.getChildNode(TagName.DESCRIPTION).getValue());
				getActionRightList()[i].setIsStockCash(lvRow.getChildNode("ISSTOCKCASH").getValue());
				getActionRightList()[i].setBookCloseQty(lvRow.getChildNode("BOOKCLOSEQTY").getValue());
			}			
		}
	}
	

	/**
	 * @param mvClientID the mvClientID to set
	 */
	public void setMvClientID(String mvClientID) {
		this.mvClientID = mvClientID;
	}

	/**
	 * @return the mvClientID
	 */
	public String getMvClientID() {
		return mvClientID;
	}

	/**
	 * @return the mvStockCode
	 */
	public String getMvStockCode() {
		return mvStockCode;
	}

	/**
	 * @param mvStockCode the mvStockCode to set
	 */
	public void setMvStockCode(String mvStockCode) {
		this.mvStockCode = mvStockCode;
	}

	/**
	 * @return the mvMarketId
	 */
	public String getMvMarketId() {
		return mvMarketId;
	}

	/**
	 * @param mvMarketId the mvMarketId to set
	 */
	public void setMvMarketId(String mvMarketId) {
		this.mvMarketId = mvMarketId;
	}

	/**
	 * @return the mvEntitlementId
	 */
	public String getMvEntitlementId() {
		return mvEntitlementId;
	}

	/**
	 * @param mvEntitlementId the mvEntitlementId to set
	 */
	public void setMvEntitlementId(String mvEntitlementId) {
		this.mvEntitlementId = mvEntitlementId;
	}

	/**
	 * @param mvQuantity the mvQuantity to set
	 */
	public void setMvQuantity(String mvQuantity) {
		this.mvQuantity = mvQuantity;
	}

	/**
	 * @return the mvQuantity
	 */
	public String getMvQuantity() {
		return mvQuantity;
	}

	/**
	 * @return the mvLotSize
	 */
	public String getMvLotSize() {
		return mvLotSize;
	}

	/**
	 * @param mvLotSize the mvLotSize to set
	 */
	public void setMvLotSize(String mvLotSize) {
		this.mvLotSize = mvLotSize;
	}

	/**
	 * @return the mvTradeDate
	 */
	public String getMvTradeDate() {
		return mvTradeDate;
	}

	/**
	 * @param mvTradeDate the mvTradeDate to set
	 */
	public void setMvTradeDate(String mvTradeDate) {
		this.mvTradeDate = mvTradeDate;
	}

	/**
	 * @param historyList the historyList to set
	 */
	public void setHistoryList(HKSEntitlementHistoryBean[] historyList) {
		this.historyList = historyList;
	}

	/**
	 * @return the historyList
	 */
	public HKSEntitlementHistoryBean[] getHistoryList() {
		return historyList;
	}

	/**
	 * @param mvSuccess the mvSuccess to set
	 */
	public void setMvSuccess(Boolean mvSuccess) {
		this.mvSuccess = mvSuccess;
	}

	/**
	 * @return the mvSuccess
	 */
	public Boolean getMvSuccess() {
		return mvSuccess;
	}

	/**
	 * @param mvLocationId the mvLocationId to set
	 */
	public void setMvLocationId(String mvLocationId) {
		this.mvLocationId = mvLocationId;
	}

	/**
	 * @return the mvLocationId
	 */
	public String getMvLocationId() {
		return mvLocationId;
	}

	public void setMvStartRecord(int mvStartRecord) {
		this.mvStartRecord = mvStartRecord;
	}

	public int getMvStartRecord() {
		return mvStartRecord;
	}

	public void setMvEndRecord(int mvEndRecord) {
		this.mvEndRecord = mvEndRecord;
	}

	public int getMvEndRecord() {
		return mvEndRecord;
	}

	public void setMvTotalRecord(String mvTotalRecord) {
		this.mvTotalRecord = mvTotalRecord;
	}

	public String getMvTotalRecord() {
		return mvTotalRecord;
	}

	/**
	 * @param addtionIssueList the addtionIssueList to set
	 */
	public void setAddtionIssueList(HKSEntitlementStockInfoBean[] addtionIssueList) {
		this.addtionIssueList = addtionIssueList;
	}

	/**
	 * @return the addtionIssueList
	 */
	public HKSEntitlementStockInfoBean[] getAddtionIssueList() {
		return addtionIssueList;
	}

	/**
	 * @param mvActionType the mvActionType to set
	 */
	public void setMvActionType(String mvActionType) {
		this.mvActionType = mvActionType;
	}

	/**
	 * @return the mvActionType
	 */
	public String getMvActionType() {
		return mvActionType;
	}

	/**
	 * @param mvStartDate the mvStartDate to set
	 */
	public void setMvStartDate(String mvStartDate) {
		this.mvStartDate = mvStartDate;
	}

	/**
	 * @return the mvStartDate
	 */
	public String getMvStartDate() {
		return mvStartDate;
	}

	/**
	 * @param mvEndDate the mvEndDate to set
	 */
	public void setMvEndDate(String mvEndDate) {
		this.mvEndDate = mvEndDate;
	}

	/**
	 * @return the mvEndDate
	 */
	public String getMvEndDate() {
		return mvEndDate;
	}

	/**
	 * @param actionRightList the actionRightList to set
	 */
	public void setActionRightList(HKSActionRightBean[] actionRightList) {
		this.actionRightList = actionRightList;
	}

	/**
	 * @return the actionRightList
	 */
	public HKSActionRightBean[] getActionRightList() {
		return actionRightList;
	}

	/**
	 * @return the mvInterfaceSeq
	 */
	public String getMvInterfaceSeq() {
		return mvInterfaceSeq;
	}

	/**
	 * @param mvInterfaceSeq the mvInterfaceSeq to set
	 */
	public void setMvInterfaceSeq(String mvInterfaceSeq) {
		this.mvInterfaceSeq = mvInterfaceSeq;
	}

}
//END TASK #: - Giang Tran 20100827 [iTrade R5] EntitlementTxn
