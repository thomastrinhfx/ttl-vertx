package com.ttl.old.itrade.hks.bean;

/**
 * The HKSMultiOrderBean class define variables that to save values 
 * for action 
 * @author 
 *
 */
public class HKSMultiOrderBean {

	private String mvRefId;
	private String mvClientId;
	private String mvStockCode;
	private String mvMarketId;
	private String mvBS;
	private String mvOrderType;
	private String mvPrice;
	private String mvQty;
	private String mvStopPrice;
	private String mvGoodTillDate;
	private String mvBankId;
	private String mvBankACID;
	
	
	/**
	 * @return the mvRefId
	 */
	public String getMvRefId() {
		return mvRefId;
	}
	/**
	 * @param mvRefId the mvRefId to set
	 */
	public void setMvRefId(String mvRefId) {
		this.mvRefId = mvRefId;
	}
	/**
	 * @return the mvClientId
	 */
	public String getMvClientId() {
		return mvClientId;
	}
	/**
	 * @param mvClientId the mvClientId to set
	 */
	public void setMvClientId(String mvClientId) {
		this.mvClientId = mvClientId;
	}
	/**
	 * @return the mvStockCode
	 */
	public String getMvStockCode() {
		return mvStockCode;
	}
	/**
	 * @param mvStockCode the mvStockCode to set
	 */
	public void setMvStockCode(String mvStockCode) {
		this.mvStockCode = mvStockCode;
	}
	/**
	 * @return the mvMarketId
	 */
	public String getMvMarketId() {
		return mvMarketId;
	}
	/**
	 * @param mvMarketId the mvMarketId to set
	 */
	public void setMvMarketId(String mvMarketId) {
		this.mvMarketId = mvMarketId;
	}
	/**
	 * @return the mvBS
	 */
	public String getMvBS() {
		return mvBS;
	}
	/**
	 * @param mvBS the mvBS to set
	 */
	public void setMvBS(String mvBS) {
		this.mvBS = mvBS;
	}
	/**
	 * @return the mvOrderType
	 */
	public String getMvOrderType() {
		return mvOrderType;
	}
	/**
	 * @param mvOrderType the mvOrderType to set
	 */
	public void setMvOrderType(String mvOrderType) {
		this.mvOrderType = mvOrderType;
	}
	/**
	 * @return the mvPrice
	 */
	public String getMvPrice() {
		return mvPrice;
	}
	/**
	 * @param mvPrice the mvPrice to set
	 */
	public void setMvPrice(String mvPrice) {
		this.mvPrice = mvPrice;
	}
	/**
	 * @return the mvQty
	 */
	public String getMvQty() {
		return mvQty;
	}
	/**
	 * @param mvQty the mvQty to set
	 */
	public void setMvQty(String mvQty) {
		this.mvQty = mvQty;
	}
	/**
	 * @return the mvStopPrice
	 */
	public String getMvStopPrice() {
		return mvStopPrice;
	}
	/**
	 * @param mvStopPrice the mvStopPrice to set
	 */
	public void setMvStopPrice(String mvStopPrice) {
		this.mvStopPrice = mvStopPrice;
	}
	/**
	 * @return the mvGoodTillDate
	 */
	public String getMvGoodTillDate() {
		return mvGoodTillDate;
	}
	/**
	 * @param mvGoodTillDate the mvGoodTillDate to set
	 */
	public void setMvGoodTillDate(String mvGoodTillDate) {
		this.mvGoodTillDate = mvGoodTillDate;
	}
	/**
	 * @return the mvBankId
	 */
	public String getMvBankId() {
		return mvBankId;
	}
	/**
	 * @param mvBankId the mvBankId to set
	 */
	public void setMvBankId(String mvBankId) {
		this.mvBankId = mvBankId;
	}
	/**
	 * @return the mvBankACID
	 */
	public String getMvBankACID() {
		return mvBankACID;
	}
	/**
	 * @param mvBankACID the mvBankACID to set
	 */
	public void setMvBankACID(String mvBankACID) {
		this.mvBankACID = mvBankACID;
	}

}
