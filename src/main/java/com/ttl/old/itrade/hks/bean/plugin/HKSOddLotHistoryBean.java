package com.ttl.old.itrade.hks.bean.plugin;

import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * The HKSOddLotHistoryBean class define variables that to save values for action 
 * @author Giang Tran
 *
 */
public class HKSOddLotHistoryBean {
	private String marketId;
	private String instrumentId;
	private String locationId;
	private String price;
	private String appliedQty;
	private String fee;
	private String settleAmt;
	private String confirmDate;
	private String valueDate;
	private String status;
	private String createTime;
	private String lastModifiedTime;
	private String lastApprovalTime;
	
	/**
	 * @return the marketId
	 */
	public String getMarketId() {
		return marketId;
	}
	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(String marketId) {
		this.marketId = marketId;
	}
	/**
	 * @return the instrumentId
	 */
	public String getInstrumentId() {
		return instrumentId;
	}
	/**
	 * @param instrumentId the instrumentId to set
	 */
	public void setInstrumentId(String instrumentId) {
		this.instrumentId = instrumentId;
	}
	/**
	 * @return the locationId
	 */
	public String getLocationId() {
		return locationId;
	}
	/**
	 * @param locationId the locationId to set
	 */
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}
	/**
	 * @return the appliedQty
	 */
	public String getAppliedQty() {
		return appliedQty;
	}
	/**
	 * @param appliedQty the appliedQty to set
	 */
	public void setAppliedQty(String appliedQty) {
		this.appliedQty = appliedQty;
	}
	/**
	 * @return the fee
	 */
	public String getFee() {
		return fee;
	}
	/**
	 * @param fee the fee to set
	 */
	public void setFee(String fee) {
		this.fee = fee;
	}
	/**
	 * @return the settleAmt
	 */
	public String getSettleAmt() {
		return settleAmt;
	}
	/**
	 * @param settleAmt the settleAmt to set
	 */
	public void setSettleAmt(String settleAmt) {
		this.settleAmt = settleAmt;
	}
	/**
	 * @return the confirmDate
	 */
	public String getConfirmDate() {
		return confirmDate;
	}
	/**
	 * @param confirmDate the confirmDate to set
	 */
	public void setConfirmDate(String confirmDate) {
		this.confirmDate = confirmDate;
	}
	/**
	 * @return the valueDate
	 */
	public String getValueDate() {
		return valueDate;
	}
	/**
	 * @param valueDate the valueDate to set
	 */
	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the createTime
	 */
	public String getCreateTime() {
		return createTime;
	}
	/**
	 * @param createTime the createTime to set
	 */
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	/**
	 * @return the lastModifiedTime
	 */
	public String getLastModifiedTime() {
		return lastModifiedTime;
	}
	/**
	 * @param lastModifiedTime the lastModifiedTime to set
	 */
	public void setLastModifiedTime(String lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}
	/**
	 * @return the lastApprovalTime
	 */
	public String getLastApprovalTime() {
		return lastApprovalTime;
	}
	/**
	 * @param lastApprovalTime the lastApprovalTime to set
	 */
	public void setLastApprovalTime(String lastApprovalTime) {
		this.lastApprovalTime = lastApprovalTime;
	}
}
