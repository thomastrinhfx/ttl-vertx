package com.ttl.old.itrade.hks.bean.plugin;

public class DemoBLMQueryBean {
	private String mvStockName;
	
	/**
	 * Get the stock name
	 * @return stockName
	 */
	public String getStockName() {
		return mvStockName;
	}
	/**
	 * Set the stock name
	 * @param stockName
	 */
	public void setStockName(String pStockName) {
		mvStockName = pStockName;
	}
	
}