package com.ttl.old.itrade.hks.bean;

import java.util.List;

/**
 * The HKSEnterOrderConfirmActionBean class define variables that to save values 
 * for action 
 * @author Wind zhao
 *
 */
public class HKSGenEnterOrderBean {
	public String mvClientID ;
	public String mvDateTime;
	public String mvDisplayTips;
	public String mvLastLoginDateDisplay;
	public String mvLastLoginTimeDisplay; 
	public String mvPassConfirmation; 
	public String mvAllowGoodTillDate;
	public String mvGoodTillDate;
	public String mvDisplayGoodTillDatePopup; 
	public String mvDisplayGoodTillDateAlert; 
	public String mvMarketID;
	public String mvMarketGoodTillDateOption;
	public String mvDisplayStockSearch;
	public String mvRequestUserConfirmation;
	public String mvStockName;
	public String mvTxnMessage;
	public String mvBuySell;
	public String mvOrderType;
	public String mvStockCode;
	public String mvPrice;
	public String mvQuantity;
	public String mvErrorType;
	public String mvCBAuction;
	public String mvCBStop;
	public String mvStopType;
	public String mvStopPrice;
	public String mvCBAON;
	public String mvCBPassSaved;
	public String mvOrderTypeListOption;
	public List mvGoodTillDateValue;
	public List mvMarketIDList;
	public String mvBuyButtonColor;
	public String mvSellButtonColor;
	public String mvStockSearchDescription;
	public String mvQuantityDescription;
	public String mvOrderTypeDescription;
	public String mvGoodTillDescription;
	public String mvLastOrderType;
	public String mvLastInputGoodTillDate;
	public String mvCurrentTradeDate;
	public String mvTransType;
	public String mvInstrumentID;
	public String mvAALoginURL;
	public String mvErrorMsg;
	public String mvLastLoginDate;
	public String mvLastLoginTime;
	public String mvIfShowMarketId;
	
	/**
     * This method returns the market id if show or hidden
     * to control market id filed show or not.
     * @return the market id if show or hidden.
     */
	public String getMvIfShowMarketId() {
		return mvIfShowMarketId;
	}
	
	/**
     * This method sets the market id if show or hidden
     * to control market id filed show or not.
     * @param pIfShowMarketId The pIfShowMarketId if show or hidden.
     */
	public void setMvIfShowMarketId(String pIfShowMarketId) {
		mvIfShowMarketId = pIfShowMarketId;
	}
	
	/**
     * This method returns the list of market id.
     * @return the list of market id.
     */
	public List getMvMarketIDList() {
		return mvMarketIDList;
	}
	
	/**
     * This method sets the list of market id.
     * @param pMarketIDList The list of market id.
     */
	public void setMvMarketIDList(List pMarketIDList) {
		mvMarketIDList = pMarketIDList;
	}
	
	/**
     * This method returns the client id.
     * @return the client id of order.
     */
	public String getMvClientID() {
		return mvClientID;
	}
	
	/**
     * This method sets the client id.
     * @param pClientID The client id of order.
     */
	public void setMvClientID(String pClientID) {
		mvClientID = pClientID;
	}
	
	/**
     * This method returns the date time of order.
     * @return the date time id of order.
     */
	public String getMvDateTime() {
		return mvDateTime;
	}
	
	/**
     * This method sets the date time of order.
     * @param pDateTime The date time of order.
     */
	public void setMvDateTime(String pDateTime) {
		mvDateTime = pDateTime;
	}
	
	/**
     * This method returns the display tips of order.
     * @return the display tips id of order.
     */
	public String getMvDisplayTips() {
		return mvDisplayTips;
	}
	
	/**
     * This method sets the display tips of order.
     * @param pDisplayTips The display tips of order.
     */
	public void setMvDisplayTips(String pDisplayTips) {
		mvDisplayTips = pDisplayTips;
	}
	
	/**
     * This method returns the last login date of client.
     * @return the last login date of client.
     */
	public String getMvLastLoginDateDisplay() {
		return mvLastLoginDateDisplay;
	}
	
	/**
     * This method sets the last login date of client.
     * @param pLastLoginDateDisplay The last login date of client.
     */
	public void setMvLastLoginDateDisplay(String pLastLoginDateDisplay) {
		mvLastLoginDateDisplay = pLastLoginDateDisplay;
	}
	
	/**
     * This method returns the last login time of client.
     * @return the last login time of client.
     */
	public String getMvLastLoginTimeDisplay() {
		return mvLastLoginTimeDisplay;
	}
	
	/**
     * This method sets the last login time of client.
     * @param pLastLoginTimeDisplay The last login time of client.
     */
	public void setMvLastLoginTimeDisplay(String pLastLoginTimeDisplay) {
		mvLastLoginTimeDisplay = pLastLoginTimeDisplay;
	}
	
	/**
     * This method returns the password if confirm or not.
     * @return the password if confirm or not.
     *         [0] Y is confirm the password.
     *         [1] N is not confirm the password.
     */
	public String getMvPassConfirmation() {
		return mvPassConfirmation;
	}
	
	/**
     * This method sets the password if confirm or not.
     * @param pPassConfirmation The password if confirm or not.
     */
	public void setMvPassConfirmation(String pPassConfirmation) {
		mvPassConfirmation = pPassConfirmation;
	}
	
	/**
     * This method returns the good till date if allow of order.
     * @return the good till date if allow of order.
     */
	public String getMvAllowGoodTillDate() {
		return mvAllowGoodTillDate;
	}
	
	/**
     * This method sets the good till date if allow of order.
     * @param pAllowGoodTillDate The good till date if allow of order.
     */
	public void setMvAllowGoodTillDate(String pAllowGoodTillDate) {
		mvAllowGoodTillDate = pAllowGoodTillDate;
	}
	
	/**
     * This method returns the good till date of order.
     * @return the good till date of order is formatted.
     */
	public String getMvGoodTillDate() {
		return mvGoodTillDate;
	}
	
	/**
     * This method sets the good till date of order.
     * @param pGoodTillDate The good till date of order is formatted.
     */
	public void setMvGoodTillDate(String pGoodTillDate) {
		mvGoodTillDate = pGoodTillDate;
	}
	
	/**
     * This method returns the good till date pop up of order.
     * @return the good till date pop up.
     */
	public String getMvDisplayGoodTillDatePopup() {
		return mvDisplayGoodTillDatePopup;
	}
	
	/**
     * This method sets the good till date pop up of order.
     * @param pDisplayGoodTillDatePopup The good till date pop up.
     */
	public void setMvDisplayGoodTillDatePopup(String pDisplayGoodTillDatePopup) {
		mvDisplayGoodTillDatePopup = pDisplayGoodTillDatePopup;
	}
	
	/**
     * This method returns the good till date alert.
     * @return the good till date alert.
     */
	public String getMvDisplayGoodTillDateAlert() {
		return mvDisplayGoodTillDateAlert;
	}
	
	/**
     * This method sets the good till date alert.
     * @param pDisplayGoodTillDateAlert The good till date alert.
     */
	public void setMvDisplayGoodTillDateAlert(String pDisplayGoodTillDateAlert) {
		mvDisplayGoodTillDateAlert = pDisplayGoodTillDateAlert;
	}
	
	/**
     * This method returns the market id of order.
     * @return the market id of order.
     */
	public String getMvMarketID() {
		return mvMarketID;
	}
	
	/**
     * This method sets the market id of order.
     * @param pMarketID The market id of order.
     */
	public void setMvMarketID(String pMarketID) {
		mvMarketID = pMarketID;
	}
	
	/**
     * This method returns the market good till date option.
     * @return the market good till date option.
     */
	public String getMvMarketGoodTillDateOption() {
		return mvMarketGoodTillDateOption;
	}
	
	/**
     * This method sets the market good till date option.
     * @param pMarketGoodTillDateOption The market good till date option.
     */
	public void setMvMarketGoodTillDateOption(String pMarketGoodTillDateOption) {
		mvMarketGoodTillDateOption = pMarketGoodTillDateOption;
	}
	
	/**
     * This method returns the stock search if display.
     * @return the stock search if display.
     *        [0] false is hidden stock search.
     *        [1] true is display stock search.
     */
	public String getMvDisplayStockSearch() {
		return mvDisplayStockSearch;
	}
	
	/**
     * This method sets the stock search if display.
     * @param pDisplayStockSearch The stock search if display option
     */
	public void setMvDisplayStockSearch(String pDisplayStockSearch) {
		mvDisplayStockSearch = pDisplayStockSearch;
	}
	
	/**
     * This method returns the user if need to confirm.
     * @return the user if need to confirm.
     *        [0] false is not need to confirm.
     *        [1] true is need to confirm.
     */
	public String getMvRequestUserConfirmation() {
		return mvRequestUserConfirmation;
	}
	
	/**
     * This method sets the user if need to confirm.
     * @param pRequestUserConfirmation The user if need to confirm.
     */
	public void setMvRequestUserConfirmation(String pRequestUserConfirmation) {
		mvRequestUserConfirmation = pRequestUserConfirmation;
	}
	
	/**
     * This method returns the stock name.
     * @return the stock name of the order.
     */
	public String getMvStockName() {
		return mvStockName;
	}
	
	/**
     * This method sets the stock name.
     * @param pStockName The stock name of the order.
     */
	public void setMvStockName(String pStockName) {
		mvStockName = pStockName;
	}
	
	/**
     * This method returns the message which form txn.
     * @return the message.
     */
	public String getMvTxnMessage() {
		return mvTxnMessage;
	}
	
	/**
     * This method sets the message which form txn.
     * @param pTxnMessage The message.
     */
	public void setMvTxnMessage(String pTxnMessage) {
		mvTxnMessage = pTxnMessage;
	}
	
	/**
     * This method returns the buy or sell.
     * @return the buy or sell of the order.
     *         [0] Buy.
     *         [1] Sell
     */
	public String getMvBuySell() {
		return mvBuySell;
	}
	
	/**
     * This method sets the buy or sell.
     * @param pBuySell The buy or sell of the order.
     */
	public void setMvBuySell(String pBuySell) {
		mvBuySell = pBuySell;
	}
	
	/**
     * This method returns the type of order.
     * @return the type of order.
     */
	public String getMvOrderType() {
		return mvOrderType;
	}
	
	/**
     * This method sets the type of order.
     * @param pOrderType The type of order is formatted.
     */
	public void setMvOrderType(String pOrderType) {
		mvOrderType = pOrderType;
	}
	
	/**
     * This method returns the stock code of order.
     * @return the stock code of order.
     */
	public String getMvStockCode() {
		return mvStockCode;
	}
	
	/**
     * This method sets the stock code of order.
     * @param pStockCode The stock code of order.
     */
	public void setMvStockCode(String pStockCode) {
		mvStockCode = pStockCode;
	}
	
	/**
     * This method returns the price of order.
     * @return the price of order is formatted.
     */
	public String getMvPrice() {
		return mvPrice;
	}
	
	/**
     * This method sets the price of order.
     * @param pPrice The price of order is formatted.
     */
	public void setMvPrice(String pPrice) {
		mvPrice = pPrice;
	}
	
	/**
     * This method returns the quantity of order.
     * @return the quantity of order is formatted.
     */
	public String getMvQuantity() {
		return mvQuantity;
	}
	
	/**
     * This method sets the quantity of order.
     * @param pQuantity The quantity of order is formatted.
     */
	public void setMvQuantity(String pQuantity) {
		mvQuantity = pQuantity;
	}
	
	/**
     * This method returns the error type of order.
     * @return the error type of order.
     */
	public String getMvErrorType() {
		return mvErrorType;
	}
	
	/**
     * This method sets the error type of order.
     * @param pErrorType The error type of order.
     */
	public void setMvErrorType(String pErrorType) {
		mvErrorType = pErrorType;
	}
	
	/**
     * This method returns the auction value true or false of order
     * to control check box show or hidden.
     * @return the auction value true or false.
     *         [0] true is show check box.
     *         [1] false is hidden check box.
     */
	public String getMvCBAuction() {
		return mvCBAuction;
	}
	
	/**
     * This method sets the auction value true or false.
     * @param pCBAuction The auction value true or false of order.
     */
	public void setMvCBAuction(String pCBAuction) {
		mvCBAuction = pCBAuction;
	}
	
	/**
     * This method returns the check box value Y or N
     * to control trigger order check box show or hidden.
     * @return the auction value Y or N.
     *         [0] Y is show check box.
     *         [1] N is hidden check box.
     */
	public String getMvCBStop() {
		return mvCBStop;
	}
	
	/**
     * This method sets the check box value Y or N.
     * @param pCBStop The check box value Y or N of trigger order.
     */
	public void setMvCBStop(String pCBStop) {
		mvCBStop = pCBStop;
	}
	
	/**
     * This method returns the stop type of order.
     * @return the stop type of order.
     */
	public String getMvStopType() {
		return mvStopType;
	}
	
	/**
     * This method sets the stop type of order.
     * @param pStopType The stop type of order.
     */
	public void setMvStopType(String pStopType) {
		mvStopType = pStopType;
	}
	
	/**
     * This method returns the stop price of order.
     * @return the stop price of order.
     */
	public String getMvStopPrice() {
		return mvStopPrice;
	}
	
	/**
     * This method sets the stop price of order.
     * @param pStopPrice The stop price of order.
     */
	public void setMvStopPrice(String pStopPrice) {
		mvStopPrice = pStopPrice;
	}
	
	/**
     * This method returns the check box value false or true
     * to control all or nothing check box show or hidden.
     * @return the false or true.
     *         [0] false is hidden check box.
     *         [1] true is show check box.
     */
	public String getMvCBAON() {
		return mvCBAON;
	}
	
	/**
     * This method sets the check box value Y or N.
     * @param pCBAON The check box value Y or N of all or nothing check box.
     */
	public void setMvCBAON(String pCBAON) {
		mvCBAON = pCBAON;
	}
	
	/**
     * This method returns the password if saved or not.
     * @return the password if saved or not.
     *         [0] Y is hidden the password textfield.
     *         [1] N is show the password textfield.
     */
	public String getMvCBPassSaved() {
		return mvCBPassSaved;
	}
	
	/**
     * This method sets the password if saved or not.
     * @param pCBPassSaved The password if saved or not.
     */
	public void setMvCBPassSaved(String pCBPassSaved) {
		mvCBPassSaved = pCBPassSaved;
	}
	
	/**
     * This method returns the price list of order.
     * @return the price list of order.
     */
	public String getMvOrderTypeListOption() {
		return mvOrderTypeListOption;
	}
	
	/**
     * This method sets the type list of order.
     * @param pOrderTypeListOption The type list of order.
     */
	public void setMvOrderTypeListOption(String pOrderTypeListOption) {
		mvOrderTypeListOption = pOrderTypeListOption;
	}
	
	/**
     * This method returns the good till date list of order.
     * @return the good till date list of order.
     */
	public List getMvGoodTillDateValue() {
		return mvGoodTillDateValue;
	}
	
	/**
     * This method sets the good till date list of order.
     * @param pGoodTillDateValue The good till date list of order.
     */
	public void setMvGoodTillDateValue(List pGoodTillDateValue) {
		mvGoodTillDateValue = pGoodTillDateValue;
	}
	
	/**
     * This method returns the buy button color.
     * @return the buy button color.
     */
	public String getMvBuyButtonColor() {
		return mvBuyButtonColor;
	}
	
	/**
     * This method sets the buy button color.
     * @param pBuyButtonColor The buy button color.
     */
	public void setMvBuyButtonColor(String pBuyButtonColor) {
		mvBuyButtonColor = pBuyButtonColor;
	}
	
	/**
     * This method returns the sell button color.
     * @return the sell button color.
     */
	public String getMvSellButtonColor() {
		return mvSellButtonColor;
	}
	
	/**
     * This method sets the sell button color.
     * @param pSellButtonColor The sell button color.
     */
	public void setMvSellButtonColor(String pSellButtonColor) {
		mvSellButtonColor = pSellButtonColor;
	}
	
	/**
     * This method returns the stock search description.
     * @return the stock search description.
     */
	public String getMvStockSearchDescription() {
		return mvStockSearchDescription;
	}
	
	/**
     * This method sets the stock search description.
     * @param pStockSearchDescription The stock search description.
     */
	public void setMvStockSearchDescription(String pStockSearchDescription) {
		mvStockSearchDescription = pStockSearchDescription;
	}
	
	/**
     * This method returns the quantity description.
     * @return the quantity description of the order.
     */
	public String getMvQuantityDescription() {
		return mvQuantityDescription;
	}
	
	/**
     * This method sets the quantity description.
     * @param pQuantityDescription The quantity description of the order.
     */
	public void setMvQuantityDescription(String pQuantityDescription) {
		mvQuantityDescription = pQuantityDescription;
	}
	
	/**
     * This method returns the order type description.
     * @return the order type description of the order.
     */
	public String getMvOrderTypeDescription() {
		return mvOrderTypeDescription;
	}
	
	/**
     * This method sets the order type description.
     * @param pOrderTypeDescription The order type description of the order.
     */
	public void setMvOrderTypeDescription(String pOrderTypeDescription) {
		mvOrderTypeDescription = pOrderTypeDescription;
	}
	
	/**
     * This method returns the good till date description.
     * @return the good till date description of the order.
     */
	public String getMvGoodTillDescription() {
		return mvGoodTillDescription;
	}
	
	/**
     * This method sets the good till date description.
     * @param pGoodTillDescription The good till date description of the order.
     */
	public void setMvGoodTillDescription(String pGoodTillDescription) {
		mvGoodTillDescription = pGoodTillDescription;
	}
	
	/**
     * This method returns the last order type.
     * @return the last order type of the order.
     */
	public String getMvLastOrderType() {
		return mvLastOrderType;
	}
	
	/**
     * This method sets the last order type.
     * @param pLastOrderType The last order type of the order.
     */
	public void setMvLastOrderType(String pLastOrderType) {
		mvLastOrderType = pLastOrderType;
	}
	
	/**
     * This method returns the last input good till date.
     * @return the last input good till date of the order.
     */
	public String getMvLastInputGoodTillDate() {
		return mvLastInputGoodTillDate;
	}
	
	/**
     * This method sets the last input good till date.
     * @param pLastInputGoodTillDate The last input good till date of the order.
     */
	public void setMvLastInputGoodTillDate(String pLastInputGoodTillDate) {
		mvLastInputGoodTillDate = pLastInputGoodTillDate;
	}
	
	/**
     * This method returns the current trade date.
     * @return the current trade date of the order.
     */
	public String getMvCurrentTradeDate() {
		return mvCurrentTradeDate;
	}
	
	/**
     * This method sets the current trade date.
     * @param pCurrentTradeDate The current trade date of the order.
     */
	public void setMvCurrentTradeDate(String pCurrentTradeDate) {
		mvCurrentTradeDate = pCurrentTradeDate;
	}
	
	/**
     * This method returns the type of transaction.
     * @return the type of transaction.
     */
	public String getMvTransType() {
		return mvTransType;
	}
	
	/**
     * This method sets the type of transaction.
     * @param pTransType The type of transaction.
     */
	public void setMvTransType(String pTransType) {
		mvTransType = pTransType;
	}
	
	/**
     * This method returns the instrument id of order.
     * @return the instrument id of order.
     */
	public String getMvInstrumentID() {
		return mvInstrumentID;
	}
	
	/**
     * This method sets the instrument id of order.
     * @param pInstrumentId The instrument id of order.
     */
	public void setMvInstrumentID(String pInstrumentID) {
		mvInstrumentID = pInstrumentID;
	}
	
	/**
     * This method returns the aastock login url.
     * @return the aastock login url .
     */
	public String getMvAALoginURL() {
		return mvAALoginURL;
	}
	
	/**
     * This method sets the aastock login url.
     * @param pAALoginURL The aastock login url.
     */
	public void setMvAALoginURL(String pAALoginURL) {
		mvAALoginURL = pAALoginURL;
	}
	
	/**
     * This method returns the error message when enter order.
     * @return the error message.
     */
	public String getMvErrorMsg() {
		return mvErrorMsg;
	}
	
	/**
     * This method sets the error message when enter order
     * @param pErrorMsg The error message.
     */
	public void setMvErrorMsg(String pErrorMsg) {
		mvErrorMsg = pErrorMsg;
	}
	
	/**
     * This method returns the last login date.
     * @return the last login date.
     */
	public String getMvLastLoginDate() {
		return mvLastLoginDate;
	}
	
	/**
     * This method sets the last login date
     * @param pErrorMsg The last login date.
     */
	public void setMvLastLoginDate(String pLastLoginDate) {
		mvLastLoginDate = pLastLoginDate;
	}
	
	/**
     * This method returns the last login time.
     * @return the last login time.
     */
	public String getMvLastLoginTime() {
		return mvLastLoginTime;
	}
	
	/**
     * This method sets the last login time
     * @param pLastLoginTime The last login time.
     */
	public void setMvLastLoginTime(String pLastLoginTime) {
		mvLastLoginTime = pLastLoginTime;
	}
	
}
