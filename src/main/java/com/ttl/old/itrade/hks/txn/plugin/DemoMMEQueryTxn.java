package com.ttl.old.itrade.hks.txn.plugin;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.GenericRequest;
import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.tag.ITagXsfTagName;
import com.systekit.winvest.hks.config.mapping.*;

public class DemoMMEQueryTxn extends BaseTxn {
	
	private String stockID;
	private String stockName;

	public DemoMMEQueryTxn(String stockID) {
		super();
		this.stockID = stockID;
	}

	public String getStockName() {
		return stockName;
	}

	private String mvErrorMessageCode = null;
	
	public String getErrorMessageCode() {
		return mvErrorMessageCode;
	}

	public void setErrorMessageCode(String pErrorMessageCode) {
		mvErrorMessageCode = pErrorMessageCode;
	}

	public void process() {
		GenericRequest gr = new GenericRequest(ITradeServlet.getTpManager(), null);
		IMsgXMLNode requestNode = buildMessage(gr.buildQRYPOPUPXMLHeader());
		IMsgXMLNode responseNode = gr.sendRequestXML(requestNode);
		
      	if(responseNode == null) {
      		setErrorMessageCode("COMMUNICATION_ERROR");
      	}
      	else if(responseNode.getChildNode("C_ERROR_CODE")!=null) {
      		setErrorMessageCode("SERVER_ERROR");
      	}
      	else {
      		IMsgXMLNodeList lvNodeList = responseNode.getChildNode(ITagXsfTagName.LOOP_ROW_VALUE).getNodeList(ITagXsfTagName.ROW_VALUE);
      		// non-existing entitlement id
      		if (lvNodeList.isEmpty()) {	
      			setErrorMessageCode("NONEXISTING_STOCKID");
				return;
			}
			
      		stockName = lvNodeList.getNode(0).getAttribute("v0");
      	}
	}
   
	private IMsgXMLNode buildMessage(IMsgXMLNode node) {
		node.addChildNode(TagName.FuncName);
		node.addChildNode(TagName.ObjectName).setValue("SCINSTRUMENT");
		node.addChildNode(TagName.LoopSelect);
		node.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field).setAttribute(TagName.Name, "INSTRUMENTNAME");
		IMsgXMLNode conditionLoop = node.addChildNode(TagName.LoopWhere).addChildNode(TagName.Condition);
		IMsgXMLNode conditionNode = conditionLoop.addChildNode(TagName.Condition);
		conditionNode.setAttribute(TagName.FieldName, "INSTRUMENTID");
		conditionNode.setAttribute(TagName.FieldValue, stockID);
		conditionNode.setAttribute(TagName.FieldOperation, "=");
		node.addChildNode(TagName.LoopOrderBy);
		return node;
	}
}