package com.ttl.old.itrade.hks.bean;

/**
 * The HKSQueryFundTransferBean class define variables that to save values 
 * for action 
 * @author
 *
 */
public class HKSQueryFundTransferBean {
	private String mvTransferStatus;
	private String mvTransferId;
	private String mvErrorMsg;
	private String mvType;
	private String mvAmount;
	
	/**
	 * This method returns the transfer id.
	 * @return the transfer id is not formatted.
     */
	public String getMvTransferId() {
		return mvTransferId;
	}
	
	/**
     * This method sets the transfer id.
     * @param pTransferId The transfer id.
     */
	public void setMvTransferId(String pTransferId) {
		mvTransferId = pTransferId;
	}
	
	/**
	 * This method returns the transfer status.
	 * @return the transfer status is not formatted.
     */
	public String getMvTransferStatus() {
		return mvTransferStatus;
	}
	
	/**
     * This method sets the transfer status.
     * @param pTransferStatus The transfer status.
     */
	public void setMvTransferStatus(String pTransferStatus) {
		mvTransferStatus = pTransferStatus;
	}
	
	/**
	 * This method returns the error message.
	 * @return the error message is not formatted.
     */
	public String getMvErrorMsg() {
		return mvErrorMsg;
	}
	
	/**
     * This method sets the error message.
     * @param pErrorMsg The error message.
     */
	public void setMvErrorMsg(String pErrorMsg) {
		mvErrorMsg = pErrorMsg;
	}
	
	/**
	 * This method returns the type.
	 * @return the type is not formatted.
     */
	public String getMvType() {
		return mvType;
	}
	
	/**
     * This method sets the type.
     * @param pType The type.
     */
	public void setMvType(String pType) {
		mvType = pType;
	}
	
	/**
	 * This method returns the amount.
	 * @return the amount is not formatted.
     */
	public String getMvAmount() {
		return mvAmount;
	}
	
	/**
     * This method sets the amount.
     * @param pAmount The amount.
     */
	public void setMvAmount(String pAmount) {
		mvAmount = pAmount;
	}
	
}
