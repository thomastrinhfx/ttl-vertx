package com.ttl.old.itrade.hks.txn;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.tag.ITagXsfTagName;

/**
 * The HKSIPOFinancingPlanTxn class definition for all method
 * Planning IPO Financing Plan
 * @author User1
 *
 */
public class HKSIPOFinancingPlanTxn extends BaseTxn {
	/**
	 * This variable is used to the tp Error
	 */
	private TPErrorHandling tpError;
	private int mvReturnCode;
    private String mvErrorCode;
    private String mvErrorMessage;
    
	private String mvEntitlementID;
	private Vector mvResultLoanAmountLessThan;
	private Vector mvResultInterestRate;
    /**
     * Default constructor for HKSIPOFinancingPlanTxn class
     */
	public HKSIPOFinancingPlanTxn(){
		tpError = new TPErrorHandling();
	}
	/**
	 * Default constructor for HKSIPOFinancingPlanTxn class
	 * @param pEntitlementID the entitlement id
	 */
	public HKSIPOFinancingPlanTxn(String pEntitlementID){
		tpError = new TPErrorHandling();
		setEntitlementID(pEntitlementID);
	}
	/**
	 * This method process for Planning IPO Financing Plan
	 * @param pEntitlementID the entitlement id
	 * @return Planning IPO Financing Plan
	 */
	public Vector process(String pEntitlementID){
		Vector lvReturn = new Vector();
		try{
			Log.println("HKSIPOFinancingPlanTxn starts", Log.ACCESS_LOG);
			
			Hashtable lvTxnMap = new Hashtable();
			
			lvTxnMap.put("ENTITLEMENTID", pEntitlementID);
			
			if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSMarginPlanRequest, lvTxnMap)){
				IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList(ITagXsfTagName.ROW);
				for(int i=0; i< lvRowList.size(); i++){
					IMsgXMLNode lvNode = lvRowList.getNode(i);
					HashMap lvModel = new HashMap();
					lvModel.put("LOANAMOUNTLESSTHAN", lvNode.getChildNode("LOANAMOUNTLESSTHAN").getValue());
					lvModel.put("INTERESTRATE", lvNode.getChildNode("INTERESTRATE").getValue());
					lvReturn.add(lvModel);
				}
			}
		}catch(Exception e)
		{	
			Log.println("HKSIPOFinancingPlanTxn error" + e.toString(), Log.ERROR_LOG);
		}
		return lvReturn;
	}
	
	/**
	 * Get method for entitlement id
	 * @return entitlement id
	 */
	public String getEntitlementID(){
		return mvEntitlementID;
	}
	/**
	 * Set method for entitlement id
	 * @param pEntitlementID the entitlement id
	 */
	public void setEntitlementID(String pEntitlementID){
		this.mvEntitlementID=pEntitlementID;
	}
	/**
	 * Set method for return system code
	 * @param pReturnCode the return system code
	 */
    public void setReturnCode(int pReturnCode)
    {
        mvReturnCode = pReturnCode;
    }
    /**
     * Set method for system error code
     * @param pErrorCode the system error code
     */
    public void setErrorCode(String pErrorCode)
    {
        mvErrorCode = pErrorCode;
    }
    /**
     * Set method for system error message
     * @param pErrorMessage the system error message
     */
    public void setErrorMessage(String pErrorMessage)
    {
        mvErrorMessage = pErrorMessage;
    }
}
