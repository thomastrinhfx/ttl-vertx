package com.ttl.old.itrade.hks.util;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

import java.util.*;

public class TableFormatter
{


	public static String makeHtmlTableRow(String fields, String delimiter, String trAttributes, String tdAttributes)
	{

		String			tableRow = "<TR " + trAttributes + ">\n";

//		StringTokenizer fieldToken = new StringTokenizer(fields, delimiter);
        String [] fieldArray = TextFormatter.split(fields, delimiter);

        for (int i = 0; i < fieldArray.length; i++)
        {
            tableRow = tableRow + "<TD " + tdAttributes + ">" + fieldArray[i].trim() + "</TD>\n";
        }

//		while (fieldToken.hasMoreTokens())
//		{
//			tableRow = tableRow + "<TD " + tdAttributes + ">" + fieldToken.nextToken().trim() + "</TD>\n";
//		}
		tableRow = tableRow + "</TR>\n";

		return tableRow;
	}


	public static String makeHtmlTableRow(String fields, String delimiter)
	{
		return makeHtmlTableRow(fields, delimiter, "", "");
	}


	public static String makeHtmlTableRow(Hashtable hMap, String fieldOrder, String delimiter, String trAttributes, String tdAttributes)
	{

		String			fields = "";
		String			tempStr;

		//StringTokenizer fieldToken = new StringTokenizer(fieldOrder, delimiter);
      String[] fieldArray = TextFormatter.split(fieldOrder, delimiter);

      for (int i = 0; i < fieldArray.length; i++)
      {
         if (i != 0)
            fields += delimiter;
         tempStr = (String) hMap.get(fieldArray[i]);
         if ( (tempStr == null) || tempStr.equals(""))
            fields += " ";
         else
            fields += tempStr;
      }

//		while (fieldToken.hasMoreTokens())
//		{
//			tempStr = (String) hMap.get(fieldToken.nextToken());
//			if ((tempStr == null) || tempStr.equals(""))
//			{
//				fields = fields + " " + delimiter;
//			}
//			else
//			{
//				fields = fields + tempStr + delimiter;
//			}
//		}

		return makeHtmlTableRow(fields, delimiter, trAttributes, tdAttributes);
	}


	public static String makeHtmlTableRow(Hashtable hMap, String fieldOrder, String delimiter)
	{
		return makeHtmlTableRow(hMap, fieldOrder, delimiter, "", "");
	}
}
