package com.ttl.old.itrade.hks.util;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class StringSplitter {
	public static String[] split(String pString, String pDelim) {
		ArrayList result = new ArrayList(); 
		String lvString = pString;
		String lvDelim = pDelim;
		String lvToken = "";
		boolean lvLastTokenWasDelim = false;
		StringTokenizer stringTokenizer = new StringTokenizer(lvString, lvDelim, true); 

		while (stringTokenizer.hasMoreTokens()) { 
			lvToken = stringTokenizer.nextToken();
			if (lvToken.equals("|")) {
				if (lvLastTokenWasDelim) {
					result.add("");
				}
				lvLastTokenWasDelim = true;
			} else {
				result.add(lvToken);
				lvLastTokenWasDelim = false;
			}
			//result.add(stringTokenizer.nextToken()); 
		} 
		if (lvLastTokenWasDelim) {
			result.add("");
		}
		return (String[])result.toArray(new String[result.size()]); 
	}
}