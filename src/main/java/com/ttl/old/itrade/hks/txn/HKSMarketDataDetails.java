// ************************************
// Modified by Bowen Chau on 7 Mar 2006
// Added PendingQty methods
// ************************************

package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;

/**
 * Title:        Order Enquiry Details
 * Description:  This class stores the order enquiry details
 * Copyright:    Copyright (c) 2002
 * Company:      SYSTEK IT Ltd.
 * @author
 * @version 1.0
 * @since 2002.12.03
 */
/**
 * The HKSMarketDateDetaliTxn class entity
 * @author not attributable
 * @since 2002.12.03
 */
public class HKSMarketDataDetails
{
	private Hashtable mvHMap;
	/**
	 * Get method for hashtable 
	 * @return hashtable 
	 */
	public Hashtable getHashtable()
	{
		return mvHMap;
	}
	/**
	 * Set method for hashtable 
	 * @param pHMap the hashtable
	 */
	public void setHashtable(Hashtable pHMap)
	{
		mvHMap = pHMap;
	}
}
