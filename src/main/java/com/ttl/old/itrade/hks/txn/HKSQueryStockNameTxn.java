package com.ttl.old.itrade.hks.txn;


import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

import com.ttl.old.itrade.hks.util.HKSInstrument;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.ErrorCode;
import com.ttl.old.itrade.util.Log;
import com.systekit.winvest.hks.util.Utils;
/**
 * This class processes the query stock name
 * @author Charlie Liu
 *
 */
public class HKSQueryStockNameTxn extends BaseTxn {
	
	private String mvMarketId;
	private String mvInstrumentId;
	private String mvResult = "";
	private String mvInstrumentFullName;
	private String mvInstrumentShortName;
	private String mvInstrumentChineseShortName;
	private String mvInstrumentChineseName;
	private static Properties _pIni;
    protected Vector svUsingStockCodeFormatMarket;
	private int mvStockReturnCode = 0;
	/**
	 * Set method for properties
	 * @param p the Properties class
	 */
	static public void setProperties(Properties p){
    	_pIni = p;
    }
	/**
	 * Constructor for HKSQueryStockNameTxn class
	 * @param pMarketId the market id
	 * @param pInstrumentId the instrument id
	 */
	public HKSQueryStockNameTxn(String pMarketId, String pInstrumentId)
	{
		int lvAddZero = 5 - pInstrumentId.length();
		String lvTempInstrumentId = "";
		for(int lvI = 0; lvI < lvAddZero; lvI++){
			lvTempInstrumentId += "0";
		}
		pInstrumentId = lvTempInstrumentId + pInstrumentId;
		mvMarketId = Utils.isNullStr(mvMarketId) ? "HKEX" : pMarketId;
		this.mvInstrumentId = pInstrumentId;
	}
	
	/**
	 * The method process the query stock name
	 */
	public void process()
    {
		try{
    		//From Walter
			//BEGIN TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
			String lvUsingStockCodeFormatMarket = Utils.isNullStr(IMain.getProperty("UsingStockCodeFormatMarket")) ? "" : IMain.getProperty("UsingStockCodeFormatMarket") ;
    		//END TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
            if(!Utils.isNullStr(lvUsingStockCodeFormatMarket))
            {
                    svUsingStockCodeFormatMarket = new Vector();
                    StringTokenizer lvTokenList = new StringTokenizer(lvUsingStockCodeFormatMarket, ",");
                    while (lvTokenList.hasMoreTokens())
                    {
                            String lvMarketID = lvTokenList.nextToken();
                            if (lvMarketID != null)
                            {
                                    svUsingStockCodeFormatMarket.add(lvMarketID.trim());
                            }
                    }
            }

            //End Walter
			
	        HKSInstrument lvInstrument = getInstrument(mvMarketId, mvInstrumentId);
	
	        if (lvInstrument == null)
	        {
	         setStockReturnCode(1);
	       	 // stock code invalid error
	       	 mvErrorCode = new ErrorCode(new String[0], "HKSRISK0002", "Instrument does not belong to Exchange.");
	        }
	        else{
	        	setStockReturnCode(0);
		        setInstrumentId(lvInstrument.getInstrumentID());
		        setInstrumentFullName(lvInstrument.getInstrumentName());
		        setInstrumentShortName(lvInstrument.getShortName());
		        setInstrumentChineseName(lvInstrument.getChineseNameAPI());
		        setInstrumentChineseShortName(lvInstrument.getChineseShortNameAPI());
	        }
    	}
    	catch(NullPointerException npe)
        {
                npe.printStackTrace();
                Log.println("[HKSQueryStockNameTxn.process(): Source is null]", Log.ACCESS_LOG);
        }catch(Exception ex)
        {
                ex.printStackTrace();
                Log.println("[HKSQueryStockNameTxn.process(): Source Error ]", Log.ACCESS_LOG);
        }      
    }
	
	/**
	 * Get method for result
	 * @return result
	 */
    public String getResult()
    {
    	return mvResult;
    }
    /**
     * Get method for stock return system code
     * @return  stock return system code
     */
    public int getStockReturnCode(){
    	return mvStockReturnCode;
    }
    /**
     * Set method for stock return system code
     * @param pStockReturnCode the  stock return system code
     */
    public void setStockReturnCode(int pStockReturnCode){
    	mvStockReturnCode = pStockReturnCode;
    }
	/**
	 * Set method for instrument id
	 * @param pInstrumentId the instrument id
	 */
    public void setInstrumentId(String pInstrumentId){
    	mvInstrumentId = pInstrumentId;
    }
    /**
     * Set method for instrument full name
     * @param pInstrumentFullName the instrument full name
     */
    public void setInstrumentFullName(String pInstrumentFullName){
    	mvInstrumentFullName = pInstrumentFullName;
    }
    /**
     * Set method for instrument short name
     * @param pInstrumentShortName the instrument short name
     */
    public void setInstrumentShortName(String pInstrumentShortName){
    	mvInstrumentShortName = pInstrumentShortName;
    }
    /**
     * Set method for instrument chinese short name
     * @param pInstrumentChineseShortName the instrument chinese short name
     */
    public void setInstrumentChineseShortName(String pInstrumentChineseShortName){
    	mvInstrumentChineseShortName = pInstrumentChineseShortName;
    }
    /**
     * Set method for instrument chinese name
     * @param pInstrumentChineseName the instrument chinese name
     */
    public void setInstrumentChineseName(String pInstrumentChineseName){
    	mvInstrumentChineseName = pInstrumentChineseName;
    }
    /**
     * Get method for instrument id
     * @return instrument id
     */
    public String getInstrumentId(){
    	return mvInstrumentId;
    }
    /**
     * Get method for instrument full name
     * @return instrument full name
     */
    public String getInstrumentFullName(){
    	return mvInstrumentFullName;
    }
    /**
     * Get method for instrument short name
     * @return instrument short name
     */
    public String getInstrumentShortName(){
    	return mvInstrumentShortName;
    }
    /**
     * Get method for instrument chinese short name
     * @return the instrument chinese short name
     */
    public String getInstrumentChineseShortName(){
    	return mvInstrumentChineseShortName;
    }
    /**
     * Get method for instrument chinese name
     * @return instrument chinese name
     */
    public String getInstrumentChineseName(){
    	return mvInstrumentChineseName;
    }
    
    //From EnterOrder action
    /**
     * This method for instrument
     * @param pMarketID the market id
     * @param pStockCode the stock code
     * @return instrument result set
     */
    protected HKSInstrument getInstrument(String pMarketID, String pStockCode)
    {
        HKSInstrument lvHKSInstrument = new HKSInstrument();
                  if(!Utils.isNullStr(pMarketID))
                  {
                          Log.println("[ DefaultAction MarketID : " + pMarketID + "]", Log.DEBUG_LOG);
                          if (svUsingStockCodeFormatMarket != null && svUsingStockCodeFormatMarket.contains( (String) pMarketID))
                          {
                                  lvHKSInstrument = (HKSInstrument) TPManager.mvInstrumentVector.get(pMarketID.concat("|").concat(Utils.getFormattedInstrumentID(pStockCode)));
                          }
                          else
                          {
                                  lvHKSInstrument = (HKSInstrument) TPManager.mvInstrumentVector.get(pMarketID.concat("|").concat(pStockCode.toUpperCase()));
                          }
                  }
        return lvHKSInstrument;
    }
}
