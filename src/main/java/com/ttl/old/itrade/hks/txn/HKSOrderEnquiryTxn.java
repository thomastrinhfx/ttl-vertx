package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.hks.util.FieldSplitter;
import com.ttl.old.itrade.hks.util.TextFormatter;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.ErrorCode;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.winvest.hks.config.mapping.TagName;
import com.systekit.winvest.hks.util.Utils;

/**
 * The HKSOrderEnquiryTxn class definition for all method stores the order enquiry details
 * 
 * @author not attributable
 * 
 */
public class HKSOrderEnquiryTxn extends BaseTxn {
	private String mvStatus;
	private String mvClientId;
	private String mvLoopCounter;
	private String mvSorting;

	private int mvStartRecord;
	private int mvEndRecord;
	private String mvOrderType;
	private String mvOrderBS;

	// Start Task: yulong.xu 20080731

	private String mvDateTime;

	/**
	 * Get method for date time
	 * 
	 * @return date time
	 */
	public String getMvDateTime() {
		return mvDateTime;
	}

	/**
	 * Set method for date time
	 * 
	 * @param pDateTime the date time
	 */
	public void setMvDateTime(String pDateTime) {
		this.mvDateTime = pDateTime;
	}

	private String mvMarketID;
	private String mvStartDate;
	private String mvEndDate;
	private String mvOrderID;
	private String mvInstrumentID;
	private String mvBranchID;
	private String mvUserID;
	private String mvChannelID;
	private String mvOrderRef;
	private String mvCompletetionTime;
	private int mvTotalOrders;

	/**
	 * Constructor for HKSOrderEnquiryTxn class
	 * 
	 * @param pMarketID the market id
	 * @param pStartDate the start date
	 * @param pEndDate the end date
	 * @param pOrderID the order id
	 * @param pClientID the client id
	 * @param pStockID the stock id
	 * @param pBranchID the branch id
	 * @param pUserID the user id
	 * @param pChannelID the channel id
	 * @param pOrderRef the order ref
	 * @param pStatus the status
	 * @param pCompletetionTime the completetion time
	 */
	public HKSOrderEnquiryTxn(String pMarketID, String pStartDate, String pEndDate, String pOrderID, String pClientID, String pStockID,
			String pBranchID, String pUserID, String pChannelID, String pOrderRef, String pStatus, String pCompletetionTime) {

		super();
		mvClientId = pClientID;
		mvStatus = pStatus;

		mvMarketID = pMarketID;
		mvStartDate = pStartDate;
		mvEndDate = pEndDate;
		mvOrderID = pOrderID;
		mvInstrumentID = pStockID;
		mvBranchID = pBranchID;
		mvUserID = pUserID;
		mvChannelID = pChannelID;
		mvOrderRef = pOrderRef;
		mvCompletetionTime = pCompletetionTime;

	}

	/**
	 * Get method for market id
	 * 
	 * @return market id
	 */
	public String getMvMarketID() {
		return mvMarketID;
	}

	/**
	 * Set method for market id
	 * 
	 * @param pMarketID the market id
	 */
	public void setMvMarketID(String pMarketID) {
		this.mvMarketID = pMarketID;
	}

	/**
	 * Get method for start date
	 * 
	 * @return start date
	 */
	public String getMvStartDate() {
		return mvStartDate;
	}

	/**
	 * Set method for start date
	 * 
	 * @param pStartDate the start date
	 */
	public void setMvStartDate(String pStartDate) {
		this.mvStartDate = pStartDate;
	}

	/**
	 * Get method for end date
	 * 
	 * @return end date
	 */
	public String getMvEndDate() {
		return mvEndDate;
	}

	/**
	 * Set method for end date
	 * 
	 * @param pEndDate the end date
	 */
	public void setMvEndDate(String pEndDate) {
		this.mvEndDate = pEndDate;
	}

	/**
	 * Get method for order id
	 * 
	 * @return order id
	 */
	public String getMvOrderID() {
		return mvOrderID;
	}

	/**
	 * Set method for order id
	 * 
	 * @param pOrderID the order id
	 */
	public void setMvOrderID(String pOrderID) {
		this.mvOrderID = pOrderID;
	}

	/**
	 * Get method for stock id
	 * 
	 * @return stock id
	 */
	public String getMvInstrumentID() {
		return mvInstrumentID;
	}

	/**
	 * Set method for stock id
	 * 
	 * @param pStockID the stock id
	 */
	public void setMvInstrumentID(String pStockID) {
		this.mvInstrumentID = pStockID;
	}

	/**
	 * Get method for branch id
	 * 
	 * @return branch id
	 */
	public String getMvBranchID() {
		return mvBranchID;
	}

	/**
	 * Set method for branch id
	 * 
	 * @param pBranchID branch id
	 */
	public void setMvBranchID(String pBranchID) {
		this.mvBranchID = pBranchID;
	}

	/**
	 * Get method for user id
	 * 
	 * @return user id
	 */
	public String getMvUserID() {
		return mvUserID;
	}

	/**
	 * Set method for user id
	 * 
	 * @param pUserID the user id
	 */
	public void setMvUserID(String pUserID) {
		this.mvUserID = pUserID;
	}

	/**
	 * Get method for channel id
	 * 
	 * @return channel id
	 */
	public String getMvChannelID() {
		return mvChannelID;
	}

	/**
	 * Set method for channel id
	 * 
	 * @param pChannelID the channel id
	 */
	public void setMvChannelID(String pChannelID) {
		this.mvChannelID = pChannelID;
	}

	/**
	 * Get method for order ref
	 * 
	 * @return order ref
	 */
	public String getMvOrderRef() {
		return mvOrderRef;
	}

	/**
	 * Set method for order ref
	 * 
	 * @param mvOrderRef the order ref
	 */
	public void setMvOrderRef(String pOrderRef) {
		this.mvOrderRef = pOrderRef;
	}

	/**
	 * Get method for completetion time
	 * 
	 * @return completetion time
	 */
	public String getMvCompletetionTime() {
		return mvCompletetionTime;
	}

	/**
	 * Set method for completetion time
	 * 
	 * @param pCompletetionTime the completetion time
	 */
	public void setMvCompletetionTime(String pCompletetionTime) {
		this.mvCompletetionTime = pCompletetionTime;
	}

	// End Task: yulong.xu 20080731

	// Begin task YuLong Xu 04 Nov 2008
	/*
	 * private int mvReturnCode; TPErrorHandling tpError; private String mvErrorCode;
	 * 
	 * public int getMvReturnCode() { return mvReturnCode; } public void setMvReturnCode(int mvReturnCode) { this.mvReturnCode =
	 * mvReturnCode; } public String getMvErrorCode() { return mvErrorCode; } public void setMvErrorCode(String mvErrorCode) {
	 * this.mvErrorCode = mvErrorCode; }
	 */
	// End task YuLong Xu 04 Nov 2008

	private HKSOrderEnqDetails[] mvHKSOrderEnqDetails;

	/**
	 * Constructor for HKSOrderEnquiryTxn class
	 * 
	 * @param pClientId the client id
	 * @param pStatus the status
	 * @param pSorting the sorting
	 */
	public HKSOrderEnquiryTxn(String pClientId, String pStatus, String pSorting) {
		super();
		mvClientId = pClientId;
		mvStatus = pStatus;
		mvSorting = pSorting;
	}

	/**
	 * The method stores the order enquiry details
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void process() {
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put(TagName.CLIENTID, getClientId());

		if (Utils.isNullStr(getSorting()))
			this.setSorting("2");
		else
			lvTxnMap.put(TagName.SORTING, getSorting());
		if (!getStatus().equals("A")) {
			lvTxnMap.put(TagName.STATUS, getStatus());
		}

		if (!Utils.isNullStr(getMvOrderType()))
			lvTxnMap.put(TagName.ORDERTYPE, getMvOrderType());

		if (!Utils.isNullStr(getMvOrderBS()))
			lvTxnMap.put("ORDERBS", getMvOrderBS());

		if (!Utils.isNullStr(getMvOrderID()))
			lvTxnMap.put(TagName.ORDERID, getMvOrderID());

		if (!Utils.isNullStr(getMvInstrumentID()))
			lvTxnMap.put(TagName.INSTRUMENTID, getMvInstrumentID());

		lvTxnMap.put("STARTRECORD", String.valueOf(mvStartRecord));
		lvTxnMap.put("ENDRECORD", String.valueOf(mvEndRecord));

		if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSOrderEnquiryRequest, lvTxnMap)) {
			IMsgXMLNode lvNodeFnames = mvReturnNode.getChildNode(TagName.RESULT).getChildNode(TagName.FieldNames);
			IMsgXMLNodeList lvNodeListValues = mvReturnNode.getChildNode(TagName.RESULT).getChildNode(TagName.LoopRows)
					.getNodeList(TagName.Values);

			if (mvReturnNode.getChildNode(TagName.TOTALQTY) != null) {
				mvTotalOrders = Integer.parseInt(mvReturnNode.getChildNode(TagName.TOTALQTY).getValue());
			}

			mvHKSOrderEnqDetails = new HKSOrderEnqDetails[lvNodeListValues.size()];

			for (int i = 0; i < lvNodeListValues.size(); i++) {
				mvHKSOrderEnqDetails[i] = new HKSOrderEnqDetails();

				String lvFieldnames = lvNodeFnames.getValue();
				String lvValues = lvNodeListValues.getNode(i).getValue();
				Hashtable lvHMap = FieldSplitter.tokenize(lvFieldnames, lvValues, "|");
				mvHKSOrderEnqDetails[i].setHashtable(lvHMap);
				mvHKSOrderEnqDetails[i].setOrderGroupID((String) lvHMap.get(TagName.ORDERGROUPID));
				mvHKSOrderEnqDetails[i].setOrderID((String) lvHMap.get(TagName.ORDERID));
				mvHKSOrderEnqDetails[i].setClientID((String) lvHMap.get(TagName.CLIENTID));
				mvHKSOrderEnqDetails[i].setStockID((String) lvHMap.get(TagName.INSTRUMENTID));
				mvHKSOrderEnqDetails[i].setBS((String) lvHMap.get(TagName.BS));
				// Begin Task: WL00619 Walter Lau 2007 July 27
				mvHKSOrderEnqDetails[i].setMarketID((String) lvHMap.get(TagName.MARKETID));
				// End Task: WL00619 Walter Lau 2007 July 27

				mvHKSOrderEnqDetails[i].setPrice((String) lvHMap.get(TagName.PRICE));
				mvHKSOrderEnqDetails[i].setQty((String) lvHMap.get(TagName.QTY));
				mvHKSOrderEnqDetails[i].setPendingPrice((String) lvHMap.get(TagName.PENDPRICE));
				mvHKSOrderEnqDetails[i].setPendingQty((String) lvHMap.get(TagName.PENDQTY));
				mvHKSOrderEnqDetails[i].setCancelQty((String) lvHMap.get(TagName.CANCELQTY));
				mvHKSOrderEnqDetails[i].setOSQty((String) lvHMap.get(TagName.OSQTY));
				mvHKSOrderEnqDetails[i].setFilledQty((String) lvHMap.get(TagName.FILLEDQTY));
				mvHKSOrderEnqDetails[i].setStatus((String) lvHMap.get(TagName.STATUS_INTERNAL));

				mvHKSOrderEnqDetails[i].setValidityDate((String) lvHMap.get(TagName.VALIDITYDATE));

				// Begin task: Edit for mitrade YuLong.Xu 20080923
				mvHKSOrderEnqDetails[i].setMvDateTime(TextFormatter.getFormattedTime((String) lvHMap.get(TagName.INPUTTIME),
						"yyyy-MM-dd HH:mm:ss"));
				// End task: Edit for mitrade YuLong.Xu 20080923

				mvHKSOrderEnqDetails[i].setInputTime(TextFormatter.getFormattedTime((String) lvHMap.get(TagName.INPUTTIME), "HH:mm:ss"));
				mvHKSOrderEnqDetails[i].setModifiedTime(TextFormatter.getFormattedTime((String) lvHMap.get(TagName.MODIFIEDTIME),
						"HH:mm:ss"));
				// BEGIN - TASK#: CL00030 - Charlie Liu 20081023
				mvHKSOrderEnqDetails[i].setModifiedDate(TextFormatter.getFormattedTime((String) lvHMap.get(TagName.MODIFIEDTIME),
						"yyyy-MM-dd"));
				mvHKSOrderEnqDetails[i].setCreateTime(TextFormatter.getFormattedTime((String) lvHMap.get(TagName.CREATETIME),
						"yyyy-MM-dd HH:mm:ss"));
				mvHKSOrderEnqDetails[i].setModifiedDateTime(TextFormatter.getFormattedTime((String) lvHMap.get(TagName.MODIFIEDTIME),
						"yyyy-MM-dd HH:mm:ss"));
				// END - TASK# CL00030
				mvHKSOrderEnqDetails[i].setEntityID((String) lvHMap.get(TagName.ENTITYID));
				mvHKSOrderEnqDetails[i].setUserID((String) lvHMap.get(TagName.USERID));
				mvHKSOrderEnqDetails[i].setDNSeq((String) lvHMap.get(TagName.DNSEQ));
				mvHKSOrderEnqDetails[i].setOrderType((String) lvHMap.get(TagName.ORDERTYPE));
				mvHKSOrderEnqDetails[i].setAON((String) lvHMap.get(TagName.ALLORNOTHING));
				mvHKSOrderEnqDetails[i].setStopOrderType((String) lvHMap.get(TagName.STOPORDERTYPE));
				mvHKSOrderEnqDetails[i].setStopPrice((String) lvHMap.get(TagName.STOPPRICE));
				mvHKSOrderEnqDetails[i].setStopOrderExpDate((String) lvHMap.get(TagName.STOPORDEREXPIRYDATE));
				mvHKSOrderEnqDetails[i].setActivationDate((String) lvHMap.get(TagName.ACTIVATIONDATE));
				mvHKSOrderEnqDetails[i].setGoodTillDate((String) lvHMap.get(TagName.GOODTILLDATE));
				mvHKSOrderEnqDetails[i].setAvgPrice((String) lvHMap.get(TagName.AVGPRICE));
				mvHKSOrderEnqDetails[i].setRemark((String) lvHMap.get(TagName.REMARK));
				mvHKSOrderEnqDetails[i].setContactPhone((String) lvHMap.get(TagName.CONTACTPHONE));
				mvHKSOrderEnqDetails[i].setGrossAmt((String) lvHMap.get(TagName.GROSSAMT));
				mvHKSOrderEnqDetails[i].setNetAmt((String) lvHMap.get(TagName.NETAMT));
				mvHKSOrderEnqDetails[i].setSCRIP((String) lvHMap.get(TagName.SCRIP));

				mvHKSOrderEnqDetails[i].setRejectReason((String) lvHMap.get(TagName.REJECTREASON));
				mvHKSOrderEnqDetails[i].setModifyOrderID((String) lvHMap.get(TagName.MODIFYORDERID));
				mvHKSOrderEnqDetails[i].setCurrencyID((String) lvHMap.get(TagName.CURRENCYID));

				// Begin Task: yulong.xu 20080731

				mvHKSOrderEnqDetails[i].setInstrumentId((String) lvHMap.get(TagName.INSTRUMENTID));
				mvHKSOrderEnqDetails[i].setShortName((String) lvHMap.get(TagName.SHORTNAME));
				mvHKSOrderEnqDetails[i].setClientRemarks((String) lvHMap.get(TagName.CLIENTREMARKS));
				mvHKSOrderEnqDetails[i].setInvestorGroupId((String) lvHMap.get(TagName.INVESTORGROUPID));
				mvHKSOrderEnqDetails[i].setInvestorClassId((String) lvHMap.get(TagName.INVESTORCLASSID));
				mvHKSOrderEnqDetails[i].setRepOrterGroupId((String) lvHMap.get(TagName.REPORTERGROUPID));
				mvHKSOrderEnqDetails[i].setStatusInternal((String) lvHMap.get(TagName.STATUS_INTERNAL));
				mvHKSOrderEnqDetails[i].setReporTime((String) lvHMap.get(TagName.REPORTTIME));
				mvHKSOrderEnqDetails[i].setReporTackTime((String) lvHMap.get(TagName.REPORTACKTIME));
				mvHKSOrderEnqDetails[i].setOgackTime((String) lvHMap.get(TagName.OGACKTIME));
				mvHKSOrderEnqDetails[i].setTradeTime((String) lvHMap.get(TagName.TRADETIME));
				mvHKSOrderEnqDetails[i].setLastTradeTime((String) lvHMap.get(TagName.LASTTRADETIME));
				mvHKSOrderEnqDetails[i].setStopTriggerTime((String) lvHMap.get(TagName.STOPTRIGGERTIME));
				mvHKSOrderEnqDetails[i].setAllorNothing((String) lvHMap.get(TagName.ALLORNOTHING));
				mvHKSOrderEnqDetails[i].setHostId((String) lvHMap.get(TagName.HOSTID));
				;
				mvHKSOrderEnqDetails[i].setInActive((String) lvHMap.get(TagName.INACTIVE));
				mvHKSOrderEnqDetails[i].setNotifiedFlag((String) lvHMap.get(TagName.NOTIFICATIONFLAG));
				mvHKSOrderEnqDetails[i].setIsPostExecutedOrder((String) lvHMap.get(TagName.ISPOSTEXECUTEDORDER));
				mvHKSOrderEnqDetails[i].setApprovalTime((String) lvHMap.get(TagName.APPROVALTIME));
				mvHKSOrderEnqDetails[i].setSupervisorId(((String) lvHMap.get(TagName.SUPERVISORID)));
				mvHKSOrderEnqDetails[i].setApprovalRemark((String) lvHMap.get(TagName.APPROVALREMARK));
				mvHKSOrderEnqDetails[i].setHoldConfirmQty((String) lvHMap.get(TagName.HOLDCONFIRMQTY));
				mvHKSOrderEnqDetails[i].setTradeConsideration((String) lvHMap.get(TagName.TRADECONSIDERATION));
				mvHKSOrderEnqDetails[i].setPrevtradeConsideration((String) lvHMap.get(TagName.PREVTRADECONSIDERATION));
				mvHKSOrderEnqDetails[i].setIsReleased((String) lvHMap.get(TagName.ISRELEASED));
				mvHKSOrderEnqDetails[i].setSupervisorrejected((String) lvHMap.get(TagName.SUPERVISORREJECTED));
				mvHKSOrderEnqDetails[i].setPendAction((String) lvHMap.get(TagName.PENDACTION));
				mvHKSOrderEnqDetails[i].setExceededAmt((String) lvHMap.get(TagName.EXCEEDEDAMT));
				mvHKSOrderEnqDetails[i].setLastModifiedUserId((String) lvHMap.get(TagName.LASTMODIFIEDUSERID));

				// End Task: yulong.xu 20080731

				// BEGIN - TASK#: CL00010 - Charlie Liu 20081120
				mvHKSOrderEnqDetails[i].setInstrumentShortName((String) lvHMap.get(TagName.INSTRUMENTSHORTNAME));
				mvHKSOrderEnqDetails[i].setBranchID((String) lvHMap.get(TagName.BRANCHID));
				mvHKSOrderEnqDetails[i].setChannelID((String) lvHMap.get(TagName.CHANNELID));
				mvHKSOrderEnqDetails[i].setBrokerID((String) lvHMap.get(TagName.BROKERID));
				mvHKSOrderEnqDetails[i].setIsManualTrade((String) lvHMap.get(TagName.ISMANUALTRADE));
				mvHKSOrderEnqDetails[i].setOrigin((String) lvHMap.get(TagName.ORIGIN));
				mvHKSOrderEnqDetails[i].setHedge((String) lvHMap.get(TagName.HEDGE));
				mvHKSOrderEnqDetails[i].setShortsell((String) lvHMap.get(TagName.SHORTSELL));
				mvHKSOrderEnqDetails[i].setIsOddLot((String) lvHMap.get(TagName.ISODDLOT));
				mvHKSOrderEnqDetails[i].setApprovalReason((String) lvHMap.get(TagName.APPROVALREASON));
				// END - TASK# CL00010
				// BEGIN - TASK: Giang Tran 20100507: Add the settlement account for order details
				mvHKSOrderEnqDetails[i].setMvBankID((String) lvHMap.get(TagName.BANKID));
				mvHKSOrderEnqDetails[i].setMvBankACID((String) lvHMap.get(TagName.BANKACID));
				// END - TASK: Giang Tran 20100507: Add the settlement account for order details

				lvHMap.remove(TagName.PRICE);
				lvHMap.remove(TagName.AVGPRICE);
				lvHMap.remove(TagName.STATUS);
				lvHMap.put(TagName.PRICE, TextFormatter.formatNumber(mvHKSOrderEnqDetails[i].getPrice(), "", "#,##0"));
				lvHMap.put(TagName.AVGPRICE, TextFormatter.formatNumber(mvHKSOrderEnqDetails[i].getAvgPrice(), "", "#,##0"));
				lvHMap.put(TagName.STATUS, mvHKSOrderEnqDetails[i].getStatus());
			}

			IMsgXMLNode lvLoopCounterNode = mvReturnNode.getChildNode(TagName.RESULT).getChildNode(TagName.LoopCounter);
			if (lvLoopCounterNode != null) {
				setLoopCounter(lvLoopCounterNode.getValue());
			} else {
				setLoopCounter("0");
			}
		} else { // Unhandled Business Exception
			setErrorCode(new ErrorCode(new String[0], mvReturnNode.getChildNode("C_ERROR_CODE").getValue(), ""));

			// Handle special cases
			// 1. Agreement is not signed
			if (mvReturnNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase("HKSFOE00026")) {
				setErrorCode(new ErrorCode(new String[0], mvReturnNode.getChildNode("C_ERROR_CODE").getValue(), mvReturnNode.getChildNode(
						"C_ERROR_DESC").getValue()));
			}

			// 2. When TP is down
			if (mvReturnNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase("TPBASEREQERROR")) {
				setErrorCode(new ErrorCode(new String[0], "0100", "No connection with TP"));
			}
		}
	}

	/**
	 * Get method for status
	 * 
	 * @return status
	 */
	public String getStatus() {
		return mvStatus;
	}

	/**
	 * Set method for status
	 * 
	 * @param pStatus the status
	 */
	public void setStatus(String pStatus) {
		mvStatus = pStatus;
	}

	/**
	 * Get method for client id
	 * 
	 * @return client id
	 */
	public String getClientId() {
		return mvClientId;
	}

	/**
	 * Set method for client id
	 * 
	 * @param pClientId the client id
	 */
	public void setClientId(String pClientId) {
		mvClientId = pClientId;
	}

	/**
	 * Get method for loop counter
	 * 
	 * @return Loop counter counts the number of
	 */
	public int getLoopCounter() {
		int lvCnt;
		try {
			lvCnt = Integer.parseInt(mvLoopCounter);
		} catch (Exception e) {
			return -1;
		}
		return lvCnt;
	}

	/**
	 * Set method for Loop Counter
	 * 
	 * @param Loop Counter
	 */
	public void setLoopCounter(String pLoopCounter) {
		mvLoopCounter = pLoopCounter;
	}

	/**
	 * Get method for Order Enquiry Details
	 * 
	 * @return Array of Order Enquiry Details
	 */
	public HKSOrderEnqDetails[] getOrderEnqDetails() {
		return mvHKSOrderEnqDetails;
	}

	/**
	 * Set method for Order Enquiry Details
	 * 
	 * @param pHKSOrderEnqDetails the Array of Order Enquiry Details
	 */
	public void setOrderEnqDetails(HKSOrderEnqDetails[] pHKSOrderEnqDetails) {
		mvHKSOrderEnqDetails = pHKSOrderEnqDetails;
	}

	/**
	 * Get method for sorting
	 * 
	 * @return sorting
	 */
	public String getSorting() {
		return mvSorting;
	}

	/**
	 * Set method for sorting
	 * 
	 * @param pSorting the sorting
	 */
	public void setSorting(String pSorting) {
		this.mvSorting = pSorting;
	}

	/**
	 * @param mvStartRecord the mvStartRecord to set
	 */
	public void setMvStartRecord(int mvStartRecord) {
		this.mvStartRecord = mvStartRecord;
	}

	/**
	 * @return the mvStartRecord
	 */
	public int getMvStartRecord() {
		return mvStartRecord;
	}

	/**
	 * @param mvEndRecord the mvEndRecord to set
	 */
	public void setMvEndRecord(int mvEndRecord) {
		this.mvEndRecord = mvEndRecord;
	}

	/**
	 * @return the mvEndRecord
	 */
	public int getMvEndRecord() {
		return mvEndRecord;
	}

	/**
	 * @param mvTotalOrders the mvTotalOrders to set
	 */
	public void setMvTotalOrders(int mvTotalOrders) {
		this.mvTotalOrders = mvTotalOrders;
	}

	/**
	 * @return the mvTotalOrders
	 */
	public int getMvTotalOrders() {
		return mvTotalOrders;
	}

	/**
	 * @param mvOrderType the mvOrderType to set
	 */
	public void setMvOrderType(String mvOrderType) {
		this.mvOrderType = mvOrderType;
	}

	/**
	 * @return the mvOrderType
	 */
	public String getMvOrderType() {
		return mvOrderType;
	}

	public String getMvOrderBS() {
		return mvOrderBS;
	}

	public void setMvOrderBS(String mvOrderBS) {
		this.mvOrderBS = mvOrderBS;
	}
}
