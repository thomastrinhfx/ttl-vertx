package com.ttl.old.itrade.hks.bean;

/**
 * The HKSTransactionHistoryBean class define variables that to save values 
 * for action 
 * @author
 *
 */
public class HKSTransactionHistoryBean {
	private String tradeDate;
	private String settleDate;
	private String refId;
	private String action;
	private String actionCode;
	private String stockCode;
	private String stockName;
	private String buyQty;
	private String buyPrice;
	private String buyAmount;
	private String sellQty;
	private String sellPrice;
	private String sellAmount;
	private String fee;
	private String feePercent;
	//BEGIN TASK: TTL-VN VanTran 20100525 add description field
	private String desc;
	
	/**
	 * @param desc the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}
	//END TASK: TTL-VN VanTran 20100525 add description field
	/**
	* This method returns the trade date.
	* @return the trade date is not formatted.
    */
	public String getTradeDate() {
		return tradeDate;
	}
	
	/**
     * This method sets the trade date.
     * @param pTradeDate The trade date.
     */
	
	public void setTradeDate(String pTradeDate) {
		tradeDate = pTradeDate;
	}
	
	/**
	* This method returns the settle date.
	* @return the settle date is not formatted.
    */
	public String getSettleDate() {
		return settleDate;
	}
	
	/**
     * This method sets the settle date.
     * @param pSettleDate The settle date.
     */
	public void setSettleDate(String pSettleDate) {
		settleDate = pSettleDate;
	}
	
	/**
	* This method returns the ref id.
	* @return the ref id is  not formatted.
    */
	public String getRefId() {
		return refId;
	}
	
	/**
     * This method sets the ref id.
     * @param pRefId The ref id.
     */
	public void setRefId(String pRefId) {
		refId = pRefId;
	}
	
	/**
	* This method returns the Action.
	* @return the Action is not formatted.
    */
	public String getAction() {
		return action;
	}
	
	/**
     * This method sets the action.
     * @param pAction The action.
     */
	public void setAction(String pAction) {
		action = pAction;
	}
	
	/**
	* This method returns the stock code.
	* @return the stock code is not formatted.
    */
	public String getStockCode() {
		return stockCode;
	}
	
	/**
     * This method sets the stock code.
     * @param pStockCode The stock code.
     */
	public void setStockCode(String pStockCode) {
		stockCode = pStockCode;
	}
	
	/**
	* This method returns the stock name.
	* @return the stock name is not formatted.
    */
	public String getStockName() {
		return stockName;
	}
	
	/**
     * This method sets the stock name.
     * @param pStockName The stock name.
     */
	public void setStockName(String pStockName) {
		stockName = pStockName;
	}
	
	/**
	 * @param buyQty the buyQty to set
	 */
	public void setBuyQty(String buyQty) {
		this.buyQty = buyQty;
	}

	/**
	 * @return the buyQty
	 */
	public String getBuyQty() {
		return buyQty;
	}

	/**
	 * @param buyPrice the buyPrice to set
	 */
	public void setBuyPrice(String buyPrice) {
		this.buyPrice = buyPrice;
	}

	/**
	 * @return the buyPrice
	 */
	public String getBuyPrice() {
		return buyPrice;
	}

	/**
	 * @param buyAmount the buyAmount to set
	 */
	public void setBuyAmount(String buyAmount) {
		this.buyAmount = buyAmount;
	}

	/**
	 * @return the buyAmount
	 */
	public String getBuyAmount() {
		return buyAmount;
	}

	/**
	 * @param selQty the selQty to set
	 */
	public void setSellQty(String selQty) {
		this.sellQty = selQty;
	}

	/**
	 * @return the selQty
	 */
	public String getSellQty() {
		return sellQty;
	}

	/**
	 * @param selPrice the selPrice to set
	 */
	public void setSellPrice(String selPrice) {
		this.sellPrice = selPrice;
	}

	/**
	 * @return the selPrice
	 */
	public String getSellPrice() {
		return sellPrice;
	}

	/**
	 * @param sellAmount the sellAmount to set
	 */
	public void setSellAmount(String sellAmount) {
		this.sellAmount = sellAmount;
	}

	/**
	 * @return the sellAmount
	 */
	public String getSellAmount() {
		return sellAmount;
	}

	/**
	 * @param fee the fee to set
	 */
	public void setFee(String fee) {
		this.fee = fee;
	}

	/**
	 * @return the fee
	 */
	public String getFee() {
		return fee;
	}

	/**
	 * @param feePercent the feePercent to set
	 */
	public void setFeePercent(String feePercent) {
		this.feePercent = feePercent;
	}

	/**
	 * @return the feePercent
	 */
	public String getFeePercent() {
		return feePercent;
	}

	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	public String getActionCode() {
		return actionCode;
	}
}
