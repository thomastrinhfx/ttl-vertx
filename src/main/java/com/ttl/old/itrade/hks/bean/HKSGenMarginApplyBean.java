package com.ttl.old.itrade.hks.bean;

/**
 * The HKSGenMarginApplyBean class define variables that to save values 
 * for action 
 * @author
 *
 */
public class HKSGenMarginApplyBean {
	private String mvEntitlementId;
	private String mvInstrumentId;
	private String mvStockName;
	private String mvNumberofPubOfferShare;
	private String mvOfferPrice;
	private String mvAmtList;
	private String mvQuantityList;
	private String mvInputQty;
	private String mvApplyMethod;
	private String mvApplicationInputMethod;
	private String mvMarginPercentageList;
	private String mvFlatFee;
	private String mvDepositAmt;
	private String mvFiancneFee;
	private String mvLoanAmt;
	private String mvDepositRate;
	private String mvMarginPercentage;
	private String mvLendingPercentage;
	private String mvSMS;
	private String mvSMSLang;
	private String mvTel;
	private String mvMobile;
	private String mvEmail;
	private String mvErrorMsg;
	
	/**
     * This method returns the entitlement id of margin apply.
     * @return the entitlement id of margin apply.
     */
	public String getMvEntitlementId() {
		return mvEntitlementId;
	}
	
	/**
     * This method sets the entitlement id of margin apply.
     * @param pEntitlementId The entitlement id of margin apply.
     */
	public void setMvEntitlementId(String pEntitlementId) {
		mvEntitlementId = pEntitlementId;
	}
	
	/**
     * This method returns the id of instrument.
     * @return the id of instrument.
     */
	public String getMvInstrumentId() {
		return mvInstrumentId;
	}
	
	/**
     * This method sets the id of instrument.
     * @param pInstrumentId The id of instrument.
     */
	public void setMvInstrumentId(String pInstrumentId) {
		mvInstrumentId = pInstrumentId;
	}
	
	/**
     * This method returns the name of stock.
     * @return the name of stock.
     */
	public String getMvStockName() {
		return mvStockName;
	}
	
	/**
     * This method sets the name of stock.
     * @param pStockName The name of stock.
     */
	public void setMvStockName(String pStockName) {
		mvStockName = pStockName;
	}
	
	/**
     * This method returns the number of pub offer share.
     * @return the number of pub offer share.
     */
	public String getMvNumberofPubOfferShare() {
		return mvNumberofPubOfferShare;
	}
	
	/**
     * This method sets the number of pub offer share.
     * @param pNumberofPubOfferShare The number of pub offer share.
     */
	public void setMvNumberofPubOfferShare(String pNumberofPubOfferShare) {
		mvNumberofPubOfferShare = pNumberofPubOfferShare;
	}
	
	/**
     * This method returns the offer price.
     * @return the offer price.
     */
	public String getMvOfferPrice() {
		return mvOfferPrice;
	}
	
	/**
     * This method sets the offer price.
     * @param pOfferPrice The offer price.
     */
	public void setMvOfferPrice(String pOfferPrice) {
		mvOfferPrice = pOfferPrice;
	}
	
	/**
     * This method returns the amount list.
     * @return the amount list.
     */
	public String getMvAmtList() {
		return mvAmtList;
	}
	
	/**
     * This method sets the amount list.
     * @param pAmtList The amount list.
     */
	public void setMvAmtList(String pAmtList) {
		mvAmtList = pAmtList;
	}
	
	/**
     * This method returns the quantity list.
     * @return the quantity list.
     */
	public String getMvQuantityList() {
		return mvQuantityList;
	}
	
	/**
     * This method sets the quantity list.
     * @param pQuantityList The quantity list.
     */
	public void setMvQuantityList(String pQuantityList) {
		mvQuantityList = pQuantityList;
	}
	
	/**
     * This method returns the input quantity.
     * @return the input quantity.
     */
	public String getMvInputQty() {
		return mvInputQty;
	}
	
	/**
     * This method sets the input quantity.
     * @param pInputQty The input quantity.
     */
	public void setMvInputQty(String pInputQty) {
		mvInputQty = pInputQty;
	}
	
	/**
     * This method returns the apply method.
     * @return the apply method.
     */
	public String getMvApplyMethod() {
		return mvApplyMethod;
	}
	
	/**
     * This method sets the apply method.
     * @param pApplyMethod The apply method.
     */
	public void setMvApplyMethod(String pApplyMethod) {
		mvApplyMethod = pApplyMethod;
	}
	
	/**
     * This method returns the application apply method.
     * @return the application apply method.
     */
	public String getMvApplicationInputMethod() {
		return mvApplicationInputMethod;
	}
	
	/**
     * This method sets the application apply method.
     * @param pApplicationInputMethod The application apply method.
     */
	public void setMvApplicationInputMethod(String pApplicationInputMethod) {
		mvApplicationInputMethod = pApplicationInputMethod;
	}
	
	/**
     * This method returns the margin percentage list.
     * @return the margin percentage list.
     */
	public String getMvMarginPercentageList() {
		return mvMarginPercentageList;
	}
	
	/**
     * This method sets the margin percentage list.
     * @param pMarginPercentageList The margin percentage list.
     */
	public void setMvMarginPercentageList(String pMarginPercentageList) {
		mvMarginPercentageList = pMarginPercentageList;
	}
	
	/**
     * This method returns the flat fee.
     * @return the flat fee.
     */
	public String getMvFlatFee() {
		return mvFlatFee;
	}
	
	/**
     * This method sets the flat fee.
     * @param pFlatFee The flat fee.
     */
	public void setMvFlatFee(String pFlatFee) {
		mvFlatFee = pFlatFee;
	}
	
	/**
     * This method returns the deposit amount.
     * @return the deposit amount.
     */
	public String getMvDepositAmt() {
		return mvDepositAmt;
	}
	
	/**
     * This method sets the deposit amount.
     * @param pDepositAmt The deposit amount.
     */
	public void setMvDepositAmt(String pDepositAmt) {
		mvDepositAmt = pDepositAmt;
	}
	
	/**
     * This method returns the finance fee.
     * @return the finance fee.
     */
	public String getMvFiancneFee() {
		return mvFiancneFee;
	}
	
	/**
     * This method sets the finance fee.
     * @param pFiancneFee The finance fee.
     */
	public void setMvFiancneFee(String pFiancneFee) {
		mvFiancneFee = pFiancneFee;
	}
	
	/**
     * This method returns the loan amount.
     * @return the loan amount.
     */
	public String getMvLoanAmt() {
		return mvLoanAmt;
	}
	
	/**
     * This method sets the loan amount.
     * @param pLoanAmt The loan amount.
     */
	public void setMvLoanAmt(String pLoanAmt) {
		mvLoanAmt = pLoanAmt;
	}
	
	/**
     * This method returns the deposit rate.
     * @return the deposit rate.
     */
	public String getMvDepositRate() {
		return mvDepositRate;
	}
	
	/**
     * This method sets the deposit rate.
     * @param pDepositRate The deposit rate.
     */
	public void setMvDepositRate(String pDepositRate) {
		mvDepositRate = pDepositRate;
	}
	
	/**
     * This method returns the margin percentage.
     * @return the margin percentage.
     */
	public String getMvMarginPercentage() {
		return mvMarginPercentage;
	}
	
	/**
     * This method sets the margin percentage.
     * @param pMarginPercentage The margin percentage.
     */
	public void setMvMarginPercentage(String pMarginPercentage) {
		mvMarginPercentage = pMarginPercentage;
	}
	
	/**
     * This method returns the lending percentage.
     * @return the lending percentage.
     */
	public String getMvLendingPercentage() {
		return mvLendingPercentage;
	}
	
	/**
     * This method sets the lending percentage.
     * @param pLendingPercentage The lending percentage.
     */
	public void setMvLendingPercentage(String pLendingPercentage) {
		mvLendingPercentage = pLendingPercentage;
	}
	
	/**
     * This method returns the SMS.
     * @return the SMS.
     */
	public String getMvSMS() {
		return mvSMS;
	}
	
	/**
     * This method sets the SMS.
     * @param pSMS The SMS.
     */
	public void setMvSMS(String pSMS) {
		mvSMS = pSMS;
	}
	
	/**
     * This method returns the SMS language.
     * @return the SMS language.
     */
	public String getMvSMSLang() {
		return mvSMSLang;
	}
	
	/**
     * This method sets the SMS language.
     * @param pSMSLang The SMS language.
     */
	public void setMvSMSLang(String pSMSLang) {
		mvSMSLang = pSMSLang;
	}
	
	/**
     * This method returns the telephone.
     * @return the telephone.
     */
	public String getMvTel() {
		return mvTel;
	}
	
	/**
     * This method sets the telephone.
     * @param pTel The telephone.
     */
	public void setMvTel(String pTel) {
		mvTel = pTel;
	}
	
	/**
     * This method returns the mobile.
     * @return the mobile.
     */
	public String getMvMobile() {
		return mvMobile;
	}
	
	/**
     * This method sets the mobile.
     * @param pMobile The mobile.
     */
	public void setMvMobile(String pMobile) {
		mvMobile = pMobile;
	}
	
	/**
     * This method returns the email.
     * @return the email.
     */
	public String getMvEmail() {
		return mvEmail;
	}
	
	/**
     * This method sets the email.
     * @param pEmail The email.
     */
	public void setMvEmail(String pEmail) {
		mvEmail = pEmail;
	}
	
	/**
     * This method returns the error message.
     * @return the error message.
     */
	public String getMvErrorMsg() {
		return mvErrorMsg;
	}
	
	/**
     * This method sets the error message
     * @param pErrorMsg The error message.
     */
	public void setMvErrorMsg(String pErrorMsg) {
		mvErrorMsg = pErrorMsg;
	}
}