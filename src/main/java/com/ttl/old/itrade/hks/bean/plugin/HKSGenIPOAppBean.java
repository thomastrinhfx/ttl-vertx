package com.ttl.old.itrade.hks.bean.plugin;

public class HKSGenIPOAppBean {
	private String mvEntitlementId;
	private String mvStockID;
	private String mvFormattedStockID;
	private String mvStockName;
	private String mvMaxQty;
	private String mvOfferPrice;
	private String mvRemark;
	private String mvAmountList;
	private String mvApplicationinputmethod;
	private String mvQuantityList;
	private String mvApplymethod1;
	private String mvApplymethod2;
	private String mvTel;
	private String mvMobile;
	private String mvReceivedSMS;
	private String mvNotReceivedSMS;
	private String mvSMSTraditionalChineseSelection;
	private String mvSMSEnglishSelection;
	private String mvEmail;
	private String mvErrMsg;
	private String mvClientId;
	
	private String mvNumberofPubOfferShare;
	private String mvAmtList;
	private String mvInputQty;
	private String mvApplyMethod;
	private String mvApplicationInputMethod;
	private String mvMarginPercentageList;
	private String mvFlatFee;
	private String mvDepositAmt;
	private String mvFiancneFee;
	private String mvLoanAmt;
	private String mvDepositRate;
	private String mvMarginPercentage;
	private String mvLendingPercentage;
	private String mvSMS;
	private String mvSMSLang;
	private String mvQty;
	
	/**
     * This method returns the entitlement id of margin apply.
     * @return the entitlement id of margin apply.
     */
	public String getMvEntitlementId() {
		return mvEntitlementId;
	}
	
	/**
     * This method sets the entitlement id of margin apply.
     * @param pEntitlementId The entitlement id of margin apply.
     */
	public void setMvEntitlementId(String pEntitlementId) {
		mvEntitlementId = pEntitlementId;
	}
	
	/**
     * This method returns the id of stock.
     * @return the id of stock.
     */
	public String getMvStockID() {
		return mvStockID;
	}
	
	/**
     * This method sets the id of stock.
     * @param pStockID The id of stock.
     */
	public void setMvStockID(String pStockID) {
		mvStockID = pStockID;
	}
	
	/**
     * This method returns the id of stock.
     * @return the id of stock is formatted.
     */
	public String getMvFormattedStockID() {
		return mvFormattedStockID;
	}
	
	/**
     * This method sets the id of stock.
     * @param pFormattedStockID The id of stock is formatted.
     */
	public void setMvFormattedStockID(String pFormattedStockID) {
		mvFormattedStockID = pFormattedStockID;
	}
	
	/**
     * This method returns the name of stock.
     * @return the name of stock.
     */
	public String getMvStockName() {
		return mvStockName;
	}
	
	/**
     * This method sets the name of stock.
     * @param pStockName The name of stock.
     */
	public void setMvStockName(String pStockName) {
		mvStockName = pStockName;
	}
	
	/**
     * This method returns the max quantity.
     * @return the max quantity is formatted.
     */
	public String getMvMaxQty() {
		return mvMaxQty;
	}
	
	/**
     * This method sets the max quantity.
     * @param pMaxQty The max quantity is formatted.
     */
	public void setMvMaxQty(String pMaxQty) {
		mvMaxQty = pMaxQty;
	}
	
	/**
     * This method returns the offer price.
     * @return the offer price is formatted.
     */
	public String getMvOfferPrice() {
		return mvOfferPrice;
	}
	
	/**
     * This method sets the offer price.
     * @param pOfferPrice The offer price is formatted.
     */
	public void setMvOfferPrice(String pOfferPrice) {
		mvOfferPrice = pOfferPrice;
	}
	
	/**
     * This method returns the remark.
     * @return the remark.
     */
	public String getMvRemark() {
		return mvRemark;
	}
	
	/**
     * This method sets the remark.
     * @param pRemark The remark.
     */
	public void setMvRemark(String pRemark) {
		mvRemark = pRemark;
	}
	
	/**
     * This method returns the list of amount.
     * @return the list of amount.
     */
	public String getMvAmountList() {
		return mvAmountList;
	}
	
	/**
     * This method sets the list of amount.
     * @param pAmountList The list of amount.
     */
	public void setMvAmountList(String pAmountList) {
		mvAmountList = pAmountList;
	}
	
	/**
     * This method returns the application input method.
     * @return the application input method.
     */
	public String getMvApplicationinputmethod() {
		return mvApplicationinputmethod;
	}
	
	/**
     * This method sets the application input method.
     * @param pApplicationinputmethod The application input method.
     */
	public void setMvApplicationinputmethod(String pApplicationinputmethod) {
		mvApplicationinputmethod = pApplicationinputmethod;
	}
	
	/**
     * This method returns the list of quantity.
     * @return the list of quantity.
     */
	public String getMvQuantityList() {
		return mvQuantityList;
	}
	
	/**
     * This method sets the list of quantity.
     * @param pQuantityList The list of quantity.
     */
	public void setMvQuantityList(String pQuantityList) {
		mvQuantityList = pQuantityList;
	}
	
	/**
     * This method returns the apply method 1.
     * @return the apply method 1.
     */
	public String getMvApplymethod1() {
		return mvApplymethod1;
	}
	
	/**
     * This method sets the apply method 1 .
     * @param pApplymethod1 The apply method 1.
     */
	public void setMvApplymethod1(String pApplymethod1) {
		mvApplymethod1 = pApplymethod1;
	}
	
	/**
     * This method returns the apply method 2.
     * @return the apply method 2.
     */
	public String getMvApplymethod2() {
		return mvApplymethod2;
	}
	
	/**
     * This method sets the apply method 2 .
     * @param pApplymethod2 The apply method 2.
     */
	public void setMvApplymethod2(String pApplymethod2) {
		mvApplymethod2 = pApplymethod2;
	}
	
	/**
     * This method returns the telephone.
     * @return the telephone.
     */
	public String getMvTel() {
		return mvTel;
	}
	
	/**
     * This method sets the telephone.
     * @param pTel The telephone.
     */
	public void setMvTel(String pTel) {
		mvTel = pTel;
	}
	
	/**
     * This method returns the mobile.
     * @return the mobile.
     */
	public String getMvMobile() {
		return mvMobile;
	}
	
	/**
     * This method sets the mobile.
     * @param pMobile The mobile.
     */
	public void setMvMobile(String pMobile) {
		mvMobile = pMobile;
	}
	
	/**
     * This method returns the received SMS.
     * @return the received SMS.
     */
	public String getMvReceivedSMS() {
		return mvReceivedSMS;
	}
	
	/**
     * This method sets the received SMS.
     * @param pReceivedSMS The received SMS.
     */
	public void setMvReceivedSMS(String pReceivedSMS) {
		mvReceivedSMS = pReceivedSMS;
	}
	
	/**
     * This method returns the not received SMS.
     * @return the not received SMS.
     */
	public String getMvNotReceivedSMS() {
		return mvNotReceivedSMS;
	}
	
	/**
     * This method sets the not received SMS.
     * @param pNotReceivedSMS The not received SMS.
     */
	public void setMvNotReceivedSMS(String pNotReceivedSMS) {
		mvNotReceivedSMS = pNotReceivedSMS;
	}
	
	/**
     * This method returns the received SMS using traditional Chinese.
     * @return the received SMS using traditional Chinese.
     */
	public String getMvSMSTraditionalChineseSelection() {
		return mvSMSTraditionalChineseSelection;
	}
	
	/**
     * This method sets the received SMS using traditional Chinese.
     * @param pSMSTraditionalChineseSelection The received SMS using traditional Chinese.
     */
	public void setMvSMSTraditionalChineseSelection(
			String pSMSTraditionalChineseSelection) {
		mvSMSTraditionalChineseSelection = pSMSTraditionalChineseSelection;
	}
	
	/**
     * This method returns the received SMS using English.
     * @return the received SMS using English.
     */
	public String getMvSMSEnglishSelection() {
		return mvSMSEnglishSelection;
	}
	
	/**
     * This method sets the received SMS using English.
     * @param pSMSEnglishSelection The received SMS using English.
     */
	public void setMvSMSEnglishSelection(String pSMSEnglishSelection) {
		mvSMSEnglishSelection = pSMSEnglishSelection;
	}
	
	/**
     * This method returns the email.
     * @return the email.
     */
	public String getMvEmail() {
		return mvEmail;
	}
	
	/**
     * This method sets the email.
     * @param pEmail The email.
     */
	public void setMvEmail(String pEmail) {
		mvEmail = pEmail;
	}
	
	/**
     * This method returns the error message.
     * @return the error message.
     */
	public String getMvErrMsg() {
		return mvErrMsg;
	}
	
	/**
     * This method sets the error message.
     * @param pErrMsg The error message.
     */
	public void setMvErrMsg(String pErrMsg) {
		mvErrMsg = pErrMsg;
	}
	
	/**
     * This method returns the client id.
     * @return the client id.
     */
	public String getMvClientId() {
		return mvClientId;
	}
	
	/**
     * This method sets the client id.
     * @param pClientId The client id.
     */
	public void setMvClientId(String pClientId) {
		mvClientId = pClientId;
	}
	
	/**
     * This method returns the number of pub offer share.
     * @return the number of pub offer share.
     */
	public String getMvNumberofPubOfferShare() {
		return mvNumberofPubOfferShare;
	}
	
	/**
     * This method sets the number of pub offer share.
     * @param pNumberofPubOfferShare The number of pub offer share.
     */
	public void setMvNumberofPubOfferShare(String pNumberofPubOfferShare) {
		mvNumberofPubOfferShare = pNumberofPubOfferShare;
	}
	
	/**
     * This method returns the amount list.
     * @return the amount list.
     */
	public String getMvAmtList() {
		return mvAmtList;
	}
	
	/**
     * This method sets the amount list.
     * @param pAmtList The amount list.
     */
	public void setMvAmtList(String pAmtList) {
		mvAmtList = pAmtList;
	}
	
	/**
     * This method returns the input quantity.
     * @return the input quantity.
     */
	public String getMvInputQty() {
		return mvInputQty;
	}
	
	/**
     * This method sets the input quantity.
     * @param pInputQty The input quantity.
     */
	public void setMvInputQty(String pInputQty) {
		mvInputQty = pInputQty;
	}
	
	/**
     * This method returns the apply method.
     * @return the apply method.
     */
	public String getMvApplyMethod() {
		return mvApplyMethod;
	}
	
	/**
     * This method sets the apply method.
     * @param pApplyMethod The apply method.
     */
	public void setMvApplyMethod(String pApplyMethod) {
		mvApplyMethod = pApplyMethod;
	}
	
	/**
     * This method returns the application apply method.
     * @return the application apply method.
     */
	public String getMvApplicationInputMethod() {
		return mvApplicationInputMethod;
	}
	
	/**
     * This method sets the application apply method.
     * @param pApplicationInputMethod The application apply method.
     */
	public void setMvApplicationInputMethod(String pApplicationInputMethod) {
		mvApplicationInputMethod = pApplicationInputMethod;
	}
	
	/**
     * This method returns the margin percentage list.
     * @return the margin percentage list.
     */
	public String getMvMarginPercentageList() {
		return mvMarginPercentageList;
	}
	
	/**
     * This method sets the margin percentage list.
     * @param pMarginPercentageList The margin percentage list.
     */
	public void setMvMarginPercentageList(String pMarginPercentageList) {
		mvMarginPercentageList = pMarginPercentageList;
	}
	
	/**
     * This method returns the flat fee.
     * @return the flat fee.
     */
	public String getMvFlatFee() {
		return mvFlatFee;
	}
	
	/**
     * This method sets the flat fee.
     * @param pFlatFee The flat fee.
     */
	public void setMvFlatFee(String pFlatFee) {
		mvFlatFee = pFlatFee;
	}
	
	/**
     * This method returns the deposit amount.
     * @return the deposit amount.
     */
	public String getMvDepositAmt() {
		return mvDepositAmt;
	}
	
	/**
     * This method sets the deposit amount.
     * @param pDepositAmt The deposit amount.
     */
	public void setMvDepositAmt(String pDepositAmt) {
		mvDepositAmt = pDepositAmt;
	}
	
	/**
     * This method returns the finance fee.
     * @return the finance fee.
     */
	public String getMvFiancneFee() {
		return mvFiancneFee;
	}
	
	/**
     * This method sets the finance fee.
     * @param pFiancneFee The finance fee.
     */
	public void setMvFiancneFee(String pFiancneFee) {
		mvFiancneFee = pFiancneFee;
	}
	
	/**
     * This method returns the loan amount.
     * @return the loan amount.
     */
	public String getMvLoanAmt() {
		return mvLoanAmt;
	}
	
	/**
     * This method sets the loan amount.
     * @param pLoanAmt The loan amount.
     */
	public void setMvLoanAmt(String pLoanAmt) {
		mvLoanAmt = pLoanAmt;
	}
	
	/**
     * This method returns the deposit rate.
     * @return the deposit rate.
     */
	public String getMvDepositRate() {
		return mvDepositRate;
	}
	
	/**
     * This method sets the deposit rate.
     * @param pDepositRate The deposit rate.
     */
	public void setMvDepositRate(String pDepositRate) {
		mvDepositRate = pDepositRate;
	}
	
	/**
     * This method returns the margin percentage.
     * @return the margin percentage.
     */
	public String getMvMarginPercentage() {
		return mvMarginPercentage;
	}
	
	/**
     * This method sets the margin percentage.
     * @param pMarginPercentage The margin percentage.
     */
	public void setMvMarginPercentage(String pMarginPercentage) {
		mvMarginPercentage = pMarginPercentage;
	}
	
	/**
     * This method returns the lending percentage.
     * @return the lending percentage.
     */
	public String getMvLendingPercentage() {
		return mvLendingPercentage;
	}
	
	/**
     * This method sets the lending percentage.
     * @param pLendingPercentage The lending percentage.
     */
	public void setMvLendingPercentage(String pLendingPercentage) {
		mvLendingPercentage = pLendingPercentage;
	}
	
	/**
     * This method returns the SMS.
     * @return the SMS.
     */
	public String getMvSMS() {
		return mvSMS;
	}
	
	/**
     * This method sets the SMS.
     * @param pSMS The SMS.
     */
	public void setMvSMS(String pSMS) {
		mvSMS = pSMS;
	}
	
	/**
     * This method returns the SMS language.
     * @return the SMS language.
     */
	public String getMvSMSLang() {
		return mvSMSLang;
	}
	
	/**
     * This method sets the SMS language.
     * @param pSMSLang The SMS language.
     */
	public void setMvSMSLang(String pSMSLang) {
		mvSMSLang = pSMSLang;
	}
	
	/**
	 * This method returns the quantity.
	 * @return the quantity
	 */
	public String getMvQty() {
		return mvQty;
	}

	/**
	 *  This method sets the quantity.
	 * @param pQty The quantity
	 */
	public void setMvQty(String pQty) {
		mvQty = pQty;
	}
}
