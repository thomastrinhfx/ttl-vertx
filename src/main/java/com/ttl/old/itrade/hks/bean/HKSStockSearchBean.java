package com.ttl.old.itrade.hks.bean;

/**
 * The HKSStockSearchBean class define variables that to save values 
 * for action 
 * @author
 *
 */
public class HKSStockSearchBean {
	private String stockName;
	private String stockCode;
	private String lotSize;
	private String mvMarketID;
	
	/**
	* This method returns the market id.
	* @return the market id is formatted.
    */
	public String getMvMarketID() {
		return mvMarketID;
	}
	
	/**
     * This method sets the market id.
     * @param pMarketID The market id.
     */
	public void setMvMarketID(String pMarketID) {
		mvMarketID = pMarketID;
	}
	
	/**
	* This method returns the stock name.
	* @return the stock name is not formatted.
    */
	public String getStockName() {
		return stockName;
	}
	
	/**
     * This method sets the stock name.
     * @param pStockName The stock name.
     */	
	public void setStockName(String pStockName) {
		stockName = pStockName;
	}
	
	/**
	* This method returns the stock code.
	* @return the stock code is not formatted.
    */
	public String getStockCode() {
		return stockCode;
	}
	
	/**
     * This method sets the stock code.
     * @param pStockCode The stock code.
     */	
	public void setStockCode(String pStockCode) {
		stockCode = pStockCode;
	}
	
	/**
	* This method returns the lot size.
	* @return the lot size is not formatted.
    */
	public String getLotSize() {
		return lotSize;
	}
	
	/**
     * This method sets the lot size.
     * @param pLotSize The lot size.
     */	
	public void setLotSize(String pLotSize) {
		lotSize = pLotSize;
	}
}
