package com.ttl.old.itrade.hks;

import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * The HKSTag interface defined field that it be used repeatedly String for constant.
 * @author
 *
 */
public abstract interface HKSTag extends TagName {
	//tag names for xml header.
	public final static String RESVR = "resvr";
	public final static String ISSUE_TIME = "issueTime";
	public final static String WINDOW_ID = "WINDOWSID";

	// TagNames
	public final static String errorCode = "errorCode";
	public final static String SCREENSIZE = "SCREENSIZE";
	public final static String SHOWDISCLAIMERFLAG = "SHOWDISCLAIMERFLAG";
	public final static String NEEDCHANGEPASSWORDFLAG = "NEEDCHANGEPASSWORDFLAG";
	public final static String LASTACTION = "LASTACTION";
	// BEGIN Task #: BN00013 Bennis Ng 20070502
	public final static String AUTOREFRESH = "AUTOREFRESH";
	// END Task #: BN00013
	// Action ID (Should have the same mapping with menu.jsp)
	// BEGIN RN00049 Ricky Ngan 20081127
	public final static String ActionIDchangePasswordPage = "0";
	// END
	public final static String ActionIDAccountBalancePage = "1";
	public final static String ActionIDOrderEnquiryPage = "2";
	public final static String ActionIDTransactionEnquiryPage = "3";
	public final static String ActionIDPortfolioEnquiryPage = "4";
	public final static String ActionIDFundTransferPage = "5";
	public final static String ActionIDIPOPage = "6";
	public final static String ActionIDOrderHistoryPage = "7";
	public final static String ActionIDPriceAlertPage = "8";
	public final static String ActionIDEMessagePage = "9";
	public final static String ActionIDAccountBalanceHostPage = "10";
	public final static String ActionIDPortfolioEnquiryPage2 = "11";
	public final static String ActionIDScripOptionPage = "12";
	public final static String ActionIDExercisePage = "13";
	public final static String ActionIDInstrumentNewsAndResearchReportPage = "14";
	public final static String ActionIDResearchReportsPage = "15";
	// BEGIN Task: RN00024 Ricky Ngan 20080710
	public final static String ActionIDIPOFinancingPage = "16";
	public final static String ActionIDSecuritiesOrderPage = "17";
	public final static String ActionIPOAllotmentResultPage = "18";
	// END Task: RN00024
	
	// BEGIN Task: WTRAD-4[http://tts.tx-tech.com/devtrack/browse/WTRAD-4] TanPham 20170706
	public final static String ActionIDSignOrderPage = "19";
	// END Task: WTRAD-4

	// Action Names
	public final static String ActionHKSGenLogin = "hksGenLogin";
	public final static String ActionHKSGenChangePassword = "hksGenChangePassword";
	public final static String ActionHKSGenResetPassword = "hksGenResetPassword";
	public final static String ActionHKSLogin = "hksLogin";
	public final static String ActionHKSLogout = "hksLogout";
	public final static String ActionHKSChangePassword = "hksChangePassword";
	public final static String ActionHKSResetPassword = "hksResetPassword";
	public final static String ActionHKSSubmitDisclaimer = "hksSubmitDisclaimer";
	public final static String ActionHKSGenPlaceOrderFrame = "hksGenPlaceOrderFrame";
	public final static String ActionHKSGenEnterOrderFrame = "hksGenEnterOrderFrame";
	public final static String ActionHKSEnterOrderAction = "hksEnterOrderAction";
	public final static String ActionHKSEnterOrderConfirmAction = "hksEnterOrderConfirmAction";
	public final static String ActionHKSEnterOrderSuccessAction = "hksEnterOrderSuccessAction";
	public final static String ActionHKSEnterOrderFailAction = "hksEnterOrderFailAction";
	public final static String ActionHKSOrderEnquiry = "hksOrderEnquiry";
	public final static String ActionHKSModifyOrderAction = "hksModifyOrder";
	public final static String ActionHKSModifyOrderFailAction = "hksModifyOrderFailAction";
	public final static String ActionHKSModifyOrderSuccessAction = "hksModifyOrderSuccessAction";
	public final static String ActionHKSGenCancelOrderFrameAction = "hksGenCancelOrderFrame";
	public final static String ActionHKSCancelOrderAction = "hksCancelOrder";
	public final static String ActionHKSCancelOrderSuccessAction = "hksCancelOrderSuccessAction";
	public final static String ActionHKSCancelOrderFailAction = "hksCancelOrderFailAction";
	public final static String ActionHKSTransactionHistory = "hksTransactionHistory";
	public final static String ActionHKSGenMainPage = "hksGenMainPageAction";
	public final static String ActionHKSGenHeaderPage = "hksGenHeaderPageAction";
	public final static String ActionHKSGenDisclaimerPage = "hksGenDisclaimerPageAction";
	public final static String ActionHKSGenWelcomePage = "hksGenWelcomePageAction";
	public final static String ActionHKSOrderHistoryEnquiryAction = "hksOrderHistoryEnquiryAction";
	public final static String ActionHKSTradeEnquiryAction = "hksTradeEnquiryAction";
	public final static String ActionHKSOrderDetailsEnquiryAction = "hksOrderDetailsEnquiryAction";
	public final static String ActionHKSPriceAlertSubscriptionAction = "hksPriceAlertSubscriptionAction";
	public final static String ActionHKSPriceAlertEnquiryAction = "hksPriceAlertEnquiryAction";
	public final static String ActionHKSPriceAlertDeleteAction = "hksPriceAlertDeleteAction";
	public final static String ActionHKSEMessageEnquiryAction = "hksEMessageEnquiryAction";
	public final static String ActionHKSEMessageDeleteAction = "hksEMessageDeleteAction";
	public final static String ActionHKSAccountBalanceEnquiryAction = "hksAccountBalanceEnquiry";
	public final static String ActionHKSAccountBalanceHostEnquiryAction = "hksAccountBalanceHostEnquiry";
	public final static String ActionHKSPortfolioEnquiryAction = "hksPortfolioEnquiry";
	public final static String ActionHKSPortfolioEnquiryAction2 = "hksPortfolioEnquiry2";
	public final static String ActionHKSStockSearchAction = "hksStockSearchAction";
	public final static String ActionHKSQueryStockNameAction = "queryStockName";
	public final static String ActionHKSShowDescriptionAction = "hksShowDescriptionAction";
	public final static String ActionHKSContactUsAction = "hksContactUsAction";
	public final static String ActionHKSAccountRegistrationAction = "hksAccountRegistrationAction";
	public final static String ActionHKSScripOptionStatusEnquiryAction = "hksScripOptionStatusEnquiryAction";
	public final static String ActionHKSScripOptionGenDetailAction = "hksScripOptionGenDetailAction";
	public final static String ActionHKSScripOptionGenTermsAction = "hksScripOptionGenTermsAction";
	public final static String ActionHKSScripOptionGenProspectusAction = "hksScripOptionGenProspectusAction";
	public final static String ActionHKSScripOptionGenApplyAction = "hksScripOptionGenApplyAction";
	public final static String ActionHKSScripOptionSubmitAction = "hksScripOptionSubmitAction";
	public final static String ActionHKSScripOptionGenConfirmAction = "hksScripOptionConfirmAction";
	public final static String ActionHKSGenInformationEnquiryAction = "hksInformationEnquiryAction";
	public final static String ActionHKSInstrumentNewsAndResearchReportEnquiryAction = "hksInstrumentNewsAndResearchReportEnquiryAction";
	public final static String ActionHKSResearchReportsEnquiryAction = "hksResearchReportsEnquiryAction";
	// WL00702
	public final static String ActionHKSGenSingleSignOnUserIDSetupPageAction = "hksGenSingleSignOnUserIDSetupPageAction";
	public final static String ActionHKSUserIDSetupAction = "hksUserIDSetupAction";
	public final static String ActionHKSGenIndexAction = "hksGenIndexAction";
	// WL00702

	public final static String ActionHKSGenAALoginAction = "hksGenAALoginAction";

	// BEGIN Task: WTRAD-4[http://tts.tx-tech.com/devtrack/browse/WTRAD-4] TanPham 20170706
	public final static String ActionHKSSignOrderEnquiryAction = "hksSignOrderEnquiryAction";
	// END Task: WTRAD-4
		
	// SessionModelNames
	public final static String SessionModelEnterOrder = "SessionModelEnterOrder";
	public final static String SessionModelModifyOrder = "SessionModelModifyOrder";
	public final static String SessionModelCancelOrder = "SessionModelCancelOrder";

	public final static String STOCKNAME = "STOCKNAME";

	// redesign and implement
	// and implement

	//BEGIN TASK: TTL-GZ-XYL-00045 XuYuLong 20091211 [iTrade R5] Plugin redesign and implement
	/**
	 * Properites Tag For Base Function
	 */
	public final static String BaseFunctionInMainMenuBarCount = "BaseFunctionInMainMenuBar_Count";
	public final static String BaseFunctionInMainMenuBar = "BaseFunctionInMainMenuBar";
	public final static String BaseFunctionInApplicationMenuCount = "BaseFunctionInApplicationMenu_Count";
	public final static String BaseFunctionInApplicationMenu = "BaseFunctionInApplicationMenu";
	/**
	 * Properites Tag For Plugin Function
	 */
	public final static String PluginFunctionInMainMenuBarCount = "PluginFunctionInMainMenuBar_Count";
	public final static String PluginFunctionInMainMenuBar = "PluginFunctionInMainMenuBar";
	public final static String PluginFunctionInApplicationMenuCount = "PluginFunctionInApplicationMenu_Count";
	public final static String PluginFunctionInApplicationMenu = "PluginFunctionInApplicationMenu";
	public final static String functionJSPath = "_JSPath";
	public final static String functionLanguageSourceJSPath = "_LanguageSourceJSPath";
	/**
	 * Properites Tag For Common
	 */
	public final static String functionName = "_Name";
	public final static String functionEnable = "_Enable";
	public final static String functionHandler = "_Handler";
	public final static String functionIconPath = "_IconPath";
	//END TASK: TTL-GZ-XYL-00045 XuYuLong 20091211 [iTrade R5] Plugin redesign and implement
	
	public final static String HOSE_EXCHANGE_CODE = "HO";
	public final static String HNX_EXCHANGE_CODE = "HA";
	public final static String UPCOME_EXCHANGE_CODE = "OTC";
	
	/**
	 * Properites Tag For UIManagement 
	 */
	
	public final static String GROUPNAME = "GROUPNAME";
	public final static String GROUPTYPE = "GROUPTYPE";
	public final static String ISDEFAULT = "ISDEFAULT";
	public final static String SAVEDCONTENT = "SAVEDCONTENT";	
	
}
