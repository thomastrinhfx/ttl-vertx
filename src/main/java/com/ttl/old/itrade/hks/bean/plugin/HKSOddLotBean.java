package com.ttl.old.itrade.hks.bean.plugin;

/**
 * The HKSOddLotBean class define variables that to save values for action 
 * @author Giang Tran
 *
 */
public class HKSOddLotBean {
	private String marketId;
	private String stockCode;
	private String stockName;
	private String settledBal;
	private String drawable;
	private String oddLotQty;
	private String holdQty;
	private String location;
	private String nominalPrice;
	private String collectionPrice;
	
	/**
	 * @return the marketId
	 */
	public String getMarketId() {
		return marketId;
	}
	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(String marketId) {
		this.marketId = marketId;
	}
	/**
	 * @return the stockCode
	 */
	public String getStockCode() {
		return stockCode;
	}
	/**
	 * @param stockCode the stockCode to set
	 */
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	/**
	 * @return the stockName
	 */
	public String getStockName() {
		return stockName;
	}
	/**
	 * @param stockName the stockName to set
	 */
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	/**
	 * @return the settledBal
	 */
	public String getSettledBal() {
		return settledBal;
	}
	/**
	 * @param settledBal the settledBal to set
	 */
	public void setSettledBal(String settledBal) {
		this.settledBal = settledBal;
	}
	/**
	 * @return the drawable
	 */
	public String getDrawable() {
		return drawable;
	}
	/**
	 * @param drawable the drawable to set
	 */
	public void setDrawable(String drawable) {
		this.drawable = drawable;
	}
	/**
	 * @return the oddLotQty
	 */
	public String getOddLotQty() {
		return oddLotQty;
	}
	/**
	 * @param oddLotQty the oddLotQty to set
	 */
	public void setOddLotQty(String oddLotQty) {
		this.oddLotQty = oddLotQty;
	}
	/**
	 * @return the holdQty
	 */
	public String getHoldQty() {
		return holdQty;
	}
	/**
	 * @param holdQty the holdQty to set
	 */
	public void setHoldQty(String holdQty) {
		this.holdQty = holdQty;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}
	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}
	
	public String getNominalPrice() {
		return nominalPrice;
	}
	
	public void setNominalPrice(String nominalPrice) {
		this.nominalPrice = nominalPrice;
	}
	
	public String getCollectionPrice() {
		return collectionPrice;
	}
	
	public void setCollectionPrice(String collectionPrice) {
		this.collectionPrice = collectionPrice;
	}
	
}
