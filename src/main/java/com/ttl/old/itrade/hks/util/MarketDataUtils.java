package com.ttl.old.itrade.hks.util;

import org.apache.commons.validator.GenericValidator;

/**
 * 
 * @author Nha-Dang, Upgraded duonghuynh
 * 
 */
public class MarketDataUtils {
	
	/**
	 * Format number for price. If strNumber is "" or "-" or "0" return "-" / If strNumber is number, return formatted number Otherwise
	 * return strNumber (for ATO, ATC prices)
	 * 
	 * @param strNumber
	 * @return
	 */
	public static String formatPrice(String strNumber) {
		if (strNumber == null || strNumber.trim().equals("") || strNumber.trim().equals("-")) {
			return "-";
		} else if (GenericValidator.isDouble(strNumber)) {
			if (Double.parseDouble(strNumber) <= 0) {
				return "-";
			} else {
				return new DisplayNumber(strNumber, 1).toString();
			}
		} else {
			return strNumber; // for ATO, ATC...
		}
	}

	/**
	 * Format number for volume. If strNumber is "" or "-" or "0" return "-" / If input strNumber is number, return formatted number
	 * 
	 * @param strNumber
	 * @return
	 */
	public static String formatVolume(String strNumber) {

		if (strNumber == null || strNumber.trim().equals("") || strNumber.trim().equals("-")) {
			return "-";
		} else if (GenericValidator.isDouble(strNumber)) {
			if (Double.parseDouble(strNumber) <= 0) {
				return "-";
			} else {
				return new DisplayNumber(strNumber).toString();
			}
		} else {
			return "-"; // for invalid input
		}
	}

	/**
	 * Return number or zero if the input is not a valid number
	 * 
	 * @param strNumber
	 * @return
	 */
	public static Double parseDouble(String strNumber) {
		if (strNumber == null) {
			return new Double(0);
		}
		try {
			return Double.parseDouble(strNumber);
		} catch (NumberFormatException e) {
			return new Double(0);
		}
	}

}
