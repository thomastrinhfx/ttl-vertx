package com.ttl.old.itrade.hks.txn;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.tag.ITagXsfTagName;
import com.systekit.winvest.hks.config.mapping.TagName;
import com.systekit.winvest.hks.server.model.trade.TradeSModel;
/**
 * This class store the trading enquiry
 * @author not attributable
 *
 */
public class HKSTradeEnquiryTxn extends BaseTxn
{
   private String mvOrderGroupID;
   private String mvOrderID;
   private String mvIsHistory;
   /**
    * Default constructor for HKSTradeEnquiryTxn class
    * @param pOrderGroupID the order group id
    * @param pOrderID the order id
    * @param pIsHistory the is history
    */
   public HKSTradeEnquiryTxn(String pOrderGroupID, String pOrderID, String pIsHistory)
   {
      super();
      mvOrderGroupID = pOrderGroupID;
      mvOrderID = pOrderID;
      mvIsHistory = pIsHistory;
   }
   /**
    * This method for store trading enquiry
    * @return trading enquiry result set
    */
   public Vector process()
   {
      Hashtable lvTxnMap = new Hashtable();
      lvTxnMap.put(TagName.ORDERGROUPID, mvOrderGroupID);
	  //Begin Task: CL00055 Charlie Lau 20081206
      lvTxnMap.put(TagName.ORDERID, mvOrderID);
      lvTxnMap.put("ISHISTORY", mvIsHistory);
      lvTxnMap.put("BYORDERID", "Y");
	  //End Task: CL00055
      Vector lvReturn = new Vector();
      if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSTradeEnquiry, lvTxnMap))
      {
         IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList(ITagXsfTagName.ROW);
         for (int i = 0; i < lvRowList.size(); i++)
         {
            IMsgXMLNode lvRow = lvRowList.getNode(i);
            TradeSModel lvModel = new TradeSModel();
            lvModel.setTradeTime(new Timestamp(Long.parseLong(lvRow.getChildNode(TagName.TRADETIME).getValue())));
            //lvModel.setPrice(Double.parseDouble(lvRow.getChildNode(TagName.PRICE).getValue()));
            lvModel.setPrice(new BigDecimal(lvRow.getChildNode(TagName.PRICE).getValue()));
            lvModel.setQty(Long.parseLong(lvRow.getChildNode(TagName.QTY).getValue()));
            lvReturn.add(lvModel);
         }
      }
      return lvReturn;
   }
}
