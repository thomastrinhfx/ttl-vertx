package com.ttl.old.itrade.hks.txn;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.ttl.old.itrade.hks.bean.HKSClientDetailsBean;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.ErrorCode;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * This class store the query client details
 * @author YuLong.Xu
 * @since 20091125
 */
public class HKSQueryClientDetailsTxn extends BaseTxn{

	private String mvClientID;
	private String mvDisplaySettlementAcc;
	private List mvSettlementAccList;
	/**
	 * Get method for settlement account list
	 * @return settlement account list
	 */
	public List getMvSettlementAccList() {
		return mvSettlementAccList;
	}
	/**
	 * Set method for settlement account list
	 * @param pSettlementAccList the settlement account list
	 */
	public void setMvSettlementAccList(List pSettlementAccList) {
		this.mvSettlementAccList = pSettlementAccList;
	}
	/**
	 * Get method for display settlement account
	 * @return display settlement account
	 */
	public String getMvDisplaySettlementAcc() {
		return mvDisplaySettlementAcc;
	}
	/**
	 * Set method for display settlement account
	 * @param pDisplaySettlementAcc the display settlement account
	 */
	public void setMvDisplaySettlementAcc(String pDisplaySettlementAcc) {
		this.mvDisplaySettlementAcc = pDisplaySettlementAcc;
	}
	/**
	 * Get method for client id
	 * @return client id
	 */
	public String getMvClientID() {
		return mvClientID;
	}
	/**
	 * Set method for client id
	 * @param pClientID the client id
	 */
	public void setMvClientID(String pClientID) {
		this.mvClientID = pClientID;
	}
	/**
	 * Constructor for HKSQueryClientDetailsTxn class
	 * @param pClientID
	 * @param pDisplaySettlementAcc
	 */
	public HKSQueryClientDetailsTxn(String pClientID, String pDisplaySettlementAcc){
		
		super();
		this.setMvClientID(pClientID);
		this.setMvDisplaySettlementAcc(pDisplaySettlementAcc);
	}
	
	/**
	 * @Discription Setting the reqeust xml and calling TP; Encapsulating the result into beans and setting the beans into a list.
	 * @Param 
	 * @Return void
	 * @Exception NullPointerException, Exception
	 * @Date 20091125
	 * @author YuLong.Xu
	 */
	public void process(){
		

		try {
			Hashtable lvTxnMap = new Hashtable();
			lvTxnMap.put("CLIENTID", this.getMvClientID());
			lvTxnMap.put("DISPLAYSETTLEMENTACCS", this.getMvDisplaySettlementAcc());
			if (TPErrorHandling.TP_NORMAL == process("HKSGetClientDetailsRequest", lvTxnMap)){
				IMsgXMLNode lvSettltmentAccsNodeList = this.mvReturnNode.getChildNode("SETTLEMENTACCS");
				if (lvSettltmentAccsNodeList != null) {
					this.mvSettlementAccList = new ArrayList();
					
					//Begin Task #: - Giang Tran March 17, 2010 Add the ORS Bank Acc
					HKSClientDetailsBean bean = new HKSClientDetailsBean();
					bean.setMvBankID("");
					bean.setMvBankACID("");
					bean.setMvSettlementAccountValue("");
					bean.setMvSettlementAccountDisplayName("--------");
					this.mvSettlementAccList.add(bean);
					
					IMsgXMLNodeList lvAccsNodeList = lvSettltmentAccsNodeList.getNodeList("ACC");
					
					for (int m = 0; m < lvAccsNodeList.size(); m ++) {
						bean = new HKSClientDetailsBean();
						bean.setMvBankID(lvAccsNodeList.getNode(m).getChildNode(TagName.BANKID).getValue());
						bean.setMvBankACID(lvAccsNodeList.getNode(m).getChildNode(TagName.BANKACID).getValue());
						bean.setMvSettlementAccountValue(lvAccsNodeList.getNode(m).getChildNode(TagName.BANKACID).getValue());
						bean.setMvSettlementAccountDisplayName(lvAccsNodeList.getNode(m).getChildNode(TagName.BANKID).getValue() + " - " + lvAccsNodeList.getNode(m).getChildNode(TagName.BANKACID).getValue());
						
						this.mvSettlementAccList.add(bean);
					}
					
					//End Task #: - Giang Tran March 17, 2010 Add the ORS Bank Acc
				}
			}else{
		           setErrorCode(new ErrorCode(new String[0], mvReturnNode.getChildNode("C_ERROR_CODE").getValue(), ""));

		           // Handle special cases
		           // 1. Agreement is not signed
		           if (mvReturnNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase("HKSFOE00026")) {
		          	 setErrorCode(new ErrorCode(new String[0], mvReturnNode.getChildNode("C_ERROR_CODE").getValue(), mvReturnNode.getChildNode("C_ERROR_DESC").getValue()));
		           }

		           // 2. When TP is down
		           if (mvReturnNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase("TPBASEREQERROR")) {
		          	 setErrorCode(new ErrorCode(new String[0], "0100", "No connection with TP"));
		           }
			}
		} catch (NullPointerException e) {
			// TODO: handle exception
	         e.printStackTrace();
	         Log.println("[ HKSQueryClientDetailsTxn.process()].... NullPointerException",  Log.ERROR_LOG);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
