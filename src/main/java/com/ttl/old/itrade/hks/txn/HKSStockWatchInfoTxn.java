//BEGIN TASK: TTL-GZ-JJ-00001 JiangJi 20091206 [iTradeR5 VN] FO:Pop up stock info
package com.ttl.old.itrade.hks.txn;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.HKSTag;
import com.ttl.old.itrade.hks.request.GenericRequest;
import com.ttl.old.itrade.hks.util.DateUtil;
import com.ttl.old.itrade.hks.util.HKSInstrument;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * This class store the Stock Info
 * 
 * @author not attributable
 * 
 */
public class HKSStockWatchInfoTxn extends BaseTxn {
	public static final String CLIENTID = "CLIENTID";
	private String mvLanguage;
	private String mvInstrument;
	private String mvMarketId;

	private String mvStatusDescription;
	private String mvCurrentRoom;
	private String mvErrorDescription = null;
	// BEGIN TASK: TTL-GZ-JJ-00016 JiangJi 20100310 [iTrade R5] Stock popup new
	// layout
	private String mvDayOpen;
	private String mvDayClose;

	private String mvFloor;
	private String mvCeiling;
	private String mvReferencePrice;
	// END TASK: TTL-GZ-JJ-00016 JiangJi 20100310 [iTrade R5] Stock popup new
	// layout

	// BEGIN TASK: TTL-VN VanTran 20100426 [iTrade R5] Stock popup new layout
	private String mvBidPrice1;
	private String mvBidPrice2;
	private String mvBidPrice3;
	private String mvOfferPrice1;
	private String mvOfferPrice2;
	private String mvOfferPrice3;
	private String mvBidVol1;
	private String mvBidVol2;
	private String mvBidVol3;
	private String mvHigh;
	private String mvNomial;

	private String mvNoalPriSubRefPri;
	private String mvNoalPriPerRate;
	
	private String mvPreClosePrice;

	private String mvNextFloor;
	private String mvNextCeiling;
	private Date mvLastModifiedTime;
	public String getMvHigh() {
		return mvHigh;
	}

	public void setMvHigh(String mvHigh) {
		this.mvHigh = mvHigh;
	}

	public String getMvLow() {
		return mvLow;
	}

	public void setMvLow(String mvLow) {
		this.mvLow = mvLow;
	}

	private String mvLow;

	public String getMvBidPrice1() {
		return mvBidPrice1;
	}

	public void setMvBidPrice1(String mvBidPrice1) {
		this.mvBidPrice1 = mvBidPrice1;
	}

	public String getMvBidPrice2() {
		return mvBidPrice2;
	}

	public void setMvBidPrice2(String mvBidPrice2) {
		this.mvBidPrice2 = mvBidPrice2;
	}

	public String getMvBidPrice3() {
		return mvBidPrice3;
	}

	public void setMvBidPrice3(String mvBidPrice3) {
		this.mvBidPrice3 = mvBidPrice3;
	}

	public String getMvOfferPrice1() {
		return mvOfferPrice1;
	}

	public void setMvOfferPrice1(String mvOfferPrice1) {
		this.mvOfferPrice1 = mvOfferPrice1;
	}

	public String getMvOfferPrice2() {
		return mvOfferPrice2;
	}

	public void setMvOfferPrice2(String mvOfferPrice2) {
		this.mvOfferPrice2 = mvOfferPrice2;
	}

	public String getMvOfferPrice3() {
		return mvOfferPrice3;
	}

	public void setMvOfferPrice3(String mvOfferPrice3) {
		this.mvOfferPrice3 = mvOfferPrice3;
	}

	public String getMvBidVol1() {
		return mvBidVol1;
	}

	public void setMvBidVol1(String mvBidVol1) {
		this.mvBidVol1 = mvBidVol1;
	}

	public String getMvBidVol2() {
		return mvBidVol2;
	}

	public void setMvBidVol2(String mvBidVol2) {
		this.mvBidVol2 = mvBidVol2;
	}

	public String getMvBidVol3() {
		return mvBidVol3;
	}

	public void setMvBidVol3(String mvBidVol3) {
		this.mvBidVol3 = mvBidVol3;
	}

	public String getMvOfferVol1() {
		return mvOfferVol1;
	}

	public void setMvOfferVol1(String mvOfferVol1) {
		this.mvOfferVol1 = mvOfferVol1;
	}

	public String getMvOfferVol2() {
		return mvOfferVol2;
	}

	public void setMvOfferVol2(String mvOfferVol2) {
		this.mvOfferVol2 = mvOfferVol2;
	}

	public String getMvOfferVol3() {
		return mvOfferVol3;
	}

	public void setMvOfferVol3(String mvOfferVol3) {
		this.mvOfferVol3 = mvOfferVol3;
	}

	private String mvOfferVol1;
	private String mvOfferVol2;
	private String mvOfferVol3;

	// END TASK: TTL-VN VanTran 20100426 [iTrade R5] Stock popup new layout

	

	/**
	 * Get the user locale
	 * 
	 * @return user locale
	 */
	public String getLanguage() {
		return mvLanguage;
	}

	/**
	 * Set the user locale
	 * 
	 * @param pLanguage
	 *            the user locale
	 */
	public void setLanguage(String pLanguage) {
		mvLanguage = pLanguage;
	}

	/**
	 * Get the Instrument
	 * 
	 * @return Instrument
	 */
	public String getInstrument() {
		return mvInstrument;
	}

	/**
	 * Set the Instrument
	 * 
	 * @param pInstrument
	 *            the Instrument
	 */
	public void setInstrument(String pInstrument) {
		mvInstrument = pInstrument;
	}

	/**
	 * Get the Market id
	 * 
	 * @return Market id
	 */
	public String getMarketId() {
		return mvMarketId;
	}

	/**
	 * Set the Market id
	 * 
	 * @param pMarketId
	 *            the Market id
	 */
	public void setMarketId(String pMarketId) {
		mvMarketId = pMarketId;
	}

	/**
	 * constructor for HKSStockInfoTxn class
	 * 
	 * @param pClientId
	 *            the client id
	 * @param pInstrument
	 *            the instrument
	 * @param pMarketId
	 *            the market id
	 */
	public HKSStockWatchInfoTxn(String pInstrument,String pMarketId) {
		super();
		setInstrument(pInstrument);
		setMarketId(pMarketId);
	}

	/**
	 * Get the Current Room
	 * 
	 * @return Current Room
	 */
	public String getCurrentRoom() {
		return mvCurrentRoom;
	}

	/**
	 * Set Current Room
	 * 
	 * @param pCurrentRoom
	 *            the Current Room
	 */
	public void setCurrentRoom(String pCurrentRoom) {
		mvCurrentRoom = pCurrentRoom;
	}

	// END TASK: TTL-GZ-JJ-00006 JiangJi 20091229 [iTrade R5] Stock popup fields
	// remove decimal

	/**
	 * Get the Status Description
	 * 
	 * @return Status Description
	 */
	public String getStatusDescription() {
		return mvStatusDescription;
	}

	/**
	 * Set Status Description
	 * 
	 * @param pStatusDescription
	 *            the Status Description
	 */
	public void setStatusDescription(String pStatusDescription) {
		mvStatusDescription = pStatusDescription;
	}

	/**
	 * Get the Error Description
	 * 
	 * @return Error Description
	 */
	public String getErrorDescription() {
		return mvErrorDescription;
	}

	/**
	 * Set Error Description
	 * 
	 * @param pErrorDescription
	 *            the error system description
	 */
	public void setErrorDescription(String pErrorDescription) {
		mvErrorDescription = pErrorDescription;
	}

	// BEGIN TASK: TTL-GZ-JJ-00016 JiangJi 20100310 [iTrade R5] Stock popup new
	// layout
	/**
	 * Get the day open price
	 * 
	 * @return The day open price
	 */
	public String getDayOpen() {
		return mvDayOpen;
	}

	/**
	 * Sets the day open price
	 * 
	 * @param pDayOpen
	 *            the day open price
	 */
	public void setDayOpen(String pDayOpen) {
		mvDayOpen = pDayOpen;
	}

	/**
	 * Get the floor price
	 * 
	 * @return The floor price
	 */
	public String getFloor() {
		return mvFloor;
	}

	/**
	 * Sets the floor price
	 * 
	 * @param pFloor
	 *            the floor price
	 */
	public void setFloor(String pFloor) {
		mvFloor = pFloor;
	}

	/**
	 * Get the ceiling price
	 * 
	 * @return The ceiling price
	 */
	public String getCeiling() {
		return mvCeiling;
	}

	/**
	 * Sets the ceiling price
	 * 
	 * @param pCeiling
	 *            the ceiling price
	 */
	public void setCeiling(String pCeiling) {
		mvCeiling = pCeiling;
	}

	/**
	 * Get the reference price
	 * 
	 * @return The reference price
	 */
	public String getReferencePrice() {
		return mvReferencePrice;
	}

	/**
	 * Sets the reference price
	 * 
	 * @param pReferencePrice
	 *            the reference price
	 */
	public void setReferencePrice(String pReferencePrice) {
		mvReferencePrice = pReferencePrice;
	}
	
	// END TASK: TTL-GZ-JJ-00016 JiangJi 20100310 [iTrade R5] Stock popup new
	// layout

	// END TASK: TTL-GZ-JJ-00017 JiangJi 20100311 [iTrade R5] Account specific
	// data

	public String getMvNoalPriPerRate() {
		return mvNoalPriPerRate;
	}

	public void setMvNoalPriPerRate(String mvNoalPriPerRate) {
		this.mvNoalPriPerRate = mvNoalPriPerRate;
	}

	// BEGIN TASK: TTL-GZ-JJ-00017 JiangJi 20100311 [iTrade R5] Account specific
	// data
	/**
	 * This method store the data for stock balance.
	 */
	public void process() {

		if (getErrorDescription() == null) {
			// get the stock info
			getStockInfoBalance();
		}
	}

	/**
	 * This method to get 'foreign room' and 'instrument status' for stock info.
	 */
	private void getStockInfoBalance(){
		//BEGIN TASK: TTL-GZ-JJ-00014 JiangJi 20100128 [iTradeR5] Display 'foreign room' and 'instrument status' fields to portfolio page
		GenericRequest lvGRY = new GenericRequest(ITradeServlet.getTpManager(), null);
		IMsgXMLNode lvNodeY = lvGRY.buildQRYPOPUPXMLHeader();
		lvNodeY.addChildNode(TagName.FuncName);
		lvNodeY.addChildNode(TagName.ObjectName).setValue("VCINSTRUMENTDETAIL");
		lvNodeY.addChildNode(TagName.LoopSelect);
		lvNodeY.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field).setAttribute(TagName.Name, "CURRENTROOM");
		lvNodeY.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field).setAttribute(TagName.Name, "CLOSINGPRICE");  
      	IMsgXMLNode lvConditionLoopY = lvNodeY.addChildNode(TagName.LoopWhere).addChildNode(TagName.Condition);
      	//product id With itrade.ini 
        //BEGIN TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
      	String lvProductId=IMain.getProperty("product");
        //END TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
      	if(lvProductId!=null && lvProductId!=null){
      		IMsgXMLNode lvProductIDConditionY = lvConditionLoopY.addChildNode(TagName.Condition);
	        lvProductIDConditionY.setAttribute(TagName.FieldName, "PRODUCTID");
	        lvProductIDConditionY.setAttribute(TagName.FieldValue, lvProductId);
	        lvProductIDConditionY.setAttribute(TagName.FieldOperation, "=");

	        lvConditionLoopY.addChildNode("LogicalOperator").setAttribute("LogicalOperation", "AND");

	      	IMsgXMLNode lvMarketIDCondition = lvConditionLoopY.addChildNode(TagName.Condition);
	      	lvMarketIDCondition.setAttribute(TagName.FieldName, "MARKETID");
	      	String lvMarketId=getMarketId();
	      	lvMarketIDCondition.setAttribute(TagName.FieldValue, lvMarketId);
	      	lvMarketIDCondition.setAttribute(TagName.FieldOperation, "=");

	      	lvConditionLoopY.addChildNode("LogicalOperator").setAttribute("LogicalOperation", "AND");

	      	IMsgXMLNode lvInstrumentIDCondition = lvConditionLoopY.addChildNode(TagName.Condition);
	      	lvInstrumentIDCondition.setAttribute(TagName.FieldName, "INSTRUMENTID");
	      	lvInstrumentIDCondition.setAttribute(TagName.FieldValue, mvInstrument);
	      	lvInstrumentIDCondition.setAttribute(TagName.FieldOperation, "=");
	      	
	      	lvNodeY.addChildNode(TagName.LoopOrderBy);

	      	IMsgXMLNode lvNodeLoopJoin = lvNodeY.addChildNode("LoopJoin");
	  	  	IMsgXMLNode lvNodeJoin = lvNodeLoopJoin.addChildNode("Join");
	  	  	lvNodeJoin.addChildNode(TagName.ObjectName).setValue("MCCODE");
	  	  	
	  	  	lvNodeJoin.addChildNode(TagName.LoopSelect).addChildNode(TagName.Field).setAttribute(TagName.Name, "DESCRIPTION");
	      	
	  	  	IMsgXMLNode lvJoinCriteria = lvNodeJoin.addChildNode("JoinCriteria");

			IMsgXMLNode lvJoinCondidions = lvJoinCriteria.addChildNode(TagName.Condition);

			IMsgXMLNode lvJoinCondition1 = lvJoinCondidions.addChildNode(TagName.Condition);
			IMsgXMLNode lvField = lvJoinCondition1.addChildNode(TagName.Condition);
			lvField.setAttribute(TagName.FieldName, "STATUS");
			lvField.setAttribute(TagName.FieldValue, "CODEID");
			lvField.setAttribute(TagName.FieldOperation, "=");

			lvJoinCondidions.addChildNode("LogicalOperator").setAttribute("LogicalOperation", "AND");

			IMsgXMLNode lvJoinCondition2 = lvJoinCondidions.addChildNode(TagName.Condition);
			lvField = lvJoinCondition2.addChildNode(TagName.Condition);
			lvField.setAttribute(TagName.FieldName, "CODETYPE");
			lvField.setAttribute(TagName.FieldValue, "'LISTSTATUS'");
			lvField.setAttribute(TagName.FieldOperation, "=");

			lvNodeJoin.addChildNode(TagName.LoopWhere);
			lvNodeJoin.addChildNode(TagName.LoopOrderBy);
			IMsgXMLNode lvreturnNodeY=lvGRY.sendRequestXML(lvNodeY);
			if(lvreturnNodeY==null){
          		//lvreturnNodeY is null
          		Log.println("TP XML Response : is null",Log.ERROR_LOG);
          		setErrorDescription("UNEXPECTEDERROR");
          	}else if(lvreturnNodeY.getChildNode("C_ERROR_CODE")!=null)
          	{
          		//lvreturnNodeY return a errorMessage xml
          		setErrorDescription(lvreturnNodeY.getChildNode("C_ERROR_CODE").getValue());
          	}else
          	{
          		//lvreturnNodeY is correct
          		try {
					String lvStatusDescription=lvreturnNodeY.getChildNode(TagName.LoopRowValue).getChildNode(TagName.RowValue).getAttribute("v2");
					String lvCurrentRoom=lvreturnNodeY.getChildNode(TagName.LoopRowValue).getChildNode(TagName.RowValue).getAttribute("v0");
					String lvPreClosePrice=lvreturnNodeY.getChildNode(TagName.LoopRowValue).getChildNode(TagName.RowValue).getAttribute("v1");
					setMvPreClosePrice(lvPreClosePrice.trim());
					setCurrentRoom(lvCurrentRoom.trim());
					//BEGIN TASK: TTL-GZ-JJ-00006 JiangJi 20091229 [iTrade R5] Stock popup fields remove decimal
					setStatusDescription(lvStatusDescription.trim());
				} catch (Exception e) {}
          		//END TASK: TTL-GZ-JJ-00006 JiangJi 20091229 [iTrade R5] Stock popup fields remove decimal
          		getInstrumentPriceData();
          	}
      	}else{
      		Log.println("AT itrade.ini: product id not Initialization",Log.ERROR_LOG);
			setErrorDescription("UNEXPECTEDERROR");
      	}
       //END TASK: TTL-GZ-JJ-00014 JiangJi 20100128 [iTradeR5] Display 'foreign room' and 'instrument status' fields to portfolio page
	}

	/**
	 * This method to get "DAYOPEN", "FLOOR","CEILING",and "REFERENCEPRICE" for
	 * the stock info.
	 */
	private void getInstrumentPriceData() {
		// BEGIN TASK: TTL-GZ-JJ-00016 JiangJi 20100310 [iTrade R5] Stock popup
		// new layout
		GenericRequest lvGRInstrument = new GenericRequest(ITradeServlet.getTpManager(), null);
		IMsgXMLNode lvInstrumentNode = lvGRInstrument.buildQRYPOPUPXMLHeader();
		lvInstrumentNode.addChildNode(TagName.FuncName);
		lvInstrumentNode.addChildNode(TagName.ObjectName).setValue(
				"MCINSTRUMENT");

		lvInstrumentNode.addChildNode(TagName.LoopSelect).addChildNode(
				TagName.Field).setAttribute(TagName.Name, "STATUS");

		// END TASK: TTL-VN VanTran 20100425 [iTrade R5] add more query fields
		IMsgXMLNode lvInstrumentConditionLoop = lvInstrumentNode.addChildNode(
				TagName.LoopWhere).addChildNode(TagName.Condition);
		IMsgXMLNode lvInstrumentCondition = lvInstrumentConditionLoop
				.addChildNode(TagName.Condition);
		lvInstrumentCondition.setAttribute(TagName.FieldName, "INSTRUMENTID");
		lvInstrumentCondition.setAttribute(TagName.FieldValue, mvInstrument);
		lvInstrumentCondition.setAttribute(TagName.FieldOperation, "=");
		lvInstrumentNode.addChildNode(TagName.LoopOrderBy);

		IMsgXMLNode lvNodeLoopJoin = lvInstrumentNode.addChildNode("LoopJoin");
		IMsgXMLNode lvNodeJoin = lvNodeLoopJoin.addChildNode("Join");
		lvNodeJoin.addChildNode(TagName.ObjectName).setValue(
				"MCINSTRUMENTPRICE");

		lvNodeJoin.addChildNode(TagName.LoopSelect);
		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "DAYOPEN");
		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "FLOOR");
		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "CEILING");
		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "REFERENCEPRICE");
		// BEGIN TASK: TTL-VN VanTran 20100425 [iTrade R5] add more query fields
		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "BESTBIDPRICE1");
		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "BESTBIDPRICE2");
		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "BESTBIDPRICE3");
		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "BESTBIDVOL1");
		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "BESTBIDVOL2");
		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "BESTBIDVOL3");

		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "BESTOFFERPRICE1");
		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "BESTOFFERPRICE2");
		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "BESTOFFERPRICE3");
		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "BESTOFFERVOL1");
		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "BESTOFFERVOL2");
		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "BESTOFFERVOL3");

		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "DAYHIGH");
		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "DAYLOW");
		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "NOMINALPRICE");

		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "DAYCLOSE");

		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "NEXTDAYFLOOR");

		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "NEXTDAYCEILING");
		
		lvNodeJoin.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
		.setAttribute(TagName.Name, "LASTMODIFIEDTIME");

		IMsgXMLNode lvJoinCriteria = lvNodeJoin.addChildNode("JoinCriteria");

		IMsgXMLNode lvJoinCondidions = lvJoinCriteria
				.addChildNode(TagName.Condition);

		IMsgXMLNode lvJoinCondition1 = lvJoinCondidions
				.addChildNode(TagName.Condition);
		IMsgXMLNode lvField = lvJoinCondition1.addChildNode(TagName.Condition);
		lvField.setAttribute(TagName.FieldName, "MCINSTRUMENT.INSTRUMENTID");
		lvField.setAttribute(TagName.FieldValue,
				"MCINSTRUMENTPRICE.INSTRUMENTID");
		lvField.setAttribute(TagName.FieldOperation, "=");

		lvJoinCondidions.addChildNode("LogicalOperator").setAttribute(
				"LogicalOperation", "AND");

		IMsgXMLNode lvJoinCondition3 = lvJoinCondidions
				.addChildNode(TagName.Condition);
		lvField = lvJoinCondition3.addChildNode(TagName.Condition);
		lvField.setAttribute(TagName.FieldName, "MCINSTRUMENT.MARKETID");
		lvField.setAttribute(TagName.FieldValue, "MCINSTRUMENTPRICE.MARKETID");
		lvField.setAttribute(TagName.FieldOperation, "=");

		lvJoinCondidions.addChildNode("LogicalOperator").setAttribute(
				"LogicalOperation", "AND");

		IMsgXMLNode lvJoinCondition2 = lvJoinCondidions
				.addChildNode(TagName.Condition);
		lvField = lvJoinCondition2.addChildNode(TagName.Condition);
		lvField.setAttribute(TagName.FieldName, "MCINSTRUMENT.STATUS");
		lvField.setAttribute(TagName.FieldValue, "('L','S')");
		lvField.setAttribute(TagName.FieldOperation, "in");

		lvNodeJoin.addChildNode(TagName.LoopWhere);
		lvNodeJoin.addChildNode(TagName.LoopOrderBy);

		IMsgXMLNode lvReturnInstrumentNode = lvGRInstrument
				.sendRequestXML(lvInstrumentNode);
		if (lvReturnInstrumentNode == null) {
			// lvReturnInstrumentNode is null
			Log.println("TP XML Response : is null", Log.ERROR_LOG);
			setErrorDescription("UNEXPECTEDERROR");
		} else if (lvReturnInstrumentNode.getChildNode("C_ERROR_CODE") != null) {
			// lvReturnInstrumentNode return a errorMessage xml
			setErrorDescription(lvReturnInstrumentNode.getChildNode(
					"C_ERROR_CODE").getValue());
		} else {
			// lvReturnInstrumentNode is correct
			String lvDayOpen = "";
			//if (getMarketId().equalsIgnoreCase(HKSTag.HOSE_EXCHANGE_CODE)) {
				HKSInstrument lvHKSInstrument = (HKSInstrument) TPManager.mvInstrumentVector
						.get(getMarketId().concat("|").concat(
								getInstrument().toUpperCase()));
				if (lvHKSInstrument != null) {
					lvDayOpen = lvHKSInstrument.getMvOpenPrice();
				//}
			} else {
				lvDayOpen = lvReturnInstrumentNode.getChildNode(
						TagName.LoopRowValue).getChildNode(TagName.RowValue)
						.getAttribute("v1");
			}
			
			String lvFloor = lvReturnInstrumentNode.getChildNode(
					TagName.LoopRowValue).getChildNode(TagName.RowValue)
					.getAttribute("v2");
			String lvCeiling = lvReturnInstrumentNode.getChildNode(
					TagName.LoopRowValue).getChildNode(TagName.RowValue)
					.getAttribute("v3");
			String lvReferencePrice = lvReturnInstrumentNode.getChildNode(
					TagName.LoopRowValue).getChildNode(TagName.RowValue)
					.getAttribute("v4");
		
			// VanTran add
			String temp = lvReturnInstrumentNode.getChildNode(
					TagName.LoopRowValue).getChildNode(TagName.RowValue)
					.getAttribute("v5");
			setMvBidPrice1(temp);
			temp = lvReturnInstrumentNode.getChildNode(TagName.LoopRowValue)
					.getChildNode(TagName.RowValue).getAttribute("v6");
			setMvBidPrice2(temp);
			temp = lvReturnInstrumentNode.getChildNode(TagName.LoopRowValue)
					.getChildNode(TagName.RowValue).getAttribute("v7");
			setMvBidPrice3(temp);

			temp = lvReturnInstrumentNode.getChildNode(TagName.LoopRowValue)
					.getChildNode(TagName.RowValue).getAttribute("v8");
			setMvBidVol1(temp);
			temp = lvReturnInstrumentNode.getChildNode(TagName.LoopRowValue)
					.getChildNode(TagName.RowValue).getAttribute("v9");
			setMvBidVol2(temp);
			temp = lvReturnInstrumentNode.getChildNode(TagName.LoopRowValue)
					.getChildNode(TagName.RowValue).getAttribute("v10");
			setMvBidVol3(temp);

			temp = lvReturnInstrumentNode.getChildNode(TagName.LoopRowValue)
					.getChildNode(TagName.RowValue).getAttribute("v11");
			setMvOfferPrice1(temp);
			temp = lvReturnInstrumentNode.getChildNode(TagName.LoopRowValue)
					.getChildNode(TagName.RowValue).getAttribute("v12");
			setMvOfferPrice2(temp);
			temp = lvReturnInstrumentNode.getChildNode(TagName.LoopRowValue)
					.getChildNode(TagName.RowValue).getAttribute("v13");
			setMvOfferPrice3(temp);

			temp = lvReturnInstrumentNode.getChildNode(TagName.LoopRowValue)
					.getChildNode(TagName.RowValue).getAttribute("v14");
			setMvOfferVol1(temp);
			temp = lvReturnInstrumentNode.getChildNode(TagName.LoopRowValue)
					.getChildNode(TagName.RowValue).getAttribute("v15");
			setMvOfferVol2(temp);
			temp = lvReturnInstrumentNode.getChildNode(TagName.LoopRowValue)
					.getChildNode(TagName.RowValue).getAttribute("v16");
			setMvOfferVol3(temp);

			temp = lvReturnInstrumentNode.getChildNode(TagName.LoopRowValue)
					.getChildNode(TagName.RowValue).getAttribute("v17");
			setMvHigh(temp);
			temp = lvReturnInstrumentNode.getChildNode(TagName.LoopRowValue)
					.getChildNode(TagName.RowValue).getAttribute("v18");
			setMvLow(temp);
			temp = lvReturnInstrumentNode.getChildNode(TagName.LoopRowValue)
					.getChildNode(TagName.RowValue).getAttribute("v19");
			setMvNomial(temp);
      		
			BigDecimal lvNominalPrice =new BigDecimal(0);
			if(!temp.trim().equals("")){
				lvNominalPrice = new BigDecimal(temp);
			}
			
			temp = lvReturnInstrumentNode.getChildNode(TagName.LoopRowValue)
					.getChildNode(TagName.RowValue).getAttribute("v20");
			setMvDayClose(temp);
			
			temp = lvReturnInstrumentNode.getChildNode(TagName.LoopRowValue)
					.getChildNode(TagName.RowValue).getAttribute("v21");
			setMvNextFloor(temp);
	
			temp = lvReturnInstrumentNode.getChildNode(TagName.LoopRowValue)
					.getChildNode(TagName.RowValue).getAttribute("v22");
			setMvNextCeiling(temp);
			
			temp = lvReturnInstrumentNode.getChildNode(TagName.LoopRowValue)
					.getChildNode(TagName.RowValue).getAttribute("v23");
			try {
				setMvLastModifiedTime(DateUtil.convertStringToDate("yyyy-MM-dd", temp));
			} catch (ParseException e) {
//				Log.println(e, Log.ERROR_LOG);
			}
			
			// If HOSE then reference price = pre close price
      		/*if(getMarketId().equalsIgnoreCase(HKSTag.HOSE_EXCHANGE_CODE)){
          		setReferencePrice(getMvPreClosePrice());
      		}else {
          		setReferencePrice(lvReferencePrice);
      		}*/
			if(IMain.stockHistMap.containsKey(getInstrument())){
				if (getMarketId().equalsIgnoreCase(HKSTag.HOSE_EXCHANGE_CODE)) {
					long lastKey = IMain.stockHistMap.get(getInstrument()).lastKey();
					String value = IMain.stockHistMap.get(getInstrument()).get(lastKey);
					String[] lastObject = value.split("\\|");
					try {
						setReferencePrice(lastObject[6]);
					} catch (Exception e) {
						Log.println(e, Log.ERROR_LOG);
					}
				}else{
					setReferencePrice(lvReferencePrice);
				}
			}else{
				if (getMarketId().equalsIgnoreCase(HKSTag.HOSE_EXCHANGE_CODE)) {
					setReferencePrice(getMvPreClosePrice());
				} else {
					setReferencePrice(lvReferencePrice);
				}
			}
//      		System.out.println(">>>>>>>>>>>>>" + getInstrument() + "->" + getReferencePrice());
			BigDecimal lvReferencePriceDecimal = (getReferencePrice() == null || getReferencePrice().equals("")) ? new BigDecimal(0):new BigDecimal(getReferencePrice());
			
			if (lvNominalPrice.doubleValue() <= 0) {
				setMvNoalPriSubRefPri("-");
				setMvNoalPriPerRate("-");
			} else {
				setMvNoalPriSubRefPri((lvNominalPrice
						.subtract(lvReferencePriceDecimal).setScale(1,
						BigDecimal.ROUND_HALF_UP)).toString());
				
				if (lvReferencePriceDecimal.doubleValue() != 0) {
					setMvNoalPriPerRate((lvNominalPrice.subtract(lvReferencePriceDecimal).multiply(new BigDecimal(100)).divide(
							lvReferencePriceDecimal, 1, BigDecimal.ROUND_HALF_UP)).toString());
				}
			}
		
			// End VanTran add
			setDayOpen(lvDayOpen);
			setFloor(lvFloor);
			setCeiling(lvCeiling);
		}
		// END TASK: TTL-GZ-JJ-00016 JiangJi 20100310 [iTrade R5] Stock popup
		// new layout
	}

	public void setMvNomial(String mvNomial) {
		this.mvNomial = mvNomial;
	}

	public String getMvNomial() {
		return mvNomial;
	}

	/**
	 * @param mvDayClose
	 *            the mvDayClose to set
	 */
	public void setMvDayClose(String mvDayClose) {
		this.mvDayClose = mvDayClose;
	}

	/**
	 * @return the mvDayClose
	 */
	public String getMvDayClose() {
		return mvDayClose;
	}

	/**
	 * @param mvNoalPriSubRefPri the mvNoalPriSubRefPri to set
	 */
	public void setMvNoalPriSubRefPri(String mvNoalPriSubRefPri) {
		this.mvNoalPriSubRefPri = mvNoalPriSubRefPri;
	}

	/**
	 * @return the mvNoalPriSubRefPri
	 */
	public String getMvNoalPriSubRefPri() {
		return mvNoalPriSubRefPri;
	}

	public void setMvPreClosePrice(String mvPreClosePrice) {
		this.mvPreClosePrice = mvPreClosePrice;
	}

	public String getMvPreClosePrice() {
		return mvPreClosePrice;
	}

	public void setMvNextFloor(String mvNextFloor) {
		this.mvNextFloor = mvNextFloor;
	}

	public String getMvNextFloor() {
		return mvNextFloor;
	}

	public void setMvNextCeiling(String mvNextCeiling) {
		this.mvNextCeiling = mvNextCeiling;
	}

	public String getMvNextCeiling() {
		return mvNextCeiling;
	}

	public void setMvLastModifiedTime(Date mvLastModifiedTime) {
		this.mvLastModifiedTime = mvLastModifiedTime;
	}

	public Date getMvLastModifiedTime() {
		return mvLastModifiedTime;
	}
}
// END TASK: TTL-GZ-JJ-00001 JiangJi 20091206 [iTradeR5 VN] FO:Pop up stock info