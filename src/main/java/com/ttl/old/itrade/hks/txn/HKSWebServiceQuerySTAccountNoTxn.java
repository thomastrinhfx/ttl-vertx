package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;

/**
 * This class process the web service query stock account No
 * @author not attributable
 *
 */
public class HKSWebServiceQuerySTAccountNoTxn extends BaseTxn{
	
	private String mvCINO="";
	private String mvResult="";
	
	TPErrorHandling tpError;
	
	private int mvReturnCode;
	private String mvErrorCode;
	private String mvErrorMessage;
	/**
	 * Default constructor for HKSWebServiceQuerySTAccountNoTxn class
	 */
	public HKSWebServiceQuerySTAccountNoTxn(){
		tpError = new TPErrorHandling();
	}
	/**
	 * Default constructor for HKSWebServiceQuerySTAccountNoTxn class
	 * @param pCINO the client NO
	 */
	public HKSWebServiceQuerySTAccountNoTxn(String pCINO){
		tpError = new TPErrorHandling();
		setCINO(pCINO);
	}
	/**
	 * The method process the web service query stock account No
	 */
	public void process()
	{
		try
		{
			Log.println("[ HKSWebServiceQuerySTAccountNoTxn.process(): CINO "+ getCINO()+" starts " , Log.ACCESS_LOG);
			Hashtable lvTxnMap = new Hashtable();
			IMsgXMLNode lvRetNode;
			TPManager ivTPManager = ITradeServlet.getTPManager(RequestName.HKSWSQuerySTAccountNoRequest);
			TPBaseRequest  lvHKSWebServiceRequest = ivTPManager.getRequest(RequestName.HKSWSQuerySTAccountNoRequest);

			lvTxnMap.put("CINO", this.getCINO());

			lvRetNode = lvHKSWebServiceRequest.send(lvTxnMap);
			
			setReturnCode(tpError.checkError(lvRetNode));
			
			if (mvReturnCode != tpError.TP_NORMAL)
			{
				setErrorMessage(tpError.getErrDesc());
				if (mvReturnCode == tpError.TP_APP_ERR)
				{
					setErrorCode(tpError.getErrCode());
				}
			}
			else
			{
				mvResult = lvRetNode.getChildNode("STNO").getValue();
			}
		}catch (Exception e)
		{	
			Log.println("HKSWebServiceQuerySTAccountNoTxn CINO "+ getCINO()+" Error: " + e.toString(), Log.ERROR_LOG);
		}
	}
	/**
	 * Get method for result
	 * @return result
	 */
	public String getResult() {
		return mvResult;
	}
	/**
	 * Set method for return system code
	 * @param pReturnCode the return system code
	 */
	public void setReturnCode(int pReturnCode)
	{
	    mvReturnCode = pReturnCode;
	}
	/**
	 * Set method for error system code
	 * @param pErrorCode the error system code
	 */
	public void setErrorCode(String pErrorCode)
    {
        mvErrorCode = pErrorCode;
    }
	/**
	 * Set method for error system message
	 * @param pErrorMessage the error system message
	 */
	public void setErrorMessage(String pErrorMessage)
    {
        mvErrorMessage = pErrorMessage;
    }
	/**
	 * Get method for client NO
	 * @return client NO
	 */
	public String getCINO() {
		return mvCINO;
	}
	/**
	 * Set method for client NO
	 * @param pCINO the client NO
	 */
	public void setCINO(String pCINO) {
		this.mvCINO = pCINO;
	}
}
