package com.ttl.old.itrade.hks.bean;

/**
 * The HKSComboBoxBean class define variables that to save values 
 * for action 
 * @author 
 *
 */
public class HKSComboBoxBean {

	private String mvID;
	private String mvOptionValue;
	private String mvOptionDisplay;
	
	/**
     * This method returns the combobox id.
     * @return the combobox id.
     */
	public String getMvID() {
		return mvID;
	}
	/**
     * This method sets the combobox id.
     * @param pID The combobox id.
     */
	public void setMvID(String pID) {
		this.mvID = pID;
	}
	/**
     * This method returns the combobox option value.
     * @return the combobox option value.
     */
	public String getMvOptionValue() {
		return mvOptionValue;
	}
	/**
     * This method sets the combobox option value.
     * @param pOptionValue The combobox option value.
     */
	public void setMvOptionValue(String pOptionValue) {
		this.mvOptionValue = pOptionValue;
	}
	/**
     * This method returns the combobox option display value.
     * @return the combobox option display value.
     */
	public String getMvOptionDisplay() {
		return mvOptionDisplay;
	}
	/**
     * This method sets the combobox option display value.
     * @param pOptionDisplay The combobox option display value.
     */
	public void setMvOptionDisplay(String pOptionDisplay) {
		this.mvOptionDisplay = pOptionDisplay;
	}
}
