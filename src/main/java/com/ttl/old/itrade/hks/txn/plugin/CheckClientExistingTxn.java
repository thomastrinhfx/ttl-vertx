package com.ttl.old.itrade.hks.txn.plugin;

import java.util.Hashtable;

import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * 
 * @author Nha-Dang
 *
 */
public class CheckClientExistingTxn extends BaseTxn {

	private String  clientId;		 
	
	private String language;
	
	private boolean isExisting = false;
	
	private String clientName = null;
	
	public CheckClientExistingTxn(String clientId) {
		super();
		this.clientId = clientId;
	}
	
	public void process() {
		Hashtable<String,String> lvTxnMap = new Hashtable<String,String>();
		lvTxnMap.put(TagName.CLIENTID, clientId);
		if (TPErrorHandling.TP_NORMAL == process("CheckClientExisting", lvTxnMap, language)) {
			try {
				isExisting = Boolean.valueOf(mvReturnNode.getChildNode("IS_CLIENT_EXISTING").getValue());
				clientName = (mvReturnNode.getChildNode("CLIENT_NAME").getValue().trim());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}			
			
		}
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public boolean isExisting() {
		return isExisting;
	}

	public void setExisting(boolean isExisting) {
		this.isExisting = isExisting;
	}
	
}
