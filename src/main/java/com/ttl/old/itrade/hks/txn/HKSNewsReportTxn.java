package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPErrorHandling;

/**
 * This class store the News Report
 * @author not attributable
 *
 */
public class HKSNewsReportTxn extends BaseTxn
{
	
	private String mvMarketID;
	private String mvClientID;
	private String mvLoopCounter;
	/**
	 * Constructor for HKSNewsReportTxn class
	 * @param pMarketID the market id
	 * @param pClientID the client id
	 */
	public HKSNewsReportTxn(String pMarketID, String pClientID)
	{
		super();
		mvMarketID = pMarketID;
		mvClientID = pClientID;
	}
	/**
	 * This method store the News Report
	 * @return A collection of news reports
	 */
	public Vector process()
	{
		Vector lvReturn = new Vector();
		
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put("MARKETID", getMarketID());
		lvTxnMap.put("CLIENTID", getClientID());
		
		if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSMarketDataRequest, lvTxnMap))
    	{
			
    	}
		
		return lvReturn;
	}
	/**
	 * Get method for market id
	 * @return market id
	 */
	public String getMarketID()
	{
		return mvMarketID;
	}
	/**
	 * Get method for client id
	 * @return  client id
	 */
	public String getClientID()
	{
		return mvClientID;
	}
	/**
	 * Set method for market id
	 * @param pMarketID the market id
	 */
	public void setMarketID(String pMarketID)
	{
		mvMarketID = pMarketID;
	}
	/**
	 * Set method for client id
	 * @param pClientID the client id
	 */
	public void setClientID(String pClientID)
	{
		mvClientID = pClientID;
	}
	/**
	 * Get method for loop counter
	 * @return Loop counter counts the number of
	 */
	public int getLoopCounter()
    {
        int lvCnt;
        try
        {
            lvCnt = Integer.parseInt(mvLoopCounter);
        }
        catch (Exception e)
        {
            return -1;
        }
        return lvCnt;
    }
	/**
	 * Set method for loop counter
	 * @param pLoopCounter the loop counter
	 */
	public void setLoopCounter(String pLoopCounter)
    {
        mvLoopCounter = pLoopCounter;
    }
	
}
