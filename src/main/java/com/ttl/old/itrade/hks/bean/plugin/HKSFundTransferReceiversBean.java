package com.ttl.old.itrade.hks.bean.plugin;

/**
 * The HKSAdvancePaymentInfoBean class define variables that to save values for action 
 * @author Wind.Zhao
 *
 */

//Begin Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100127 [iTrade R5] Make Advance Payment a Plugin Module
public class HKSFundTransferReceiversBean {
	private String receiverAccType;
	private String receiverAccID;
	private String receiverName;
	private String receiverBankName;
	private String transferFee;
	private String minFeeAmt;
	private String maxFeeAmt;
	
	
	/**
	 * @return the receiverAccID
	 */
	public String getReceiverAccID() {
		return receiverAccID;
	}
	/**
	 * @param receiverAccID the receiverAccID to set
	 */
	public void setReceiverAccID(String receiverAccID) {
		this.receiverAccID = receiverAccID;
	}
	/**
	 * @return the receiverAccType
	 */
	public String getReceiverAccType() {
		return receiverAccType;
	}
	/**
	 * @param receiverAccType the receiverAccType to set
	 */
	public void setReceiverAccType(String receiverAccType) {
		this.receiverAccType = receiverAccType;
	}
	/**
	 * @return the receiverName
	 */
	public String getReceiverName() {
		return receiverName;
	}
	/**
	 * @param receiverName the receiverName to set
	 */
	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}
	/**
	 * @return the receiverBankName
	 */
	public String getReceiverBankName() {
		return receiverBankName;
	}
	/**
	 * @param receiverBankName the receiverBankName to set
	 */
	public void setReceiverBankName(String receiverBankName) {
		this.receiverBankName = receiverBankName;
	}
	/**
	 * @return the transferFee
	 */
	public String getTransferFee() {
		return transferFee;
	}
	/**
	 * @param transferFee the transferFee to set
	 */
	public void setTransferFee(String transferFee) {
		this.transferFee = transferFee;
	}
	/**
	 * @param minFeeAmt the minFeeAmt to set
	 */
	public void setMinFeeAmt(String minFeeAmt) {
		this.minFeeAmt = minFeeAmt;
	}
	/**
	 * @return the minFeeAmt
	 */
	public String getMinFeeAmt() {
		return minFeeAmt;
	}
	/**
	 * @param maxFeeAmt the maxFeeAmt to set
	 */
	public void setMaxFeeAmt(String maxFeeAmt) {
		this.maxFeeAmt = maxFeeAmt;
	}
	/**
	 * @return the maxFeeAmt
	 */
	public String getMaxFeeAmt() {
		return maxFeeAmt;
	}
	
	
}
//End Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100127 [iTrade R5] Make Advance Payment a Plugin Module
