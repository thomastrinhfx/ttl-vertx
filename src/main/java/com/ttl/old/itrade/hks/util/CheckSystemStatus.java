package com.ttl.old.itrade.hks.util;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.GenericRequest;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.winvest.hks.config.mapping.TagName;

public class CheckSystemStatus {
	public static boolean isDayEndSystemStatus() {
		boolean isDayEnd = false;

		GenericRequest lvGRY = new GenericRequest(ITradeServlet.getTpManager(),
				null);
		IMsgXMLNode lvNodeY = lvGRY.buildQRYPOPUPXMLHeader();
		lvNodeY.addChildNode(TagName.FuncName);
		lvNodeY.addChildNode(TagName.ObjectName).setValue("MCSYS");
		lvNodeY.addChildNode(TagName.LoopSelect);
		lvNodeY.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "STATUS");
		/*lvNodeY.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "TRADEDATE");*/
		lvNodeY.addChildNode(TagName.LoopWhere);
		lvNodeY.addChildNode(TagName.LoopOrderBy);

		IMsgXMLNode lvreturnNodeY = lvGRY.sendRequestXML(lvNodeY);
		if (lvreturnNodeY == null) {
			Log.println("TP XML Response : is null", Log.ERROR_LOG);
		} else if (lvreturnNodeY.getChildNode("C_ERROR_CODE") != null) {
			Log.println(lvreturnNodeY.getChildNode("C_ERROR_CODE").getValue(),
					Log.ERROR_LOG);
		} else {
			String lvStatus = lvreturnNodeY.getChildNode(TagName.LoopRowValue)
					.getChildNode(TagName.RowValue).getAttribute("v0").trim();
			/*String lvTradeDate = lvreturnNodeY.getChildNode(
					TagName.LoopRowValue).getChildNode(TagName.RowValue)
					.getAttribute("v1");*/
			/*Log.println("Trade date :" + lvTradeDate
					+ " and DAYENDSTARTSTATUS: " + lvStatus, Log.DEBUG_LOG);*/
			isDayEnd = "E".equalsIgnoreCase(lvStatus);
		}
		return isDayEnd;
	}
}
