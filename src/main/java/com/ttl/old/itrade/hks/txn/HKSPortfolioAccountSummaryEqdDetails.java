package com.ttl.old.itrade.hks.txn;

public class HKSPortfolioAccountSummaryEqdDetails {

	private String mvClientID;
	private String mvToCurrencyID;
	private String mvCurrencyID;
	private int mvAccountSeq;
	private String mvCDueBuy;
	private String mvCDueSell;
	private String mvCSettled;
	private String mvCUnsettleBuy;
	private String mvCUnsettleSell;

	private String mvCTodayBuy;
	private String mvCTodaySell;
	private String mvCTodayConfirmShortsell;
	private String mvCTodayConfirmBuy;
	private String mvCTodayConfirmSell;
	private String mvCTodayIn;
	private String mvCTodayOut;
	private String mvCPendingDeposit;
	private String mvCPendingWithdrawal;
	private String mvCCashWithdrawable;		//HIEU LE: get one more field
	private String mvCTodayUnsettleBuy;
	private String mvCTodayUnsettleSell;
	private String mvCT1UnsettleBuy;
	private String mvCT2UnsettleBuy;
	private String mvCT3UnsettleBuy;
	private String mvCT4UnsettleBuy;
	private String mvCT1UnsettleSell;
	private String mvCT2UnsettleSell;
	private String mvCT3UnsettleSell;
	private String mvCT4UnsettleSell;
	private String mvCManualHold;
	private String mvCManualReserve;
	private String mvCCreditLine;
	private String mvCMortgagePendingAmt;
	private String mvCMortgageHoldAmt;
	private String mvCDailyOpenBal;
	private String mvCMonthlyOpenBal;
	private String mvAvailableBal;
	private String mvLedgerBal;
	private String mvSettledBalance;
	private String mvMarginableBalf;
	private String mvMarginableBal;
	private String mvTradableBal;
	private String mvDrawableBal;
	private String mvAccruedInterest;

	private String mvTodaySettlement;
	private String mvHoldAmount;
	private String mvReserveAmount;
	private String mvInterest;
	private String mvDPWD;
	private String mvUsable;

	private String mvDebitAccruedInterest;
	private String mvCreditAccruedInterest;
	private String mvCNextDayDueSell;
	private String mvCNextDayDueBuy;
	private String mvCDailyMarginableBal;
	private String mvCInactiveBuy;
	private String mvCTodayHoldAmtExt;
	private String mvCShortSellAmt;
	private String mvCTodayOverrideAmt;
	private String mvBaseCcy;
	private String mvEvaluationRate;
	private String mvConversionRate;

	private String mvTDMortgageAmt;
	private String mvCManualReleaseAmt;
	private String mvCUnrealizedLoan;
	private String mvCChargeableAmt;
	private String mvAvailAdvanceMoney;
	private String mvOutstandingLoan;
	private String mvPenddingWithdrawMoney;

	private String mvTotalOutAdvance;
	private String mvBuyingPowerd;
	private String buyAmt;
	private String totalAsset;
	private String equity;
	private String stockMaintenance;
	private String cashMaintenance;
	private String unrealizedLoan;

	public HKSPortfolioAccountSummaryEqdDetails() {

	}

	public String getClientID() {
		return mvClientID;
	}

	public String getToCurrencyID() {
		return mvToCurrencyID;
	}

	public String getCurrencyID() {
		return mvCurrencyID;
	}

	public int getAccountSeq() {
		return mvAccountSeq;
	}

	public String getCDueBuy() {
		return mvCDueBuy;
	}

	public String getCDueSell() {
		return mvCDueSell;
	}

	public String getCSettled() {
		return mvCSettled;
	}

	public String getCUnsettleBuy() {
		return mvCUnsettleBuy;
	}

	public String getCUnsettleSell() {
		return mvCUnsettleSell;
	}

	public String getCTodayBuy() {
		return mvCTodayBuy;
	}

	public String getCTodaySell() {
		return mvCTodaySell;
	}

	public String getCTodayConfirmShortsell() {
		return mvCTodayConfirmShortsell;
	}

	public String getCTodayConfirmBuy() {
		return mvCTodayConfirmBuy;
	}

	public String getCTodayConfirmSell() {
		return mvCTodayConfirmSell;
	}

	public String getCManualHold() {
		return mvCManualHold;
	}

	public String getCManualReserve() {
		return mvCManualReserve;
	}

	public String getCCreditLine() {
		return mvCCreditLine;
	}

	public String getCMortgagePendingAmt() {
		return mvCMortgagePendingAmt;
	}

	public String getCMortgageHoldAmt() {
		return mvCMortgageHoldAmt;
	}

	public String getCDailyOpenBal() {
		return mvCDailyOpenBal;
	}

	public String getCDailyMarginableBal() {
		return mvCDailyMarginableBal;
	}

	public String getCMonthlyOpenBal() {
		return mvCMonthlyOpenBal;
	}

	public String getAvailableBal() {
		return mvAvailableBal;
	}

	public String getLedgerBal() {
		return mvLedgerBal;
	}

	public String getMarginableBalf() {
		return mvMarginableBalf;
	}

	public String getMarginableBal() {
		return mvMarginableBal;
	}

	public String getTradableBal() {
		return mvTradableBal;
	}

	public String getDrawableBal() {
		return mvDrawableBal;
	}

	public String getAccruedInterest() {
		return mvAccruedInterest;
	}

	public String getDebitAccruedInterest() {
		return mvDebitAccruedInterest;
	}

	public String getCreditAccruedInterest() {
		return mvCreditAccruedInterest;
	}

	public String getCNextDayDueSell() {
		return mvCNextDayDueSell;
	}

	public String getCNextDayDueBuy() {
		return mvCNextDayDueBuy;
	}

	public String getCInactiveBuy() {
		return mvCInactiveBuy;
	}

	public String getCTodayHoldAmtExt() {
		return mvCTodayHoldAmtExt;
	}

	public String getCShortSellAmt() {
		return mvCShortSellAmt;
	}

	public String getCTodayOverrideAmt() {
		return mvCTodayOverrideAmt;
	}

	public String getBaseCcy() {
		return mvBaseCcy;
	}

	public String getEvaluationRate() {
		return mvEvaluationRate;
	}

	public String getConversionRate() {
		return mvConversionRate;
	}

	public String getTDMortgageAmt() {
		return mvTDMortgageAmt;
	}

	public String getCManualReleaseAmt() {
		return mvCManualReleaseAmt;
	}

	public void setClientID(String pClientID) {
		mvClientID = pClientID;
	}

	public void setToCurrencyID(String pToCurrencyID) {
		mvToCurrencyID = pToCurrencyID;
	}

	public void setCurrencyID(String pCurrencyID) {
		mvCurrencyID = pCurrencyID;
	}

	public void setBaseCcy(String pBaseCcy) {
		mvBaseCcy = pBaseCcy;
	}

	public void setAccountSeq(int pAccountSeq) {
		mvAccountSeq = pAccountSeq;
	}

	public void setCDueBuy(String pCDueBuy) {
		mvCDueBuy = pCDueBuy;
	}

	public void setCDueSell(String pCDueSell) {
		mvCDueSell = pCDueSell;
	}

	public void setCSettled(String pCSettled) {
		mvCSettled = pCSettled;
	}

	public void setCUnsettleBuy(String pCUnsettleBuy) {
		mvCUnsettleBuy = pCUnsettleBuy;
	}

	public void setCUnsettleSell(String pCUnsettleSell) {
		mvCUnsettleSell = pCUnsettleSell;
	}

	public void setCTodayBuy(String pCTodayBuy) {
		mvCTodayBuy = pCTodayBuy;
	}

	public void setCTodaySell(String pCTodaySell) {
		mvCTodaySell = pCTodaySell;
	}

	public void setCTodayConfirmShortsell(String pCTodayConfirmShortsell) {
		mvCTodayConfirmShortsell = pCTodayConfirmShortsell;
	}

	public void setCTodayConfirmBuy(String pCTodayConfirmBuy) {
		mvCTodayConfirmBuy = pCTodayConfirmBuy;
	}

	public void setCTodayConfirmSell(String pCTodayConfirmSell) {
		mvCTodayConfirmSell = pCTodayConfirmSell;
	}

	public void setCManualHold(String pCManualHold) {
		mvCManualHold = pCManualHold;
	}

	public void setCManualReserve(String pCManualReserve) {
		mvCManualReserve = pCManualReserve;
	}

	public void setCCreditLine(String pCCreditLine) {
		mvCCreditLine = pCCreditLine;
	}

	public void setCMortgagePendingAmt(String pCMortgagePendingAmt) {
		mvCMortgagePendingAmt = pCMortgagePendingAmt;
	}

	public void setCMortgageHoldAmt(String pCMortgageHoldAmt) {
		mvCMortgageHoldAmt = pCMortgageHoldAmt;
	}

	public void setCDailyOpenBal(String pCDailyOpenBal) {
		mvCDailyOpenBal = pCDailyOpenBal;
	}

	public void setCMonthlyOpenBal(String pCMonthlyOpenBal) {
		mvCMonthlyOpenBal = pCMonthlyOpenBal;
	}

	public void setAvailableBal(String pAvailableBal) {
		mvAvailableBal = pAvailableBal;
	}

	public void setLedgerBal(String pLedgerBal) {
		mvLedgerBal = pLedgerBal;
	}

	public void setEvaluationRate(String pEvaluationRate) {
		mvEvaluationRate = pEvaluationRate;
	}

	public void setConversionRate(String pConversionRate) {
		mvConversionRate = pConversionRate;
	}

	public void setMarginableBalf(String pMarginableBalf) {
		mvMarginableBalf = pMarginableBalf;
	}

	public void setMarginableBal(String pMarginableBal) {
		mvMarginableBal = pMarginableBal;
	}

	public void setTradableBal(String pTradableBal) {
		mvTradableBal = pTradableBal;
	}

	public void setDrawableBal(String pDrawableBal) {
		mvDrawableBal = pDrawableBal;
	}

	public void setAccruedInterest(String pAccruedInterest) {
		mvAccruedInterest = pAccruedInterest;
	}

	public void setDebitAccruedInterest(String pDebitAccruedInterest) {
		mvDebitAccruedInterest = pDebitAccruedInterest;
	}

	public void setCreditAccruedInterest(String pCreditAccruedInterest) {
		mvCreditAccruedInterest = pCreditAccruedInterest;
	}

	public void setCNextDayDueSell(String pCNextDayDueSell) {
		mvCNextDayDueSell = pCNextDayDueSell;
	}

	public void setCNextDayDueBuy(String pCNextDayDueBuy) {
		mvCNextDayDueBuy = pCNextDayDueBuy;
	}

	public void setCDailyMarginableBal(String pCDailyMarginableBal) {
		mvCDailyMarginableBal = pCDailyMarginableBal;
	}

	public void setCInactiveBuy(String pCInactiveBuy) {
		mvCInactiveBuy = pCInactiveBuy;
	}

	public void setCTodayHoldAmtExt(String pCTodayHoldAmtExt) {
		mvCTodayHoldAmtExt = pCTodayHoldAmtExt;
	}

	public void setCShortSellAmt(String pCShortSellAmt) {
		mvCShortSellAmt = pCShortSellAmt;
	}

	public void setCTodayOverrideAmt(String pCTodayOverrideAmt) {
		mvCTodayOverrideAmt = pCTodayOverrideAmt;
	}

	public void setTDMortgageAmt(String pTDMortgageAmt) {
		mvTDMortgageAmt = pTDMortgageAmt;
	}

	public void setCManualReleaseAmt(String pvManualReleaseAmt) {
		mvCManualReleaseAmt = pvManualReleaseAmt;
	}

	public String getCTodayUnsettleBuy() {
		return mvCTodayUnsettleBuy;
	}

	public void setCTodayUnsettleBuy(String pTodayUnsettleBuy) {
		mvCTodayUnsettleBuy = pTodayUnsettleBuy;
	}

	public String getCTodayUnsettleSell() {
		return mvCTodayUnsettleSell;
	}

	public void setCTodayUnsettleSell(String pTodayUnsettleSell) {
		mvCTodayUnsettleSell = pTodayUnsettleSell;
	}

	public String getCT1UnsettleBuy() {
		return mvCT1UnsettleBuy;
	}

	public void setCT1UnsettleBuy(String pUnsettleBuy) {
		mvCT1UnsettleBuy = pUnsettleBuy;
	}

	public String getCT2UnsettleBuy() {
		return mvCT2UnsettleBuy;
	}

	public void setCT2UnsettleBuy(String pUnsettleBuy) {
		mvCT2UnsettleBuy = pUnsettleBuy;
	}

	public String getCT3UnsettleBuy() {
		return mvCT3UnsettleBuy;
	}

	public void setCT3UnsettleBuy(String pUnsettleBuy) {
		mvCT3UnsettleBuy = pUnsettleBuy;
	}

	public String getCT4UnsettleBuy() {
		return mvCT4UnsettleBuy;
	}

	public void setCT4UnsettleBuy(String pUnsettleBuy) {
		mvCT4UnsettleBuy = pUnsettleBuy;
	}

	public String getCT1UnsettleSell() {
		return mvCT1UnsettleSell;
	}

	public void setCT1UnsettleSell(String pUnsettleSell) {
		mvCT1UnsettleSell = pUnsettleSell;
	}

	public String getCT2UnsettleSell() {
		return mvCT2UnsettleSell;
	}

	public void setCT2UnsettleSell(String pUnsettleSell) {
		mvCT2UnsettleSell = pUnsettleSell;
	}

	public String getCT3UnsettleSell() {
		return mvCT3UnsettleSell;
	}

	public void setCT3UnsettleSell(String pUnsettleSell) {
		mvCT3UnsettleSell = pUnsettleSell;
	}

	public String getCT4UnsettleSell() {
		return mvCT4UnsettleSell;
	}

	public void setCT4UnsettleSell(String pUnsettleSell) {
		mvCT4UnsettleSell = pUnsettleSell;
	}

	public String getCTodayIn() {
		return mvCTodayIn;
	}

	public void setCTodayIn(String pTodayIn) {
		mvCTodayIn = pTodayIn;
	}

	public String getCTodayOut() {
		return mvCTodayOut;
	}

	public void setCTodayOut(String pTodayOut) {
		mvCTodayOut = pTodayOut;
	}

	public void setCPendingDeposit(String pPendingDeposit) {
		mvCPendingDeposit = pPendingDeposit;
	}

	public String getCPendingWithdrawal() {
		return mvCPendingWithdrawal;
	}

	public void setCPendingWithdrawal(String pPendingWithdrawal) {
		mvCPendingWithdrawal = pPendingWithdrawal;
	}

	public String getCPendingDeposit() {
		return mvCPendingDeposit;
	}

	public String getCUnrealizedLoan() {
		return mvCUnrealizedLoan;
	}

	public void setCUnrealizedLoan(String pCUnrealizedLoan) {
		mvCUnrealizedLoan = pCUnrealizedLoan;
	}

	public String getCChargeableAmt() {
		return mvCChargeableAmt;
	}

	public void setCChargeableAmt(String pCChargeableAmt) {
		mvCChargeableAmt = pCChargeableAmt;
	}

	/**
	 * @return the mvSettledBalance
	 */
	public String getMvSettledBalance() {
		return mvSettledBalance;
	}

	/**
	 * @param mvSettledBalance
	 *            the mvSettledBalance to set
	 */
	public void setMvSettledBalance(String mvSettledBalance) {
		this.mvSettledBalance = mvSettledBalance;
	}

	/**
	 * @return the mvTodaySettlement
	 */
	public String getMvTodaySettlement() {
		return mvTodaySettlement;
	}

	/**
	 * @param mvTodaySettlement
	 *            the mvTodaySettlement to set
	 */
	public void setMvTodaySettlement(String mvTodaySettlement) {
		this.mvTodaySettlement = mvTodaySettlement;
	}

	/**
	 * @return the mvHoldAmount
	 */
	public String getMvHoldAmount() {
		return mvHoldAmount;
	}

	/**
	 * @param mvHoldAmount
	 *            the mvHoldAmount to set
	 */
	public void setMvHoldAmount(String mvHoldAmount) {
		this.mvHoldAmount = mvHoldAmount;
	}

	/**
	 * @return the mvReserveAmount
	 */
	public String getMvReserveAmount() {
		return mvReserveAmount;
	}

	/**
	 * @param mvReserveAmount
	 *            the mvReserveAmount to set
	 */
	public void setMvReserveAmount(String mvReserveAmount) {
		this.mvReserveAmount = mvReserveAmount;
	}

	/**
	 * @return the mvInterest
	 */
	public String getMvInterest() {
		return mvInterest;
	}

	/**
	 * @param mvInterest
	 *            the mvInterest to set
	 */
	public void setMvInterest(String mvInterest) {
		this.mvInterest = mvInterest;
	}

	/**
	 * @return the mvDPWD
	 */
	public String getMvDPWD() {
		return mvDPWD;
	}

	/**
	 * @param mvDPWD
	 *            the mvDPWD to set
	 */
	public void setMvDPWD(String mvDPWD) {
		this.mvDPWD = mvDPWD;
	}

	/**
	 * @return the mvUsable
	 */
	public String getMvUsable() {
		return mvUsable;
	}

	/**
	 * @param mvUsable
	 *            the mvUsable to set
	 */
	public void setMvUsable(String mvUsable) {
		this.mvUsable = mvUsable;
	}

	/**
	 * @return the mvAvailAdvanceMoney
	 */
	public String getMvAvailAdvanceMoney() {
		return mvAvailAdvanceMoney;
	}

	/**
	 * @param mvAvailAdvanceMoney
	 *            the mvAvailAdvanceMoney to set
	 */
	public void setMvAvailAdvanceMoney(String mvAvailAdvanceMoney) {
		this.mvAvailAdvanceMoney = mvAvailAdvanceMoney;
	}

	/**
	 * @return the mvOutstandingLoan
	 */
	public String getMvOutstandingLoan() {
		return mvOutstandingLoan;
	}

	/**
	 * @param mvOutstandingLoan
	 *            the mvOutstandingLoan to set
	 */
	public void setMvOutstandingLoan(String mvOutstandingLoan) {
		this.mvOutstandingLoan = mvOutstandingLoan;
	}

	/**
	 * @return the mvPenddingWithdrawMoney
	 */
	public String getMvPenddingWithdrawMoney() {
		return mvPenddingWithdrawMoney;
	}

	/**
	 * @param mvPenddingWithdrawMoney
	 *            the mvPenddingWithdrawMoney to set
	 */
	public void setMvPenddingWithdrawMoney(String mvPenddingWithdrawMoney) {
		this.mvPenddingWithdrawMoney = mvPenddingWithdrawMoney;
	}

	/**
	 * @return the mvTotalOutAdvance
	 */
	public String getMvTotalOutAdvance() {
		return mvTotalOutAdvance;
	}

	/**
	 * @param mvTotalOutAdvance
	 *            the mvTotalOutAdvance to set
	 */
	public void setMvTotalOutAdvance(String mvTotalOutAdvance) {
		this.mvTotalOutAdvance = mvTotalOutAdvance;
	}

	/**
	 * @return the mvBuyingPowerd
	 */
	public String getMvBuyingPowerd() {
		return mvBuyingPowerd;
	}

	/**
	 * @param mvBuyingPowerd the mvBuyingPowerd to set
	 */
	public void setMvBuyingPowerd(String mvBuyingPowerd) {
		this.mvBuyingPowerd = mvBuyingPowerd;
	}

	public String getMvCCashWithdrawable() {
		return mvCCashWithdrawable;
	}

	public void setMvCCashWithdrawable(String mvCCashWithdrawable) {
		this.mvCCashWithdrawable = mvCCashWithdrawable;
	}

	public String getBuyAmt() {
		return buyAmt;
	}

	public void setBuyAmt(String buyAmt) {
		this.buyAmt = buyAmt;
	}

	public String getTotalAsset() {
		return totalAsset;
	}

	public void setTotalAsset(String totalAsset) {
		this.totalAsset = totalAsset;
	}

	public String getEquity() {
		return equity;
	}

	public void setEquity(String equity) {
		this.equity = equity;
	}

	public String getStockMaintenance() {
		return stockMaintenance;
	}

	public void setStockMaintenance(String stockMaintenance) {
		this.stockMaintenance = stockMaintenance;
	}

	public String getCashMaintenance() {
		return cashMaintenance;
	}

	public void setCashMaintenance(String cashMaintenance) {
		this.cashMaintenance = cashMaintenance;
	}

	public String getUnrealizedLoan() {
		return unrealizedLoan;
	}

	public void setUnrealizedLoan(String unrealizedLoan) {
		this.unrealizedLoan = unrealizedLoan;
	}

}
