package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * This class processes the stock Price Alert Subscription
 * @author not attributable
 *
 */
public class HKSPriceAlertSubscriptionTxn extends BaseTxn
{
	/**
	 *  Constructor for HKSPriceAlertSubscriptionTxn class
	 * @param pClientID the client id
	 * @param pMarketID the market id
	 * @param pInstrumentID the instrument id
	 * @param pAlertSign the alert sign
	 * @param pAlertPrice the alert stock price
	 */
   public void process(String pClientID, String pMarketID, String pInstrumentID, String pAlertSign, String pAlertPrice)
   {
      Hashtable lvTxnMap = new Hashtable();
      lvTxnMap.put(TagName.CLIENTID, pClientID);
      lvTxnMap.put(TagName.MARKETID, pMarketID);
      lvTxnMap.put(TagName.INSTRUMENTID, pInstrumentID);
      lvTxnMap.put("ALERTSIGN", pAlertSign);
      lvTxnMap.put("ALERTPRICE", pAlertPrice);

      if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSPriceAlertSubscriptionRequest, lvTxnMap))
      {
      }
   }
}
