package com.ttl.old.itrade.hks.bean;

/**
 * 
 * @author duonghuynh
 * 
 */
public class HKSOverdueDebtBean {
	private String overdueDebt;
	private String processedDebt;
	private String upcomingDueDebt;
	private String forceSell;
	private String advanceRequest;
	private String cashSupplement;
	private String sellStockRequest;
	private String loanDuration;
	private String remindDays;
	private String forceSellDays;
	private String cashReserve;

	/**
	 * @return the overdueDebt
	 */
	public String getOverdueDebt() {
		return overdueDebt;
	}

	/**
	 * @param overdueDebt the overdueDebt to set
	 */
	public void setOverdueDebt(String overdueDebt) {
		this.overdueDebt = overdueDebt;
	}

	/**
	 * @return the processedDebt
	 */
	public String getProcessedDebt() {
		return processedDebt;
	}

	/**
	 * @param processedDebt the processedDebt to set
	 */
	public void setProcessedDebt(String processedDebt) {
		this.processedDebt = processedDebt;
	}

	/**
	 * @return the upcomingDueDebt
	 */
	public String getUpcomingDueDebt() {
		return upcomingDueDebt;
	}

	/**
	 * @param upcomingDueDebt the upcomingDueDebt to set
	 */
	public void setUpcomingDueDebt(String upcomingDueDebt) {
		this.upcomingDueDebt = upcomingDueDebt;
	}

	/**
	 * @return the forceSell
	 */
	public String getForceSell() {
		return forceSell;
	}

	/**
	 * @param forceSell the forceSell to set
	 */
	public void setForceSell(String forceSell) {
		this.forceSell = forceSell;
	}

	/**
	 * @return the advanceRequest
	 */
	public String getAdvanceRequest() {
		return advanceRequest;
	}

	/**
	 * @param advanceRequest the advanceRequest to set
	 */
	public void setAdvanceRequest(String advanceRequest) {
		this.advanceRequest = advanceRequest;
	}

	/**
	 * @return the cashSupplement
	 */
	public String getCashSupplement() {
		return cashSupplement;
	}

	/**
	 * @param cashSupplement the cashSupplement to set
	 */
	public void setCashSupplement(String cashSupplement) {
		this.cashSupplement = cashSupplement;
	}

	/**
	 * @return the sellStockRequest
	 */
	public String getSellStockRequest() {
		return sellStockRequest;
	}

	/**
	 * @param sellStockRequest the sellStockRequest to set
	 */
	public void setSellStockRequest(String sellStockRequest) {
		this.sellStockRequest = sellStockRequest;
	}

	/**
	 * @return the loanDuration
	 */
	public String getLoanDuration() {
		return loanDuration;
	}

	/**
	 * @param loanDuration the loanDuration to set
	 */
	public void setLoanDuration(String loanDuration) {
		this.loanDuration = loanDuration;
	}

	/**
	 * @return the remindDays
	 */
	public String getRemindDays() {
		return remindDays;
	}

	/**
	 * @param remindDays the remindDays to set
	 */
	public void setRemindDays(String remindDays) {
		this.remindDays = remindDays;
	}

	/**
	 * @return the forceSellDays
	 */
	public String getForceSellDays() {
		return forceSellDays;
	}

	/**
	 * @param forceSellDays the forceSellDays to set
	 */
	public void setForceSellDays(String forceSellDays) {
		this.forceSellDays = forceSellDays;
	}

	/**
	 * @return
	 */
	public String getCashReserve() {
		return cashReserve;
	}

	/**
	 * @param cashReserve
	 */
	public void setCashReserve(String cashReserve) {
		this.cashReserve = cashReserve;
	}

}
