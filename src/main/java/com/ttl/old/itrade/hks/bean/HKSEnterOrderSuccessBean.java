package com.ttl.old.itrade.hks.bean;

/**
 * The HKSEnterOrderSuccessBean class define variables that to save values 
 * for action 
 * @author Wind zhao
 *
 */
public class HKSEnterOrderSuccessBean {
	private String mvReasonMessage;
	private String mvBS;
	private String mvOrderId;
	private String mvBuyOrSell;
	private String mvInstrumentId;
	private String mvMarketId;
	private String mvStopTypeValue;
	private String mvStopPriceValue;
	private String mvClientId;
	private String mvDateTime;
	private String mvInstrumentName;
	private String mvCurrencyId;
	private String mvPrice;
	private String mvQuantity;
	private String mvUnitPriceCurrencyId;
	private String mvFormAtted_StopPriceValue;
	private String mvOrderType;
	private String mvGoodTillDate;
	private String mvNetAmt;
	private String mvQuantityDescription;
	private String mvOrderTypeDESCRIPTION;
	private String mvGoodTillDescription;
	private String mvTransType;
	
	/**
     * This method returns the type of transaction.
     * @return the type of transaction.
     */
	public String getMvTransType() {
		return mvTransType;
	}
	
	/**
     * This method sets the type of transaction.
     * @param pTransType The type of transaction.
     */
	public void setMvTransType(String pTransType) {
		mvTransType = pTransType;
	}
	
	/**
     * This method returns the quantity of order.
     * @return the quantity of order is formatted.
     */
	public String getMvQuantity() {
		return mvQuantity;
	}
	
	/**
     * This method sets the quantity of order.
     * @param pQuantity The quantity of order is formatted.
     */
	public void setMvQuantity(String pQuantity) {
		mvQuantity = pQuantity;
	}
	
	/**
     * This method returns the waring message of order.
     * @return the waring message of order.
     */
	public String getMvReasonMessage() {
		return mvReasonMessage;
	}
	
	/**
     * This method sets the waring message of order.
     * @param pReasonMessage The waring message of order.
     */
	public void setMvReasonMessage(String pReasonMessage) {
		mvReasonMessage = pReasonMessage;
	}
	
	/**
     * This method returns the B(Buy) or S(Sell) of order.
     * @return the B(Buy) or S(Sell)  of order.
     *         [0] B is Buy.
     *         [1] S is Sell.
     */
	public String getMvBS() {
		return mvBS;
	}
	
	/**
     * This method sets the B(Buy) or S(Sell) of order.
     * @param pBS The B(Buy) or S(Sell) of order.
     */
	public void setMvBS(String pBS) {
		mvBS = pBS;
	}
	
	/**
     * This method returns the id of order.
     * @return the id of order is.
     */
	public String getMvOrderId() {
		return mvOrderId;
	}
	
	/**
     * This method sets the id of order.
     * @param pOrderId The id of order.
     */
	public void setMvOrderId(String pOrderId) {
		mvOrderId = pOrderId;
	}
	
	/**
     * This method returns the Buy or Sell of order.
     * @return the Buy or Sell  of order.
     *         [0] Buy.
     *         [1] Sell.
     */
	public String getMvBuyOrSell() {
		return mvBuyOrSell;
	}
	
	/**
     * This method sets the Buy or Sell of order.
     * @param pBuyOrSell The Buy or Sell of order.
     */
	public void setMvBuyOrSell(String pBuyOrSell) {
		mvBuyOrSell = pBuyOrSell;
	}
	
	/**
     * This method returns the instrument id of order.
     * @return the instrument id of order.
     */
	public String getMvInstrumentId() {
		return mvInstrumentId;
	}
	
	/**
     * This method sets the instrument id of order.
     * @param pInstrumentId The instrument id of order.
     */
	public void setMvInstrumentId(String pInstrumentId) {
		mvInstrumentId = pInstrumentId;
	}
	
	/**
     * This method returns the market id of order.
     * @return the market id of order.
     */
	public String getMvMarketId() {
		return mvMarketId;
	}
	
	/**
     * This method sets the market id of order.
     * @param pMarketId The market id of order.
     */
	public void setMvMarketId(String pMarketId) {
		mvMarketId = pMarketId;
	}
	
	/**
     * This method returns the stop type of order.
     * @return the stop type of order.
     */
	public String getMvStopTypeValue() {
		return mvStopTypeValue;
	}
	
	/**
     * This method sets the stop type of order.
     * @param pStopTypeValue The stop type of order.
     */
	public void setMvStopTypeValue(String pStopTypeValue) {
		mvStopTypeValue = pStopTypeValue;
	}
	
	/**
     * This method returns the stop price of order.
     * @return the stop price of order.
     */
	public String getMvStopPriceValue() {
		return mvStopPriceValue;
	}
	
	/**
     * This method sets the stop price of order.
     * @param pStopPriceValue The stop price of order.
     */
	public void setMvStopPriceValue(String pStopPriceValue) {
		mvStopPriceValue = pStopPriceValue;
	}
	
	/**
     * This method returns the client id.
     * @return the client id of order.
     */
	public String getMvClientId() {
		return mvClientId;
	}
	
	/**
     * This method sets the client id.
     * @param pClientId The client id of order.
     */
	public void setMvClientId(String pClientId) {
		mvClientId = pClientId;
	}
	
	/**
     * This method returns the date time of order.
     * @return the date time id of order.
     */
	public String getMvDateTime() {
		return mvDateTime;
	}
	
	/**
     * This method sets the date time of order.
     * @param pDateTime The date time of order.
     */
	public void setMvDateTime(String pDateTime) {
		mvDateTime = pDateTime;
	}
	
	/**
     * This method returns the instrument name of order.
     * @return the instrument name of order.
     */
	public String getMvInstrumentName() {
		return mvInstrumentName;
	}
	
	/**
     * This method sets the instrument name of order.
     * @param pInstrumentName The instrument name of order.
     */
	public void setMvInstrumentName(String pInstrumentName) {
		mvInstrumentName = pInstrumentName;
	}
	
	/**
     * This method returns the currency id of instrument.
     * @return currency id of instrument.
     */
	public String getMvCurrencyId() {
		return mvCurrencyId;
	}
	
	/**
     * This method sets the currency id of instrument.
     * @param pCurrencyId The currency id of instrument.
     */
	public void setMvCurrencyId(String pCurrencyId) {
		mvCurrencyId = pCurrencyId;
	}
	
	/**
     * This method returns the price of order.
     * @return the price of order is formatted.
     */
	public String getMvPrice() {
		return mvPrice;
	}
	
	/**
     * This method sets the price of order.
     * @param pPrice The price of order is formatted.
     */
	public void setMvPrice(String pPrice) {
		mvPrice = pPrice;
	}
	
	/**
     * This method returns the unit price of order.
     * @return the unit price of order with currency id.
     */
	public String getMvUnitPriceCurrencyId() {
		return mvUnitPriceCurrencyId;
	}
	
	/**
     * This method sets the unit price of order.
     * @param pUnitPriceCurrencyId The unit price of order with currency id.
     */
	public void setMvUnitPriceCurrencyId(String pUnitPriceCurrencyId) {
		mvUnitPriceCurrencyId = pUnitPriceCurrencyId;
	}
	
	/**
     * This method returns the stop price of order.
     * @return the stop price of order is formatted with currency id.
     */
	public String getMvFormAtted_StopPriceValue() {
		return mvFormAtted_StopPriceValue;
	}
	
	/**
     * This method sets the stop price of order.
     * @param mvFormAtted_StopPriceValue The stop price of order is formatted with currency id.
     */
	public void setMvFormAtted_StopPriceValue(String pFormAtted_StopPriceValue) {
		mvFormAtted_StopPriceValue = pFormAtted_StopPriceValue;
	}
	
	/**
     * This method returns the type of order.
     * @return the type of order.
     */
	public String getMvOrderType() {
		return mvOrderType;
	}
	
	/**
     * This method sets the type of order.
     * @param pOrderType The type of order is formatted.
     */
	public void setMvOrderType(String pOrderType) {
		mvOrderType = pOrderType;
	}
	
	/**
     * This method returns the good till date of order.
     * @return the good till date of order is formatted.
     */
	public String getMvGoodTillDate() {
		return mvGoodTillDate;
	}
	
	/**
     * This method sets the good till date of order.
     * @param pGoodTillDate The good till date of order is formatted.
     */
	public void setMvGoodTillDate(String pGoodTillDate) {
		mvGoodTillDate = pGoodTillDate;
	}
	
	/**
     * This method returns the net amount of order.
     * @return the net amount of order is formatted.
     */
	public String getMvNetAmt() {
		return mvNetAmt;
	}
	
	/**
     * This method sets the net amount of order.
     * @param pNetAmt The net amount of order is formatted.
     */
	public void setMvNetAmt(String pNetAmt) {
		mvNetAmt = pNetAmt;
	}
	
	/**
     * This method returns the quantity description of order.
     * @return the quantity description of order.
     */
	public String getMvQuantityDescription() {
		return mvQuantityDescription;
	}
	
	/**
     * This method sets the quantity description of order.
     * @param pQuantityDescription The quantity description of order.
     */
	public void setMvQuantityDescription(String pQuantityDescription) {
		mvQuantityDescription = pQuantityDescription;
	}
	
	/**
     * This method returns the type description of order.
     * @return the type description of order.
     */
	public String getMvOrderTypeDESCRIPTION() {
		return mvOrderTypeDESCRIPTION;
	}
	
	/**
     * This method sets the type description of order.
     * @param pOrderTypeDescription The type description of order.
     */
	public void setMvOrderTypeDESCRIPTION(String pOrderTypeDescription) {
		mvOrderTypeDESCRIPTION = pOrderTypeDescription;
	}
	
	/**
     * This method returns the good till date description of order.
     * @return the good till date description of order.
     */
	public String getMvGoodTillDescription() {
		return mvGoodTillDescription;
	}
	
	/**
     * This method sets the good till date description of order.
     * @param pGoodTillDescription The good till date description of order.
     */
	public void setMvGoodTillDescription(String pGoodTillDescription) {
		mvGoodTillDescription = pGoodTillDescription;
	}
}
