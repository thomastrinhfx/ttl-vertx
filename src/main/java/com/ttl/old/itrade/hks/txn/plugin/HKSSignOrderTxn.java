package com.ttl.old.itrade.hks.txn.plugin;

import java.util.Hashtable;

import com.ttl.old.itrade.hks.bean.plugin.HKSSignOrderBean;
import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.ErrorCode;
import com.systekit.winvest.hks.config.mapping.TagName;

public class HKSSignOrderTxn extends BaseTxn {
	
	private static final long serialVersionUID = 1L;

	private String mvClientID;
	private HKSSignOrderBean[] signOrderList;
	private String mvInterfaceSeq;

	private Boolean mvSuccess = false;

	private int mvStartRecord;
	private int mvEndRecord;
	private String mvTotalRecord;
	
	public HKSSignOrderTxn(String pClientID) {
		super();
		this.setMvClientID(pClientID);
	}
	
	
	/**
	 * Function submit sign-orders list
	 * 
	 * @param signOrderListStr
	 */
//	public void submitSignOrder(String signOrderListStr, String announceId) {
	public void submitSignOrder(String signOrderListStr) {
		// Send TP request
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put(TagName.CLIENTID, mvClientID);
		//lvTxnMap.put("ANNOUNCEMENTID", announceId);
		//lvTxnMap.put(TagName.INTERFACESEQ, mvInterfaceSeq);
		lvTxnMap.put("DETAIL", signOrderListStr);

		if (TPErrorHandling.TP_NORMAL == process("HKSDoSubmitSignOrder", lvTxnMap)) {
			String result = mvReturnNode.getChildNode(TagName.RESULT).getValue();
			String returnCode = mvReturnNode.getChildNode(TagName.RETURNCODE).getValue();
			String msg = mvReturnNode.getChildNode(TagName.RETURNMESSAGE).getValue();
			if ("FAILED".equals(result) && result != null) {
				setMvSuccess(false);
				setErrorCode(new ErrorCode(new String[0], returnCode, msg));
			} else {
				setMvSuccess(true);
			}
		}
	}
	
	/**
	 * @return the mvClientID
	 */
	public String getMvClientID() {
		return mvClientID;
	}

	/**
	 * @param mvClientID the mvClientID to set
	 */
	public void setMvClientID(String mvClientID) {
		this.mvClientID = mvClientID;
	}

	/**
	 * @return the signOrderList
	 */
	public HKSSignOrderBean[] getSignOrderList() {
		return signOrderList;
	}

	/**
	 * @param signOrderList the signOrderList to set
	 */
	public void setSignOrderList(HKSSignOrderBean[] signOrderList) {
		this.signOrderList = signOrderList;
	}

	/**
	 * @return the mvInterfaceSeq
	 */
	public String getMvInterfaceSeq() {
		return mvInterfaceSeq;
	}

	/**
	 * @param mvInterfaceSeq the mvInterfaceSeq to set
	 */
	public void setMvInterfaceSeq(String mvInterfaceSeq) {
		this.mvInterfaceSeq = mvInterfaceSeq;
	}

	/**
	 * @return the mvSuccess
	 */
	public Boolean getMvSuccess() {
		return mvSuccess;
	}

	/**
	 * @param mvSuccess the mvSuccess to set
	 */
	public void setMvSuccess(Boolean mvSuccess) {
		this.mvSuccess = mvSuccess;
	}

	/**
	 * @return the mvStartRecord
	 */
	public int getMvStartRecord() {
		return mvStartRecord;
	}

	/**
	 * @param mvStartRecord the mvStartRecord to set
	 */
	public void setMvStartRecord(int mvStartRecord) {
		this.mvStartRecord = mvStartRecord;
	}

	/**
	 * @return the mvEndRecord
	 */
	public int getMvEndRecord() {
		return mvEndRecord;
	}

	/**
	 * @param mvEndRecord the mvEndRecord to set
	 */
	public void setMvEndRecord(int mvEndRecord) {
		this.mvEndRecord = mvEndRecord;
	}

	/**
	 * @return the mvTotalRecord
	 */
	public String getMvTotalRecord() {
		return mvTotalRecord;
	}

	/**
	 * @param mvTotalRecord the mvTotalRecord to set
	 */
	public void setMvTotalRecord(String mvTotalRecord) {
		this.mvTotalRecord = mvTotalRecord;
	}
}
