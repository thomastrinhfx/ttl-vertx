package com.ttl.old.itrade.hks.bean;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class HKSHistoricalChartDataBean {
	private String mvStockCode;
	private Set<String> mvMatchPrice;
	private Collection<BigDecimal> mvMatchedQty;
	private List mvHistoricalData;

	/**
	 * @param mvStockCode
	 *            the mvStockCode to set
	 */
	public void setMvStockCode(String mvStockCode) {
		this.mvStockCode = mvStockCode;
	}

	/**
	 * @return the mvStockCode
	 */
	public String getMvStockCode() {
		return mvStockCode;
	}

	
	/**
	 * @param mvHistoricalData the mvHistoricalData to set
	 */
	public void setMvHistoricalData(List mvHistoricalData) {
		this.mvHistoricalData = mvHistoricalData;
	}

	/**
	 * @return the mvHistoricalData
	 */
	public List getMvHistoricalData() {
		return mvHistoricalData;
	}

	/**
	 * @param mvMatchPrice the mvMatchPrice to set
	 */
	public void setMvMatchPrice(Set<String> mvMatchPrice) {
		this.mvMatchPrice = mvMatchPrice;
	}

	/**
	 * @return the mvMatchPrice
	 */
	public Set<String> getMvMatchPrice() {
		return mvMatchPrice;
	}

	/**
	 * @param mvMatchedQty the mvMatchedQty to set
	 */
	public void setMvMatchedQty(Collection<BigDecimal> mvMatchedQty) {
		this.mvMatchedQty = mvMatchedQty;
	}

	/**
	 * @return the mvMatchedQty
	 */
	public Collection<BigDecimal> getMvMatchedQty() {
		return mvMatchedQty;
	}

}
