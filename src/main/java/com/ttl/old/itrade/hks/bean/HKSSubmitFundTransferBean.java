package com.ttl.old.itrade.hks.bean;

/**
 * The HKSSubmitFundTransferBean class define variables that to save values 
 * for action 
 * @author
 *
 */
public class HKSSubmitFundTransferBean{
	private String mvType;
	private String mvClientId;
	private String mvErrorMsg;
	private String mvOrderId;
	private String mvAmount;
	private String mvAmountDisplay;
	private String mvAccountNumber;
	private String mvPasswordConfirmation;
	private String mvDateTime;
	
	/**
	* This method returns the date time.
	* @return the date time is not formatted.
    */
	public String getMvDateTime() {
		return mvDateTime;
	}
	
	/**
     * This method sets the date time.
     * @param pDateTime The date time.
     */
	public void setMvDateTime(String pDateTime) {
		mvDateTime = pDateTime;
	}
	
	/**
	* This method returns the password confirmation.
	* @return the password confirmation is  not formatted.
    */
	public String getMvPasswordConfirmation() {
		return mvPasswordConfirmation;
	}
	
	/**
     * This method sets the password confirmation.
     * @param pPasswordConfirmation The password confirmation.
     */
	public void setMvPasswordConfirmation(String pPasswordConfirmation) {
		mvPasswordConfirmation = pPasswordConfirmation;
	}
	
	/**
	* This method returns the account number.
	* @return the account number is not formatted.
    */
	public String getMvAccountNumber() {
		return mvAccountNumber;
	}
	
	/**
     * This method sets the account number.
     * @param pAccountNumber The account number.
     */
	public void setMvAccountNumber(String pAccountNumber) {
		mvAccountNumber = pAccountNumber;
	}
	
	/**
	* This method returns the amount display.
	* @return the amount display is not formatted.
    */
	public String getMvAmountDisplay() {
		return mvAmountDisplay;
	}
	
	/**
     * This method sets the amount display.
     * @param pAmountDisplay The amount display.
     */
	public void setMvAmountDisplay(String pAmountDisplay) {
		mvAmountDisplay = pAmountDisplay;
	}
	
	/**
	* This method returns the amount.
	* @return the amount is not formatted.
    */
	public String getMvAmount() {
		return mvAmount;
	}
	
	/**
     * This method sets the amount.
     * @param pAmount The amount.
     */
	public void setMvAmount(String pAmount) {
		mvAmount = pAmount;
	}
	
	/**
	* This method returns the order id.
	* @return the order id is not formatted.
    */
	public String getMvOrderId() {
		return mvOrderId;
	}
	
	/**
     * This method sets the order id.
     * @param pOrderId The order id.
     */
	public void setMvOrderId(String pOrderId) {
		mvOrderId = pOrderId;
	}
	
	/**
	* This method returns the error message.
	* @return the error message is not formatted.
    */
	public String getMvErrorMsg() {
		return mvErrorMsg;
	}
	
	/**
     * This method sets the error message.
     * @param pErrorMsg The error message.
     */
	public void setMvErrorMsg(String pErrorMsg) {
		mvErrorMsg = pErrorMsg;
	}
	
	/**
	* This method returns the client id.
	* @return the client id is not formatted.
    */
	public String getMvClientId() {
		return mvClientId;
	}
	
	/**
     * This method sets the client id.
     * @param pClientId The client id.
     */
	public void setMvClientId(String pClientId) {
		mvClientId = pClientId;
	}
	
	/**
	* This method returns the type.
	* @return the type is not formatted.
    */
	public String getMvType() {
		return mvType;
	}
	
	/**
     * This method sets the type.
     * @param pType The type.
     */
	public void setMvType(String pType) {
		mvType = pType;
	}
}
