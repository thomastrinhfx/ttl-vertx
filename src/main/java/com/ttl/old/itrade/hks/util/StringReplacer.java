package com.ttl.old.itrade.hks.util;

public class StringReplacer {
	public static String replace(String src, String astr, String bstr) {
		int index = 0;
		StringBuffer sb = new StringBuffer();
		  
		do {
		  index = src.indexOf(astr);
		  if(index == -1) sb.append(src);
		  else {
		    sb.append(src.substring(0,index));
		    sb.append(bstr);
		    src = src.substring(index + astr.length());
		  }
		} while(index != -1);
		  
		return sb.toString();
	}
}
