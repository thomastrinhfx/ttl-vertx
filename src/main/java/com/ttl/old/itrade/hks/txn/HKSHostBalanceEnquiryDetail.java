package com.ttl.old.itrade.hks.txn;

import java.math.BigDecimal;
/**
 * The HKSHostBalanceEnquiryDetail class definition for all method
 * HKSHostBalanceEnquiryDetail class entity
 * @author not attributable
 *
 */
public class HKSHostBalanceEnquiryDetail 
{
	private String mvClientID = null;
	private String mvSettlementAccount = null;
	private BigDecimal mvAvailableBalance = null;
	private BigDecimal mvCurrentBalance = null;
	private BigDecimal mvSystemHoldAmount = null;
	private BigDecimal mvHostHoldAmount = null;
	
	/**
	 * Constructor of the host balance detail
	 * @param pClientID the client id 
	 * @param pSettlementAccount the settlement bank account
	 * @param pAvailableBalance the available balance
	 * @param pCurrentBalance the current balance
	 * @param pSystemHoldAmount the system hold amount
	 * @param pHostHoldAmount the host hole amount
	 */
	public HKSHostBalanceEnquiryDetail(String pClientID, String pSettlementAccount, BigDecimal pAvailableBalance, BigDecimal pCurrentBalance, BigDecimal pSystemHoldAmount, BigDecimal pHostHoldAmount)
	{
		mvClientID = pClientID;
		mvSettlementAccount = pSettlementAccount;
		mvAvailableBalance = pAvailableBalance;
		mvCurrentBalance = pCurrentBalance;
		mvSystemHoldAmount = pSystemHoldAmount;
		mvHostHoldAmount = pHostHoldAmount;
	}
	
	/**
	 * Get Client ID
	 * @return Client ID
	 */
	public String getClientID()
	{
		return mvClientID;
	}
	
	/**
	 * Get Settlement Account
	 * @return Settlement Account
	 */
	public String getSettlementAccount()
	{
		return mvSettlementAccount;
	}
	
	/**
	 * Get Available Balance
	 * @return Available Balance
	 */
	public BigDecimal getAvailableBalance()
	{
		return mvAvailableBalance;
	}

	/**
	 * Get Current Balance
	 * @return Current Balance
	 */
	public BigDecimal getCurrentBalance()
	{
		return mvCurrentBalance;
	}

	/**
	 * Get System Hold Amount
	 * @return System Hold Amount
	 */
	public BigDecimal getSystemHoldAmount()
	{
		return mvSystemHoldAmount;
	}

	/**
	 * Get Host Hold Amount
	 * @return Host Hold Amount
	 */
	public BigDecimal getHostHoldAmount()
	{
		return mvHostHoldAmount;
	}

}
