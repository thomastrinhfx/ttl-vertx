package com.ttl.old.itrade.hks.bean.plugin;


/**
 * @author Khanh Nguyen
 *
 */

public class HKSAgentBean {	
	
	private String agentPhone;
	private String agentName;
	private String agentIDNumber;
	private String agentAttorney;
	
	
	public void setAgentPhone(String agentPhone) {
		this.agentPhone = agentPhone;
	}
	public String getAgentPhone() {
		return agentPhone;
	}
	/**
	 * @param agentName the agentName to set
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	/**
	 * @return the agentName
	 */
	public String getAgentName() {
		return agentName;
	}
	/**
	 * @param agentIDNumber the agentIDNumber to set
	 */
	public void setAgentIDNumber(String agentIDNumber) {
		this.agentIDNumber = agentIDNumber;
	}
	/**
	 * @return the agentIDNumber
	 */
	public String getAgentIDNumber() {
		return agentIDNumber;
	}
	/**
	 * @param agentAttorney the agentAttorney to set
	 */
	public void setAgentAttorney(String agentAttorney) {
		this.agentAttorney = agentAttorney;
	}
	/**
	 * @return the agentAttorney
	 */
	public String getAgentAttorney() {
		return agentAttorney;
	}	

}
