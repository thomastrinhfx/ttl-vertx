package com.ttl.old.itrade.hks;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.GenericRequest;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.AppletGateway;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ttl.old.itrade.comet.eventHandler.caching.MarketIndexCachingManager;
//import com.itrade.comet.eventHandler.producer.StockWatchReader;
//import com.itrade.hks.txn.HKSDownloadInstrumentTxn;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.winvest.hks.config.mapping.TagName;

public class HKSClearPrevDateHistoricalDataTask implements Job {

	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";
	private static final String CONFIG_CLEAR_TIME = "timeToClearPrevData";

	
	/**
	 * 
	 */
	private void clearPrevDateHistoricalData() {
		// clear stock history
		IMain.stockHistMap.clear();
		AppletGateway.ivPendingMarketData.clear();
		// clear stock info
		deleteFilePrevDateHistoryData();
		//StockWatchReader.stockInfoCached.clear();
		//StockWatchReader.sysStockInfoCached.clear();
		IMain.loadStockHistData();
		// clear market index
		IMain.mvMarketIndex.clear();
		MarketIndexCachingManager.clearMarketIndex();
		IMain.mvBestPrice.clear();
		IMain.mvMarketHistoricalIndex.clear();
		// IMain.mvMarketStatus.clear();
		IMain.mvIsRunClearPreHistoricalData=false;
		 try {
			//HKSDownloadInstrumentTxn instrumentDownloadTxn = new HKSDownloadInstrumentTxn(
					//"HKEX");
			//instrumentDownloadTxn.process();
		} catch (Exception e) {
			Log.println(e, Log.ERROR_LOG);
		}
	}
	
	private void deleteFilePrevDateHistoryData()
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	    Calendar c1 = Calendar.getInstance();
	    String filePrefix = sdf.format(c1.getTime());
	    File f = new File(IMain.getProperty("HistDataFileDir")+filePrefix+".txt");	  
	    
	    try
	    {
	    	if (f.exists()) {
				if (!f.canWrite())	    	    	
					  throw new IllegalArgumentException("DeleteFilePrevDateHistoryData: write protected: without permission " );
					
				boolean success = f.delete();
				
				if (!success)
				  throw new IllegalArgumentException("Delete: deletion failed");	
				else
				{
					f.createNewFile();
				}	
		    }	  	    	
	    }
	    catch (Exception e) {
			// TODO: handle exception
	    	System.err.println("Error: " + e.getMessage());
			Log.println( " [DeleteFilePrevDateHistoryData] " + e, Log.ERROR_LOG);
		}      
	}

	/**
	 * Check the current time with setting time If greater than then set to next
	 * date time Else set to current date time
	 * 
	 * @return
	 */
	public Date calculateTimeForSchedule() {

		// Setting Default value
		int fFOUR_AM = 7;
		int fZERO_MINUTES = 30;
		Calendar scheduleTime = new GregorianCalendar();
		Calendar result = null;
		try {
			String timeInConfig = IMain.getProperty(CONFIG_CLEAR_TIME);

			try {
				if (timeInConfig != null && !timeInConfig.trim().isEmpty()) {
					String timeArray[] = timeInConfig.split(":");
					fFOUR_AM = Integer.valueOf(timeArray[0]);
					fZERO_MINUTES = Integer.valueOf(timeArray[1]);
				} else {
					Log.println("Configuration invalid. Using default value",
							Log.ACCESS_LOG);
				}
			} catch (Exception e) {
				Log.println("Exception when paser Configuration setting:" + e,
						Log.ERROR_LOG);
			}

			Calendar cal = Calendar.getInstance();
			if ((cal.get(Calendar.HOUR) > fFOUR_AM)
					|| ((cal.get(Calendar.HOUR) == fFOUR_AM) && cal
							.get(Calendar.MINUTE) > fZERO_MINUTES)) {
				scheduleTime.add(Calendar.DATE, 1);
			}

			result = new GregorianCalendar(scheduleTime.get(Calendar.YEAR),
					scheduleTime.get(Calendar.MONTH), scheduleTime
							.get(Calendar.DATE), fFOUR_AM, fZERO_MINUTES);

		} catch (Exception e) {
			Log.println("Exception in calculateTimeForSchedule:" + e,
					Log.ERROR_LOG);
		}
		Log.println("Schedule clear data job in:" + result.getTime(),
				Log.ACCESS_LOG);
		return result.getTime();
	}

	/**
	 * 
	 * @return
	 */
	private boolean isDayStartOperationCompleted() {
		boolean IsStart = false;

		GenericRequest lvGRY = new GenericRequest(ITradeServlet.getTpManager(), null);
		
		IMsgXMLNode lvNodeY = lvGRY.buildQRYPOPUPXMLHeader();
		lvNodeY.addChildNode(TagName.FuncName);
		lvNodeY.addChildNode(TagName.ObjectName).setValue("MCSYS");
		lvNodeY.addChildNode(TagName.LoopSelect);
		lvNodeY.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "STATUS");
		lvNodeY.getChildNode(TagName.LoopSelect).addChildNode(TagName.Field)
				.setAttribute(TagName.Name, "TRADEDATE");
		lvNodeY.addChildNode(TagName.LoopWhere);
		lvNodeY.addChildNode(TagName.LoopOrderBy);

		IMsgXMLNode lvreturnNodeY = lvGRY.sendRequestXML(lvNodeY);
		if (lvreturnNodeY == null) {
			Log.println("TP XML Response : is null", Log.ERROR_LOG);
		} else if (lvreturnNodeY.getChildNode("C_ERROR_CODE") != null) {
			Log.println(lvreturnNodeY.getChildNode("C_ERROR_CODE").getValue(),
					Log.ERROR_LOG);
		} else {
			String lvStatus = lvreturnNodeY.getChildNode(TagName.LoopRowValue)
					.getChildNode(TagName.RowValue).getAttribute("v0");
			String lvTradeDate = lvreturnNodeY.getChildNode(
					TagName.LoopRowValue).getChildNode(TagName.RowValue)
					.getAttribute("v1");
			Log.println("Trade date :" + lvTradeDate
					+ " and DAYENDSTARTSTATUS: " + lvStatus, Log.DEBUG_LOG);
			IsStart = lvStatus.trim().equalsIgnoreCase("S");
		}
		return IsStart;
	}

	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		Log
				.println("Start clear previous date historical data",
						Log.ACCESS_LOG);
		// 1) Check if the current date is trade date. Yes continue else skip
		// operation
		// Current trade date will be updated when FO do Order Expiration
		// If current trade date > Itrade Date(may be current day is weekends)
		// then skip operation
		Log.println("Curent trade date in map :" + HKSMain.svCurrentTradeDate,
				Log.ACCESS_LOG);
		
		clearPrevDateHistoricalData();
		
		/*
		Date currentTradeDateMap = (Date) HKSMain.svCurrentTradeDate.clone();

		Calendar currentTime = new GregorianCalendar();
		Calendar result = new GregorianCalendar(currentTime.get(Calendar.YEAR),
				currentTime.get(Calendar.MONTH), currentTime.get(Calendar.DATE));

		if (result.getTime().compareTo(currentTradeDateMap) == 0) {
			Thread lvThread = new Thread(new Runnable() {
				boolean isStartedDayOperationFlag = false;

				@Override
				public void run() {
					Log.println(
							"Start Thread checking do DAYSTART operation yet",
							Log.DEBUG_LOG);
					// 1.1) Check the system already do DAYSTAT operation yet
					checkDayStartOpearation();
					while (!isStartedDayOperationFlag) {
						// 1.1.2) If NOT ----> retry after 1 minutes to waiting
						// system do DAYSTART
						Log.println("Set Thread Waiting 1 minutes",
								Log.DEBUG_LOG);
						try {
							Thread.sleep(60 * 1000);
							checkDayStartOpearation();
						} catch (InterruptedException e) {
							Log.println(e, Log.ERROR_LOG);
						}
					}

					if (isStartedDayOperationFlag) {
						// 1.1.1) If YES ---> Clear historical data
						Log.println("Start clear Historical Data",
								Log.DEBUG_LOG);
						clearPrevDateHistoricalData();
					}
				}

				public void checkDayStartOpearation() {
					isStartedDayOperationFlag = isDayStartOperationCompleted();
				}

			});
			lvThread.start();
		}*/
		Log.println("End clear previous date historical data", Log.ACCESS_LOG);
	}
}
