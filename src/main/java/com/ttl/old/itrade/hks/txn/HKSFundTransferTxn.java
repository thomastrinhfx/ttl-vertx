package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;

import javax.servlet.http.HttpSession;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.winvest.hks.util.Utils;


/**
 * The HKSFundTransferTxn class definition for all method
 * client bank account Fund Transfer
 * 
 * @author Tree Lam
 * @since 20080625
 */
public class HKSFundTransferTxn
{
    private String mvClientID;
    private String mvDirection;
    private String mvAmount;
    private String mvPassword;
    private boolean mvConfirmationWithPass;
    private int mvReturnCode;
    private String mvReturnMessage;
    private String mvErrorCode;
    private String mvErrorMessage;
    private String mvIsSuccess;
    private String mvAccountID;
    private String mvCINO = new String();
    private String mvSSOUserID = new String();
    private String mvExternalPasswordVerificationEnable = new String();
    private String mvTranID;
    private String mvTranDate;
    
    //Begin Task #RC00181 - Rice Cheng 20090108
    private String mvLanguage;
    //End Task #RC00181 - Rice Cheng 20090108
    
    // BEGIN RN00053 Ricky Ngan 20081127
    private String mvReturnErrorCode;
    // END RN00053
    
    //Begin Task #RC00152 - Rice Cheng 20081212
    private String mvReplyCode="";
    //End Task #RC00152 - Rice Cheng 20081212
    /**
     * This variable is used to the tp Error
     */
    TPErrorHandling tpError;

    /**
     * Constructor for HKSFundTransferTxn class
     * @param pClientID the client id
     * @param pDirection the direction
     * @param pAccountID the client bank accoount id
     * @param pAmount the amount
     * @param pPassword the client bank account password
     * @param pVerifyPassword the client bank account Verify password
     */
    public HKSFundTransferTxn(String pClientID, String pDirection, String pAccountID, String pAmount, String pPassword, String pVerifyPassword)
    {
    	//Begin Task #RC00181 - Rice Cheng 20090108
//        mvClientID = pClientID;
//        mvDirection = pDirection;
//        mvAccountID = pAccountID;
//        mvAmount = pAmount;
//        mvPassword = pPassword;
//        mvConfirmationWithPass = pVerifyPassword.equals("Y");
//        tpError = new TPErrorHandling();
    	this(pClientID, pDirection, pAccountID, pAmount, pPassword, pVerifyPassword, "", "");
        //End Task #RC00181 - Rice Cheng 20090108
    }

    /**
     * Constructor for HKSFundTransferTxn class
     * @param pClientID the client id
     * @param pDirection the direction
     * @param pAccountID the client bank accoount id
     * @param pAmount the amount
     * @param pPassword the client bank account password
     * @param pVerifyPassword the client bank account Verify password
     * @param pExternalPasswordVerificationEnable the External client bank account password Verification Enable
     */
    public HKSFundTransferTxn(String pClientID, String pDirection, String pAccountID, String pAmount, String pPassword, String pVerifyPassword, String pExternalPasswordVerificationEnable)
    {
    	//Begin Task #RC00181 - Rice Cheng 20090108
//        mvClientID = pClientID;
//        mvDirection = pDirection;
//        mvAccountID = pAccountID;
//        mvAmount = pAmount;
//        mvPassword = pPassword;
//        mvExternalPasswordVerificationEnable = pExternalPasswordVerificationEnable;
//        mvConfirmationWithPass = pVerifyPassword.equals("Y");
//        tpError = new TPErrorHandling();
    	this(pClientID, pDirection, pAccountID, pAmount, pPassword, pVerifyPassword, pExternalPasswordVerificationEnable, "");
        //End Task #RC00181 - Rice Cheng 20090108
    }

    //Begin Task #RC00181 - Rice Cheng 20090108
    public HKSFundTransferTxn(String pClientID, String pDirection, String pAccountID, String pAmount, String pPassword, String pVerifyPassword, String pExternalPasswordVerificationEnable, String pLanguage)
    {
        mvClientID = pClientID;
        mvDirection = pDirection;
        mvAccountID = pAccountID;
        mvAmount = pAmount;
        mvPassword = pPassword;
        mvExternalPasswordVerificationEnable = pExternalPasswordVerificationEnable;
        mvConfirmationWithPass = pVerifyPassword.equals("Y");
        tpError = new TPErrorHandling();
        
        mvLanguage = pLanguage;
    }
    //End Task #RC00181 - Rice Cheng 20090108

    /**
     * The method Process client bank account Fund Transfer
     *
     */
    public void process()
 {
     try
     {
         Hashtable lvTxnMap = new Hashtable();
         IMsgXMLNode lvRetNode;
         TPManager ivTPManager = ITradeServlet.getTPManager(RequestName.HKSFundTransferRequest);
         TPBaseRequest lvFundTransferRequest = ivTPManager.getRequest(RequestName.HKSFundTransferRequest);

         lvTxnMap.put("CLIENTID", getMvClientID());
         lvTxnMap.put("DIRECTION", getMvDirection());
         lvTxnMap.put("AMOUNT", getMvAmount());
         lvTxnMap.put("ACCOUNTID", mvAccountID);



         if(isMvConfirmationWithPass()) {

                 lvTxnMap.put("SSOUSERID", "");
                 lvTxnMap.put("VRUPASSWORDVERIFICATION", "N");

                 lvTxnMap.put("PASSWORDVERIFICATION", "Y");
                 lvTxnMap.put("PASSWORD", getMvPassword());
         }
         else {
             lvTxnMap.put("PASSWORDVERIFICATION", "N");
             lvTxnMap.put("PASSWORD", "");
         }

         //Begin Task #RC00181 - Rice Cheng 20090108
         //lvRetNode = lvFundTransferRequest.send(lvTxnMap);
         lvRetNode = lvFundTransferRequest.send(lvTxnMap, mvLanguage);
         //End Task #RC00181 - Rice Cheng 20090108
         
         mvReturnCode = tpError.checkError(lvRetNode);
         if (mvReturnCode != TPErrorHandling.TP_NORMAL)
         {
        	 if (lvRetNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase("TPBASEREQERROR"))
        	 {
        		 mvErrorCode = "0100";
        		 mvErrorMessage = "No connection with TP";
        	 }
        	 else{
	             mvErrorMessage = tpError.getErrDesc();
	             if (mvReturnCode == TPErrorHandling.TP_APP_ERR)
	             {
	                 mvErrorCode = tpError.getErrCode();
	             }
        	 }
        	// BEGIN RN00053 Ricky Ngan 20081127
             mvReturnErrorCode = lvRetNode.getChildNode("ReplyErrorCode").getValue();
             // END RN00053
             mvIsSuccess = "N";
             return;
         }
         else
         {        	 
             mvIsSuccess = "Y";
             mvReturnMessage = lvRetNode.getChildNode("ReplyMessage").getValue();
             // BEGIN RN00053 Ricky Ngan 20081127
             mvReturnErrorCode = lvRetNode.getChildNode("ReplyErrorCode").getValue();
             // END RN00053
             //Begin Task: CL00055 Charlie Lau 20081206
             mvTranDate = lvRetNode.getChildNode("CreationTime").getValue();
             //End Task: CL00055
             
             //Begin Task #RC00152 - Rice Cheng 20081212
             //For ITrade WebServices
             mvReplyCode = lvRetNode.getChildNode("ReplyCode").getValue();
    		 mvErrorCode = lvRetNode.getChildNode("ReplyErrorCode").getValue();
    		 mvErrorMessage = lvRetNode.getChildNode("ReplyInfo").getValue();
             //End Task #RC00152 - Rice Cheng 20081212
             
         }
     }
     catch(NullPointerException npe) {
         npe.printStackTrace();
         Log.println("[ HKSFundTransferTxn.process()].... NullPointerException",  Log.ERROR_LOG);
 }
     catch (Exception e)
     {
         e.printStackTrace();
     }
 }


    /**
     * The method Process client bank account Fund Transfer
     * @param pSession the Session
     */
    public void process(HttpSession pSession)
    {
        try
        {
            HttpSession lvSession = pSession;
            Hashtable lvTxnMap = new Hashtable();
            IMsgXMLNode lvRetNode;
            TPManager ivTPManager = ITradeServlet.getTPManager(RequestName.HKSFundTransferRequest);
            TPBaseRequest lvFundTransferRequest = ivTPManager.getRequest(RequestName.HKSFundTransferRequest);

            lvTxnMap.put("CLIENTID", getMvClientID());
            lvTxnMap.put("DIRECTION", getMvDirection());
            lvTxnMap.put("AMOUNT", getMvAmount());
            lvTxnMap.put("ACCOUNTID", mvAccountID);

            if (isMvConfirmationWithPass())
            {
                   if(lvSession != null) {
                   try {
                           if(!Utils.isNullStr(lvSession.getAttribute("CINO"))) {
                                   mvCINO = (String) lvSession.getAttribute("CINO");
                                   mvSSOUserID = (String) lvSession.getAttribute("LOGINID");
                                   if(!Utils.isNullStr(mvCINO) && getExternalPasswordVerificationEnable().equals("Y")) {
                                           lvTxnMap.put("SSOUSERID", mvSSOUserID);
                                           lvTxnMap.put("VRUPASSWORDVERIFICATION", "Y");
                                   }
                                   else {
                                           lvTxnMap.put("SSOUSERID", "");
                                           lvTxnMap.put("VRUPASSWORDVERIFICATION", "N");
                                   }
                           }
                   } catch(NullPointerException npe) {
                           npe.printStackTrace();
                           Log.println("[ HKSModifyOrderTxn.process() CINO, SSOUSER Null Pointer ]", Log.ERROR_LOG);
                           lvTxnMap.put("SSOUSERID", "");
                           lvTxnMap.put("VRUPASSWORDVERIFICATION", "N");

                   } catch(Exception ex) {
                           ex.printStackTrace();
                           Log.println("[ HKSModifyOrderTxn.process() CINO, SSOUSER Null Pointer ]", Log.ERROR_LOG);
                           lvTxnMap.put("SSOUSERID", "");
                           lvTxnMap.put("VRUPASSWORDVERIFICATION", "N");
                   }


           }




                lvTxnMap.put("PASSWORDVERIFICATION", "Y");
                lvTxnMap.put("PASSWORD", getMvPassword());
            } else {
                lvTxnMap.put("PASSWORDVERIFICATION", "N");
                lvTxnMap.put("PASSWORD", "");
            }

            lvRetNode = lvFundTransferRequest.send(lvTxnMap);
            mvReturnCode = tpError.checkError(lvRetNode);
            if (mvReturnCode != TPErrorHandling.TP_NORMAL)
            {
                mvErrorMessage = tpError.getErrDesc();
                if (mvReturnCode == TPErrorHandling.TP_APP_ERR)
                {
                    mvErrorCode = tpError.getErrCode();
                }
                mvIsSuccess = "N";
                return;
            }
            else
            {
                mvIsSuccess = "Y";
                mvReturnMessage = lvRetNode.getChildNode("ReplyMessage").getValue();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    //Begin Task #RC00152 - Rice Cheng 20081212
    /**
     * Get Reply Code
     * @return Reply Code
     */
    public String getMvReplyCode()
    {
        return mvReplyCode;
    }
    //End Task #RC00152 - Rice Cheng 20081212
    
    /**
     * Get Error Message
     * @return Error Message
     */
    public String getMvErrorMessage()
    {
        return mvErrorMessage;
    }

    /**
     * Get Error Code
     * @return Error Code
     */
    public String getMvErrorCode()
    {
        return mvErrorCode;
    }

    /**
     * Get Return Message
     * @return Return Message
     */
    public String getMvReturnMessage()
    {
        return mvReturnMessage;
    }

    /**
     * Get Client ID
     * @return Client ID
     */
    public String getMvClientID()
    {
        return mvClientID;
    }

    /**
     * Get Is Confirm with password
     * @return the Is Confirm with password
     */
    public boolean isMvConfirmationWithPass()
    {
        return mvConfirmationWithPass;
    }

    /**
     * Get Is Success
     * @return Is Success
     */
    public String getMvIsSuccess()
    {
        return mvIsSuccess;
    }

    /**
     * Get Direction
     * @return D or W
     */
    public String getMvDirection()
    {
        return mvDirection;
    }

    /**
     * Get Amount
     * @return Amount
     */
    public String getMvAmount()
    {
        return mvAmount;
    }

    /**
     * Get Password
     * @return Password
     */
    public String getMvPassword()
    {
        return mvPassword;
    }

    /**
     * Get method for External client bank account Password Verification Enable
     * @return the External client bank account Password Verification Enable
     */
    public String getExternalPasswordVerificationEnable()
    {
            return mvExternalPasswordVerificationEnable;
    }
    /**
     * Set method for External client bank account Password Verification Enable
     * @param pExternalPasswordVerificationEnable the External client bank account Password Verification Enable
     */
    public void setExternalPasswordVerificationEnable(String pExternalPasswordVerificationEnable)
    {
            mvExternalPasswordVerificationEnable = pExternalPasswordVerificationEnable;
    }
    
    //BEGIN TASK#: CL00029 Charlie liu 20081021
    /**
     * Get Account ID
     * @return Account ID
     */
    public String getMvAccountID()
    {
        return mvAccountID;
    }
    //END TASK#: CL00029
    
   /**
    * Get method for System Return Code
    * @return System Return Code
    */
    public int getReturnCode()
    {
        return mvReturnCode;
    }

   /**
    * Set method for System Return Code
    * @param pReturnCode the System Return Code
    */
    public void setReturnCode(int pReturnCode)
    {
        mvReturnCode = pReturnCode;
    }

    // BEGIN RN00053 Ricky Ngan 20081127
    /**
     * Get the return System error code
     * @return System error code
     */
	public String getReturnErrorCode() {
		return mvReturnErrorCode;
	}
	/**
	 * Set the return System error code
	 * @param pReturnErrorCode the retrun System error code
	 */
	public void setReturnErrorCode(String pReturnErrorCode) {
		mvReturnErrorCode = pReturnErrorCode;
	}
	// END RN00053
	
    /**
     * Get Tran Date
     * @return Tran Date
     */
    public String getTranDate()
    {
        return mvTranDate;
    }
}
