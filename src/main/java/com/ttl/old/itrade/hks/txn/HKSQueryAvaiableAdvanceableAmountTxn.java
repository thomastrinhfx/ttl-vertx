package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;

import com.ttl.old.itrade.hks.HKSTag;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.ErrorCode;
import com.systekit.winvest.hks.config.mapping.HKSConstant;

public class HKSQueryAvaiableAdvanceableAmountTxn extends BaseTxn {
	private String mvClientId;
	private String mvAdvanceableAmount;
	
	public void setMvClientId(String mvClientId) {
		this.mvClientId = mvClientId;
	}

	public String getMvClientId() {
		return mvClientId;
	}
	
	public HKSQueryAvaiableAdvanceableAmountTxn(String pClientID){
		mvClientId = pClientID;
	}
	
	@SuppressWarnings("unchecked")
	public void process() {
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put(HKSTag.CLIENTID, mvClientId);
		lvTxnMap.put(HKSTag.PRODUCTID, HKSConstant.ProductID);
		
		if (TPErrorHandling.TP_NORMAL == process("HKSQueryAvaiableAdvanceableAmountRequest", lvTxnMap)) {
	        setMvAdvanceableAmount(mvReturnNode.getChildNode("AVAILABLEADVANCEMONEY").getValue());
		}else { 
			 // Unhandled Business Exception
	         setErrorCode(new ErrorCode(new String[0], mvReturnNode.getChildNode("C_ERROR_CODE").getValue(), mvReturnNode.getChildNode("C_ERROR_DESC").getValue()));

	         // Handle special cases
	         // 1. Agreement is not signed
	         if (mvReturnNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase("HKSFOE00026")) {
	        	 setErrorCode(new ErrorCode(new String[0], mvReturnNode.getChildNode("C_ERROR_CODE").getValue(), mvReturnNode.getChildNode("C_ERROR_DESC").getValue()));
	         }

	         // 2. When TP is down
	         if (mvReturnNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase("TPBASEREQERROR")) {
	        	 setErrorCode(new ErrorCode(new String[0], "0100", "No connection with TP"));
	         }
	      }
	}

	public void setMvAdvanceableAmount(String mvAdvanceableAmount) {
		this.mvAdvanceableAmount = mvAdvanceableAmount;
	}

	public String getMvAdvanceableAmount() {
		return mvAdvanceableAmount;
	}
	
}
