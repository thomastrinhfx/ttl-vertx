package com.ttl.wtrade.defmess;

public class DefMessTag {
	
	public String serverTag;
	public String clientTag;
	public String defaultVal;
	public String getServerTag() {
		return serverTag;
	}
	public void setServerTag(String serverTag) {
		this.serverTag = serverTag;
	}
	public String getClientTag() {
		return clientTag;
	}
	public void setClientTag(String clientTag) {
		this.clientTag = clientTag;
	}
	public String getDefaultVal() {
		return defaultVal;
	}
	public void setDefaultVal(String defaultVal) {
		this.defaultVal = defaultVal;
	}
	public DefMessTag(String serverTag, String clientTag, String defaultVal) {
		super();
		this.serverTag = serverTag;
		this.clientTag = clientTag;
		this.defaultVal = defaultVal;
	}
	public DefMessTag(String serverTag, String clientTag) {
		this(serverTag, clientTag, "");
	}
	
	

}
