package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessContactUs extends DefaultDefMess {
	
	public DefMessContactUs(){
		super("HKSWB020Q01", "20021209053839", "HK", "01", "1", "", "0000000000000000000000001", "en", "US");
		tags = new DefMessTag[11];
		tags[0] = new DefMessTag("LANGUAGE", "en", "");
		tags[1] = new DefMessTag("TITLE", "title", "");
		tags[2] = new DefMessTag("NAME", "name", "");
		tags[3] = new DefMessTag("PHONENUMBER", "phonenumber", "");
		tags[4] = new DefMessTag("MOBILENUMBER", "mobilenumber", "");
		tags[5] = new DefMessTag("EMAIL", "email", "");
		tags[6] = new DefMessTag("SUBJECT", "subject", "");
		tags[7] = new DefMessTag("CONTENT", "content", "");
		tags[8] = new DefMessTag("EDDS_CONTACT_PERSON", "edds_contact_person", "");
		tags[9] = new DefMessTag("EDDS_SUBJECT", "edds_subject", "");
		tags[10] = new DefMessTag("EDDS_MESSAGE", "edds_message", "");
		//ADDTAG
	}
	
}

