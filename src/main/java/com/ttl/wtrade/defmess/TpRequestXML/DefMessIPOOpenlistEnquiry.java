package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessIPOOpenlistEnquiry extends DefaultDefMess {
	
	public DefMessIPOOpenlistEnquiry(){
		super("HKSWB006Q01", "20040601111216", "001", "01", "2", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[4];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("MARGINFINANCINGTYPE", "marginfinancingtype", "");
		tags[2] = new DefMessTag("DAYAFTERALLOTMENT", "dayafterallotment", "");
		tags[3] = new DefMessTag("INTERNET", "internet", "Y");
		//ADDTAG
	}
	
}

