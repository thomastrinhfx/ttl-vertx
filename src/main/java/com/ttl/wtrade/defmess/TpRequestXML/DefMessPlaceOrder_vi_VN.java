package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessPlaceOrder_vi_VN extends DefaultDefMess {
	
	public DefMessPlaceOrder_vi_VN(){
		super("HKSOR001Q01", "20040507112730", "001", "01", "2", "123456", "0000000000000000000000004", "vi", "VN");
		tags = new DefMessTag[1];
		tags[0] = new DefMessTag("LOOP_ORDER", "loop_order", "");
		//ADDTAG
	}
	
}

