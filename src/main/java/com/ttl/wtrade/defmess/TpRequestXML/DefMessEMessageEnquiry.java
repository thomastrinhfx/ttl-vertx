package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessEMessageEnquiry extends DefaultDefMess {
	
	public DefMessEMessageEnquiry(){
		super("HKSWB017Q01", "20021209053839", "HK", "01", "1", "", "0000000000000000000000001", "en", "US");
		tags = new DefMessTag[2];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("ISACTIVE", "isactive", "N");
		//ADDTAG
	}
	
}

