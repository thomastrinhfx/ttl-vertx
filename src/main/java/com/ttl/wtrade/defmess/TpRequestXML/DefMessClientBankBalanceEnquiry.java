package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessClientBankBalanceEnquiry extends DefaultDefMess {
	
	public DefMessClientBankBalanceEnquiry(){
		super("HKSOS040Q01", "20080815163429", "ITRADE", "", "ITRADE", "asdf", "0000000007", "en", "US");
		tags = new DefMessTag[7];
		tags[0] = new DefMessTag("NEEDQUERY", "needquery", "N");
		tags[1] = new DefMessTag("ServerAC", "serverac", "1");
		tags[2] = new DefMessTag("BankID", "bankid", "DAB");
		tags[3] = new DefMessTag("ClientID", "clientid", "010000");
		tags[4] = new DefMessTag("AccountSeq", "accountseq", "1");
		tags[5] = new DefMessTag("BankACID", "bankacid", ">12345678");
		tags[6] = new DefMessTag("SendingChannelID", "sendingchannelid", "INT");
		//ADDTAG
	}
	
}

