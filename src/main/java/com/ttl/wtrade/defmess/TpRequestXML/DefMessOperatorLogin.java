package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessOperatorLogin extends DefaultDefMess {
	
	public DefMessOperatorLogin(){
		super("COROL001Q01", "20040301103742", "001", "01", "1", "", "0000000000000000000000002", "en", "US");
		tags = new DefMessTag[5];
		tags[0] = new DefMessTag("OPERATORID", "operatorid", "1");
		tags[1] = new DefMessTag("PASSWORD", "password", "123456");
		tags[2] = new DefMessTag("ENTITYID", "entityid", "GRANDIF");
		tags[3] = new DefMessTag("BRANCHID", "branchid", "001");
		tags[4] = new DefMessTag("CHANNELID", "channelid", "INT");
		//ADDTAG
	}
	
}

