package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessUserIDSetupRequest extends DefaultDefMess {
	
	public DefMessUserIDSetupRequest(){
		super("HKSBC001Q01", "20021227054828", "001", "01", "1", "", "0000000000000000000000003", "en", "US");
		tags = new DefMessTag[4];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("CINO", "cino", "");
		tags[2] = new DefMessTag("USERID", "userid", "");
		tags[3] = new DefMessTag("PASSWORD", "password", "");
		//ADDTAG
	}
	
}

