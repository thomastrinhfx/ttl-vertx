package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessPriceAlertSubscriptionRequest extends DefaultDefMess {
	
	public DefMessPriceAlertSubscriptionRequest(){
		super("HKSWB014Q01", "20021209053839", "HK", "01", "1", "", "0000000000000000000000001", "en", "US");
		tags = new DefMessTag[5];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("MARKETID", "marketid", "");
		tags[2] = new DefMessTag("INSTRUMENTID", "instrumentid", "");
		tags[3] = new DefMessTag("ALERTSIGN", "alertsign", "");
		tags[4] = new DefMessTag("ALERTPRICE", "alertprice", "");
		//ADDTAG
	}
	
}

