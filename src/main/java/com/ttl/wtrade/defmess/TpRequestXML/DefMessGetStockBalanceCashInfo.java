package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessGetStockBalanceCashInfo extends DefaultDefMess {
	
	public DefMessGetStockBalanceCashInfo(){
		super("HKSOS079Q01", "20040709022500", "001", "01", "KEVIN", "", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[8];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "1");
		tags[1] = new DefMessTag("BANKID", "bankid", "");
		tags[2] = new DefMessTag("BANKACID", "bankacid", "");
		tags[3] = new DefMessTag("MARKETID", "marketid", "");
		tags[4] = new DefMessTag("STOCKID", "stockid", "");
		tags[5] = new DefMessTag("BS", "bs", "");
		tags[6] = new DefMessTag("ISMARGINCLIENT", "ismarginclient", "");
		tags[7] = new DefMessTag("ACTION", "action", "");
		//ADDTAG
	}
	
}

