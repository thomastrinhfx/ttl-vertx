package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessWSSTNORequest extends DefaultDefMess {
	
	public DefMessWSSTNORequest(){
		super("HKSWB035Q01", "20021227054828", "001", "01", "1", "", "0000000000000000000000003", "en", "US");
		tags = new DefMessTag[2];
		tags[0] = new DefMessTag("CINO", "cino", "");
		tags[1] = new DefMessTag("STNO", "stno", "");
		//ADDTAG
	}
	
}

