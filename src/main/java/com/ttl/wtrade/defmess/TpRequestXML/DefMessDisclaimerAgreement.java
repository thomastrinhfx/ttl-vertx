package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessDisclaimerAgreement extends DefaultDefMess {
	
	public DefMessDisclaimerAgreement(){
		super("HKSSN004Q01", "20021122015345", "001", "01", "1", "", "0000000000000000000000003", "en", "US");
		tags = new DefMessTag[4];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "1000000011");
		tags[1] = new DefMessTag("TRADINGACCSEQ", "tradingaccseq", "1");
		tags[2] = new DefMessTag("AGREE", "agree", "Y");
		tags[3] = new DefMessTag("INTERNET", "internet", "Y");
		//ADDTAG
	}
	
}

