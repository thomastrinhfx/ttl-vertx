package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessScripOptionSubmitRequest extends DefaultDefMess {
	
	public DefMessScripOptionSubmitRequest(){
		super("HKSWB025Q01", "20040601111216", "001", "01", "2", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[30];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("TRADINGACCSEQ", "tradingaccseq", "");
		tags[2] = new DefMessTag("ENTITLEMENTID", "entitlementid", "");
		tags[3] = new DefMessTag("PRODUCTID", "productid", "");
		tags[4] = new DefMessTag("MARKETID", "marketid", "");
		tags[5] = new DefMessTag("INSTRUMENTID", "instrumentid", "");
		tags[6] = new DefMessTag("BOOKCLOSEDATE", "bookclosedate", "");
		tags[7] = new DefMessTag("BOOKCLOSEQTY", "bookcloseqty", "");
		tags[8] = new DefMessTag("MAXDIVIDENDAMOUNT", "maxdividendamount", "");
		tags[9] = new DefMessTag("DIVIDENDAMOUNT", "dividendamount", "");
		tags[10] = new DefMessTag("SCRIPOPTIONQTY", "scripoptionqty", "");
		tags[11] = new DefMessTag("STATUS", "status", "");
		tags[12] = new DefMessTag("STATUSDESCRIPTION", "statusdescription", "");
		tags[13] = new DefMessTag("NUMOFSHARESHELD", "numofsharesheld", "");
		tags[14] = new DefMessTag("NUMOFSHARESHELDBALANCEDATE", "numofsharesheldbalancedate", "");
		tags[15] = new DefMessTag("DIVIDENDDISTRIBUTED", "dividenddistributed", "");
		tags[16] = new DefMessTag("CURRENCYID", "currencyid", "");
		tags[17] = new DefMessTag("DIVIDENDPERSHARE", "dividendpershare", "");
		tags[18] = new DefMessTag("CONVERSIONPRICE", "conversionprice", "");
		tags[19] = new DefMessTag("CLOSINGDATE", "closingdate", "");
		tags[20] = new DefMessTag("MAXRESULTQTY", "maxresultqty", "");
		tags[21] = new DefMessTag("ISCALREMAININGAMOUNT", "iscalremainingamount", "");
		tags[22] = new DefMessTag("ISCHECKPASSWORD", "ischeckpassword", "");
		tags[23] = new DefMessTag("ISCONFIRM", "isconfirm", "");
		tags[24] = new DefMessTag("PASSWORD", "password", "");
		tags[25] = new DefMessTag("REMAININGAMOUNT", "remainingamount", "");
		tags[26] = new DefMessTag("REGISTEREDQTY", "registeredqty", "");
		tags[27] = new DefMessTag("DIVIDENDDELIVER", "dividenddeliver", "");
		tags[28] = new DefMessTag("DIVIDENDRATIOPER", "dividendratioper", "");
		tags[29] = new DefMessTag("SETTLECURRENCYID", "settlecurrencyid", "");
		//ADDTAG
	}
	
}

