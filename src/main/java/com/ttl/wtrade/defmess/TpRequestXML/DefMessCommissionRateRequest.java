package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessCommissionRateRequest extends DefaultDefMess {
	
	public DefMessCommissionRateRequest(){
		super("HKSOR007Q02", "20021209053839", "HK", "01", "1", "", "0000000000000000000000001", "en", "US");
		tags = new DefMessTag[3];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "1");
		tags[1] = new DefMessTag("INSTRUMENTID", "instrumentid", "00001");
		tags[2] = new DefMessTag("MARKETID", "marketid", "HKEX");
		//ADDTAG
	}
	
}

