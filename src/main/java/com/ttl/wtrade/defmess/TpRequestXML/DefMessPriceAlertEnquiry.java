package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessPriceAlertEnquiry extends DefaultDefMess {
	
	public DefMessPriceAlertEnquiry(){
		super("HKSWB015Q01", "20021209053839", "HK", "01", "1", "", "0000000000000000000000001", "en", "US");
		tags = new DefMessTag[1];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		//ADDTAG
	}
	
}

