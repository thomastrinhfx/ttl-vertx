package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessFundTransferRequest extends DefaultDefMess {
	
	public DefMessFundTransferRequest(){
		super("HKSWB005Q01", "20040601111216", "001", "01", "2", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[8];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "00001");
		tags[1] = new DefMessTag("DIRECTION", "direction", "D");
		tags[2] = new DefMessTag("AMOUNT", "amount", "1");
		tags[3] = new DefMessTag("ACCOUNTID", "accountid", "123456789");
		tags[4] = new DefMessTag("PASSWORDVERIFICATION", "passwordverification", "N");
		tags[5] = new DefMessTag("PASSWORD", "password", "123456");
		tags[6] = new DefMessTag("SSOUSERID", "ssouserid", "");
		tags[7] = new DefMessTag("VRUPASSWORDVERIFICATION", "vrupasswordverification", "N");
		//ADDTAG
	}
	
}

