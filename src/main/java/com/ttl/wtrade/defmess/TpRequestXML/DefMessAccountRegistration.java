package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessAccountRegistration extends DefaultDefMess {
	
	public DefMessAccountRegistration(){
		super("HKSWB021Q01", "20021209053839", "HK", "01", "1", "", "0000000000000000000000001", "en", "US");
		tags = new DefMessTag[24];
		tags[0] = new DefMessTag("LANGUAGE", "en", "");
		tags[1] = new DefMessTag("ACCOUNTTYPE", "accounttype", "");
		tags[2] = new DefMessTag("ACCOUNTNUMBER", "accountnumber", "");
		tags[3] = new DefMessTag("TITLE", "title", "");
		tags[4] = new DefMessTag("NAME", "name", "");
		tags[5] = new DefMessTag("IDTYPE", "idtype", "");
		tags[6] = new DefMessTag("IDNUMBER", "idnumber", "");
		tags[7] = new DefMessTag("JOINTTITLE", "jointtitle", "");
		tags[8] = new DefMessTag("JOINTNAME", "jointname", "");
		tags[9] = new DefMessTag("JOINTIDTYPE", "jointidtype", "");
		tags[10] = new DefMessTag("JOINTIDNUMBER", "jointidnumber", "");
		tags[11] = new DefMessTag("OFFICENUMBER", "officenumber", "");
		tags[12] = new DefMessTag("PHONENUMBER", "phonenumber", "");
		tags[13] = new DefMessTag("MOBILENUMBER", "mobilenumber", "");
		tags[14] = new DefMessTag("EMAIL", "email", "");
		tags[15] = new DefMessTag("BRANCHID", "branchid", "");
		tags[16] = new DefMessTag("CONTACTPERSON", "contactperson", "");
		tags[17] = new DefMessTag("SUBJECT", "subject", "");
		tags[18] = new DefMessTag("MESSAGE", "message", "");
		tags[19] = new DefMessTag("ACCOUNTTYPESTRING", "accounttypestring", "");
		tags[20] = new DefMessTag("TITLESTRING", "titlestring", "");
		tags[21] = new DefMessTag("IDTYPESTRING", "idtypestring", "");
		tags[22] = new DefMessTag("JOINTTITLESTRING", "jointtitlestring", "");
		tags[23] = new DefMessTag("JOINTIDTYPESTRING", "jointidtypestring", "");
		//ADDTAG
	}
	
}

