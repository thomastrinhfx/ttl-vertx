package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessDownloadInstrument extends DefaultDefMess {
	
	public DefMessDownloadInstrument(){
		super("COMCM001Q01", "20021209053839", "HK", "01", "1", "", "0000000000000000000000001", "en", "US");
		tags = new DefMessTag[9];
		tags[0] = new DefMessTag("ReplyMsgID", "replymsgid", "HKSFODL001D02");
		tags[1] = new DefMessTag("ObjectName", "objectname", "VSINSTRUMENTDOWNLOAD");
		tags[2] = new DefMessTag("MULTIPART_ENABLED", "multipart_enabled", "Y");
		tags[3] = new DefMessTag("LoopCounter", "loopcounter", "1");
		tags[4] = new DefMessTag("LoopKeyField", "loopkeyfield", "");
		tags[5] = new DefMessTag("LoopCounter", "loopcounter", "1");
		tags[6] = new DefMessTag("LoopFieldName", "loopfieldname", "");
		tags[7] = new DefMessTag("LoopCounter", "loopcounter", "1");
		tags[8] = new DefMessTag("LoopWhere", "loopwhere", "");
		//ADDTAG
	}
	
}

