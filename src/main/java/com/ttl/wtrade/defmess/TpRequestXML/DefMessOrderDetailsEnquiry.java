package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessOrderDetailsEnquiry extends DefaultDefMess {
	
	public DefMessOrderDetailsEnquiry(){
		super("HKSWB012Q01", "20040601111216", "001", "01", "2", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[4];
		tags[0] = new DefMessTag("ORDERGROUPID", "ordergroupid", "");
		tags[1] = new DefMessTag("ORDERID", "orderid", "00000000");
		tags[2] = new DefMessTag("ISHISTORY", "ishistory", "N");
		tags[3] = new DefMessTag("BYORDERID", "byorderid", "N");
		//ADDTAG
	}
	
}

