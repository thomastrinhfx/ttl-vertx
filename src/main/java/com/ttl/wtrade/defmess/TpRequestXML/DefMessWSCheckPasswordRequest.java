package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessWSCheckPasswordRequest extends DefaultDefMess {
	
	public DefMessWSCheckPasswordRequest(){
		super("HKSWB035Q02", "20021227054828", "001", "01", "1", "", "0000000000000000000000003", "en", "US");
		tags = new DefMessTag[3];
		tags[0] = new DefMessTag("STNO", "stno", "");
		tags[1] = new DefMessTag("PASSWORD", "password", "");
		tags[2] = new DefMessTag("CHECKPASSWORD", "checkpassword", "");
		//ADDTAG
	}
	
}

