package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessMarketIndexHistoryDataRequest extends DefaultDefMess {
	
	public DefMessMarketIndexHistoryDataRequest(){
		super("HKSMIH001Q01", "20110329131900", "001", "01", "ITRADE", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[3];
		tags[0] = new DefMessTag("MARKETID", "marketid", "");
		tags[1] = new DefMessTag("FROMDATE", "fromdate", "");
		tags[2] = new DefMessTag("TODATE", "todate", "");
		//ADDTAG
	}
	
}

