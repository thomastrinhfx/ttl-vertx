package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessQueryMarketStatusInfo extends DefaultDefMess {
	
	public DefMessQueryMarketStatusInfo(){
		super("HKSMTI001Q01", "20021122015806", "001", "01", "1", "", "0000000000000000000000005", "en", "US");
		tags = new DefMessTag[2];
		tags[0] = new DefMessTag("MARKETID", "marketid", "");
		tags[1] = new DefMessTag("TYPE", "type", "");
		//ADDTAG
	}
	
}

