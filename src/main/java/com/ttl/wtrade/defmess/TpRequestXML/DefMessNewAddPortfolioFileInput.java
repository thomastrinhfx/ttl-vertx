package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessNewAddPortfolioFileInput extends DefaultDefMess {
	
	public DefMessNewAddPortfolioFileInput(){
		super("HKSWB040Q01", "20091127132830", "888", "01", "USR1", "123456", "0000000011", "en", "US");
		tags = new DefMessTag[3];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "1");
		tags[1] = new DefMessTag("NEWQUERY", "newquery", "Y");
		tags[2] = new DefMessTag("FILEINPUTLIST", "fileinputlist", "");
		//ADDTAG
	}
	
}

