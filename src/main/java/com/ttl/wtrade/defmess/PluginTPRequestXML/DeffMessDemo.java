package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DeffMessDemo extends DefaultDefMess {
	
	public DeffMessDemo(){
		super("HKSCE001Q01", "20091225023222", " ", "01", "TTLTL", "", "0000000000000000000000010", "en", "US");
		tags = new DefMessTag[6];
		tags[0] = new DefMessTag("OPERATORID", "operatorid", "");
		tags[1] = new DefMessTag("INSTRUMENTID", "instrumentid", "");
		tags[2] = new DefMessTag("OPERATORLIMIT", "operatorlimit", "");
		tags[3] = new DefMessTag("WINDOWSID", "windowsid", "");
		tags[4] = new DefMessTag("DISPLAYINTERNETBALANCE", "displayinternetbalance", "");
		tags[5] = new DefMessTag("MARKETID", "marketid", "");
		//ADDTAG
	}
	
}

