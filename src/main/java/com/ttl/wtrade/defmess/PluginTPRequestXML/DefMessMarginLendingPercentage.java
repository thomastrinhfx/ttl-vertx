package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessMarginLendingPercentage extends DefaultDefMess {
	
	public DefMessMarginLendingPercentage(){
		super("HKSWB038Q01", "20040507112730", "001", "01", "2", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[1];
		tags[0] = new DefMessTag("ENTITLEMENTID", "entitlementid", "");
		//ADDTAG
	}
	
}

