package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessGetOddLotAnnouncementData extends DefaultDefMess {
	
	public DefMessGetOddLotAnnouncementData(){
		super("HKSBOODD002", "20021218201009", "", "01", "1234567890", "1234567890123456789012345678", "2002111820100901564885275", "en", "US");
		tags = new DefMessTag[1];
		tags[0] = new DefMessTag("STATUS", "status", "");
		//ADDTAG
	}
	
}

