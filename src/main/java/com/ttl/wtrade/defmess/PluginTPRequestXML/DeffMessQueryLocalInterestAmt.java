package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DeffMessQueryLocalInterestAmt extends DefaultDefMess {
	
	public DeffMessQueryLocalInterestAmt(){
		super("HKSWB001AN04", "20021218201009", "", "01", "2", "123456", "2002111820100901564885275", "en", "US");
		tags = new DefMessTag[5];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("TRADINGACCSEQ", "tradingaccseq", "1");
		tags[2] = new DefMessTag("SETTLEDATE", "settledate", "");
		tags[3] = new DefMessTag("FEEID", "feeid", "");
		tags[4] = new DefMessTag("AMOUNT", "amount", "");
		//ADDTAG
	}
	
}

