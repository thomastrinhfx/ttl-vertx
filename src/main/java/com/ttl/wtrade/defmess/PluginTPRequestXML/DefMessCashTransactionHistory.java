package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessCashTransactionHistory extends DefaultDefMess {
	
	public DefMessCashTransactionHistory(){
		super("HKSTQ001Q02", "20021122015806", "001", "01", "1", "", "0000000000000000000000005", "en", "US");
		tags = new DefMessTag[13];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("TRADINGACCSEQ", "tradingaccseq", "");
		tags[2] = new DefMessTag("FROMDATE", "fromdate", "2002-01-01");
		tags[3] = new DefMessTag("TODATE", "todate", "2002-12-01");
		tags[4] = new DefMessTag("INTERNET", "internet", "Y");
		tags[5] = new DefMessTag("TXNTYPE", "txntype", "'ABC'");
		tags[6] = new DefMessTag("BANKEXTERNALFEEID", "bankexternalfeeid", "");
		tags[7] = new DefMessTag("BANKINTERNALFEEID", "bankinternalfeeid", "");
		tags[8] = new DefMessTag("BANKSYSTEMFEEID", "banksystemfeeid", "");
		tags[9] = new DefMessTag("BANKSYSTEMID", "banksystemid", "");
		tags[10] = new DefMessTag("STARTRECORD", "startrecord", "");
		tags[11] = new DefMessTag("ENDRECORD", "endrecord", "");
		tags[12] = new DefMessTag("NEEDTARGETCLIENTREGISTER", "needtargetclientregister", "N");
		//ADDTAG
	}
	
}

