package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessClientContract extends DefaultDefMess {
	
	public DefMessClientContract(){
		super("HKSWBOCCT001", "20021122015806", "001", "01", "1", "", "0000000000000000000000005", "en", "US");
		tags = new DefMessTag[6];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("FROMDATE", "fromdate", "");
		tags[2] = new DefMessTag("TODATE", "todate", "");
		tags[3] = new DefMessTag("UNSETTLED", "unsettled", "Y");
		tags[4] = new DefMessTag("BANKID", "bankid", "");
		tags[5] = new DefMessTag("BANKACID", "bankacid", "");
		//ADDTAG
	}
	
}

