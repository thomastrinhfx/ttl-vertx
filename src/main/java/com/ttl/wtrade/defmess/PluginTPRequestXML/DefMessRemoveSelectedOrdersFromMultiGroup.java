package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessRemoveSelectedOrdersFromMultiGroup extends DefaultDefMess {
	
	public DefMessRemoveSelectedOrdersFromMultiGroup(){
		super("HKSBOMO003", "20040601111216", "001", "01", "2", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[2];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("SELECTEDORDERID", "selectedorderid", "");
		//ADDTAG
	}
	
}

