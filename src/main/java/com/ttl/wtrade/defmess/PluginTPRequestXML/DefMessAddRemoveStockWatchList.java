package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessAddRemoveStockWatchList extends DefaultDefMess {
	
	public DefMessAddRemoveStockWatchList(){
		super("HKSCW001Q01", "20021122015806", "001", "01", "1", "", "0000000000000000000000005", "en", "US");
		tags = new DefMessTag[12];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("MARKETID", "marketid", "");
		tags[2] = new DefMessTag("INSTRUMENTID", "instrumentid", "");
		tags[3] = new DefMessTag("TARGETBUYPRICE", "targetbuyprice", "");
		tags[4] = new DefMessTag("STOPLOSTPRICE", "stoplostprice", "");
		tags[5] = new DefMessTag("STOPPROFITPRICE", "stopprofitprice", "");
		tags[6] = new DefMessTag("CATEGORY", "category", "");
		tags[7] = new DefMessTag("OPERATION", "operation", "");
		tags[8] = new DefMessTag("ERRORCODE", "errorcode", "");
		tags[9] = new DefMessTag("ERRORMSG", "errormsg", " ");
		tags[10] = new DefMessTag("INSTRUMENTLIST", "instrumentlist", "");
		tags[11] = new DefMessTag("MARKETLIST", "marketlist", "");
		//ADDTAG
	}
	
}

