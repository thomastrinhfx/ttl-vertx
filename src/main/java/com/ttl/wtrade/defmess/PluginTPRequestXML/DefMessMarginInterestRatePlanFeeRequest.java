package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessMarginInterestRatePlanFeeRequest extends DefaultDefMess {
	
	public DefMessMarginInterestRatePlanFeeRequest(){
		super("HKSWB039Q01", "20040507112730", "001", "01", "2", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[4];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("ENTITLEMENTID", "entitlementid", "");
		tags[2] = new DefMessTag("APPLIEDQTY", "appliedqty", "");
		tags[3] = new DefMessTag("MARGINPERCENTAGE", "marginpercentage", "");
		//ADDTAG
	}
	
}

