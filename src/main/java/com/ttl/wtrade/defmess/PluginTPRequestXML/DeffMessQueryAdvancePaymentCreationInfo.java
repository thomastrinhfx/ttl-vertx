package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DeffMessQueryAdvancePaymentCreationInfo extends DefaultDefMess {
	
	public DeffMessQueryAdvancePaymentCreationInfo(){
		super("HKSWB001AN06", "20021218201009", "", "01", "2", "123456", "2002111820100901564885275", "en", "US");
		tags = new DefMessTag[1];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		//ADDTAG
	}
	
}

