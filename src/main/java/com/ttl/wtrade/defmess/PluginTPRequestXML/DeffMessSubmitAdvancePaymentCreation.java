package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DeffMessSubmitAdvancePaymentCreation extends DefaultDefMess {
	
	public DeffMessSubmitAdvancePaymentCreation(){
		super("HKSWB001AN07", "20021218201009", "", "01", "1234567890", "1234567890123456789012345678", "2002111820100901564885275", "en", "US");
		tags = new DefMessTag[8];
		tags[0] = new DefMessTag("OPRID", "oprid", "");
		tags[1] = new DefMessTag("EMAILLIST", "emaillist", "");
		tags[2] = new DefMessTag("CLIENTID", "clientid", "");
		tags[3] = new DefMessTag("AMOUNT", "amount", "");
		tags[4] = new DefMessTag("ADVAVAIABLE", "advavaiable", "");
		tags[5] = new DefMessTag("ADVREQUEST", "advrequest", "");
		tags[6] = new DefMessTag("FULLNAME", "fullname", "");
		tags[7] = new DefMessTag("ISAPPROVAL", "isapproval", "");
		//ADDTAG
	}
	
}

