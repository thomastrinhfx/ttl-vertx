package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessOverdueDebt extends DefaultDefMess {
	
	public DefMessOverdueDebt(){
		super("HKSWBOOD001", "20021122015806", "001", "01", "1", "", "0000000000000000000000005", "en", "US");
		tags = new DefMessTag[6];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("QUERYDATE", "querydate", "");
		tags[2] = new DefMessTag("LOANDURATION", "loanduration", "");
		tags[3] = new DefMessTag("REMINDDAYS", "reminddays", "");
		tags[4] = new DefMessTag("FORCESELLDAYS", "forceselldays", "");
		tags[5] = new DefMessTag("TYPE", "type", "OVERDUEDEBT");
		//ADDTAG
	}
	
}

