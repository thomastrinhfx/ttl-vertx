package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessDoSubmitOddLot extends DefaultDefMess {
	
	public DefMessDoSubmitOddLot(){
		super("HKSBOODD003", "20021218201009", "", "01", "1234567890", "1234567890123456789012345678", "2002111820100901564885275", "en", "US");
		tags = new DefMessTag[4];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("ANNOUNCEMENTID", "announcementid", "");
		tags[2] = new DefMessTag("DETAIL", "detail", "");
		tags[3] = new DefMessTag("INTERFACESEQ", "interfaceseq", "-1");
		//ADDTAG
	}
	
}

