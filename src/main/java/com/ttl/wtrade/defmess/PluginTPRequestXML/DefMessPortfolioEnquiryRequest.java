package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessPortfolioEnquiryRequest extends DefaultDefMess {
	
	public DefMessPortfolioEnquiryRequest(){
		super("HKSBOPE001", "20021227054828", "001", "01", "ITRADE", "", "0000000000000000000000003", "en", "US");
		tags = new DefMessTag[8];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("TRADINGACCSEQ", "tradingaccseq", "1");
		tags[2] = new DefMessTag("INTERNET", "internet", "Y");
		tags[3] = new DefMessTag("WITHTRADE", "withtrade", "N");
		tags[4] = new DefMessTag("INCLUDECASHACCOUNTSUMMARY", "includecashaccountsummary", "Y");
		tags[5] = new DefMessTag("INCLUDETRADINGACCOUNTSUMMARY", "includetradingaccountsummary", "Y");
		tags[6] = new DefMessTag("INCLUDETRADINGACCOUNTDETAILS", "includetradingaccountdetails", "N");
		tags[7] = new DefMessTag("ENABLEADVANCEMONEYQUERY", "enableadvancemoneyquery", "Y");
		//ADDTAG
	}
	
}

