package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessMarginList extends DefaultDefMess {
	
	public DefMessMarginList(){
		super("HKSBOAML001", "20120502155230", "001", "01", "1", "", "0000000000000000000000005", "en", "US");
		tags = new DefMessTag[6];
		tags[0] = new DefMessTag("INSTRUMENTID", "instrumentid", "");
		tags[1] = new DefMessTag("MARKETID", "marketid", "");
		tags[2] = new DefMessTag("LENDINGPERCENTAGE", "lendingpercentage", "");
		tags[3] = new DefMessTag("STARTRECORD", "startrecord", "");
		tags[4] = new DefMessTag("ENDRECORD", "endrecord", "");
		tags[5] = new DefMessTag("EXPORTDATA", "exportdata", "");
		//ADDTAG
	}
	
}

