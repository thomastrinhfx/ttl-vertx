package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessLoanHistoryEnquiry extends DefaultDefMess {
	
	public DefMessLoanHistoryEnquiry(){
		super("HKSBOLR001", "20021218201009", "", "01", "1234567890", "1234567890123456789012345678", "2002111820100901564885275", "en", "US");
		tags = new DefMessTag[8];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("ACCOUNTSEQ", "accountseq", "1");
		tags[2] = new DefMessTag("CURRENCYID", "currencyid", "VND");
		tags[3] = new DefMessTag("TXNTYPEID", "txntypeid", "RECMB");
		tags[4] = new DefMessTag("FROMDATE", "fromdate", "");
		tags[5] = new DefMessTag("TODATE", "todate", "");
		tags[6] = new DefMessTag("STARTRECORD", "startrecord", "");
		tags[7] = new DefMessTag("ENDRECORD", "endrecord", "");
		//ADDTAG
	}
	
}

