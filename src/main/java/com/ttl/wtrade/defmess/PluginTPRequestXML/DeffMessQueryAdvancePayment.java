package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DeffMessQueryAdvancePayment extends DefaultDefMess {
	
	public DeffMessQueryAdvancePayment(){
		super("HKSWB001Q01", "20021218201009", "", "01", "1234567890", "1234567890123456789012345678", "2002111820100901564885275", "en", "US");
		tags = new DefMessTag[5];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("TRADINGACCSEQ", "tradingaccseq", "1");
		tags[2] = new DefMessTag("BANKID", "bankid", "");
		tags[3] = new DefMessTag("TPLUSX", "tplusx", "");
		tags[4] = new DefMessTag("FEEID", "feeid", "");
		//ADDTAG
	}
	
}

