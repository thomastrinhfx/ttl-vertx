package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessQueryAccountBalanceInfo extends DefaultDefMess {
	
	public DefMessQueryAccountBalanceInfo(){
		super("HKSCAS001Q01", "20021122015806", "001", "01", "1", "", "0000000000000000000000005", "en", "US");
		tags = new DefMessTag[4];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("TRADINGACCSEQ", "tradingaccseq", "");
		tags[2] = new DefMessTag("TYPE", "type", "");
		tags[3] = new DefMessTag("ISMARGINACC", "ismarginacc", "");
		//ADDTAG
	}
	
}

