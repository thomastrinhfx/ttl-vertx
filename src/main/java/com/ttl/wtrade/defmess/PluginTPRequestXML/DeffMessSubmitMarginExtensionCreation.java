package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DeffMessSubmitMarginExtensionCreation extends DefaultDefMess {
	
	public DeffMessSubmitMarginExtensionCreation(){
		super("HKSBOME002", "20021218201009", "", "01", "1234567890", "1234567890123456789012345678", "2002111820100901564885275", "en", "US");
		tags = new DefMessTag[5];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("FULLNAME", "fullname", "");
		tags[2] = new DefMessTag("EXPIRYDATE", "expirydate", "");
		tags[3] = new DefMessTag("OPRID", "oprid", "");
		tags[4] = new DefMessTag("EMAILLIST", "emaillist", "");
		//ADDTAG
	}
	
}

