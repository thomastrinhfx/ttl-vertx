package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessTruthClientCardMatrix extends DefaultDefMess {
	
	public DefMessTruthClientCardMatrix(){
		super("HKSBOCCM001", "20021122015806", "001", "01", "1", "", "0000000000000000000000005", "en", "US");
		tags = new DefMessTag[5];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "C000965");
		tags[1] = new DefMessTag("SERIALNUMBER", "serialnumber", "00988887");
		tags[2] = new DefMessTag("WORDMATRIX01", "wordmatrix01", "A2:7");
		tags[3] = new DefMessTag("WORDMATRIX02", "wordmatrix02", "F9:9");
		tags[4] = new DefMessTag("ATTEMPTLIMIT", "attemptlimit", "10");
		//ADDTAG
	}
	
}

