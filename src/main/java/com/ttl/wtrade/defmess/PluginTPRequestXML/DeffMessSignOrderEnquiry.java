package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DeffMessSignOrderEnquiry extends DefaultDefMess {
	
	public DeffMessSignOrderEnquiry(){
		super("HKSFOSO00001", "20170706155030", "002", "01", "2", "", "0000000000000000000000005", "en", "US");
		tags = new DefMessTag[13];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("STOCKID", "stockid", "");
		tags[2] = new DefMessTag("MARKETID", "marketid", "");
		tags[3] = new DefMessTag("BS", "bs", "");
		tags[4] = new DefMessTag("FROMTIME", "fromtime", "");
		tags[5] = new DefMessTag("TOTIME", "totime", "");
		tags[6] = new DefMessTag("STARTRECORD", "startrecord", "");
		tags[7] = new DefMessTag("ENDRECORD", "endrecord", "");
		tags[8] = new DefMessTag("EXPORTDATA", "exportdata", "N");
		tags[9] = new DefMessTag("SORTING", "sorting", "");
		tags[10] = new DefMessTag("STATUSFILTER", "statusfilter", "");
		tags[11] = new DefMessTag("INTERNET", "internet", "Y");
		tags[12] = new DefMessTag("ORDERTYPE", "ordertype", "");
		//ADDTAG
	}
	
}

