package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DeffMessTTLAdvance extends DefaultDefMess {
	
	public DeffMessTTLAdvance(){
		super("HKSWB001AN01", "20021218201009", "", "01", "2", "123456", "2002111820100901564885275", "en", "US");
		tags = new DefMessTag[3];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("AMOUNT", "amount", "");
		tags[2] = new DefMessTag("OPERATORID", "operatorid", "");
		//ADDTAG
	}
	
}

