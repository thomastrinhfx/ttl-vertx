package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessGetEntitlementActionRightList extends DefaultDefMess {
	
	public DefMessGetEntitlementActionRightList(){
		super("HKSBOENT007", "20021218201009", "", "01", "1234567890", "1234567890123456789012345678", "2002111820100901564885275", "en", "US");
		tags = new DefMessTag[7];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("STARTRECORD", "startrecord", "");
		tags[2] = new DefMessTag("ENDRECORD", "endrecord", "");
		tags[3] = new DefMessTag("FROMDATE", "fromdate", "");
		tags[4] = new DefMessTag("TODATE", "todate", "");
		tags[5] = new DefMessTag("ISSUETYPE", "issuetype", "");
		tags[6] = new DefMessTag("INSTRUMENTID", "instrumentid", "");
		//ADDTAG
	}
	
}

