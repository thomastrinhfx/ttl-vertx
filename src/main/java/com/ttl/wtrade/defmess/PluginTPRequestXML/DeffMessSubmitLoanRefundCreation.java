package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DeffMessSubmitLoanRefundCreation extends DefaultDefMess {
	
	public DeffMessSubmitLoanRefundCreation(){
		super("HKSBOLR004", "20021218201009", "", "01", "1234567890", "1234567890123456789012345678", "2002111820100901564885275", "en", "US");
		tags = new DefMessTag[10];
		tags[0] = new DefMessTag("OPRID", "oprid", "");
		tags[1] = new DefMessTag("EMAILLIST", "emaillist", "");
		tags[2] = new DefMessTag("CLIENTID", "clientid", "");
		tags[3] = new DefMessTag("AMOUNT", "amount", "");
		tags[4] = new DefMessTag("REMARK", "remark", "");
		tags[5] = new DefMessTag("LOANCUR", "loancur", "");
		tags[6] = new DefMessTag("LOANPAY", "loanpay", "");
		tags[7] = new DefMessTag("LOANREMAIN", "loanremain", "");
		tags[8] = new DefMessTag("FULLNAME", "fullname", "");
		tags[9] = new DefMessTag("ISAPPROVAL", "isapproval", "");
		//ADDTAG
	}
	
}

