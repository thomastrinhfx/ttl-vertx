package com.ttl.wtrade.defmess;

import java.util.Vector;

public abstract class DefaultDefMess {

	private String msgId;
	private String issueTime;
	private String issueLoc;
	private String issueMetd;
	private String oprId;
	private String pwd;
	private String resvr;
	private String language;
	private String country;
	
	public DefMessTag[] tags;
	
	public DefaultDefMess(String msgId, String issueTime, String issueLoc, String issueMetd, String oprId,
			String pwd, String resvr, String language, String country){
		this.msgId = msgId;
		this.issueTime = issueTime;
		this.issueLoc = issueLoc;
		this.issueMetd = issueMetd;
		this.oprId = oprId;
		this.pwd = pwd;
		this.resvr = resvr;
		this.language = language;
		this.country = country;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getIssueTime() {
		return issueTime;
	}

	public void setIssueTime(String issueTime) {
		this.issueTime = issueTime;
	}

	public String getIssueLoc() {
		return issueLoc;
	}

	public void setIssueLoc(String issueLoc) {
		this.issueLoc = issueLoc;
	}

	public String getIssueMetd() {
		return issueMetd;
	}

	public void setIssueMetd(String issueMetd) {
		this.issueMetd = issueMetd;
	}

	public String getOprId() {
		return oprId;
	}

	public void setOprId(String oprId) {
		this.oprId = oprId;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getResvr() {
		return resvr;
	}

	public void setResvr(String resvr) {
		this.resvr = resvr;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public DefMessTag[] getTags() {
		return tags;
	}

	public void setTags(DefMessTag[] tags) {
		this.tags = tags;
	}
	
	
	
}
