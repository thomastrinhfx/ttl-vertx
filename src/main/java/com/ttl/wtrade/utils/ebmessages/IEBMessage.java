package com.ttl.wtrade.utils.ebmessages;

public interface IEBMessage {
    public static final String CLIENT_ID = "ClientID";
    public static final String MESSAGE_TYPE = "Type";
    public static final String FROM_COMP = "From";
    public static final String TO_COMP = "To";
    public static final String ATTACHMENT = "Attach";
}
