package com.ttl.wtrade.utils;

public class ResponseField {
	private String mvName;
	private String mvReturn;
	private String mvValueType;
	private String mvFormat;
	private String mvDefaultValue;
	
	public ResponseField(String pName, String pReturn) {
		setMvName(pName);
		setMvReturn(pReturn);
	}
	
	public ResponseField(String pName, String pReturn, String pValueType) {
		setMvName(pName);
		setMvReturn(pReturn);
		setMvValueType(pValueType);
	}
	
	public ResponseField(String pName, String pReturn, String pValueType, String pFormat) {
		setMvName(pName);
		setMvReturn(pReturn);
		setMvValueType(pValueType);
		setMvFormat(pFormat);
	}

	public String getMvName() {
		return mvName;
	}

	public void setMvName(String mvName) {
		this.mvName = mvName;
	}

	public String getMvReturn() {
		return mvReturn;
	}

	public void setMvReturn(String mvReturn) {
		this.mvReturn = mvReturn;
	}

	public String getMvValueType() {
		return mvValueType;
	}

	public void setMvValueType(String mvValueType) {
		this.mvValueType = mvValueType;
	}

	public String getMvFormat() {
		return mvFormat;
	}

	public void setMvFormat(String mvFormat) {
		this.mvFormat = mvFormat;
	}

	public String getMvDefaultValue() {
		return mvDefaultValue;
	}

	public void setMvDefaultValue(String mvDefaultValue) {
		this.mvDefaultValue = mvDefaultValue;
	}
	
}
