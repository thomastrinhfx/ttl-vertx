package com.ttl.wtrade.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

import com.systekit.common.err.errParsingException;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLParser;
import com.systekit.common.msg.MsgManager;

public class ResponseFieldBuilder {
	
	private static ConcurrentHashMap<String, ResponseField> returnFields = null;
	
	public static ConcurrentHashMap<String, ResponseField> getFields() {
		if(returnFields == null) {
			try {
				returnFields = initField();
			} catch (errParsingException | IOException e) {
				e.printStackTrace();
			}
		}
		return returnFields;
	}
	
	@SuppressWarnings("resource")
	public static ConcurrentHashMap<String, ResponseField> initField() 
			throws IOException, errParsingException {
		IMsgXMLParser parser = MsgManager.createParser();
		
		FileInputStream fis = new FileInputStream("response_mapping.xml");
		StringBuffer sb = new StringBuffer();
		int i;
		while((i = fis.read()) != -1){
			sb = sb.append((char) i);
		}
		String toBeParsedXML = sb.toString();
		IMsgXMLNode resultNode = parser.parseXML(toBeParsedXML);
		
		IMsgXMLNode fields = resultNode.getChildNode("fields");
		ConcurrentHashMap<String, ResponseField> returnFields = 
				new ConcurrentHashMap<String, ResponseField>();
		
		for(int iter =0; iter < fields.getChildTotal(); iter++) {
			IMsgXMLNode node = fields.getChildNode(iter);
			ResponseField tmp = new ResponseField(
					node.getAttribute("name"), 
					node.getAttribute("return"),
					node.getAttribute("type"));
			
			returnFields.put(node.getAttribute("name"), tmp);
		}
		return returnFields;
	}
}
