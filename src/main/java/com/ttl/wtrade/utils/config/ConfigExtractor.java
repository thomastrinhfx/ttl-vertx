package com.ttl.wtrade.utils.config;

import java.util.*;

public class ConfigExtractor {
    private final String WORKER_GROUP_PREFIX = "PluginExternalTPServer";
    private final String ACTION_GROUP_PREFIX = "Action";
    private Properties rawConfig = null;
    private Map actionMap = null;

    public ConfigExtractor(Properties pConfigRaw) {
        this.rawConfig = pConfigRaw;
    }

    public Map<String, WorkerConfig> extractAllWorkerConfig() {
        boolean isAvailable = this.rawConfig.getProperty(WORKER_GROUP_PREFIX + "Available").equals("true");
        if (!isAvailable) return null;

        //Import all workers config
        Map<String, WorkerConfig> workerMap = new HashMap<>();
        int numOfWorkers = Integer.parseInt(this.rawConfig.getProperty(WORKER_GROUP_PREFIX + "Count"));
        for (int i = 1; i <= numOfWorkers; i++) {
            WorkerConfig workerConfig = getWorkerConfig(i);
            workerMap.put(workerConfig.getName(), workerConfig);
        }

        //Import all actions
        importActionInfoToWorker(workerMap);
        return workerMap;
    }

    private void importActionInfoToWorker(Map<String, WorkerConfig> workerMap) {
        if (workerMap.size() == 0) return;
        int numOfActions = Integer.parseInt(this.rawConfig.getProperty(ACTION_GROUP_PREFIX + "Count"));
        for (int i = 1; i <= numOfActions; i++) {
            ActionConfig actionConfig = getActionConfig(i);
            WorkerConfig willBeImportedWorker = workerMap.get(actionConfig.getWorker());

            //Add action to worker
            willBeImportedWorker.addAction(actionConfig);
            //Add action to actionList
            if (this.getActionMap() == null) {
                this.setActionMap(new HashMap());
            }
            this.getActionMap().put(actionConfig.getName(), actionConfig);
        }
    }

//    private List extractListValue(String keyword, Properties allConfigs) {
//        String listString = allConfigs.getProperty(keyword);
//        String[] listStringArray = listString.split(",");
//        return Arrays.asList(listStringArray);
//    }

    private ActionConfig getActionConfig(int index) {
        String ACTION_PREFIX = String.format("%s%d_", ACTION_GROUP_PREFIX, index);

        String actionName = this.rawConfig.getProperty(ACTION_PREFIX + "NAME");
        String actionClassName = this.rawConfig.getProperty(ACTION_PREFIX + "CLASSNAME");
        String actionMessDefClassName = this.rawConfig.getProperty(ACTION_PREFIX + "MESSDEF");
        String actionWorker = this.rawConfig.getProperty(ACTION_PREFIX + "WORKER");

        return new ActionConfig(actionName,actionClassName,actionMessDefClassName,actionWorker);
    }

    public WorkerConfig getWorkerConfig(int index) {
        String WORKER_PREFIX = String.format("%s%d_", WORKER_GROUP_PREFIX, index);
        //Extract connection info
        String workerIP = this.rawConfig.getProperty(WORKER_PREFIX + "IP");
        String workerPort = this.rawConfig.getProperty(WORKER_PREFIX + "PORT");
        int revBuffLen = Integer.parseInt(this.rawConfig.getProperty(WORKER_PREFIX + "RECEIVEBUFFERLEN"));
        int headerLen = Integer.parseInt(this.rawConfig.getProperty(WORKER_PREFIX + "HEADERLEN"));
        String serverSide = this.rawConfig.getProperty(WORKER_PREFIX + "SERVERSIDE");
        
        //Extract worker
        String workerName = this.rawConfig.getProperty(WORKER_PREFIX + "NAME");
        String workerClassName = this.rawConfig.getProperty(WORKER_PREFIX + "CLASSNAME");

        //Extract worker's actions info
        Map workerActionMap = new HashMap();

        return new WorkerConfig(workerName, workerClassName, workerIP, workerPort, revBuffLen, headerLen, serverSide, workerActionMap);
    }

    public Map getActionMap() {
        return actionMap;
    }

    public void setActionMap(Map actionMap) {
        this.actionMap = actionMap;
    }
}
