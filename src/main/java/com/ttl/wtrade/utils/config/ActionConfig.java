package com.ttl.wtrade.utils.config;

public class ActionConfig {
    private String name;
    private String className;
    private String messDef;
    private String worker;

    public ActionConfig(String name, String className, String messDef, String worker) {
        this.name = name;
        this.className = className;
        this.messDef = messDef;
        this.worker = worker;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMessDef() {
        return messDef;
    }

    public void setMessDef(String messDef) {
        this.messDef = messDef;
    }

    public String getWorker() {
        return worker;
    }

    public void setWorker(String worker) {
        this.worker = worker;
    }
}
