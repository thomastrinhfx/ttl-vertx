package com.ttl.wtrade.utils.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WorkerConfig {
    private String name = null;
    private String className = null;
    private String ip = null;
    private String port = null;
    private int revBuffLen = 0;
    private int headerLen = 0;
    private String serverSide = null;
    private Map<String, ActionConfig> actions = null;

    public WorkerConfig(String name, String className, String ip, String port, int revBuffLen, int headerLen, String serverSide, Map actions) {
        this.setName(name);
        this.setIp(ip);
        this.setPort(port);
        this.setActions(actions);
        this.setClassName(className);
        this.setRevBuffLen(revBuffLen);
        this.setHeaderLen(headerLen);
        this.setServerSide(serverSide);
    }
    
    

    public String getServerSide() {
		return serverSide;
	}



	public void setServerSide(String serverSide) {
		this.serverSide = serverSide;
	}



	public int getRevBuffLen() {
		return revBuffLen;
	}

	public void setRevBuffLen(int revBuffLen) {
		this.revBuffLen = revBuffLen;
	}

	public int getHeaderLen() {
		return headerLen;
	}



	public void setHeaderLen(int headerLen) {
		this.headerLen = headerLen;
	}



	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public Map<String, ActionConfig> getActions() {
        return actions;
    }

    public void setActions(Map actions) {
        this.actions = actions;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public void addAction(ActionConfig newAction) {
        if (this.actions == null) {
            this.actions = new HashMap<>();
        }
        this.actions.put(newAction.getName(), newAction);
    }
}
