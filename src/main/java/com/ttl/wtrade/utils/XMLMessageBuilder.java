package com.ttl.wtrade.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import com.systekit.common.err.errParsingException;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLParser;
import com.systekit.common.msg.MsgManager;
import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

import io.vertx.core.json.JsonObject;

public class XMLMessageBuilder {
	
//	private static IMsgXMLNode resultNode = null;

	public static IMsgXMLNode buildNode(JsonObject params, DefaultDefMess defMess) throws errParsingException, IOException{
		IMsgXMLParser parser = MsgManager.createParser();
		IMsgXMLNode resultNode = null;
		IMsgXMLNode tmp = null;
		if(resultNode == null){
			FileInputStream fis = new FileInputStream("XMLTPTemplate.xml");
			StringBuffer sb = new StringBuffer();
			int i;
			while((i = fis.read()) != -1){
				sb = sb.append((char) i);
			}
			String toBeParsedXML = sb.toString().replaceAll("MSGID", defMess.getMsgId());
			resultNode = parser.parseXML(toBeParsedXML);
		}
		resultNode.setAttribute("msgId", defMess.getMsgId());
		resultNode.setAttribute("issueTime", defMess.getIssueTime());
		resultNode.setAttribute("issueLoc", defMess.getIssueLoc());
		resultNode.setAttribute("issueMetd", defMess.getIssueMetd());
		resultNode.setAttribute("oprId", defMess.getOprId());
		resultNode.setAttribute("pwd", defMess.getPwd());
		resultNode.setAttribute("resvr", defMess.getResvr());
		resultNode.setAttribute("language", defMess.getLanguage());
		resultNode.setAttribute("country", defMess.getCountry());
		for(int i = 0; i < defMess.tags.length; i++){
			DefMessTag tag = defMess.tags[i];
			String serverTag = tag.serverTag;
			String clientTag = tag.clientTag;
			String value = params.getString(clientTag) != null ? params.getString(clientTag):tag.defaultVal;
			tmp = resultNode.addChildNode(serverTag);
			tmp.setValue(value);
		}
//		Iterator<DefMessTag> it = defMess.tags.iterator();
//		while(it.hasNext()){
//			DefMessTag tag = it.next();
//			String serverTag = tag.serverTag;
//			String clientTag = tag.clientTag;
//			String value = params.getString(clientTag) != null ? params.getString(clientTag):tag.defaultVal;
//			tmp = resultNode.addChildNode(serverTag);
//			tmp.setValue(value);
//		}
		return resultNode;
	}
	
}
