package com.ttl.wtrade.utils;

import com.systekit.common.msg.IMsgXMLNode;

import io.vertx.core.json.JsonObject;

public class XMLParser {
	public static JsonObject toJSONObject(IMsgXMLNode pNode) {
		JsonObject lvReturnObj = new JsonObject();
	
		
		int mvNumOfRecord = pNode.getChildTotal();

		for(int i = 0; i < mvNumOfRecord; i++) {
			IMsgXMLNode lvRecord = pNode.getChildNode(i);
			
			if(lvRecord.getChildTotal() > 1) {
				lvReturnObj.put(lvRecord.getName(), toJSONObject(lvRecord));
			} else {
				lvReturnObj.put(lvRecord.getName(), lvRecord.getValue());
			}
		}
		
		
		
		return lvReturnObj;
	}
}
