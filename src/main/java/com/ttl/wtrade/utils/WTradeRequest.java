package com.ttl.wtrade.utils;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.Session;

import java.util.Hashtable;
import java.util.Set;

public class WTradeRequest implements IDefaultRequest {
    private String mvAction = null;
    private RoutingContext mvRouteCtx = null;
    private String mvClientID = null;
    private Set mvCookies = null;
    private JsonObject mvJson = null;
    private Session mvSession = null;
    private Hashtable mvFormData = null;

    public WTradeRequest(RoutingContext ctx) {
        if (ctx != null) {
            this.mvRouteCtx = ctx;
            this.mvAction = ctx.request().getParam("actionName");
            this.mvJson = ctx.getBodyAsJson();
            this.mvClientID = mvJson.getString("clientID");
            this.mvCookies = ctx.cookies();
            this.mvSession = ctx.session();
        }
    }

    public WTradeRequest(String pvAction, Set pvCookies, JsonObject pvParams, RoutingContext pvRouteCtx, String pvClientID) {
        this.setMvAction(pvAction);
        this.setMvCookies(pvCookies);
        this.setMvJson(pvParams);
        this.setMvRouteCtx(pvRouteCtx);
        this.setMvClientID(pvClientID);
    }

    public boolean validate() {
        if (this.mvClientID == null || this.mvAction == null || this.mvJson == null) {
            return false;
        }
        return true;
    }

    public String getMvClientID() {
        return mvClientID;
    }

    public void setMvClientID(String mvClientID) {
        this.mvClientID = mvClientID;
    }

    public Set getMvCookies() {
        return mvCookies;
    }

    public void setMvCookies(Set mvCookies) {
        this.mvCookies = mvCookies;
    }

    public JsonObject getMvJson() {
        return mvJson;
    }

    public void setMvJson(JsonObject mvJson) {
        this.mvJson = mvJson;
    }

    public RoutingContext getMvRouteCtx() {
        return mvRouteCtx;
    }

    public void setMvRouteCtx(RoutingContext mvRouteCtx) {
        this.mvRouteCtx = mvRouteCtx;
    }

    public String getMvAction() {
        return mvAction;
    }

    public void setMvAction(String mvAction) {
        this.mvAction = mvAction;
    }

    public String getParameter(String pvParamName) {
        return this.mvJson.getString(pvParamName);
    }

    public WTradeResponse getReponse() {
        return new WTradeResponse(mvRouteCtx, mvClientID, mvCookies);
    }

    public String printContent() {
        return String.format("\n +Action: %s\n +Params: %s\n +Cookies: %s\n +ClientID: %s", mvAction, mvJson.toString(), mvCookies, mvClientID);
    }

    public Hashtable getMvFormData() {
        return mvFormData;
    }

    public void setMvFormData(Hashtable mvFormData) {
        this.mvFormData = mvFormData;
    }

    public Session getMvSession() {
        return mvSession;
    }

    public void setMvSession(Session mvSession) {
        this.mvSession = mvSession;
    }
}
