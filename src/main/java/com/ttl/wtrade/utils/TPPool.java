package com.ttl.wtrade.utils;

import java.util.HashMap;
import java.util.Map.Entry;

import com.systekit.common.msg.IMsgXMLParser;
import com.systekit.common.msg.MsgManager;
import com.ttl.old.itrade.hks.HKSMain;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPConnector;
import com.ttl.old.itrade.tp.TPConnectorThread;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.wtrade.utils.config.ActionConfig;
import com.ttl.wtrade.utils.config.WorkerConfig;

public class TPPool {

	private static HashMap<String, TPManager> TPManagerPool = null;
	private static IMain hksMain = null;
	
	public static void add(WorkerConfig workerConfig){
		if(TPManagerPool == null){
			TPManagerPool = new HashMap<>();
		}
		if(hksMain == null){
			hksMain = new HKSMain();
			IMsgXMLParser parser = MsgManager.createParser();
	        hksMain.setParser(parser);
		}
		for(Entry<String, ActionConfig> entry:workerConfig.getActions().entrySet()){
			String[] ip = {workerConfig.getIp()};
	        int[] port = {Integer.parseInt(workerConfig.getPort())};
	        int revBuffLen = workerConfig.getRevBuffLen();
	        int headerLen = workerConfig.getHeaderLen();
	        String side = workerConfig.getServerSide();
	        TPConnector connector = new TPConnector(ip, port, revBuffLen, headerLen);
	        TPManager manager = new TPManager(connector, hksMain, side);
	        TPConnectorThread connectorThread = new TPConnectorThread(manager, connector);
	        connectorThread.start();
	        manager.start();
	        TPManagerPool.put(entry.getKey(), manager);
		}	
	}
	
	public static TPManager get(String actionName){
		return TPManagerPool.get(actionName);
	}
	
}
