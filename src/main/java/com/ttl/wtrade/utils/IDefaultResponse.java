package com.ttl.wtrade.utils;

public interface IDefaultResponse {
    boolean validate();
    void send();
}
