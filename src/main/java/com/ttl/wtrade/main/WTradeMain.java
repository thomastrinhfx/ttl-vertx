package com.ttl.wtrade.main;

import com.hazelcast.config.Config;
import com.ttl.wtrade.utils.WTradeLogger;
import com.ttl.wtrade.verticles.WTradeServer;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.spi.cluster.ClusterManager;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;

public class WTradeMain {
    private final static String NAME = "WTradeMain";
    public static void main(String[] args) {
        Config hazelcastConfig = new Config();
        hazelcastConfig.getNetworkConfig()
                .getJoin()
                .getTcpIpConfig()
                .addMember("127.0.0.1")
                .setEnabled(true);
        hazelcastConfig.getNetworkConfig()
                .getJoin()
                .getMulticastConfig().setEnabled(false);

        ClusterManager mgr = new HazelcastClusterManager(hazelcastConfig);
        VertxOptions options = new VertxOptions().setClusterManager(mgr);

        Vertx.clusteredVertx(options, res -> {
            if (res.succeeded()) {
                WTradeLogger.print(NAME, "Cluster starts successfully!");
                Vertx vertx = res.result();
                vertx.deployVerticle(new WTradeServer(), response -> {
                    if (response.succeeded()) {
                        WTradeLogger.print(NAME, "WTradeServer deployed successfully!");
                    } else {
                        WTradeLogger.print(NAME, "WTradeServer deployment failed: " + response.cause());
                    }
                });
            } else {
                WTradeLogger.print(NAME,"Starting cluster failed: " + res.cause());
            }
        });
    }
}
