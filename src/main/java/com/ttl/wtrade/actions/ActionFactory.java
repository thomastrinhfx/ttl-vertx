package com.ttl.wtrade.actions;

import com.ttl.wtrade.defmess.DefaultDefMess;
import com.ttl.wtrade.utils.config.ActionConfig;
import com.ttl.wtrade.utils.WTradeLogger;
import com.ttl.wtrade.utils.WTradeRequest;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class ActionFactory {
    private static Map<String, ActionConfig> actions = new HashMap<String, ActionConfig>();
    private static int count = 0;
    public static void generateFactory(Map<String, ActionConfig> configMap) {
        actions.putAll(configMap);
        WTradeLogger.print("AAAAAAAAAAAAAA   ", count++ + "");
    }

    public static DefaultAction getActionInstance(String pActionName, WTradeRequest pRequest, DefaultDefMess pDefaultDefMess) 
    		throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, 
    		SecurityException, IllegalArgumentException, InvocationTargetException 
    {
        Constructor ctr = Class.forName(actions.get(pActionName).getClassName()).getConstructor(WTradeRequest.class, DefaultDefMess.class);
    	return (DefaultAction) ctr.newInstance(pRequest, pDefaultDefMess);
    	
    }
}
