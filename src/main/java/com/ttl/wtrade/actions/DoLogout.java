package com.ttl.wtrade.actions;

import com.systekit.common.msg.IMsgXMLNode;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.wtrade.defmess.DefaultDefMess;
import com.ttl.wtrade.utils.TPPool;
import com.ttl.wtrade.utils.WTradeRequest;
import com.ttl.wtrade.utils.WTradeResponse;
import com.ttl.wtrade.utils.XMLMessageBuilder;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Session;

public class DoLogout extends DefaultAction {

	public DoLogout(WTradeRequest pRequest, DefaultDefMess pDefaultDefMess){
		super(pRequest, pDefaultDefMess);
	}
	
	@Override
	public STATUS exec() {
		JsonObject mvParams = mvRequest.getMvJson();
        Session lvSession = mvRequest.getMvRouteCtx().session();
        JsonObject lvReturnObj = new JsonObject();
        
        IMsgXMLNode mvXMLMessage;
		try {
			String lvClientID = lvSession.get("CLIENTID");
			mvXMLMessage = XMLMessageBuilder.buildNode(mvParams, mvDefaultDefMess);
			mvXMLMessage.getChildNode("CLIENTID").setValue(lvClientID.substring(3, lvClientID.length()));
			mvXMLMessage.getChildNode("TRADINGACCSEQ").setValue("1");
			
			IMsgXMLNode mvReturnNode = TPPool.get(mvRequest.getMvAction())
					.send(mvXMLMessage.getAttribute("resvr"), mvXMLMessage.toString());
			
			if(TPErrorHandling.TP_NORMAL == mvTPError.checkError(mvReturnNode)) {
				lvReturnObj.put("mvMessage", "");
		    	lvReturnObj.put("success", true);
				lvSession.destroy();
			}
			else {
				lvReturnObj.put("mvMessage", "");
		    	lvReturnObj.put("success", false);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		WTradeResponse mvResponse = mvRequest.getReponse();
		mvResponse.setMvResJson(lvReturnObj);
		mvResponse.send();	
        
        return STATUS.NORMAL;
	}

}
