package com.ttl.wtrade.actions;


import com.ttl.old.itrade.tp.TPErrorHandling;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.winvest.hks.util.Utils;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.wtrade.actions.DefaultAction.STATUS;
import com.ttl.wtrade.defmess.DefaultDefMess;
import com.ttl.wtrade.utils.TPPool;
import com.ttl.wtrade.utils.WTradeLogger;
import com.ttl.wtrade.utils.WTradeRequest;
import com.ttl.wtrade.utils.WTradeResponse;
import com.ttl.wtrade.utils.XMLMessageBuilder;
import com.ttl.wtrade.utils.XMLParser;
import com.ttl.wtrade.verticles.MDSWorker;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Session;

public class DoLogin extends DefaultAction {
	public DoLogin(WTradeRequest pRequest, DefaultDefMess pDefaultDefMess){
		super(pRequest, pDefaultDefMess);
	}

	@Override
	public STATUS exec() {
		
        JsonObject mvParams = mvRequest.getMvJson();
        JsonObject lvReturnObj = new JsonObject();
        try {
        	
        	String lvClientID = mvParams.getString("mvClientID");
        	
			IMsgXMLNode mvXMLMessage = XMLMessageBuilder.buildNode(mvParams, mvDefaultDefMess);
			mvXMLMessage.getChildNode("CLIENTID").setValue(lvClientID.substring(3, lvClientID.length()));
			
			boolean lvSkipPasswordChecking = true;
			String lvCheckWWWEnabled = "false";
			String lvITradeServerIP = "localhost";
			String lvSingleSignOnPhase = "0";
			String lvCINO = new String();
			String lvEBPassword = "";
			String lvHostString = "00";
			
			mvXMLMessage.getChildNode("EBPASSWORD").setValue(lvEBPassword);
			mvXMLMessage.getChildNode("HOSTSTRING").setValue(lvHostString);
			mvXMLMessage.getChildNode("ITRADESERVERIP").setValue(lvITradeServerIP);
			mvXMLMessage.getChildNode("CHECKWWWENABLED").setValue(lvCheckWWWEnabled);
			mvXMLMessage.getChildNode("SKIPPASSWORDCHECKING").setValue(lvSkipPasswordChecking?"Y":"N");
			mvXMLMessage.getChildNode("SINGLESIGNONPHASE").setValue(lvSingleSignOnPhase);
//			mvXMLMessage.getChildNode("CINO").setValue(lvCINO);
			mvXMLMessage.setName("A_HKSSN001Q01");
			mvXMLMessage.setAttribute("retryForTimeout", "T");
			mvXMLMessage.setAttribute("language", "vi");
			mvXMLMessage.setAttribute("country", "VN");

			TPManager x = TPPool.get(mvRequest.getMvAction());
			IMsgXMLNode mvReturnNode = x
					.send(mvXMLMessage.getAttribute("resvr"), mvXMLMessage.toString());
			
			WTradeLogger.print("DO LOGIN RESPONSE", mvReturnNode.toString());
			
			//lvReturnObj = XMLParser.toJSONObject(mvReturnNode);
			
			if(TPErrorHandling.TP_NORMAL == mvTPError.checkError(mvReturnNode)) {
				
				lvReturnObj.put("mvMessage", "");
		    	lvReturnObj.put("needChangePwd", "null");
		    	lvReturnObj.put("success", true);
		    	
		    	Session session = mvRequest.getMvRouteCtx().session();
		    	String s = mvParams.getValue("mvClientID").toString();
		    	session.put("CLIENTID", s);
				
			} else {
				IMsgXMLNode lvErrorCodeNode = mvReturnNode.getChildNode("C_ERROR_CODE");
				IMsgXMLNode lvErrorDescNode = mvReturnNode.getChildNode("C_ERROR_DESC");
				
				if( lvErrorCodeNode == null || lvErrorCodeNode.getValue().equals("") ) {
					lvReturnObj.put("mvMessage", "ERROR");
				} else {
					lvReturnObj.put("mvMessage", lvErrorDescNode == null ? "" : lvErrorDescNode.getValue());
				}
				
		    	lvReturnObj.put("needChangePwd", "null");
		    	lvReturnObj.put("success", false);
			}
		} catch (Exception e) {
			
		}
    	

    	WTradeResponse mvResponse = mvRequest.getReponse();
		mvResponse.setMvResJson(lvReturnObj);
		mvResponse.send();	
		return STATUS.NORMAL;
	}
}
