package com.ttl.wtrade.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ttl.old.itrade.tp.TPErrorHandling;
import com.opensymphony.xwork2.Action;
import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.HKSTag;
import com.ttl.old.itrade.hks.bean.FunctionBean;
import com.ttl.old.itrade.hks.util.HKSInstrument;
import com.ttl.old.itrade.hks.util.StringReplacer;
import com.ttl.old.itrade.interfaces.IMain;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import com.systekit.winvest.hks.config.mapping.HKSConstant;
import com.systekit.winvest.hks.util.Utils;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.ttl.wtrade.defmess.DefaultDefMess;
import com.ttl.wtrade.utils.WTradeLogger;
import com.ttl.wtrade.utils.WTradeRequest;
import com.ttl.wtrade.utils.WTradeResponse;

/**
 * The DefaultAction class defines methods that are common to all
 * action, including handling information which were returned from server .
 *
 * @author not attribute
 *
 */
public abstract class DefaultAction extends ActionSupport implements Action,
		Preparable {
//		, ServletRequestAware, ServletResponseAware {
	
	public static enum STATUS{
		NORMAL, ERROR
	}
	
	protected String _sActionName;
//	protected SessionTracker mvSessionTracker;
	protected Properties _pIni;
	protected String _htmlTemplatePath;
	protected String _sNotAvailableUrl;
	protected String _sNotCNAvailableUrl;
	protected String _sNotTWAvailableUrl;
	protected static Vector svUsingStockCodeFormatMarket;
	public Vector svSkipResetTimeOutFunction;

	protected static String svClientIDFormat;
	protected static String svStockCodeFormat;
	protected static String svStockPriceFormat;
	protected static String svStockAvgPriceFormat;
	protected static String svStockQuantityFormat;
	protected static String svIntPriceFormat;
	protected static String svCashValueFormat;
	protected static String svDateFormat;
	protected static String svTimeFormat;
	protected static String svDateTimeFormat;
	protected static String svSymbol;
	protected static String svSymbolPosition;
	public static String svSingleSignOnPhase;

	protected static int svPasswordMinLength;
	protected static int svPasswordMaxLength;

    //BEGIN - Task #: TTL-HK-WLCW-00963. - Walter Lau 20100122 Re-Try Download Data
    protected static long svReDownloadDataTime;
    protected static int svReDownloadDataDownloadTimes;
    protected static boolean svReDownloadDataEnable;
    //END - Task #: TTL-HK-WLCW-00963. - Walter Lau 20100122 Re-Try Download Data

//	public String mvCINO = new String();
    public String mvCINO = "";
	public String mvMessage;
	public String mvErrorCode;
	public String errorMessage;

	// Start task: Giang Tran 2010-04-29: remove static because have error merge session when many user login
	//Start Modified By YuLong On 20090710
//	public HttpServletRequest mvRequest;
	//End Modified By YuLong On 20090710
	// End task: Giang Tran 2010-04-29: remove static because have error merge session when many user login

//	public HttpServletResponse mvResponse;
	public Map mvSession;
	public String mvLastAction;
	public String mvChildLastAction;
	public String mvLang = "0";
	//Begin Task #: - TTL-GZ-ZZW-00020 Wind Zhao 20091230 [iTrade R5] Full Internationalization and Localization
	public String mvLanguage = "en_US";
	//End Task #: - TTL-GZ-ZZW-00020 Wind Zhao 20091230 [iTrade R5] Full Internationalization and Localization
	public String mvTimelyUpdate;
	public String mvErrorResult="fail";
	public String mvIndex;

	//Begin Task #: - TTL-GZ-ZZW-00019 Wind Zhao 20091230 [iTrade R5] Numeric locale for Vietnam
	private static int svStockPriceDecimalPlaces = -1;
	private static int svStockAvgPriceDecimalPlaces = -1;
	private static int svIntPriceDecimalPlace = -1;
	private static int svCashValueDecimalPlaces = -1;
	private static String[] svLanguageAndCountry;
	static{
		String defLanguage = "en_US";/*IMain.getProperty("defaultLanguage");*/
//		if(defLanguage == null || "".equals(defLanguage)){
//			defLanguage = "en_US";
//		}
		svLanguageAndCountry = defLanguage.split("_");
	}

	public static String SAVEAUTHENTICATE = "SAVEAUTHENTICATE";
	public static String CHILDLASTACTION = "CHILDLASTACTION";

	static{
			svClientIDFormat = IMain.getProperty("clientIDMask");
			svStockCodeFormat = IMain.getProperty("stockCodeFormat");
			svStockPriceFormat = IMain.getProperty("stockPriceFormat");
			svStockAvgPriceFormat = IMain.getProperty("stockAvgPriceFormat");
			svStockQuantityFormat = IMain.getProperty("stockQuantityFormat");
			svIntPriceFormat = IMain.getProperty("intPriceFormat");
			svCashValueFormat = IMain.getProperty("cashValueFormat");
			svDateFormat = IMain.getProperty("dateFormat");
			svTimeFormat = IMain.getProperty("timeFormat");
			svDateTimeFormat = IMain.getProperty("dateTimeFormat");
			svSymbol = IMain.getProperty("DefaultCurrencySymbol")==null ? ""  : IMain.getProperty("DefaultCurrencySymbol");
			svSymbolPosition = IMain.getProperty("SymbolPosition")==null ? ""  : IMain.getProperty("SymbolPosition");
			svSingleSignOnPhase = !Utils.isNullStr(IMain
					.getProperty("SingleSignOnPhase")) ? IMain
					.getProperty("SingleSignOnPhase") : "0";
			svPasswordMinLength = !Utils.isNullStr(IMain
					.getProperty("PasswordMinLength")) ? Integer.parseInt(IMain
					.getProperty("PasswordMinLength")) : 6;
			svPasswordMaxLength = !Utils.isNullStr(IMain
					.getProperty("PasswordMaxLength")) ? Integer.parseInt(IMain
					.getProperty("PasswordMaxLength")) : 8;
	        svReDownloadDataTime = Integer.parseInt(IMain.getProperty("ReDownloadDataTime"));
	        svReDownloadDataDownloadTimes = Integer.parseInt(IMain.getProperty("ReDownloadDataDownloadTimes"));
	        svReDownloadDataEnable = Boolean.getBoolean(IMain.getProperty("ReDownloadDataEnable"));
	}

	protected WTradeRequest mvRequest = null;
	protected WTradeResponse mvResponse = null;
	protected DefaultDefMess mvDefaultDefMess = null;
	protected TPErrorHandling mvTPError;
	DefaultAction(WTradeRequest pvRequest, DefaultDefMess pvDefaultDefMess) {
	    setMvRequest(pvRequest);
	    setMvDefaultDefMess(pvDefaultDefMess);
	    mvTPError = new TPErrorHandling();
    }

	/**
	 * Returns the stock price decimal places
	 * @return a int to control how many decimal points will be keep
	 * when use svStockPriceDecimalPlaces to format
	 */
	public static int getSvStockPriceDecimalPlaces() {
		return svStockPriceDecimalPlaces;
	}

	/**
	 * Sets the stock price decimal places which received from ini
	 * @param pStockPriceDecimalPlaces The stock price decimal places which received from ini
	 */
	public static void setSvStockPriceDecimalPlaces(int pStockPriceDecimalPlaces) {
		svStockPriceDecimalPlaces = pStockPriceDecimalPlaces;
	}

	/**
	 * Returns the stock average price decimal places
	 * @return a int which will control how many decimal points will be keep
	 * when use svStockAvgPriceDecimalPlaces to format
	 */
	public static int getSvStockAvgPriceDecimalPlaces() {
		return svStockAvgPriceDecimalPlaces;
	}

	/**
	 * Sets the stock average price decimal places which received from ini
	 * @param pStockAvgPriceDecimalPlaces The stock average price decimal places which received from ini
	 */
	public static void setSvStockAvgPriceDecimalPlaces(int pStockAvgPriceDecimalPlaces) {
		svStockAvgPriceDecimalPlaces = pStockAvgPriceDecimalPlaces;
	}

	/**
	 * Returns the price decimal places
	 * @return a int which will control how many decimal points will be keep
	 * when use svIntPriceDecimalPlace to format
	 */
	public static int getSvIntPriceDecimalPlace() {
		return svIntPriceDecimalPlace;
	}

	/**
	 * Sets the price decimal places which received from ini
	 * @param pIntPriceDecimalPlace The price decimal places which received from ini
	 */
	public static void setSvIntPriceDecimalPlace(int pIntPriceDecimalPlace) {
		svIntPriceDecimalPlace = pIntPriceDecimalPlace;
	}

	/**
	 * Returns the cash value decimal places
	 * @return a int which will control how many decimal points will be keep
	 * when use svIntPriceDecimalPlace to format
	 */
	public static int getSvCashValueDecimalPlaces() {
		return svCashValueDecimalPlaces;
	}

	/**
	 * Sets the cash value decimal places which received from ini
	 * @param pCashValueDecimalPlaces The cash value decimal places which received from ini
	 */
	public static void setSvCashValueDecimalPlaces(int pCashValueDecimalPlaces) {
		svCashValueDecimalPlaces = pCashValueDecimalPlaces;
	}

	/**
	 * Returns language and country
	 * @return a String[]
	 *        [1] svLanguageAndCountry[0] is language such as "en", "zh", "vi"
	 *        [2] svLanguageAndCountry[1] is country such as "US", "CN", "TW", "vi"
	 */
	public static String[] getSvLanguageAndCountry() {
		return svLanguageAndCountry;
	}

	/**
	 * Sets language and country which received from session
	 * @param pLanguageAndCountry The language and country which received from session
	 */
	public static void setSvLanguageAndCountry(String[] pLanguageAndCountry) {
		svLanguageAndCountry = pLanguageAndCountry;
	}

	public static String getSvSymbol() {
		return svSymbol;
	}

	public static void setSvSymbol(String pSymbol) {
		if(svSymbol==null||svSymbol.trim().equals("")){
			svSymbol = "$";
		}
		svSymbol = pSymbol;
	}

	public static String getSvSymbolPosition() {
		if(svSymbolPosition==null||svSymbolPosition.trim().equals("")){
			svSymbolPosition = "B";
		}
		return svSymbolPosition;
	}

	public static void setSvSymbolPosition(String pSymbolPosition) {
		svSymbolPosition = pSymbolPosition;
	}

	//End Task #: - TTL-GZ-ZZW-00019 Wind Zhao 20091230 [iTrade R5] Numeric locale for Vietnam

	private String mvTheme;

	/**
	 * Returns the theme received from user interface.
	 * @return the theme
	 */
	public String getMvTheme()
	{
		return mvTheme;
	}

	/**
	 * Sets the theme which can be retrieved by user interface.
	 * @param pTheme  The theme received from user interface
	 */
	public void setMvTheme(String pTheme)
	{
		mvTheme = pTheme;
	}

	/**
	 * Returns the error result.
	 * @return the error result
	 */
	public String getMvErrorResult()
	{
		return mvErrorResult;
	}

	/**
	 * Sets the error result which can be retrieved by user interface.
	 * @param pErrorResult  The error result
	 */
	public void setMvErrorResult(String pErrorResult)
	{
		mvErrorResult = pErrorResult;
	}

	/**
	 * Returns the timely update.
	 * @return the timely update
	 */
	public String getMvTimelyUpdate()
	{
		return mvTimelyUpdate;
	}

	/**
	 * Sets the timely update.
	 * @param pTimelyUpdate  the timely update
	 */
	public void setMvTimelyUpdate(String pTimelyUpdate)
	{
		mvTimelyUpdate = pTimelyUpdate;
	}

	/**
	 * Returns the lang received from user interface.
	 * @return the lang
	 */
	public String getMvLang()
	{
		return mvLang;
	}

	/**
	 * Sets the lang.
	 * @param pLang  The lang received from user interface
	 */
	public void setMvLang(String pLang)
	{
		mvLang = pLang;
	}

	/**
	 * Returns the language received from user interface.
	 * @return the language
	 */
	public String getMvLanguage()
	{
		return mvLanguage;
	}

	/**
	 * Sets the language .
	 * @param pLanguage  The language received from user interface
	 */
	public void setMvLanguage(String pLanguage)
	{
		mvLanguage = pLanguage;
	}

	public String mvUserID;

	/**
	 * Returns the user id.
	 * @return the user id
	 */
	public String getMvUserID()
	{
		return mvUserID;
	}

	/**
	 * Sets the user id.
	 * @param mvUserID  the user id
	 */
	public void setMvUserID(String mvUserID)
	{
		this.mvUserID = mvUserID;
	}

	/**
	 * Returns the latest page clicked by user.
	 * @return the latest page clicked by user
	 */
	public String getMvLastAction()
	{
		return mvLastAction;
	}

	/**
	 * Sets the latest page which can be retrieved by user interface.
	 * @param pLastAction  the latest page clicked by user
	 */
	public void setMvLastAction(String pLastAction)
	{
		mvLastAction = pLastAction;
	}

	/**
	 * @param mvChildLastAction the mvChildLastAction to set
	 */
	public void setMvChildLastAction(String mvChildLastAction) {
		this.mvChildLastAction = mvChildLastAction;
	}

	/**
	 * @return the mvChildLastAction
	 */
	public String getMvChildLastAction() {
		return mvChildLastAction;
	}

//	/**
//	 * @Override method in ServletRequestAware, sets the request.
//	 * @param pRequest  the request received from client
//	 */
//	public void setServletRequest(HttpServletRequest pRequest)
//	{
//		mvRequest = pRequest;
//	}
//
//	/**
//	 * @Override method in ServletResponseAware, sets the response.
//	 * @param  pResponse  the response returned to client
//	 */
//	public void setServletResponse(HttpServletResponse pResponse)
//	{
//		mvResponse = pResponse;
//	}
//
	/**
	 * Returns the request received from client.
	 * @return the request received from client
	 */
//	public HttpServletRequest getServletRequest()
//	{
//		return this.mvRequest;
//	}

	/**
	 * Returns the response which returned to client.
	 * @return the response returned to client
	 */
//	public HttpServletResponse getServletResponse()
//	{
//		return this.mvResponse;
//	}

	/**
	 * Returns the key pairs.
	 * @return the object of map
	 */
//	public Map getMvSession()
//	{
//		return mvSession;
//	}

	/**
	 * Sets the key pairs.
	 * @param pSession  the object of map
	 */
//	public void setMvSession(Map pSession)
//	{
//		mvSession = pSession;
//	}

	/**
	 * Returns the error code.
	 * @return the error code
	 */
	public String getMvErrorCode()
	{
		return mvErrorCode;
	}

	/**
	 * Sets the error code which can be retrieved by user interface.
	 * @param pErrorCode  the error code
	 */
	public void setMvErrorCode(String pErrorCode)
	{
		mvErrorCode = pErrorCode;
	}

	/**
	 * Sets the message which can be retrieved by user interface.
	 * @param pMessage  the message
	 */
	public void setMvMessage(String pMessage)
	{
		this.mvMessage =pMessage;
	}

	/**
	 * Returns the message.
	 * @return the message
	 */
	public String getMvMessage()
	{
		return this.mvMessage;
	}

	/**
	 * Returns the single sign on phase.
	 * @return the single sign on phase
	 */
	public String getSingleSignOnPhase()
	{
		return svSingleSignOnPhase;
	}

	/**
	 * Constructor for the default action class
	 */
	public DefaultAction(Properties pIni, WTradeRequest request)
	{
//		mvSessionTracker = sessionTracker;
		_pIni = pIni;
		setMvRequest(request);
	}

	/**
	 * @Override method in Action, this method get format information
	 * and language information
	 */
	public void init()
	{
		if(!getMvErrorResult().equals("fail"))
		{}
		else
		{
			// return if mvRequest
			if(getMvRequest() == null) return;
            mvLanguage = "en_US";
//			if(mvRequest.getSession().getAttribute("mvLanguage")!= null&&!mvRequest.getSession().getAttribute("mvLanguage").toString().trim().equals("")){
//				mvLanguage = mvRequest.getSession().getAttribute("mvLanguage").toString();
//			} else {
//				/*
//				mvLanguage = IMain.getProperty("defaultLanguage");
//				if(mvLanguage == null || "".equals(mvLanguage)){
//					mvLanguage = "en_US";
//				}
//				*/
//				mvLanguage = "en_US";
//				mvRequest.getSession().setAttribute("mvLanguage", mvLanguage);
//			}

			// Start Task: Giang Tran 20100505: Build language array to display numeric
			if(IMain.getProperty("alwaysGetDecimalFormatUS") != null
					&& !IMain.getProperty("alwaysGetDecimalFormatUS").trim().toString().equals("true")) {
				svLanguageAndCountry = this.mvLanguage.split("_");
			} else {
				svLanguageAndCountry = new String[]{"en", "US"};
				/*
				if(mvLanguage == null || "".equals(mvLanguage)){
					svLanguageAndCountry = new String[]{"en", "US"};
				}else {
					svLanguageAndCountry = mvLanguage.split("_");
				}
				*/
			}
			// End Task: Giang Tran 20100505: Build language array to display numeric

			//End Task #: - TTL-GZ-ZZW-00019 Wind Zhao 20100106[iTrade R5] Numeric locale for Vietnam
			ActionContext.getContext().setLocale(new Locale(this.mvLanguage.split("_")[0],this.mvLanguage.split("_")[1]));
			//End Task #: - TTL-GZ-ZZW-00020 Wind Zhao 20091230 [iTrade R5] Full Internationalization and Localization

			//mvLastAction = this.mvRequest.getSession().getAttribute("INDEX").toString();
			//mvIndex = this.mvRequest.getSession().getAttribute("INDEX").toString();

			//Begin Task #: - TTL-GZ-ZZW-00019 Wind Zhao 20091230 [iTrade R5] Numeric locale for Vietnam
			getLocaleFormatInfo();
			//End Task #: - TTL-GZ-ZZW-00019 Wind Zhao 20091230 [iTrade R5] Numeric locale for Vietnam
            mvLang = "0";
//			if(this.mvRequest.getSession().getAttribute("lang") != null) {
//				mvLang = this.mvRequest.getSession().getAttribute("lang").toString();
//			} else {
//				mvLang = "0";
//				this.mvRequest.getSession().setAttribute("lang", mvLang);
//			}
            mvTheme = "ext-all";
			// set mvThemType
//			if(this.mvRequest.getSession().getAttribute("mvThemeType") != null) {
//				mvTheme = this.mvRequest.getSession().getAttribute("lang").toString();
//			} else {
//				mvTheme = "ext-all";
//				this.mvRequest.getSession().setAttribute("mvThemeType", mvTheme);
//			}

			//BEGIN TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
			_htmlTemplatePath = IMain.getProperty("htmlTemplatePath");
			_sNotAvailableUrl = IMain.getProperty("notAvailableUrl");
			_sNotCNAvailableUrl = IMain.getProperty("notCNAvailableUrl");
			_sNotTWAvailableUrl = IMain.getProperty("notTWAvailableUrl");
			//END TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization

	        String lvSkipResetTimeOutFunction = IMain
			.getProperty("SkipResetTimeOutFunction");
			svSkipResetTimeOutFunction = new Vector();
			if (!Utils.isNullStr(lvSkipResetTimeOutFunction)) {
				StringTokenizer lvResetTimeOutFunctionTokenList = new StringTokenizer(
						lvSkipResetTimeOutFunction, ",");
				while (lvResetTimeOutFunctionTokenList.hasMoreTokens()) {
					String lvFunctionName = lvResetTimeOutFunctionTokenList
							.nextToken();
					if (lvFunctionName != null) {
						svSkipResetTimeOutFunction.add(lvFunctionName.trim());
					}
				}
			}

			//BEGIN TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
			String lvUsingStockCodeFormatMarket = IMain.getProperty("UsingStockCodeFormatMarket");
			//END TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
			if (!Utils.isNullStr(lvUsingStockCodeFormatMarket)) {
				svUsingStockCodeFormatMarket = new Vector();
				StringTokenizer lvTokenList = new StringTokenizer(
						lvUsingStockCodeFormatMarket, ",");
				while (lvTokenList.hasMoreTokens()) {
					String lvMarketID = lvTokenList.nextToken();
					if (lvMarketID != null) {
						svUsingStockCodeFormatMarket.add(lvMarketID.trim());
					}
				}
			}
			mvTimelyUpdate = this.getMvRequest().getParameter("mvTimelyUpdate");
			if(mvTimelyUpdate==null||mvTimelyUpdate.equals("")){
				this.setMvTimelyUpdate("Y");
			}
//			if(enableSessionTimeUpdate() && !Utils.isNullStr(mvTimelyUpdate)&&mvTimelyUpdate.equals("Y")){
//				this.mvRequest.getSession().setAttribute("mvLastUpdateTime", new java.sql.Timestamp(System.currentTimeMillis()));
//			}
		}
		//return BundleLoader.initBundle(_sActionName);
	}

	protected boolean enableSessionTimeUpdate(){
		return true;
	}

	/**
	 * Returns the index .
	 * @return the index
	 */
	public String getMvIndex()
	{
		return mvIndex;
	}

	/**
	 * Sets the index
	 * @param pIndex  the index
	 */
	public void setMvIndex(String pIndex)
	{
		this.mvIndex = pIndex;
	}

	/**
	 * default constructor for default action
	 */
	public DefaultAction()
	{}

	/**
	 * Override method in ActionSupport
	 */
	public void prepare()
	{
		init();
	}

	/**
	 * This method set one attribute in session to identify whether password was saved,
	 * with a given session and a given status
	 * @param pSession  the session
	 * @param pIsSavePassword  if save password then true,else false
	 */
	protected void savePassword(HttpSession pSession, boolean pIsSavePassword)
	{
		if (pSession != null)
			pSession.setAttribute("isPassSaved", pIsSavePassword ? "Y" : "N");
		else
			throw new NullPointerException(
					"DefaultAction.savePassword pSession is null!");
	}

	/**
	 * This method dispatch to page with a given request , a given response
	 * and a given distination
	 * @param pRequest	 the request received from user interface
	 * @param pResponse  the response returned to user interface
	 * @param pDistination  the destination
	 */
	protected void dispatchToPage(HttpServletRequest pRequest,
			HttpServletResponse pResponse, String pDistination) {
		pDistination = StringReplacer.replace(pDistination, "../servlet",
				"/servlet");
		Log.println("Dispatch To Page Path: " + pDistination, Log.DEBUG_LOG);
		RequestDispatcher lvRequestDispatcher = pRequest
				.getRequestDispatcher(pDistination);

		try {
			lvRequestDispatcher.forward(pRequest, pResponse);
		} catch (IOException ex) {
			ex.printStackTrace();
			Log.println(ex, Log.ERROR_LOG);
		} catch (ServletException ex) {
			ex.printStackTrace();
			Log.println(ex, Log.ERROR_LOG);
		}
	}

	/**
	 * Gets all request data and returns them as a hashtable with a given request
	 *
	 * @param request the HttpServletRequest object
	 * @return the request data in a hashtable
	 */
	protected Hashtable getFormData(HttpServletRequest request) {
		Hashtable hFormData = new Hashtable();
		Enumeration eFormDataKeys = request.getParameterNames();
		String sKey;
		String[] sValues;
//		String sValue;
		StringBuffer lvValue;
		int i;

		while (eFormDataKeys.hasMoreElements()) {
			sKey = (String) eFormDataKeys.nextElement();
			sValues = request.getParameterValues(sKey);
//			sValue = new String(sValues[0]);
			lvValue = new StringBuffer();
			lvValue.append(sValues[0]);

			for (i = 1; i < sValues.length; i++) {
//				sValue += ";" + sValues[i];
				lvValue.append(";").append(sValues[i]);
			}
//			hFormData.put(sKey, sValue.trim());
			hFormData.put(sKey, lvValue.toString().trim());
		}
		return hFormData;
	}

	/**
	 * Shows a server error page when an unexpected error is encountered,
	 * with a given request and a given response
	 *
	 * @param request the HttpServletRequest object
	 * @param response the HttpServletResponse object
	 *
	 */
	protected void showServerErrorPage(HttpServletRequest request,
			HttpServletResponse response) {
		System.out.println("Showing Server Error Page: "
				+ new Date(System.currentTimeMillis()));
		Log.println("Showing Server Error Page: "
				+ new Date(System.currentTimeMillis()), Log.ERROR_LOG);
/*		String sAcceptLanguage = null; // request.getHeader("Accept-Language");
		String sAcceptCharset = null; // request.getHeader("Accept-Charset");

		sAcceptLanguage = LangUtil.getAcceptLanguage(0);
		sAcceptCharset = LangUtil.getAcceptCharset(0);*/
		String lvLangStr = request.getParameter("lang");
		try {
			try {
				if (("0").equals(lvLangStr)) {
					response.sendRedirect(_sNotAvailableUrl);
				} else if (("1").equals(lvLangStr)) {
					response.sendRedirect(_sNotTWAvailableUrl);
				} else if (("2").equals(lvLangStr)) {
					response.sendRedirect(_sNotCNAvailableUrl);
				} else {
					response.sendRedirect(_sNotAvailableUrl);
				}
			} catch (IOException e) {
				Log.println("[DefaultAction.showServerErrorPage(): Error = "
						+ e + "]", Log.ERROR_LOG);
			}

		} catch (MissingResourceException e) {
			Log.println("[DefaultAction.showServerErrorPage(): Error = " + e
					+ "]", Log.ERROR_LOG);
			try {
				if (lvLangStr.equals("0")) {
					response.sendRedirect(_sNotAvailableUrl);
				} else if (lvLangStr.equals("1")) {
					response.sendRedirect(_sNotTWAvailableUrl);
				} else if (lvLangStr.equals("2")) {
					response.sendRedirect(_sNotCNAvailableUrl);
				} else {
					response.sendRedirect(_sNotAvailableUrl);
				}
			} catch (IOException ioe) {
				Log.println("[DefaultAction.showServerErrorPage(): Error = "
						+ ioe + "]", Log.ERROR_LOG);
			}
		}
	}

	/**
	 * Returns the instrument with a given id of market, code of stock and a given id of market
	 * and a given code of stock
	 * @param pMarketID  the market id
	 * @param pStockCode  the stock code
	 * @return the instrument
	 */
	public static HKSInstrument getInstrument(String pMarketID, String pStockCode)
	{
		HKSInstrument lvHKSInstrument = new HKSInstrument();
		if (!Utils.isNullStr(pMarketID)) {
            WTradeLogger.print("AddOrRemoveStockAction","[ DefaultAction MarketID : " + pMarketID + "]");
//			Log.println("[ DefaultAction MarketID : " + pMarketID + "]",
//					Log.DEBUG_LOG);
			if (svUsingStockCodeFormatMarket != null
					&& svUsingStockCodeFormatMarket
							.contains((String) pMarketID)) {
				lvHKSInstrument = (HKSInstrument) TPManager.mvInstrumentVector
						.get(pMarketID.concat("|").concat(
								Utils.getFormattedInstrumentID(pStockCode)));
			} else {
				lvHKSInstrument = (HKSInstrument) TPManager.mvInstrumentVector
						.get(pMarketID.concat("|").concat(
								pStockCode.toUpperCase()));
			}
		}
		return lvHKSInstrument;
	}

	/**
	 * Returns the short name of instrument,with a given id of market and a given code of stock
	 * @param pMarketID  the market id
	 * @param pStockCode  the stock code
	 * @return the short name of instrument
	 */
	protected HKSInstrument getInstrumentShortName(String pMarketID,String pStockCode)
	{
		HKSInstrument lvHKSInstrument = new HKSInstrument();
		if (!Utils.isNullStr(pMarketID)) {
            WTradeLogger.print("AddOrRemoveStockAction","[ DefaultAction MarketID : " + pMarketID + "]");
//            Log.println("[ DefaultAction MarketID : " + pMarketID + "]",
//					Log.DEBUG_LOG);
			if (svUsingStockCodeFormatMarket != null
					&& svUsingStockCodeFormatMarket
							.contains((String) pMarketID)) {
				lvHKSInstrument = (HKSInstrument) TPManager.mvInstrumentNameVector
						.get(pMarketID.concat("|").concat(
								Utils.getFormattedInstrumentID(pStockCode)));
			} else {
				lvHKSInstrument = (HKSInstrument) TPManager.mvInstrumentNameVector
						.get(pMarketID.concat("|").concat(
								pStockCode.toUpperCase()));
			}
		}
		return lvHKSInstrument;
	}

	/**
	 * Returns the instrument with code of stock
	 * @param pStockCode  the stock code
	 * @return the instrument
	 */
	protected HKSInstrument getInstrument(String pStockCode)
	{
		return getInstrument(HKSConstant.MarketID, pStockCode);
	}

	//Begin Task #: - TTL-GZ-ZZW-00019 Wind Zhao 20091230 [iTrade R5] Numeric locale for Vietnam
	/**
	 * this method is get all kinds of format informations form ini such as svStockPriceDecimalPlacesLocale,
	 * svStockAvgPriceDecimalPlacesLocale, svIntPriceDecimalPlacesLocale, svCashValueDecimalPlacesLocale,
	 * svSymbolPosition,svSymbol.
	 * @author Wind Zhao
	 */
	protected void getLocaleFormatInfo(){
		try{
			//BEGIN TASK: TTL-GZ-XYL-00050 XuYuLong 20100111 [iTrade R5] Allow plugin.ini full Customization
			if(IMain.getProperty("stockPriceDecimalPlaces_"+this.mvLanguage)!=null&&!IMain.getProperty("stockPriceDecimalPlaces_"+this.mvLanguage).trim().equals("")){
				svStockPriceDecimalPlaces = Integer.parseInt(IMain.getProperty("stockPriceDecimalPlaces_"+this.mvLanguage));
			}else{
				svStockPriceDecimalPlaces = -1;
			}
		}catch(Exception e){
			svStockPriceDecimalPlaces = -1;
		}

		try{
			if(IMain.getProperty("stockAvgPriceDecimalPlaces_"+this.mvLanguage)!=null&&!IMain.getProperty("stockAvgPriceDecimalPlaces_"+this.mvLanguage).trim().equals("")){
				svStockAvgPriceDecimalPlaces = Integer.parseInt(IMain.getProperty("stockAvgPriceDecimalPlaces_"+this.mvLanguage));
			}else{
				svStockAvgPriceDecimalPlaces = -1;
			}
		}catch(Exception e){
			svStockAvgPriceDecimalPlaces = -1;
		}

		try{

			if(IMain.getProperty("intPriceDecimalPlaces_"+this.mvLanguage)!=null&&!IMain.getProperty("intPriceDecimalPlaces_"+this.mvLanguage).trim().equals("")){
				svIntPriceDecimalPlace = Integer.parseInt(IMain.getProperty("intPriceDecimalPlaces_"+this.mvLanguage));
			}else{
				svIntPriceDecimalPlace = -1;
			}
		}catch(Exception e){
			svIntPriceDecimalPlace = -1;
		}

		try{
			if(IMain.getProperty("cashValueDecimalPlaces_"+this.mvLanguage)!=null&&!IMain.getProperty("cashValueDecimalPlaces_"+this.mvLanguage).trim().equals("")){
				svCashValueDecimalPlaces = Integer.parseInt(IMain.getProperty("cashValueDecimalPlaces_"+this.mvLanguage));
			}else{
				svCashValueDecimalPlaces = -1;
			}
		}catch(Exception e){
			svCashValueDecimalPlaces = -1;
		}
	}
	//End Task #: - TTL-GZ-ZZW-00019 Wind Zhao 20091230 [iTrade R5] Numeric locale for Vietnam

	//BEGIN TASK: TTL-GZ-XYL-00045 XuYuLong 20091211 [iTrade R5] Plugin redesign and implement
	/**
	 * Initialize and get the function items that in system 'Main Menu Bar', include Base Functions and Plugin Functions
	 * @return List lvMainMenuBarFunctionList
	 * @author YuLong.Xu
	 */
	protected List getSysMainMenuItems(){
		/**
    	 * For base main menu bar
    	 */
		List lvMainMenuBarFunctionList = new ArrayList();
    	Object lvBaseFunctionInMainMenuBarCountObj = IMain.getProperty(HKSTag.BaseFunctionInMainMenuBarCount);
    	if(lvBaseFunctionInMainMenuBarCountObj != null){
    		int lvBaseFunctionInMainMenuBarCountInt = Integer.parseInt(lvBaseFunctionInMainMenuBarCountObj.toString());
    		for(int i = 1; i <= lvBaseFunctionInMainMenuBarCountInt; i ++){
    			FunctionBean lvFunctionBean = new FunctionBean();
    			lvFunctionBean.setMvID(IMain.getProperty(HKSTag.BaseFunctionInMainMenuBar + i + HKSTag.functionName).toString());
    			lvFunctionBean.setMvName(this.getText(lvFunctionBean.getMvID()));
    			lvFunctionBean.setMvIsEnable(IMain.getProperty(HKSTag.BaseFunctionInMainMenuBar + i + HKSTag.functionEnable).equals("true") ? true : false);
    			lvFunctionBean.setMvUrl(IMain.getProperty(HKSTag.BaseFunctionInMainMenuBar + i + HKSTag.functionHandler));

    			lvMainMenuBarFunctionList.add(lvFunctionBean);
    		}
    	}
    	/**
    	 * For plugin main menu bar
    	 */
    	if(ITradeServlet.getMvPluginITradeProperties() != null){
        	Object lvPluginFunctionInMainMenuBarCountObj = IMain.getProperty(HKSTag.PluginFunctionInMainMenuBarCount);
        	if(lvPluginFunctionInMainMenuBarCountObj != null){
        		int lvPluginFunctionInMainMenuBarCountInt = Integer.parseInt(lvPluginFunctionInMainMenuBarCountObj.toString());
        		for(int i = 1; i <= lvPluginFunctionInMainMenuBarCountInt; i ++){
        			FunctionBean lvFunctionBean = new FunctionBean();
        			lvFunctionBean.setMvID(IMain.getProperty(HKSTag.PluginFunctionInMainMenuBar + i + HKSTag.functionName).toString());
        			lvFunctionBean.setMvName(this.getText(lvFunctionBean.getMvID()));
        			lvFunctionBean.setMvIsEnable(IMain.getProperty(HKSTag.PluginFunctionInMainMenuBar + i + HKSTag.functionEnable).equals("true") ? true : false);
        			lvFunctionBean.setMvUrl(IMain.getProperty(HKSTag.PluginFunctionInMainMenuBar + i + HKSTag.functionHandler));

        			lvMainMenuBarFunctionList.add(lvFunctionBean);
        		}
        	}
    	}

    	return lvMainMenuBarFunctionList;
	}
	/**
	 * Initialize and get the function items that in system 'Application Menu', include Base Functions and Plugin Functions
	 * @author YuLong.Xu
	 */
	protected List getSysAppMenuItems(){
		/**
    	 * For base application menu
    	 */
		List lvApplicationMenuFunctionList = new ArrayList();
    	Object lvBaseFunctionInApplicationMenuCountObj = IMain.getProperty(HKSTag.BaseFunctionInApplicationMenuCount);
    	if(lvBaseFunctionInApplicationMenuCountObj != null){
    		int lvBaseFunctionInApplicationMenuCountInt = Integer.parseInt(lvBaseFunctionInApplicationMenuCountObj.toString());
    		for(int i = 1; i <= lvBaseFunctionInApplicationMenuCountInt; i ++){
    			FunctionBean lvFunctionBean = new FunctionBean();
    			lvFunctionBean.setMvID(IMain.getProperty(HKSTag.BaseFunctionInApplicationMenu + i + HKSTag.functionName).toString());
    			lvFunctionBean.setMvName(lvFunctionBean.getMvID());
    			lvFunctionBean.setMvDisplayName(this.getText(lvFunctionBean.getMvID()));
    			lvFunctionBean.setMvIsEnable(IMain.getProperty(HKSTag.BaseFunctionInApplicationMenu + i + HKSTag.functionEnable).equals("true") ? true : false);
    			lvFunctionBean.setMvUrl(IMain.getProperty(HKSTag.BaseFunctionInApplicationMenu + i + HKSTag.functionHandler));
    			lvFunctionBean.setMvICON(IMain.getProperty(HKSTag.BaseFunctionInApplicationMenu + i + HKSTag.functionIconPath));

    			//EEGIN TASK:#TTL-GZ-ZYZ-00005 Yingzhi Zhang 06-JAN-2010 [iTrade R5] Bug fix: Black theme slow loading and bugs in Firefox
    			lvFunctionBean.setMvJSPath("");
    			lvFunctionBean.setMvLanguageSourceJSPath("");
    			//EEGIN TASK:#TTL-GZ-ZYZ-00005 Yingzhi Zhang 06-JAN-2010 [iTrade R5] Bug fix: Black theme slow loading and bugs in Firefox
    			lvApplicationMenuFunctionList.add(lvFunctionBean);
    		}
    	}
    	/**
    	 * For plugin application menu
    	 */
    	if(ITradeServlet.getMvPluginITradeProperties() != null){
    		Object lvPluginFunctionInApplicationMenuCountObj = IMain.getProperty(HKSTag.PluginFunctionInApplicationMenuCount);
        	if(lvPluginFunctionInApplicationMenuCountObj != null){
        		int lvPluginFunctionInApplicationMenuCountInt = Integer.parseInt(lvPluginFunctionInApplicationMenuCountObj.toString());
        		for(int i = 1; i <= lvPluginFunctionInApplicationMenuCountInt; i ++){
        			FunctionBean lvFunctionBean = new FunctionBean();
        			lvFunctionBean.setMvID(IMain.getProperty(HKSTag.PluginFunctionInApplicationMenu + i + HKSTag.functionName).toString());
        			lvFunctionBean.setMvName(lvFunctionBean.getMvID());
        			lvFunctionBean.setMvDisplayName(this.getText(lvFunctionBean.getMvID()));
        			lvFunctionBean.setMvIsEnable(IMain.getProperty(HKSTag.PluginFunctionInApplicationMenu + i + HKSTag.functionEnable).equals("true") ? true : false);
        			lvFunctionBean.setMvUrl(IMain.getProperty(HKSTag.PluginFunctionInApplicationMenu + i + HKSTag.functionHandler));
        			lvFunctionBean.setMvICON(IMain.getProperty(HKSTag.PluginFunctionInApplicationMenu + i + HKSTag.functionIconPath));
        			lvFunctionBean.setMvJSPath(IMain.getProperty(HKSTag.PluginFunctionInApplicationMenu + i + HKSTag.functionJSPath));
        			lvFunctionBean.setMvLanguageSourceJSPath(IMain.getProperty(HKSTag.PluginFunctionInApplicationMenu + i + HKSTag.functionLanguageSourceJSPath));

        			lvApplicationMenuFunctionList.add(lvFunctionBean);
        		}
        	}
    	}

    	return lvApplicationMenuFunctionList;
	}
	//END TASK: TTL-GZ-XYL-00045 XuYuLong 20091211 [iTrade R5] Plugin redesign and implement
	public abstract STATUS exec();

    public WTradeRequest getMvRequest() {
        return mvRequest;
    }

    public void setMvRequest(WTradeRequest mvRequest) {
        this.mvRequest = mvRequest;
    }
    
    public DefaultDefMess getMvDefaultDefMess() {
		return mvDefaultDefMess;
	}

	public void setMvDefaultDefMess(DefaultDefMess mvDefaultDefMess) {
		this.mvDefaultDefMess = mvDefaultDefMess;
	}
}
