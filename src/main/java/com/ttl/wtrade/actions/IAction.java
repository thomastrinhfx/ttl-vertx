package com.ttl.wtrade.actions;


public interface IAction {
    void exec();
}
