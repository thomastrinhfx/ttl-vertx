package com.ttl.wtrade.verticles;

import com.ttl.old.itrade.hks.txn.HKSAddOrRemoveStockTxn;
import com.ttl.old.itrade.mds.action.StockInfoMdstxn;
import com.ttl.old.itrade.util.Log;
import com.ttl.wtrade.utils.WTradeLogger;
import com.ttl.wtrade.utils.ebmessages.IEBMessage;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Context;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;

public class WatchStockTPWorker extends AbstractVerticle {


    @Override
    public void init(Vertx vertx, Context context) {
        // TODO Auto-generated method stub
        super.init(vertx, context);
    }

    @SuppressWarnings("static-access")
    @Override
    public void start() {
        WTradeLogger.print("TPWorker", "Start TP connect in: " + Thread.currentThread());
        Log.println("[TPWorker] Starting in " + Thread.currentThread().getName(), Log.DEBUG_LOG);
        EventBus eventBus = vertx.eventBus();
        eventBus.consumer("TPMessage.data", message -> {    // Get evetn bus get client id get stock list
            Log.println("[TPWorker] Consuming data in " + Thread.currentThread().getName(), Log.DEBUG_LOG);
            WTradeLogger.print(this.getClass().getName(), String.format("Comsuming data in %s", Thread.currentThread().getName()));

            //Extract info from message
            JsonObject lvEBMessage = (JsonObject) message.body();
            String lvClientID = lvEBMessage.getString(IEBMessage.CLIENT_ID);

            //Create new txnObject
            HKSAddOrRemoveStockTxn txn = new HKSAddOrRemoveStockTxn(lvClientID, "", "", "QUERY", "1");
            txn.process();
            WTradeLogger.print(this.getClass().getName(), "Server will send data for list stock:" + txn.getMvInstrumentList());
//            System.out.println("Server will send data for list stock:" + txn.getMvInstrumentList());
            if (txn.getMvInstrumentList() != null) {
                //Instrument list: String("ACB, MCA, ...")
                for (String instrumentId : txn.getMvInstrumentList().split(",")) {
                    StockInfoMdstxn.getInstance().addMvStockSessionMap(instrumentId, lvClientID);
//                    eventBus.send("AORMessage", new EBMessage(mess.getValue(), "1", instrumentId));
                    eventBus.publish("MDSStockID.data", instrumentId);
                }
            }
            message.reply("Success!");
        });

    }

    @Override
    public void stop() {
        Log.println("[TPWorker] Stop " + Thread.currentThread().getName(), Log.DEBUG_LOG);
    }
}
