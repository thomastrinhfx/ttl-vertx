package com.ttl.wtrade.verticles;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import com.ttl.wtrade.actions.ActionFactory;
import com.ttl.wtrade.actions.DefaultAction;
import com.ttl.wtrade.actions.DefaultAction.STATUS;
import com.ttl.wtrade.defmess.DefaultDefMess;
import com.ttl.wtrade.utils.config.ActionConfig;
import com.ttl.wtrade.utils.TPPool;
import com.ttl.wtrade.utils.WTradeLogger;
import com.ttl.wtrade.utils.WTradeRequest;
import com.ttl.wtrade.utils.config.WorkerConfig;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;

public class ActionWorker extends AbstractVerticle {
    private String workerName = null;
    private WorkerConfig workerConfig = null;
    private DefaultDefMess defaultDefMess = null;
    private HashMap<String, ActionConfig> actionMap = null;

    public void start() {
        setWorkerName(config().getString("WorkerName"));
        EventBus eb = vertx.eventBus();
        eb.consumer(this.getWorkerName() + "_Config", this::onConfigSetting);
        eb.consumer(this.getWorkerName(), this::onMessage);
    }

    private void onConfigSetting(Message configMessage) {
        setWorkerConfig((WorkerConfig) configMessage.body());
        WTradeLogger.print(getWorkerName(), "receive new action CONFIG: " + getWorkerConfig().getName());
        TPPool.add(getWorkerConfig());
        setActionMap((HashMap) getWorkerConfig().getActions());
        ActionFactory.generateFactory(getActionMap());
        configMessage.reply("");
    }

    private void onMessage(Message requestMessage) {
        WTradeLogger.print(getWorkerName(), "receive new action REQUEST");
        WTradeRequest request = (WTradeRequest) requestMessage.body();
        
        if (request==null /*|| !request.validate()*/) {
            WTradeLogger.print("ActionWorker", String.format("Invalid request: %s", request.printContent()), WTradeLogger.ERROR_MESSAGE);
        } else {
            WTradeLogger.print("ActionWorker", String.format("Received a request from %s", request.getMvClientID()));
            try {
            	getActionMap().forEach((actionName, actionConfig) -> {
            		if(actionName.equals(request.getMvAction())){
            			DefaultDefMess tempDefMess = null;
						try {
							tempDefMess = (DefaultDefMess) Class.forName(actionConfig.getMessDef()).newInstance();
						} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
            			setDefaultDefMess(tempDefMess);
            		}
            	});
                DefaultAction willBeExecAction = ActionFactory.getActionInstance(request.getMvAction(), request, getDefaultDefMess());
                STATUS status = willBeExecAction.exec();
                switch(status){
	                case ERROR:
	                	requestMessage.reply("Something wrong happened");
	                	break;
	                case NORMAL:
	                	requestMessage.reply("Message sent");
	                	break;
                }
            } catch (InstantiationException e) {
                WTradeLogger.print("ActionWorker", "InstantiationException: " + e.getMessage(), WTradeLogger.ERROR_MESSAGE);
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                WTradeLogger.print("ActionWorker", "IllegalAccessException: " + e.getMessage(), WTradeLogger.ERROR_MESSAGE);
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                WTradeLogger.print("ActionWorker", "ClassNotFoundException: " + e.getMessage(), WTradeLogger.ERROR_MESSAGE);
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }
    
    

    public HashMap<String, ActionConfig> getActionMap() {
		return actionMap;
	}

	public void setActionMap(HashMap<String, ActionConfig> actionMap) {
		this.actionMap = actionMap;
	}

	public String getWorkerName() {
        return workerName;
    }

    public void setWorkerName(String workerName) {
        this.workerName = workerName;
    }

    public WorkerConfig getWorkerConfig() {
        return workerConfig;
    }

    public void setWorkerConfig(WorkerConfig workerConfig) {
        this.workerConfig = workerConfig;
    }

	public DefaultDefMess getDefaultDefMess() {
		return defaultDefMess;
	}

	public void setDefaultDefMess(DefaultDefMess defaultDefMess) {
		this.defaultDefMess = defaultDefMess;
	}
    
    
}
