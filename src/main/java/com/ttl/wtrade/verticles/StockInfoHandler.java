package com.ttl.wtrade.verticles;

import org.atmosphere.cpr.ApplicationConfig;
import org.atmosphere.vertx.VertxAtmosphere;

import com.ttl.old.itrade.StockInfoSocket;

import io.vertx.core.Vertx;
import io.vertx.ext.web.RoutingContext;

public class StockInfoHandler {
	private Vertx vertx;
	
	public StockInfoHandler(Vertx pvertx) {
		this.vertx = pvertx;
	}
	
	public void handleGetStockInfo(RoutingContext context) {
		VertxAtmosphere.Builder b = new VertxAtmosphere.Builder();
		b.resource(StockInfoSocket.class).httpServer(this.vertx.createHttpServer())
											.url("ITradePushServer/StockInfo/Get/:clientId")
											.webroot(System.getProperty("basedirectory")+"/webapp/")
											.initParam(ApplicationConfig.WEBSOCKET_CONTENT_TYPE, "application/json")
											.vertx(vertx)
											.build();
		vertx.createHttpServer().requestHandler(req -> req.response().end("Create StockInfoPublisher WebSocket"));
		
	}
	
	public void initAutionInSharedData(RoutingContext context) {
		
	}
}
