package com.ttl.wtrade.verticles;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.mds.action.BaseAction;
import com.ttl.old.itrade.mds.connector.MDSConnection;
import com.ttl.old.itrade.mds.connector.MDSConnection.EServerStatus;
import com.ttl.old.itrade.mds.connector.MDSConnection.RequestMsgType;
import com.ttl.old.itrade.util.Log;
import com.ttl.wtrade.utils.WTradeLogger;
import com.txtech.mds.msg.MsgExchangeID;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Context;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;

public class MDSWorker extends AbstractVerticle {
    private MDSConnection mds = null;

    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);
        this.vertx =vertx;
    }

    @Override
    public void start() {
        mds = MDSConnection.getInstance();
        if (mds.ServerStatus() != EServerStatus.CONNECTED) {
            mds.start(new BaseAction());
        }

        WTradeLogger.print("MDSWorker", "Starting in " + Thread.currentThread().getName());
        Log.println("[MDSWorker] Starting in " + Thread.currentThread().getName(), Log.DEBUG_LOG);

        EventBus eventBus = vertx.eventBus();

        eventBus.<String>consumer("MDSStockID.data", message -> {
            WTradeLogger.print("MDSWorker", "Consuming data in " + Thread.currentThread().getName());
//            System.out.println("[MDSWorker] Consuming data in " + Thread.currentThread().getName());
            Log.println("[MDSWorker] Consuming data in " + Thread.currentThread().getName(), Log.DEBUG_LOG);

            String lvStockID = message.body();

            mds.subscribe(RequestMsgType.Security_Definition, MsgExchangeID.valueOf("HN"), lvStockID);
            mds.subscribe(RequestMsgType.AggregateOrddrBook, MsgExchangeID.valueOf("HN"), lvStockID);
            WTradeLogger.print("MDSWorker",String.format("[MDSWorker | %s] Success Subscribe Message: %s", Thread.currentThread().getName(),lvStockID));
        });

    }

    @Override
    public void stop() {
        WTradeLogger.print("MDSWorker", "========MDSWorker stopped==============");
    }
}
