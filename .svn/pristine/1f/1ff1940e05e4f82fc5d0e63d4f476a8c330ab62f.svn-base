package com.ttl.old.itrade.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.ServletOutputStream;

/**
 * The HtmlTemplate class defined methods that are object make print or read to specified place.
 * @author
 *
 */
public class HtmlTemplate extends Object
{
	private Vector  _vTemplate = null;
	public String   nameOfFile = "";

	/**
	 * Constructor for HtmlTemplate class with file name.
	 * @param pFilename The file name.
	 * @throws IOException If you can not create a html template.
	 */
	public HtmlTemplate(String pFilename) throws IOException
	{
		readInFile(pFilename);
		nameOfFile = pFilename;
	}

	/**
	 * Constructor for HtmlTemplate class with url.
	 * @param pUrl The will read url.
	 * @throws IOException If you can not create a html template.
	 */
	public HtmlTemplate(URL pUrl) throws IOException
	{
		readInURL(pUrl);
	}

	/**
	 * This method to print data with specific key.
	 * @param pOut The Character output stream.
	 * @param pKeys The key of string.
	 * @param pData The specified String array.
	 * @throws IOException If an I/O error.
	 */
	public void print(PrintWriter pOut, String pKeys[], String pData[]) throws IOException
	{
		if (_vTemplate == null)
		{
			return;
		}
		Enumeration E = _vTemplate.elements();
		String		sLineIn;
		int			x, c1 = 0;
		while (E.hasMoreElements())
		{
			sLineIn = (String) E.nextElement();

			for (x = 0; x < pKeys.length; x++)
			{
				c1 = sLineIn.indexOf(pKeys[x]);
				while (c1 != -1)
				{
					sLineIn = sLineIn.substring(0, c1) + pData[x] + sLineIn.substring(c1 + pKeys[x].length(), sLineIn.length());
					c1 = sLineIn.indexOf(pKeys[x], c1 + 1);
				}
			}
			pOut.println(sLineIn);
		}
		pOut.flush();
	}

	/**
	 * This method to print data with specific charset. 
	 * @param pOut The Character output stream.
	 * @param pData The specified map data.
	 * @param pCharset The coding type.
	 * @throws IOException If an I/O error.
	 */
	public void print(PrintWriter pOut, Hashtable pData, String pCharset) throws IOException
	{
		if (_vTemplate == null)
		{
			return;
		}
		Enumeration fileElements = _vTemplate.elements();
		Enumeration keys;
		String		sLineIn;
		String		sKey;
		int			i = 0;

		while (fileElements.hasMoreElements())
		{
			sLineIn = (String) fileElements.nextElement();

			keys = pData.keys();
			while (keys.hasMoreElements())
			{
				sKey = (String) keys.nextElement();
				i = sLineIn.indexOf(sKey);
				while (i != -1)
				{
					sLineIn = sLineIn.substring(0, i) + pData.get(sKey) + sLineIn.substring(i + sKey.length(), sLineIn.length());
					i = sLineIn.indexOf(sKey, i + 1);
				}
			}
			//sLineIn = new String(sLineIn.getBytes("CP1252"), pCharset);
			pOut.println(sLineIn);
			pOut.flush();
		}
	}

	/**
	 * This method to print data in servlet.
	 * @param pOut The servlet output stream.
	 * @param pData The specified map data.
	 * @throws IOException If an I/O error.
	 */
	public void print(ServletOutputStream pOut, Hashtable pData) throws IOException
	{
		if (_vTemplate == null)
		{
			return;
		}

		Enumeration fileElements = _vTemplate.elements();
		Enumeration keys;
		String		sLineIn;
		String		sKey;
		int			i = 0;

		while (fileElements.hasMoreElements())
		{
			sLineIn = (String) fileElements.nextElement();

			keys = pData.keys();
			while (keys.hasMoreElements())
			{
				sKey = (String) keys.nextElement();
				i = sLineIn.indexOf(sKey);
				while (i != -1)
				{
					sLineIn = sLineIn.substring(0, i) + pData.get(sKey) + sLineIn.substring(i + sKey.length(), sLineIn.length());
					i = sLineIn.indexOf(sKey, i + 1);
				}
			}
			if (pOut != null)
			{
				pOut.println(sLineIn);
				pOut.flush();
			}
			else
			{
				System.out.println("out is null");
			}
		}
	}

	/**
	 * This method make map data to format String.
	 * @param pData The specified map data.
	 * @return format string.
	 */
	public String formatToString(Hashtable pData)
	{
		if (_vTemplate == null)
		{
			return null;

		}
		Enumeration		fileElements = _vTemplate.elements();
		Enumeration		keys;
		String			sLineIn;
		String			sKey;
		int				i = 0;

		StringBuffer	sBuf = new StringBuffer(2000);

		while (fileElements.hasMoreElements())
		{
			sLineIn = (String) fileElements.nextElement();

			keys = pData.keys();
			while (keys.hasMoreElements())
			{
				sKey = (String) keys.nextElement();
				i = sLineIn.indexOf(sKey);
				while (i != -1)
				{
					sLineIn = sLineIn.substring(0, i) + pData.get(sKey) + sLineIn.substring(i + sKey.length(), sLineIn.length());
					i = sLineIn.indexOf(sKey, i + 1);
				}
			}

			sBuf.append(sLineIn);
		}
		return sBuf.toString();
	}

	/**
	 * This method read object to file.
	 * @param pFilename The file name.
	 * @throws IOException If an I/O error.
	 */
	private void readInFile(String pFilename) throws IOException
	{

		// - Reads in the template file
		_vTemplate = new Vector(30, 5);
		BufferedReader	file = new BufferedReader(new InputStreamReader(new FileInputStream(pFilename), "UTF-8"));

		String				sLine;
		while ((sLine = file.readLine()) != null)
		{

//			sLine = sLine.substring(0, sLine.length());
			if (sLine.indexOf("\r") != -1)
			{
				sLine = sLine.substring(0, sLine.length() - 1);
			}
//			sLine = new String(sLine.getBytes("CP1252"));
			_vTemplate.addElement(sLine);
		}

		file.close();
	}

	/**
	 * This method read object to specified url.
	 * @param pHost The specified url.
	 * @throws IOException If an I/O error.
	 */
	private void readInURL(URL pHost) throws IOException
	{

		// - Reads in the template file
		BufferedReader  inFile = new BufferedReader(new InputStreamReader(pHost.openStream()));
		_vTemplate = new Vector(30, 5);

		String  sLine;
		while ((sLine = inFile.readLine()) != null)
		{
			sLine = sLine.substring(0, sLine.length());
			_vTemplate.addElement(sLine);
		}
		inFile.close();
	}

	/**
	 * Gets a vector template.
	 * @return The vector template.
	 */
	public Vector getTemplate()
	{
		return _vTemplate;
	}
}
