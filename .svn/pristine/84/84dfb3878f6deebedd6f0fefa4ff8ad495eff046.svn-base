package com.ttl.old.itrade.hks.bean;

/**
 * The HKSMarketBean class define variables that to save values 
 * for action 
 * @author Wind Zhao
 *
 */

public class HKSMarketBean {
	private int mvPages;
	private int mvTotalRecord;
	private int mvCurrentPage;
	private String mvMarketURL;
	private String mvMarketId;
	private String mvEnableAutoRefresh;
	private String mvAutoRefreshRate;
	private String mvIfShowEnableRegisterClient;
	private String mvIfShowDisableRegisterClient;
	private String mvStockList;
	private String mvNewQuery;
	private String mvAjaxRefreshTime;
	private String mvMarketIDList;
	//Begin Task #: - TTL-GZ-ZZW-00004 Wind Zhao 20090911
	private String mvErrorMsg;
	
	/**
     * This method returns the error message.
     * @return the error message.
     */
	public String getMvErrorMsg() {
		return mvErrorMsg;
	}
	
	/**
     * This method sets the error message.
     * @param pErrorMsg The error message.
     */
	public void setMvErrorMsg(String pErrorMsg) {
		mvErrorMsg = pErrorMsg;
	}
	
	/**
     * This method returns the current page.
     * @return the current page.
     */
	//Begin Task #: - TTL-GZ-ZZW-00004 Wind Zhao 20090911
	public int getMvCurrentPage() {
		return mvCurrentPage;
	}
	
	/**
     * This method sets the current page.
     * @param pCurrentPage The current page.
     */
	public void setMvCurrentPage(int pCurrentPage) {
		mvCurrentPage = pCurrentPage;
	}
	
	/**
     * This method returns the market id list.
     * @return the market id list.
     */
	public String getMvMarketIDList() {
		return mvMarketIDList;
	}
	
	/**
     * This method sets the market id list.
     * @param pMarketIDList The market id list.
     */
	public void setMvMarketIDList(String pMarketIDList) {
		mvMarketIDList = pMarketIDList;
	}
	
	/**
     * This method returns the pages of the market date record.
     * @return the pages of the market date record.
     */
	public int getMvPages() {
		return mvPages;
	}
	
	/**
     * This method sets the pages of the market date record.
     * @param pPages The pages of the market date record.
     */
	public void setMvPages(int pPages) {
		mvPages = pPages;
	}
	
	/**
     * This method returns the total record.
     * @return the total record.
     */
	public int getMvTotalRecord() {
		return mvTotalRecord;
	}
	
	/**
     * This method sets the total record.
     * @param pTotalRecord The total record.
     */
	public void setMvTotalRecord(int pTotalRecord) {
		mvTotalRecord = pTotalRecord;
	}
	
	/**
     * This method returns the ajax refresh time.
     * @return the ajax refresh time.
     */
	public String getMvAjaxRefreshTime() {
		return mvAjaxRefreshTime;
	}
	
	/**
     * This method sets the ajax refresh time.
     * @param pAjaxRefreshTime The ajax refresh time.
     */
	public void setMvAjaxRefreshTime(String pAjaxRefreshTime) {
		mvAjaxRefreshTime = pAjaxRefreshTime;
	}
	
	/**
     * This method returns the enable which control register client's enable or disable.
     * @return the enable which control register client's enable or disable.
     */
	public String getMvIfShowEnableRegisterClient() {
		return mvIfShowEnableRegisterClient;
	}
	
	/**
     * This method sets the enable which control register client's enable or disable.
     * @param pIfShowEnableRegisterClient The enable which control register client's enable or disable.
     */
	public void setMvIfShowEnableRegisterClient(String pIfShowEnableRegisterClient) {
		mvIfShowEnableRegisterClient = pIfShowEnableRegisterClient;
	}
	
	/**
     * This method returns the enable of disable for register client.
     * @return the enable of disable for register client.
     */
	public String getMvIfShowDisableRegisterClient() {
		return mvIfShowDisableRegisterClient;
	}
	
	/**
     * This method sets the enable of disable for register client.
     * @param pIfShowDisableRegisterClient The enable of disable for register client.
     */
	public void setMvIfShowDisableRegisterClient(
			String pIfShowDisableRegisterClient) {
		mvIfShowDisableRegisterClient = pIfShowDisableRegisterClient;
	}
	
	/**
     * This method returns the new query.
     * @return the new query.
     */
	public String getMvNewQuery() {
		return mvNewQuery;
	}
	
	/**
     * This method sets the new query.
     * @param pNewQuery The new query.
     */
	public void setMvNewQuery(String pNewQuery) {
		mvNewQuery = pNewQuery;
	}
	
	/**
     * This method returns the stock list.
     * @return the stock list.
     */
	public String getMvStockList() {
		return mvStockList;
	}
	
	/**
     * This method sets the stock list.
     * @param pStockList The stock list.
     */
	public void setMvStockList(String pStockList) {
		mvStockList = pStockList;
	}
	
	/**
     * This method returns the market url.
     * @return the market url.
     */
	public String getMvMarketURL() {
		return mvMarketURL;
	}
	
	/**
     * This method sets the market url.
     * @param pMarketURL The market url.
     */
	public void setMvMarketURL(String pMarketURL) {
		mvMarketURL = pMarketURL;
	}
	
	/**
     * This method returns the market id.
     * @return the market id.
     */
	public String getMvMarketId() {
		return mvMarketId;
	}
	
	/**
     * This method sets the market id.
     * @param pMarketId The market id.
     */
	public void setMvMarketId(String pMarketId) {
		mvMarketId = pMarketId;
	}
	
	/**
     * This method returns the enable of auto refresh.
     * @return the enable of auto refresh.
     */
	public String getMvEnableAutoRefresh() {
		return mvEnableAutoRefresh;
	}
	
	/**
     * This method sets the enable of auto refresh.
     * @param pEnableAutoRefresh The enable of auto refresh.
     */
	public void setMvEnableAutoRefresh(String pEnableAutoRefresh) {
		mvEnableAutoRefresh = pEnableAutoRefresh;
	}
	
	/**
     * This method returns the auto refresh rate.
     * @return the auto refresh rate.
     */
	public String getMvAutoRefreshRate() {
		return mvAutoRefreshRate;
	}
	
	/**
     * This method sets the auto refresh rate.
     * @param pAutoRefreshRate auto refresh rate.
     */
	public void setMvAutoRefreshRate(String pAutoRefreshRate) {
		mvAutoRefreshRate = pAutoRefreshRate;
	}
}
