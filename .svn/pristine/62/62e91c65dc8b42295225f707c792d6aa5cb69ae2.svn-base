package com.ttl.old.itrade.comet.cometManagement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ttl.old.itrade.comet.dataInfo.StockWatchInfo;
import com.ttl.old.itrade.comet.eventHandler.caching.MarketDataCachingManager;
import com.ttl.old.itrade.comet.eventHandler.caching.StockWatchDataCachingManager;
import com.ttl.old.itrade.comet.scheduler.MainScheduler;
import com.ttl.old.itrade.comet.utils.Constants;
import com.ttl.old.itrade.comet.utils.Utils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


public class BrowserManager {
	private static final Logger logger = LogManager.getLogger(BrowserManager.class);	
	/**
	 * Validation cycle.
	 */
	private static final int VALIDATION_CYCLE = 60000; //1 minute
	/**
	 * Number of workers (default 5).
	 */
	private static int numOfWorkers = 5;
	/**
	 * The offset to exec balancing number of clients for each worker.
	 */
	private static int balanceOffset = 20;
	
	/**
	 * List of RequestHandlerWorker.
	 */	
	private static final List<RequestWorker> workers = new ArrayList<RequestWorker>();	
	/**
	 * Validation clients counter.			
	 */
	private static int validationCounter = VALIDATION_CYCLE;

	private static int currentHHmm = 0;
	
	/**
	 * Initialize handler based on a web context.
	 * 
	 * @param pNumWorker int
	 */

	public static void init(final int pNumWorker) {
		try {
			numOfWorkers = pNumWorker;			
			for (int i = 1; i <= numOfWorkers; i++) {
				final RequestWorker w = new RequestWorker(i);
				workers.add(w);
				w.start();
			}			
			//Tick cycle (30s)
			final int tickCycle = 1000*30;
			//Schedule to validate clients and synchronize time
			MainScheduler.schedule(new Runnable() {
				public final void run() {
					checkToResponeTime();
					checkToValidate(tickCycle);
				}
			}, tickCycle);			
		} catch (Exception ex) {
			logger.error("Init comet event handler error", ex);
		}
	}

	private static void checkToResponeTime() {
		try {
			final Calendar calendar = Calendar.getInstance();					
			final int currentTime = 100 * calendar.get(Calendar.HOUR_OF_DAY) + calendar.get(Calendar.MINUTE);
			if (currentTime != currentHHmm) {
				currentHHmm = currentTime;
				final String serverTime = Utils.getServerTimeInJson();
				for (RequestWorker w : workers) {
					w.responseServerTime(serverTime);														
				}
			}
		} catch (Exception ex) {
			logger.error("Response time exception", ex);
		} catch (Error er) {
			logger.error("Response time error", er);
		}
	}
	
	/**
	 * Check to validate clients.
	 * 
	 * @param tickCycle int
	 */
	private static void checkToValidate(final int tickCycle) {
		try {
			//Check to validate clients
			validationCounter -= tickCycle;
			if (validationCounter <= 0) {
				int numInvalidClients = 0;
				for (RequestWorker w : workers) {
					numInvalidClients += w.validateClients();						
				}						
				validationCounter = VALIDATION_CYCLE;
				if (numInvalidClients >= balanceOffset) {
					balanceClientsForWorkers();
				}
				if (logger.isDebugEnabled())
					logger.debug(String.format("Number of invalid clients is %d", numInvalidClients));
			}
		} catch (Exception ex) {
			logger.error("Validate exception", ex);
		} catch (Error er) {
			logger.error("Validate error", er);
		}
	}	
	/**
	 * Destroy.
	 */
	public static void destroy() {
		logger.info("Begin destroy");
		//Stop workers
		for (RequestWorker w : workers)
			w.stop();
		logger.info("End destroy");
	}
	/**
	 * Register a client. 
	 * 
	 * @param account String
	 * @param symbols String contains symbols separated by comma. For example, 'ACB,AAM'
	 */
	public static void register(final String account, final HttpSession session , final int sessionTimeout, final String symbols, final HttpServletResponse response) {
		if (logger.isDebugEnabled())
			logger.debug(String.format("Register for client %s with selected symbols %s",
					account, symbols));
		final List<String> listSymbols = new ArrayList<String>();
		if (symbols != null && symbols.length() > 0) {
			final String[] array = symbols.split(",");
			for (String s : array)
				listSymbols.add(s);
		}
		RequestWorker receivableWorker = null;
		int numClient = -1;
		Browser cClient = null;
		for (RequestWorker w : workers) {
			cClient = w.getClient(account);
			// check session
			boolean sessionChanged = cClient != null? session != cClient.getMvSession() : false;
			if(cClient != null){
				sessionChanged = session != cClient.getMvSession();
				if(sessionChanged){
					// remove existed registered
					BrowserManager.unregisterByAccount(cClient.getAccount());
					cClient = null;
				}
			}
			
			if (cClient != null) {
				//Update selected symbols
				cClient.setSymbolInfo(listSymbols);
				break;
			} else {
				if (numClient == -1) {
					numClient = w.getNumClients();
					receivableWorker = w;
				} else if (numClient > w.getNumClients()) {
					numClient = w.getNumClients();
					receivableWorker = w;
				}
			}
		}		
		if (cClient == null) {			
			cClient = new Browser(account, session, sessionTimeout);
			cClient.setSymbolInfo(listSymbols);
			if (receivableWorker != null)
				receivableWorker.resgisterClient(cClient);
		}
		cClient.finishRegister(response, account);
	}
	/**
	 * 	Dispatch polling action.
	 * 
	 * @param account String
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 */
	public static void dispatchPolling(final String account,
			final HttpServletRequest request, final HttpServletResponse response){
		Browser client = null;
		for (RequestWorker w : workers) {
			client = w.getClient(account);
			if (client != null)
			{
				client.doPoll(request, response);
				if(client.isMvIsExpiredSession())
				{					
					w.unresgisterByAccount(account) ;								
				}
				break;
			}
				
		}	
	}
	/**
	 * Unregister a client.
	 * 
	 * @param client Client
	 */
	public static void unregisterByAccount(final String account) {
		if (logger.isDebugEnabled())
			logger.debug(String.format("Unregister by account %s", account));
		boolean succes = false;
		for (RequestWorker w : workers) {
			if (w.unresgisterByAccount(account) != null) {
				succes = true;
				break;
			}
		}
		if (succes)
			balanceClientsForWorkers();
	}	
	/**
	 * Balance number of clients for each worker.	 
	 */
	private static void balanceClientsForWorkers() {
		if (workers.size() < 2)
			return;
		final long start = System.currentTimeMillis();
		int totalClient = 0;
		int minNumClient = -1;
		int maxNumClient = 0;
		for (RequestWorker w : workers) {
			w.setBalancingClient(true);
			totalClient += w.getNumClients();
			if (minNumClient == -1) {
				minNumClient = w.getNumClients();				
			} else if (minNumClient > w.getNumClients()) {
				minNumClient = w.getNumClients();				
			}
			if (maxNumClient < w.getNumClients())
				maxNumClient = w.getNumClients();
		}
		//Balance number of clients for each worker if the difference is more than balanceOffset
		final int diff = maxNumClient - minNumClient;
		if (totalClient > 0 && diff >= balanceOffset) {			
			final int numClientPerWorker = totalClient / numOfWorkers;
			final List<Browser> movingList = new ArrayList<Browser>();
			final List<RequestWorker> receivableWorkers = new ArrayList<RequestWorker>();
			for (RequestWorker w : workers) {
				final List<Browser> removedClients = w.checkToRemove(numClientPerWorker);
				if (removedClients != null && !removedClients.isEmpty()) {
					movingList.addAll(removedClients);
				} else {
					receivableWorkers.add(w);
				}
			}
			//Move removed clients to receivable workers
			for (RequestWorker w : receivableWorkers) {						
				int num = 0;
				while (num < numClientPerWorker && !movingList.isEmpty()) {
					num = w.resgisterClient(movingList.remove(0));
				}
			}
			//still remain clients should be moved => Add them to 1st worker of receivable list
			if (!movingList.isEmpty()) {
				final RequestWorker rcvWroker = receivableWorkers.get(0);
				while (!movingList.isEmpty()) {
					rcvWroker.resgisterClient(movingList.remove(0));
				}
			}
		}
		//Done
		for (RequestWorker w : workers) {
			w.setBalancingClient(false);
			if (logger.isDebugEnabled())
				logger.debug(String.format("RequestHandlerWorker#%d handles %d clients",
						w.getWorkerId(), w.getNumClients()));
		}
		if (logger.isDebugEnabled()) {
			logger.debug(String.format("End balancing number of clients(%d) in %d(ms)",
					totalClient, System.currentTimeMillis() - start));
		}
	}
	/**
	 * @return the workers
	 */
	public static List<RequestWorker> getWorkers() {
		return workers;
	}
	/**
	 * Get all online accounts.
	 * 
	 * @return List<String> (never null)
	 */
	public static List<String> getAllOnlineAccounts() {
		final List<String> list = new ArrayList<String>();
		for (RequestWorker w : workers)
			list.addAll(w.getAllAccounts());
		return list;
	}
	
	/**
	 * Get all online accounts that register specific info.
	 * 
	 * @return List<String> (never null)
	 */
	public static List<String> getAllOnlineAccounts(String filterType) {
		final List<String> list = new ArrayList<String>();
		for (RequestWorker w : workers)
			list.addAll(w.getAllAccounts(filterType));
		return list;
	}
	
	/**
	 * Get all online accounts split into sub-list, each sub-list has maximum splitSize elements.
	 * 
	 * @param splitSize int
	 * @return List<List<String>> (maybe null)
	 */
	public static List<List<String>> getAllOnlineAccounts(final int splitSize) {
		final List<String> all = new ArrayList<String>();
		for (RequestWorker w : workers)
			all.addAll(w.getAllAccounts());
		final int size = all.size();
		logger.info("Number of online users: " + size);
		if (size > 0) {
			final List<List<String>> retList = new ArrayList<List<String>>();
			int numSplit = size / splitSize;
			if (size % splitSize != 0)
				numSplit++;
			int fromIndex;
			int toIndex;
			for (int i = 0; i < numSplit; i++) {
				fromIndex = i * splitSize;
				toIndex = fromIndex + splitSize;
				if (toIndex > size)
					toIndex = size;
				retList.add(all.subList(fromIndex, toIndex));
			}
			all.clear();
			return retList;
		} else {
			return null;
		}
	}
	
	//Register/unRegister dynamic update information
	public static int registerDynamicUpdateInfo(String accountNo,
			HttpServletRequest request, HttpServletResponse response, boolean isRegister) {
		int result = Constants.CONNECTED_SUCCESS;
		Browser client = null;
		for (RequestWorker w : workers) {
			client = w.getClient(accountNo);
			if (client != null)
				break;
		}
		
		if (client != null) {
			String typesString = request.getParameter(Constants.XTYPE);
			if(typesString != null && !typesString.isEmpty()){
				Set<String> types = new HashSet<String>(Arrays.asList(typesString.split(Constants.SEPERATE_CHAR)));
				for(String type: types)
				{
					updateDynamicInfo(client, type, isRegister, request);					
				}
				
				//Send init data for new register info
				if(isRegister == true)
				{
					client.finishRegister(response, accountNo, types);					
				}
				else
				{
					client.finishunRegister(response);
				}
			}			
		}
		else
		{
			//This client is not initialized successfully or lost session
			result = Constants.ERROR_NOT_REGISTERED;			
		}
		
		return result;
	}
	

	//Update dynamic info
	private static void updateDynamicInfo(Browser client, String updateType, boolean isRegister,
			HttpServletRequest request) {
		if (Constants.CASH_BALANCE_TYPE.equalsIgnoreCase(updateType)) {
			client.setEnableAccountBalance(isRegister);
		} else if (Constants.MARKET_INDEX_TYPE.equalsIgnoreCase(updateType)) {
			client.setEnableMarketIndex(isRegister);
		} else if (Constants.ORDER_LIST_TYPE.equalsIgnoreCase(updateType)) {
			client.setEnableOrderEnquiry(isRegister);
		} else if (Constants.WARNING_LIST_TYPE.equalsIgnoreCase(updateType)) {
			String listSymbol = request.getParameter("listSymbol");
			List<String> registerSymbols = new ArrayList<String>();
			if (listSymbol != null) {
				String[] list = listSymbol.split(Constants.SEPERATE_CHAR);
				for (String symbol : list) {
					registerSymbols.add(symbol);
				}
			} else {
				registerSymbols.add("");
			}

			client.setWarningList(registerSymbols);
			client.setEnableWarningList(isRegister);
		} else if (Constants.WATCH_LIST_TYPE.equalsIgnoreCase(updateType)) {
			client.setEnableWatchList(isRegister);
			client.setEnableStockwatchInfo(true);
			String listSymbol = request.getParameter("listSymbol");
			List<String> registerSymbols = new ArrayList<String>();
			if (listSymbol != null) {
				String[] list = listSymbol.split(Constants.SEPERATE_CHAR);
				for (String symbol : list) {
					String[] stockInfos = symbol.split("\\|");
					if(stockInfos.length >= 2){
						String swId = client.getAccount() + "|" + symbol + "|" + StockWatchInfo.WATCH_LIST;
						// add stock symbol to dynamic update
						MarketDataCachingManager.addStockWatchSymbol(String.format("%s|%s", stockInfos[1], stockInfos[0]));
						StockWatchDataCachingManager.registerStockWatch(client.getAccount(), stockInfos[1], stockInfos[0], swId, StockWatchInfo.WATCH_LIST);
					}
					
					registerSymbols.add(symbol);
				}
			} else {
				registerSymbols.add("");
			}

			client.setSymbolInfo(registerSymbols);
			client.setEnableWatchList(isRegister);
		} else if (Constants.WORLD_MARKET_INDEX_TYPE.equalsIgnoreCase(updateType)) {
			client.setEnableWorldMarketIndex(isRegister);
		} else if (Constants.STOCK_WATCH_TYPE.equalsIgnoreCase(updateType)) {
			client.setEnableStockwatchInfo(true);
			if (isRegister) {
				String currentSymbol = request.getParameter("symbol");
				String marketId = request.getParameter("marketId");
				String mvStockWatchID = client.getAccount() + "|" + marketId + "|" + currentSymbol + "|" + StockWatchInfo.STOCK_WATCH;
				// add stock symbol to dynamic update
				MarketDataCachingManager.addStockWatchSymbol(String.format("%s|%s", marketId, currentSymbol));
				StockWatchDataCachingManager.registerStockWatch(client.getAccount(), currentSymbol, marketId,mvStockWatchID);
				client.changedStockWatchId(mvStockWatchID);
			} else {
				String mvStockWatchID = request.getParameter("stockWatchID");
				StockWatchDataCachingManager.unregisterStockWatch(mvStockWatchID);
			}
		}

	}
	
	public static Browser getCurrentBrowser(String lvAccount)
	{
		Browser cClient = null;
		for (RequestWorker w : workers) {
			cClient = w.getClient(lvAccount);
			if(cClient != null)
				break;
		}			
			
		return cClient;		
	}
}
