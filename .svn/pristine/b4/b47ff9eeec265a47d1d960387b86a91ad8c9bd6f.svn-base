package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.ttl.old.itrade.util.Presentation;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * The HKSExerciseSubmitTxn Class definition for all method
 * Market Exercise to submit on the server
 * @author not attributable
 *
 */
public class HKSExerciseSubmitTxn
{
    private String mvEntitlementId;
    private String mvType;
    private String mvTypeDescription;
    private String mvProductId;
    private String mvMarketId;
    private String mvBookCloseInstrumentId;
    private String mvInstrumentID;
    private String mvExerciseQty;
    private String mvExcessQty;
    private String mvStatus;
    private String mvNumOfSharesHeld;
    private String mvNumOfSharesHeldBalanceDate;
    private String mvNameOfRightsIssue;
    private String mvCurrencyID;
    private String mvNumOfSharesAvailableOfSubscription;
    private String mvIssuePrice;
    private String mvClosingDate;
    private String mvExercisableQty;
    private String mvNumOfSharesAvailableForAcquisition;
    private String mvProposedAcquistionPrice;
    private String mvSubscriptionId;
    private String mvClientId;
    private String mvTradingAccSeq;
    private String mvIsConfirm;
    private String mvIsCheckPassword;
    private String mvPassword;
    private int mvReturnCode;
    private String mvErrorCode;
    private String mvErrorMessage;

    private String mvStatusDescription;
    private String mvOffercurrencyID;
    private String mvExerciseRatioDelivery;
    private String mvExerciseRatioPer;
    private String mvExIssueInstrumentID;
    private String mvAccountSeq;
    private String mvLocationID;
    //private String mvProvisionalCode;


    private String mvLastModifiedTime;

    /**
     * This variable is used to the tp Error
     */
    TPErrorHandling				tpError;
    Presentation presentation = null;

    /**
     * Default constructor for HKSExerciseSubmitTxn class
     * @param pPresentation the Presentation class
     */
    public HKSExerciseSubmitTxn(Presentation pPresentation)
    {
        tpError = new TPErrorHandling();
        presentation = pPresentation;
    }

    /**
     * Default constructor for HKSExerciseSubmitTxn class
     * @param pPresentation the Presentation class
     * @param pClientId the client id
     */
    public HKSExerciseSubmitTxn(Presentation pPresentation, String pClientId)
    {
        this(pPresentation);
        setClientId(pClientId);
    }
    /**
     * This method process Market Exercise to submit on the server
     * @param pPresentation the Presentation class
     */
    public void process(Presentation pPresentation)
    {
        try
        {
            Hashtable		lvTxnMap = new Hashtable();

            IMsgXMLNode		lvRetNode;
            TPManager ivTPManager = ITradeServlet.getTPManager(RequestName.HKSExerciseSubmitRequest);
            TPBaseRequest       lvExerciseSubmitRequest = ivTPManager.getRequest(RequestName.HKSExerciseSubmitRequest);

            lvTxnMap.put(TagName.CLIENTID, getClientId());
            lvTxnMap.put("TRADINGACCSEQ", getTradingAccSeq());
            lvTxnMap.put(TagName.PRODUCTID, getProductId());
            lvTxnMap.put(TagName.MARKETID, getMarketId());
            lvTxnMap.put(TagName.BOOKCLOSEINSTRUMENTID, getBookCloseInstrumentId());
            lvTxnMap.put(TagName.INSTRUMENTID, getInstrumentID());
            lvTxnMap.put(TagName.STATUS, getStatus());
            lvTxnMap.put(TagName.ENTITLEMENTID, getEntitlementId());
            lvTxnMap.put(TagName.TYPE, getType());
            lvTxnMap.put("TYPEDISCRIPTION", getTypeDescription());
            lvTxnMap.put("EXCESSQTY", getExcessQty());
            lvTxnMap.put("NUMOFSHARESHELD", getNumOfSharesHeld());
            lvTxnMap.put("NUMOFSHARESHELDBALANCEDATE", getNumOfSharesHeldBalanceDate());
            lvTxnMap.put("NAMEOFRIGHTSISSUE", getNameOfRightsIssue());
            lvTxnMap.put(TagName.CURRENCYID, getCurrencyID());
            lvTxnMap.put("NUMOFSHARESAVAILABLEOFSUB", getNumOfSharesAvailableOfSubscription());
            lvTxnMap.put("ISSUEPRICE", getIssuePrice());
            lvTxnMap.put("CLOSINGDATE", getClosingDate());
            lvTxnMap.put("EXERCISABLEQTY", getExercisableQty());
            lvTxnMap.put("NUMOFSHARESAVAILABLEFORACQ", getNumOfSharesAvailableForAcquisition());
            lvTxnMap.put("PROPOSEDACQUISTIONPRICE", getProposedAcquistionPrice());
            lvTxnMap.put("SUBSCRIPTIONID", getSubscriptionId());
            lvTxnMap.put("EXERCISEQTY", getExerciseQty());
            lvTxnMap.put("PASSWORD", getPassword());
            lvTxnMap.put("ISCONFIRM", getIsConfirm());
            lvTxnMap.put("ISCHECKPASSWORD", getIsCheckPassword());
            lvTxnMap.put("EXERCISERATIODELIVERY", getExerciseRatioDelivery());
            lvTxnMap.put("EXISSUEINSTRUMENTID", getExIssueInstrumentID());
            lvTxnMap.put("EXERCISERATIOPER", getExerciseRatioPer());
            lvTxnMap.put("ACCOUNTSEQ", getAccountSeq());
            lvTxnMap.put("OFFERCURRENCYID", getOffercurrencyID());
            lvTxnMap.put("STATUSDESCRIPTION", getStatusDescription());
            lvTxnMap.put("LOCATIONID", getLocationID());











            lvRetNode = lvExerciseSubmitRequest.send(lvTxnMap);

            setReturnCode(tpError.checkError(lvRetNode));
            if (mvReturnCode != TPErrorHandling.TP_NORMAL)
            {
                setErrorMessage(tpError.getErrDesc());
                if (mvReturnCode == TPErrorHandling.TP_APP_ERR)
                {
                    setErrorCode(tpError.getErrCode());
                }
            }
            else
            {
                if (lvRetNode.getChildNode(TagName.LOOP).getChildNode(TagName.LOOP_ELEMENT).getChildNode(TagName.LASTMODIFIEDTIME)!=null)
                {
                        setLastModifiedTime(lvRetNode.getChildNode(TagName.LOOP).getChildNode(TagName.LOOP_ELEMENT).getChildNode(TagName.LASTMODIFIEDTIME).getValue());
                }
                if (lvRetNode.getChildNode(TagName.LOOP).getChildNode(TagName.LOOP_ELEMENT).getChildNode("REMAININGAMOUNT")==null)
                    return;


            }
        }
        catch (Exception e)
        {
            Log.println( e , Log.ERROR_LOG);
        }
    }
    /**
     * Get method for entitlement id
     * @return entitlement id
     */
    public String getEntitlementId()
    {
        return mvEntitlementId;
    }
    /**
     * Set method for entitlement id
     * @param pEntitlementId the entitlement id
     */
    public void setEntitlementId(String pEntitlementId)
    {
        mvEntitlementId = pEntitlementId;
    }
    /**
     * Get method for subscription id
     * @return subscription id
     */
    public String getSubscriptionId()
    {
            return mvSubscriptionId;
    }
    /**
     * Get method for subscription id
     * @param pSubscriptionId the subscription id
     */
    public void setSubscriptionId(String pSubscriptionId)
    {
            mvSubscriptionId = pSubscriptionId;
    }
    /**
     * Get method for exercise quantity
     * @return	exercise quantity
     */
    public String getExerciseQty()
    {
            return mvExerciseQty;
    }
    /**
     * Set method for exercise quantity
     * @param pExerciseQty the exercise quantity
     */
    public void setExerciseQty(String pExerciseQty)
    {
            mvExerciseQty = pExerciseQty;
    }
    /**
     * Get method for exerciesable quantity
     * @return exerciesable quantity
     */
    public String getExercisableQty()
    {
            return mvExercisableQty;
    }
    /**
     * Set method for exerciesable quantity
     * @param pExercisableQty the exerciesable quantity
     */
    public void setExercisableQty(String pExercisableQty)
    {
            mvExercisableQty = pExercisableQty;
    }

    /**
     * Get method for Excess Quantity
     * @return Excess Quantity
     */
    public String getExcessQty()
    {
            return mvExcessQty;
    }
    /**
     * Set method for Excess Quantity
     * @param pExcessQty the Excess Quantity
     */
    public void setExcessQty(String pExcessQty)
    {
            mvExcessQty = pExcessQty;
    }
    /**
     * Get method for excrcise type
     * @return excrcise type
     */
    public String getType()
    {
            return mvType;
    }
    /**
     * Set method for excrcise type
     * @param pType the excrcise type
     */
    public void setType(String pType)
    {
            mvType = pType;
    }
    /**
     * Get method for excrcise type description    
     * @return excrcise type description 
     */
    public String getTypeDescription()
    {
            return mvTypeDescription;
    }
    /**
     * Set method for excrcise type description  
     * @param pTypeDescription the excrcise type description 
     */
    public void setTypeDescription(String pTypeDescription)
    {
            mvTypeDescription = pTypeDescription;
    }
    /**
     * Get method for number of shares held
     * @return number of shares held
     */
    public String getNumberOfSharesHeld()
    {
            return mvNumOfSharesHeld;
    }
    /**
     * Set method for number of shares held
     * @param pNumOfSharesHeld the number of shares held
     */
    public void setNumberOfSharesHeld(String pNumOfSharesHeld)
    {
            mvNumOfSharesHeld = pNumOfSharesHeld;
    }
    /**
     * Get method for product id
     * @return product id
     */
    public String getProductId()
    {
            return mvProductId;
    }
    /**
     * Set method for product id
     * @param pProductId the product id
     */
    public void setProductId(String pProductId)
    {
            mvProductId = pProductId;
    }
    /**
     * Get method for market id
     * @return market id
     */
    public String getMarketId()
    {
            return mvMarketId;
    }
    /**
     * Set method for market id
     * @param pMarketId the market id
     */
    public void setMarketId(String pMarketId)
    {
            mvMarketId = pMarketId;
    }
    /**
     * Get method for book close instrument id
     * @return book close instrument id
     */
    public String getBookCloseInstrumentId()
    {
            return mvBookCloseInstrumentId;
    }
    /**
     * Set method for book close instrument id
     * @param pBookCloseInstrumentId the book close instrument id
     */
    public void setBookCloseInstrumentId(String pBookCloseInstrumentId)
    {
            mvBookCloseInstrumentId = pBookCloseInstrumentId;
    }
    /**
     * Get method for instrument id 
     * @return instrument id 
     */
    public String getInstrumentID()
    {
            return mvInstrumentID;
    }
    /**
     * Set method for instrument id 
     * @param pInstrumentID the instrument id 
     */
    public void setInstrumentID(String pInstrumentID)
    {
            mvInstrumentID = pInstrumentID;
    }

    /**
     * Get method for exercise status 
     * @return exercise status
     */
    public String getStatus()
    {
            return mvStatus;
    }
    /**
     * Set method for exercise status 
     * @param pStatus the exercise status
     */
    public void setStatus(String pStatus)
    {
            mvStatus = pStatus;
    }
    /**
     * Get method for number of shares held
     * @return number of shares held
     */
    public String getNumOfSharesHeld()
    {
            return mvNumOfSharesHeld;
    }
    /**
     * Set method for number of shares held
     * @param pNumOfSharesHeld the number of shares held
     */
    public void setNumOfSharesHeld(String pNumOfSharesHeld)
    {
            mvNumOfSharesHeld = pNumOfSharesHeld;
    }
    /**
     * Get method for number of shares held balance date
     * @return number of shares held balance date
     */
    public String getNumOfSharesHeldBalanceDate()
    {
            return mvNumOfSharesHeldBalanceDate;
    }
    /**
     * Set method for number of shares held balance date
     * @param pNumOfSharesHeldBalanceDate the number of shares held balance date
     */
    public void setNumOfSharesHeldBalanceDate(String pNumOfSharesHeldBalanceDate)
    {
            mvNumOfSharesHeldBalanceDate = pNumOfSharesHeldBalanceDate;
    }
    /**
     * Get method for name fo right issue
     * @return the name fo right issue
     */
    public String getNameOfRightsIssue()
    {
            return mvNameOfRightsIssue;
    }
    /**
     * Set method for name fo right issue
     * @param pNameOfRightsIssue the name fo right issue
     */
    public void setNameOfRightsIssue(String pNameOfRightsIssue)
    {
            mvNameOfRightsIssue = pNameOfRightsIssue;
    }
    /**
     * Get method for currency id
     * @return currency id 
     */
    public String getCurrencyID()
    {
            return mvCurrencyID;
    }
    /**
     * Set method for currency id
     * @param pCurrencyID the currency id
     */
    public void setCurrencyID(String pCurrencyID)
    {
            mvCurrencyID = pCurrencyID;
    }
    /**
     * Get method for number of shares available of subscription
     * @return number of shares available of subscription
     */
    public String getNumOfSharesAvailableOfSubscription()
    {
            return mvNumOfSharesAvailableOfSubscription;
    }
    /**
     * Set method for number of shares available of subscription
     * @param pNumOfSharesAvailableOfSubscription the number of shares available of subscription
     */
    public void setNumOfSharesAvailableOfSubscription(String pNumOfSharesAvailableOfSubscription)
    {
            mvNumOfSharesAvailableOfSubscription = pNumOfSharesAvailableOfSubscription;
    }
    /**
     * Get method for issue price
     * @return issue price
     */
    public String getIssuePrice()
    {
            return mvIssuePrice;
    }
    /**
     * Set method for issue price
     * @param pIssuePrice the issue price
     */
    public void setIssuePrice(String pIssuePrice)
    {
            mvIssuePrice = pIssuePrice;
    }
    /**
     * Get mthod for book closing date
     * @return book closing date
     */
    public String getClosingDate()
    {
            return mvClosingDate;
    }
    /**
     * Set mthod for book closing date
     * @param pClosingDate the book closing date
     */
    public void setClosingDate(String pClosingDate)
    {
            mvClosingDate = pClosingDate;
    }
    /**
     * Get method for number of shares available for acquisition 
     * @return number of shares available for acquisition 
     */
    public String getNumOfSharesAvailableForAcquisition()
    {
            return mvNumOfSharesAvailableForAcquisition;
    }
    /**
     * Get method for number of shares available for acquisition 
     * @param pNumOfSharesAvailableForAcquisition the number of shares available for acquisition 
     */
    public void setNumOfSharesAvailableForAcquisition(String pNumOfSharesAvailableForAcquisition)
    {
            mvNumOfSharesAvailableForAcquisition = pNumOfSharesAvailableForAcquisition;
    }
    /**
     * Get method for proposed acquistion price 
     * @return proposed acquistion price 
     */
    public String getProposedAcquistionPrice()
    {
            return mvProposedAcquistionPrice;
    }
    /**
     * Set method for proposed acquistion price 
     * @param pProposedAcquistionPrice the proposed acquistion price 
     */
    public void setProposedAcquistionPrice(String pProposedAcquistionPrice)
    {
            mvProposedAcquistionPrice = pProposedAcquistionPrice;
    }






    /**
     * Get method for is check client password
     * @return is check client password
     */
    public String getIsCheckPassword()
    {
            return mvIsCheckPassword;
    }
    /**
     * Set method for is check client password
     * @param pIsCheckPassword the is check client password
     */
    public void setIsCheckPassword(String pIsCheckPassword)
    {
            mvIsCheckPassword = pIsCheckPassword;
    }
    /**
     * Get method for is order confirm
     * @return is order confirm
     */
    public String getIsConfirm()
    {
            return mvIsConfirm;
    }
    /**
     * Set method for is order confirm
     * @param pIsConfirm the is order confirm
     */
    public void setIsConfirm(String pIsConfirm)
    {
            mvIsConfirm = pIsConfirm;
    }

    /**
     * Get method for client id
     * @return client id
     */
    public String getClientId()
    {
        return mvClientId;
    }
    /**
     * Set method for client id
     * @param pClientId the client id
     */
    public void setClientId(String pClientId)
    {
        mvClientId = pClientId;
    }
    /**
     * Get method for trading account seq
     * @return trading account seq
     */
    public String getTradingAccSeq()
    {
            return mvTradingAccSeq;
    }
    /**
     * Set method for trading account seq
     * @param pTradingAccSeq the trading account seq
     */
    public void setTradingAccSeq(String pTradingAccSeq)
    {
            mvTradingAccSeq = pTradingAccSeq;
    }
    /**
     * Get method for client password
     * @return client password
     */
    public String getPassword()
    {
            return mvPassword;
    }
    /**
     * Set method for client password
     * @param pPassword the client password
     */
    public void setPassword(String pPassword)
    {
            mvPassword = pPassword;
    }
    /**
     * Get method for exercise status description
     * @return exercise status description
     */
    public String getStatusDescription()
    {
            return mvStatusDescription;
    }
    /**
     * Set method for offer currency id
     * @return offer currency id
     */
    public String getOffercurrencyID()
    {
            return mvOffercurrencyID;
    }
    /**
     * Get method for the exercise ration delivery 
     * @return the exercise ration delivery 
     */
    public String getExerciseRatioDelivery()
    {
            return mvExerciseRatioDelivery;
    }
    /**
     * Get method for exercis ratio per
     * @return exercis ratio per
     */
    public String getExerciseRatioPer()
    {
            return mvExerciseRatioPer;
    }
    /**
     * Get method for exercise issue instrument id
     * @return exercise issue instrument id
     */
    public String getExIssueInstrumentID()
    {
            return mvExIssueInstrumentID;
    }
    /**
     * Get method for account seq 
     * @return account seq
     */
    public String getAccountSeq()
    {
            return mvAccountSeq;
    }
    /**
     * Get method for location id
     * @return location id
     */
    public String getLocationID()
    {
            return mvLocationID;
    }

//    public String getProvisionalCode()
//    {
//            return mvProvisionalCode;
//    }
    /**
     * Get method for last modified exercise time
     */
    public String getLastModifiedTime()
    {
            return mvLastModifiedTime;
    }

    /**
     * Set method for exercise status description
     * @param pStatusDescription the exercise status description
     */
    public void setStatusDescription(String pStatusDescription)
    {
            mvStatusDescription = pStatusDescription;
    }
    /**
     * Set method for offer currency id
     * @param pOffercurrencyID the offer currency id
     */
    public void setOffercurrencyID(String pOffercurrencyID)
    {
            mvOffercurrencyID = pOffercurrencyID;
    }
    /**
     * Set method for exercise ratio delivery
     * @param pExerciseRatioDelivery the exercise ratio delivery
     */
    public void setExerciseRatioDelivery(String pExerciseRatioDelivery)
    {
            mvExerciseRatioDelivery = pExerciseRatioDelivery;
    }
    /**
     * Set method for exercise ratio per
     * @param pExerciseRatioPer the exercise ratio per
     */
    public void setExerciseRatioPer(String pExerciseRatioPer)
    {
            mvExerciseRatioPer = pExerciseRatioPer;
    }
    /**
     * Set method for exercise issue instrument id
     * @param pExIssueInstrumentID the exercise issue instrument id
     */
    public void setExIssueInstrumentID(String pExIssueInstrumentID)
    {
            mvExIssueInstrumentID = pExIssueInstrumentID;
    }
    /**
     * Set method for account seq
     * @param pAccountSeq the account seq
     */
    public void setAccountSeq(String pAccountSeq)
    {
            mvAccountSeq = pAccountSeq;
    }
    /**
     * Set method for Location id
     * @param pLocationID the Location id
     */
    public void setLocationID(String pLocationID)
    {
            mvLocationID = pLocationID;
    }

//    public void setProvisionalCode(String pProvisionalCode)
//    {
//            mvProvisionalCode = pProvisionalCode;
//    }
    /**
     * Set method for last modified exercise time
     * @return pLastModifiedTime the last modified exercise time
     */
    public void setLastModifiedTime(String pLastModifiedTime)
    {
            mvLastModifiedTime = pLastModifiedTime;
    }


    /**
     * Get method for system return code
     * @return system return code
     */
    public int getReturnCode()
    {
        return mvReturnCode;
    }
    /**
     * Set method for system return code
     * @param pReturnCode the system return code
     */
    public void setReturnCode(int pReturnCode)
    {
        mvReturnCode = pReturnCode;
    }
    /**
     * Get method for system error code
     * @return system error code
     */
    public String getErrorCode()
    {
        return mvErrorCode;
    }
    /**
     * Set method for system error code
     * @param pErrorCode the system error code
     */
    public void setErrorCode(String pErrorCode)
    {
        mvErrorCode = pErrorCode;
    }
    /**
     * Get method for system error message
     * @return system error message
     */
    public String getErrorMessage()
    {
        return mvErrorMessage;
    }
    /**
     * Set method for system error message
     * @param pErrorMessage the system error message
     */
    public void setErrorMessage(String pErrorMessage)
    {
        mvErrorMessage = pErrorMessage;
    }
    /**
     * This method for Localized error message
     * @param pErrorCode the localized error code
     * @param pDefaultMesg the default message
     * @return  the Localized error message
     */
   public String getLocalizedErrorMessage(String pErrorCode, String pDefaultMesg)
   {
      String lvRetMessage;

      try
      {
         if (pErrorCode.equals("0") || pErrorCode.equals("999"))
         {
            lvRetMessage = pDefaultMesg;
         }
         else
         {
            lvRetMessage = (String)presentation.getErrorCodeBundle().getString(pErrorCode);
         }
      }
      catch (Exception ex)
      {
         Log.println("Presentation._showPage: missing error mapping for " + pErrorCode + ", Default = " + pDefaultMesg, Log.ERROR_LOG);

         lvRetMessage = pDefaultMesg;
      }
      return lvRetMessage;
   }
}
