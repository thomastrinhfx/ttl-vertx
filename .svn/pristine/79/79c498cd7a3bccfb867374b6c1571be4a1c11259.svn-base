package com.ttl.old.itrade.comet.cometManagement;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ttl.old.itrade.comet.translator.DataTranslator;
import com.ttl.old.itrade.hks.bean.HKSAccountBalanceEnquiryBean;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.eclipse.jetty.continuation.Continuation;
import org.eclipse.jetty.continuation.ContinuationSupport;


import com.ttl.old.itrade.comet.dataInfo.Alert;
import com.ttl.old.itrade.comet.dataInfo.CommonInfo;
import com.ttl.old.itrade.comet.dataInfo.MarketIndex;
import com.ttl.old.itrade.comet.dataInfo.MarketInfo;
import com.ttl.old.itrade.comet.dataInfo.StockWatchList;
import com.ttl.old.itrade.comet.dataInfo.WorldMarketIndex;
import com.ttl.old.itrade.comet.eventHandler.caching.MarketDataCachingManager;
import com.ttl.old.itrade.comet.translator.DataTranslatorFactory;
import com.ttl.old.itrade.comet.utils.Constants;
import com.ttl.old.itrade.hks.HKSMain;
import com.ttl.old.itrade.hks.HKSTag;
import com.ttl.old.itrade.hks.bean.HKSOrderBean;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.util.Log;



public class Browser {
	private static final Logger logger = LogManager.getLogger(Browser.class);
	private static final DataTranslator translator = DataTranslatorFactory.getDataTranslator();
	private final int maxAge;
	private static final String POLL_ACTION_MSG = "{\"action\":\"update\"}";
	private final String account;
	private final String accountType;
	private final List<String> symbolInfo;
	private Continuation continuation;
	private final Object syncSession = new Object();
	private final List<String> pendingList;
	private long lastAccessTime;
	private String currentStockWatch;
	private List<String> warningList;
	
	private boolean enableMarketIndex = false;
	private boolean enableWatchList = false;
	private boolean enableAccountBalance = false;
	//private boolean enableOrderList = false;
	private boolean enableWarningList = false;
	private boolean enableWorldMarketIndex = false;
	private boolean enableStockwatchInfo = false;
	private boolean enableOrderEnquiry = false;
	
	private  HttpSession mvSession; 
	
	private boolean mvIsExpiredSession = false;
	
	private Set<String> changedStockWatchIds = new HashSet<String>();
	
	/**
	 * Constructor.
	 * 
	 * @param account String
	 */
	public Browser(final String account, final HttpSession session, final int suspendTimeout) {
		this.account = account;
		this.mvSession =  session;
		this.accountType = session != null ? session.getAttribute(HKSTag.ACCOUNTTYPE).toString(): "";
		this.pendingList = new ArrayList<String>();		
		this.symbolInfo = new ArrayList<String>();
		this.maxAge = suspendTimeout + 5000;
		this.currentStockWatch = "";
		this.warningList = new ArrayList<String>();
		this.lastAccessTime = System.currentTimeMillis();
	}
	/**
	 * Notify finishing register.
	 * 
	 * @param response HttpServletResponse
	 */
	public final void finishRegister(final HttpServletResponse response, final String accountNo) {
		//this.write(response, serverTime);
		this.write(response, translator.translateInitDataToJson(accountNo, this));
		//this.write(response, translator.translateAccountSummaryToJson(AccountSummaryDataCachingManager.getAccountSummaryInfo(accountNo)));
	}
	
	public final void finishRegister(final HttpServletResponse response, final String accountNo, Set<String> types) {
		this.write(response, translator.translateChangedDataToJson(accountNo, this, types));
	}
	
	public final void finishunRegister(final HttpServletResponse response) {
		this.write(response, "{'unregister':'success'}");
	
	}
	/**
	 * Execute poll action.
	 * 
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 */
	public final void doPoll(final HttpServletRequest request, final HttpServletResponse response) {
		synchronized (syncSession) {
			if(!isExpiredSession())
			{
				this.lastAccessTime = System.currentTimeMillis();
				if (!pendingList.isEmpty()) {
					try {
						final StringBuilder strBuff = new StringBuilder();
						for(String elm : pendingList) {
							if (elm.startsWith("{")) {
								elm = elm.substring(1, elm.length() -1);
							}
							if (strBuff.length() == 0) {
								strBuff.append('{').append(elm);
							} else {
								strBuff.append(',').append(elm);
							}
						}
						strBuff.append("}");
						this.write(response, strBuff.toString());
						//this.write(response, elm);
						pendingList.clear();
					} catch (Exception ex) {
						logger.error(String.format("Do poll for client %s error %s", account, ex.getMessage()));
					}
				} else {
					continuation = ContinuationSupport.getContinuation(request);
					if (continuation.isInitial()) {
						continuation.setTimeout(maxAge);
						try
						{
							continuation.suspend();
						}
						catch (Exception e) {
							// TODO: handle exception
							logger.error(e.toString());
						}
						
					} else {
						this.write(response, POLL_ACTION_MSG);
					}
					try {
						ContinuationUtils.storeContinuation(mvSession, continuation);
					} catch (Exception e) {
						logger.error("Store continuation failed: " + e.getMessage());
					}
				}				
			}	
			else
			{				
				this.write(response, String.format("{'success':false, 'mvResult':'%d'}", Constants.ERROR_SESSION_EXPIRE));
				mvIsExpiredSession = true;
			}
		}
	}
	
	/**
	 * Write data to the client.
	 * 
	 * @param response HttpServletResponse
	 * @param data String - data in json
	 */
	private void write(final HttpServletResponse response, final String data) {
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(data);
			out.flush();
		} catch (Exception ex) {
			logger.error("Cannot write data for client " + account, ex);
		} finally {
			if (out != null)
				try {
					out.close();
				} catch (Exception ex) {}
		}		
	}	
	/**
	 * Response data to client.
	 * 
	 * @param dataInJson String
	 */
	public final void response(final String dataInJson) {
		synchronized (syncSession) {
			pendingList.add(dataInJson);
			this.notifyToPoll();	
		}
	}	
	/**
	 * Check age of the client.
	 * 
	 * @return true if the client is valid, otherwise false
	 */
	public final boolean checkAge() {
		
		if(IMain.getProperty("checkCommetSession")!= null && IMain.getProperty("checkCommetSession").equalsIgnoreCase("false"))
			return true;
		else
		{
			synchronized (syncSession) {
				final long age = System.currentTimeMillis() - lastAccessTime;
				final boolean valid = (age < maxAge);
				if (!valid/* && logger.isInfoEnabled()*/) {
					logger.info(String.format("Check age: client %s has been expired", account));				
				}			
				return valid;
			}			
		}		
	}
	/**
	 * Notify client gets data.
	 */
	private void notifyToPoll() {
		if (continuation != null) {
			try {
				continuation.resume();
//				continuation = null;
			} catch (Exception ex) { //Maybe client closed
				if (logger.isDebugEnabled())
					logger.debug(String.format("Response to client %s error %s", account, ex.getMessage()));
			}
		}
	}
	/**
	 * @param symbols the symbols to set
	 */
	public void setSymbolInfo(final List<String> symbols) {
		synchronized (syncSession) {
			this.symbolInfo.clear();
			this.symbolInfo.addAll(symbols);
			MarketDataCachingManager.addRegisterSymbol(symbols);
		}
	}
	
	public List<String> getSymbolInfo()
	{
		return this.symbolInfo;
	}
	
	public void setWarningList(final List<String> symbols) {
		synchronized (syncSession) {
			this.warningList.clear();
			this.warningList.addAll(symbols);
			MarketDataCachingManager.setWarningSymbols(symbols);
		}
	}
	
	/**
	 * @return the account
	 */
	public String getAccount() {
		return account;
	}

	
	public String getAccountType() {
		return accountType;
	}
	/**
	 * Response account summary if there is any change.
	 * 
	 * @param accSum AccountSummaryInfo
	 */
	public final void responseAccountSummary(final HKSAccountBalanceEnquiryBean accSum) {
		synchronized (syncSession) {
			if(enableAccountBalance)
			{
				String accountBalance = translator.translateAccountSummaryToJson(accSum);
				if(!GenericValidator.isBlankOrNull(accountBalance))
				{
					pendingList.add(accountBalance);
					this.notifyToPoll();
				}
			}
		}
	}
	
	//Response Watch List - Market Data
	public final void responseMarketData(final MarketInfo market) {
		synchronized (syncSession) {
			if(enableWatchList)
			{
				String values = translator.translateMarketDataToJson(market, symbolInfo);
				if(!GenericValidator.isBlankOrNull(values))
				{
					pendingList.add(values);
					this.notifyToPoll();
				}				
			}
			
			if(enableWarningList)
			{
				String values = translator.translateWarningMarketDataToJson(market, warningList);
				if(!GenericValidator.isBlankOrNull(values))
				{
					pendingList.add(values);
					this.notifyToPoll();
				}				
			}
		}
	}
	
	//Response Market Index
	public final void responseMarketIndex(final MarketIndex marketIndex) {
		synchronized (syncSession) {
			if(enableMarketIndex)
			{
				String values = translator.translateMarketIndexToJson(marketIndex);
				if(!GenericValidator.isBlankOrNull(values))
				{
					pendingList.add(values);
					this.notifyToPoll();
				}
			}
		}
	}
	
	//Response World Market Index
	public final void responseWorldMarketIndex(final WorldMarketIndex wMarketIndex) {
		synchronized (syncSession) {
			if(enableWorldMarketIndex)
			{
				String values = translator.translateWorldMarketIndexToJson(wMarketIndex);
				if(!GenericValidator.isBlankOrNull(values))
				{
					pendingList.add(values);
					this.notifyToPoll();
				}
			}
		}
	}
	
	public final void responseStockWatchInfo(final StockWatchList stockWatch) {
		synchronized (syncSession) {
			if(enableStockwatchInfo)
			{
				String values = translator.translateStockWatchInfoToJson(stockWatch);
				if(!GenericValidator.isBlankOrNull(values))
				{
					pendingList.add(values);
					this.notifyToPoll();
				}
			}
		}
	}
	
	public void responseOrderEnquiryInfo(final List<HKSOrderBean> beanList) {
		synchronized (syncSession) {
			if(enableOrderEnquiry)
			{
				String values = translator.translateOrderEnquiryInfoToJson(beanList);
				if(!GenericValidator.isBlankOrNull(values))
				{
					pendingList.add(values);
					this.notifyToPoll();
				}
			}
		}
	}
	
	public void responseAlertInfo(List<Alert> alerts) {
		synchronized (syncSession) {
			String values = translator.translateAlertInfoToJson(alerts);
			if(!GenericValidator.isBlankOrNull(values))
			{
				pendingList.add(values);
				this.notifyToPoll();
			}
		}
	}
	
	public void responseCommonInfo(CommonInfo info) {
		synchronized (syncSession) {
			String values = translator.translateCommonInfoToJson(info);
			if(!GenericValidator.isBlankOrNull(values))
			{
				pendingList.add(values);
				this.notifyToPoll();
			}
		}
	}
	
	public void responseErrorConnection(final int errorCode) {
		synchronized (syncSession) {
			pendingList.add(String.format("{'success':false, 'mvResult':'%d'}", errorCode));
			this.notifyToPoll();
		}
	}
	
	public boolean isExpiredSession()
	{
		if (this.mvSession == null){
	        return true;
		}
		
		String lvClientID = (String) this.mvSession.getAttribute(HKSTag.CLIENTID);
		if(HKSMain.svSessionTracker != null){
			HttpSession lvClientSession = (HttpSession)HKSMain.svSessionTracker.getSession(lvClientID);
			if(lvClientSession != null){
				java.sql.Timestamp lvLastUpdatedTime = (java.sql.Timestamp)lvClientSession.getAttribute("LASTUPDATETIME");
	            if(lvLastUpdatedTime != null && (System.currentTimeMillis() - lvLastUpdatedTime.getTime()) > 1800000){
					try {
						forceExpire();
						lvClientSession.invalidate();
					} catch(IllegalStateException e){
		                  //No need to log because it will be thrown only when Session is already invalidated
		            }
		            Log.println("HKSRegisterClientService : Remove Session! ", Log.DEBUG_LOG);		
		            HKSMain.svSessionTracker.removeSession(lvClientID);
		            return true;
	            }
			}
		}
		return false;		
	}
	
	public String getCurrentStockWatch() {
		return currentStockWatch;
	}
	public void setCurrentStockWatch(String currentStockWatch) {
		this.currentStockWatch = currentStockWatch;
	}
	public boolean isEnableMarketIndex() {
		return enableMarketIndex;
	}
	public void setEnableMarketIndex(boolean enableMarketIndex) {
		this.enableMarketIndex = enableMarketIndex;
	}
	public boolean isEnableWatchList() {
		return enableWatchList;
	}
	public void setEnableWatchList(boolean enableWatchList) {
		this.enableWatchList = enableWatchList;
	}
	public boolean isEnableAccountBalance() {
		return enableAccountBalance;
	}
	public void setEnableAccountBalance(boolean enableAccountBalance) {
		this.enableAccountBalance = enableAccountBalance;
	}

	public boolean isEnableWarningList() {
		return enableWarningList;
	}
	public void setEnableWarningList(boolean enableWarningList) {
		this.enableWarningList = enableWarningList;
	}
	public boolean isEnableWorldMarketIndex() {
		return enableWorldMarketIndex;
	}
	public void setEnableWorldMarketIndex(boolean enableWorldMarketIndex) {
		this.enableWorldMarketIndex = enableWorldMarketIndex;
	}
	public boolean isEnableStockwatchInfo() {
		return enableStockwatchInfo;
	}
	public void setEnableStockwatchInfo(boolean enableStockwatchInfo) {
		this.enableStockwatchInfo = enableStockwatchInfo;
	}
	public boolean isEnableOrderEnquiry() {
		return enableOrderEnquiry;
	}
	public void setEnableOrderEnquiry(boolean enableOrderEnquiry) {
		this.enableOrderEnquiry = enableOrderEnquiry;
	}
	/**
	 * @param mvSession the mvSession to set
	 */
	public void setMvSession(HttpSession mvSession) {
		this.mvSession = mvSession;
	}
	/**
	 * @return the mvSession
	 */
	public HttpSession getMvSession() {
		return mvSession;
	}
	/**
	 * @param mvIsExpiredSession the mvIsExpiredSession to set
	 */
	public void setMvIsExpiredSession(boolean mvIsExpiredSession) {
		this.mvIsExpiredSession = mvIsExpiredSession;
	}
	/**
	 * @return the mvIsExpiredSession
	 */
	public boolean isMvIsExpiredSession() {
		return mvIsExpiredSession;
	}
	
	public void changedStockWatchId(String id){
		changedStockWatchIds.add(id);
	}
	
	public Set<String> getChangedStockWatchIds() {
		return changedStockWatchIds;
	}
	
	@Override
	public String toString() {
		return "Browser [maxAge=" + maxAge + ", account=" + account + ", accountType=" + accountType + ", symbolInfo="
				+ symbolInfo + ", continuation=" + continuation + ", syncSession=" + syncSession + ", pendingList="
				+ pendingList + ", lastAccessTime=" + lastAccessTime + ", currentTime=" + System.currentTimeMillis() + ", currentStockWatch=" + currentStockWatch
				+ ", warningList=" + warningList + ", enableMarketIndex=" + enableMarketIndex + ", enableWatchList="
				+ enableWatchList + ", enableAccountBalance=" + enableAccountBalance + ", enableWarningList="
				+ enableWarningList + ", enableWorldMarketIndex=" + enableWorldMarketIndex + ", enableStockwatchInfo="
				+ enableStockwatchInfo + ", enableOrderEnquiry=" + enableOrderEnquiry + ", mvSession=" + mvSession
				+ ", mvIsExpiredSession=" + mvIsExpiredSession + "]";
	}
	
	public void forceExpire(){
		try {
			try {
				Log.println(">>> forceExpire all existing continuations for client account: " + account, Log.ACCESS_LOG);
				int count = ContinuationUtils.expiredExistingContinuations(mvSession, null);
				Log.println("<<< forceExpire all existing continuations for client account: " + account + ": count: " + count, Log.ACCESS_LOG);
			} catch (Exception e) {
				Log.println("forceExpire all existing continuations for client account: " + account + ": error: " + e.getMessage(), Log.ERROR_LOG);
			}
			Log.println(">>> forceExpire current continuation for client account: " + account + ":continuation:" + continuation, Log.ACCESS_LOG);
			ContinuationUtils.expiredContinuation(continuation);
			Log.println("<<< forceExpire current continuation for client account: " + account + ":continuation:" + continuation, Log.ACCESS_LOG);
		} catch (Exception e) {
			Log.println("forceExpire current continuation for client account: " + account + ":continuation:" + continuation + " >>> " + e.getMessage(), Log.ERROR_LOG);
		}
	}
	
}
