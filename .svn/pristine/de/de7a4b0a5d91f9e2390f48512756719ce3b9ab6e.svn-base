package com.ttl.old.itrade.hks.txn;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import com.ttl.old.itrade.hks.HKSTag;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.hks.util.TextFormatter;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.tag.ITagXsfTagName;
import com.systekit.winvest.hks.config.mapping.TagName;
import com.systekit.winvest.hks.util.Utils;

public class HKSAccountBalanceSummaryTxn extends BaseTxn {
	
	private String lvClientID;
	private String lvTradingAccSeq;
	@SuppressWarnings("unchecked")
	private Hashtable lvTxnMap;
	private static String HKSBO_WEB_QUERY_PORTFOLIO_CONFIRMED_BUY_SELL = "CBS";
	private static String HKSBO_WEB_QUERY_CURRENT_PORTFOLIO = "CU";
	private static String HKSBO_WEB_QUERY_ACCOUNT_SUMMARY = "AS";
	private static final String SELL="S";

	private boolean IsMarginAcc;
	private  List lvConfirmedBuyPortfolio = new ArrayList();
	
	private  List lvConfirmedSellPortfolio = new ArrayList();
	
	@SuppressWarnings("unchecked")
	private List lvCurrentPortfolio = new ArrayList();
	
	private List lvAccountSummary  = new ArrayList();
	
	DateFormat dateFormatFull = new SimpleDateFormat ("yyyy-MM-dd");
	DateFormat dateFormat = new SimpleDateFormat ("dd/MM/yyyy"); 
	
	public static final String CASHACCOUNTSUMMARY = "CASHACCOUNTSUMMARY";
	public static final String TRADINGACCOUNTSUMMARY = "TRADINGACCOUNTSUMMARY";
	/**
	 * @param lvConfirmedSellPortfolio the lvConfirmedSellPortfolio to set
	 */
	public void setLvConfirmedSellPortfolio(List lvConfirmedSellPortfolio) {
		this.lvConfirmedSellPortfolio = lvConfirmedSellPortfolio;
	}

	/**
	 * @return the lvConfirmedSellPortfolio
	 */
	public List getLvConfirmedSellPortfolio() {
		return lvConfirmedSellPortfolio;
	}

	/**
	 * @param lvConfirmedBuyPortfolio the lvConfirmedBuyPortfolio to set
	 */
	public  void setLvConfirmedBuyPortfolio(List pConfirmedBuyPortfolio) {
		lvConfirmedBuyPortfolio = pConfirmedBuyPortfolio;
	}

	/**
	 * @return the lvConfirmedBuyPortfolio
	 */
	public  List getLvConfirmedBuyPortfolio() {
		return lvConfirmedBuyPortfolio;
	}

	public HKSAccountBalanceSummaryTxn(String pClientId,String pTradingAccSeq){
		lvClientID = pClientId;
		lvTradingAccSeq = pTradingAccSeq;
	}
	
	@SuppressWarnings("unchecked")
	public void queryConfirmedBuySellPortfolio()
	{
		Log.println("Start queryConfirmedSellPortfolio Txn",Log.ACCESS_LOG);
		try {
			lvTxnMap = new Hashtable();
		    lvTxnMap.put(TagName.CLIENTID, lvClientID);
		    // add Confirm Buy(CB) type
		    lvTxnMap.put(TagName.TRADINGACCSEQ, lvTradingAccSeq);
		    lvTxnMap.put(TagName.TYPE,HKSBO_WEB_QUERY_PORTFOLIO_CONFIRMED_BUY_SELL);
		    HashMap lvModel = null;
		    if (TPErrorHandling.TP_NORMAL == process("HKSQueryConfirmBuySellPortfolioRequest", lvTxnMap)) {
				IMsgXMLNodeList lvRowList = mvReturnNode
						.getNodeList(ITagXsfTagName.CHILD_ROW);
				for (int i = 0; i < lvRowList.size(); i++) {
					IMsgXMLNode lvNode = lvRowList.getNode(i);
					 
					lvModel = new HashMap();
					lvModel.put("STOCKID", lvNode.getChildNode("STOCKID").getValue());
					lvModel.put("MARKETID", lvNode.getChildNode("MARKETID").getValue());
					Date activationDate = dateFormatFull.parse(lvNode.getChildNode("ACTIVATIONDATE").getValue());
					lvModel.put("ACTIVATIONDATE", dateFormat.format(activationDate));
					
					//Date dateFormatString = dateFormatFull.parse(lvNode.getChildNode("ACTIVATIONDATE").getValue());
					//dateFormatString.toString();
					lvModel.put("AVGPRICE", lvNode.getChildNode("AVGPRICE").getValue());
					lvModel.put("FEE", lvNode.getChildNode("FEE").getValue());
					
					Date settledDate = dateFormatFull.parse(lvNode.getChildNode("SETTLEDDATE").getValue());
					lvModel.put("SETTLEDDATE", dateFormat.format(settledDate));
					
					lvModel.put("FILLEDQTY", lvNode.getChildNode("FILLEDQTY").getValue());
					lvModel.put("TRADECONSIDERATION", lvNode.getChildNode("TRADECONSIDERATION").getValue());
					lvModel.put("NOMINALPRICE", lvNode.getChildNode("NOMINALPRICE").getValue());
					lvModel.put("CURRENTAMT", lvNode.getChildNode("CURRENTAMT").getValue());
					
					if (lvNode.getChildNode(TagName.BS).getValue()
													.equalsIgnoreCase(SELL)) {
						lvConfirmedSellPortfolio.add(lvModel);
					} else {
						lvConfirmedBuyPortfolio.add(lvModel);
					}
				}
		    }else {
		    	Log.println("Error error occur when communicate with TP",Log.ACCESS_LOG);
		    	if(mvReturnNode.getChildNode("C_ERROR_CODE") != null)
		    	{
		    		Log.println(mvReturnNode.getChildNode("C_ERROR_CODE").getValue()+
	    					mvReturnNode.getChildNode("C_ERROR_DESC").getValue(),Log.ACCESS_LOG);
		    	}
		    }
		} catch (Exception e) {
			Log.println(e,Log.ERROR_LOG);
		}finally{
			Log.println("End queryConfirmedSellPortfolio Txn",Log.ACCESS_LOG);
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public void queryCurrentPortfolio()
	{
		Log.println("Start queryCurrentPortfolio Txn",Log.ACCESS_LOG);
		try {
			lvTxnMap = new Hashtable();
		    lvTxnMap.put(TagName.CLIENTID, lvClientID);
		    // add Confirm Buy(CB) type
		    lvTxnMap.put(TagName.TRADINGACCSEQ, lvTradingAccSeq);
		    lvTxnMap.put(TagName.TYPE, HKSBO_WEB_QUERY_CURRENT_PORTFOLIO);
		  
		    HKSPortfolioEnquiryTxn lvHKSPortfolioEnquiryTxn = new HKSPortfolioEnquiryTxn(lvClientID, lvTradingAccSeq, true);
	        lvHKSPortfolioEnquiryTxn.process();
	        int lvReturnCode = lvHKSPortfolioEnquiryTxn.getReturnCode();
	        
			if (lvReturnCode != TPErrorHandling.TP_NORMAL) {
				if (lvReturnCode == TPErrorHandling.TP_APP_ERR) {
					Log.println("[ HKSPortfolioAction.enquiryPortfolio(): Portfolio Enquiry for " + lvClientID + " fail with application error:" + lvHKSPortfolioEnquiryTxn.getErrorMessage()+ " ]",Log.ERROR_LOG);
				} else if (lvReturnCode == TPErrorHandling.TP_SYS_ERR) {
					Log.println("[ HKSPortfolioAction.enquiryPortfolio(): Portfolio Enquiry for " + lvClientID + " fail with system error:" + lvHKSPortfolioEnquiryTxn.getErrorMessage() + " ]",Log.ERROR_LOG);

				}
			}else{
				HKSPortfolioEnquiryDetails[] lvHKSPortfolioEnqDetails = lvHKSPortfolioEnquiryTxn.getPortfolioEnquiryDetails();
				if(lvHKSPortfolioEnquiryTxn.getLoopCounter() > 0){
					
					HashMap lvModel = null;
					for(int i = 0; i < lvHKSPortfolioEnqDetails.length; i++){
						lvModel= new HashMap();
						
						//BEGIN TASK #:- TTL-VN Giang Tran 20101122 Fixed bug portfolio
						BigDecimal lvMarketPrice =  new BigDecimal(lvHKSPortfolioEnqDetails[i].getMvMktPrice()) ;
						BigDecimal lvLegerQty =  new BigDecimal(lvHKSPortfolioEnqDetails[i].getLedgerQty()) ;						
						
						BigDecimal lvSettledBalance = new BigDecimal(lvHKSPortfolioEnqDetails[i].getTSettled());						
						BigDecimal lvTTodayConfirmSell = new BigDecimal(lvHKSPortfolioEnqDetails[i].getTTodayConfirmSell());
						BigDecimal lvTInactiveSell = new BigDecimal(lvHKSPortfolioEnqDetails[i].getTInactiveSell());
						BigDecimal lvTTodaySell = new BigDecimal(lvHKSPortfolioEnqDetails[i].getTTodaySell());
						BigDecimal lvQueuingSell = lvTTodaySell.subtract(lvTTodayConfirmSell).add(lvTInactiveSell);
						
						// Market Value = MarketPrice * LedgerQty
					//	BigDecimal lvMarketValue = lvLegerQty.multiply(lvMarketPrice);
					
			            //END TASK #:- TTL-VN Giang Tran 20101122 Fixed bug portfolio
						
						BigDecimal lvUsable = lvSettledBalance.subtract(lvTTodaySell);
						BigDecimal lvMarketValue = lvUsable.multiply(lvMarketPrice);
						
						lvModel.put(TagName.MARKETID, lvHKSPortfolioEnqDetails[i].getMarketID().toString());
						lvModel.put(TagName.STOCKID, lvHKSPortfolioEnqDetails[i].getInstrumentId().toString());
						
						lvModel.put(TagName.USABLE, lvUsable.toString());
						
						lvModel.put(TagName.NOMINALPRICE, lvHKSPortfolioEnqDetails[i].getNominalPrice().toString());
						lvModel.put(TagName.AVGPRICE, lvHKSPortfolioEnqDetails[i].getMvUnitPrice().toString());
						
						lvModel.put(TagName.MARKETVALUE, lvMarketValue.toString());
						
						 // get leger balance & avgPrice
			            BigDecimal lvAvgPrice = new BigDecimal(lvHKSPortfolioEnqDetails[i].getMvUnitPrice());
			            
			            // WAC: Average Value = AvgPrice * LegerBalance
			            BigDecimal lvAverageValue = lvUsable.multiply(lvAvgPrice);
						
						lvModel.put(TagName.COSTQTY, lvAverageValue.toString());
						//BigDecimal lvTotalAverageCost = new BigDecimal(lvHKSPortfolioEnqDetails[i].getTotalAverageCost());
					
						
						if(lvUsable.compareTo(new BigDecimal(0)) > 0 || lvQueuingSell.compareTo(new BigDecimal(0)) > 0){
							getLvCurrentPortfolio().add(lvModel);
						}
					}
				}
				}
			          
		} catch (Exception e) {
			Log.println(e,Log.ERROR_LOG);
		}finally{
			Log.println("End queryCurrentPortfolio Txn",Log.ACCESS_LOG);
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public void queryAccountSummary()
	{
		Log.println("Start queryAccountSummary Txn",Log.ACCESS_LOG);
		try {
			lvTxnMap = new Hashtable();
		    lvTxnMap.put(TagName.CLIENTID, lvClientID);
		    // add Account Summary(AS) type
		    lvTxnMap.put(TagName.TRADINGACCSEQ, lvTradingAccSeq);
		    lvTxnMap.put(TagName.TYPE, HKSBO_WEB_QUERY_ACCOUNT_SUMMARY);
		    if(IsMarginAcc){
		    	lvTxnMap.put("ISMARGINACC", "Y");
		    }else{
		    	lvTxnMap.put("ISMARGINACC", "N");
		    }
		    HashMap lvModel= null;
		    if (TPErrorHandling.TP_NORMAL == process("HKSQueryAccountBalanceInfoRequest", lvTxnMap)) {
		    	IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList(ITagXsfTagName.CHILD_ROW);
		    	for (int i = 0; i < lvRowList.size(); i++) {
		    	IMsgXMLNode lvNode = lvRowList.getNode(i);
		    	lvModel = new  HashMap();
		    	
		    	lvModel.put("ISMARGINACCOUNT", isIsMarginAcc());
		    	
		    	lvModel.put("CTODAYUNSETTLEBUY", lvNode.getChildNode("CTODAYUNSETTLEBUY").getValue());
				lvModel.put("CT1UNSETTLEBUY", lvNode.getChildNode("CT1UNSETTLEBUY").getValue());
				lvModel.put("CT2UNSETTLEBUY", lvNode.getChildNode("CT2UNSETTLEBUY").getValue());
				lvModel.put("CT3UNSETTLEBUY", lvNode.getChildNode("CT3UNSETTLEBUY").getValue());
				
				
				lvModel.put("CTODAYUNSETTLESELL", lvNode.getChildNode("CTODAYUNSETTLESELL").getValue());
				lvModel.put("CT1UNSETTLESELL", lvNode.getChildNode("CT1UNSETTLESELL").getValue());
				lvModel.put("CT2UNSETTLESELL", lvNode.getChildNode("CT2UNSETTLESELL").getValue());
				lvModel.put("CT3UNSETTLESELL", lvNode.getChildNode("CT3UNSETTLESELL").getValue());
		
				
				lvModel.put("CASHHOLDVALUE", lvNode.getChildNode("CASHHOLDVALUE").getValue());
				
				lvModel.put("ADVANCEABLEAMT", lvNode.getChildNode("ADVANCEABLEAMT").getValue());
				lvModel.put("DRAWABLEBAL", lvNode.getChildNode("DRAWABLEBAL").getValue());

				/*if(IsMarginAcc){
					BigDecimal manualReserve =  Utils.parseBigDecimal( lvNode.getChildNode("CMANUALRESERVE").getValue());
					// Begin Task - VanTran  2010-11-11 If margin account then check system status is day end or not
			         // If system is not day end then set Reserve amount = Reserve amount - today out
					if (!CheckSystemStatus.isDayEndSystemStatus()) {
						BigDecimal todayOut =  Utils.parseBigDecimal( lvNode.getChildNode("CTODAYOUT").getValue());
						manualReserve = manualReserve.subtract(todayOut);
					}
					// End Task - VanTran  2010-11-11 If margin account then check system status is day end or not
			         // If system is not day end then set Reserve amount = Reserve amount - today out
					if(manualReserve.signum() <0){
						lvModel.put("TRADABLEBAL", "0");
					}else {
						lvModel.put("TRADABLEBAL", manualReserve.toString());
					}
				}else {*/
					//BigDecimal cSettled = Utils.parseBigDecimal( lvNode.getChildNode("CSETTLED").getValue());
					BigDecimal lvSettled =Utils.parseBigDecimal(lvNode.getChildNode("DRAWABLEBAL").getValue()).subtract(
							Utils.parseBigDecimal(lvNode.getChildNode("ADVANCEABLEAMT").getValue()));
					if(lvSettled.signum() < 0){
						lvModel.put("TRADABLEBAL", "0");
					}else {
						lvModel.put("TRADABLEBAL", lvSettled.toString());
					}
				//}
				
				lvModel.put("STOCKMARKETVALUE", lvNode.getChildNode("STOCKMARKETVALUE").getValue());
				lvModel.put("STOCKHOLDVALUE", lvNode.getChildNode("STOCKHOLDVALUE").getValue());
				lvModel.put("TMORTGAGEHOLDAMT", lvNode.getChildNode("TMORTGAGEHOLDAMT").getValue());
	
				lvModel.put("TOTALOUTSTANDINGADVANCE", lvNode.getChildNode("TOTALOUTSTANDINGADVANCE").getValue());

				if(IsMarginAcc){
					//LOANOUTSTANDINGF,MARGINCALLF,MARGINABLEVALUEF,MARGINVALUEF
					lvModel.put("LOANOUTSTANDINGF", lvNode.getChildNode("LOANOUTSTANDINGF").getValue());
					lvModel.put("MARGINCALLF", lvNode.getChildNode("MARGINCALLF").getValue());
					lvModel.put("MARGINABLEVALUEF", lvNode.getChildNode("MARGINABLEVALUEF").getValue());
					lvModel.put("MARGINVALUEF", lvNode.getChildNode("MARGINVALUEF").getValue());
					lvModel.put("DEBITACCRUEDINTEREST", lvNode.getChildNode("DEBITACCRUEDINTEREST").getValue());
					lvModel.put("MARGINEXPIRYDATE", lvNode.getChildNode("MARGINEXPIRYDATE").getValue());
					lvModel.put("CREDITLIMIT", lvNode.getChildNode("CREDITLIMIT").getValue());
					
				}else {
					lvModel.put("LOANOUTSTANDINGF", 0);
				}
				lvAccountSummary.add(lvModel);
		    	}
		    }else {
		    	Log.println("Error error occur when communicate with TP",Log.ACCESS_LOG);
		    	if(mvReturnNode.getChildNode("C_ERROR_CODE") != null)
		    	{
		    		Log.println(mvReturnNode.getChildNode("C_ERROR_CODE").getValue()+
	    					mvReturnNode.getChildNode("C_ERROR_DESC").getValue(),Log.ACCESS_LOG);
		    	}
		    }
		} catch (Exception e) {
			Log.println( e,Log.ERROR_LOG);
		}finally{
			Log.println("End queryAccountSummary Txn",Log.ACCESS_LOG);
		}		
	}
	
	
	@SuppressWarnings("unchecked")
	public void queryAccountSummaryUsingPortfolio()
	{
		
		Log.println("Start queryAccountSummaryUsingPortfolio Txn",Log.ACCESS_LOG);
		try {
			
			lvTxnMap = new Hashtable();
		    lvTxnMap.put(TagName.CLIENTID, lvClientID);
		    lvTxnMap.put("ENABLEADVANCEMONEYQUERY","Y");
		    lvTxnMap.put("INCLUDETRADINGACCOUNTDETAILS","Y");
		      
			if (TPErrorHandling.TP_NORMAL == process(
					RequestName.HKSClientAccountBalanceEnquiry, lvTxnMap, "")) {
				//TRADINGACCOUNTSUMMARY
				IMsgXMLNodeList  lvTradingDetail =  mvReturnNode.getChildNode("LOOP").getNodeList("LOOP_ELEMENT");
				Double lvCT0UnsettleBuy = new Double(0);
				Double lvCT1UnsettleBuy = new Double(0);
				Double lvCT2UnsettleBuy = new Double(0);
				Double lvCT3UnsettleBuy = new Double(0);
				Double lvNominalPrice = new Double(0);
				Double lvTempValue = new Double(0);
				Double lvTodaySellHold = new Double(0);
				if (lvTradingDetail != null) {
					for (int i = 0; i < lvTradingDetail.size(); i++) {
						lvNominalPrice = Utils.parseDouble(lvTradingDetail.getNode(i).getChildNode("NOMINAL").getValue());
						lvTempValue = Utils.parseDouble(lvTradingDetail.getNode(i).getChildNode("DUEBUY").getValue());
						
						lvCT3UnsettleBuy += lvNominalPrice*lvTempValue;
						
						lvTempValue = Utils.parseDouble(lvTradingDetail.getNode(i).getChildNode("TT2UNSETTLEBUY").getValue());
						lvCT2UnsettleBuy += lvNominalPrice*lvTempValue;

						lvTempValue = Utils.parseDouble(lvTradingDetail.getNode(i).getChildNode("TT1UNSETTLEBUY").getValue());
						lvCT1UnsettleBuy += lvNominalPrice*lvTempValue;

						// lvCT0UnsettleBuy = TTODAYCONFIRMBUY + TTODAYUNSETTLEBUY
						lvTempValue = Utils.parseDouble(lvTradingDetail.getNode(i).getChildNode("TTODAYCONFIRMBUY").getValue());
						lvCT0UnsettleBuy += lvNominalPrice*lvTempValue;
						lvTempValue = Utils.parseDouble(lvTradingDetail.getNode(i).getChildNode("TTODAYUNSETTLEBUY").getValue());
						lvCT0UnsettleBuy += lvNominalPrice*lvTempValue;
						
						// Today sell hold = TODAYSELL - TTODAYCONFIRMSELL
						lvTempValue = Utils.parseDouble(lvTradingDetail
								.getNode(i).getChildNode("TODAYSELL")
								.getValue())
								- Utils.parseDouble(lvTradingDetail.getNode(i)
										.getChildNode("TTODAYCONFIRMSELL")
										.getValue());
						lvTodaySellHold +=lvNominalPrice*lvTempValue;
					}
				}
				HashMap lvModel= new HashMap();
				lvModel.put("ISMARGINACCOUNT", isIsMarginAcc());
		    	
		    	lvModel.put("CTODAYUNSETTLEBUY", lvCT0UnsettleBuy.toString());
				lvModel.put("CT1UNSETTLEBUY", lvCT1UnsettleBuy.toString());
				lvModel.put("CT2UNSETTLEBUY", lvCT2UnsettleBuy.toString());
				lvModel.put("CT3UNSETTLEBUY", lvCT3UnsettleBuy.toString());
				//CT2UNSETTLEBUY
				
				Double lvCTodayUnsettleSell = new Double(0);
				lvCTodayUnsettleSell = Utils.parseDouble(mvReturnNode
						.getChildNode(CASHACCOUNTSUMMARY)
						.getChildNode("CTODAYCONFIRMSELL").getValue())
						+ Utils.parseDouble(mvReturnNode
								.getChildNode(CASHACCOUNTSUMMARY)
								.getChildNode("CTODAYUNSETTLESELL").getValue());
				lvModel.put("CTODAYUNSETTLESELL",lvCTodayUnsettleSell.toString());
				lvModel.put("CT1UNSETTLESELL", mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("CT1UNSETTLESELL").getValue());
				lvModel.put("CT2UNSETTLESELL", mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("CT2UNSETTLESELL").getValue());
				lvModel.put("CT3UNSETTLESELL", mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("CDUESELL").getValue());
				
				
				//lvModel.put("CASHHOLDVALUE", lvNode.getChildNode("CASHHOLDVALUE").getValue());
				
				Double lvCTodayBuy =Utils.parseDouble(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("CTODAYBUY").getValue());
				Double lvCTodayConfirmBuy =Utils.parseDouble(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("CTODAYCONFIRMBUY").getValue());
				lvModel.put("CASHHOLDVALUE", -(lvCTodayBuy-lvCTodayConfirmBuy));
				//CMAN CTODAYBUY-CTODAYCONFIRMBUY
				//lvModel.put("ADVANCEABLEAMT", lvNode.getChildNode("ADVANCEABLEAMT").getValue());
				String lvAdvanceableAmt = mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("AVAILABLEADVANCEMONEY").getValue();
				lvModel.put("ADVANCEABLEAMT", lvAdvanceableAmt);
				
				//lvModel.put("DRAWABLEBAL", lvNode.getChildNode("DRAWABLEBAL").getValue());
				String lvDrawableBal = mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("DRAWABLEBAL").getValue();
				lvModel.put("DRAWABLEBAL", lvDrawableBal);
				
				/*if(IsMarginAcc){
					BigDecimal manualReserve =  Utils.parseBigDecimal( lvNode.getChildNode("CMANUALRESERVE").getValue());
					// Begin Task - VanTran  2010-11-11 If margin account then check system status is day end or not
			         // If system is not day end then set Reserve amount = Reserve amount - today out
					if (!CheckSystemStatus.isDayEndSystemStatus()) {
						BigDecimal todayOut =  Utils.parseBigDecimal( lvNode.getChildNode("CTODAYOUT").getValue());
						manualReserve = manualReserve.subtract(todayOut);
					}
					// End Task - VanTran  2010-11-11 If margin account then check system status is day end or not
			         // If system is not day end then set Reserve amount = Reserve amount - today out
					if(manualReserve.signum() <0){
						lvModel.put("TRADABLEBAL", "0");
					}else {
						lvModel.put("TRADABLEBAL", manualReserve.toString());
					}
				}else {*/
					//BigDecimal cSettled = Utils.parseBigDecimal( lvNode.getChildNode("CSETTLED").getValue());
				BigDecimal lvMarginValue= new BigDecimal(0);
				
				if(IsMarginAcc){
					lvMarginValue = Utils.parseBigDecimal(mvReturnNode
							.getChildNode(TRADINGACCOUNTSUMMARY)
							.getChildNode("MARGINVALUE").getValue());
				}
				
				//BigDecimal lvPendingOutAdv =null;
				
//				if(IsMarginAcc){
//					//   marginvalue + plus due(todaysettlement = opendaysettle - cduebuy) - today buy amt >0 
//					lvPendingOutAdv = lvMarginValue
//							.add(Utils
//									.parseBigDecimal(mvReturnNode
//											.getChildNode(CASHACCOUNTSUMMARY)
//											.getChildNode("TODAYSETTLEMENT")
//											.getValue())).subtract(
//									Utils.parseBigDecimal(lvCTodayConfirmBuy));
//				}else{
//					// Calculate Pending Outstanding Advance: when drawableBal - advanceableAmt <0 for normal account
//					lvPendingOutAdv = Utils.parseBigDecimal(lvDrawableBal)
//					.subtract(Utils.parseBigDecimal(lvAdvanceableAmt));
//				}
//				if(lvPendingOutAdv.signum() < 0){
//					// Add pending out advance
//					lvModel.put("PENDINGOUTADVANCE", lvPendingOutAdv.abs().toString());
//					//lvModel.put("TRADABLEBAL", "0");
//				}else {
//					// Add pending out advance
//					lvModel.put("PENDINGOUTADVANCE", "0");
//					//lvModel.put("TRADABLEBAL", lvPendingOutAdv.toString());
//				}
				//}
				
				BigDecimal cSettled = Utils.parseBigDecimal( mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("SETTLEDBALANCE").getValue());
				BigDecimal lvCDueBuy = Utils.parseBigDecimal( mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("CDUEBUY").getValue());
				
				lvModel.put("TRADABLEBAL", cSettled.toString());
//				if(cSettled.signum() >0){
//				
//					lvModel.put("OUTLOANSETTLE", "0");
//
//				}else{
//					lvModel.put("TRADABLEBAL", "0");
//					lvModel.put("OUTLOANSETTLE", cSettled.toString());
//				}
				BigDecimal lvPendingBuySettle =new BigDecimal(0);
				
				lvPendingBuySettle = lvCDueBuy.add(Utils.parseBigDecimal(lvCTodayConfirmBuy)).negate();
				
				if(lvPendingBuySettle.signum() < 0){
					// Add pending buy settle
					lvModel.put("PENDINGBUYSETTLE", lvPendingBuySettle.toString());
				}else {
					lvModel.put("PENDINGBUYSETTLE", "0");
				}
						
				
				//lvModel.put("STOCKMARKETVALUE", lvNode.getChildNode("STOCKMARKETVALUE").getValue());
				Double lvMarketValue = Utils.parseDouble(mvReturnNode.getChildNode(TRADINGACCOUNTSUMMARY).getChildNode("MARKETVALUE").getValue());
				
				lvModel.put("STOCKMARKETVALUE", lvMarketValue - lvCT0UnsettleBuy-lvCT1UnsettleBuy-lvCT2UnsettleBuy-lvCT3UnsettleBuy-lvTodaySellHold);
				//MARKETVALUE// TDAYMARKETVALUE
				
				//String lvTodaySell = mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("CTODAYSELL").getValue();
				//String lvTodayConfirmSell = mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("CTODAYCONFIRMSELL").getValue();
				
				//lvModel.put("STOCKHOLDVALUE", Utils.parseBigDecimal(lvTodaySell).subtract(
				//						Utils.parseBigDecimal(lvTodayConfirmSell)));
				
				lvModel.put("STOCKHOLDVALUE", lvTodaySellHold);
				
				//TTODAYSELL - CTODAYCONFIRMSELL
				lvModel.put("TMORTGAGEHOLDAMT", mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("CMORTGAGEHOLDAMT").getValue());

				//lvModel.put("TOTALOUTSTANDINGADVANCE", lvNode.getChildNode("TOTALOUTSTANDINGADVANCE").getValue());
				BigDecimal lvTotalOutAdvance = Utils.parseBigDecimal(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("TOTALOUTSTANDINGADVANCEAMOUNT").getValue());
				

				lvModel.put("TOTALOUTSTANDINGADVANCE", lvTotalOutAdvance.negate().toString());
				//TOTALOUTSTANDINGADVANCEAMOUNT
				
				if(IsMarginAcc){
					//LOANOUTSTANDINGF,MARGINCALLF,MARGINABLEVALUEF,MARGINVALUEF
					//lvModel.put("LOANOUTSTANDINGF", lvNode.getChildNode("LOANOUTSTANDINGF").getValue());
					lvModel.put("LOANOUTSTANDINGF", mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("OUTSTANDINGLOAN").getValue());
					//OUTSTANDINGLOAN
					//lvModel.put("MARGINCALLF", lvNode.getChildNode("MARGINCALLF").getValue());
					lvModel.put("MARGINCALLF", mvReturnNode.getChildNode(TRADINGACCOUNTSUMMARY).getChildNode("MARGINCALL").getValue());
					
					//lvModel.put("MARGINABLEVALUEF", lvNode.getChildNode("MARGINABLEVALUEF").getValue());
					lvModel.put("MARGINABLEVALUEF", mvReturnNode.getChildNode(TRADINGACCOUNTSUMMARY).getChildNode("MARGINABLEVALUE").getValue());
					
					//lvModel.put("MARGINVALUEF", lvNode.getChildNode("MARGINVALUEF").getValue());
					lvModel.put("MARGINVALUEF", lvMarginValue);

					lvModel.put("DEBITACCRUEDINTEREST", mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("DEBITINTEREST").getValue());
					lvModel.put("CREDITLIMIT", mvReturnNode.getChildNode(TRADINGACCOUNTSUMMARY).getChildNode(HKSTag.CREDITLIMIT).getValue());						
									
					String marginExpiryDate = queryAccountMarginContentSummary().get("MARGINEXPIRYDATE").toString();
					
					if (!("").equalsIgnoreCase(marginExpiryDate)) {						
							marginExpiryDate = TextFormatter.getFormattedTime(
									marginExpiryDate, "dd/MM/yyyy");					
					}				
					
					lvModel.put("MARGINEXPIRYDATE", marginExpiryDate );	
					
				}else {
					lvModel.put("LOANOUTSTANDINGF", 0);
				}
				
				lvAccountSummary.add(lvModel);		
		    }
		} catch (Exception e) {
			Log.println( e,Log.ERROR_LOG);
		}finally{
			Log.println("End queryAccountSummaryUsingPortfolio Txn",Log.ACCESS_LOG);
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public HashMap queryAccountMarginContentSummary()
	{  
		HashMap lvModel= null;
		Log.println("Start queryAccountMarginSummary Txn",Log.ACCESS_LOG);
		try {
			lvTxnMap = new Hashtable();
		    lvTxnMap.put(TagName.CLIENTID, lvClientID);		
		    lvTxnMap.put(TagName.TRADINGACCSEQ, lvTradingAccSeq);
		    lvTxnMap.put(TagName.TYPE, HKSBO_WEB_QUERY_ACCOUNT_SUMMARY);
		    lvTxnMap.put("ISMARGINACC", "Y");		    
		   
		  
		    if (TPErrorHandling.TP_NORMAL == process("HKSQueryWebCashBalanceInfoRequest", lvTxnMap)) {
		    	IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList(ITagXsfTagName.CHILD_ROW);
		    	for (int i = 0; i < lvRowList.size(); i++) {
			    	IMsgXMLNode lvNode = lvRowList.getNode(i);
			    	lvModel = new  HashMap();				
			    	
			    	if(lvNode.getChildNode("MARGINEXPIRYDATE")!= null )
			    	{
			    		lvModel.put("MARGINEXPIRYDATE", lvNode.getChildNode("MARGINEXPIRYDATE").getValue());
			    	}			    
			    	else
			    		lvModel.put("MARGINEXPIRYDATE", "");
									
		    	}
		    }else {
		    	Log.println("Error error occur when communicate with TP",Log.ACCESS_LOG);
		    	if(mvReturnNode.getChildNode("C_ERROR_CODE") != null)
		    	{
		    		Log.println(mvReturnNode.getChildNode("C_ERROR_CODE").getValue()+
	    					mvReturnNode.getChildNode("C_ERROR_DESC").getValue(),Log.ACCESS_LOG);
		    	}
		    }
		    
		} catch (Exception e) {
			Log.println( e,Log.ERROR_LOG);
		}finally{
			Log.println("End queryAccountMarginSummary Txn",Log.ACCESS_LOG);
		}
		return lvModel;		
	}
	

	/**
	 * @param lvCurrentPortfolio the lvCurrentPortfolio to set
	 */
	public void setLvCurrentPortfolio(List lvCurrentPortfolio) {
		this.lvCurrentPortfolio = lvCurrentPortfolio;
	}

	/**
	 * @return the lvCurrentPortfolio
	 */
	public List getLvCurrentPortfolio() {
		return lvCurrentPortfolio;
	}

	/**
	 * @param lvAccountSummary the lvAccountSummary to set
	 */
	public void setLvAccountSummary(List lvAccountSummary) {
		this.lvAccountSummary = lvAccountSummary;
	}

	/**
	 * @return the lvAccountSummary
	 */
	public List getLvAccountSummary() {
		return lvAccountSummary;
	}

	public void setIsMarginAcc(boolean isMarginAcc) {
		IsMarginAcc = isMarginAcc;
	}

	public boolean isIsMarginAcc() {
		return IsMarginAcc;
	}
}
