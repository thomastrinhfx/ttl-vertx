# Project guideline (WTRADE)
## Install dependencies
### Prerequisite
#### Maven
+ To install maven: 
	-| [Ubuntu] 	$sudo apt-get install maven
	-| [Windows] 	https://www.mkyong.com/maven/how-to-install-maven-in-windows/
	-| [RHEL]	https://tecadmin.net/install-apache-maven-on-centos/
### Import libraries to local .m2 repo
+ Firstly, run "install_Lib" script file to install libraries in your local maven repositories.
## Import project
Import project as a maven project if your IDE did not recognize it automatically (with pom.xml).
## Classpath
./src/main/java
## React build directory
./src/main/resources/webroot
## Log directory
./TTLLogs
## Build jar package
+ mvn clean package
## Deploy jar package
+ java -jar {FAT_JAR_PACKAGE}.jar, e.g. 
	-| $java -jar Itrade-0.0.1-SNAPSHOT-fat.jar
